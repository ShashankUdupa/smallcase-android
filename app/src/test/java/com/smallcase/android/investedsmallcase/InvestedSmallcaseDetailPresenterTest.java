package com.smallcase.android.investedsmallcase;

import android.app.Activity;

import com.smallcase.android.data.model.Ratios;
import com.smallcase.android.data.model.Stock;
import com.smallcase.android.data.model.StockHistorical;
import com.smallcase.android.data.model.StockInfo;
import com.smallcase.android.data.model.StockPriceAndChange;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.model.PStock;
import com.smallcase.android.view.model.PStockInfo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * Created by shashankm on 16/01/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class InvestedSmallcaseDetailPresenterTest {
    private InvestedSmallcaseDetailPresenter investedSmallcaseDetailPresenter;
    @Mock private SmallcaseRepository smallcaseRepository;
    @Mock private InvestedSmallcaseDetailContract.View investedSmallcaseDetailContract;
    @Mock private SharedPrefService sharedPrefService;
    @Mock private MarketRepository marketRepository;
    @Mock private NetworkHelper networkHelper;
    @Mock private UserRepository userRepository;
    @Mock private Activity activity;
    @Mock private UserSmallcaseRepository userSmallcaseRepository;
    private StockHelper stockHelper = new StockHelper();
    private PStock firstStock = spy(PStock.class);
    private PStock secondStock = spy(PStock.class);
    private PStock thirdStock = spy(PStock.class);
    private PStock fourthStock = spy(PStock.class);
    private PStock fifthStock = spy(PStock.class);
    private PStock sixthStock = spy(PStock.class);

    @Before
    public void setUp() throws Exception {
        investedSmallcaseDetailPresenter = new InvestedSmallcaseDetailPresenter(activity, smallcaseRepository, investedSmallcaseDetailContract,
                sharedPrefService, marketRepository, networkHelper, stockHelper, userRepository, "", userSmallcaseRepository);
    }

    @Test
    public void checkSortForLessThanFiveStocksForPAndL() throws Exception {
        List<PStock> stocks = new ArrayList<>();
        firstStock.setStockPAndL(9.56);
        secondStock.setStockPAndL(91.56);
        thirdStock.setStockPAndL(19.56);
        addToList(stocks, firstStock, secondStock, thirdStock);

        List<PStock> result = investedSmallcaseDetailPresenter.getSortedStocks(stocks, true, 5);
        assertEquals(91.56, result.get(0).getStockPAndL(), 0);
        assertEquals(19.56, result.get(1).getStockPAndL(), 0);
        assertEquals(9.56, result.get(2).getStockPAndL(), 0);
    }

    @Test
    public void checkSortForMoreThanFiveStocksForPAndLReturningAll() throws Exception {
        List<PStock> stocks = new ArrayList<>();
        firstStock.setStockPAndL(9.56);
        secondStock.setStockPAndL(91.56);
        thirdStock.setStockPAndL(19.56);
        fourthStock.setStockPAndL(26.72);
        fifthStock.setStockPAndL(31.72);
        sixthStock.setStockPAndL(-1.72);
        addToList(stocks, firstStock, secondStock, thirdStock, fourthStock, fifthStock, sixthStock);

        List<PStock> result = investedSmallcaseDetailPresenter.getSortedStocks(stocks, true, 6);
        assertEquals(91.56, result.get(0).getStockPAndL(), 0);
        assertEquals(31.72, result.get(1).getStockPAndL(), 0);
        assertEquals(26.72, result.get(2).getStockPAndL(), 0);
        assertEquals(19.56, result.get(3).getStockPAndL(), 0);
        assertEquals(9.56, result.get(4).getStockPAndL(), 0);
        assertEquals(-1.72, result.get(5).getStockPAndL(), 0);
    }

    @Test
    public void checkSortForMoreThanFiveStocksForPAndLReturningFive() throws Exception {
        List<PStock> stocks = new ArrayList<>();
        firstStock.setStockPAndL(9.56);
        secondStock.setStockPAndL(91.56);
        thirdStock.setStockPAndL(19.56);
        fourthStock.setStockPAndL(26.72);
        fifthStock.setStockPAndL(31.72);
        sixthStock.setStockPAndL(-1.72);
        addToList(stocks, firstStock, secondStock, thirdStock, fourthStock, fifthStock, sixthStock);

        List<PStock> result = investedSmallcaseDetailPresenter.getSortedStocks(stocks, true, 5);
        assertEquals(91.56, result.get(0).getStockPAndL(), 0);
        assertEquals(31.72, result.get(1).getStockPAndL(), 0);
        assertEquals(26.72, result.get(2).getStockPAndL(), 0);
        assertEquals(19.56, result.get(3).getStockPAndL(), 0);
        assertEquals(9.56, result.get(4).getStockPAndL(), 0);
        assertEquals(5, result.size());
    }

    @Test
    public void checkSortForLessThanFiveStocksForWeight() throws Exception {
        List<PStock> stocks = new ArrayList<>();
        firstStock.setStockWeightage(9.56);
        secondStock.setStockWeightage(91.56);
        thirdStock.setStockWeightage(19.56);
        addToList(stocks, firstStock, secondStock, thirdStock);

        List<PStock> result = investedSmallcaseDetailPresenter.getSortedStocks(stocks, false, 5);
        assertEquals(91.56, result.get(0).getStockWeightage(), 0);
        assertEquals(19.56, result.get(1).getStockWeightage(), 0);
        assertEquals(9.56, result.get(2).getStockWeightage(), 0);
    }

    @Test
    public void checkSortForMoreThanFiveStocksForWeightReturningAll() throws Exception {
        List<PStock> stocks = new ArrayList<>();
        firstStock.setStockWeightage(9.56);
        secondStock.setStockWeightage(91.56);
        thirdStock.setStockWeightage(19.56);
        fourthStock.setStockWeightage(26.72);
        fifthStock.setStockWeightage(31.72);
        sixthStock.setStockWeightage(-1.72);
        addToList(stocks, firstStock, secondStock, thirdStock, fourthStock, fifthStock, sixthStock);

        List<PStock> result = investedSmallcaseDetailPresenter.getSortedStocks(stocks, false, 6);
        assertEquals(91.56, result.get(0).getStockWeightage(), 0);
        assertEquals(31.72, result.get(1).getStockWeightage(), 0);
        assertEquals(26.72, result.get(2).getStockWeightage(), 0);
        assertEquals(19.56, result.get(3).getStockWeightage(), 0);
        assertEquals(9.56, result.get(4).getStockWeightage(), 0);
        assertEquals(-1.72, result.get(5).getStockWeightage(), 0);
    }

    @Test
    public void checkSortForMoreThanFiveStocksForWeightReturningFive() throws Exception {
        List<PStock> stocks = new ArrayList<>();
        firstStock.setStockWeightage(9.56);
        secondStock.setStockWeightage(91.56);
        thirdStock.setStockWeightage(19.56);
        fourthStock.setStockWeightage(26.72);
        fifthStock.setStockWeightage(31.72);
        sixthStock.setStockWeightage(-1.72);
        addToList(stocks, firstStock, secondStock, thirdStock, fourthStock, fifthStock, sixthStock);

        List<PStock> result = investedSmallcaseDetailPresenter.getSortedStocks(stocks, false, 5);
        assertEquals(91.56, result.get(0).getStockWeightage(), 0);
        assertEquals(31.72, result.get(1).getStockWeightage(), 0);
        assertEquals(26.72, result.get(2).getStockWeightage(), 0);
        assertEquals(19.56, result.get(3).getStockWeightage(), 0);
        assertEquals(9.56, result.get(4).getStockWeightage(), 0);
        assertEquals(5, result.size());
    }

    @Test
    public void checkStockInfoViewModel() throws Exception {
        StockInfo stockInfo = mock(StockInfo.class, RETURNS_DEEP_STUBS);
        StockHistorical stockHistorical = mock(StockHistorical.class);
        StockPriceAndChange stockPriceAndChange = mock(StockPriceAndChange.class, RETURNS_DEEP_STUBS);
        Stock stock = mock(Stock.class, RETURNS_DEEP_STUBS);
        Ratios ratios = mock(Ratios.class);

        when(stockPriceAndChange.getChange()).thenReturn("20");
        when(stockPriceAndChange.getClose()).thenReturn(100.00);
        when(stockPriceAndChange.getPrice()).thenReturn(210.21);

        when(ratios.getFourwpct()).thenReturn(20.123);
        when(ratios.getFiftyTwowpct()).thenReturn(35.7198212);
        when(ratios.getFiftyTwowHigh()).thenReturn(127.56212);
        when(ratios.getFiftyTwowLow()).thenReturn(101.10);
        when(ratios.getPe()).thenReturn(1.0);
        when(ratios.getBeta()).thenReturn(2.71);
        when(ratios.getDivYield()).thenReturn(2.12);

        when(stock.getRatios()).thenReturn(ratios);
        when(stockInfo.getStock()).thenReturn(stock);

        PStockInfo pStockInfo = stockHelper.createStockInfoViewModel(stockInfo, stockPriceAndChange);

        assertEquals("20.00 (20.00%)", pStockInfo.getChange());
        assertEquals("₹ 210.21", pStockInfo.getCurrentPrice());
        assertEquals("20.12%", pStockInfo.getOneMonthReturns());
        assertEquals("35.72%", pStockInfo.getOneYearReturns());
        assertEquals("127.6", pStockInfo.getFiftyTwoWeekHigh());
        assertEquals("101.1", pStockInfo.getFiftyTwoWeekLow());
        assertEquals("1.0", pStockInfo.getpAndE());
        assertEquals("2.7", pStockInfo.getBeta());
        assertEquals("2.1%", pStockInfo.getDivYield());
    }

    private void addToList(List<PStock> stockList, PStock... stocks) {
        Collections.addAll(stockList, stocks);
    }
}