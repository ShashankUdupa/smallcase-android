package com.smallcase.android.investedsmallcase;

import android.app.Activity;
import android.text.SpannableString;

import com.smallcase.android.data.model.InvestedSmallcases;
import com.smallcase.android.data.model.Returns;
import com.smallcase.android.data.model.Stats;
import com.smallcase.android.data.repository.InvestmentRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.util.InvestmentHelper;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.model.PInvestedSmallcase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by shashankm on 11/12/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class InvestedSmallcasesPresenterTest {
    @Mock private InvestedSmallcasesContract.Service investedSmallcasesServiceContract;
    @Mock private InvestedSmallcasesContract.View investedSmallcasesContract;
    @Mock private NetworkHelper networkHelper;
    @Mock private SharedPrefService sharedPrefService;
    @Mock private Activity context;
    @Mock private InvestmentRepository investmentRepository;
    private InvestmentHelper investmentHelper = new InvestmentHelper();
    private InvestedSmallcasesContract.Presenter investedSmallcasesPresenter;
    
    @Before
    public void setUp() throws Exception {
        investedSmallcasesPresenter = new InvestedSmallcasesPresenter(context, investedSmallcasesContract, sharedPrefService,
                investmentRepository, investmentHelper, networkHelper);
    }

    @Test
    public void checkInvestmentsViewModel() throws Exception {

        // Mock param data
        InvestedSmallcases investedSmallcases = mock(InvestedSmallcases.class, RETURNS_DEEP_STUBS);
        Stats stats = mock(Stats.class, RETURNS_DEEP_STUBS);
        Returns returns = mock(Returns.class);

        List<InvestedSmallcases> investedSmallcasesList = new ArrayList<>();

        when(stats.getIndexValue()).thenReturn(120.027);
        when(stats.getLastCloseIndex()).thenReturn(120.01);

        when(returns.getNetworth()).thenReturn(240121.72);
        when(returns.getUnrealizedInvestment()).thenReturn(251231.21);

        when(investedSmallcases.getReturns()).thenReturn(returns);
        when(investedSmallcases.getStats()).thenReturn(stats);
        investedSmallcasesList.add(investedSmallcases);

        // Call method to be tested
        investedSmallcasesPresenter.onInvestmentsReceived(investedSmallcasesList);

        // Create result list
        List<PInvestedSmallcase> viewModelList = new ArrayList<>();
        viewModelList.add(new PInvestedSmallcase("", "","80", "120.03", true, "₹ 2,40,121", new SpannableString("4.42%"), false, "", "", ""));
        verify(investedSmallcasesContract).onInvestedSmallcasesReceived(viewModelList);
    }


}