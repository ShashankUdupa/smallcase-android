package com.smallcase.android.api;

/**
 * Created by shashankm on 29/11/16.
 */
public class TestData {
    public static final String INVESTED_SMALLCASE_JSON = "{\n" +
            "        \"_id\": \"5816daf85f8e1b17c304d59b\",\n" +
            "        \"name\": \"Digital Inclusion\",\n" +
            "        \"source\": \"PROFESSIONAL\",\n" +
            "        \"scid\": \"SCNM_0014\",\n" +
            "        \"status\": \"VALID\",\n" +
            "        \"stats\": {\n" +
            "          \"indexValue\": 106.05505951745346,\n" +
            "          \"lastCloseIndex\": 105.07756979547962,\n" +
            "          \"pe\": 0,\n" +
            "          \"lastWeekCloseIndex\": 105.07756979547962,\n" +
            "          \"lastToLastWeekCloseIndex\": 98.72483499749339\n" +
            "        },\n" +
            "        \"returns\": {\n" +
            "          \"divReturns\": 21,\n" +
            "          \"otherReturns\": 0,\n" +
            "          \"realizedReturns\": 855,\n" +
            "          \"realizedInvestment\": -855,\n" +
            "          \"unrealizedInvestment\": 40655.93000000001,\n" +
            "          \"networth\": 42675822.9\n" +
            "        },\n" +
            "        \"date\": \"2016-10-31T05:47:36.470Z\"\n" +
            "      }";

    public static final String INVESTED_NEWS_JSON_ARRAY = "[{\n" +
            "      \"_id\": \"584aaadb9c81136b00b47a54\",\n" +
            "      \"source\": \"pocket\",\n" +
            "      \"link\": \"http://www.bloombergquint.com/business/2016/12/08/raymond-looks-to-complete-its-own-premium_intro-and-offerings\",\n" +
            "      \"date\": \"2016-12-08T00:00:00.000Z\",\n" +
            "      \"headline\": \"Raymond Looks To ‘Complete’ Its Own Image And Offerings\",\n" +
            "      \"summary\": \"The apparel maker which wants to make you look like a ‘complete man’, is itself going through an premium_intro makeover. Raymond Ltd., one of India’s largest branded cloth manufacturers, has already downed the shutters on 35-40 stores this year and will close another 100 over the next three years.\",\n" +
            "      \"imageUrl\": \"https://img.readitlater.com/i/images.assettype.com/bloombergquint/2016-12/4975ed29-90a6-4b08-a355-54dcb8977610/raymond%202.jpg\",\n" +
            "      \"videoUrl\": null,\n" +
            "      \"status\": \"published\",\n" +
            "      \"sids\": null,\n" +
            "      \"smallcases\": [\n" +
            "        {\n" +
            "          \"info\": {\n" +
            "            \"shortDescription\": \"Companies that own brands new India is embracing\",\n" +
            "            \"name\": \"Brand Value\"\n" +
            "          },\n" +
            "          \"stats\": {\n" +
            "            \"indexValue\": 162.68168394522863\n" +
            "          },\n" +
            "          \"initialIndex\": 165.13729462112212,\n" +
            "          \"scid\": \"SCNM_0007\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"info\": {\n" +
            "            \"shortDescription\": \"Companies catering to India's emerging luxury market\",\n" +
            "            \"name\": \"Bringing the Bling\"\n" +
            "          },\n" +
            "          \"stats\": {\n" +
            "            \"indexValue\": 188.31936809193786\n" +
            "          },\n" +
            "          \"initialIndex\": 188.61061465611607,\n" +
            "          \"scid\": \"SCNM_0014\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"info\": {\n" +
            "            \"shortDescription\": \"Companies expected to benefit from increased economic activity during the Indian festive season\",\n" +
            "            \"name\": \"Shubh Deepawali\"\n" +
            "          },\n" +
            "          \"stats\": {\n" +
            "            \"indexValue\": 88.9290617315051\n" +
            "          },\n" +
            "          \"initialIndex\": 90.0572803715196,\n" +
            "          \"scid\": \"SCNM_0022\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"publisher\": {\n" +
            "        \"publisherId\": \"www.bloombergquint.com\",\n" +
            "        \"name\": \"Bloomberg Quint\",\n" +
            "        \"logo\": \"https://www.smallcase.com/images/newslogos/BloombergQuint.png\"\n" +
            "      },\n" +
            "      \"tags\": [\n" +
            "        \"brand value nm_7\",\n" +
            "        \"bringing the bling nm_15\",\n" +
            "        \"shubh deepawali nm_22\"\n" +
            "      ],\n" +
            "      \"__v\": 0,\n" +
            "      \"flags\": {\n" +
            "        \"featured\": false\n" +
            "      }\n" +
            "    },\n" +
            "    {\n" +
            "      \"_id\": \"58480eebe35d2336ed2e0b11\",\n" +
            "      \"source\": \"pocket\",\n" +
            "      \"link\": \"http://economictimes.indiatimes.com/markets/expert-view/closer-to-budget-keen-on-consumer-stocks-kunj-bansal-centrum-wealth-management/articleshow/55848060.cms\",\n" +
            "      \"date\": \"2016-12-07T00:00:00.000Z\",\n" +
            "      \"headline\": \"Closer to Budget, keen on consumer stocks: Kunj Bansal, Centrum Wealth Management\",\n" +
            "      \"summary\": \"In a chat with ET Now, Kunj Bansal, ED & CIO, Centrum Wealth Management Ltd, says focus will come back to the consumer durable companies as well as consumer goods companies where the portfolio will be positioned on the higher side Edited excerpts: Infrastructure is the consensus.\",\n" +
            "      \"imageUrl\": \"https://img.readitlater.com/i/img.etimg.com/thumb/msid-55848087,width-310,resizemode-4,imglength-10303/kunj-bansal-centrum-wealth-management-ltd.jpg\",\n" +
            "      \"videoUrl\": null,\n" +
            "      \"status\": \"published\",\n" +
            "      \"sids\": null,\n" +
            "      \"smallcases\": [\n" +
            "        {\n" +
            "          \"info\": {\n" +
            "            \"shortDescription\": \"Consumer companies expected to benefit from India's growing middle class\",\n" +
            "            \"name\": \"The Great Indian Middle Class\"\n" +
            "          },\n" +
            "          \"stats\": {\n" +
            "            \"indexValue\": 196.46178234721657\n" +
            "          },\n" +
            "          \"initialIndex\": 197.38883970280895,\n" +
            "          \"scid\": \"SCNM_0014\"\n" +
            "        },\n" +
            "        {\n" +
            "          \"info\": {\n" +
            "            \"shortDescription\": \"Consumer companies expected to benefit from India's growing middle class\",\n" +
            "            \"name\": \"The Great Indian Middle Class - Low-Cost Version\"\n" +
            "          },\n" +
            "          \"stats\": {\n" +
            "            \"indexValue\": 160.5917052243863\n" +
            "          },\n" +
            "          \"initialIndex\": 161.25106381333097,\n" +
            "          \"scid\": \"SCNM_0011\"\n" +
            "        }\n" +
            "      ],\n" +
            "      \"publisher\": {\n" +
            "        \"publisherId\": \"economictimes.indiatimes.com\",\n" +
            "        \"name\": \"Economic Times\",\n" +
            "        \"logo\": \"https://www.smallcase.com/images/newslogos/TheEconomicTimes.png\"\n" +
            "      },\n" +
            "      \"tags\": [\n" +
            "        \"gimc nm_10\",\n" +
            "        \"gimc nm_11\"\n" +
            "      ],\n" +
            "      \"__v\": 0,\n" +
            "      \"flags\": {\n" +
            "        \"featured\": false\n" +
            "      }\n" +
            "    }]";

    public static final String INVESTED_SMALLCASE_DETAIL = "{\n" +
            "    \"_id\": \"584a31aff67ce42096d337d4\",\n" +
            "    \"name\": \"Bargain Buys\",\n" +
            "    \"source\": \"PROFESSIONAL\",\n" +
            "    \"scid\": \"SCMO_0002\",\n" +
            "    \"status\": \"VALID\",\n" +
            "    \"version\": 3,\n" +
            "    \"failedBatch\": {\n" +
            "      \"sellAmount\": 0,\n" +
            "      \"buyAmount\": 0\n" +
            "    },\n" +
            "    \"stats\": {\n" +
            "      \"indexValue\": 107.94205583863226,\n" +
            "      \"lastCloseIndex\": 106.71453236198258,\n" +
            "      \"pe\": 0,\n" +
            "      \"lastWeekCloseIndex\": 106.71453236198258,\n" +
            "      \"lastToLastWeekCloseIndex\": 104.42161115088224\n" +
            "    },\n" +
            "    \"returns\": {\n" +
            "      \"divReturns\": 0,\n" +
            "      \"otherReturns\": 0,\n" +
            "      \"realizedReturns\": 0,\n" +
            "      \"realizedInvestment\": 0,\n" +
            "      \"unrealizedInvestment\": 2374.7,\n" +
            "      \"networth\": 2563.3\n" +
            "    },\n" +
            "    \"currentConfig\": {\n" +
            "      \"segments\": [\n" +
            "        {\n" +
            "          \"label\": \"Real Estate\",\n" +
            "          \"constituents\": [\n" +
            "            \"SUNT\"\n" +
            "          ]\n" +
            "        },\n" +
            "      ],\n" +
            "      \"constituents\": [\n" +
            "        {\n" +
            "          \"sid\": \"SUNT\",\n" +
            "          \"shares\": 3,\n" +
            "          \"averagePrice\": 199,\n" +
            "          \"sidInfo\": {\n" +
            "            \"name\": \"Sunteck Realty Ltd\",\n" +
            "            \"ticker\": \"SUNTECK\",\n" +
            "            \"exchange\": \"NSE\",\n" +
            "            \"sector\": \"Real Estate \",\n" +
            "            \"description\": \"Sunteck Realty Limited is engaged in the business of designing, developing and managing residential and commercial properties.\",\n" +
            "            \"nseSeries\": \"EQ\"\n" +
            "          }\n" +
            "        },\n" +
            "        {\n" +
            "          \"sid\": \"SJVN\",\n" +
            "          \"shares\": 20,\n" +
            "          \"averagePrice\": 32,\n" +
            "          \"sidInfo\": {\n" +
            "            \"name\": \"SJVN Ltd\",\n" +
            "            \"ticker\": \"SJVN\",\n" +
            "            \"exchange\": \"NSE\",\n" +
            "            \"sector\": \"Utilities\",\n" +
            "            \"description\": \"SJVN Limited is primarily engaged in the business of generation and sale of power. The Company operates through Thermal Power segment.\",\n" +
            "            \"nseSeries\": \"EQ\"\n" +
            "          }\n" +
            "        },\n" +
            "        {\n" +
            "          \"sid\": \"NITT\",\n" +
            "          \"shares\": 1,\n" +
            "          \"averagePrice\": 437.8,\n" +
            "          \"sidInfo\": {\n" +
            "            \"name\": \"NIIT Technologies Ltd\",\n" +
            "            \"ticker\": \"NIITTECH\",\n" +
            "            \"exchange\": \"NSE\",\n" +
            "            \"sector\": \"Software \",\n" +
            "            \"description\": \"NIIT Technologies Limited is an information technology (IT) solutions company. The Company is engaged in providing computer programming consultancy and related activities.\",\n" +
            "            \"nseSeries\": \"EQ\"\n" +
            "          }\n" +
            "        },\n" +
            "        {\n" +
            "          \"sid\": \"GALK\",\n" +
            "          \"shares\": 2,\n" +
            "          \"averagePrice\": 349.95,\n" +
            "          \"sidInfo\": {\n" +
            "            \"name\": \"Gujarat Alkalies And Chemicals Ltd\",\n" +
            "            \"ticker\": \"GUJALKALI\",\n" +
            "            \"exchange\": \"NSE\",\n" +
            "            \"sector\": \"Chemicals\",\n" +
            "            \"description\": \"Gujarat Alkalies and Chemicals Limited is a Caustic Soda manufacturing company. The Company produces caustic soda in India with installed production capacity of 4,29,050 MT of Caustic Soda.\",\n" +
            "            \"nseSeries\": \"EQ\"\n" +
            "          }\n" +
            "        }\n" +
            "      ]\n" +
            "    },\n" +
            "    \"date\": \"2016-12-09T04:23:11.438Z\",\n" +
            "    \"dateModified\": \"2016-12-09T04:21:12.818Z\"\n" +
            "  }";

    public static final String STOCK_PRICE = "{\n" +
            "  \"success\": true,\n" +
            "  \"errors\": null,\n" +
            "  \"data\": {\n" +
            "    \"SUNT\": 248.3,\n" +
            "  }\n" +
            "}";
}
