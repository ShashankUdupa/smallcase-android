package com.smallcase.android.smallcase.discover;

import com.smallcase.android.R;
import com.smallcase.android.util.NetworkHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by shashankm on 09/02/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class DiscoverPresenterTest {
    @Mock private DiscoverContract.Service serviceContract;
    @Mock private DiscoverContract.View viewContract;
    @Mock private List<String> investedScids;
    @Mock private NetworkHelper networkHelper;
    private DiscoverContract.Presenter discoverPresenter;

    @Before
    public void setUp() throws Exception {
        discoverPresenter = new DiscoverPresenter(serviceContract, viewContract, networkHelper);
    }

    @Test
    public void showTryAgainWhenCouldNotFetchAnyDiscoverItemsInFeaturedSmallcase() throws Exception {
        when(serviceContract.fetchedPopularSmallcases()).thenReturn(false);
        when(serviceContract.fetchedCollection()).thenReturn(false);
        when(serviceContract.fetchedNews()).thenReturn(false);

        discoverPresenter.onFailedFetchingFeaturedSmallcases();

        verify(viewContract).showTryAgain();
    }

    @Test
    public void showSnackBarWhenCouldNotFetchFeaturedSmallcase() throws Exception {
        when(serviceContract.fetchedPopularSmallcases()).thenReturn(true);
        when(serviceContract.fetchedCollection()).thenReturn(false);
        when(serviceContract.fetchedNews()).thenReturn(false);

        discoverPresenter.onFailedFetchingFeaturedSmallcases();

        verify(viewContract).showSnackBar(R.string.something_wrong);
    }
}