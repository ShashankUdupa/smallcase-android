package com.smallcase.android.onboarding;

import com.smallcase.android.R;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.util.NetworkHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by shashankm on 09/11/16.
 */

@RunWith(MockitoJUnitRunner.class)
public class LandingPresenterTest {
    private LandingPresenter landingPresenter;
    @Mock private LandingContract.View landingContract;
    @Mock private KiteRepository kiteRepository;
    @Mock private NetworkHelper networkHelper;
    @Mock private SharedPrefService sharedPrefService;

    @Before
    public void setUp() throws Exception {
        landingPresenter = new LandingPresenter(landingContract, networkHelper, sharedPrefService, kiteRepository);
    }

    @Test
    public void showErrorWhenNameIsInvalid() throws Exception {
        when(landingContract.getName()).thenReturn("Shashank1");
        landingPresenter.createAccount();
        verify(landingContract).showDataValidationError(R.string.invalid_name);
    }

    @Test
    public void showErrorWhenNameContainsSpecialCharacters() throws Exception {
        when(landingContract.getName()).thenReturn("Shashank-M");
        landingPresenter.createAccount();
        verify(landingContract).showDataValidationError(R.string.invalid_name);
    }

    @Test
    public void showErrorWhenEmailIsInvalid() throws Exception {
        when(landingContract.getName()).thenReturn("Shashank");
        when(landingContract.getEmail()).thenReturn("shashank@.com");
        landingPresenter.createAccount();
        verify(landingContract).showDataValidationError(R.string.invalid_email);
    }

    @Test
    public void showErrorWhenNumberContainsNonDigits() throws Exception {
        when(landingContract.getName()).thenReturn("Shashank");
        when(landingContract.getEmail()).thenReturn("shashank@ymail.com");
        when(landingContract.getNumber()).thenReturn("+919538335596");
        landingPresenter.createAccount();
        verify(landingContract).showDataValidationError(R.string.invalid_number);
    }
}