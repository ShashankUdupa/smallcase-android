package com.smallcase.android.onboarding;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by shashankm on 09/11/16.
 */
public class LoginPresenterTest {
    private LoginPresenter mLoginPresenter;
    private static final String REDIRECT_URL = "https://www.smallcase.com/?" +
            "status=success&request_token=8ddzioaf366d0twdumv6p8zzzej4iukx#/login/kite";

    @Before
    public void setUp() throws Exception {
        mLoginPresenter = new LoginPresenter();
    }

    @Test
    public void checkTokenParser() throws Exception {
        String token = mLoginPresenter.extractToken(REDIRECT_URL);
        assertEquals("8ddzioaf366d0twdumv6p8zzzej4iukx", token);
    }

    @Test
    public void checkResponseOnSmallCaseUrl() throws Exception {
        boolean response = mLoginPresenter.isSmallCaseUrl(REDIRECT_URL);
        assertEquals(true, response);
    }

    @Test
    public void checkResponseOnNonSmallCaseUrl() throws Exception {
        boolean response = mLoginPresenter.isSmallCaseUrl("https://kite.com");
        assertEquals(false, response);
    }
}