package com.smallcase.android.user.dashboard;


import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.smallcase.android.api.TestData;
import com.smallcase.android.data.model.InvestedSmallcases;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.repository.InvestmentRepository;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.util.InvestmentHelper;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.model.PInvestedSmallcaseWithNews;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.schedulers.Schedulers;

import static org.junit.Assert.assertEquals;

/**
 * Created by shashankm on 12/12/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class DashboardPresenterTest {
    @Mock private MarketRepository marketRepository;
    @Mock private InvestmentRepository investmentRepository;
    @Mock private DashboardContract.Service dashboardService;
    @Mock private DashboardContract.View dashboardContract;
    @Mock private SharedPrefService sharedPrefService;
    @Mock private NetworkHelper networkHelper;
    @Mock private SmallcaseRepository smallcaseRepository;
    @Mock private UserRepository userRepository;
    @Mock private Activity activity;
    private DashboardPresenter dashboardPresenter;

    @Before
    public void setUp() throws Exception {
        InvestmentHelper investmentHelper = new InvestmentHelper();
        dashboardPresenter = new DashboardPresenter(activity, marketRepository, investmentRepository, dashboardContract,
                sharedPrefService, networkHelper, smallcaseRepository, investmentHelper, userRepository);

        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
            @Override
            public Scheduler getMainThreadScheduler() {
                return Schedulers.immediate();
            }
        });
    }

    @Test
    public void checkInvestedSmallcaseNewsViewModel() throws Exception {
        Gson gson = new Gson();

        Type listType = new TypeToken<List<News>>(){}.getType();
        List<News> newsList = gson.fromJson(TestData.INVESTED_NEWS_JSON_ARRAY, listType);
        InvestedSmallcases investedSmallcases = gson.fromJson(TestData.INVESTED_SMALLCASE_JSON, InvestedSmallcases.class);
        HashMap<String, InvestedSmallcases> investedSmallcasesHashMap = new HashMap<>();
        investedSmallcasesHashMap.put(investedSmallcases.getScid(), investedSmallcases);

        List<PInvestedSmallcaseWithNews> investedSmallcaseWithNewses = dashboardPresenter.createInvestedSmallcaseWithNewsViewModel
                (newsList, investedSmallcasesHashMap);
        PInvestedSmallcaseWithNews investedSmallcaseWithNews = investedSmallcaseWithNewses.get(0);

        assertEquals(2, investedSmallcaseWithNewses.size());
        assertEquals("-0.15%", investedSmallcaseWithNews.getInvestedSmallcase().get(0).getPercentage());
        assertEquals(false, investedSmallcaseWithNews.getInvestedSmallcase().get(0).isIndexUp());
    }

    @After
    public void tearDown() throws Exception {
        RxAndroidPlugins.getInstance().reset();
    }
}