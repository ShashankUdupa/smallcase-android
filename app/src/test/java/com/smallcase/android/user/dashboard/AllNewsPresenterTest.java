package com.smallcase.android.user.dashboard;


import com.smallcase.android.R;
import com.smallcase.android.data.model.InvestedSmallcases;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.news.AllNewsContract;
import com.smallcase.android.news.AllNewsPresenter;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.model.PInvestedSmallcaseWithNews;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by shashankm on 12/12/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class AllNewsPresenterTest {
    @Mock private DashboardPresenter dashboardPresenter;
    @Mock private NetworkHelper networkHelper;
    @Mock private SmallcaseRepository smallcaseRepository;
    @Mock private AllNewsContract.Service allNewsServiceContract;
    @Mock private AllNewsContract.View allNewsContract;
    @Mock private SharedPrefService sharedPrefService;
    @Mock private List<String> scids;
    private AllNewsContract.Presenter allNewsPresenter;

    @Before
    public void setUp() throws Exception {
        allNewsPresenter = new AllNewsPresenter(null, dashboardPresenter, networkHelper, smallcaseRepository,
                sharedPrefService, allNewsContract, scids);
    }

    @Test
    public void checkResponseWhenNoNet() throws Exception {
        when(networkHelper.isNetworkAvailable()).thenReturn(false);

        allNewsPresenter.fetchMoreInvestedSmallcaseNews(5, 10, AppConstants.NEWS_TYPE_INVESTED);

        verify(allNewsContract).showSnackBar(R.string.no_internet);
    }

    @Test
    public void checkResponseWhenNoNewsArePresent() throws Exception {
        when(dashboardPresenter.createInvestedSmallcaseWithNewsViewModel(new ArrayList<News>(),
                new HashMap<String, InvestedSmallcases>())).thenReturn(Collections.<PInvestedSmallcaseWithNews>emptyList());

        allNewsPresenter.onNewsFetchedSuccessfully(Collections.<News>emptyList());

        verify(allNewsContract).noMoreNews();
    }


}