package com.smallcase.android.onboarding;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.smallcase.android.R;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.view.adapter.OnBoardingPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;

public class OnBoardingActivity extends AppCompatActivity {
    @BindView(R.id.onboarding_view_pager) ViewPager viewPager;
    @BindView(R.id.indicator) CircleIndicator indicator;
    @BindView(R.id.login_to_account) View loginToAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);

        ButterKnife.bind(this);

        OnBoardingPagerAdapter onBoardingPagerAdapter = new OnBoardingPagerAdapter(this);
        viewPager.setAdapter(onBoardingPagerAdapter);
        indicator.setViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 3) {
                    indicator.setVisibility(View.GONE);
                    loginToAccount.setVisibility(View.VISIBLE);
                } else {
                    indicator.setVisibility(View.VISIBLE);
                    loginToAccount.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @OnClick(R.id.login_to_account)
    public void loginToAccount() {
        new SharedPrefService(this).userViewedOnBoardTutorial();
        startActivity(new Intent(this, LandingActivity.class));
        overridePendingTransition(0, 0);
        finish();
    }
}
