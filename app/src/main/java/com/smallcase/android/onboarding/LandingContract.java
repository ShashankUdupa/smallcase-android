package com.smallcase.android.onboarding;

import android.text.SpannableString;

import com.smallcase.android.user.UserService;

import java.util.HashMap;

import okhttp3.ResponseBody;

/**
 * Created by shashankm on 05/12/16.
 */

public interface LandingContract {
    interface View {
        String getEmail();

        String getName();

        String getNumber();

        String getHeardFrom();

        void showDataValidationError(int errorResId);

        void showSnackBar(int errorResId);

        void authenticationSuccessful();

        void requestedForAccount(String redirectUrl);

        void failedToRequestAccount(SpannableString spannableString);

        void reShowLogin();

        void unexpectedEntry();

        void existingClientError(SpannableString alreadyExistingClientSpan);
    }

    interface Presenter {
        void getUpdateStatus(int versionCode, final UserService.UpdateStatusCallBack updateStatusCallBack);

        void requestAuthToken(String requestToken, String broker);

        void createAccount();

        void onSignUpRequestReceived(ResponseBody responseBody);

        void onSignUpFailed(Throwable throwable);

        void destroySubscriptions();
    }

    interface Service {
        void getUpdateStatus(int versionCode, final UserService.UpdateStatusCallBack updateStatusCallBack);

        void signUpUser(HashMap<String, Object> body);

        void destroySubscriptions();
    }
}
