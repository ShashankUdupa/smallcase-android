package com.smallcase.android.onboarding;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.user.UserService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import io.sentry.event.Breadcrumb;
import retrofit2.adapter.rxjava.HttpException;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 17/01/17.
 */

class LandingService implements LandingContract.Service {
    private static final String TAG = "LandingService";

    private LandingContract.Presenter landingPresenter;
    private KiteRepository kiteRepository;
    private CompositeSubscription compositeSubscription;
    private UserRepository userRepository;

    LandingService(KiteRepository kiteRepository, LandingContract.Presenter landingPresenter, UserRepository userRepository) {
        this.kiteRepository = kiteRepository;
        this.landingPresenter = landingPresenter;
        this.userRepository = userRepository;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getUpdateStatus(int versionCode, final UserService.UpdateStatusCallBack updateStatusCallBack) {
        userRepository.getUpdateStatus("smallcase", versionCode, "android")
                .subscribe(responseBody -> {
                    try {
                        JSONObject response = new JSONObject(responseBody.string());
                        updateStatusCallBack.onUpdateStatusReceived(response.getString("data"));
                    } catch (JSONException | IOException e) {
                        updateStatusCallBack.onFailedFetchingUpdateStatus();
                        e.printStackTrace();
                        SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; getUpdateStatus()");
                    }
                }, throwable -> {
                    updateStatusCallBack.onFailedFetchingUpdateStatus();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(null, throwable, TAG + "; getUpdateStatus()");
                });
    }

    @Override
    public void signUpUser(final HashMap<String, Object> body) {
        compositeSubscription.add(kiteRepository.signUpUser(body).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(responseBody -> landingPresenter.onSignUpRequestReceived(responseBody), throwable -> {
                    HashMap<String, String> data = new HashMap<>();
                    data.put("email", (String) body.get("email"));
                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed to sign up", data);
                    SentryAnalytics.getInstance().captureEvent(null, throwable, TAG + "; signUpUser()");

                    landingPresenter.onSignUpFailed(throwable);
                }));
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }
}
