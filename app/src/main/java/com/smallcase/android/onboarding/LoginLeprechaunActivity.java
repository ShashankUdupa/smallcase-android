package com.smallcase.android.onboarding;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.smallcase.android.R;
import com.smallcase.android.data.repository.AuthRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.smallcase.LoginRequest;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.user.ContainerActivity;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Code is shit but this is for internal purpose :/
 */
public class LoginLeprechaunActivity extends AppCompatActivity implements SmallcaseHelper.LoginResponse {
    @BindView(R.id.leprechaun_id) EditText leprechaunId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_leprechaun);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.login)
    public void loginLeprechaun() {
        HashMap<String, String> body = new HashMap<>();
        body.put("broker", "kite-leprechaun");
        body.put("reqToken", leprechaunId.getText().toString());
        body.put("app", "platform");

        LoginRequest loginRequest = new SmallcaseHelper(new AuthRepository(), this, new SharedPrefService(this));
        loginRequest.getJwtToken(body);
    }

    @Override
    public void onJwtSavedSuccessfully() {
        startActivity(new Intent(this, ContainerActivity.class));
        finish();
    }

    @Override
    public void onFailedFetchingJwt() {
        new AlertDialog.Builder(this)
                .setTitle("Error!")
                .setMessage("Something went wrong")
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {

                }).create().show();
    }

    @Override
    public void onDifferentUserLoggedIn() {
        // No way this method would get called
    }
}
