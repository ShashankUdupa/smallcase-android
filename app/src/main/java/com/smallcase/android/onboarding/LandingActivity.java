package com.smallcase.android.onboarding;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.repository.AuthRepository;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.news.WebActivity;
import com.smallcase.android.user.ContainerActivity;
import com.smallcase.android.user.UserService;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.UpdateStatus;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.android.SpinnerAdapter;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

import static com.smallcase.android.onboarding.LoginActivity.REQUEST_TOKEN_KEY;

public class LandingActivity extends AppCompatActivity implements LandingContract.View, UserService.UpdateStatusCallBack {
  /**
   * Code to identify response from login activity.
   */
  private static final int LOGIN_REQUEST_CODE = 111;

  private static final long SLIDE_DURATION = 250;

  private static final AccelerateDecelerateInterpolator ACCELERATE_DECELERATE_INTERPOLATOR = new AccelerateDecelerateInterpolator();

  private static final String TAG = LandingActivity.class.getSimpleName();

  /**
   * Visible height of screen to determine if keyboard is visible or not.
   */
  private static int PREVIOUS_HEIGHT = -1;

  @BindView(R.id.layout_sign_up)
  View signUpLayout;
  @BindView(R.id.image_small_case)
  View smallcaseLogo;
  @BindView(R.id.card_login)
  View cardLogin;
  @BindView(R.id.activity_landing)
  View rootLayout;
  @BindView(R.id.text_name)
  EditText name;
  @BindView(R.id.text_email)
  EditText email;
  @BindView(R.id.text_phone)
  EditText phone;
  @BindView(R.id.spinner_referral)
  Spinner referral;
  @BindView(R.id.sign_up_loading)
  ProgressBar signUpLoading;
  @BindView(R.id.sign_up_message)
  GraphikText signUpMessage;
  @BindView(R.id.text_small_case_sign_up)
  GraphikText textSmallCase;
  @BindView(R.id.text_sign_up_layout)
  View signUpText;
  @BindView(R.id.sign_up_container)
  View signUpContainer;
  @BindView(R.id.create_account)
  GraphikText createAccount;
  @BindView(R.id.parent)
  View parentView;
  @BindView(R.id.landing_animation_view)
  ImageView landingAnimationView;
  @BindView(R.id.zerodha_link_card)
  View zerodhaLinkCard;
  @BindView(R.id.application_status_text)
  GraphikText applicationStatusText;
  @BindView(R.id.pop_up_blur)
  View popUpBlur;
  @BindView(R.id.opening_card)
  View openingCard;
  @BindView(R.id.opening_title)
  GraphikText openingTitle;
  @BindView(R.id.opening_description)
  GraphikText openingDescription;
  @BindView(R.id.opening_image)
  ImageView openingImage;
  @BindView(R.id.primary_cta_text)
  GraphikText primaryCta;
  @BindView(R.id.secondary_cta)
  View secondaryCtaCard;
  @BindView(R.id.primary_cta)
  View primaryCtaCard;

  private LandingContract.Presenter landingPresenter;
  private ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener;
  private Animations animations;
  private int clickCount = 0;
  private GestureDetector gestureDetector;
  private AnalyticsContract analyticsContract;
  private Handler handler;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_landing);
    ButterKnife.bind(this);

    //Make status bar transparent and allow background to be drawn below it
    if (Build.VERSION.SDK_INT >= 21) {
      getWindow().setStatusBarColor(Color.TRANSPARENT);
      getWindow().getDecorView().setSystemUiVisibility(
          View.SYSTEM_UI_FLAG_LAYOUT_STABLE
              | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    startAnimation();
    keyBoardChangeListener();

    Glide.with(this)
        .load(R.drawable.landing_illustration)
        .into(landingAnimationView);

    analyticsContract = new AnalyticsManager(getApplicationContext());

    analyticsContract.sendEvent(null, "Viewed Login Screen", Analytics.MIXPANEL);

    landingPresenter = new LandingPresenter(this, new NetworkHelper(this), new
        SharedPrefService(this), new KiteRepository(), new AuthRepository(), new UserRepository());
    animations = new Animations();

    populateSpinner();
    setFonts();
    singUpFormFling();

    email.setOnFocusChangeListener((v, hasFocus) -> {
      if (hasFocus) analyticsContract.sendEvent(null, "Changed Email", Analytics.MIXPANEL);
    });

    name.setOnFocusChangeListener((v, hasFocus) -> {
      if (hasFocus) analyticsContract.sendEvent(null, "Changed Name", Analytics.MIXPANEL);
    });

    phone.setOnFocusChangeListener((v, hasFocus) -> {
      if (hasFocus) analyticsContract.sendEvent(null, "Changed Phone", Analytics.MIXPANEL);
    });

    try {
      PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
      landingPresenter.getUpdateStatus(packageInfo.versionCode, this);
    } catch (PackageManager.NameNotFoundException e) {
      e.printStackTrace();
    }

  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
      //Received request_token from Kite login. Request authToken from server.
      signUpLayout.setVisibility(View.GONE);
      signUpText.setVisibility(View.GONE);
      cardLogin.setVisibility(View.GONE);
      signUpLoading.setVisibility(View.VISIBLE);

      landingPresenter.requestAuthToken(data.getStringExtra(REQUEST_TOKEN_KEY), "kite");
    }
  }

  @Override
  public void onBackPressed() {
    if (signUpLayout.getVisibility() == View.VISIBLE) {
      toggleSignUpForm();
      return;
    }
    super.onBackPressed();
  }

  /**
   * Unregister view tree observer (used for listening keyboard
   * events) to prevent memory leak.
   */
  @Override
  protected void onDestroy() {
    rootLayout.getViewTreeObserver()
        .removeOnGlobalLayoutListener(onGlobalLayoutListener);
    landingPresenter.destroySubscriptions();
    super.onDestroy();
  }

  @OnClick(R.id.card_login)
  public void loginWithKite() {
    startActivityForResult(new Intent(LandingActivity.this, LoginActivity.class),
        LOGIN_REQUEST_CODE);
    overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
  }

  @OnClick(R.id.image_small_case)
  public void secretLogin() {
    if (clickCount == 10) {
      smallcaseLogo.setClickable(false);
      startActivity(new Intent(this, LoginLeprechaunActivity.class));
      finish();
      return;
    }
    clickCount++;
  }

  @OnTouch({R.id.card_login})
  public boolean animateButton(View view, MotionEvent event) {
    animations.itemClick(view, event);
    return true;
  }

  @OnTouch(R.id.sign_up_container)
  public boolean toggleSignUpForm(MotionEvent event) {
    gestureDetector.onTouchEvent(event);
    return true;
  }

  @OnClick(R.id.text_sign_up_layout)
  public void toggleSignUp() {
    toggleSignUpForm();
  }

  @OnClick(R.id.create_account)
  public void createAccount() {
    analyticsContract.sendEvent(null, "Submitted Sign Up Form", Analytics.MIXPANEL);

    cardLogin.setVisibility(View.GONE);
    signUpText.setVisibility(View.GONE);
    signUpLayout.setVisibility(View.GONE);
    signUpLoading.setVisibility(View.VISIBLE);
    landingPresenter.createAccount();
  }

  @Override
  public String getEmail() {
    return email.getText().toString().trim();
  }

  @Override
  public String getName() {
    return name.getText().toString().trim();
  }

  @Override
  public String getNumber() {
    return phone.getText().toString().trim();
  }

  @Override
  public String getHeardFrom() {
    return referral.getSelectedItem().toString();
  }

  @Override
  public void showDataValidationError(int errorResId) {
    signUpText.setVisibility(View.VISIBLE);
    signUpLayout.setVisibility(View.VISIBLE);
    signUpLoading.setVisibility(View.GONE);
    cardLogin.setVisibility(View.VISIBLE);
    errorMessage(errorResId, errorIcon());
  }

  @Override
  public void showSnackBar(int errorResId) {
    if (signUpLoading.getVisibility() == View.VISIBLE) signUpLoading.setVisibility(View.GONE);

    AppUtils.getInstance().showSnackBar(parentView, Snackbar.LENGTH_LONG, errorResId);
    //Reshow sign up form if signUpError.
    if (signUpText.getVisibility() == View.GONE) {
      signUpLayout.setVisibility(View.VISIBLE);
      signUpText.setVisibility(View.VISIBLE);
    }
  }

  @Override
  public void authenticationSuccessful() {
    analyticsContract.sendEvent(null, "Logged In", Analytics.MIXPANEL);
    startActivity(new Intent(this, ContainerActivity.class));
    finish();
  }

  @Override
  public void requestedForAccount(final String redirectUrl) {
    signUpLoading.setVisibility(View.GONE);
    signUpMessage.setVisibility(View.VISIBLE);
    zerodhaLinkCard.setVisibility(View.VISIBLE);
    applicationStatusText.setText("CONTINUE ON ZERODHA");

    String thankYouMessage = "Thank you for your information\n\n";
    SpannableString spannableString = new SpannableString(thankYouMessage + "You can now continue to open up your account on Zerodha by setting up a password.\nDo keep your PAN number handy to get started");
    spannableString.setSpan(new RelativeSizeSpan(0.8f), thankYouMessage.length(), spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

    signUpMessage.setText(spannableString);

    zerodhaLinkCard.setOnClickListener(v -> {
      startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(redirectUrl)));
      overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    });
  }

  @Override
  public void failedToRequestAccount(SpannableString spannableString) {
    signUpLoading.setVisibility(View.GONE);
    signUpMessage.setVisibility(View.VISIBLE);
    zerodhaLinkCard.setVisibility(View.VISIBLE);

    signUpMessage.setText(spannableString);
    applicationStatusText.setText("CHECK APPLICATION STATUS");

    zerodhaLinkCard.setOnClickListener(v -> {
      Intent intent = new Intent(LandingActivity.this, WebActivity.class);
      intent.putExtra(WebActivity.TITLE, "Zerodha");
      intent.putExtra(WebActivity.URL, "https://zerodha.com/account/");
      startActivity(intent);
      overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    });
  }

  @Override
  public void reShowLogin() {
    signUpLayout.setVisibility(View.GONE);
    signUpText.setVisibility(View.VISIBLE);
    cardLogin.setVisibility(View.VISIBLE);
    signUpLoading.setVisibility(View.GONE);
  }

  @Override
  public void unexpectedEntry() {
    signUpLoading.setVisibility(View.GONE);
    zerodhaLinkCard.setVisibility(View.GONE);
    signUpMessage.setVisibility(View.VISIBLE);
    signUpMessage.setText(getString(R.string.something_wrong));
  }

  @Override
  public void existingClientError(SpannableString alreadyExistingClientSpan) {
    signUpLoading.setVisibility(View.GONE);
    signUpMessage.setVisibility(View.VISIBLE);
    signUpMessage.setText(alreadyExistingClientSpan);
    cardLogin.setVisibility(View.VISIBLE);
  }

  /**
   * Set error message and icon on edit text based on the type of error.
   *
   * @param err       type.
   * @param errorIcon to display on edit text.
   */
  private void errorMessage(int err, Drawable errorIcon) {
    errorIcon.setBounds(0, 0, (int) AppUtils.getInstance().dpToPx(24),
        (int) AppUtils.getInstance().dpToPx(24));
    switch (err) {
      case R.string.invalid_name:
        name.setError(getString(err), errorIcon);
        name.requestFocus();
        break;

      case R.string.invalid_email:
        email.setError(getString(err), errorIcon);
        email.requestFocus();
        break;

      case R.string.invalid_number:
        phone.setError(getString(err), errorIcon);
        phone.requestFocus();
        break;
    }
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  private Drawable errorIcon() {
    return Build.VERSION.SDK_INT >= 21 ? getResources().getDrawable
        (R.drawable.android_alert, null) : getResources().getDrawable(R.drawable.android_alert);
  }

  /**
   * Listens for global layout change. Used for hiding and un-hiding views
   * depending on keyboard visibility. Keyboard's visibility is deduced based on the height
   * change of screen.
   */
  private void keyBoardChangeListener() {
    ViewTreeObserver viewTreeObserver = rootLayout.getViewTreeObserver();
    viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
      @Override
      public void onGlobalLayout() {
        Rect r = new Rect();
        onGlobalLayoutListener = this;
        //r will be populated with the coordinates of your view that area still visible.
        rootLayout.getWindowVisibleDisplayFrame(r);

        int heightDiff = rootLayout.getRootView().getHeight() - (r.bottom - r.top);
        if (PREVIOUS_HEIGHT == -1) {
          PREVIOUS_HEIGHT = heightDiff;
        }

        if (heightDiff > (PREVIOUS_HEIGHT + 100)) {
          PREVIOUS_HEIGHT = heightDiff;
          moveSignUpForm(AppUtils.getInstance().dpToPx(-30f));
          toggleViews(View.GONE);
        } else if (heightDiff < (PREVIOUS_HEIGHT - 200)) {
          PREVIOUS_HEIGHT = heightDiff;
          moveSignUpForm(0f);
          toggleViews(View.VISIBLE);
        }
      }
    });
  }

  private void moveSignUpForm(float value) {
    signUpContainer.animate()
        .translationY(value)
        .setDuration(150)
        .start();
  }

  private void toggleViews(int state) {
    cardLogin.setVisibility(state);
    smallcaseLogo.setVisibility(state);
  }

  private void toggleSignUpForm() {
    if (handler == null) {
      handler = new Handler();
    }

    float yOffSet = 0;
    float yOffSetTop = 0;

    float scale = 1f;
    boolean isOpen = signUpLayout.getVisibility() == View.VISIBLE;
    if (!isOpen) {
      yOffSet = AppUtils.getInstance().dpToPx(-10);
      yOffSetTop = AppUtils.getInstance().dpToPx(-45);
      scale = 0.75f;
    }
    slideLoginAndImage(yOffSet, yOffSetTop, scale);
    if (isOpen) {
      animations.collapse(signUpLayout, ACCELERATE_DECELERATE_INTERPOLATOR, 250);
      handler.postDelayed(() -> landingAnimationView.setVisibility(View.VISIBLE), 250);
    } else {
      analyticsContract.sendEvent(null, "Clicked SignUp", Analytics.MIXPANEL);
      landingAnimationView.setVisibility(View.INVISIBLE);
      animations.expand(signUpLayout, ACCELERATE_DECELERATE_INTERPOLATOR, 200);
    }
  }

  private void slideLoginAndImage(float yOffSet, float yOffSetTop, float scale) {
    cardLogin.animate()
        .translationY(yOffSet)
        .setInterpolator(ACCELERATE_DECELERATE_INTERPOLATOR)
        .setDuration(SLIDE_DURATION)
        .start();
    smallcaseLogo.animate()
        .translationY(yOffSetTop)
        .scaleX(scale)
        .scaleY(scale)
        .setInterpolator(ACCELERATE_DECELERATE_INTERPOLATOR)
        .setDuration(SLIDE_DURATION)
        .start();
  }

  private void singUpFormFling() {
    gestureDetector = new GestureDetector(this, new GestureDetector.OnGestureListener() {
      @Override
      public boolean onDown(MotionEvent e) {
        return false;
      }

      @Override
      public void onShowPress(MotionEvent e) {

      }

      @Override
      public boolean onSingleTapUp(MotionEvent e) {
        return false;
      }

      @Override
      public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
      }

      @Override
      public void onLongPress(MotionEvent e) {

      }

      @Override
      public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (e1.getY() < e2.getY() && signUpLayout.getVisibility() == View.VISIBLE) {
          toggleSignUpForm();
        }

        if (e1.getY() > e2.getY() && signUpLayout.getVisibility() == View.GONE) {
          toggleSignUpForm();
        }

        return true;
      }
    });
  }

  private void startAnimation() {
    Animation slideUpFast = AnimationUtils.loadAnimation(this, R.anim.slide_up);
    slideUpFast.setInterpolator(new OvershootInterpolator());
    final Animation scaleIn = AnimationUtils.loadAnimation(this, R.anim.scale_in);
    scaleIn.setInterpolator(new OvershootInterpolator(1f));
    smallcaseLogo.startAnimation(slideUpFast);
    new Handler().postDelayed(() -> {
      cardLogin.setVisibility(View.VISIBLE);
      signUpContainer.setVisibility(View.VISIBLE);
      landingAnimationView.setVisibility(View.VISIBLE);

      cardLogin.startAnimation(scaleIn);
      signUpContainer.startAnimation(scaleIn);
    }, 600);
  }

  private void setFonts() {
    name.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR));
    email.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR));
    phone.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR));
  }

  private void populateSpinner() {
    SpinnerAdapter optionsAdapter = new SpinnerAdapter(this,
        R.layout.heard_from_drop_down, Arrays.asList(getResources().getStringArray
        (R.array.heard_from_options)));

    referral.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        GraphikText selectedText = (GraphikText) view;
        if (null == selectedText) return;
        selectedText.setTextColor(ContextCompat.getColor(LandingActivity.this, R.color.primary_text));
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    referral.setAdapter(optionsAdapter);
  }

  @Override
  public void onUpdateStatusReceived(String updateStatus) {
    if (UpdateStatus.FORCE_UPDATE.equals(updateStatus)) {
      openingCard.setVisibility(View.VISIBLE);
      Glide.with(this)
          .load(R.drawable.old_android)
          .into(openingImage);
      openingTitle.setText(R.string.app_old);
      openingDescription.setText(R.string.no_longer_supported);
      primaryCta.setText(R.string.update_now_caps);
      secondaryCtaCard.setVisibility(View.GONE);
      popUpBlur.setVisibility(View.VISIBLE);
      popUpBlur.setClickable(true);

      primaryCtaCard.setOnClickListener(v -> {
        try {
          startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
        } catch (android.content.ActivityNotFoundException anfe) {
          startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
        }
      });
    } else {
      openingCard.setVisibility(View.GONE);
      popUpBlur.setVisibility(View.GONE);
    }

  }

  @Override
  public void onFailedFetchingUpdateStatus() {

  }
}
