package com.smallcase.android.onboarding;

/**
 * Created by shashankm on 07/11/16.
 */

class LoginPresenter {
  /**
   * Extract request_token from url on successful login.
   *
   * @param url from which request_token needs to be extracted.
   * @return request_token for the user.
   */
  String extractToken(String url) {
    int start = url.indexOf("request_token");
    start = url.indexOf('=', start) + 1;
    int end = url.indexOf('#');
    return url.substring(start, end);
  }

  /**
   * Check if the url is redirected to smallcase.
   *
   * @param url that needs to be checked.
   * @return true if url contains the string 'smallcase'.
   */
  boolean isSmallCaseUrl(String url) {
    return url.contains("smallcase");
  }
}
