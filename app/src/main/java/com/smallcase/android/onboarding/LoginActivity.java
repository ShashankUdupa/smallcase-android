package com.smallcase.android.onboarding;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.smallcase.android.BuildConfig;
import com.smallcase.android.R;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.GraphikText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {
    /**
     * Key for getting login token
     */
    public static final String REQUEST_TOKEN_KEY = "login_key";

    @BindView(R.id.web_login)
    WebView webLogin;
    @BindView(R.id.loading_indicator)
    View loadingIndicator;
    @BindView(R.id.text_tool_bar_title)
    GraphikText toolBarTitle;
    @BindView(R.id.tool_bar)
    Toolbar toolbar;
    @BindView(R.id.back)
    ImageView backIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setTitle("");
        }
        toolBarTitle.setText(R.string.login_with_kite);
        backIcon.setImageResource(R.drawable.ic_cancel);
        int padding = (int) AppUtils.getInstance().dpToPx(16);
        backIcon.setPadding(padding, padding, padding, padding);

        setUpWebView();
    }

    /**
     * Indicate that the user did not login by setting RESULT_CANCELED.
     */
    @Override
    public void onBackPressed() {
        // When the user hits the back button set the resultCode
        // to RESULT_CANCELED to indicate a failure
        setResult(RESULT_CANCELED);
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in_with_scale, R.anim.top_to_down);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @OnClick(R.id.back)
    void back() {
        onBackPressed();
    }

    private void setUpWebView() {
        webLogin.getSettings().setAppCacheEnabled(false);
        webLogin.setWebViewClient(new WebClient());
        webLogin.getSettings().setJavaScriptEnabled(true);

        //Login Url for kite with SmallCase api_key
        final String KITE_LOGIN_URL = "https://kite.zerodha.com/connect/login?api_key=" + BuildConfig.KITE_TOKEN;
        webLogin.loadUrl(KITE_LOGIN_URL);
    }

    private class WebClient extends WebViewClient {
        private final String TAG = WebViewClient.class.getSimpleName();
        LoginPresenter loginPresenter = new LoginPresenter();

        /**
         * Check if the url is smallcase's. If yes, extract request_token
         * from the url params and kill web view.
         *
         * @param view web view in question.
         * @param url  url of page.
         * @return true if we handle the url, false otherwise.
         */
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (loginPresenter.isSmallCaseUrl(url)) {
                Intent intent = new Intent();
                intent.putExtra(REQUEST_TOKEN_KEY, loginPresenter.extractToken(url));
                setResult(RESULT_OK, intent);
                finish();
                return true;
            }
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            //Hide loader if visible
            if (loadingIndicator.getVisibility() == View.VISIBLE) {
                webLogin.setVisibility(View.VISIBLE);
                loadingIndicator.setVisibility(View.GONE);
            }
            super.onPageFinished(view, url);
        }
    }
}
