package com.smallcase.android.onboarding;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;

import com.smallcase.android.R;
import com.smallcase.android.data.repository.AuthRepository;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.smallcase.LoginRequest;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.user.UserService;
import com.smallcase.android.util.NetworkHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by shashankm on 08/11/16.
 */
class LandingPresenter implements LandingContract.Presenter, SmallcaseHelper.LoginResponse {
    private static final String TAG = LandingPresenter.class.getSimpleName();
    private LandingContract.View landingContract;
    private LandingContract.Service landingService;
    private NetworkHelper networkHelper;
    private LoginRequest loginRequest;

    LandingPresenter(LandingContract.View landingContract, NetworkHelper networkHelper, SharedPrefService
                     sharedPrefService, KiteRepository kiteRepository, AuthRepository authRepository, UserRepository userRepository) {
        this.landingContract = landingContract;
        this.networkHelper = networkHelper;
        this.loginRequest = new SmallcaseHelper(authRepository, this, sharedPrefService);
        this.landingService = new LandingService(kiteRepository, this, userRepository);
    }

    @Override
    public void getUpdateStatus(int versionCode, UserService.UpdateStatusCallBack updateStatusCallBack) {
        landingService.getUpdateStatus(versionCode, updateStatusCallBack);
    }

    @Override
    public void requestAuthToken(String requestToken, String broker) {
        HashMap<String, String> body = new HashMap<>();
        body.put("broker", broker);
        body.put("reqToken", requestToken);
        body.put("app", "platform");

        if (!networkHelper.isNetworkAvailable()) {
            landingContract.showSnackBar(R.string.no_internet);
            return;
        }

        loginRequest.getJwtToken(body);
    }

    /**
     * Perform validation of fields and throw appropriate error if any
     * fails. If not try to sign user up.
     */
    @Override
    public void createAccount() {
        if (isInvalidName()) {
            landingContract.showDataValidationError(R.string.invalid_name);
            return;
        }

        if (isInvalidEmail()) {
            landingContract.showDataValidationError(R.string.invalid_email);
            return;
        }

        if (isInValidNumber()) {
            landingContract.showDataValidationError(R.string.invalid_number);
            return;
        }

        HashMap<String, Object> body = new HashMap<>();
        body.put("name", landingContract.getName());
        body.put("phone", landingContract.getNumber());
        body.put("email", landingContract.getEmail());
        body.put("channel", landingContract.getHeardFrom());

        if (!networkHelper.isNetworkAvailable()) {
            landingContract.showSnackBar(R.string.no_internet);
            return;
        }

        landingService.signUpUser(body);
    }

    @Override
    public void onSignUpRequestReceived(ResponseBody responseBody) {
        handleAccountRequestResponseStatus(extractAccountRequest(responseBody));
    }

    @Override
    public void onSignUpFailed(Throwable throwable) {
        if (throwable instanceof HttpException) {
            handleAccountRequestResponseStatus(extractAccountRequest(((HttpException) throwable)
                    .response().errorBody()));
            return;
        }

        landingContract.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void destroySubscriptions() {
        landingService.destroySubscriptions();
    }

    private void handleAccountRequestResponseStatus(Map<String, String> data) {
        switch (data.get("status")) {
            case "DONE":
                landingContract.requestedForAccount(data.get("redirect"));
                break;

            case "EXISTINGCLIENT":
                landingContract.existingClientError(createAlreadyExistingClientSpan());
                break;

            case "ALREADYREGISTERED":
            case "DUPLICATEPRIORITY":
            case "DUPLICATELEAD":
                landingContract.failedToRequestAccount(createDuplicateEntrySpan());
                break;

            default:
                landingContract.unexpectedEntry();
                break;
        }
    }

    private SpannableString createAlreadyExistingClientSpan() {
        String alreadyHaveAccount = "Looks like you already have a Zerodha account";
        SpannableString spannableString = new SpannableString(alreadyHaveAccount + "\n\nYou can log in to smallcase with your Zerodha Kite credentials below");
        spannableString.setSpan(new RelativeSizeSpan(0.8f), alreadyHaveAccount.length(), spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    private SpannableString createDuplicateEntrySpan() {
        String alreadyHaveAccount = "Looks like you have already signed up for a Zerodha account earlier.";
        SpannableString spannableString = new SpannableString(alreadyHaveAccount + "\n\nIn case you don\'t remember your password to check status, click the \'Forgot password\' link on the Application Status page to receive the same in your email");
        spannableString.setSpan(new RelativeSizeSpan(0.8f), alreadyHaveAccount.length(), spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    private boolean isInValidNumber() {
        if (landingContract.getNumber() == null && landingContract.getNumber().isEmpty()) {
            return true;
        }

        if (landingContract.getNumber().length() != 10) return true;

        for (char c : landingContract.getNumber().toCharArray()) {
            if (!Character.isDigit(c)) {
                return true;
            }
        }

        return false;
    }

    private boolean isInvalidEmail() {
        boolean isValid = true;

        String emailValidationRegex = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = landingContract.getEmail();

        Pattern pattern = Pattern.compile(emailValidationRegex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = false;
        }
        return isValid;
    }

    private boolean isInvalidName() {
        String nameValidationRegex = "^[a-zA-Z\" \"]+$";
        CharSequence inputStr = landingContract.getName();

        Pattern pattern = Pattern.compile(nameValidationRegex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        return !matcher.matches();
    }

    private Map<String, String> extractAccountRequest(ResponseBody responseBody) {
        try {
            if (responseBody == null) return Collections.<String, String>emptyMap();

            JSONObject errorObj = new JSONObject(responseBody.string());
            JSONObject data = errorObj.getJSONObject("data");
            HashMap<String, String> dataMap = new HashMap<>();
            dataMap.put("status", data.getString("status"));
            if (data.has("redirect")) dataMap.put("redirect", data.getString("redirect"));
            return dataMap;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return Collections.emptyMap();
    }

    @Override
    public void onJwtSavedSuccessfully() {
        landingContract.authenticationSuccessful();
    }

    @Override
    public void onFailedFetchingJwt() {
        landingContract.showSnackBar(R.string.something_wrong);
        landingContract.reShowLogin();
    }

    @Override
    public void onDifferentUserLoggedIn() {
        // Doesn't make sense here, this method would ideally never get called in this class
    }
}
