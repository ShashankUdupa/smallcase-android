package com.smallcase.android.view.buy;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.data.repository.AuthRepository;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.smallcase.LoginRequest;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.user.ContainerActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.Stock;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by shashankm on 26/03/17.
 */

public class InvestAmountPopUp implements InvestContract.View, InvestContract.Authenticate, SmallcaseHelper.LoginResponse {
    private static final String TAG = InvestAmountPopUp.class.getSimpleName();

    @BindView(R.id.invest_amount_card)
    CardView investAmountCard;
    @BindView(R.id.amount_to_invest)
    EditText amountToInvest;
    @BindView(R.id.amount_divider)
    View amountDivider;
    @BindView(R.id.min_investment_amount)
    GraphikText minInvestmentAmount;
    @BindView(R.id.min_amount_container)
    View minAmountContainer;
    @BindView(R.id.loading_min_investment)
    View loadingMinInvestment;
    @BindView(R.id.more_funds_needed)
    GraphikText moreFundsNeeded;
    @BindView(R.id.confirm_amount_text)
    GraphikText confirmAmountText;
    @BindView(R.id.confirm_amount)
    CardView confirmAmount;
    @BindView(R.id.un_changeable_amount)
    GraphikText unChangeableAmount;
    @BindView(R.id.enter_amount_layout)
    View enterAmountLayout;

    private Activity activity;
    private InvestContract.Response responseContract;
    private Unbinder unbinder;
    private InvestContract.Presenter presenter;
    private LoginRequest loginRequest;
    private BottomSheetBehavior investBottomSheet;
    private AnalyticsContract analyticsContract;

    public InvestAmountPopUp(Activity activity, SharedPrefService sharedPrefService, MarketRepository marketRepository, StockHelper
            stockHelper, KiteRepository kiteRepository, InvestContract.Response responseContract, boolean isInvestMore,
                             AnalyticsContract analyticsContract) {
        unbinder = ButterKnife.bind(this, activity);

        this.activity = activity;
        this.responseContract = responseContract;
        this.analyticsContract = analyticsContract;

        investBottomSheet = BottomSheetBehavior.from(investAmountCard);
        investBottomSheet.setHideable(true);
        investBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);

        investBottomSheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    hideKeyboard();
                    moreFundsNeeded.setVisibility(View.GONE);
                    InvestAmountPopUp.this.responseContract.hideBlur();

                    confirmAmountText.setText("CONFIRM AMOUNT");
                    confirmAmount.setCardBackgroundColor(ContextCompat.getColor(InvestAmountPopUp.this.activity, R.color.blue_800));

                    minAmountContainer.setVisibility(View.INVISIBLE);
                    loadingMinInvestment.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        presenter = new InvestPresenter(activity, this, sharedPrefService, marketRepository, stockHelper,
                kiteRepository, responseContract, isInvestMore);
        loginRequest = new SmallcaseHelper(new AuthRepository(), this, sharedPrefService);
        amountToInvest.setTypeface(AppUtils.getInstance().getFont(activity, AppConstants.FontType.REGULAR));
    }

    @Override
    public void unBind() {
        if (unbinder != null) {
            unbinder.unbind();
            activity = null;
            presenter.unBind();
        }
    }

    @Override
    public boolean isInvestCardVisible() {
        return investBottomSheet.getState() == BottomSheetBehavior.STATE_EXPANDED;
    }

    @Override
    public void getMinAmount(List<Stock> stockList) {
        enterAmountLayout.setVisibility(View.VISIBLE);
        amountDivider.setVisibility(View.VISIBLE);
        unChangeableAmount.setVisibility(View.GONE);

        presenter.updateStockList(stockList);
        presenter.fetchMinimumAmount();

        if (Build.VERSION.SDK_INT < 21) {
            investAmountCard.setCardElevation(0);
            investAmountCard.setMaxCardElevation(0);
        }

        investBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
        responseContract.showBlur();
    }

    @Override
    public void showSimpleSnackBar(int errId) {

    }

    @Override
    public void onMinPriceReceived(double minPrice) {
        if (loadingMinInvestment == null) return;

        loadingMinInvestment.setVisibility(View.GONE);
        minAmountContainer.setVisibility(View.VISIBLE);


        if (enterAmountLayout.getVisibility() == View.VISIBLE) {
            String minAmount = AppConstants.getDecimalNumberFormatter().format(minPrice);
            SpannableString minInvestmentSpan = new SpannableString("Minimum investment is " + AppConstants.RUPEE_SYMBOL + minAmount);
            minInvestmentSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primary_text)), 21, minInvestmentSpan.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            minInvestmentAmount.setText(minInvestmentSpan);
            amountToInvest.setText(String.format(Locale.getDefault(), "%.2f", minPrice));
        } else {
            minInvestmentAmount.setText(R.string.based_on_shares_specified_by_you);
            unChangeableAmount.setText(AppConstants.RUPEE_SYMBOL + String.format(Locale.getDefault(), "%.2f", minPrice));
        }

        investBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    public void hideInvestCard() {
        hideKeyboard();
        moreFundsNeeded.setVisibility(View.GONE);
        responseContract.hideBlur();

        investBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
        minAmountContainer.setVisibility(View.INVISIBLE);
        loadingMinInvestment.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.confirm_amount)
    public void confirmOrAddFunds() {
        if (confirmAmountText.getText().toString().equalsIgnoreCase("Add funds")) {
            analyticsContract.sendEvent(null, "Proceeded To Adding Funds", Analytics.INTERCOM, Analytics.CLEVERTAP, Analytics.MIXPANEL);
            responseContract.onAddFundsClicked(getInvestmentAmount(), presenter.getMinInvestmentAmount());
        } else {
            responseContract.onConfirmAmountClicked(getInvestmentAmount(), presenter.getMinInvestmentAmount());
            if (presenter.isEnteredAmountGreaterThanMinAmount()) {
                minAmountContainer.setVisibility(View.INVISIBLE);
                loadingMinInvestment.setVisibility(View.VISIBLE);
                presenter.placeOrder();
            }
        }
    }

    @Override
    public double getInvestmentAmount() {
        if (!presenter.isWeightBasedCalculation()) {
            return presenter.getInvestmentAmount();
        }

        if (amountToInvest.getText() == null || amountToInvest.getText().toString().trim().length() == 0) return 0;

        return Double.valueOf(amountToInvest.getText().toString());
    }

    @Override
    public void showEditTextError(int errId) {
        Drawable errorIcon = errorIcon();
        errorIcon.setBounds(0, 0, (int) AppUtils.getInstance().dpToPx(24),
                (int) AppUtils.getInstance().dpToPx(24));
        loadingMinInvestment.setVisibility(View.GONE);
        minAmountContainer.setVisibility(View.VISIBLE);
        amountToInvest.setError(activity.getString(errId), errorIcon);
    }

    @Override
    public void setInvestmentAmount(double closestAmount) {
        amountToInvest.setText(String.format(Locale.getDefault(), "%.2f", closestAmount));
    }

    @Override
    public void showNotEnoughFunds(SpannableString neededFunds) {
        loadingMinInvestment.setVisibility(View.GONE);
        minAmountContainer.setVisibility(View.VISIBLE);

        moreFundsNeeded.setVisibility(View.VISIBLE);
        moreFundsNeeded.setText(neededFunds);

        confirmAmountText.setText("ADD FUNDS");
        confirmAmount.setCardBackgroundColor(ContextCompat.getColor(activity, R.color.purple_600));

        if (enterAmountLayout.getVisibility() == View.GONE) return;

        amountDivider.setBackgroundColor(ContextCompat.getColor(activity, R.color.red_600));

        amountToInvest.setSelection(0, amountToInvest.getText().length());
        amountToInvest.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                amountDivider.setBackgroundColor(ContextCompat.getColor(activity, R.color.grey_300));
                confirmAmountText.setText("CONFIRM AMOUNT");
                confirmAmount.setCardBackgroundColor(ContextCompat.getColor(activity, R.color.blue_800));
                amountToInvest.removeTextChangedListener(this);
                moreFundsNeeded.setVisibility(View.GONE);
                responseContract.onConfirmShownAgain();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void getAuthToken(HashMap<String, String> body) {
        loginRequest.getJwtToken(body);
    }

    @Override
    public void sendUserToKiteLogin() {
        responseContract.sendUserToKiteLogin();
    }

    @Override
    public void getAmountFromShares(List<Stock> stocksList) {
        enterAmountLayout.setVisibility(View.GONE);
        amountDivider.setVisibility(View.GONE);
        unChangeableAmount.setVisibility(View.VISIBLE);

        presenter.updateStockList(stocksList);
        presenter.calculateAmountFromShares();

        if (Build.VERSION.SDK_INT < 21) {
            investAmountCard.setCardElevation(0);
            investAmountCard.setMaxCardElevation(0);
        }

        investBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
        responseContract.showBlur();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private Drawable errorIcon() {
        return Build.VERSION.SDK_INT >= 21 ? activity.getResources().getDrawable
                (R.drawable.android_alert, null) : activity.getResources().getDrawable(R.drawable.android_alert);
    }

    private void hideKeyboard() {
        if (activity == null) return;

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        if (null == activity.getCurrentFocus()) return;
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onJwtSavedSuccessfully() {
        presenter.checkSufficientFunds();
    }

    @Override
    public void onFailedFetchingJwt() {
        hideInvestCard();
        showSimpleSnackBar(R.string.something_wrong);
    }

    @Override
    public void onDifferentUserLoggedIn() {
        if (activity == null) return;

        Intent intent = new Intent(activity, ContainerActivity.class);
        intent.putExtra(ContainerActivity.NEW_USER, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
    }
}
