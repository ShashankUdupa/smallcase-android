package com.smallcase.android.view.model;

/**
 * Created by shashankm on 06/03/17.
 */

public class PSegment {
    private String name;
    private String weightage;
    private int colorResId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeightage() {
        return weightage;
    }

    public void setWeightage(String weightage) {
        this.weightage = weightage;
    }

    public int getColorResId() {
        return colorResId;
    }

    public void setColorResId(int colorResId) {
        this.colorResId = colorResId;
    }
}
