package com.smallcase.android.view.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.news.WebActivity;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PPlainNews;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shashankm on 16/12/16.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {
    private static final String TAG = NewsAdapter.class.getSimpleName();

    private List<PPlainNews> newsList;
    private Activity activity;
    private AnalyticsContract analyticsContract;
    private boolean isInvestedNews;

    public NewsAdapter(Activity activity, List<PPlainNews> newsList, AnalyticsContract analyticsContract, boolean isInvestedNews) {
        this.activity = activity;
        this.analyticsContract = analyticsContract;
        this.isInvestedNews = isInvestedNews;
        this.newsList = new ArrayList<>(newsList);
    }

    @Override
    public NewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_card, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsAdapter.ViewHolder holder, int position) {
        final PPlainNews news = newsList.get(position);

        holder.headingLoading.setVisibility(View.GONE);
        holder.dateLoading.setVisibility(View.GONE);

        holder.newsImage.setBackgroundColor(0);
        holder.newsHeadline.setVisibility(View.VISIBLE);
        holder.timeAgo.setVisibility(View.VISIBLE);

        if (null == news.getNewsImage() || news.getNewsImage().equals("")) {
            holder.newsImage.setVisibility(View.GONE);
        } else {
            holder.newsImage.setVisibility(View.VISIBLE);
            Glide.with(activity)
                    .load(news.getNewsImage())
                    .asBitmap()
                    .placeholder(activity.getResources().getDrawable(R.color.grey_300))
                    .into(holder.newsImage);
        }
        holder.newsHeadline.setText(news.getNewsHeading());
        holder.timeAgo.setText(news.getDate());

        holder.newsCard.setOnClickListener(v -> {
            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("accessedFrom", isInvestedNews ? "Investment Details" : "smallcase Profile");
            eventProperties.put("newsDate", news.getDate());
            analyticsContract.sendEvent(eventProperties, "Viewed News Article", Analytics.MIXPANEL);

            Intent intent = new Intent(activity, WebActivity.class);
            intent.putExtra(WebActivity.TITLE, activity.getString(R.string.smallcase_news));
            intent.putExtra(WebActivity.URL, news.getLink());
            intent.putExtra(WebActivity.IS_LITE_URL, true);
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
        });
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public List<PPlainNews> getNewsList() {
        return newsList;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.news_card)
        View newsCard;
        @BindView(R.id.image_p_l_today)
        ImageView newsImage;
        @BindView(R.id.text_news_headline)
        GraphikText newsHeadline;
        @BindView(R.id.text_time_ago)
        GraphikText timeAgo;
        @BindView(R.id.headline_loading)
        View headingLoading;
        @BindView(R.id.date_loading)
        View dateLoading;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
