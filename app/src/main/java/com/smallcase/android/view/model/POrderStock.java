package com.smallcase.android.view.model;

/**
 * Created by shashankm on 07/04/17.
 */

public class POrderStock {
    private String stockTicker;

    private String quantityFilled;

    private String averagePrice;

    private String type;

    private boolean isFilled;

    private double remainingShares;

    private String triggeredPrice;

    private String orderType;

    private String productValidity;

    private String orderId;

    private String exchangeOrderId;

    private String orderStatus;

    private String sid;

    private String statusMessage;

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public String getQuantityFilled() {
        return quantityFilled;
    }

    public void setQuantityFilled(String quantityFilled) {
        this.quantityFilled = quantityFilled;
    }

    public String getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(String averagePrice) {
        this.averagePrice = averagePrice;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isFilled() {
        return isFilled;
    }

    public void setFilled(boolean filled) {
        isFilled = filled;
    }

    public double getRemainingShares() {
        return remainingShares;
    }

    public void setRemainingShares(double remainingShares) {
        this.remainingShares = remainingShares;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getSid() {
        return sid;
    }

    public String getTriggeredPrice() {
        return triggeredPrice;
    }

    public void setTriggeredPrice(String triggeredPrice) {
        this.triggeredPrice = triggeredPrice;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getProductValidity() {
        return productValidity;
    }

    public void setProductValidity(String productValidity) {
        this.productValidity = productValidity;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getExchangeOrderId() {
        return exchangeOrderId;
    }

    public void setExchangeOrderId(String exchangeOrderId) {
        this.exchangeOrderId = exchangeOrderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
