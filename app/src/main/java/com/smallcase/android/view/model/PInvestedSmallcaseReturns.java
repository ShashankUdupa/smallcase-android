package com.smallcase.android.view.model;

/**
 * Created by shashankm on 14/12/16.
 */

public class PInvestedSmallcaseReturns {
    private String networth;
    private String unRealizedPAndLPercent;
    private String unRealizedAmount;
    private boolean isUnRealizedProfit;
    private String currentInvestment;
    private String realizedPAndLAmount;
    private boolean isRealizedProfit;
    private String totalInvestment;
    private String totalPAndLAmount;
    private boolean isTotalProfit;
    private String dividend;

    public PInvestedSmallcaseReturns(String networth, String unRealizedPAndLPercent, String unRealizedAmount, boolean isUnRealizedProfit, String currentInvestment, String realizedPAndLAmount, boolean isRealizedProfit, String totalInvestment, String totalPAndLAmount, boolean isTotalProfit, String dividend) {
        this.networth = networth;
        this.unRealizedPAndLPercent = unRealizedPAndLPercent;
        this.unRealizedAmount = unRealizedAmount;
        this.isUnRealizedProfit = isUnRealizedProfit;
        this.currentInvestment = currentInvestment;
        this.realizedPAndLAmount = realizedPAndLAmount;
        this.isRealizedProfit = isRealizedProfit;
        this.totalInvestment = totalInvestment;
        this.totalPAndLAmount = totalPAndLAmount;
        this.isTotalProfit = isTotalProfit;
        this.dividend = dividend;
    }

    public String getNetworth() {
        return networth;
    }

    public String getUnRealizedPAndLPercent() {
        return unRealizedPAndLPercent;
    }

    public String getUnRealizedAmount() {
        return unRealizedAmount;
    }

    public boolean isUnRealizedProfit() {
        return isUnRealizedProfit;
    }

    public String getCurrentInvestment() {
        return currentInvestment;
    }

    public String getRealizedPAndLAmount() {
        return realizedPAndLAmount;
    }

    public boolean isRealizedProfit() {
        return isRealizedProfit;
    }

    public String getTotalInvestment() {
        return totalInvestment;
    }

    public String getTotalPAndLAmount() {
        return totalPAndLAmount;
    }

    public boolean isTotalProfit() {
        return isTotalProfit;
    }

    public String getDividend() {
        return dividend;
    }
}
