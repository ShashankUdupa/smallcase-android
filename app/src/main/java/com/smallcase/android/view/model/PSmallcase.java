package com.smallcase.android.view.model;

import com.smallcase.android.util.AppConstants;

/**
 * Created by shashankm on 25/01/17.
 */

public class PSmallcase {
    private String scid;

    private String smallcaseImage;

    private String smallcaseName;

    private String indexValue;

    private boolean isIndexUp;

    private boolean isInvested;

    private String smallcaseType;

    private String tier;

    public PSmallcase(String scid, String resolution, String smallcaseName, String indexValue, boolean isIndexUp, boolean
            isInvested, String smallcaseType, String tier) {
        this.scid = scid;
        this.smallcaseName = smallcaseName;
        this.indexValue = indexValue;
        this.isIndexUp = isIndexUp;
        this.smallcaseType = smallcaseType;
        this.isInvested = isInvested;
        this.smallcaseType = smallcaseType;
        this.tier = tier;
        this.smallcaseImage = AppConstants.SMALLCASE_IMAGE_URL + resolution + "/" + scid + ".png";
    }

    public String getScid() {
        return scid;
    }

    public String getSmallcaseImage() {
        return smallcaseImage;
    }

    public String getSmallcaseName() {
        return smallcaseName;
    }

    public String getIndexValue() {
        return indexValue;
    }

    public boolean isIndexUp() {
        return isIndexUp;
    }

    public boolean isInvested() {
        return isInvested;
    }

    public String getSmallcaseType() {
        return smallcaseType;
    }

    public String getTier() {
        return tier;
    }
}
