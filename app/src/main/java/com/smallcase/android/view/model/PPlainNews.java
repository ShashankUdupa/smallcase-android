package com.smallcase.android.view.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by shashankm on 16/12/16.
 */

public class PPlainNews implements Parcelable {
    private String newsImage;
    private String newsHeading;
    private String date;
    private String link;

    public PPlainNews() {

    }

    private PPlainNews(Parcel in) {
        newsImage = in.readString();
        newsHeading = in.readString();
        date = in.readString();
        link = in.readString();
    }

    public static final Creator<PPlainNews> CREATOR = new Creator<PPlainNews>() {
        @Override
        public PPlainNews createFromParcel(Parcel in) {
            return new PPlainNews(in);
        }

        @Override
        public PPlainNews[] newArray(int size) {
            return new PPlainNews[size];
        }
    };

    public String getNewsImage() {
        return newsImage;
    }

    public void setNewsImage(String newsImage) {
        this.newsImage = newsImage;
    }

    public String getNewsHeading() {
        return newsHeading;
    }

    public void setNewsHeading(String newsHeading) {
        this.newsHeading = newsHeading;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(newsImage);
        dest.writeString(newsHeading);
        dest.writeString(date);
        dest.writeString(link);
    }
}
