package com.smallcase.android.view.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by shashankm on 30/01/17.
 */

public class PCollection implements Parcelable {
    private String collectionName;
    private String cid;
    private String collectionImage;
    private String collectionDescription;
    private String smallcasesNumber;
    private List<String> scids;

    public PCollection(String collectionName, String cid, String collectionDescription, String smallcasesNumber, List<String> scids) {
        this.collectionName = collectionName;
        this.cid = cid;
        this.scids = scids;
        this.collectionImage = "https://www.smallcase.com/images/collections/80/" + this.cid +".png";
        this.collectionDescription = collectionDescription;
        this.smallcasesNumber = smallcasesNumber;
    }

    PCollection(Parcel in) {
        collectionName = in.readString();
        cid = in.readString();
        collectionImage = in.readString();
        collectionDescription = in.readString();
    }

    public static final Creator<PCollection> CREATOR = new Creator<PCollection>() {
        @Override
        public PCollection createFromParcel(Parcel in) {
            return new PCollection(in);
        }

        @Override
        public PCollection[] newArray(int size) {
            return new PCollection[size];
        }
    };

    public String getCollectionName() {
        return collectionName;
    }

    public String getCollectionImage() {
        return collectionImage;
    }

    public String getCollectionDescription() {
        return collectionDescription;
    }

    public String getSmallcasesNumber() {
        return String.valueOf(smallcasesNumber);
    }

    public String getCid() {
        return cid;
    }

    public List<String> getScids() {
        return scids;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(collectionName);
        dest.writeString(cid);
        dest.writeString(collectionImage);
        dest.writeString(collectionDescription);
    }
}
