package com.smallcase.android.view;

/**
 * Used for adding click listeners to items in recycler view.
 */

public interface ItemClickListener {
    /**
     * Call this method inside your onclick listener of an item in recycler view to
     * delegate the click handling to the parent recycler view.
     * @param type of caller. An appropriate action should be taken based on the item calling this method.
     * @param position of item in recycler view.
     */
    void onItemClicked(int type, int position);
}
