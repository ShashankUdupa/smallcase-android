package com.smallcase.android.view.buy;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;

import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.kite.KiteContract;
import com.smallcase.android.kite.KiteHelper;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.model.Stock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shashankm on 26/03/17.
 */

class InvestPresenter implements InvestContract.Presenter, KiteContract.Response, StockHelper.StockPriceCallBack {
    private static final String TAG = InvestPresenter.class.getSimpleName();

    private List<Stock> stockList = new ArrayList<>();
    private InvestContract.View viewContract;
    private InvestContract.Response detailContract;
    private StockHelper stockHelper;
    private double minInvestmentAmount = -1;
    private KiteContract.Request kiteHelper;
    private Activity activity;
    private MarketRepository marketRepository;
    private SharedPrefService sharedPrefService;
    private boolean isCalculatingPrice = false, isWeightBasedCalculation = true;
    private boolean isInvestMore;

    InvestPresenter(Activity activity, InvestContract.View viewContract, SharedPrefService
            sharedPrefService, MarketRepository marketRepository, StockHelper stockHelper, KiteRepository
                            kiteRepository, InvestContract.Response detailContract, boolean isInvestMore) {
        this.activity = activity;
        this.viewContract = viewContract;
        this.stockHelper = stockHelper;
        this.detailContract = detailContract;
        this.marketRepository = marketRepository;
        this.sharedPrefService = sharedPrefService;
        this.isInvestMore = isInvestMore;
        this.kiteHelper = new KiteHelper(sharedPrefService, kiteRepository);
    }

    @Override
    public void fetchMinimumAmount() {
        isWeightBasedCalculation = true;
        getStockPrice();
    }

    @Override
    public void onStockPricesReceived(HashMap<String, Double> stockMap) {
        // Get price if each stock
        for (Stock stock : stockList) {
            double price = stockMap.get(stock.getSid());
            stock.setPrice(price);
        }

        if (!isWeightBasedCalculation) {
            minInvestmentAmount = stockHelper.calculateAmountFromShares(stockList);
            viewContract.onMinPriceReceived(minInvestmentAmount);
            return;
        }

        // Calculate weightage only for invest more && if weight isn't already calculated
        if (isInvestMore && !stockList.isEmpty() && stockList.get(0).getWeight() <= 0) {
            stockHelper.calculateWeightageFromShares(stockList);
        }

        // Check if already calculating min amount.
        if (isCalculatingPrice) return;

        isCalculatingPrice = true;
        Observable.fromCallable(() -> stockHelper.calculateSharesAndMinAmountFromWeight(stockList, -1))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(stockListWithMinAmount -> {
                    isCalculatingPrice = false;
                    minInvestmentAmount = AppUtils.getInstance().round(stockListWithMinAmount.getMinAmount(), 2);
                    stockList = stockListWithMinAmount.getStockList();
                    viewContract.onMinPriceReceived(stockListWithMinAmount.getMinAmount());
                }, throwable -> {
                    isCalculatingPrice = false;
                    SentryAnalytics.getInstance().captureEvent(null, throwable, TAG + "; onStockPricesReceived()");
                    Log.d(TAG, "call: " + throwable.toString());
                    viewContract.showSimpleSnackBar(R.string.something_wrong);
                });
    }

    @Override
    public void onFailedFetchingStockPrice(Throwable throwable, @Nullable String sid) {
        if (throwable != null && throwable instanceof HttpException && ((HttpException) throwable).code() == 401) {
            viewContract.sendUserToKiteLogin();
            return;
        }

        SentryAnalytics.getInstance().captureEvent(null, throwable, TAG + "; onFailedFetchingStockPrice()");
        viewContract.showSimpleSnackBar(R.string.something_wrong);
    }

    @Override
    public void updateStockList(List<Stock> stockList) {
        this.stockList = stockList;
    }

    @Override
    public boolean isEnteredAmountGreaterThanMinAmount() {
        if (!isWeightBasedCalculation) return true;

        if (viewContract.getInvestmentAmount() < minInvestmentAmount) {
            viewContract.showEditTextError(R.string.greater_amount);
            return false;
        }
        return true;
    }

    @Override
    public void placeOrder() {
        if (!isWeightBasedCalculation) {
            kiteHelper.checkSufficientFunds(stockHelper.calculateAmountFromShares(stockList), InvestPresenter.this);
            return;
        }

        if (viewContract.getInvestmentAmount() == minInvestmentAmount) {
            kiteHelper.checkSufficientFunds(viewContract.getInvestmentAmount(), this);
            return;
        }

        double closestAmount = stockHelper.calculateClosestInvestableAmountFromWeight(stockList, viewContract.getInvestmentAmount());
        viewContract.setInvestmentAmount(closestAmount);
        kiteHelper.checkSufficientFunds(closestAmount, InvestPresenter.this);
    }

    @Override
    public void checkSufficientFunds() {
        kiteHelper.checkSufficientFunds(viewContract.getInvestmentAmount(), this);
    }

    @Override
    public double getMinInvestmentAmount() {
        return minInvestmentAmount;
    }

    @Override
    public void calculateAmountFromShares() {
        isWeightBasedCalculation = false;
        getStockPrice();
    }

    @Override
    public boolean isWeightBasedCalculation() {
        return isWeightBasedCalculation;
    }

    @Override
    public double getInvestmentAmount() {
        return stockHelper.calculateAmountFromShares(stockList);
    }

    @Override
    public void unBind() {
        activity = null;
    }

    @Override
    public void onFailedFetchingFunds(@Nullable Throwable throwable) {
        if (throwable == null) {
            viewContract.showSimpleSnackBar(R.string.something_wrong);
            return;
        }

        if (throwable instanceof HttpException) {
            HttpException exception = (HttpException) throwable;
            if (exception.code() == 401) {
                viewContract.sendUserToKiteLogin();
            }
        }
    }

    @Override
    public void sufficientFundsAvailable() {
        detailContract.canContinueTransaction(stockList);
    }

    @Override
    public void needMoreFunds(double availableFunds) {
        String needed = AppConstants.RUPEE_SYMBOL + AppConstants.getDecimalNumberFormatter().format(availableFunds);
        String availableFundText = "Oops, looks like you don’t have enough funds in your Kite account. Available funds: ";
        SpannableString neededFunds = new SpannableString(availableFundText + needed);
        neededFunds.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primary_text))
                , availableFundText.length(), neededFunds.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        viewContract.showNotEnoughFunds(neededFunds);
    }

    private void getStockPrice() {
        List<String> sids = new ArrayList<>();
        for (Stock stock : stockList) {
            sids.add(stock.getSid());
        }
        stockHelper.getStockPrices(marketRepository, sharedPrefService, sids, this);
    }
}
