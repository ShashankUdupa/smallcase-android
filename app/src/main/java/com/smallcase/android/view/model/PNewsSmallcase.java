package com.smallcase.android.view.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.smallcase.android.util.AppConstants;

/**
 * Created by shashankm on 31/01/17.
 */

public class PNewsSmallcase implements Parcelable {
    private String smallcaseImage;

    private String smallcaseName;

    private String change;

    private String iScid;

    private String scid;

    private boolean isChangePositive;

    private String tier;

    protected PNewsSmallcase(Parcel in) {
        smallcaseImage = in.readString();
        smallcaseName = in.readString();
        change = in.readString();
        iScid = in.readString();
        scid = in.readString();
        isChangePositive = in.readByte() != 0;
        tier = in.readString();
    }

    public PNewsSmallcase(String res, String smallcaseName, String change, String iScid, String scid,
                          boolean isChangePositive, String tier) {
        this.smallcaseImage = AppConstants.SMALLCASE_IMAGE_URL + res + "/" + scid +".png";
        this.smallcaseName = smallcaseName;
        this.change = change;
        this.iScid = iScid;
        this.scid = scid;
        this.isChangePositive = isChangePositive;
        this.tier = tier;
    }

    static final Creator<PNewsSmallcase> CREATOR = new Creator<PNewsSmallcase>() {
        @Override
        public PNewsSmallcase createFromParcel(Parcel in) {
            return new PNewsSmallcase(in);
        }

        @Override
        public PNewsSmallcase[] newArray(int size) {
            return new PNewsSmallcase[size];
        }
    };

    public String getSmallcaseImage() {
        return smallcaseImage;
    }

    public String getSmallcaseName() {
        return smallcaseName;
    }

    public String getChange() {
        return change;
    }

    public String getiScid() {
        return iScid;
    }

    public String getTier() {
        return tier;
    }

    public String getScid() {
        return scid;
    }

    public boolean isChangePositive() {
        return isChangePositive;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(smallcaseImage);
        dest.writeString(smallcaseName);
        dest.writeString(change);
        dest.writeString(iScid);
        dest.writeString(scid);
        dest.writeByte((byte) (isChangePositive ? 1 : 0));
        dest.writeString(tier);
    }
}
