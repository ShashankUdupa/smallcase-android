package com.smallcase.android.view.model;

/**
 * Created by shashankm on 21/11/16.
 */

public class PHeader {
    private String title, link;
    private int type;

    public PHeader(String title, String link, int type) {
        this.title = title;
        this.link = link;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public int getType() {
        return type;
    }
}
