package com.smallcase.android.view.android;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.smallcase.android.R;
import com.smallcase.android.view.ItemTouchHelperAdapter;

/**
 * Created by shashankm on 29/09/17.
 */

public class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {
    private static final String TAG = "SimpleItemHelper";

    private final ItemTouchHelperAdapter adapter;

    private Paint paint;

    private int unAcceptedViewType;

    private boolean isDrag = false;

    public SimpleItemTouchHelperCallback(Activity activity, ItemTouchHelperAdapter adapter, int unAcceptedViewType) {
        this.adapter = adapter;
        this.unAcceptedViewType = unAcceptedViewType;
        paint = new Paint();
        paint.setColor(ContextCompat.getColor(activity, R.color.red_400));
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        if (viewHolder.getItemViewType() == unAcceptedViewType) return makeMovementFlags(0, 0);

        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                          RecyclerView.ViewHolder target) {
        adapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (actionState == ItemTouchHelper.ACTION_STATE_DRAG) isDrag = true;

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) isDrag = false;

        if (actionState == ItemTouchHelper.ACTION_STATE_IDLE && isDrag) adapter.onItemMoved();

        super.onSelectedChanged(viewHolder, actionState);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        adapter.onItemDismiss(viewHolder.getAdapterPosition());
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            // Get RecyclerView item from the ViewHolder
            View itemView = viewHolder.itemView;

            if (dX > 0) {
                // Draw Rect with varying right side, equal to displacement dX
                c.drawRect((float) itemView.getLeft(), (float) itemView.getTop(), dX,
                        (float) itemView.getBottom(), paint);
            } else {
                // Draw Rect with varying left side, equal to the item's right side plus negative displacement dX
                c.drawRect((float) itemView.getRight() + dX, (float) itemView.getTop(),
                        (float) itemView.getRight(), (float) itemView.getBottom(), paint);
            }
        }

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }
}
