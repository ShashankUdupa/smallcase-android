package com.smallcase.android.view.adapter;

import android.app.Activity;
import android.support.transition.TransitionManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.investedsmallcase.InvestedSmallcaseDetailContract;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.view.ItemClickListener;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PStock;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shashankm on 15/12/16.
 */

public class StockAdapterDelegate extends RecyclerView.Adapter<StockAdapterDelegate.StockViewHolder> {
    private static final String TAG = StockAdapterDelegate.class.getSimpleName();
    private static final AccelerateDecelerateInterpolator ACCELERATE_DECELERATE_INTERPOLATOR = new AccelerateDecelerateInterpolator();

    private LayoutInflater inflater;
    private Activity activity;
    private ItemClickListener itemClickListener;
    private List<PStock> stocks;
    private AnalyticsContract analyticsContract;
    private Animations animations;
    private int expandPosition = -1;
    private InvestedSmallcaseDetailContract.View view;

    public StockAdapterDelegate(Activity activity, ItemClickListener itemClickListener, List<PStock> stocks, AnalyticsContract analyticsContract,
                                InvestedSmallcaseDetailContract.View view) {
        this.activity = activity;
        this.itemClickListener = itemClickListener;
        this.stocks = new ArrayList<>(stocks);
        this.analyticsContract = analyticsContract;
        this.view = view;
        inflater = activity.getLayoutInflater();
        animations = new Animations();
    }

    @Override
    public StockViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StockViewHolder(inflater.inflate(R.layout.invested_stock_card, parent, false));
    }

    @Override
    public void onBindViewHolder(final StockViewHolder viewHolder, final int position) {
        final PStock stock = stocks.get(position);
        if (stock.isHeading()) {
            viewHolder.stockName.setText(activity.getString(R.string.stock));
            viewHolder.pAndL.setText(activity.getString(R.string.p_l));
            viewHolder.moreIcon.setVisibility(View.INVISIBLE);
            viewHolder.moreStockInfoLayout.setVisibility(View.GONE);

            viewHolder.stockName.setTextColor(ContextCompat.getColor(activity, R.color.secondary_text));
            viewHolder.pAndL.setTextColor(ContextCompat.getColor(activity, R.color.secondary_text));
            return;
        }

        viewHolder.moreIcon.setVisibility(View.VISIBLE);

        if (stock.isExpanded()) {
            if (expandPosition == position) {
                animations.expand(viewHolder.moreStockInfoLayout, ACCELERATE_DECELERATE_INTERPOLATOR, 300);
            } else {
                viewHolder.moreStockInfoLayout.setVisibility(View.VISIBLE);
            }
            viewHolder.moreIcon.setRotation(270);
        } else {
            if (expandPosition == position) {
                animations.collapse(viewHolder.moreStockInfoLayout, ACCELERATE_DECELERATE_INTERPOLATOR, 300);
            } else {
                viewHolder.moreStockInfoLayout.setVisibility(View.GONE);
            }
            viewHolder.moreIcon.setRotation(90);
        }

        Glide.with(activity)
                .load(R.drawable.arrow_right)
                .into(viewHolder.moreIcon);

        viewHolder.stockName.setText(stock.getStockName());
        viewHolder.shares.setText(String.valueOf((int) stock.getShares()));
        viewHolder.averageBuyPrice.setText(stock.getAverageBuyPrice());
        viewHolder.currentPrice.setText(stock.getCurrentPrice());

        String weightage = String.format(Locale.getDefault(), "%.1f", stock.getStockWeightage()) + "%";
        viewHolder.weightage.setText(weightage);

        String pAndL = String.format(Locale.getDefault(), "%.2f", stock.getStockPAndL()) + "%";
        viewHolder.pAndL.setText(pAndL);
        if (stock.isProfit()) {
            viewHolder.pAndL.setTextColor(ContextCompat.getColor(activity, R.color.green_600));
        } else {
            viewHolder.pAndL.setTextColor(ContextCompat.getColor(activity, R.color.red_600));
        }

        viewHolder.stockName.setTextColor(ContextCompat.getColor(activity, R.color.primary_text));

        viewHolder.stockCard.setOnClickListener(v -> toggleCard(viewHolder, stock));

        viewHolder.stockName.setOnClickListener(v -> {
            if (position == 0) return;

            itemClickListener.onItemClicked(AppConstants.STOCK, position);
        });
    }

    private void toggleCard(StockViewHolder viewHolder, PStock stock) {
        if (viewHolder.getAdapterPosition() == 0) return;

        stock.setExpanded(!stock.isExpanded());
        expandPosition = viewHolder.getAdapterPosition();
        if (!stock.isExpanded()) TransitionManager.beginDelayedTransition(view.getRecyclerView());
        notifyItemChanged(viewHolder.getAdapterPosition());
    }

    @Override
    public int getItemCount() {
        return stocks.size();
    }

    public void updateData(List<PStock> stocks) {
        this.stocks.clear();
        this.stocks = new ArrayList<>(stocks);
        notifyDataSetChanged();
    }

    public List<PStock> getStocks() {
        return stocks;
    }

    class StockViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_stock_name)
        GraphikText stockName;
        @BindView(R.id.text_p_n_l)
        GraphikText pAndL;
        @BindView(R.id.stock_card)
        View stockCard;
        @BindView(R.id.divider)
        View divider;
        @BindView(R.id.more_icon)
        ImageView moreIcon;
        @BindView(R.id.more_stock_info_layout)
        View moreStockInfoLayout;
        @BindView(R.id.shares)
        GraphikText shares;
        @BindView(R.id.weightage)
        GraphikText weightage;
        @BindView(R.id.avg_buy_price)
        GraphikText averageBuyPrice;
        @BindView(R.id.current_price)
        GraphikText currentPrice;

        StockViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
