package com.smallcase.android.view.buy;

import android.text.SpannableString;

import com.smallcase.android.view.model.Stock;

import java.util.HashMap;
import java.util.List;

/**
 * Created by shashankm on 26/03/17.
 */

public interface InvestContract {

    interface Authenticate {
        void getAuthToken(HashMap<String, String> body);
    }

    interface View {
        void unBind();

        boolean isInvestCardVisible();

        void getMinAmount(List<Stock> stockList);

        void showSimpleSnackBar(int errId);

        void onMinPriceReceived(double minPrice);

        void hideInvestCard();

        double getInvestmentAmount();

        void showEditTextError(int errId);

        void setInvestmentAmount(double closestAmount);

        void showNotEnoughFunds(SpannableString neededFunds);

        void sendUserToKiteLogin();

        void getAmountFromShares(List<Stock> stocksList);
    }

    interface Presenter {
        void fetchMinimumAmount();

        void updateStockList(List<Stock> stockList);

        boolean isEnteredAmountGreaterThanMinAmount();

        void placeOrder();

        void checkSufficientFunds();

        double getMinInvestmentAmount();

        void calculateAmountFromShares();

        boolean isWeightBasedCalculation();

        double getInvestmentAmount();

        void unBind();
    }

    interface Response {

        void showBlur();

        void hideBlur();

        void sendUserToKiteLogin();

        void canContinueTransaction(List<Stock> stockList);

        void onConfirmAmountClicked(double investmentAmount, double minInvestmentAmount);

        void onAddFundsClicked(double investmentAmount, double minInvestmentAmount);

        void onConfirmShownAgain();
    }
}
