package com.smallcase.android.view.android;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * This is scroll listener which is used to determine if the user has scrolled to
 * the end of recycler view.
 */
public class ScrollListener extends RecyclerView.OnScrollListener {
    private LinearLayoutManager layoutManager;
    private ScrollInterface scrollInterface;

    public interface ScrollInterface {
        //Called when user reached the end of recycler view
        void onListEndReached();
    }

    //layout manager should be vertical linear layout, if needed for horizontal check for dx instead of dy
    public ScrollListener(LinearLayoutManager layoutManager, ScrollInterface scrollInterface) {
        this.layoutManager = layoutManager;
        this.scrollInterface = scrollInterface;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if (dy > 0) {
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();

            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) scrollInterface.onListEndReached();
        }
    }
}
