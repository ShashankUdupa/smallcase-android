package com.smallcase.android.view.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shashankm on 31/01/17.
 */

public class PSmallcaseWithNews implements Parcelable {
    private String newsImage;
    private String newsHeadline;
    private String newsDate;
    private String newsLink;
    private boolean isExpanded;
    private List<PNewsSmallcase> investedSmallcase;

    protected PSmallcaseWithNews(Parcel in) {
        newsImage = in.readString();
        newsHeadline = in.readString();
        newsDate = in.readString();
        newsLink = in.readString();
        isExpanded = in.readByte() != 0;
        investedSmallcase = new ArrayList<PNewsSmallcase>();
        in.readTypedList(investedSmallcase, PNewsSmallcase.CREATOR);
    }

    public PSmallcaseWithNews(String newsImage, String newsHeadline, String newsDate, List<PNewsSmallcase>
            investedSmallcase, String newsLink) {
        this.newsImage = newsImage;
        this.newsHeadline = newsHeadline;
        this.newsDate = newsDate;
        this.isExpanded = false;
        this.newsLink = newsLink;
        this.investedSmallcase = investedSmallcase;
    }

    public static final Creator<PSmallcaseWithNews> CREATOR = new Creator<PSmallcaseWithNews>() {
        @Override
        public PSmallcaseWithNews createFromParcel(Parcel in) {
            return new PSmallcaseWithNews(in);
        }

        @Override
        public PSmallcaseWithNews[] newArray(int size) {
            return new PSmallcaseWithNews[size];
        }
    };

    public String getNewsImage() {
        return newsImage;
    }

    public String getNewsHeadline() {
        return newsHeadline;
    }

    public String getNewsDate() {
        return newsDate;
    }

    public List<PNewsSmallcase> getInvestedSmallcase() {
        return investedSmallcase;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(newsImage);
        dest.writeString(newsHeadline);
        dest.writeString(newsDate);
        dest.writeString(newsLink);
        dest.writeByte((byte) (isExpanded? 1 : 0));
        dest.writeTypedList(investedSmallcase);
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public String getNewsLink() {
        return newsLink;
    }
}
