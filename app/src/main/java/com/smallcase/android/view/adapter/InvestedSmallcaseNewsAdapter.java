package com.smallcase.android.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.investedsmallcase.InvestedSmallcaseDetailActivity;
import com.smallcase.android.news.AllNewsContract;
import com.smallcase.android.news.WebActivity;
import com.smallcase.android.smallcase.smallcasedetail.SmallcaseDetailActivity;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PInvestedSmallcase;
import com.smallcase.android.view.model.PInvestedSmallcaseWithNews;
import com.smallcase.android.view.model.PNewsSmallcase;
import com.smallcase.android.view.model.PSmallcaseWithNews;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shashankm on 31/01/17.
 */

public class InvestedSmallcaseNewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = InvestedSmallcaseNewsAdapter.class.getSimpleName();

    private List<Parcelable> newsItemList;
    private Activity activity;
    private static final int LIST_TYPE_LOADING = 2;
    private static final int LIST_TYPE_INVESTED = 1;
    private static final int LIST_TYPE_UN_INVESTED = 3;
    private boolean itemsRemaining;
    private AllNewsContract.Presenter allNewsPresenter;
    private LayoutInflater inflater;
    private AnalyticsContract analyticsContract;

    public InvestedSmallcaseNewsAdapter(Activity activity, AllNewsContract.Presenter allNewsPresenter,
                                        List<Parcelable> newsItemList, AnalyticsContract analyticsContract) {
        this.activity = activity;
        this.allNewsPresenter = allNewsPresenter;
        this.newsItemList = new ArrayList<>(newsItemList);
        this.analyticsContract = analyticsContract;
        itemsRemaining = true;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == newsItemList.size() && itemsRemaining) {
            return LIST_TYPE_LOADING;
        } else if (newsItemList.get(position) instanceof PInvestedSmallcaseWithNews) {
            return LIST_TYPE_INVESTED;
        } else if (newsItemList.get(position) instanceof PSmallcaseWithNews) {
            return LIST_TYPE_UN_INVESTED;
        }
        return -1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == LIST_TYPE_INVESTED) {
            viewHolder = new NewsViewHolder(inflater.inflate(R.layout.invested_news_card, parent, false));
        } else if (viewType == LIST_TYPE_UN_INVESTED) {
            viewHolder = new UnInvestedHolder(inflater.inflate(R.layout.un_invested_news_card, parent, false));
        } else {
            viewHolder = new LoadingViewHolder(inflater.inflate(R.layout.loading_indicator, parent, false));
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == LIST_TYPE_INVESTED) {
            final NewsViewHolder newsViewHolder = (NewsViewHolder) holder;
            final PInvestedSmallcaseWithNews smallcaseNews = (PInvestedSmallcaseWithNews) newsItemList.get(holder.getAdapterPosition());
            newsViewHolder.bind(smallcaseNews);
        } else if (holder.getItemViewType() == LIST_TYPE_UN_INVESTED) {
            UnInvestedHolder unInvestedHolder = (UnInvestedHolder) holder;
            final PSmallcaseWithNews smallcaseWithNews = (PSmallcaseWithNews) newsItemList.get(position);
            unInvestedHolder.bind(smallcaseWithNews);
        }
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder) {
        if (holder instanceof NewsViewHolder) {
            NewsViewHolder newsViewHolder = (NewsViewHolder) holder;
            //Remove dynamically added view (so that expanded smallcases don't stick on to other views when recycled)
            if (newsViewHolder.newsContainer.getChildCount() > 4) {
                newsViewHolder.newsContainer.removeViews(4, newsViewHolder.newsContainer.getChildCount() - 4);
            }
        } else if (holder instanceof UnInvestedHolder) {
            UnInvestedHolder unInvestedHolder = (UnInvestedHolder) holder;
            //Remove dynamically added view (so that expanded smallcases don't stick on to other views when recycled)
            if (unInvestedHolder.newsUnInvestedCard.getChildCount() > 4) {
                unInvestedHolder.newsUnInvestedCard.removeViews(4, unInvestedHolder.newsUnInvestedCard.getChildCount() - 4);
            }
        }

        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        if (itemsRemaining) return newsItemList.size() + 1;
        return newsItemList.size();
    }

    private void toggleViews(int visibility, View... views) {
        for (View view : views) {
            view.setVisibility(visibility);
        }
    }

    public void noMoreItems() {
        itemsRemaining = false;
    }

    public List<Parcelable> getNewsItems() {
        return newsItemList;
    }

    class NewsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_index_value_invested)
        TextView smallcaseIndex;
        @BindView(R.id.up_or_down_since_news)
        ImageView upOrDown;
        @BindView(R.id.image_invested_smallcase)
        ImageView smallcaseImage;
        @BindView(R.id.text_invested_smallcase)
        TextView smallcaseName;
        @BindView(R.id.invested_smallcase_card)
        View investedSmallcaseCard;
        @BindView(R.id.image_p_l_today)
        ImageView newsImage;
        @BindView(R.id.text_news_headline)
        TextView newsHeading;
        @BindView(R.id.text_time_ago)
        TextView timeAgo;
        @BindView(R.id.news_card)
        View newsCard;
        @BindView(R.id.divider_news)
        View newsDivider;
        @BindView(R.id.smallcase_name_loading)
        View smallcaseNameLoading;
        @BindView(R.id.headline_loading)
        View headingLoading;
        @BindView(R.id.date_loading)
        View dateLoading;
        @BindView(R.id.more_smallcases_container)
        View moreSmallcaseContainer;
        @BindView(R.id.text_more_smallcases)
        GraphikText moreSmallcases;
        @BindView(R.id.news_container)
        LinearLayout newsContainer;

        NewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(final PInvestedSmallcaseWithNews smallcaseNews) {
            toggleViews(View.GONE, smallcaseNameLoading, headingLoading, dateLoading);
            newsImage.setBackgroundColor(0);
            smallcaseImage.setBackgroundColor(0);
            toggleViews(View.VISIBLE, smallcaseIndex, smallcaseName, upOrDown, newsHeading, timeAgo);

            if (smallcaseNews.getInvestedSmallcase().isEmpty()) return; // Shouldn't be empty
            final PInvestedSmallcase investedSmallcase = smallcaseNews.getInvestedSmallcase().get(0);

            if (null == smallcaseNews.getNewsImage()) {
                newsImage.setVisibility(View.GONE);
            } else {
                newsImage.setVisibility(View.VISIBLE);
                Glide.with(activity)
                        .load(smallcaseNews.getNewsImage())
                        .asBitmap()
                        .placeholder(activity.getResources().getDrawable(R.color.grey_300))
                        .into(newsImage);
            }

            Glide.with(activity)
                    .load(investedSmallcase.getSmallcaseImage())
                    .asBitmap()
                    .placeholder(activity.getResources().getDrawable(R.color.grey_300))
                    .into(new BitmapImageViewTarget(smallcaseImage) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable roundImage = RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                            roundImage.setCornerRadius(AppUtils.getInstance().dpToPx(2));
                            smallcaseImage.setImageDrawable(roundImage);
                        }
                    });
            Glide.with(activity)
                    .load(investedSmallcase.isIndexUp() ? R.drawable.up_arrow : R.drawable.down_arrow_red)
                    .asBitmap()
                    .into(upOrDown);

            newsDivider.setVisibility(View.VISIBLE);

            smallcaseName.setText(investedSmallcase.getSmallcaseName());
            newsHeading.setText(smallcaseNews.getNewsHeadline());
            timeAgo.setText(AppUtils.getInstance().getTimeAgoDate(smallcaseNews.getDate()));
            smallcaseIndex.setText(investedSmallcase.getSmallcaseIndex());

            investedSmallcaseCard.setOnClickListener(allNewsPresenter.getSmallcaseClickListener(
                    activity, investedSmallcase));

            newsCard.setOnClickListener(v -> {
                Map<String, Object> eventProperties = new HashMap<>();
                eventProperties.put("accessedFrom", "All News");
                analyticsContract.sendEvent(eventProperties, "Viewed News Article", Analytics.MIXPANEL);

                allNewsPresenter.getNewsClickListener(activity, smallcaseNews.getNewsLink());
            });

            if (smallcaseNews.isExpanded()) {
                addInvestedSmallcases(smallcaseNews);
            }

            if (smallcaseNews.getInvestedSmallcase().size() > 1 && !smallcaseNews.isExpanded()) {
                moreSmallcaseContainer.setVisibility(View.VISIBLE);
                int remaining = smallcaseNews.getInvestedSmallcase().size() - 1;
                String smallcaseNumber = "+" + remaining + " smallcase" + (remaining > 1 ? "s" : "");
                moreSmallcases.setText(smallcaseNumber);
                moreSmallcaseContainer.setOnClickListener(v -> {
                    smallcaseNews.setExpanded(true);
                    notifyItemChanged(getAdapterPosition());
                });
            } else {
                moreSmallcaseContainer.setVisibility(View.GONE);
            }
        }

        private void addInvestedSmallcases(PInvestedSmallcaseWithNews smallcaseNews) {
            moreSmallcaseContainer.setVisibility(View.GONE);
            for (int i = 1; i < smallcaseNews.getInvestedSmallcase().size(); i++) {
                final PInvestedSmallcase smallcase = smallcaseNews.getInvestedSmallcase().get(i);
                View view = inflater.inflate(R.layout.invested_smallcase_news_card, (ViewGroup) investedSmallcaseCard.getParent(), false);
                final ImageView smallcaseImage = view.findViewById(R.id.image_invested_smallcase);
                View smallcaseNameLoading = view.findViewById(R.id.smallcase_name_loading);
                GraphikText investedSmallcaseName = view.findViewById(R.id.text_invested_smallcase);
                GraphikText investedIndex = view.findViewById(R.id.text_index_value_invested);
                ImageView upOrDown = view.findViewById(R.id.up_or_down_since_news);
                View divider = view.findViewById(R.id.divider_smallcase);
                View investedSmallcaseCard = view.findViewById(R.id.invested_smallcase_card);

                smallcaseNameLoading.setVisibility(View.GONE);
                toggleViews(View.VISIBLE, investedSmallcaseName, investedIndex, upOrDown);
                smallcaseImage.setBackgroundColor(0);
                Glide.with(activity)
                        .load(smallcase.getSmallcaseImage())
                        .asBitmap()
                        .placeholder(activity.getResources().getColor(R.color.grey_300))
                        .into(new BitmapImageViewTarget(smallcaseImage) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable roundImage = RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                                roundImage.setCornerRadius(AppUtils.getInstance().dpToPx(2));
                                smallcaseImage.setImageDrawable(roundImage);
                            }
                        });
                investedSmallcaseName.setText(smallcase.getSmallcaseName());
                investedIndex.setText(smallcase.getSmallcaseIndex());
                Glide.with(activity)
                        .load(smallcase.isIndexUp() ? R.drawable.up_arrow : R.drawable.down_arrow_red)
                        .asBitmap()
                        .into(upOrDown);
                investedSmallcaseCard.setOnClickListener(v -> {
                    Intent intent = new Intent(activity, InvestedSmallcaseDetailActivity.class);
                    intent.putExtra(InvestedSmallcaseDetailActivity.SCID, smallcase.getScid());
                    intent.putExtra(InvestedSmallcaseDetailActivity.ISCID, smallcase.getiScid());
                    intent.putExtra(InvestedSmallcaseDetailActivity.SOURCE, smallcase.getSource());
                    activity.startActivity(intent);
                });

                newsContainer.addView(view, i + 3, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                divider.setVisibility(i < (smallcaseNews.getInvestedSmallcase().size() - 1) ? View.VISIBLE : View.GONE);
            }
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        LoadingViewHolder(View itemView) {
            super(itemView);
        }
    }

    class UnInvestedHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_p_l_today)
        ImageView newsImage;
        @BindView(R.id.text_news_headline)
        TextView newsHeading;
        @BindView(R.id.text_time_ago)
        TextView timeAgo;
        @BindView(R.id.news_card)
        View newsCard;
        @BindView(R.id.divider_news)
        View newsDivider;
        @BindView(R.id.smallcase_name_loading)
        View smallcaseNameLoading;
        @BindView(R.id.headline_loading)
        View headingLoading;
        @BindView(R.id.date_loading)
        View dateLoading;
        @BindView(R.id.text_since_news)
        GraphikText sinceNewsText;
        @BindView(R.id.up_or_down_since_news)
        ImageView upOrDown;
        @BindView(R.id.percent_change)
        GraphikText percentChange;
        @BindView(R.id.image_smallcase)
        ImageView smallcaseImage;
        @BindView(R.id.text_smallcase_name)
        GraphikText smallcaseName;
        @BindView(R.id.news_un_invested_card)
        LinearLayout newsUnInvestedCard;
        @BindView(R.id.more_smallcases_container)
        View moreSmallcasesContainer;
        @BindView(R.id.text_more_smallcases)
        GraphikText numberOfSmallcases;
        @BindView(R.id.smallcase_card_un_invested)
        View unInvestedCard;

        UnInvestedHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(final PSmallcaseWithNews smallcaseWithNews) {
            toggleViews(View.GONE, dateLoading, headingLoading, smallcaseNameLoading);
            toggleViews(View.VISIBLE, sinceNewsText, newsHeading, timeAgo);
            smallcaseImage.setBackgroundColor(0);
            newsImage.setBackgroundColor(0);
            newsDivider.setVisibility(View.VISIBLE);

            newsHeading.setText(smallcaseWithNews.getNewsHeadline());
            timeAgo.setText(AppUtils.getInstance().getTimeAgoDate(smallcaseWithNews.getNewsDate()));
            Glide.with(activity)
                    .load(smallcaseWithNews.getNewsImage())
                    .asBitmap()
                    .placeholder(activity.getResources().getDrawable(R.color.grey_300))
                    .into(newsImage);
            if (smallcaseWithNews.getInvestedSmallcase().isEmpty()) return;

            final PNewsSmallcase smallcase = smallcaseWithNews.getInvestedSmallcase().get(0);
            smallcaseName.setText(smallcase.getSmallcaseName());
            percentChange.setText(smallcase.getChange());
            int color = smallcase.isChangePositive() ? R.color.green_600 : R.color.red_600;
            percentChange.setTextColor(ContextCompat.getColor(activity, color));
            Glide.with(activity)
                    .load(smallcase.isChangePositive() ? R.drawable.up_arrow : R.drawable.down_arrow_red)
                    .asBitmap()
                    .into(upOrDown);
            Glide.with(activity)
                    .load(smallcase.getSmallcaseImage())
                    .asBitmap()
                    .placeholder(activity.getResources().getDrawable(R.color.grey_300))
                    .into(new BitmapImageViewTarget(smallcaseImage) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable roundImage = RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                            roundImage.setCornerRadius(AppUtils.getInstance().dpToPx(2));
                            smallcaseImage.setImageDrawable(roundImage);
                        }
                    });

            newsCard.setOnClickListener(v -> {
                Intent intent = new Intent(activity, WebActivity.class);
                intent.putExtra(WebActivity.TITLE, activity.getString(R.string.smallcase_news));
                intent.putExtra(WebActivity.URL, smallcaseWithNews.getNewsLink());
                intent.putExtra(WebActivity.IS_LITE_URL, true);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
            });

            unInvestedCard.setOnClickListener(v -> {
                Intent intent = new Intent(activity, SmallcaseDetailActivity.class);
                intent.putExtra(SmallcaseDetailActivity.TITLE, smallcase.getSmallcaseName());
                intent.putExtra(SmallcaseDetailActivity.SCID, smallcase.getScid());
                intent.putExtra(SmallcaseDetailActivity.ACCESSED_FROM, "All News");
                activity.startActivity(intent);
            });

            if (smallcaseWithNews.isExpanded()) {
                moreSmallcasesContainer.setVisibility(View.GONE);
                for (int i = 1; i < smallcaseWithNews.getInvestedSmallcase().size(); i++) {
                    expandSmallcase(smallcaseWithNews, i);
                }
            }

            if (smallcaseWithNews.getInvestedSmallcase().size() > 1 && !smallcaseWithNews.isExpanded()) {
                moreSmallcasesContainer.setVisibility(View.VISIBLE);
                int remaining = smallcaseWithNews.getInvestedSmallcase().size() - 1;
                String smallcaseNumber = "+" + remaining + " smallcase" + (remaining > 1 ? "s" : "");
                numberOfSmallcases.setText(smallcaseNumber);
                moreSmallcasesContainer.setOnClickListener(v -> {
                    smallcaseWithNews.setExpanded(true);
                    notifyItemChanged(getAdapterPosition());
                });
            } else {
                moreSmallcasesContainer.setVisibility(View.GONE);
            }
        }

        private void expandSmallcase(PSmallcaseWithNews smallcaseWithNews, int i) {
            final PNewsSmallcase newsSmallcase = smallcaseWithNews.getInvestedSmallcase().get(i);
            View view = inflater.inflate(R.layout.smallcase_news_card, null);
            final ImageView smallcaseImage = view.findViewById(R.id.image_smallcase);
            View smallcaseNameLoading = view.findViewById(R.id.smallcase_name_loading);
            GraphikText smallcaseName = view.findViewById(R.id.text_smallcase_name);
            GraphikText percentChange = view.findViewById(R.id.percent_change);
            ImageView upOrDown = view.findViewById(R.id.up_or_down_since_news);
            View divider = view.findViewById(R.id.divider_smallcase);
            View textSinceNews = view.findViewById(R.id.text_since_news);
            View unInvestedCard = view.findViewById(R.id.smallcase_card_un_invested);

            textSinceNews.setVisibility(View.VISIBLE);
            newsUnInvestedCard.addView(view, i + 3, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            smallcaseImage.setBackgroundColor(0);
            Glide.with(activity)
                    .load(newsSmallcase.getSmallcaseImage())
                    .asBitmap()
                    .placeholder(activity.getResources().getDrawable(R.color.grey_300))
                    .into(new BitmapImageViewTarget(smallcaseImage) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable roundImage = RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                            roundImage.setCornerRadius(AppUtils.getInstance().dpToPx(2));
                            smallcaseImage.setImageDrawable(roundImage);
                        }
                    });

            unInvestedCard.setOnClickListener(v -> {
                Intent intent = new Intent(activity, SmallcaseDetailActivity.class);
                intent.putExtra(SmallcaseDetailActivity.TITLE, newsSmallcase.getSmallcaseName());
                intent.putExtra(SmallcaseDetailActivity.SCID, newsSmallcase.getScid());
                activity.startActivity(intent);
            });

            smallcaseNameLoading.setVisibility(View.GONE);
            smallcaseName.setText(newsSmallcase.getSmallcaseName());
            percentChange.setText(newsSmallcase.getChange());
            Glide.with(activity)
                    .load(newsSmallcase.isChangePositive() ? R.drawable.up_arrow : R.drawable.down_arrow_red)
                    .asBitmap()
                    .into(upOrDown);
            divider.setVisibility(i < (smallcaseWithNews.getInvestedSmallcase().size() - 1) ? View.VISIBLE : View.INVISIBLE);
        }
    }
}
