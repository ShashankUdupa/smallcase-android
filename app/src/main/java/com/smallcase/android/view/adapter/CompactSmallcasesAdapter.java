package com.smallcase.android.view.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.smallcase.discover.DiscoverSmallcaseActivity;
import com.smallcase.android.smallcase.smallcasedetail.SmallcaseDetailActivity;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.SmallcaseTier;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PSmallcase;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shashankm on 27/01/17.
 */

public class CompactSmallcasesAdapter extends RecyclerView.Adapter<CompactSmallcasesAdapter.ViewHolder> {
    private static final String TAG = CompactSmallcasesAdapter.class.getSimpleName();

    private Activity activity;
    private List<PSmallcase> smallcaseList;
    private boolean fetchedRecentSmallcase = false;
    private Animations animations;
    private int type;
    private AnalyticsContract analyticsContract;

    public CompactSmallcasesAdapter(Activity activity, int type, AnalyticsContract analyticsContract) {
        this.activity = activity;
        this.type = type;
        this.analyticsContract = analyticsContract;
        animations = new Animations();
    }

    @Override
    public CompactSmallcasesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.smallcase_card, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CompactSmallcasesAdapter.ViewHolder holder, int position) {
        if (null == smallcaseList || !fetchedRecentSmallcase) {
            animations.loadingContent(holder.smallcaseNameLoading, holder.smallcaseIndexLoading);
            return;
        }

        animations.stopAnimation();
        if (holder.getAdapterPosition() == smallcaseList.size()) {
            holder.smallcasesContainer.setVisibility(View.GONE);
            holder.seeAllSmallcasesContainer.setVisibility(View.VISIBLE);

            holder.seeAllSmallcasesContainer.setOnClickListener(v -> {
                Intent intent = new Intent(activity, DiscoverSmallcaseActivity.class);
                String value = "";
                if (type == AppConstants.RECENT_SMALLCASES) {
                    intent.putExtra(DiscoverSmallcaseActivity.RECENT, true);
                    value = "See All Recently Added smallcases";
                } else if (type == AppConstants.POPULAR_SMALLCASES) {
                    intent.putExtra(DiscoverSmallcaseActivity.SORT_BY, "popularity");
                    intent.putExtra(DiscoverSmallcaseActivity.POPULARITY, true);
                    value = "See All Popular smallcases";
                }

                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.left_to_right, R.anim.fade_out_with_scale);
            });
            return;
        }

        holder.smallcasesContainer.setVisibility(View.VISIBLE);
        holder.seeAllSmallcasesContainer.setVisibility(View.GONE);

        holder.investedImage.setBackgroundColor(0);
        holder.smallcaseNameLoading.setVisibility(View.GONE);
        holder.smallcaseIndexLoading.setVisibility(View.GONE);

        final PSmallcase smallcase = smallcaseList.get(position);
        holder.smallcaseName.setText(smallcase.getSmallcaseName());
        holder.smallcaseIndex.setText(smallcase.getIndexValue());

        holder.smallcaseCard.setOnClickListener(v -> {
            Intent intent = new Intent(activity, SmallcaseDetailActivity.class);
            intent.putExtra(SmallcaseDetailActivity.SCID, smallcase.getScid());
            intent.putExtra(SmallcaseDetailActivity.TITLE, smallcase.getSmallcaseName());
            intent.putExtra(SmallcaseDetailActivity.ACCESSED_FROM, type == AppConstants.RECENT_SMALLCASES ? "Recent smallcases" : "Popular smallcases");
            activity.startActivity(intent);
        });

        Glide.with(activity)
                .load(smallcase.isIndexUp() ? R.drawable.up_arrow : R.drawable.down_arrow_red)
                .asBitmap()
                .into(holder.upOrDown);
        Glide.with(activity)
                .load(smallcase.getSmallcaseImage())
                .asBitmap()
                .placeholder(ContextCompat.getColor(activity, R.color.grey_300))
                .into(holder.smallcaseImage);

        if (smallcase.getTier() != null && SmallcaseTier.PREMIUM.equals(smallcase.getTier())) {
            holder.premiumIcon.setVisibility(View.VISIBLE);
            Glide.with(activity)
                    .load(R.drawable.premium_icon)
                    .into(holder.premiumIcon);
        } else {
            holder.premiumIcon.setVisibility(View.GONE);
        }

        if (smallcase.isInvested()) {
            holder.investedImage.setVisibility(View.VISIBLE);
            Glide.with(activity)
                    .load(R.drawable.invested_green)
                    .asBitmap()
                    .placeholder(ContextCompat.getColor(activity, R.color.grey_300))
                    .into(holder.investedImage);
        } else {
            holder.investedImage.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (null == smallcaseList || !fetchedRecentSmallcase) return 4;
        return smallcaseList.size() + 1;
    }

    public void onSmallcaseReceived(List<PSmallcase> smallcaseList) {
        this.smallcaseList = new ArrayList<>(smallcaseList);
        fetchedRecentSmallcase = true;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.smallcase_name_loading)
        View smallcaseNameLoading;
        @BindView(R.id.smallcase_index_loading)
        View smallcaseIndexLoading;
        @BindView(R.id.text_smallcase_name)
        GraphikText smallcaseName;
        @BindView(R.id.text_smallcase_index)
        GraphikText smallcaseIndex;
        @BindView(R.id.image_index_up_or_down)
        ImageView upOrDown;
        @BindView(R.id.image_smallcase)
        ImageView smallcaseImage;
        @BindView(R.id.image_invested)
        ImageView investedImage;
        @BindView(R.id.smallcase_container)
        View smallcasesContainer;
        @BindView(R.id.see_all_smallcases_container)
        View seeAllSmallcasesContainer;
        @BindView(R.id.smallcase_card)
        View smallcaseCard;
        @BindView(R.id.premium_icon)
        ImageView premiumIcon;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
