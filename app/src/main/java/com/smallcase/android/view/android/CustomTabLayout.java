package com.smallcase.android.view.android;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;

import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;

/**
 * Created by shashankm on 06/03/17.
 */

public class CustomTabLayout extends TabLayout {
    private static final String TAG = CustomTabLayout.class.getSimpleName();

    private Typeface mTypeface;

    public CustomTabLayout(Context context) {
        super(context);
        init();
    }

    public CustomTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTabLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mTypeface = AppUtils.getInstance().getFont(getContext(), AppConstants.FontType.MEDIUM);
    }

    @Override
    public void setupWithViewPager(ViewPager viewPager) {
        super.setupWithViewPager(viewPager);

        if (mTypeface != null) {
            this.removeAllTabs();

            ViewGroup slidingTabStrip = (ViewGroup) getChildAt(0);

            PagerAdapter adapter = viewPager.getAdapter();

            for (int i = 0, count = adapter.getCount(); i < count; i++) {
                Tab tab = this.newTab();
                this.addTab(tab.setText(adapter.getPageTitle(i)));
                AppCompatTextView view = (AppCompatTextView) ((ViewGroup) slidingTabStrip.getChildAt(i)).getChildAt(1);
                view.setAllCaps(false);
                view.setTypeface(mTypeface, Typeface.NORMAL);
                view.setMaxLines(1);
                view.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            }
        }
    }
}
