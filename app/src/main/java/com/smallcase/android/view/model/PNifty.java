package com.smallcase.android.view.model;

/**
 * Created by shashankm on 07/12/16.
 */

public class PNifty {
    private String index;
    private String change;
    private boolean isUp;
    private String date;

    public PNifty(String index, String change, boolean isUp, String date) {
        this.index = index;
        this.change = change;
        this.isUp = isUp;
        this.date = date;
    }

    public String getIndex() {
        return index;
    }

    public String getChange() {
        return change;
    }

    public boolean isUp() {
        return isUp;
    }

    public String getDate() {
        return date;
    }
}
