package com.smallcase.android.view.android;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * This class is a ScrollListener for RecyclerView that allows to show/hide
 * views when list is scrolled.
 * */

public abstract class DiscoverScrollListener extends RecyclerView.OnScrollListener {
    private static final int HIDE_THRESHOLD = 40;
    private int scrolledDistance = 0;
    private boolean controlsVisible = true;
    private LinearLayoutManager layoutManager;
    private DiscoverScrollListener.ScrollInterface scrollInterface;

    public interface ScrollInterface {
        void onListEndReached();
    }

    protected DiscoverScrollListener(LinearLayoutManager layoutManager, DiscoverScrollListener.ScrollInterface scrollInterface) {
        this.layoutManager = layoutManager;
        this.scrollInterface = scrollInterface;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {
            onHide();
            controlsVisible = false;
            scrolledDistance = 0;
        } else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible) {
            onShow();
            controlsVisible = true;
            scrolledDistance = 0;
        }

        if((controlsVisible && dy>0) || (!controlsVisible && dy<0)) {
            scrolledDistance += dy;
        }

        if (dy > 0) {
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();

            if ((visibleItemCount + pastVisibleItems) >= totalItemCount) scrollInterface.onListEndReached();
        }
    }

    public abstract void onHide();

    public abstract void onShow();

}
