package com.smallcase.android.view.model;

import com.smallcase.android.util.AppConstants;

/**
 * Created by shashankm on 24/01/17.
 */

public class PFeaturedSmallcase {
    private String smallcaseImage;
    private String smallcaseName;
    private String description;
    private String label;
    private String index;
    private String scid;
    private boolean isInvested;
    private boolean isIndexUp;

    public PFeaturedSmallcase(String resolution, String smallcaseName, String description, String label, String
            index, boolean isInvested, boolean isIndexUp, String scid) {
        this.smallcaseName = smallcaseName;
        this.description = description;
        this.label = label;
        this.index = index;
        this.isInvested = isInvested;
        this.isIndexUp = isIndexUp;
        this.scid = scid;
        this.smallcaseImage = AppConstants.SMALLCASE_IMAGE_URL + resolution + "/" + scid + ".png";
    }

    public String getSmallcaseImage() {
        return smallcaseImage;
    }

    public String getSmallcaseName() {
        return smallcaseName;
    }

    public String getDescription() {
        return description;
    }

    public String getLabel() {
        return label;
    }

    public String getIndex() {
        return index;
    }

    public boolean isInvested() {
        return isInvested;
    }

    public boolean isIndexUp() {
        return isIndexUp;
    }

    public String getScid() {
        return scid;
    }
}
