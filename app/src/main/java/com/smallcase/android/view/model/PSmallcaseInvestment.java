package com.smallcase.android.view.model;

import android.text.SpannableString;

/**
 * Created by shashankm on 28/02/17.
 */

public class PSmallcaseInvestment {
    private SpannableString pAndL;
    private String broughtOn;
    private String iScid;
    private boolean isProfit;
    private String source;

    public SpannableString getpAndL() {
        return pAndL;
    }

    public void setpAndL(SpannableString pAndL) {
        this.pAndL = pAndL;
    }

    public String getiScid() {
        return iScid;
    }

    public void setiScid(String iScid) {
        this.iScid = iScid;
    }

    public boolean isProfit() {
        return isProfit;
    }

    public void setProfit(boolean profit) {
        isProfit = profit;
    }

    public String getBroughtOn() {
        return broughtOn;
    }

    public void setBroughtOn(String broughtOn) {
        this.broughtOn = broughtOn;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
