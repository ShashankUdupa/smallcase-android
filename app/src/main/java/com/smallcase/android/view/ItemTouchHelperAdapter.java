package com.smallcase.android.view;

import android.support.annotation.Nullable;

import com.smallcase.android.view.model.Stock;

/**
 * Created by shashankm on 29/09/17.
 */

public interface ItemTouchHelperAdapter {

    void onItemMove(int fromPosition, int toPosition);

    void onItemMoved();

    void onItemDismiss(int position);

    void onFinishedInteraction(@Nullable Stock stock, double oldWeight, double newWeight);
}
