package com.smallcase.android.view.model;

import com.smallcase.android.data.model.StockHistorical;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shashankm on 19/12/16.
 */

public class PStockInfo {
    private String stockName;
    private String sector;
    private String industry;
    private String description;
    private String currentPrice;
    private String oneMonthReturns;
    private String oneYearReturns;
    private String change;
    private String ticker;
    private String fiftyTwoWeekHigh;
    private String fiftyTwoWeekLow;
    private String pAndE;
    private String beta;
    private String divYield;
    private boolean isChangePositive;
    private boolean isOneMonthPositive;
    private boolean isOneYearPositive;
    private List<StockHistorical> stockHistoricals;

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getIndustry() {
        return industry;
    }

    public boolean isOneMonthPositive() {
        return isOneMonthPositive;
    }

    public void setOneMonthPositive(boolean oneMonthPositive) {
        isOneMonthPositive = oneMonthPositive;
    }

    public boolean isOneYearPositive() {
        return isOneYearPositive;
    }

    public void setOneYearPositive(boolean oneYearPositive) {
        isOneYearPositive = oneYearPositive;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(String currentPrice) {
        this.currentPrice = currentPrice;
    }

    public String getOneMonthReturns() {
        return oneMonthReturns;
    }

    public void setOneMonthReturns(String oneMonthReturns) {
        this.oneMonthReturns = oneMonthReturns;
    }

    public String getOneYearReturns() {
        return oneYearReturns;
    }

    public void setOneYearReturns(String oneYearReturns) {
        this.oneYearReturns = oneYearReturns;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public boolean isChangePositive() {
        return isChangePositive;
    }

    public void setChangePositive(boolean changePositive) {
        isChangePositive = changePositive;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getFiftyTwoWeekHigh() {
        return fiftyTwoWeekHigh;
    }

    public void setFiftyTwoWeekHigh(String fiftyTwoWeekHigh) {
        this.fiftyTwoWeekHigh = fiftyTwoWeekHigh;
    }

    public String getFiftyTwoWeekLow() {
        return fiftyTwoWeekLow;
    }

    public void setFiftyTwoWeekLow(String fiftyTwoWeekLow) {
        this.fiftyTwoWeekLow = fiftyTwoWeekLow;
    }

    public String getpAndE() {
        return pAndE;
    }

    public void setpAndE(String pAndE) {
        this.pAndE = pAndE;
    }

    public String getBeta() {
        return beta;
    }

    public void setBeta(String beta) {
        this.beta = beta;
    }

    public String getDivYield() {
        return divYield;
    }

    public void setDivYield(String divYield) {
        this.divYield = divYield;
    }

    public List<StockHistorical> getStockHistoricals() {
        return stockHistoricals;
    }

    public void setStockHistoricals(List<StockHistorical> stockHistoricals) {
        this.stockHistoricals = new ArrayList<>(stockHistoricals);
    }
}
