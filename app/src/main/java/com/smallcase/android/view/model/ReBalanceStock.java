package com.smallcase.android.view.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by shashankm on 03/05/17.
 */

public class ReBalanceStock implements Parcelable {
    private String sid;

    private String stockName;

    private double oldWeight;

    private double newWeight;

    private double oldShares;

    private double newShares;

    public ReBalanceStock() {

    }

    public ReBalanceStock(ReBalanceStock reBalanceStock) {
        sid = reBalanceStock.getSid();
        stockName = reBalanceStock.getStockName();
        oldWeight = reBalanceStock.getOldWeight();
        newWeight = reBalanceStock.getNewWeight();
        oldShares = reBalanceStock.getOldShares();
        newShares = reBalanceStock.getNewShares();
    }

    ReBalanceStock(Parcel in) {
        sid = in.readString();
        stockName = in.readString();
        oldWeight = in.readDouble();
        newWeight = in.readDouble();
        oldShares = in.readDouble();
        newShares = in.readDouble();
    }

    public static final Creator<ReBalanceStock> CREATOR = new Creator<ReBalanceStock>() {
        @Override
        public ReBalanceStock createFromParcel(Parcel in) {
            return new ReBalanceStock(in);
        }

        @Override
        public ReBalanceStock[] newArray(int size) {
            return new ReBalanceStock[size];
        }
    };

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public double getOldWeight() {
        return oldWeight;
    }

    public void setOldWeight(double oldWeight) {
        this.oldWeight = oldWeight;
    }

    public double getNewWeight() {
        return newWeight;
    }

    public void setNewWeight(double newWeight) {
        this.newWeight = newWeight;
    }

    public double getOldShares() {
        return oldShares;
    }

    public void setOldShares(double oldShares) {
        this.oldShares = oldShares;
    }

    public double getNewShares() {
        return newShares;
    }

    public void setNewShares(double newShares) {
        this.newShares = newShares;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sid);
        dest.writeString(stockName);
        dest.writeDouble(oldWeight);
        dest.writeDouble(newWeight);
        dest.writeDouble(oldShares);
        dest.writeDouble(newShares);
    }
}
