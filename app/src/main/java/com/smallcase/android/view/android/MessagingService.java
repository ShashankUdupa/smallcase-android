package com.smallcase.android.view.android;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.investedsmallcase.InvestedSmallcaseDetailActivity;
import com.smallcase.android.investedsmallcase.sip.AllSipsActivity;
import com.smallcase.android.rebalance.ReBalancesAvailableActivity;
import com.smallcase.android.user.ContainerActivity;
import com.smallcase.android.user.WatchlistActivity;
import com.smallcase.android.user.dashboard.BuyReminderActivity;
import com.smallcase.android.user.order.BatchDetailsActivity;
import com.smallcase.android.util.PushNotificationType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import io.intercom.android.sdk.push.IntercomPushClient;
import io.sentry.event.Breadcrumb;

/**
 * Created by shashankm on 22/12/16.
 */

public class MessagingService extends FirebaseMessagingService {
    private static final String TAG = "MessagingService";
    private static int NOTIFICATION_ID = 1;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        IntercomPushClient intercomPushClient = new IntercomPushClient();
        Log.d(TAG, "onMessageReceived: data - " + remoteMessage.getData().toString());
        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, remoteMessage.getData().toString(), null);

        Map<String, String> message = remoteMessage.getData();
        if (intercomPushClient.isIntercomPush(message)) {
            intercomPushClient.handlePush(getApplication(), message);
            return;
        }

        Intent resultIntent;

        if (message.containsKey("type")) {
            switch (message.get("type")) {
                case PushNotificationType.WEEKLY_P_N_L:
                    resultIntent = new Intent(this, SplashActivity.class);
                    resultIntent.putExtra(ContainerActivity.INVESTMENTS, true);
                    break;

                case PushNotificationType.ALERT:
                    try {
                        JSONObject data = new JSONObject(message.get("data"));
                        if (data.getBoolean("internal")) {
                            resultIntent = sendToInternalScreen(data.getString("type"));
                        } else {
                            resultIntent = new Intent(Intent.ACTION_VIEW);
                            resultIntent.setData(Uri.parse(data.getString("url")));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; onMessageReceived()");
                        resultIntent = new Intent(this, SplashActivity.class);
                    }
                    break;

                case PushNotificationType.MARKET_OPEN:
                    resultIntent = new Intent(this, BuyReminderActivity.class);
                    break;

                case PushNotificationType.ORDER_UPDATE:
                    resultIntent = new Intent(this, BatchDetailsActivity.class);
                    try {
                        JSONObject data = new JSONObject(message.get("data"));
                        resultIntent.putExtra(BatchDetailsActivity.SCID, data.getString("scid"));
                        resultIntent.putExtra(BatchDetailsActivity.ISCID, data.getString("iscid"));
                        resultIntent.putExtra(BatchDetailsActivity.SOURCE, data.getString("source"));
                        resultIntent.putExtra(BatchDetailsActivity.SMALLCASE_NAME, data.getString("name"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; onMessageReceived()");
                    }
                    break;

                case PushNotificationType.REBALANCE:
                    resultIntent = new Intent(this, ReBalancesAvailableActivity.class);
                    break;

                case PushNotificationType.PLAYED_OUT:
                    resultIntent = new Intent(this, InvestedSmallcaseDetailActivity.class);
                    try {
                        JSONObject data = new JSONObject(message.get("data"));
                        resultIntent.putExtra(InvestedSmallcaseDetailActivity.SCID, data.getString("scid"));
                        resultIntent.putExtra(InvestedSmallcaseDetailActivity.ISCID, data.getString("iscid"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; onMessageReceived()");
                    }
                    break;

                case PushNotificationType.SIP:
                    resultIntent = new Intent(this, AllSipsActivity.class);
                    break;

                default:
                    resultIntent = new Intent(this, SplashActivity.class);
                    break;
            }
        } else {
            resultIntent = new Intent(this, SplashActivity.class);
        }

        PendingIntent resultPendingIntent = PendingIntent.getActivity(
                this,
                0,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        String groupKey = "";
        if (remoteMessage.getData().containsKey("group_id")) {
            groupKey = remoteMessage.getData().get("group_id");
        }

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.intercom_push_icon) // Should be named the exact name for intercom notification icon to show up (Yeah that's not clean)
                        .setContentTitle(remoteMessage.getData().get("title"))
                        .setContentText(remoteMessage.getData().get("message"))
                        .setContentIntent(resultPendingIntent)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                        .setGroup(groupKey)
                        .setAutoCancel(true);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID++, mBuilder.build());
    }

    private Intent sendToInternalScreen(String type) {
        Intent intent;
        switch (type) {
            case PushNotificationType.ACCOUNT:
                intent = new Intent(this, SplashActivity.class);
                intent.putExtra(ContainerActivity.ACCOUNT, true);
                break;

            case PushNotificationType.WATCH_LIST:
                intent = new Intent(this, WatchlistActivity.class);
                break;

            case PushNotificationType.INVESTMENT_DETAILS:
                intent = new Intent(this, InvestedSmallcaseDetailActivity.class);
                break;

            default:
                intent = new Intent(this, SplashActivity.class);
                break;
        }
        return intent;
    }
}
