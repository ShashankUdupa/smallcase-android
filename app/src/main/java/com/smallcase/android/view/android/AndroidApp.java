package com.smallcase.android.view.android;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.clevertap.android.sdk.ActivityLifecycleCallback;
import com.smallcase.android.BuildConfig;
import com.smallcase.android.analytics.MixPanelAnalytics;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import io.intercom.android.sdk.Intercom;
import io.sentry.Sentry;
import io.sentry.android.AndroidSentryClientFactory;

/**
 * Created by shashankm on 26/12/16.
 */
public class AndroidApp extends MultiDexApplication {
    private static final String TAG = AndroidApp.class.getSimpleName();

    private RefWatcher refWatcher;

    @Override
    public void onCreate() {
        ActivityLifecycleCallback.register(this);
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        refWatcher = LeakCanary.install(this);

        Intercom.initialize(this, BuildConfig.INTERCOM_API_KEY, BuildConfig.INTERCOM_APP_ID);
        Sentry.init(BuildConfig.SENTRY_KEY, new AndroidSentryClientFactory(this));

        //Start frame rate library
        //Takt.stock(this).color(ContextCompat.getColor(this, R.color.primary_text)).play();
    }

    public static RefWatcher getRefWatcher(Context context) {
        AndroidApp application = (AndroidApp) context.getApplicationContext();
        return application.refWatcher;
    }

    @Override
    public void onTerminate() {
        //End it
        //Takt.finish();
        new MixPanelAnalytics(this).flushMixPanel();
        super.onTerminate();
    }
}
