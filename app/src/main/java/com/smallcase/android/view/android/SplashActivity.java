package com.smallcase.android.view.android;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.onboarding.LandingActivity;
import com.smallcase.android.onboarding.OnBoardingActivity;
import com.smallcase.android.user.ContainerActivity;
import com.smallcase.android.user.UserService;

/**
 * Splash screen which purely exists for it's theme that's set in styles.
 */
public class SplashActivity extends AppCompatActivity {
    private static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPrefService sharedPrefService = new SharedPrefService(this);
        Handler handler = new Handler();

        Log.d(TAG, "onCreate: " + sharedPrefService.getAuthorizationToken());
        if (sharedPrefService.getAuthorizationToken() != null) {
            sendUserToHome(sharedPrefService, handler);
            return;
        }

        handler.postDelayed(this::sendUserToLogin, 500);
    }

    private void sendUserToHome(SharedPrefService sharedPrefService, Handler handler) {
        // Call user object
        new UserService().getUserData(new UserRepository(), sharedPrefService, null);

        handler.postDelayed(() -> {
            Intent intent = new Intent(SplashActivity.this, ContainerActivity.class);
            if (getIntent().getData() != null && !getIntent().getData().getPathSegments().isEmpty() &&
                    "investments".equals(getIntent().getData().getPathSegments().get(0))) {
                intent.putExtra(ContainerActivity.INVESTMENTS, true);
            } else {
                intent.putExtra(ContainerActivity.INVESTMENTS, getIntent().getBooleanExtra(ContainerActivity.INVESTMENTS, false));
            }
            startActivity(intent);
            finish();
        }, 500);
    }

    private void sendUserToLogin() {
        SharedPrefService sharedPrefService = new SharedPrefService(this);
        if (sharedPrefService.hasUserViewedOnBoardTutorial()) {
            startActivity(new Intent(SplashActivity.this, LandingActivity.class));
            overridePendingTransition(0, 0);
        } else {
            startActivity(new Intent(SplashActivity.this, OnBoardingActivity.class));
            overridePendingTransition(0, 0);
        }
        finish();
    }
}
