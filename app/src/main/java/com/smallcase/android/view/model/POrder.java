package com.smallcase.android.view.model;

import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.SmallcaseSource;

import java.util.List;

/**
 * Created by shashankm on 07/04/17.
 */

public class POrder {
    private String smallcaseName;

    private String smallcaseImage;

    private String source;

    private String iScid;

    private String scid;

    private List<PBatch> batches;

    public String getSmallcaseName() {
        return smallcaseName;
    }

    public void setSmallcaseName(String smallcaseName) {
        this.smallcaseName = smallcaseName;
    }

    public String getSmallcaseImage() {
        return smallcaseImage;
    }

    public void setSmallcaseImage(String res) {
        this.smallcaseImage = (SmallcaseSource.CREATED.equals(source)? AppConstants.CREATED_SMALLCASE_IMAGE_URL :
            AppConstants.SMALLCASE_IMAGE_URL) + res + "/" + scid + ".png";
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getiScid() {
        return iScid;
    }

    public void setiScid(String iScid) {
        this.iScid = iScid;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    public List<PBatch> getBatches() {
        return batches;
    }

    public void setBatches(List<PBatch> batches) {
        this.batches = batches;
    }
}
