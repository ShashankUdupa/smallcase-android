package com.smallcase.android.view.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smallcase.android.R;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.view.AdapterDelegate;
import com.smallcase.android.view.ItemClickListener;
import com.smallcase.android.view.android.GraphikText;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shashankm on 02/12/16.
 */

public class ExitedSmallcaseAdapterDelegate implements AdapterDelegate<List<Object>> {
    private LayoutInflater inflater;
    private ItemClickListener itemClickListener;

    public ExitedSmallcaseAdapterDelegate(Activity activity, ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
        inflater = activity.getLayoutInflater();
    }

    @Override
    public boolean isForViewType(@NonNull List<Object> items, int position) {
        return items.get(position) instanceof String;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new RealizedReturnViewHolder(inflater.inflate(R.layout.exited_returns_card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull List<Object> items, int position, @NonNull final RecyclerView.ViewHolder holder) {
        String exitedSmallcaseSize = (String) items.get(position);
        RealizedReturnViewHolder viewHolder = (RealizedReturnViewHolder) holder;

        if (exitedSmallcaseSize.equals("1")) {
            viewHolder.realisedPAndLText.setText("You have exited " + exitedSmallcaseSize + " smallcase");
        } else {
            viewHolder.realisedPAndLText.setText("You have exited " + exitedSmallcaseSize + " smallcases");
        }

        viewHolder.realisedPAndLText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClicked(AppConstants.EXITED_SMALLCASES, holder.getAdapterPosition());
            }
        });
    }

    @SuppressWarnings("WeakerAccess")
    class RealizedReturnViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_exited_smallcase_size)
        GraphikText realisedPAndLText;

        RealizedReturnViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
