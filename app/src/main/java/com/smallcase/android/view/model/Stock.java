package com.smallcase.android.view.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by shashankm on 07/03/17.
 */

public class Stock implements Parcelable {
    private String stockName;

    private String sid;

    // Used in create/customize
    private String sector;

    private double price;

    private double weight;

    private double shares;

    // Used for calculating minimum amount
    private double pByW;

    private boolean isBuy;

    private double investmentAmount;

    // Used in order, to check number of orders filled
    private double filled;

    // Used in re-balance
    private double bracketWeight;

    // Used in re-balance
    private double tempWeight;

    // Used in re-balance
    private boolean isWeightFixed;

    // Used in create/customize
    private boolean isLocked;

    // Used in re-balance
    private double prescribedWeight;

    // Used in create/customize
    private double marketCap;

    public Stock() {
        //default constructor
    }

    // To prevent shallow copy
    public Stock(Stock stock) {
        stockName = stock.getStockName();
        sid = stock.getSid();
        price = stock.getPrice();
        weight = stock.getWeight();
        shares = stock.getShares();
        pByW = stock.getpByW();
        isBuy = stock.isBuy();
        investmentAmount = stock.getInvestmentAmount();
        filled = stock.getFilled();
        isLocked = stock.isLocked();
        marketCap = stock.getMarketCap();
        sector = stock.getSector();
    }

    protected Stock(Parcel in) {
        stockName = in.readString();
        sid = in.readString();
        price = in.readDouble();
        weight = in.readDouble();
        shares = in.readDouble();
        isBuy = in.readByte() != 0;
        filled = in.readDouble();
        isLocked = in.readByte() != 0;
        marketCap = in.readDouble();
        sector = in.readString();
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getShares() {
        return shares;
    }

    public void setShares(double shares) {
        this.shares = shares;
    }

    public double getpByW() {
        return pByW;
    }

    public void setpByW(double pByW) {
        this.pByW = pByW;
    }

    public double getInvestmentAmount() {
        return investmentAmount;
    }

    public void setInvestmentAmount(double investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    public boolean isBuy() {
        return isBuy;
    }

    public void setBuy(boolean buy) {
        isBuy = buy;
    }

    public static final Creator<Stock> CREATOR = new Creator<Stock>() {
        @Override
        public Stock createFromParcel(Parcel in) {
            return new Stock(in);
        }

        @Override
        public Stock[] newArray(int size) {
            return new Stock[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(stockName);
        dest.writeString(sid);
        dest.writeDouble(price);
        dest.writeDouble(weight);
        dest.writeDouble(shares);
        dest.writeByte((byte) (isBuy? 1 : 0));
        dest.writeDouble(filled);
        dest.writeByte((byte) (isLocked? 1 : 0));
        dest.writeDouble(marketCap);
        dest.writeString(sector);
    }

    public double getFilled() {
        return filled;
    }

    public void setFilled(double filled) {
        this.filled = filled;
    }

    public double getBracketWeight() {
        return bracketWeight;
    }

    public void setBracketWeight(double bracketWeight) {
        this.bracketWeight = bracketWeight;
    }

    public double getTempWeight() {
        return tempWeight;
    }

    public void setTempWeight(double tempWeight) {
        this.tempWeight = tempWeight;
    }

    public boolean isWeightFixed() {
        return isWeightFixed;
    }

    public void setWeightFixed(boolean weightFixed) {
        isWeightFixed = weightFixed;
    }

    public double getPrescribedWeight() {
        return prescribedWeight;
    }

    public void setPrescribedWeight(double prescribedWeight) {
        this.prescribedWeight = prescribedWeight;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    public double getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(double marketCap) {
        this.marketCap = marketCap;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }
}
