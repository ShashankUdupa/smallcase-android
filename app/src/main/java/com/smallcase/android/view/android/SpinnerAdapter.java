package com.smallcase.android.view.android;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.smallcase.android.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shashankm on 13/01/17.
 */

public class SpinnerAdapter extends ArrayAdapter<String> {
    private List<String> items;

    public SpinnerAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        items = new ArrayList<>(objects);
    }

    @Override
    public View getDropDownView(int position, View aConvertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.heard_from_drop_down, parent, false);

        GraphikText heardFromOptions = (GraphikText) view.findViewById(R.id.text_heard_from_options);
        if (position == 0) {
            heardFromOptions.setTextColor(getContext().getResources().getColor(R.color.secondary_text));
        } else {
            heardFromOptions.setTextColor(getContext().getResources().getColor(R.color.primary_text));
        }

        heardFromOptions.setText(items.get(position));

        return view;
    }

    @Override
    public String getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isEnabled(int position) {
        return position != 0;
    }
}
