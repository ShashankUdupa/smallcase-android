package com.smallcase.android.view.android;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.user.notification.NotificationService;

import io.intercom.android.sdk.push.IntercomPushClient;

/**
 * Created by shashankm on 22/12/16.
 */

public class InstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = InstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        new NotificationService(new UserRepository(), new SharedPrefService(this), null).registerDevice();
        new IntercomPushClient().sendTokenToIntercom(getApplication(), refreshedToken);
    }
}
