package com.smallcase.android.view.model;

import android.text.SpannableString;

import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.SmallcaseSource;

import java.util.HashMap;
import java.util.List;

/**
 * Created by shashankm on 28/02/17.
 */

public class PSmallcaseDetail {
    private String smallcaseImage;
    private String smallcaseName;
    private String smallcaseDescription;
    private String type;
    private String scid;
    private String smallcaseIndex;
    private boolean isIndexUp;
    private boolean isOneYearPositive;
    private String oneYearReturn;
    private boolean isOneMonthPositive;
    private String onMonthReturn;
    private boolean isPlayedOut;
    private String source;
    private String reasonForPlayedOut;
    private String playedOutTitle;
    private boolean isSmallcasesFree;
    private boolean isWatchListed;
    private String rationale;
    private String minAmount;
    private String rebalanceDetail;
    private String nextRebalance;
    private SpannableString smallcaseInceptionInfo;
    private HashMap<String, List<PStockWeight>> segmentMap;

    public String getSmallcaseImage() {
        return smallcaseImage;
    }

    public void setSmallcaseImage(String res, String source, String scid) {
        this.scid = scid;
        this.smallcaseImage = (SmallcaseSource.CREATED.equals(source) ? AppConstants.CREATED_SMALLCASE_IMAGE_URL
                : AppConstants.SMALLCASE_IMAGE_URL) + res + "/" + scid + ".png";
    }

    public String getSmallcaseName() {
        return smallcaseName;
    }

    public void setSmallcaseName(String smallcaseName) {
        this.smallcaseName = smallcaseName;
    }

    public String getSmallcaseDescription() {
        return smallcaseDescription;
    }

    public void setSmallcaseDescription(String smallcaseDescription) {
        this.smallcaseDescription = smallcaseDescription;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSmallcaseIndex() {
        return smallcaseIndex;
    }

    public void setSmallcaseIndex(String smallcaseIndex) {
        this.smallcaseIndex = smallcaseIndex;
    }

    public boolean isIndexUp() {
        return isIndexUp;
    }

    public void setIndexUp(boolean indexUp) {
        isIndexUp = indexUp;
    }

    public String getOneYearReturn() {
        return oneYearReturn;
    }

    public void setOneYearReturn(String oneYearReturn) {
        this.oneYearReturn = oneYearReturn;
    }

    public String getOnMonthReturn() {
        return onMonthReturn;
    }

    public void setOnMonthReturn(String onMonthReturn) {
        this.onMonthReturn = onMonthReturn;
    }

    public boolean isPlayedOut() {
        return isPlayedOut;
    }

    public void setPlayedOut(boolean playedOut) {
        isPlayedOut = playedOut;
    }

    public String getReasonForPlayedOut() {
        return reasonForPlayedOut;
    }

    public void setReasonForPlayedOut(String reasonForPlayedOut) {
        this.reasonForPlayedOut = reasonForPlayedOut;
    }

    public boolean isSmallcasesFree() {
        return isSmallcasesFree;
    }

    public void setSmallcasesFree(boolean smallcasesFree) {
        isSmallcasesFree = smallcasesFree;
    }

    public boolean isWatchListed() {
        return isWatchListed;
    }

    public void setWatchListed(boolean watchListed) {
        isWatchListed = watchListed;
    }

    public String getRationale() {
        return rationale;
    }

    public void setRationale(String rationale) {
        this.rationale = rationale;
    }

    public String getScid() {
        return scid;
    }

    public String getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(String minAmount) {
        this.minAmount = minAmount;
    }

    public boolean isOneYearPositive() {
        return isOneYearPositive;
    }

    public void setOneYearPositive(boolean oneYearPositive) {
        isOneYearPositive = oneYearPositive;
    }

    public boolean isOneMonthPositive() {
        return isOneMonthPositive;
    }

    public void setOneMonthPositive(boolean oneMonthPositive) {
        isOneMonthPositive = oneMonthPositive;
    }

    public String getPlayedOutTitle() {
        return playedOutTitle;
    }

    public void setPlayedOutTitle(String playedOutTitle) {
        this.playedOutTitle = playedOutTitle;
    }

    public HashMap<String, List<PStockWeight>> getSegmentMap() {
        return segmentMap;
    }

    public void setSegmentMap(HashMap<String, List<PStockWeight>> segmentMap) {
        this.segmentMap = segmentMap;
    }

    public String getRebalanceDetail() {
        return rebalanceDetail;
    }

    public void setRebalanceDetail(String rebalanceDetail) {
        this.rebalanceDetail = rebalanceDetail;
    }

    public String getNextRebalance() {
        return nextRebalance;
    }

    public void setNextRebalance(String nextRebalance) {
        this.nextRebalance = nextRebalance;
    }

    public SpannableString getSmallcaseInceptionInfo() {
        return smallcaseInceptionInfo;
    }

    public void setSmallcaseInceptionInfo(SpannableString smallcaseInceptionInfo) {
        this.smallcaseInceptionInfo = smallcaseInceptionInfo;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
