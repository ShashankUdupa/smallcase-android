package com.smallcase.android.view.model;

import java.util.Date;

/**
 * Created by shashankm on 02/03/17.
 */

public class PPoints {
    private String date;

    private double smallcaseIndex;

    private Date originalDate;

    private double niftyIndex;

    private double originalSmallcaseIndex;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getSmallcaseIndex() {
        return smallcaseIndex;
    }

    public void setSmallcaseIndex(double smallcaseIndex) {
        this.smallcaseIndex = smallcaseIndex;
    }

    public double getNiftyIndex() {
        return niftyIndex;
    }

    public void setNiftyIndex(double niftyIndex) {
        this.niftyIndex = niftyIndex;
    }

    public double getOriginalSmallcaseIndex() {
        return originalSmallcaseIndex;
    }

    public void setOriginalSmallcaseIndex(double originalSmallcaseIndex) {
        this.originalSmallcaseIndex = originalSmallcaseIndex;
    }

    public Date getOriginalDate() {
        return originalDate;
    }

    public void setOriginalDate(Date originalDate) {
        this.originalDate = originalDate;
    }
}