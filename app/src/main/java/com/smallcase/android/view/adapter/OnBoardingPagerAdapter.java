package com.smallcase.android.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.POnboarding;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shashankm on 09/08/17.
 */

public class OnBoardingPagerAdapter extends PagerAdapter {

    private Activity activity;
    private List<POnboarding> pOnboardingList;
    private LayoutInflater layoutInflater;

    public OnBoardingPagerAdapter(Activity activity){
        this.activity = activity;

        layoutInflater = (LayoutInflater) activity.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        pOnboardingList = new ArrayList<>();
        POnboarding firstSlide = new POnboarding(R.drawable.onboarding_0, "Welcome to smallcase,\nstock investing for everyone",
                "A new, simple & healthy way to invest in the right stocks for the long term");
        POnboarding secondSlide = new POnboarding(R.drawable.onboarding_1, "Investing made smart\nand simple",
                "smallcases are curated baskets of stocks \n" + "that give you exposure to easy to \n" + "understand ideas & strategies");
        POnboarding thirdSlide = new POnboarding(R.drawable.onboarding_2, "Invest in what matters \nto you",
                "Choose from 60+ professionally \n" + "built smallcases or create your\n" + "own easily");
        POnboarding fourthSlide = new POnboarding(R.drawable.onboarding_3, "Modern, cost effective\ninvesting on the go",
                "Buy, Track and Manage your \n" + "investments from anywhere for a \n" + "one-time fee of ₹100 per smallcase");
        pOnboardingList.add(firstSlide);
        pOnboardingList.add(secondSlide);
        pOnboardingList.add(thirdSlide);
        pOnboardingList.add(fourthSlide);
    }

    @Override
    public int getCount() {
        return pOnboardingList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = layoutInflater.inflate(R.layout.onboarding_layout, container, false);

        ImageView illustration = (ImageView) view.findViewById(R.id.illustration);
        GraphikText onboardingTitle = (GraphikText) view.findViewById(R.id.onboarding_title);
        GraphikText onboardingDescription = (GraphikText) view.findViewById(R.id.onboarding_description);

        POnboarding pOnboarding = pOnboardingList.get(position);
        Glide.with(activity)
                .load(pOnboarding.getImageResId())
                .into(illustration);
        onboardingTitle.setText(pOnboarding.getTitle());
        onboardingDescription.setText(pOnboarding.getDescription());

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }

}
