package com.smallcase.android.view.model;

/**
 * Created by shashankm on 11/04/17.
 */

public class PSmallcaseFees {
    private String smallcaseName;

    private String iScid;

    private String scid;

    private String feesPaid;

    private String broughtOn;

    public String getSmallcaseName() {
        return smallcaseName;
    }

    public void setSmallcaseName(String smallcaseName) {
        this.smallcaseName = smallcaseName;
    }

    public String getiScid() {
        return iScid;
    }

    public void setiScid(String iScid) {
        this.iScid = iScid;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    public String getFeesPaid() {
        return feesPaid;
    }

    public void setFeesPaid(String feesPaid) {
        this.feesPaid = feesPaid;
    }

    public String getBroughtOn() {
        return broughtOn;
    }

    public void setBroughtOn(String broughtOn) {
        this.broughtOn = broughtOn;
    }
}
