package com.smallcase.android.view.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by shashankm on 14/12/16.
 */

public class PStock implements Parcelable{
    private String stockName;

    private double stockWeightage;

    private double stockPAndL;

    private boolean isProfit;

    private boolean isHeading;

    private boolean sortPAndL;

    private boolean sortWeightage;

    private String sid;

    private double shares;

    private String averageBuyPrice;

    private String currentPrice;

    private boolean isExpanded;

    public PStock() {

    }

    protected PStock(Parcel in) {
        stockName = in.readString();
        stockWeightage = in.readDouble();
        stockPAndL = in.readDouble();
        isProfit = in.readByte() != 0;
        isHeading = in.readByte() != 0;
        sortPAndL = in.readByte() != 0;
        sortWeightage = in.readByte() != 0;
        sid = in.readString();
        shares = in.readDouble();
    }

    public static final Creator<PStock> CREATOR = new Creator<PStock>() {
        @Override
        public PStock createFromParcel(Parcel in) {
            return new PStock(in);
        }

        @Override
        public PStock[] newArray(int size) {
            return new PStock[size];
        }
    };

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public double getStockWeightage() {
        return stockWeightage;
    }

    public void setStockWeightage(double stockWeightage) {
        this.stockWeightage = stockWeightage;
    }

    public double getStockPAndL() {
        return stockPAndL;
    }

    public void setStockPAndL(double stockPAndL) {
        this.stockPAndL = stockPAndL;
    }

    public boolean isProfit() {
        return isProfit;
    }

    public void setProfit(boolean profit) {
        isProfit = profit;
    }

    public boolean isHeading() {
        return isHeading;
    }

    public void setHeading(boolean heading) {
        isHeading = heading;
    }

    public boolean isSortPAndL() {
        return sortPAndL;
    }

    public void setSortPAndL(boolean sortPAndL) {
        this.sortPAndL = sortPAndL;
    }

    public boolean isSortWeightage() {
        return sortWeightage;
    }

    public void setSortWeightage(boolean sortWeightage) {
        this.sortWeightage = sortWeightage;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public double getShares() {
        return shares;
    }

    public void setShares(double shares) {
        this.shares = shares;
    }

    public String getAverageBuyPrice() {
        return averageBuyPrice;
    }

    public void setAverageBuyPrice(String averageBuyPrice) {
        this.averageBuyPrice = averageBuyPrice;
    }

    public String getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(String currentPrice) {
        this.currentPrice = currentPrice;
    }

    public static Creator<PStock> getCREATOR() {
        return CREATOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(stockName);
        dest.writeDouble(stockWeightage);
        dest.writeDouble(stockPAndL);
        dest.writeByte((byte) (isProfit ? 1 : 0));
        dest.writeByte((byte) (isHeading ? 1 : 0));
        dest.writeByte((byte) (sortPAndL ? 1 : 0));
        dest.writeByte((byte) (sortWeightage ? 1 : 0));
        dest.writeString(sid);
        dest.writeDouble(shares);
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }
}
