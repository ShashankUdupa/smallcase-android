package com.smallcase.android.view;

import android.support.annotation.NonNull;
import android.support.v4.util.SparseArrayCompat;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * AdapterDelegateManager is responsible for managing classes which implement
 * {@link AdapterDelegate}.
 *
 * <p>
 * Classes which extend RecyclerView.Adapter and want to declare multiple view types
 * need to create an AdapterDelegateManager object and add delegates by calling
 * {@link #addDelegate(AdapterDelegate)} method.
 * </p>
 *
 * <p>
 * The following methods need to be called from the AdapterDelegateManager object:
 * <ul>
 * <li>{@link #getItemViewType(Object, int)}: Must be called from {@link
 * RecyclerView.Adapter#getItemViewType(int)}</li>
 * <li> {@link #onCreateViewHolder(ViewGroup, int)}: Must be called from {@link
 * RecyclerView.Adapter#onCreateViewHolder(ViewGroup, int)}</li>
 * <li> {@link #onBindViewHolder(Object, int, RecyclerView.ViewHolder)}: Must be called from {@link
 * RecyclerView.Adapter#onBindViewHolder(RecyclerView.ViewHolder, int)}</li>
 * </ul>
 * </p>
 *
 * @param <T> the type of data source for the required adapter.
 * @author shashankm
 */

public class AdapterDelegateManager<T> {
    private static final String TAG = AdapterDelegateManager.class.getSimpleName();
    private SparseArrayCompat<AdapterDelegate<T>> delegates = new SparseArrayCompat<>();

    /**
     * Adds an {@link AdapterDelegate}.
     * This automatically adds a key(view type) to the delegate.
     * @param delegate that needs to be added.
     * @return AdapterDelegateManager<T> object to let further addition of delegates.
     */
    public AdapterDelegateManager<T> addDelegate(@NonNull AdapterDelegate<T> delegate) {
        int viewType = delegates.size();
        delegates.put(viewType, delegate);

        return this;
    }

    /**
     * Loops through existing delegates and returns view type of required delegate.
     * @param items data source of adapter.
     * @param position in adapter's data source.
     * @return view type of delegate.
     * @throws NullPointerException if delegate is not added using
     * {@link #addDelegate(AdapterDelegate)}
     */
    public int getItemViewType(@NonNull T items, int position) {
        int delegatesCount = delegates.size();
        for (int i = 0; i < delegatesCount; i++) {
            if (delegates.valueAt(i).isForViewType(items, position)) {
                return delegates.keyAt(i);
            }
        }

        throw new NullPointerException("No AdapterDelegate added that matches position = "
                + position + " in data source");
    }

    /**
     * Checks for delegate using given view type and calls the delegate's onCreateViewHolder.
     * @param parent view no_investment of adapter.
     * @param viewType of delegate.
     * @return view holder for recycler view.
     * @throws NullPointerException if view type does not exist. {@link
     * #addDelegate(AdapterDelegate)} needs to be called onCreateViewHolder(ViewGroup, int).
     */
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        AdapterDelegate<T> delegate = getDelegateForViewType(viewType);
        if (delegate == null) {
            throw new NullPointerException("No delegate found for view type = " + viewType);
        }

        return delegate.onCreateViewHolder(parent);
    }

    /**
     * Called for each item in items. Finds delegate based on view type and passes
     * responsibility of binding views to that delegate.
     * @param items data source of adapter.
     * @param position in adapter's data source.
     * @param viewHolder to bind.
     * @throws NullPointerException if view type does not exist.
     */
    public void onBindViewHolder(@NonNull T items, int position, @NonNull RecyclerView
            .ViewHolder viewHolder) {
        AdapterDelegate<T> delegate = getDelegateForViewType(viewHolder.getItemViewType());
        if (delegate == null) {
            throw new NullPointerException("No delegate found for "
                    + viewHolder
                    + " for item at position = "
                    + viewHolder.getAdapterPosition()
                    + " for viewType = "
                    + viewHolder.getItemViewType());
        }
        delegate.onBindViewHolder(items, position, viewHolder);
    }

    private AdapterDelegate<T> getDelegateForViewType(int viewType) {
        return delegates.get(viewType);
    }
}
