package com.smallcase.android.view.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.smallcase.android.R;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PInvestedSmallcase;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shashankm on 30/01/17.
 */

public class DialogAdapter extends RecyclerView.Adapter<DialogAdapter.ViewHolder> {
    private List<PInvestedSmallcase> investedSmallcases;
    private Activity activity;

    public DialogAdapter(Activity activity, List<PInvestedSmallcase> investedSmallcases) {
        this.activity = activity;
        this.investedSmallcases = new ArrayList<>(investedSmallcases);
    }

    @Override
    public DialogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invested_smallcase_news_card, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DialogAdapter.ViewHolder holder, int position) {
        PInvestedSmallcase investedSmallcase = investedSmallcases.get(position);

        holder.smallcaseNameLoading.setVisibility(View.GONE);
        Glide.with(activity)
                .load(investedSmallcase.getSmallcaseImage())
                .asBitmap()
                .placeholder(activity.getResources().getColor(R.color.grey_300))
                .into(new BitmapImageViewTarget(holder.smallcaseImage) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable roundImage = RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                        roundImage.setCornerRadius(AppUtils.getInstance().dpToPx(2));
                        holder.smallcaseImage.setImageDrawable(roundImage);
                    }
                });
        holder.smallcaseName.setText(investedSmallcase.getSmallcaseName());
        holder.indexValue.setText(investedSmallcase.getSmallcaseIndex());
        Glide.with(activity)
                .load(investedSmallcase.isIndexUp()? R.drawable.up_arrow: R.drawable.down_arrow_red)
                .asBitmap()
                .into(holder.upOrDownIndex);
    }

    @Override
    public int getItemCount() {
        return investedSmallcases.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_invested_smallcase)
        ImageView smallcaseImage;
        @BindView(R.id.smallcase_name_loading)
        View smallcaseNameLoading;
        @BindView(R.id.text_invested_smallcase)
        GraphikText smallcaseName;
        @BindView(R.id.text_index_value_invested)
        GraphikText indexValue;
        @BindView(R.id.up_or_down_since_news)
        ImageView upOrDownIndex;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
