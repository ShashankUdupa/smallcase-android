package com.smallcase.android.view.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.investedsmallcase.InvestedSmallcaseDetailActivity;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PInvestedSmallcase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shashankm on 02/12/16.
 */

public class InvestedSmallcasesAdapter extends RecyclerView.Adapter<InvestedSmallcasesAdapter.ViewHolder> {
    private static final String TAG = InvestedSmallcasesAdapter.class.getSimpleName();

    private Activity activity;
    private List<PInvestedSmallcase> investedSmallcaseList;
    private boolean fetchedInvestments = false;
    private Animations animations;
    private AnalyticsContract analyticsContract;
    private final float TWO_DP = AppUtils.getInstance().dpToPx(2);

    public InvestedSmallcasesAdapter(Activity activity, AnalyticsContract analyticsContract) {
        this.activity = activity;
        this.analyticsContract = analyticsContract;
        animations = new Animations();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invested_smallcases_card, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {
        if (!fetchedInvestments) {
            animations.loadingContent(viewHolder.smallcaseNameLoading);
            return;
        }

        animations.stopAnimation();
        final PInvestedSmallcase investedSmallcase = investedSmallcaseList.get(viewHolder.getAdapterPosition());

        viewHolder.investedSmallcaseImage.setBackgroundColor(0);

        toggleViews(View.GONE, viewHolder.smallcaseNameLoading);
        toggleViews(View.VISIBLE, viewHolder.pAndLPercent, viewHolder.smallcaseNetworth, viewHolder.investedSmallcaseName);

        Glide.with(activity)
                .load(investedSmallcase.getSmallcaseImage())
                .asBitmap()
                .placeholder(activity.getResources().getDrawable(R.color.grey_300))
                .into(new BitmapImageViewTarget(viewHolder.investedSmallcaseImage) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable roundImage = RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                        roundImage.setCornerRadius(TWO_DP);
                        viewHolder.investedSmallcaseImage.setImageDrawable(roundImage);
                    }
                });

        if (SmallcaseSource.CUSTOM.equals(investedSmallcase.getSource())) {
            viewHolder.customTag.setVisibility(View.VISIBLE);
        } else {
            viewHolder.customTag.setVisibility(View.GONE);
        }

        viewHolder.smallcaseCard.setBackgroundColor(ContextCompat.getColor(activity, android.R.color.transparent));

        viewHolder.investedSmallcaseName.setText(investedSmallcase.getSmallcaseName());
        viewHolder.pAndLPercent.setText(investedSmallcase.getPercentage());
        viewHolder.smallcaseNetworth.setText(investedSmallcase.getNetWorth());

        viewHolder.smallcaseCard.setOnClickListener(v -> {
            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("accessedFrom", "Investments");
            analyticsContract.sendEvent(eventProperties, "Viewed Investment Details", Analytics.MIXPANEL, Analytics.CLEVERTAP);

            Intent intent = new Intent(activity, InvestedSmallcaseDetailActivity.class);
            intent.putExtra(InvestedSmallcaseDetailActivity.SCID, investedSmallcase.getScid());
            intent.putExtra(InvestedSmallcaseDetailActivity.ISCID, investedSmallcase.getiScid());
            intent.putExtra(InvestedSmallcaseDetailActivity.SOURCE, investedSmallcase.getSource());
            activity.startActivity(intent);
        });

        if (position < investedSmallcaseList.size() - 1)
            viewHolder.divider.setVisibility(View.VISIBLE);
        else viewHolder.divider.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        //Return loading items if myInvestments haven't been fetched yet
        if (null == investedSmallcaseList || !fetchedInvestments) {
            return 1;
        }
        return investedSmallcaseList.size();
    }

    public boolean didFetchInvestments() {
        return fetchedInvestments;
    }

    public void onFetchedInvestments(List<PInvestedSmallcase> investedSmallcaseList) {
        this.investedSmallcaseList = new ArrayList<>(investedSmallcaseList);
        fetchedInvestments = true;
        notifyDataSetChanged();
    }

    public void updateItems(List<PInvestedSmallcase> myInvestmentItems) {
        this.investedSmallcaseList.clear();
        this.investedSmallcaseList = myInvestmentItems;
        notifyDataSetChanged();
    }

    private void toggleViews(int visibility, View... views) {
        for (View view : views) {
            view.setVisibility(visibility);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_smallcase_networth)
        GraphikText smallcaseNetworth;
        @BindView(R.id.text_return_percentage)
        GraphikText pAndLPercent;
        @BindView(R.id.image_invested_smallcase)
        ImageView investedSmallcaseImage;
        @BindView(R.id.text_invested_smallcase)
        GraphikText investedSmallcaseName;
        @BindView(R.id.invested_smallcase_card)
        CardView smallcaseCard;
        @BindView(R.id.divider)
        View divider;
        @BindView(R.id.smallcase_name_loading)
        View smallcaseNameLoading;
        @BindView(R.id.custom_tag)
        View customTag;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
