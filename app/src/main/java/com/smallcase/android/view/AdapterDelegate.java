package com.smallcase.android.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;


/**
 * Provides mechanism for hooking up delegates to {@link RecyclerView.Adapter}.
 * {@link AdapterDelegateManager} acts as a manager for enforcing this "hook".
 *
 * @param <T> Data source type of the delegate.
 * @author shashankm
 */

public interface AdapterDelegate<T> {
    /**
     * Called to determine whether this AdapterDelegate is the responsible for the given data
     * element.
     *
     * @param items data source of adapter.
     * @param position of the data source
     * @return true if it's responsible, false otherwise.
     */
    boolean isForViewType(@NonNull T items, int position);

    /**
     * Creates the  {@link RecyclerView.ViewHolder} for the given data source item
     *
     * @param parent view no_investment of given data source.
     * @return new instance of {@link RecyclerView.ViewHolder}
     */
    @NonNull RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent);

    /**
     * Called to bind and display data from an adapter's data source.
     *
     * @param items data source of adapter.
     * @param position in data source.
     * @param holder instance of {@link RecyclerView.ViewHolder} which is used to bind item in
     * data source.
     */
    void onBindViewHolder(@NonNull T items, int position, @NonNull RecyclerView
            .ViewHolder holder);
}
