package com.smallcase.android.view.android;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.AttributeSet;

import com.smallcase.android.R;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;

/**
 * This text view should be used throughout the app. It applies Graphik font to a given text view.
 * The available font types are {@link AppConstants.FontType}. Font type can be applied using app:font
 * for Graphik text view. If no font is supplied {@link AppConstants.FontType REGULAR} is used.
 */
public class GraphikText extends android.support.v7.widget.AppCompatTextView {

    public GraphikText(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context, attrs);
    }

    public GraphikText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context, attrs);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        int fontType = typedArray.getInt(R.styleable.CustomFontTextView_fontStyleName, 0);
        float letterSpacing = typedArray.getDimension(R.styleable.CustomFontTextView_letterSpacing, 0f);

        Typeface customFont = selectTypeface(context, fontType);
        setTypeface(customFont);

        if (Build.VERSION.SDK_INT >= 21) {
            setLetterSpacing(letterSpacing);
        }

        setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
        typedArray.recycle();
    }

    /**
     * This is for applying custom font dynamically
     * @param context of activity
     * @param fontType 0: regular, 1: bold, 2: light, 3: medium, 4: medium-italic,
     *                 5: regular-italic, 6: semi-bold
     */
    public void applyCustomFont(Context context, int fontType) {
        Typeface customFont = selectTypeface(context, fontType);
        setTypeface(customFont);
        setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    @NonNull
    private Typeface selectTypeface(Context context, int fontType) {
        switch (fontType) {
            case 0:
                return AppUtils.getInstance().getFont(context, AppConstants.FontType.REGULAR);

            case 1:
                return AppUtils.getInstance().getFont(context, AppConstants.FontType.BOLD);

            case 2:
                return AppUtils.getInstance().getFont(context, AppConstants.FontType.LIGHT);

            case 3:
                return AppUtils.getInstance().getFont(context, AppConstants.FontType.MEDIUM);

            case 4:
                return AppUtils.getInstance().getFont(context, AppConstants.FontType.MEDIUM_ITALIC);

            case 5:
                return AppUtils.getInstance().getFont(context, AppConstants.FontType.REGULAR_ITALIC);

            case 6:
                return AppUtils.getInstance().getFont(context, AppConstants.FontType.SEMI_BOLD);

            default:
                return AppUtils.getInstance().getFont(context, AppConstants.FontType.REGULAR);
        }
    }
}
