package com.smallcase.android.view.model;

import java.util.List;

/**
 * Created by shashankm on 07/04/17.
 */

public class PBatch {
    private String batchType;

    private String placedOn;

    private String orderStatus;

    private String filledVsQuantity;

    private List<POrderStock> stocks;

    private String buyValue;

    private String sellValue;

    private int percentFilled;

    private String batchId;

    private boolean shouldShowOrders;

    public String getBatchType() {
        return batchType;
    }

    public void setBatchType(String batchType) {
        this.batchType = batchType;
    }

    public String getPlacedOn() {
        return placedOn;
    }

    public void setPlacedOn(String placedOn) {
        this.placedOn = placedOn;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getFilledVsQuantity() {
        return filledVsQuantity;
    }

    public void setFilledVsQuantity(String filledVsQuantity) {
        this.filledVsQuantity = filledVsQuantity;
    }

    public List<POrderStock> getStocks() {
        return stocks;
    }

    public void setStocks(List<POrderStock> stocks) {
        this.stocks = stocks;
    }

    public String getBuyValue() {
        return buyValue;
    }

    public void setBuyValue(String buyValue) {
        this.buyValue = buyValue;
    }

    public String getSellValue() {
        return sellValue;
    }

    public void setSellValue(String sellValue) {
        this.sellValue = sellValue;
    }

    public int getPercentFilled() {
        return percentFilled;
    }

    public void setPercentFilled(int percentFilled) {
        this.percentFilled = percentFilled;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public boolean isShouldShowOrders() {
        return shouldShowOrders;
    }

    public void setShouldShowOrders(boolean shouldShowOrders) {
        this.shouldShowOrders = shouldShowOrders;
    }
}
