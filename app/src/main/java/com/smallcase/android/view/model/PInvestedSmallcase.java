package com.smallcase.android.view.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannableString;

import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.SmallcaseSource;

/**
 * Created by shashankm on 08/12/16.
 */

public class PInvestedSmallcase implements Parcelable {
    private final String date;
    private String smallcaseName;
    private String smallcaseImage;
    private String smallcaseIndex;
    private boolean isIndexUp;
    private String netWorth;
    private SpannableString percentage;
    private boolean isProfit;
    private String scid;
    private String iScid;
    private String source;

    public PInvestedSmallcase(String smallcaseName, String source, String resolution, String smallcaseIndex, boolean isIndexUp, String
            netWorth, SpannableString percentage, boolean isProfit, String scid, String date, String iScid) {
        this.smallcaseName = smallcaseName;
        this.source = source;
        this.smallcaseImage = (SmallcaseSource.CREATED.equals(source)? AppConstants
                .CREATED_SMALLCASE_IMAGE_URL : AppConstants.SMALLCASE_IMAGE_URL) + resolution + "/" + scid + ".png";
        this.smallcaseIndex = smallcaseIndex;
        this.isIndexUp = isIndexUp;
        this.netWorth = netWorth;
        this.percentage = percentage;
        this.isProfit = isProfit;
        this.scid = scid;
        this.date = date;
        this.iScid = iScid;
    }

    public PInvestedSmallcase(Parcel in) {
        date = in.readString();
        smallcaseName = in.readString();
        smallcaseImage = in.readString();
        smallcaseIndex = in.readString();
        isIndexUp = in.readByte() != 0;
        netWorth = in.readString();
        percentage = SpannableString.valueOf(in.readString());
        isProfit = in.readByte() != 0;
        scid = in.readString();
        iScid = in.readString();
        source = in.readString();
    }

    public static final Creator<PInvestedSmallcase> CREATOR = new Creator<PInvestedSmallcase>() {
        @Override
        public PInvestedSmallcase createFromParcel(Parcel in) {
            return new PInvestedSmallcase(in);
        }

        @Override
        public PInvestedSmallcase[] newArray(int size) {
            return new PInvestedSmallcase[size];
        }
    };

    public String getSmallcaseName() {
        return smallcaseName;
    }

    public String getSmallcaseImage() {
        return smallcaseImage;
    }

    public String getSmallcaseIndex() {
        return smallcaseIndex;
    }

    public boolean isIndexUp() {
        return isIndexUp;
    }

    public String getNetWorth() {
        return netWorth;
    }

    public SpannableString getPercentage() {
        return percentage;
    }

    public boolean isProfit() {
        return isProfit;
    }

    public String getScid() {
        return scid;
    }

    public String getDate() {
        return date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeString(smallcaseName);
        dest.writeString(smallcaseImage);
        dest.writeString(smallcaseIndex);
        dest.writeByte((byte) (isIndexUp ? 1 : 0));
        dest.writeString(netWorth);
        dest.writeString(String.valueOf(percentage));
        dest.writeByte((byte) (isProfit ? 1 : 0));
        dest.writeString(scid);
        dest.writeString(iScid);
        dest.writeString(source);
    }

    public String getiScid() {
        return iScid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
