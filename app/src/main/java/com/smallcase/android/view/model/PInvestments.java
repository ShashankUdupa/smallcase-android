package com.smallcase.android.view.model;

/**
 * Created by shashankm on 08/12/16.
 */

public class PInvestments {
    private String currentValue;

    private String currentInvestment;

    private String currentPAndL;

    private String currentPAndLPercent;

    private boolean isCurrentPAndLProfit;

    private String totalPAndL;

    private String totalPAndLPercent;

    private boolean isTotalPAndLProfit;

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

    public String getCurrentInvestment() {
        return currentInvestment;
    }

    public void setCurrentInvestment(String currentInvestment) {
        this.currentInvestment = currentInvestment;
    }

    public String getCurrentPAndL() {
        return currentPAndL;
    }

    public void setCurrentPAndL(String currentPAndL) {
        this.currentPAndL = currentPAndL;
    }

    public String getCurrentPAndLPercent() {
        return currentPAndLPercent;
    }

    public void setCurrentPAndLPercent(String currentPAndLPercent) {
        this.currentPAndLPercent = currentPAndLPercent;
    }

    public boolean isCurrentPAndLProfit() {
        return isCurrentPAndLProfit;
    }

    public void setCurrentPAndLProfit(boolean currentPAndLProfit) {
        isCurrentPAndLProfit = currentPAndLProfit;
    }

    public String getTotalPAndL() {
        return totalPAndL;
    }

    public void setTotalPAndL(String totalPAndL) {
        this.totalPAndL = totalPAndL;
    }

    public String getTotalPAndLPercent() {
        return totalPAndLPercent;
    }

    public void setTotalPAndLPercent(String totalPAndLPercent) {
        this.totalPAndLPercent = totalPAndLPercent;
    }

    public boolean isTotalPAndLProfit() {
        return isTotalPAndLProfit;
    }

    public void setTotalPAndLProfit(boolean totalPAndLProfit) {
        isTotalPAndLProfit = totalPAndLProfit;
    }
}
