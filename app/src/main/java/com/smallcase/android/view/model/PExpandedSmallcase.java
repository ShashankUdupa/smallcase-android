package com.smallcase.android.view.model;

import com.smallcase.android.util.AppConstants;

/**
 * Created by shashankm on 03/02/17.
 */

public class PExpandedSmallcase {
    private String smallcaseImage;
    private String smallcaseName;
    private String smallcaseDescription;
    private String minAmount;
    private String indexValue;
    private boolean isIndexUp;
    private String oneMonth;
    private String oneYear;
    private boolean isMonthPositive;
    private boolean isYearPositive;
    private String scid;
    private boolean isInvested;
    private String tier;

    public PExpandedSmallcase(String res, String smallcaseName, String smallcaseDescription, String minAmount, String
            indexValue, boolean isIndexUp, String oneMonth, String oneYear, boolean isMonthPositive, boolean
            isYearPositive, String scid, boolean isInvested, String tier) {
        this.scid = scid;
        this.smallcaseImage = AppConstants.SMALLCASE_IMAGE_URL + res + "/" + scid + ".png";
        this.smallcaseName = smallcaseName;
        this.smallcaseDescription = smallcaseDescription;
        this.minAmount = minAmount;
        this.indexValue = indexValue;
        this.isIndexUp = isIndexUp;
        this.oneMonth = oneMonth;
        this.oneYear = oneYear;
        this.isMonthPositive = isMonthPositive;
        this.isYearPositive = isYearPositive;
        this.isInvested = isInvested;
        this.tier = tier;
    }

    public String getSmallcaseImage() {
        return smallcaseImage;
    }

    public String getSmallcaseName() {
        return smallcaseName;
    }

    public String getSmallcaseDescription() {
        return smallcaseDescription;
    }

    public String getMinAmount() {
        return minAmount;
    }

    public String getIndexValue() {
        return indexValue;
    }

    public boolean isIndexUp() {
        return isIndexUp;
    }

    public String getOneMonth() {
        return oneMonth;
    }

    public String getOneYear() {
        return oneYear;
    }

    public boolean isMonthPositive() {
        return isMonthPositive;
    }

    public boolean isYearPositive() {
        return isYearPositive;
    }

    public String getScid() {
        return scid;
    }

    public boolean isInvested() {
        return isInvested;
    }

    public void setInvested(boolean invested) {
        isInvested = invested;
    }

    public String getTier() {
        return tier;
    }
}
