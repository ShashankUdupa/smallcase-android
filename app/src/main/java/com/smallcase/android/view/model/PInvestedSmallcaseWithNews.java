package com.smallcase.android.view.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shashankm on 07/12/16.
 */

public class PInvestedSmallcaseWithNews implements Parcelable {
    private String newsImage;
    private String newsLink;
    private String date;
    private String newsHeadline;
    private List<PInvestedSmallcase> investedSmallcase;
    private boolean isExpanded;

    public PInvestedSmallcaseWithNews() {
        isExpanded = false;
    }

    protected PInvestedSmallcaseWithNews(Parcel in) {
        newsImage = in.readString();
        newsLink = in.readString();
        date = in.readString();
        newsHeadline = in.readString();
        investedSmallcase = new ArrayList<PInvestedSmallcase>();
        in.readTypedList(investedSmallcase, PInvestedSmallcase.CREATOR);
    }

    public static final Creator<PInvestedSmallcaseWithNews> CREATOR = new Creator<PInvestedSmallcaseWithNews>() {
        @Override
        public PInvestedSmallcaseWithNews createFromParcel(Parcel in) {
            return new PInvestedSmallcaseWithNews(in);
        }

        @Override
        public PInvestedSmallcaseWithNews[] newArray(int size) {
            return new PInvestedSmallcaseWithNews[size];
        }
    };

    public void setNewsImage(String newsImage) {
        this.newsImage = newsImage;
    }

    public void setNewsLink(String newsLink) {
        this.newsLink = newsLink;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setNewsHeadline(String newsHeadline) {
        this.newsHeadline = newsHeadline;
    }

    public String getNewsImage() {
        return newsImage;
    }

    public String getNewsLink() {
        return newsLink;
    }

    public String getDate() {
        return date;
    }

    public String getNewsHeadline() {
        return newsHeadline;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(newsImage);
        dest.writeString(newsLink);
        dest.writeString(date);
        dest.writeString(newsHeadline);
        dest.writeTypedList(investedSmallcase);
    }

    public List<PInvestedSmallcase> getInvestedSmallcase() {
        return investedSmallcase;
    }

    public void setInvestedSmallcase(List<PInvestedSmallcase> investedSmallcase) {
        this.investedSmallcase = investedSmallcase;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }
}
