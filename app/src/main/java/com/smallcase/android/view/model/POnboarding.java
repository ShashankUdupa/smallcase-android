package com.smallcase.android.view.model;

import android.support.annotation.DrawableRes;

/**
 * Created by shashankm on 09/08/17.
 */

public class POnboarding {
    private int imageResId;

    private String title;


    private String description;

    public POnboarding(@DrawableRes int imageResId, String title, String description) {
        this.imageResId = imageResId;
        this.title = title;
        this.description = description;
    }

    @DrawableRes
    public int getImageResId() {
        return imageResId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
