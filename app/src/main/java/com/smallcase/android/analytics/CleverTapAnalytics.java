package com.smallcase.android.analytics;

import android.content.Context;
import android.support.annotation.Nullable;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;

import java.util.Map;

/**
 * Created by shashankm on 12/12/17.
 */

class CleverTapAnalytics {
    private CleverTapAPI cleverTapAPI;

    CleverTapAnalytics(Context context) {
        try {
            cleverTapAPI = CleverTapAPI.getInstance(context);
            CleverTapAPI.setDebugLevel(CleverTapAPI.LogLevel.DEBUG);
        } catch (CleverTapMetaDataNotFoundException | CleverTapPermissionsNotSatisfied e) {
            SentryAnalytics.getInstance().captureEvent(null, e, "Failed to get cleaver tap instance");
        }
    }

    void trackEvent(String eventName, @Nullable Map<String, Object> metaData) {
        cleverTapAPI.event.push(eventName, metaData);
    }
}
