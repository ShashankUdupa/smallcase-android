package com.smallcase.android.analytics;

/**
 * Created by shashankm on 07/12/17.
 */

public enum Analytics {
    MIXPANEL, INTERCOM, CLEVERTAP
}
