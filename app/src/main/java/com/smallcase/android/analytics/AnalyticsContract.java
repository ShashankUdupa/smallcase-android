package com.smallcase.android.analytics;

import android.support.annotation.Nullable;

import java.util.Map;

/**
 * Created by shashankm on 07/12/17.
 */

public interface AnalyticsContract {
    /**
     * Events that need to be sent to different sources
     * @param events all the events
     * @param eventKey key of the event
     * @param analytics all the analytics, that this data needs to be sent to
     */
    void sendEvent(@Nullable Map<String, Object> events, String eventKey, Analytics... analytics);
}
