package com.smallcase.android.analytics;

import android.content.Context;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by shashankm on 07/12/17.
 */

public class AnalyticsManager implements AnalyticsContract {
    private static final String TAG = "AnalyticsManager";

    private MixPanelAnalytics mixPanelAnalytics;
    private IntercomAnalytics intercomAnalytics;
    private CleverTapAnalytics cleverTapAnalytics;

    public AnalyticsManager(Context context) {
        mixPanelAnalytics = new MixPanelAnalytics(context);
        intercomAnalytics = new IntercomAnalytics();
        cleverTapAnalytics = new CleverTapAnalytics(context);
    }

    @Override
    public void sendEvent(@Nullable Map<String, Object> events, String eventKey, Analytics... analytics) {
        for (Analytics analytic : analytics) {
            switch (analytic) {
                case INTERCOM:
                    intercomAnalytics.trackEvent(eventKey, events);
                    break;

                case MIXPANEL:
                    JSONObject jsonObject = null;
                    if (events != null) {
                        jsonObject = new JSONObject();
                        for (Map.Entry<String, Object> it : events.entrySet()) {
                            try {
                                jsonObject.put(it.getKey(), it.getValue());
                            } catch (JSONException e) {
                                SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; sendEvent(); key - " + it.getKey());
                                e.printStackTrace();
                            }
                        }
                    }
                    mixPanelAnalytics.trackEvent(eventKey, jsonObject);
                    break;

                case CLEVERTAP:
                    cleverTapAnalytics.trackEvent(eventKey, events);
                    break;
            }
        }
    }
}
