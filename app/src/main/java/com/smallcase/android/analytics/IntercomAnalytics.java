package com.smallcase.android.analytics;

import android.support.annotation.Nullable;

import java.util.Map;

import io.intercom.android.sdk.Intercom;

/**
 * Created by shashankm on 06/07/17.
 */
class IntercomAnalytics {

    void trackEvent(String eventName, @Nullable Map<String, Object> metaData) {
        if (metaData == null) {
            Intercom.client().logEvent(eventName, null);
            return;
        }

        Intercom.client().logEvent(eventName, metaData);
    }
}
