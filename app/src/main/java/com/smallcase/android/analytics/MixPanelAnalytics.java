package com.smallcase.android.analytics;

import android.content.Context;
import android.support.annotation.Nullable;

import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.smallcase.android.BuildConfig;

import org.json.JSONObject;

/**
 * Created by shashankm on 23/06/17.
 */

public class MixPanelAnalytics {
    private MixpanelAPI mixpanelAPI;

    public MixPanelAnalytics(Context context) {
        mixpanelAPI = MixpanelAPI.getInstance(context, BuildConfig.MIX_PANEL_TOKEN);
    }

    void trackEvent(String eventKey, @Nullable JSONObject jsonObject) {
        if (jsonObject == null) {
            mixpanelAPI.track(eventKey);
            return;
        }
        mixpanelAPI.track(eventKey, jsonObject);
    }

    public void flushMixPanel() {
        trackEvent("App closed", null);
        mixpanelAPI.flush();
    }

    public void identify(String id) {
        mixpanelAPI.identify(id);
    }

    public MixpanelAPI.People getPeople() {
        return mixpanelAPI.getPeople();
    }
}
