package com.smallcase.android.analytics;

import android.support.annotation.Nullable;

import com.smallcase.android.util.AppUtils;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Map;

import io.sentry.Sentry;
import io.sentry.event.Breadcrumb;
import io.sentry.event.BreadcrumbBuilder;
import io.sentry.event.User;
import io.sentry.event.UserBuilder;

/**
 * Created by shashankm on 30/06/17.
 */

public class SentryAnalytics {
    private static SentryAnalytics instance;

    public static SentryAnalytics getInstance() {
        if (instance == null) {
            instance = new SentryAnalytics();
        }
        return instance;
    }

    public void recordEvent(Breadcrumb.Type type, @Nullable String message, @Nullable Map<String, String> data) {
        BreadcrumbBuilder breadcrumbBuilder = new BreadcrumbBuilder();
        breadcrumbBuilder.setType(type);
        if (message != null) {
            breadcrumbBuilder.setMessage(message);
        }

        if (data != null) {
            breadcrumbBuilder.setData(data);
        }

        Sentry.getContext().recordBreadcrumb(breadcrumbBuilder.build());
    }

    public void captureEvent(String jwt, Throwable throwable, String classAndMethodName) {
        if (throwable instanceof SocketTimeoutException || throwable instanceof UnknownHostException) return; // Too many

        User user = new UserBuilder().setId(AppUtils.getInstance().getUserId(jwt)).build();
        Sentry.getContext().setUser(user);

        Sentry.capture(new Exception(classAndMethodName, throwable));
    }
}
