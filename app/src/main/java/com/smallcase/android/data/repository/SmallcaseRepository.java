package com.smallcase.android.data.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.smallcase.android.api.ApiClient;
import com.smallcase.android.api.ApiRoutes;
import com.smallcase.android.data.BaseObjectConverter;
import com.smallcase.android.data.model.Collection;
import com.smallcase.android.data.model.Historical;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.Smallcase;
import com.smallcase.android.data.model.UnInvestedSmallcase;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Converter;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shashankm on 14/12/16.
 */

public class SmallcaseRepository {
    private static final RxJavaCallAdapterFactory FACTORY =  RxJavaCallAdapterFactory.create();

    public Observable<List<Smallcase>> getFeaturedSmallcases(String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildSmallcaseConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getFeaturedSmallcases()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Smallcase>> getSmallcases(String auth, @Nullable List<String> types, @Nullable List<String> scids, @Nullable String sortBy, int
            sortOrder, int offSet, int count, @Nullable Integer minMinInvestAmount, @Nullable Integer maxMinInvestAmount, @Nullable String
            searchString, String csrf, @Nullable String sid, @Nullable String tier) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildSmallcaseConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getSmallcases(types, scids, sortBy, sortOrder, offSet, count, minMinInvestAmount, maxMinInvestAmount,
                        searchString, sid, tier)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Collection>> getCollections(@NonNull String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildCollectionConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getCollections()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<News>> getSmallcaseNews(String auth, @Nullable List<String> scids, int offSet, int
            count, String csrf, @Nullable List<String> sids) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildNewsConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getNewsForInvestedSmallcase(scids, offSet, count, sids)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<UnInvestedSmallcase> getSmallcaseDetails(String auth, String scid, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildConverter(UnInvestedSmallcase.class))
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getSmallcaseDetails(scid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Historical> getSmallcaseHistorical(String auth, String scid, String benchmarkType, @Nullable String
            benchmarkId, String duration, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildConverter(Historical.class))
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getSmallcaseHistorical(scid, benchmarkType, benchmarkId, duration)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Converter.Factory buildSmallcaseConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type listType = new TypeToken<List<Smallcase>>(){}.getType();
        gsonBuilder.registerTypeAdapter(listType, new BaseObjectConverter<List<Smallcase>>());
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildCollectionConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type listType = new TypeToken<List<Collection>>(){}.getType();
        gsonBuilder.registerTypeAdapter(listType, new BaseObjectConverter<List<Collection>>());
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildNewsConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type listType = new TypeToken<List<News>>(){}.getType();
        gsonBuilder.registerTypeAdapter(listType, new BaseObjectConverter<List<News>>());
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildConverter(Class classType) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(classType, new BaseObjectConverter());
        return GsonConverterFactory.create(gsonBuilder.create());
    }
}
