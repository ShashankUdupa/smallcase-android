package com.smallcase.android.data.repository;

import com.smallcase.android.api.ApiClient;
import com.smallcase.android.api.ApiRoutes;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Handles communication with server and parsing of data for onBoarding flow.
 * This includes login and sign up.
 */
public class KiteRepository {
    private static final String TAG = KiteRepository.class.getSimpleName();
    private static final RxJavaCallAdapterFactory FACTORY =  RxJavaCallAdapterFactory.create();

    public Observable<ResponseBody> signUpUser(HashMap<String, Object> body) {
        return ApiClient.getClient(null, null)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .requestAccount(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> getAvailableFunds(String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getAvailableFunds()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
