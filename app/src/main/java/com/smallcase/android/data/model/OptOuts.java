package com.smallcase.android.data.model;

/**
 * Created by shashankm on 26/12/16.
 */

public class OptOuts {
    private String[] email;

    public String[] getEmail() {
        return email;
    }

    public void setEmail(String[] email) {
        this.email = email;
    }
}
