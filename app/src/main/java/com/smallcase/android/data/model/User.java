package com.smallcase.android.data.model;

import java.util.List;

/**
 * Created by shashankm on 26/12/16.
 */

public class User {
    private Holdings holdings;

    private List<String> smallcaseWatchlist;

    private Flags flags;

    private List<ExitedSmallcase> exitedSmallcases;

    private List<UserUpdates> updates;

    private List<InvestedSmallcases> investedSmallcases;

    private List<DraftSmallcases> draftSmallcases;

    private String _id;

    private String email;

    private Broker broker;

    private UserMeta meta;

    public Holdings getHoldings() {
        return holdings;
    }

    public void setHoldings(Holdings holdings) {
        this.holdings = holdings;
    }

    public List<String> getSmallcaseWatchlist() {
        return smallcaseWatchlist;
    }

    public void setSmallcaseWatchlist(List<String> smallcaseWatchlist) {
        this.smallcaseWatchlist = smallcaseWatchlist;
    }

    public Flags getFlags() {
        return flags;
    }

    public void setFlags(Flags flags) {
        this.flags = flags;
    }

    public List<ExitedSmallcase> getExitedSmallcases() {
        return exitedSmallcases;
    }

    public void setExitedSmallcases(List<ExitedSmallcase> exitedSmallcases) {
        this.exitedSmallcases = exitedSmallcases;
    }

    public List<UserUpdates> getUpdates() {
        return updates;
    }

    public void setUpdates(List<UserUpdates> updates) {
        this.updates = updates;
    }

    public List<InvestedSmallcases> getInvestedSmallcases() {
        return investedSmallcases;
    }

    public void setInvestedSmallcases(List<InvestedSmallcases> investedSmallcases) {
        this.investedSmallcases = investedSmallcases;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Broker getBroker() {
        return broker;
    }

    public void setBroker(Broker broker) {
        this.broker = broker;
    }

    public UserMeta getMeta() {
        return meta;
    }

    public void setMeta(UserMeta meta) {
        this.meta = meta;
    }

    @Override
    public String toString() {
        return "ClassPojo [holdings = " + holdings + ", smallcaseWatchlist = " + smallcaseWatchlist + ", flags = " + flags + ", exitedSmallcases = " + exitedSmallcases + ", updates = " + updates + ", investedSmallcases = " + investedSmallcases + ", _id = " + _id + ", email = " + email + ", broker = " + broker + ", meta = " + meta + "]";
    }

    public List<DraftSmallcases> getDraftSmallcases() {
        return draftSmallcases;
    }

    public void setDraftSmallcases(List<DraftSmallcases> draftSmallcases) {
        this.draftSmallcases = draftSmallcases;
    }
}
