package com.smallcase.android.data.model;

/**
 * Created by shashankm on 19/12/16.
 */

public class Bse {
    private String ticker;

    private String name;

    private String suspended;

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSuspended() {
        return suspended;
    }

    public void setSuspended(String suspended) {
        this.suspended = suspended;
    }

    @Override
    public String toString() {
        return "ClassPojo [ticker = " + ticker + ", name = " + name + ", suspended = " + suspended + "]";
    }
}
