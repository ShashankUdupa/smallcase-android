package com.smallcase.android.data.model;

import java.util.List;

/**
 * Created by shashankm on 30/01/17.
 */

public class Groups {
    private String sortField;

    private String sortOrder;

    private String label;

    private String type;

    private List<Smallcases> smallcases;

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Smallcases> getSmallcases() {
        return smallcases;
    }

    public void setSmallcases(List<Smallcases> smallcases) {
        this.smallcases = smallcases;
    }

    @Override
    public String toString() {
        return "ClassPojo [sortField = " + sortField + ", sortOrder = " + sortOrder + ", label = " + label + ", type = " + type + ", smallcases = " + smallcases + "]";
    }
}
