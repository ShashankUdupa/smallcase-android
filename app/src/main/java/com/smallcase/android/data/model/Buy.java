package com.smallcase.android.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by shashankm on 24/05/17.
 */

public class Buy implements Parcelable {
    private String scid;

    private String name;

    private String date;

    public Buy(Parcel in) {
        scid = in.readString();
        date = in.readString();
        name = in.readString();
    }

    public static final Creator<Buy> CREATOR = new Creator<Buy>() {
        @Override
        public Buy createFromParcel(Parcel in) {
            return new Buy(in);
        }

        @Override
        public Buy[] newArray(int size) {
            return new Buy[size];
        }
    };

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(scid);
        dest.writeString(date);
        dest.writeString(name);
    }
}
