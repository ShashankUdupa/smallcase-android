package com.smallcase.android.data.model;

/**
 * Created by shashankm on 06/12/16.
 */

public class SmallcaseIndex {
    private double indexValue;

    public double getIndexValue() {
        return indexValue;
    }

    public void setIndexValue(double indexValue) {
        this.indexValue = indexValue;
    }

    @Override
    public String toString() {
        return "ClassPojo [indexValue = " + indexValue + "]";
    }
}
