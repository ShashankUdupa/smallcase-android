package com.smallcase.android.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by shashankm on 01/05/17.
 */

public class ReBalanceUpdate implements Parcelable {
    private String iscid;

    private String scid;

    private String date;

    private String label;

    private String name;

    private int version;

    public ReBalanceUpdate(Parcel in) {
        iscid = in.readString();
        scid = in.readString();
        date = in.readString();
        label = in.readString();
        name = in.readString();
        version = in.readInt();
    }

    public static final Creator<ReBalanceUpdate> CREATOR = new Creator<ReBalanceUpdate>() {
        @Override
        public ReBalanceUpdate createFromParcel(Parcel in) {
            return new ReBalanceUpdate(in);
        }

        @Override
        public ReBalanceUpdate[] newArray(int size) {
            return new ReBalanceUpdate[size];
        }
    };

    public String getIscid() {
        return iscid;
    }

    public void setIscid(String iscid) {
        this.iscid = iscid;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(iscid);
        dest.writeString(scid);
        dest.writeString(date);
        dest.writeString(label);
        dest.writeString(name);
        dest.writeInt(version);
    }
}
