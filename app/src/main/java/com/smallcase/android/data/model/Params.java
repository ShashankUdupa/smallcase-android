package com.smallcase.android.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shashankm on 02/11/17.
 */

public class Params {
    private String key;

    private String acl;

    @SerializedName("success_action_status")
    private int successActionStatus;

    private String policy;

    @SerializedName("x-amz-algorithm")
    private String xAmzAlgorithm;

    @SerializedName("x-amz-credential")
    private String xAmzCredential;

    @SerializedName("x-amz-date")
    private String xAmzDate;

    @SerializedName("x-amz-signature")
    private String xAmzSignature;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getAcl() {
        return acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    public int getSuccessActionStatus() {
        return successActionStatus;
    }

    public void setSuccessActionStatus(int successActionStatus) {
        this.successActionStatus = successActionStatus;
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public String getxAmzAlgorithm() {
        return xAmzAlgorithm;
    }

    public void setxAmzAlgorithm(String xAmzAlgorithm) {
        this.xAmzAlgorithm = xAmzAlgorithm;
    }

    public String getxAmzCredential() {
        return xAmzCredential;
    }

    public void setxAmzCredential(String xAmzCredential) {
        this.xAmzCredential = xAmzCredential;
    }

    public String getxAmzDate() {
        return xAmzDate;
    }

    public void setxAmzDate(String xAmzDate) {
        this.xAmzDate = xAmzDate;
    }

    public String getxAmzSignature() {
        return xAmzSignature;
    }

    public void setxAmzSignature(String xAmzSignature) {
        this.xAmzSignature = xAmzSignature;
    }
}
