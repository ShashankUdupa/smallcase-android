package com.smallcase.android.data.model;

/**
 * Created by shashankm on 28/02/17.
 */
public class Points {
    private String date;

    private double index;

    private double price;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getIndex() {
        return index;
    }

    public void setIndex(double index) {
        this.index = index;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
