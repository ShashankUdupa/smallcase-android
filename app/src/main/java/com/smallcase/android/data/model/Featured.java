package com.smallcase.android.data.model;

/**
 * Created by shashankm on 27/02/17.
 */

public class Featured {
    private boolean value;

    private int rank;

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
