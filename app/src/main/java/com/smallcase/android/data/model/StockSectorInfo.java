package com.smallcase.android.data.model;

/**
 * Created by shashankm on 19/12/16.
 */

public class StockSectorInfo {
    private String sector;

    private String ticker;

    private String description;

    private String name;

    private String exchange;

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    @Override
    public String toString() {
        return "ClassPojo [sector = " + sector + ", ticker = " + ticker + ", description = " + description + ", name = " + name + ", exchange = " + exchange + "]";
    }
}
