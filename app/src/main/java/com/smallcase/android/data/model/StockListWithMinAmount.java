package com.smallcase.android.data.model;

import com.smallcase.android.view.model.Stock;

import java.util.List;

/**
 * Created by shashankm on 23/08/17.
 */

public class StockListWithMinAmount {

    private List<Stock> stockList;

    private double minAmount;

    private double maxPByW;

    public StockListWithMinAmount(List<Stock> stockList, double minAmount, double maxPByW) {
        this.stockList = stockList;
        this.minAmount = minAmount;
        this.maxPByW = maxPByW;
    }

    public List<Stock> getStockList() {
        return stockList;
    }

    public double getMinAmount() {
        return minAmount;
    }

    public double getMaxPByW() {
        return maxPByW;
    }
}
