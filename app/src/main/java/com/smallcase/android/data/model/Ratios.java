package com.smallcase.android.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shashankm on 19/12/16.
 */

public class Ratios {
    private String marketCap;

    @SerializedName("52wpct")
    private double fiftyTwowpct;

    private double pe;

    private String eps;

    @SerializedName("52wHigh")
    private double fiftyTwowHigh;

    @SerializedName("52wLow")
    private double fiftyTwowLow;

    private String pb;

    private double divYield;

    private String bps;

    private double beta;

    private String roe;

    @SerializedName("3mAvgVol")
    private String threemAvgVol;

    @SerializedName("4wpct")
    private double fourwpct;


    public String getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(String marketCap) {
        this.marketCap = marketCap;
    }

    public double getFiftyTwowpct() {
        return fiftyTwowpct;
    }

    public void setFiftyTwowpct(double fiftyTwowpct) {
        this.fiftyTwowpct = fiftyTwowpct;
    }

    public double getPe() {
        return pe;
    }

    public void setPe(double pe) {
        this.pe = pe;
    }

    public String getEps() {
        return eps;
    }

    public void setEps(String eps) {
        this.eps = eps;
    }

    public double getFiftyTwowHigh() {
        return fiftyTwowHigh;
    }

    public void setFiftyTwowHigh(double fiftyTwowHigh) {
        this.fiftyTwowHigh = fiftyTwowHigh;
    }

    public double getFiftyTwowLow() {
        return fiftyTwowLow;
    }

    public void setFiftyTwowLow(double fiftyTwowLow) {
        this.fiftyTwowLow = fiftyTwowLow;
    }

    public String getPb() {
        return pb;
    }

    public void setPb(String pb) {
        this.pb = pb;
    }

    public double getDivYield() {
        return divYield;
    }

    public void setDivYield(double divYield) {
        this.divYield = divYield;
    }

    public String getBps() {
        return bps;
    }

    public void setBps(String bps) {
        this.bps = bps;
    }

    public double getBeta() {
        return beta;
    }

    public void setBeta(double beta) {
        this.beta = beta;
    }

    public String getRoe() {
        return roe;
    }

    public void setRoe(String roe) {
        this.roe = roe;
    }

    public String getThreemAvgVol() {
        return threemAvgVol;
    }

    public void setThreemAvgVol(String threemAvgVol) {
        this.threemAvgVol = threemAvgVol;
    }

    public double getFourwpct() {
        return fourwpct;
    }

    public void setFourwpct(double fourwpct) {
        this.fourwpct = fourwpct;
    }
}
