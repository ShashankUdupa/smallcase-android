package com.smallcase.android.data.model;

import java.io.Serializable;

/**
 * Created by shashankm on 14/12/16.
 */

public class Constituents implements Serializable {
    private double shares;

    private String sid;

    private double averagePrice;

    private double weight;

    private SidInfo sidInfo;

    private boolean locked;

    public double getShares() {
        return shares;
    }

    public void setShares(double shares) {
        this.shares = shares;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public double getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(double averagePrice) {
        this.averagePrice = averagePrice;
    }

    public SidInfo getSidInfo() {
        return sidInfo;
    }

    public void setSidInfo(SidInfo sidInfo) {
        this.sidInfo = sidInfo;
    }

    @Override
    public String toString() {
        return "ClassPojo [shares = " + shares + ", sid = " + sid + ", averagePrice = " + averagePrice + ", sidInfo = " + sidInfo + "]";
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }
}
