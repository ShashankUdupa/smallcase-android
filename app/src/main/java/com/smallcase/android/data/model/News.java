package com.smallcase.android.data.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shashankm on 06/12/16.
 */

public class News {
    private List<String> tags;

    private String summary;

    private String headline;

    private Flags flags;

    private List<String> sids;

    private String imageUrl;

    private String source;

    private String _id;

    private String status;

    private String videoUrl;

    private String link;

    private String __v;

    private String date;

    private List<Smallcases> smallcases;

    private Publisher publisher;

    public List<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public Flags getFlags() {
        return flags;
    }

    public void setFlags(Flags flags) {
        this.flags = flags;
    }

    public List<String> getSids() {
        return sids;
    }

    public void setSids(List<String> sids) {
        this.sids = sids;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String get__v() {
        return __v;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Smallcases> getSmallcases() {
        return smallcases;
    }

    public void setSmallcases(ArrayList<Smallcases> smallcases) {
        this.smallcases = smallcases;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    @Override
    public String toString() {
        return "ClassPojo [tags = " + tags + ", summary = " + summary + ", headline = " + headline + ", flags = " + flags + ", sids = " + sids + ", source = " + source + ", _id = " + _id + ", status = " + status + ", videoUrl = " + videoUrl + ", link = " + link + ", __v = " + __v + ", date = " + date + ", smallcases = " + smallcases + ", publisher = " + publisher + "]";
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
