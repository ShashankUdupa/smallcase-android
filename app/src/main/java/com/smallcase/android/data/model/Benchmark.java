package com.smallcase.android.data.model;

import java.util.List;

/**
 * Created by shashankm on 28/02/17.
 */
public class Benchmark {
    private String benchmarkId;

    private List<Points> points;

    public String getBenchmarkId() {
        return benchmarkId;
    }

    public void setBenchmarkId(String benchmarkId) {
        this.benchmarkId = benchmarkId;
    }

    public List<Points> getPoints() {
        return points;
    }

    public void setPoints(List<Points> points) {
        this.points = points;
    }
}
