package com.smallcase.android.data.model;

import java.io.Serializable;

/**
 * Created by shashankm on 29/05/17.
 */

public class CustomizedSmallcaseStats implements Serializable {

    private double initialValue;

    private double indexValue;

    public double getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(double initialValue) {
        this.initialValue = initialValue;
    }

    public double getIndexValue() {
        return indexValue;
    }

    public void setIndexValue(double indexValue) {
        this.indexValue = indexValue;
    }
}
