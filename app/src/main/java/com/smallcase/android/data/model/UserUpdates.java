package com.smallcase.android.data.model;

/**
 * Created by shashankm on 26/12/16.
 */

public class UserUpdates {
    private String name;

    private String iscid;

    private String label;

    private String date;

    private String version;

    private String scid;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIscid() {
        return iscid;
    }

    public void setIscid(String iscid) {
        this.iscid = iscid;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    @Override
    public String toString() {
        return "ClassPojo [name = " + name + ", iscid = " + iscid + ", label = " + label + ", date = " + date + ", version = " + version + ", scid = " + scid + "]";
    }
}
