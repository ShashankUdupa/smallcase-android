package com.smallcase.android.data.model;

/**
 * Created by shashankm on 26/12/16.
 */

public class Broker {
    private String email;

    private String name;

    private String userId;

    private String userName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "ClassPojo [email = " + email + ", name = " + name + ", userId = " + userId + ", userName = " + userName + "]";
    }
}
