package com.smallcase.android.data.repository;

import android.support.annotation.Nullable;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.smallcase.android.api.ApiClient;
import com.smallcase.android.api.ApiRoutes;
import com.smallcase.android.data.BaseObjectConverter;
import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.model.ImageUploadOptions;
import com.smallcase.android.data.model.Order;
import com.smallcase.android.data.model.SipDetails;
import com.smallcase.android.data.model.SmallcaseDetail;
import com.smallcase.android.data.model.SmallcaseOrder;
import com.smallcase.android.user.coustomize.DraftConverter;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shashankm on 06/09/17.
 */

public class UserSmallcaseRepository {
    private static final RxJavaCallAdapterFactory FACTORY =  RxJavaCallAdapterFactory.create();

    public Observable<ResponseBody> placeOrder(String auth, SmallcaseOrder smallcaseOrder, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .placeOrder(smallcaseOrder)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> addToWatchlist(String auth, HashMap<String, String> body, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .addToWatchlist(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> removeFromWatchlist(String auth, HashMap<String, String> body, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .removeFromWatchlist(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Order>> getOrderDetails(String auth, @Nullable String iScid, @Nullable String batchId,
                                                   String csrf, boolean onlyOne, boolean noDetails) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildOrderDetailsConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getOrderDetails(iScid, batchId, onlyOne, noDetails)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> cancelBatch(String auth, HashMap<String, String> body, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .cancelBatch(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<SmallcaseDetail> getInvestedSmallcaseDetails(String auth, String iScid, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildConverter(SmallcaseDetail.class))
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getInvestedSmallcaseDetails(iScid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<DraftSmallcase>> getDrafts(String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildDraftsConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getDrafts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<DraftSmallcase> getDraft(String auth, String csrf, String did) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildDraftConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getDraft(did)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> deleteDraft(String auth, String csrf, HashMap<String, String> body) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .deleteDraft(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> saveDraft(String auth, String csrf, DraftSmallcase draftSmallcase) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .saveDrafts(draftSmallcase)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> fixBatch(String auth, HashMap<String, String> body, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .fixBatch(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> cancelUpdates(String auth, String csrf, HashMap<String, String> body) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .cancelUpdates(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> noOrders(String auth, String csrf, HashMap<String, Object> body) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .noOrders(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> addReminder(String auth, String csrf, HashMap<String, String> body) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .addReminder(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<SipDetails> getSipDetails(String auth, String csrf, String iscid) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildConverter(SipDetails.class))
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getSipDetails(iscid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ImageUploadOptions> getImageUploadConfig(String auth, String csrf, String scid) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildConverter(ImageUploadOptions.class))
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getImageUploadConfig(scid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> uploadDraftImage(String url, MultipartBody
            .Part file, LinkedHashMap<String, RequestBody> partMap) {
        return ApiClient.getPlainClient(url, null)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .uploadDraftImage(partMap, file)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> setUpSip(String auth, String csrf, HashMap<String, String> body) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .setUpSip(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> manageSip(String auth, String csrf, HashMap<String, String> body) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .manageSip(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> endSip(String auth, String csrf, HashMap<String, String> body) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .endSip(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> skipSip(String auth, String csrf, HashMap<String, String> body) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .skipSip(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Converter.Factory buildOrderDetailsConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type listType = new TypeToken<List<Order>>(){}.getType();
        gsonBuilder.registerTypeAdapter(listType, new BaseObjectConverter<List<Order>>());
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildDraftConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DraftSmallcase.class, new BaseObjectConverter<DraftSmallcase>());
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildDraftsConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type listType = new TypeToken<List<DraftSmallcase>>(){}.getType();
        gsonBuilder.registerTypeAdapter(listType, new DraftConverter());
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildConverter(Class classType) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(classType, new BaseObjectConverter());
        return GsonConverterFactory.create(gsonBuilder.create());
    }
}
