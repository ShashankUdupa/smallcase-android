package com.smallcase.android.data.model;

/**
 * Created by shashankm on 14/12/16.
 */

public class SmallcaseDetail {
    private CurrentConfig currentConfig;

    private String dateModified;

    private String source;

    private String _id;

    private FailedBatch failedBatch;

    private String status;

    private Stats stats;

    private String name;

    private Returns returns;

    private String date;

    private String version;

    private String scid;

    private InvestedSmallcaseFlags flags;

    public CurrentConfig getCurrentConfig() {
        return currentConfig;
    }

    public void setCurrentConfig(CurrentConfig currentConfig) {
        this.currentConfig = currentConfig;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public FailedBatch getFailedBatch() {
        return failedBatch;
    }

    public void setFailedBatch(FailedBatch failedBatch) {
        this.failedBatch = failedBatch;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Returns getReturns() {
        return returns;
    }

    public void setReturns(Returns returns) {
        this.returns = returns;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    @Override
    public String toString() {
        return "ClassPojo [currentConfig = " + currentConfig + ", dateModified = " + dateModified + ", source = " + source + ", _id = " + _id + ", failedBatch = " + failedBatch + ", status = " + status + ", stats = " + stats + ", name = " + name + ", returns = " + returns + ", date = " + date + ", version = " + version + ", scid = " + scid + "]";
    }

    public InvestedSmallcaseFlags getFlags() {
        return flags;
    }

    public void setFlags(InvestedSmallcaseFlags flags) {
        this.flags = flags;
    }
}
