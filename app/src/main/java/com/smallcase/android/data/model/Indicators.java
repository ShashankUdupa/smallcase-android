package com.smallcase.android.data.model;

/**
 * Created by shashankm on 19/12/16.
 */

public class Indicators {
    private String fundamental;

    private String technical;

    public String getFundamental() {
        return fundamental;
    }

    public void setFundamental(String fundamental) {
        this.fundamental = fundamental;
    }

    public String getTechnical() {
        return technical;
    }

    public void setTechnical(String technical) {
        this.technical = technical;
    }

    @Override
    public String toString() {
        return "ClassPojo [fundamental = " + fundamental + ", technical = " + technical + "]";
    }
}
