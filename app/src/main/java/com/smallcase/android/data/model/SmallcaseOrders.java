package com.smallcase.android.data.model;

/**
 * Created by shashankm on 07/03/17.
 */

public class SmallcaseOrders {
    private String sid;

    private int quantity;

    private String transactionType;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
}
