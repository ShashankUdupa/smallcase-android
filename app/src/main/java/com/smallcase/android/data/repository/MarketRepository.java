package com.smallcase.android.data.repository;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.smallcase.android.api.ApiClient;
import com.smallcase.android.api.ApiRoutes;
import com.smallcase.android.data.BaseObjectConverter;
import com.smallcase.android.data.model.Nifty;
import com.smallcase.android.data.model.SearchResult;
import com.smallcase.android.data.model.Similar;
import com.smallcase.android.data.model.StockInfo;
import com.smallcase.android.data.model.StockPriceAndChange;
import com.smallcase.android.investedsmallcase.StockConverter;
import com.smallcase.android.stock.StockSearchConverter;
import com.smallcase.android.user.dashboard.NiftyDeserializer;

import java.lang.reflect.Type;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shashankm on 17/11/16.
 */

public class MarketRepository {
    private static final RxJavaCallAdapterFactory FACTORY =  RxJavaCallAdapterFactory.create();

    public Observable<Nifty> getNifty(String stocks, String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildNiftyConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getMarketPriceAndChange(stocks)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> getStocksPrice(String auth, List<String> sids, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getStocksPrice(sids)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<StockInfo> getStocksInfo(String auth, String sid, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildStockInfoConverter(sid))
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getStockInfoAndHistorical(sid)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> getStocksInfo(String auth, String csrf, List<String> stocks) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getStockInfo(stocks)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<StockPriceAndChange> getStockPriceAndChange(String auth, String sid, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildStockPriceAndChangeConverter(sid))
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getStockPriceAndChange(sid)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> getIfMarketIsOpen(String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getIfMarketIsOpen()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<SearchResult>> getStockInfo(String auth, String searchText, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildStockSearchConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getStocksInfo(searchText)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Similar>> getSimilarStocks(String auth, List<String> stocks, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildSimilarStocksConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getSimilarStocks(stocks)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> getStockHistorical(String auth, String csrf, List<String> stocks, String duration) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getStockHistorical(stocks, duration)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Converter.Factory buildSimilarStocksConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type listType = new TypeToken<List<Similar>>(){}.getType();
        gsonBuilder.registerTypeAdapter(listType, new BaseObjectConverter<List<Similar>>());
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildStockSearchConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type listType = new TypeToken<List<SearchResult>>(){}.getType();
        gsonBuilder.registerTypeAdapter(listType, new StockSearchConverter());
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildStockPriceAndChangeConverter(String sid) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(StockPriceAndChange.class, new StockConverter<StockPriceAndChange>(sid));
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildStockInfoConverter(String sid) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(StockInfo.class, new StockConverter<StockInfo>(sid));
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildNiftyConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Nifty.class, new NiftyDeserializer());
        return GsonConverterFactory.create(gsonBuilder.create());
    }
}
