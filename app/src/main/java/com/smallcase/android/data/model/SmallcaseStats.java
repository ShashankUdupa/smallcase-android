package com.smallcase.android.data.model;

/**
 * Created by shashankm on 24/01/17.
 */
public class SmallcaseStats {
    private double indexValue;

    private double unadjustedIndex;

    private double minInvestAmount;

    private SmallcaseReturns returns;

    private double divReturns;

    private double lastCloseIndex;

    public double getIndexValue() {
        return indexValue;
    }

    public void setIndexValue(double indexValue) {
        this.indexValue = indexValue;
    }

    public double getUnadjustedIndex() {
        return unadjustedIndex;
    }

    public void setUnadjustedIndex(double unadjustedIndex) {
        this.unadjustedIndex = unadjustedIndex;
    }

    public double getMinInvestAmount() {
        return minInvestAmount;
    }

    public void setMinInvestAmount(double minInvestAmount) {
        this.minInvestAmount = minInvestAmount;
    }

    public SmallcaseReturns getReturns() {
        return returns;
    }

    public void setReturns(SmallcaseReturns returns) {
        this.returns = returns;
    }

    public double getDivReturns() {
        return divReturns;
    }

    public void setDivReturns(double divReturns) {
        this.divReturns = divReturns;
    }

    public double getLastCloseIndex() {
        return lastCloseIndex;
    }

    public void setLastCloseIndex(double lastCloseIndex) {
        this.lastCloseIndex = lastCloseIndex;
    }

    @Override
    public String toString() {
        return "ClassPojo [indexValue = " + indexValue + ", unadjustedIndex = " + unadjustedIndex + ", minInvestAmount = " + minInvestAmount + ", returns = " + returns + ", divReturns = " + divReturns + ", lastCloseIndex = " + lastCloseIndex + "]";
    }
}
