package com.smallcase.android.data.model;

import com.google.gson.annotations.SerializedName;
import com.smallcase.android.util.AppConstants;

/**
 * Created by shashankm on 18/11/16.
 */

public class InvestedSmallcase {
    private String source;
    @SerializedName("_id")
    private String id;
    private String status;
    private Stats stats;
    private String name;
    private Returns returns;
    private String date;
    private String scid;
    private String imageUrl;

    public String getSource() {
        return source;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String resolution) {
        this.imageUrl = AppConstants.SMALLCASE_IMAGE_URL + resolution + "/" + this.scid +".png";
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Returns getReturns() {
        return returns;
    }

    public void setReturns(Returns returns) {
        this.returns = returns;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    @Override
    public String toString() {
        return "ClassPojo [source = " + source + ", id = " + id + ", status = " + status + ", stats = " + stats + ", name = " + name + ", returns = " + returns + ", date = " + date + ", scid = " + scid + "]";
    }
}
