package com.smallcase.android.data.repository;

import android.content.Context;
import android.content.SharedPreferences;

import com.smallcase.android.data.model.InvestedSmallcases;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.Nifty;
import com.smallcase.android.data.model.TotalInvestment;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Handles offline caching of data.
 */
public class SharedPrefService {
    private static final String TAG = SharedPrefService.class.getSimpleName();
    /**
     * Used as key for storing authorization token for user in
     * shared preferences
     */
    private static final String USER_AUTHORIZATION_TOKEN = "user_auth";
    private static final String NIFTY_OBJECT = "nifty_object";
    private static final String TOTAL_INVESTMENTS = "total_investments";
    private static final String NEWS_SET = "news_set";
    private static final String INVESTED_SMALLCASES = "invested_smallcases";
    private static final String INVESTED_SCIDS = "invested_scids";
    private static final String REGISTER_FOR_NOTIFICATION = "register_for_notification";
    private static final String USER_NAME = "user_name";
    private static final String KITE_ID = "kite_id";
    private static final String FEATURED_SMALLCASES = "featured_smallcases";
    private static final String RECENT_SMALLCASES = "recent_smallcases";
    private static final String COLLECTIONS = "collection";
    private static final String RECENT_NEWS = "recent_news";
    private static final String POPULAR_SMALLCASES = "popular_smallcases";
    private static final String WATCHLIST = "watchlist";
    private static final String WATCHLIST_NEWS = "watchlist_news";
    private static final String CSRF = "csrf";
    private static final String REBALANCE_UPDATES_LIST = "updates_list";
    private static final String WEEKLY_NOTIFICATION = "weekly_notification";
    private static final String VIEWED_WATCHLIST_TUTORIAL = "viewed_watchlist_tutorial";
    private static final String VIEWED_STOCK_INFO_TUTORIAL = "viewed_stock_info_tutorial";
    private static final String EXITED_ISCIDS = "exited_iscids";
    private static final String VIEWED_INDEX_TUTORIAL = "viewed_index_tutorial";
    private static final String VIEWED_ONBOARDING_TUTORIAL = "viewed_onboarding_tutorial";
    private static final String BROKER_NAME = "broker_name";
    private static final String SIPS_SET = "sips";
    private static final String IGNORED_UPDATE = "ignored_update";
    private static final String USER_SEEN_CREATE_TUTORIAL = "user_seen_create_tutorial";
    private static final String SHOW_CUSTOMIZE_INFO_POPUP = "show_customize_info_popup";

    private static String CSRF_TOKEN;
    private static String AUTHORIZATION_TOKEN;

    private SharedPreferences sharedPreferences;

    /**
     * Fetch shared preference with given name.
     *
     * @param context used to retrieve said shared preference.
     */
    public SharedPrefService(Context context) {
        sharedPreferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
    }

    public void saveAuthorizationToken(String token) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_AUTHORIZATION_TOKEN, token);
        editor.apply();
        AUTHORIZATION_TOKEN = null;
    }

    /**
     * Gets the authorization token that needs to be added for every network request.
     *
     * @return authorization string.
     */
    public String getAuthorizationToken() {
        if (AUTHORIZATION_TOKEN != null) {
            return AUTHORIZATION_TOKEN;
        }

        AUTHORIZATION_TOKEN = sharedPreferences.getString(USER_AUTHORIZATION_TOKEN, null);
        if (AUTHORIZATION_TOKEN != null) AUTHORIZATION_TOKEN = "JWT " + AUTHORIZATION_TOKEN;
        return AUTHORIZATION_TOKEN;
    }

    public void cacheNifty(String niftyObject) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(NIFTY_OBJECT, niftyObject);
        editor.apply();
    }

    public void cacheTotalInvestments(String totalInvestments) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TOTAL_INVESTMENTS, totalInvestments);
        editor.apply();
    }

    public void cacheInvestedNews(HashSet<String> newsSet) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(NEWS_SET, newsSet);
        editor.apply();
    }

    /**
     * Data is stored in the form of
     * json strings. The json can be converted to {@link Nifty}
     * pojo using.
     */
    public String getNifty() {
        return sharedPreferences.getString(NIFTY_OBJECT, null);
    }

    /**
     * Data is stored in the form of
     * json strings. The json can be converted to {@link TotalInvestment}
     * pojo using.
     */
    public String getTotalInvestments() {
        return sharedPreferences.getString(TOTAL_INVESTMENTS, null);
    }

    /**
     * Data is stored in the form of
     * json strings. The json can be converted to {@link News}
     * pojo using.
     */
    public Set<String> getCachedNews() {
        return sharedPreferences.getStringSet(NEWS_SET, Collections.<String>emptySet());
    }

    public void cacheInvestedSmallcases(HashSet<String> investedSmallcases) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(INVESTED_SMALLCASES, investedSmallcases);
        editor.apply();
    }

    /**
     * Data is stored in the form of
     * json strings. The json can be converted to List<{@link InvestedSmallcases}>
     * pojo using.
     */
    public Set<String> getInvestedSmallcases() {
        return sharedPreferences.getStringSet(INVESTED_SMALLCASES, Collections.<String>emptySet());
    }

    /**
     * Returns a set of strings containing scids with iscid in the form scid#iscid.
     */
    public Set<String> getInvestedScidsWithIScids() {
        return sharedPreferences.getStringSet(INVESTED_SCIDS, Collections.<String>emptySet());
    }

    /**
     * Saves list of scids with invested scids in the form scid#iscid
     * @param investedScids set of scids with iscid
     */
    public void saveInvestedScidWithIscid(HashSet<String> investedScids) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(INVESTED_SCIDS, investedScids);
        editor.apply();
    }

    public void clearSharedPreference() {
        AUTHORIZATION_TOKEN = null;
        CSRF_TOKEN = null;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Map<String,?> prefs = sharedPreferences.getAll();
        for(Map.Entry<String,?> entry : prefs.entrySet()){
            if (VIEWED_ONBOARDING_TUTORIAL.equals(entry.getKey())) continue;
            editor.remove(entry.getKey()).apply();
        }
    }

    public void deviceRegisteredForNotification() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(REGISTER_FOR_NOTIFICATION, true);
        editor.apply();
    }

    public boolean isDeviceRegisteredForNotification() {
        return sharedPreferences.getBoolean(REGISTER_FOR_NOTIFICATION, false);
    }

    public void registrationNotComplete() {
        if (sharedPreferences.contains(REGISTER_FOR_NOTIFICATION)) {
            sharedPreferences.edit().remove(REGISTER_FOR_NOTIFICATION).apply();
        }
    }

    public void cacheUserInfo(String userName, String kiteId, String brokerName) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_NAME, userName);
        editor.putString(KITE_ID, kiteId);
        editor.putString(BROKER_NAME, brokerName);
        editor.apply();
    }

    public Map<String, String> getUserInfo() {
        HashMap<String, String> userInfo = new HashMap<>();
        String name = sharedPreferences.getString(USER_NAME, "");
        String kiteId = sharedPreferences.getString(KITE_ID, "");
        String brokerName = sharedPreferences.getString(BROKER_NAME, "");
        if (name.equals("") || kiteId.equals("")) return Collections.emptyMap();
        userInfo.put("name", name);
        userInfo.put("kiteId", kiteId);
        userInfo.put("brokerName", brokerName);
        return userInfo;
    }

    public void cacheFeaturedSmallcases(HashSet<String> featuredSet) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(FEATURED_SMALLCASES, featuredSet);
        editor.apply();
    }

    public void cacheRecentSmallcases(HashSet<String> smallcaseSet) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(RECENT_SMALLCASES, smallcaseSet);
        editor.apply();
    }

    public void cacheCollection(HashSet<String> collectionSet) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(COLLECTIONS, collectionSet);
        editor.apply();
    }

    public void cacheRecentNews(HashSet<String> newsSet) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(RECENT_NEWS, newsSet);
        editor.apply();
    }

    public Set<String> getCachedFeaturedSmallcases() {
        return sharedPreferences.getStringSet(FEATURED_SMALLCASES, Collections.<String>emptySet());
    }

    public Set<String> getCachedRecentSmallcases() {
        return sharedPreferences.getStringSet(RECENT_SMALLCASES, Collections.<String>emptySet());
    }

    public Set<String> getCachedCollection() {
        return sharedPreferences.getStringSet(COLLECTIONS, Collections.<String>emptySet());
    }

    public Set<String> getCachedRecentNews() {
        return sharedPreferences.getStringSet(RECENT_NEWS, Collections.<String>emptySet());
    }

    public void cachePopularSmallcases(HashSet<String> smallcaseSet) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(POPULAR_SMALLCASES, smallcaseSet);
        editor.apply();
    }

    public Set<String> getCachedPopularSmallcases() {
        return sharedPreferences.getStringSet(POPULAR_SMALLCASES, Collections.<String>emptySet());
    }

    /**
     * Cache watchlisted scids
     *
     * @param smallcaseWatchlist list of watchlisted scids
     */
    public void cacheWatchlist(List<String> smallcaseWatchlist) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(WATCHLIST, new HashSet<>(smallcaseWatchlist));
        editor.apply();
    }

    public Set<String> getCachedWatchlist() {
        return sharedPreferences.getStringSet(WATCHLIST, Collections.<String>emptySet());
    }

    public void cacheWatchlistNews(HashSet<String> smallcasesWithNewsSet) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(WATCHLIST_NEWS, smallcasesWithNewsSet);
        editor.apply();
    }

    public Set<String> getCachedWatchlistNews() {
        return sharedPreferences.getStringSet(WATCHLIST_NEWS, Collections.<String>emptySet());
    }

    public String getKiteId() {
        return sharedPreferences.getString(KITE_ID, "");
    }

    public void saveCsrf(String csrf) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(CSRF, csrf);
        editor.apply();
        CSRF_TOKEN = null;
    }

    public String getCsrfToken() {
        if (CSRF_TOKEN != null) {
            return CSRF_TOKEN;
        }

        CSRF_TOKEN = sharedPreferences.getString(CSRF, null);
        return CSRF_TOKEN;
    }

    public void cacheReBalanceUpdates(HashSet<String> reBalanceUpdateList) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(REBALANCE_UPDATES_LIST, reBalanceUpdateList);
        editor.apply();
    }

    public Set<String> getCachedReBalanceUpdates() {
        return sharedPreferences.getStringSet(REBALANCE_UPDATES_LIST, Collections.<String>emptySet());
    }

    public void cacheWeeklyNotificationPreference(boolean isEnabled) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(WEEKLY_NOTIFICATION, isEnabled);
        editor.apply();
    }

    public boolean getWeeklyNotificationPreference() {
        return sharedPreferences.getBoolean(WEEKLY_NOTIFICATION, true);
    }

    public void updateWeeklyNotificationCache(boolean isEnabled) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(WEEKLY_NOTIFICATION, isEnabled);
        editor.apply();
    }

    public void userViewedWatchlistTutorial(boolean viewedWatchlistTutorial) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(VIEWED_WATCHLIST_TUTORIAL, viewedWatchlistTutorial);
        editor.apply();
    }

    public void userViewedIndexTutorial(boolean viewedIndexTutorial) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(VIEWED_INDEX_TUTORIAL, viewedIndexTutorial);
        editor.apply();
    }

    public boolean hasUserViewedIndexTutorial() {
        return sharedPreferences.getBoolean(VIEWED_INDEX_TUTORIAL, false);
    }

    public boolean hasUserViewedWatchlistTutorial() {
        return sharedPreferences.getBoolean(VIEWED_WATCHLIST_TUTORIAL, false);
    }

    public boolean hasUserViewedStocks() {
        return sharedPreferences.getBoolean(VIEWED_STOCK_INFO_TUTORIAL, false);
    }

    public void userViewedSmallcaseStocks(boolean viewedStockTutorial) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(VIEWED_STOCK_INFO_TUTORIAL, viewedStockTutorial);
        editor.apply();
    }

    public void cacheExitedIScids(Set<String> exitedIScids) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(EXITED_ISCIDS, exitedIScids);
        editor.apply();
    }

    public Set<String> getExitedIScids() {
        return sharedPreferences.getStringSet(EXITED_ISCIDS, Collections.<String>emptySet());
    }

    public boolean hasUserViewedOnBoardTutorial() {
        return sharedPreferences.getBoolean(VIEWED_ONBOARDING_TUTORIAL, false);
    }

    public void userViewedOnBoardTutorial() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(VIEWED_ONBOARDING_TUTORIAL, true);
        editor.apply();
    }

    public void cacheSipUpdates(HashSet<String> sipsSet) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(SIPS_SET, sipsSet);
        editor.apply();
    }

    public Set<String> getCachedSipUpdates() {
        return sharedPreferences.getStringSet(SIPS_SET, Collections.<String>emptySet());
    }

    public boolean hasUserIgnoredUpdate() {
        return sharedPreferences.getBoolean(IGNORED_UPDATE, false);
    }

    public void setUserIgnoredUpdate() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IGNORED_UPDATE, true);
        editor.apply();
    }

    public boolean hasUserSeenCreateTutorial() {
        return sharedPreferences.getBoolean(USER_SEEN_CREATE_TUTORIAL, false);
    }

    public void setUserSeenCreateTutorial() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(USER_SEEN_CREATE_TUTORIAL, true);
        editor.apply();
    }

    public void dontShowCustomizeInfoPopup() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SHOW_CUSTOMIZE_INFO_POPUP, false);
        editor.apply();
    }

    public boolean shouldShowCustomizeInfoPopup() {
        return sharedPreferences.getBoolean(SHOW_CUSTOMIZE_INFO_POPUP, true);
    }
}
