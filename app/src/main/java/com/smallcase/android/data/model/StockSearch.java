package com.smallcase.android.data.model;

/**
 * Created by shashankm on 06/04/17.
 */

public class StockSearch {
    private StockSearchInfo info;

    public StockSearchInfo getInfo() {
        return info;
    }

    public void setInfo(StockSearchInfo info) {
        this.info = info;
    }
}
