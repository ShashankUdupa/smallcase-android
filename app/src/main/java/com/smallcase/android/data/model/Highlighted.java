package com.smallcase.android.data.model;

/**
 * Created by shashankm on 24/01/17.
 */
public class Highlighted {
    private String rank;

    private String description;

    private String value;

    private String label;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "ClassPojo [rank = " + rank + ", description = " + description + ", value = " + value + ", label = " + label + "]";
    }
}
