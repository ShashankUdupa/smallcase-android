package com.smallcase.android.data.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shashankm on 14/12/16.
 */

public class Segments implements Serializable {
    private List<String> constituents;

    private String label;

    public List<String> getConstituents() {
        return constituents;
    }

    public void setConstituents(List<String> constituents) {
        this.constituents = constituents;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
