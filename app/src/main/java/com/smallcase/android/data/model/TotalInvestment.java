package com.smallcase.android.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shashankm on 17/11/16.
 */

public class TotalInvestment {
    @SerializedName("otherReturns")
    private double otherReturns;
    @SerializedName("divReturns")
    private double divReturns;
    @SerializedName("realizedReturns")
    private double realizedReturns;
    @SerializedName("realizedInvestment")
    private double realizedInvestment;
    @SerializedName("unrealizedInvestment")
    private double unrealizedInvestment;
    @SerializedName("networth")
    private double networth;

    public TotalInvestment(double otherReturns, double divReturns, double realizedReturns, double realizedInvestment, double unrealizedInvestment, double networth) {
        this.otherReturns = otherReturns;
        this.divReturns = divReturns;
        this.realizedReturns = realizedReturns;
        this.realizedInvestment = realizedInvestment;
        this.unrealizedInvestment = unrealizedInvestment;
        this.networth = networth;
    }

    public double getOtherReturns() {
        return otherReturns;
    }

    public double getDivReturns() {
        return divReturns;
    }

    public double getRealizedReturns() {
        return realizedReturns;
    }

    public double getRealizedInvestment() {
        return realizedInvestment;
    }

    public double getUnrealizedInvestment() {
        return unrealizedInvestment;
    }

    public double getNetworth() {
        return networth;
    }
}
