package com.smallcase.android.data.model;

/**
 * Created by shashankm on 26/12/16.
 */

public class Dna {
    private Integer q1;

    private Integer q2;

    public Integer getQ1() {
        return q1;
    }

    public void setQ1(Integer q1) {
        this.q1 = q1;
    }

    @Override
    public String toString() {
        return "ClassPojo [q1 = " + q1 + "; q2 = " + q2 + "]";
    }

    public Integer getQ2() {
        return q2;
    }

    public void setQ2(Integer q2) {
        this.q2 = q2;
    }
}
