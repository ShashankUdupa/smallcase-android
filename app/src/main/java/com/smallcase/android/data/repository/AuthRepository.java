package com.smallcase.android.data.repository;

import com.smallcase.android.BuildConfig;
import com.smallcase.android.api.ApiClient;
import com.smallcase.android.api.ApiRoutes;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shashankm on 02/01/18.
 */

public class AuthRepository {
    private static final String AUTH_BASE_URL = BuildConfig.AUTH_BASE_URL;
    private static final String TAG = KiteRepository.class.getSimpleName();
    private static final RxJavaCallAdapterFactory FACTORY =  RxJavaCallAdapterFactory.create();

    public Observable<ResponseBody> getAuthToken(HashMap<String, String> body) {
        return ApiClient.getPlainClient(AUTH_BASE_URL, null)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .brokerLogin(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> getRequestToken(String app, String jwt) {
        return ApiClient.getPlainClient(AUTH_BASE_URL, jwt)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getRequestToken(app)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> loginSmallcase(HashMap<String, String> body) {
        return ApiClient.getClient(null, null)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .loginSmallcase(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
