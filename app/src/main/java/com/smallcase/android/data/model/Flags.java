package com.smallcase.android.data.model;

/**
 * Created by shashankm on 06/12/16.
 */

public class Flags {
    private String featured;

    private Marker markers;

    private boolean updatedProfile;

    public String getFeatured() {
        return featured;
    }

    public void setFeatured(String featured) {
        this.featured = featured;
    }

    public Marker getMarkers() {
        return markers;
    }

    public void setMarkers(Marker markers) {
        this.markers = markers;
    }

    public boolean isUpdatedProfile() {
        return updatedProfile;
    }

    public void setUpdatedProfile(boolean updatedProfile) {
        this.updatedProfile = updatedProfile;
    }
}
