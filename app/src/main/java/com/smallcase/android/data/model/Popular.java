package com.smallcase.android.data.model;

/**
 * Created by shashankm on 25/01/17.
 */

public class Popular {
    private String rank;

    private String value;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ClassPojo [rank = " + rank + ", value = " + value + "]";
    }
}
