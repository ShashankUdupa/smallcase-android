package com.smallcase.android.data.model;

import java.util.List;

/**
 * Created by shashankm on 17/03/17.
 */

public class Order {
    private String _id;

    private String name;

    private String lastBatchDate;

    private List<Batch> batches;

    private LastBatch lastBatch;

    private String scid;

    private String source;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Batch> getBatches() {
        return batches;
    }

    public void setBatches(List<Batch> batches) {
        this.batches = batches;
    }

    public String getLastBatchDate() {
        return lastBatchDate;
    }

    public void setLastBatchDate(String lastBatchDate) {
        this.lastBatchDate = lastBatchDate;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public LastBatch getLastBatch() {
        return lastBatch;
    }

    public void setLastBatch(LastBatch lastBatch) {
        this.lastBatch = lastBatch;
    }
}
