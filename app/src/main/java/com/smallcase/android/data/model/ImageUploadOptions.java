package com.smallcase.android.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shashankm on 02/11/17.
 */

public class ImageUploadOptions {

    @SerializedName("endpoint")
    private String endPoint;

    private Params params;

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public Params getParams() {
        return params;
    }

    public void setParams(Params params) {
        this.params = params;
    }
}
