package com.smallcase.android.data.model;

/**
 * Created by shashankm on 06/12/16.
 */

public class Publisher {
    private String logo;

    private String publisherId;

    private String name;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(String publisherId) {
        this.publisherId = publisherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ClassPojo [logo = " + logo + ", publisherId = " + publisherId + ", name = " + name + "]";
    }
}
