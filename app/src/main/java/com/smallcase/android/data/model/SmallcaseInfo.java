package com.smallcase.android.data.model;

/**
 * Created by shashankm on 24/01/17.
 */
public class SmallcaseInfo {
    private String[] tags;

    private String rebalanceSchedule;

    private String blogURL;

    private String updated;

    private String created;

    private String shortDescription;

    private String name;

    private String uploaded;

    private String type;

    private String nextUpdate;

    private PlayedOut playedOut;

    private String tier;

    private String creator;

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    public String getRebalanceSchedule() {
        return rebalanceSchedule;
    }

    public void setRebalanceSchedule(String rebalanceSchedule) {
        this.rebalanceSchedule = rebalanceSchedule;
    }

    public String getBlogURL() {
        return blogURL;
    }

    public void setBlogURL(String blogURL) {
        this.blogURL = blogURL;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUploaded() {
        return uploaded;
    }

    public void setUploaded(String uploaded) {
        this.uploaded = uploaded;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNextUpdate() {
        return nextUpdate;
    }

    public void setNextUpdate(String nextUpdate) {
        this.nextUpdate = nextUpdate;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public PlayedOut getPlayedOut() {
        return playedOut;
    }

    public void setPlayedOut(PlayedOut playedOut) {
        this.playedOut = playedOut;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }
}
