package com.smallcase.android.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shashankm on 26/12/16.
 */

public class Holdings {
    @SerializedName("returns")
    private Returns returns;

    public Returns getReturns() {
        return returns;
    }

    public void setReturns(Returns returns) {
        this.returns = returns;
    }

    @Override
    public String toString() {
        return "ClassPojo [returns = " + returns + "]";
    }
}
