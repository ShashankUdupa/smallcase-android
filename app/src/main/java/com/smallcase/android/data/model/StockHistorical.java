package com.smallcase.android.data.model;

/**
 * Created by shashankm on 19/12/16.
 */

public class StockHistorical {
    private float price;

    private String change;

    private String high;

    private String date;

    private String low;

    private String close;

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    @Override
    public String toString() {
        return "ClassPojo [price = " + price + ", change = " + change + ", high = " + high + ", date = " + date + ", low = " + low + ", close = " + close + "]";
    }
}
