package com.smallcase.android.data.model;

/**
 * Created by shashankm on 26/12/16.
 */

public class Profile {
    private String phone;

    private String email;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "ClassPojo [phone = " + phone + ", email = " + email + "]";
    }
}
