package com.smallcase.android.data.model;

/**
 * Created by shashankm on 19/12/16.
 */

public class StockData {
    private StockInfo stockInfo;
    private StockPriceAndChange stockPriceAndChange;

    public StockData(StockInfo stockInfo, StockPriceAndChange stockPriceAndChange) {
        this.stockInfo = stockInfo;
        this.stockPriceAndChange = stockPriceAndChange;
    }

    public StockInfo getStockInfo() {
        return stockInfo;
    }

    public StockPriceAndChange getStockPriceAndChange() {
        return stockPriceAndChange;
    }
}
