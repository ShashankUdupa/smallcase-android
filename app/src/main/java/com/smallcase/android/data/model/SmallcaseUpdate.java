package com.smallcase.android.data.model;

/**
 * Created by shashankm on 14/02/17.
 */

public class SmallcaseUpdate {
    private String label;

    private String rationale;

    private String date;

    private int version;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRationale() {
        return rationale;
    }

    public void setRationale(String rationale) {
        this.rationale = rationale;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
