package com.smallcase.android.data.model;

/**
 * Created by shashankm on 29/09/17.
 */

public class DraftSmallcases {
    private String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
