package com.smallcase.android.data.model;

import java.util.List;

/**
 * Created by shashankm on 14/02/17.
 */

public class UnInvestedSmallcase {
    private String _id;

    private String rationale;

    private String scid;

    private List<Segments> segments;

    private List<Constituents> constituents;

    private SmallcaseFlags flags;

    private SmallcaseInfo info;

    private String __v;

    private SmallcaseStats stats;

    private int version;

    private List<SmallcaseUpdate> updates;

    private String newsTag;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getRationale() {
        return rationale;
    }

    public void setRationale(String rationale) {
        this.rationale = rationale;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    public List<Segments> getSegments() {
        return segments;
    }

    public void setSegments(List<Segments> segments) {
        this.segments = segments;
    }

    public List<Constituents> getConstituents() {
        return constituents;
    }

    public void setConstituents(List<Constituents> constituents) {
        this.constituents = constituents;
    }

    public SmallcaseFlags getFlags() {
        return flags;
    }

    public void setFlags(SmallcaseFlags flags) {
        this.flags = flags;
    }

    public SmallcaseInfo getInfo() {
        return info;
    }

    public void setInfo(SmallcaseInfo info) {
        this.info = info;
    }

    public String get__v() {
        return __v;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }

    public SmallcaseStats getStats() {
        return stats;
    }

    public void setStats(SmallcaseStats stats) {
        this.stats = stats;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public List<SmallcaseUpdate> getUpdates() {
        return updates;
    }

    public void setUpdates(List<SmallcaseUpdate> updates) {
        this.updates = updates;
    }

    public String getNewsTag() {
        return newsTag;
    }

    public void setNewsTag(String newsTag) {
        this.newsTag = newsTag;
    }
}
