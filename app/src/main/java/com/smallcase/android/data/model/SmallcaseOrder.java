package com.smallcase.android.data.model;

import java.util.List;

/**
 * Created by shashankm on 07/03/17.
 */

public class SmallcaseOrder {
    private String iscid;

    private String source;

    private String scid;

    private List<SmallcaseOrders> orders;

    private String label;

    private Integer version;

    private String did;

    public String getIscid() {
        return iscid;
    }

    public void setIscid(String iscid) {
        this.iscid = iscid;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    public List<SmallcaseOrders> getOrders() {
        return orders;
    }

    public void setOrders(List<SmallcaseOrders> orders) {
        this.orders = orders;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "{ \"iscid: \"" + iscid + ", \"scid: \"" + scid + ", \"source: \"" + source + ", \"label: \"" + label;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
