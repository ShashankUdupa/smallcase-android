package com.smallcase.android.data.model;

/**
 * Created by shashankm on 14/12/16.
 */

public class FailedBatch {
    private String sellAmount;

    private String buyAmount;

    public String getSellAmount() {
        return sellAmount;
    }

    public void setSellAmount(String sellAmount) {
        this.sellAmount = sellAmount;
    }

    public String getBuyAmount() {
        return buyAmount;
    }

    public void setBuyAmount(String buyAmount) {
        this.buyAmount = buyAmount;
    }

    @Override
    public String toString() {
        return "ClassPojo [sellAmount = " + sellAmount + ", buyAmount = " + buyAmount + "]";
    }
}
