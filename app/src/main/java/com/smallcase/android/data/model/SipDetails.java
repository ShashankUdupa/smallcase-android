package com.smallcase.android.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by shashankm on 06/09/17.
 */

public class SipDetails implements Parcelable {

    private String iscid;

    private String scid;

    private String name;

    private String source;

    private String scheduledDate;

    private String frequency;

    private double amount;

    public SipDetails(Parcel in) {
        iscid = in.readString();
        scid = in.readString();
        name = in.readString();
        source = in.readString();
        scheduledDate = in.readString();
        frequency = in.readString();
        amount = in.readDouble();
    }

    public static final Creator<SipDetails> CREATOR = new Creator<SipDetails>() {
        @Override
        public SipDetails createFromParcel(Parcel in) {
            return new SipDetails(in);
        }

        @Override
        public SipDetails[] newArray(int size) {
            return new SipDetails[size];
        }
    };

    public String getIscid() {
        return iscid;
    }

    public void setIscid(String iscid) {
        this.iscid = iscid;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(String scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(iscid);
        dest.writeString(scid);
        dest.writeString(name);
        dest.writeString(source);
        dest.writeString(scheduledDate);
        dest.writeString(frequency);
        dest.writeDouble(amount);
    }
}
