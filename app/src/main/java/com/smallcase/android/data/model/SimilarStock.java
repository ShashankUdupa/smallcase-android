package com.smallcase.android.data.model;

/**
 * Created by shashankm on 07/04/17.
 */

public class SimilarStock {
    private String sid;

    private StockSearchInfo sidInfo;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public StockSearchInfo getSidInfo() {
        return sidInfo;
    }

    public void setSidInfo(StockSearchInfo sidInfo) {
        this.sidInfo = sidInfo;
    }
}
