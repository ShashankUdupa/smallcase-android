package com.smallcase.android.data.model;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by shashankm on 18/11/16.
 */

public class Returns implements Parcelable {
    private double realizedInvestment;
    private double realizedReturns;
    private double otherReturns;
    private double networth;
    private double unrealizedInvestment;
    private double divReturns;

    protected Returns(Parcel in) {
        realizedInvestment = in.readDouble();
        realizedReturns = in.readDouble();
        otherReturns = in.readDouble();
        networth = in.readDouble();
        unrealizedInvestment = in.readDouble();
        divReturns = in.readDouble();
    }

    public static final Creator<Returns> CREATOR = new Creator<Returns>() {
        @Override
        public Returns createFromParcel(Parcel in) {
            return new Returns(in);
        }

        @Override
        public Returns[] newArray(int size) {
            return new Returns[size];
        }
    };

    public double getRealizedInvestment() {
        return realizedInvestment;
    }

    public void setRealizedInvestment(double realizedInvestment) {
        this.realizedInvestment = realizedInvestment;
    }

    public double getRealizedReturns() {
        return realizedReturns;
    }

    public void setRealizedReturns(double realizedReturns) {
        this.realizedReturns = realizedReturns;
    }

    public double getOtherReturns() {
        return otherReturns;
    }

    public void setOtherReturns(double otherReturns) {
        this.otherReturns = otherReturns;
    }

    public double getNetworth() {
        return networth;
    }

    public void setNetworth(double networth) {
        this.networth = networth;
    }

    public double getUnrealizedInvestment() {
        return unrealizedInvestment;
    }

    public void setUnrealizedInvestment(double unrealizedInvestment) {
        this.unrealizedInvestment = unrealizedInvestment;
    }

    public double getDivReturns() {
        return divReturns;
    }

    public void setDivReturns(double divReturns) {
        this.divReturns = divReturns;
    }

    @Override
    public String toString() {
        return "ClassPojo [realizedInvestment = " + realizedInvestment + ", realizedReturns = " + realizedReturns + ", otherReturns = " + otherReturns + ", networth = " + networth + ", unrealizedInvestment = " + unrealizedInvestment + ", divReturns = " + divReturns + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(realizedInvestment);
        dest.writeDouble(realizedReturns);
        dest.writeDouble(otherReturns);
        dest.writeDouble(networth);
        dest.writeDouble(unrealizedInvestment);
        dest.writeDouble(divReturns);
    }
}
