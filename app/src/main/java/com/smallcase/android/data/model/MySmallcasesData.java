package com.smallcase.android.data.model;

import java.util.List;

/**
 * Created by shashankm on 08/12/16.
 */

public class MySmallcasesData {
    private Investments investments;
    private TotalInvestment totalInvestment;
    private List<ExitedSmallcase> exitedSmallcases;

    public MySmallcasesData(Investments investments, TotalInvestment totalInvestment, List<ExitedSmallcase> exitedSmallcases) {
        this.investments = investments;
        this.totalInvestment = totalInvestment;
        this.exitedSmallcases = exitedSmallcases;
    }

    public Investments getInvestments() {
        return investments;
    }

    public TotalInvestment getTotalInvestment() {
        return totalInvestment;
    }

    public List<ExitedSmallcase> getExitedSmallcases() {
        return exitedSmallcases;
    }
}
