package com.smallcase.android.data.model;

/**
 * Created by shashankm on 26/12/16.
 */

public class Markers {
    private String smallcaseRationale;

    private String manageInvestment;

    private String navbarDiscover;

    private String smallcaseStocks;

    private String discoverTypes;

    public String getSmallcaseRationale() {
        return smallcaseRationale;
    }

    public void setSmallcaseRationale(String smallcaseRationale) {
        this.smallcaseRationale = smallcaseRationale;
    }

    public String getManageInvestment() {
        return manageInvestment;
    }

    public void setManageInvestment(String manageInvestment) {
        this.manageInvestment = manageInvestment;
    }

    public String getNavbarDiscover() {
        return navbarDiscover;
    }

    public void setNavbarDiscover(String navbarDiscover) {
        this.navbarDiscover = navbarDiscover;
    }

    public String getSmallcaseStocks() {
        return smallcaseStocks;
    }

    public void setSmallcaseStocks(String smallcaseStocks) {
        this.smallcaseStocks = smallcaseStocks;
    }

    public String getDiscoverTypes() {
        return discoverTypes;
    }

    public void setDiscoverTypes(String discoverTypes) {
        this.discoverTypes = discoverTypes;
    }

    @Override
    public String toString() {
        return "ClassPojo [smallcaseRationale = " + smallcaseRationale + ", manageInvestment = " + manageInvestment + ", navbarDiscover = " + navbarDiscover + ", smallcaseStocks = " + smallcaseStocks + ", discoverTypes = " + discoverTypes + "]";
    }
}
