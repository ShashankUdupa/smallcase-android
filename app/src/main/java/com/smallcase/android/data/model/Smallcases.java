package com.smallcase.android.data.model;


/**
 * Created by shashankm on 06/12/16.
 */

public class Smallcases {
    private String _id;

    private double initialIndex;

    private SmallcaseIndex stats;

    private String scid;

    private Info info;

    public double getInitialIndex() {
        return initialIndex;
    }

    public void setInitialIndex(double initialIndex) {
        this.initialIndex = initialIndex;
    }

    public SmallcaseIndex getStats() {
        return stats;
    }

    public void setStats(SmallcaseIndex stats) {
        this.stats = stats;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    @Override
    public String toString() {
        return "ClassPojo [initialIndex = " + initialIndex + ", stats = " + stats + ", scid = " + scid + ", info = " + info + "]";
    }
}
