package com.smallcase.android.data.model;

/**
 * Created by shashankm on 26/12/16.
 */

public class UserFlags {
    private Markers markers;

    private String updatedProfile;

    public Markers getMarkers() {
        return markers;
    }

    public void setMarkers(Markers markers) {
        this.markers = markers;
    }

    public String getUpdatedProfile() {
        return updatedProfile;
    }

    public void setUpdatedProfile(String updatedProfile) {
        this.updatedProfile = updatedProfile;
    }

    @Override
    public String toString() {
        return "ClassPojo [markers = " + markers + ", updatedProfile = " + updatedProfile + "]";
    }
}
