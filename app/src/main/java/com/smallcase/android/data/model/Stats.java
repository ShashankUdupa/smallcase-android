package com.smallcase.android.data.model;


/**
 * Created by shashankm on 18/11/16.
 */

public class Stats {
    private double lastWeekCloseIndex;

    private String pe;

    private double lastToLastWeekCloseIndex;

    private double indexValue;

    private double lastCloseIndex;

    public double getLastWeekCloseIndex() {
        return lastWeekCloseIndex;
    }

    public void setLastWeekCloseIndex(double lastWeekCloseIndex) {
        this.lastWeekCloseIndex = lastWeekCloseIndex;
    }

    public String getPe() {
        return pe;
    }

    public void setPe(String pe) {
        this.pe = pe;
    }

    public double getLastToLastWeekCloseIndex() {
        return lastToLastWeekCloseIndex;
    }

    public void setLastToLastWeekCloseIndex(double lastToLastWeekCloseIndex) {
        this.lastToLastWeekCloseIndex = lastToLastWeekCloseIndex;
    }

    public double getIndexValue() {
        return indexValue;
    }

    public void setIndexValue(double indexValue) {
        this.indexValue = indexValue;
    }

    public double getLastCloseIndex() {
        return lastCloseIndex;
    }

    public void setLastCloseIndex(double lastCloseIndex) {
        this.lastCloseIndex = lastCloseIndex;
    }

    @Override
    public String toString() {
        return "ClassPojo [lastWeekCloseIndex = " + lastWeekCloseIndex + ", pe = " + pe + ", lastToLastWeekCloseIndex = " + lastToLastWeekCloseIndex + ", indexValue = " + indexValue + ", lastCloseIndex = " + lastCloseIndex + "]";
    }
}
