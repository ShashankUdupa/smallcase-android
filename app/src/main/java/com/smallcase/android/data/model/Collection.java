package com.smallcase.android.data.model;

import java.util.List;

/**
 * Created by shashankm on 30/01/17.
 */

public class Collection {
    private String rank;

    private String _id;

    private int smallcaseCount;

    private String description;

    private String name;

    private String __v;

    private String type;

    private String cid;

    private List<Groups> groups;

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getSmallcaseCount() {
        return smallcaseCount;
    }

    public void setSmallcaseCount(int smallcaseCount) {
        this.smallcaseCount = smallcaseCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String get__v() {
        return __v;
    }

    public void set__v(String __v) {
        this.__v = __v;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public List<Groups> getGroups() {
        return groups;
    }

    public void setGroups(List<Groups> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return "ClassPojo [rank = " + rank + ", _id = " + _id + ", smallcaseCount = " + smallcaseCount + ", description = " + description + ", name = " + name + ", __v = " + __v + ", type = " + type + ", cid = " + cid + ", groups = " + groups + "]";
    }
}
