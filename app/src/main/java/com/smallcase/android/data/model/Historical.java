package com.smallcase.android.data.model;

import java.util.List;

/**
 * Created by shashankm on 28/02/17.
 */

public class Historical {
    private String scid;

    private String _id;

    private List<Points> point;

    private List<Points> points;

    private Benchmark benchmark;

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    public List<Points> getPoints() {
        return points;
    }

    public void setPoints(List<Points> points) {
        this.points = points;
    }

    public Benchmark getBenchmark() {
        return benchmark;
    }

    public void setBenchmark(Benchmark benchmark) {
        this.benchmark = benchmark;
    }

    public List<Points> getPoint() {
        return point;
    }

    public void setPoint(List<Points> point) {
        this.point = point;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
}
