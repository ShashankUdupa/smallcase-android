package com.smallcase.android.data.model;

/**
 * Created by shashankm on 24/01/17.
 */

public class Smallcase {
    private SmallcaseFlags flags;

    private String _id;

    private SmallcaseStats stats;

    private SmallcaseInfo info;

    private String scid;

    public SmallcaseFlags getFlags() {
        return flags;
    }

    public void setFlags(SmallcaseFlags flags) {
        this.flags = flags;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public SmallcaseStats getStats() {
        return stats;
    }

    public void setStats(SmallcaseStats stats) {
        this.stats = stats;
    }

    public SmallcaseInfo getInfo() {
        return info;
    }

    public void setInfo(SmallcaseInfo info) {
        this.info = info;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    @Override
    public String toString() {
        return "ClassPojo [flags = " + flags + ", _id = " + _id + ", stats = " + stats + ", info = " + info + ", scid = " + scid + "]";
    }
}
