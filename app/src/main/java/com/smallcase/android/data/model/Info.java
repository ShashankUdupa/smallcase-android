package com.smallcase.android.data.model;

import java.io.Serializable;

/**
 * Created by shashankm on 06/12/16.
 */

public class Info implements Serializable {
    private String shortDescription;

    private String name;

    private String dateUpdated;

    private String tier;

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ClassPojo [shortDescription = " + shortDescription + ", name = " + name + "]";
    }

    public String getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(String dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }
}
