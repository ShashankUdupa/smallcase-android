package com.smallcase.android.data.model;

/**
 * Created by shashankm on 06/04/17.
 */

public class SearchResult {
    private String sid;

    private StockSearch stock;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public StockSearch getStock() {
        return stock;
    }

    public void setStock(StockSearch stock) {
        this.stock = stock;
    }
}
