package com.smallcase.android.data.model;

/**
 * Created by shashankm on 16/11/16.
 */

public class Nifty {
    private double price, change, close;
    private String date;

    public Nifty(double price, double change, double close, String date) {
        this.price = price;
        this.change = change;
        this.close = close;
        this.date = date;
    }

    public double getPrice() {
        return price;
    }

    public double getChange() {
        return change;
    }

    public String getDate() {
        return date;
    }

    public double getClose() {
        return close;
    }
}
