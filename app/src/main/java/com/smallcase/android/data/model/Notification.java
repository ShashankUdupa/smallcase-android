package com.smallcase.android.data.model;

import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.SmallcaseSource;

/**
 * Created by shashankm on 14/04/17.
 */

public class Notification {
    private String _id;

    private String source;

    private String scid;

    private String iscid;

    private boolean read;

    private String time;

    private String link;

    private String type;

    private String text;

    private String header;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    public String getIscid() {
        return iscid;
    }

    public void setIscid(String iscid) {
        this.iscid = iscid;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getImage(String res) {
        return (SmallcaseSource.CREATED.equals(source) ? AppConstants
                .CREATED_SMALLCASE_IMAGE_URL : AppConstants.SMALLCASE_IMAGE_URL) + res + "/" + scid + ".png";
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
