package com.smallcase.android.data.model;

/**
 * Created by shashankm on 26/12/16.
 */

public class UserMeta {
    private Dna dna;

    private String dateCreated;

    private OptOuts optOuts;

    private Profile profile;

    public Dna getDna() {
        return dna;
    }

    public void setDna(Dna dna) {
        this.dna = dna;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public OptOuts getOptOuts() {
        return optOuts;
    }

    public void setOptOuts(OptOuts optOuts) {
        this.optOuts = optOuts;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    @Override
    public String toString() {
        return "ClassPojo [dna = " + dna + ", dateCreated = " + dateCreated + ", optOuts = " + optOuts + ", profile = " + profile + "]";
    }
}
