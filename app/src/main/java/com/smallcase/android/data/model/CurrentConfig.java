package com.smallcase.android.data.model;

import java.util.List;

/**
 * Created by shashankm on 14/12/16.
 */

public class CurrentConfig {
    private Segments[] segments;

    private List<Constituents> constituents;

    public Segments[] getSegments() {
        return segments;
    }

    public void setSegments(Segments[] segments) {
        this.segments = segments;
    }

    public List<Constituents> getConstituents() {
        return constituents;
    }

    public void setConstituents(List<Constituents> constituents) {
        this.constituents = constituents;
    }
}
