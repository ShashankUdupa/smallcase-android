package com.smallcase.android.data.model;

import java.io.Serializable;

/**
 * Created by shashankm on 14/12/16.
 */

public class SidInfo implements Serializable {
    private String sector;

    private String ticker;

    private String nseSeries;

    private String description;

    private String name;

    private String exchange;

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getNseSeries() {
        return nseSeries;
    }

    public void setNseSeries(String nseSeries) {
        this.nseSeries = nseSeries;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    @Override
    public String toString() {
        return "ClassPojo [sector = " + sector + ", ticker = " + ticker + ", nseSeries = " + nseSeries + ", description = " + description + ", name = " + name + ", exchange = " + exchange + "]";
    }
}
