package com.smallcase.android.data.model;

/**
 * Created by shashankm on 19/12/16.
 */

public class StockPriceAndChange {
    private Double price;
    private String change;
    private Double low;
    private Double high;
    private Double close;
    private String date;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }

    public Double getClose() {
        return close;
    }

    public void setClose(Double close) {
        this.close = close;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "price: " + price + "\n" + "change: " + change + "\n" + "low: " + low + "\n" + "high: " + high + "\n" + "close: " + close + "\n" + "date: " + date + "\n";
    }
}
