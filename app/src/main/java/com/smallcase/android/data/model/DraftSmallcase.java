package com.smallcase.android.data.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by shashankm on 29/05/17.
 */

public class DraftSmallcase implements Serializable {

    private List<Constituents> constituents;

    private String did;

    private List<Segments> segments;

    private Info info;

    private CustomizedSmallcaseStats stats;

    private String _id;

    private String compositionScheme;

    private String source;

    private String scid;

    public List<Constituents> getConstituents() {
        return constituents;
    }

    public void setConstituents(List<Constituents> constituents) {
        this.constituents = constituents;
    }

    public List<Segments> getSegments() {
        return segments;
    }

    public void setSegments(List<Segments> segments) {
        this.segments = segments;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public CustomizedSmallcaseStats getStats() {
        return stats;
    }

    public void setStats(CustomizedSmallcaseStats stats) {
        this.stats = stats;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCompositionScheme() {
        return compositionScheme;
    }

    public void setCompositionScheme(String compositionScheme) {
        this.compositionScheme = compositionScheme;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{ did: ").append(_id)
                .append(", source: ").append(source)
                .append(", segments: {[");
        for (Segments segment : segments) {
            stringBuilder.append(" label: ").append(segment.getLabel())
                    .append(", constituents: [");
            for (String constituent : segment.getConstituents()) {
                stringBuilder.append(constituent).append(", ");
            }
        }
        stringBuilder.append("], compositionScheme: ").append(compositionScheme)
                    .append(", constituents: [{ ");
        for (Constituents constituent : constituents) {
            stringBuilder.append("sid: ").append(constituent.getSid())
                    .append(", shares: ").append(constituent.getShares())
                    .append(", weight").append(constituent.getWeight())
                    .append(", locked").append(constituent.isLocked()).append("}, ");
        }
        return stringBuilder.toString();
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }
}
