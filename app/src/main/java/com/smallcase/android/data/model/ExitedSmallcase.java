package com.smallcase.android.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by shashankm on 08/12/16.
 */

public class ExitedSmallcase implements Parcelable {
    private String source;

    private String _id;

    private String name;

    private Returns returns;

    private String date;

    private String dateSold;

    private String scid;

    private String smallcaseImage;


    protected ExitedSmallcase(Parcel in) {
        source = in.readString();
        _id = in.readString();
        name = in.readString();
        returns = in.readParcelable(Returns.class.getClassLoader());
        date = in.readString();
        dateSold = in.readString();
        scid = in.readString();
        smallcaseImage = in.readString();
    }

    public static final Creator<ExitedSmallcase> CREATOR = new Creator<ExitedSmallcase>() {
        @Override
        public ExitedSmallcase createFromParcel(Parcel in) {
            return new ExitedSmallcase(in);
        }

        @Override
        public ExitedSmallcase[] newArray(int size) {
            return new ExitedSmallcase[size];
        }
    };

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Returns getReturns() {
        return returns;
    }

    public void setReturns(Returns returns) {
        this.returns = returns;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateSold() {
        return dateSold;
    }

    public void setDateSold(String dateSold) {
        this.dateSold = dateSold;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    @Override
    public String toString() {
        return "ClassPojo [source = " + source + ", _id = " + _id + ", name = " + name + ", returns = " + returns + ", date = " + date + ", dateSold = " + dateSold + ", scid = " + scid + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(source);
        dest.writeString(_id);
        dest.writeString(name);
        dest.writeParcelable(returns, flags);
        dest.writeString(date);
        dest.writeString(dateSold);
        dest.writeString(scid);
        dest.writeString(smallcaseImage);
    }

}
