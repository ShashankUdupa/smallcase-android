package com.smallcase.android.data.model;

/**
 * Created by shashankm on 24/07/17.
 */

public class Marker {
    private boolean mobileIndexValue;

    private boolean mobileWatchlist;

    private boolean mobileStockInfo;

    private boolean premiumPopup2;

    public boolean isMobileIndexValue() {
        return mobileIndexValue;
    }

    public void setMobileIndexValue(boolean mobileIndexValue) {
        this.mobileIndexValue = mobileIndexValue;
    }

    public boolean isMobileWatchlist() {
        return mobileWatchlist;
    }

    public void setMobileWatchlist(boolean mobileWatchlist) {
        this.mobileWatchlist = mobileWatchlist;
    }

    public boolean isMobileStockInfo() {
        return mobileStockInfo;
    }

    public void setMobileStockInfo(boolean mobileStockInfo) {
        this.mobileStockInfo = mobileStockInfo;
    }

    public boolean isPremiumPopup() {
        return premiumPopup2;
    }

    public void setPremiumPopup(boolean premiumPopup) {
        this.premiumPopup2 = premiumPopup;
    }
}
