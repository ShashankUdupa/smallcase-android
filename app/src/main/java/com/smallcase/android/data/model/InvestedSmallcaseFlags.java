package com.smallcase.android.data.model;

/**
 * Created by shashankm on 05/09/17.
 */

public class InvestedSmallcaseFlags {
    private boolean sip;

    public boolean isSip() {
        return sip;
    }

    public void setSip(boolean sip) {
        this.sip = sip;
    }
}
