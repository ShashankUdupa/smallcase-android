package com.smallcase.android.data.model;

import java.util.List;

/**
 * Created by shashankm on 07/12/16.
 */

public class DashboardData {
    private Nifty nifty;
    private TotalInvestment totalInvestment;
    private List<News> newsList;

    public DashboardData(Nifty nifty, TotalInvestment totalInvestment, List<News> newsList) {
        this.nifty = nifty;
        this.totalInvestment = totalInvestment;
        this.newsList = newsList;
    }

    public Nifty getNifty() {
        return nifty;
    }

    public TotalInvestment getTotalInvestment() {
        return totalInvestment;
    }

    public List<News> getNewsList() {
        return newsList;
    }
}
