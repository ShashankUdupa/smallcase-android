package com.smallcase.android.data.model;

/**
 * Created by shashankm on 03/03/17.
 */
public class PlayedOut {
    private String date;

    private String label;

    private String rationale;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRationale() {
        return rationale;
    }

    public void setRationale(String rationale) {
        this.rationale = rationale;
    }
}
