package com.smallcase.android.data.model;

import java.util.List;

/**
 * Created by shashankm on 07/04/17.
 */

public class Similar {
    private String _id;

    private List<SimilarStock> stocks;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public List<SimilarStock> getStocks() {
        return stocks;
    }

    public void setStocks(List<SimilarStock> stocks) {
        this.stocks = stocks;
    }
}
