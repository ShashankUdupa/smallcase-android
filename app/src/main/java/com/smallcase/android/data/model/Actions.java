package com.smallcase.android.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by shashankm on 24/05/17.
 */

public class Actions {

    @SerializedName("rebalance")
    private List<ReBalanceUpdate> reBalanceUpdates;

    @SerializedName("buy")
    private List<Buy> buys;

    @SerializedName("sip")
    private List<SipDetails> sips;

    public List<ReBalanceUpdate> getReBalanceUpdates() {
        return reBalanceUpdates;
    }

    public void setReBalanceUpdates(List<ReBalanceUpdate> reBalanceUpdates) {
        this.reBalanceUpdates = reBalanceUpdates;
    }

    public List<Buy> getBuys() {
        return buys;
    }

    public void setBuys(List<Buy> buys) {
        this.buys = buys;
    }

    public List<SipDetails> getSips() {
        return sips;
    }

    public void setSips(List<SipDetails> sips) {
        this.sips = sips;
    }
}
