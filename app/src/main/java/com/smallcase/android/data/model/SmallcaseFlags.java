package com.smallcase.android.data.model;

/**
 * Created by shashankm on 24/01/17.
 */
public class SmallcaseFlags {
    private Popular popular;

    private boolean active;

    private Featured featured;

    private Highlighted highlighted;

    public Popular getPopular() {
        return popular;
    }

    public void setPopular(Popular popular) {
        this.popular = popular;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Highlighted getHighlighted() {
        return highlighted;
    }

    public void setHighlighted(Highlighted highlighted) {
        this.highlighted = highlighted;
    }

    @Override
    public String toString() {
        return "ClassPojo [popular = " + popular + ", active = " + active + ", highlighted = " + highlighted + "]";
    }

    public Featured isFeatured() {
        return featured;
    }

    public void setFeatured(Featured featured) {
        this.featured = featured;
    }
}
