package com.smallcase.android.data.model;

/**
 * Created by shashankm on 19/12/16.
 */

public class Stock {
    private Nse nse;

    private Ratios ratios;

    private Gic gic;

    private Indicators indicators;

    private Bse bse;

    private StockSectorInfo info;

    private String isin;

    public Nse getNse() {
        return nse;
    }

    public void setNse(Nse nse) {
        this.nse = nse;
    }

    public Ratios getRatios() {
        return ratios;
    }

    public void setRatios(Ratios ratios) {
        this.ratios = ratios;
    }

    public Gic getGic() {
        return gic;
    }

    public void setGic(Gic gic) {
        this.gic = gic;
    }

    public Indicators getIndicators() {
        return indicators;
    }

    public void setIndicators(Indicators indicators) {
        this.indicators = indicators;
    }

    public Bse getBse() {
        return bse;
    }

    public void setBse(Bse bse) {
        this.bse = bse;
    }

    public StockSectorInfo getInfo() {
        return info;
    }

    public void setInfo(StockSectorInfo info) {
        this.info = info;
    }

    public String getIsin() {
        return isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }

    @Override
    public String toString() {
        return "ClassPojo [nse = " + nse + ", ratios = " + ratios + ", gic = " + gic + ", indicators = " + indicators + ", bse = " + bse + ", info = " + info + ", isin = " + isin + "]";
    }
}
