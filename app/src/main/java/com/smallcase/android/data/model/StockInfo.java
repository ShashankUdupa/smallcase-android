package com.smallcase.android.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by shashankm on 19/12/16.
 */

public class StockInfo {
    private String sid;

    private Stock stock;

    @SerializedName("historical")
    private List<StockHistorical> stockHistoricals;

    private String type;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<StockHistorical> getStockHistoricals() {
        return stockHistoricals;
    }

    public void setStockHistoricals(List<StockHistorical> stockHistoricals) {
        this.stockHistoricals = stockHistoricals;
    }
}
