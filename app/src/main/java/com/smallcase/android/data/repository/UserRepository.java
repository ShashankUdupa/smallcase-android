package com.smallcase.android.data.repository;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.smallcase.android.api.ApiClient;
import com.smallcase.android.api.ApiRoutes;
import com.smallcase.android.data.BaseObjectConverter;
import com.smallcase.android.data.model.Actions;
import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.model.Notification;
import com.smallcase.android.data.model.SmallcaseLedger;
import com.smallcase.android.data.model.User;
import com.smallcase.android.user.ReBalanceParser;
import com.smallcase.android.user.account.UserFeeConverter;
import com.smallcase.android.user.coustomize.DraftConverter;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shashankm on 26/12/16.
 */

public class UserRepository {
    private static final RxJavaCallAdapterFactory FACTORY =  RxJavaCallAdapterFactory.create();

    public Observable<User> getUserInfo(String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildUserConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getUserDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> registerDeviceForNotification(String auth, HashMap<String, String> body, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .registerDeviceForPushNotification(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> deRegisterDeviceForNotification(String auth, HashMap<String, String> body, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .deRegisterDeviceFromPushNotification(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> setSettings(String auth, HashMap<String, Object> body, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .setSettings(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> getSettings(String auth, String setting, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getSettings(setting)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> logoutUser(String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .logoutUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<SmallcaseLedger>> getFees(String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildFeesConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getFees()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<Notification>> getNotifications(String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildNotificationsConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getNotifications()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> markNotificationAsRead(String auth, String csrf, HashMap<String, String> body) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .markNotificationAsRead(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> markAllNotificationAsRead(String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .markAllNotificationAsRead()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<Actions> getPendingActions(String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildReBalanceUpdatesConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getUserUpdates()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> ignoreAllBuyReminder(String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildDraftsConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .ignoreAllBuyReminders()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> setFlag(String auth, String csrf, HashMap<String, Object> body) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildDraftsConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .setFlag(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> updateProfile(String auth, String csrf, HashMap<String, Object> body) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildDraftsConverter())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .updateProfile(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<ResponseBody> getUpdateStatus(String app, Integer version, String os) {
        return ApiClient.getClient(null, null)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(FACTORY)
                .build().create(ApiRoutes.class)
                .getUpdateStatus(app, version, os)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Converter.Factory buildDraftsConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type listType = new TypeToken<List<DraftSmallcase>>(){}.getType();
        gsonBuilder.registerTypeAdapter(listType, new DraftConverter());
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildReBalanceUpdatesConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Actions.class, new ReBalanceParser());
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildNotificationsConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type listType = new TypeToken<List<Notification>>(){}.getType();
        gsonBuilder.registerTypeAdapter(listType, new BaseObjectConverter<List<Notification>>());
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildFeesConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type listType = new TypeToken<List<SmallcaseLedger>>(){}.getType();
        gsonBuilder.registerTypeAdapter(listType, new UserFeeConverter());
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildUserConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(User.class, new BaseObjectConverter<User>());
        return GsonConverterFactory.create(gsonBuilder.create());
    }
}
