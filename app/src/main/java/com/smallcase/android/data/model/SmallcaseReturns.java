package com.smallcase.android.data.model;

/**
 * Created by shashankm on 24/01/17.
 */
public class SmallcaseReturns {
    private double yearly;

    private double monthly;

    private double daily;

    public double getYearly() {
        return yearly;
    }

    public void setYearly(double yearly) {
        this.yearly = yearly;
    }

    public double getMonthly() {
        return monthly;
    }

    public void setMonthly(double monthly) {
        this.monthly = monthly;
    }

    public double getDaily() {
        return daily;
    }

    public void setDaily(double daily) {
        this.daily = daily;
    }

    @Override
    public String toString() {
        return "ClassPojo [yearly = " + yearly + ", monthly = " + monthly + ", daily = " + daily + "]";
    }
}
