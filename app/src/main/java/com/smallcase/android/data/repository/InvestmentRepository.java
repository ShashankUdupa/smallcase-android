package com.smallcase.android.data.repository;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.smallcase.android.api.ApiClient;
import com.smallcase.android.api.ApiRoutes;
import com.smallcase.android.data.BaseObjectConverter;
import com.smallcase.android.data.model.ExitedSmallcase;
import com.smallcase.android.data.model.Investments;
import com.smallcase.android.data.model.TotalInvestment;
import com.smallcase.android.investedsmallcase.exited.ExitedSmallcaseConverter;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Converter;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shashankm on 18/11/16.
 */

public class InvestmentRepository {

    public Observable<Investments> getInvestments(String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildInvestmentsConverter())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build().create(ApiRoutes.class)
                .getInvestedScallcases()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<TotalInvestment> getTotalInvestments(String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildTotalInvestmentsConverter())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build().create(ApiRoutes.class)
                .getInvestmentsTotal()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<ExitedSmallcase>> getExitedSmallcases(String auth, String csrf) {
        return ApiClient.getClient(auth, csrf)
                .addConverterFactory(buildExitedSmallcaseConverter())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build().create(ApiRoutes.class)
                .getExitedSmallcases()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Converter.Factory buildExitedSmallcaseConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Type listType = new TypeToken<List<ExitedSmallcase>>(){}.getType();
        gsonBuilder.registerTypeAdapter(listType, new ExitedSmallcaseConverter());
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildInvestmentsConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Investments.class, new BaseObjectConverter<Investments>());
        return GsonConverterFactory.create(gsonBuilder.create());
    }

    private Converter.Factory buildTotalInvestmentsConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(TotalInvestment.class, new BaseObjectConverter<TotalInvestment>());
        return GsonConverterFactory.create(gsonBuilder.create());
    }
}
