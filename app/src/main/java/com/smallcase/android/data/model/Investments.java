package com.smallcase.android.data.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shashankm on 06/12/16.
 */

public class Investments {

    private List<InvestedSmallcases> investedSmallcases;

    private String _id;

    public List<InvestedSmallcases> getInvestedSmallcases() {
        return investedSmallcases;
    }

    public void setInvestedSmallcases(ArrayList<InvestedSmallcases> investedSmallcases) {
        this.investedSmallcases = investedSmallcases;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

}
