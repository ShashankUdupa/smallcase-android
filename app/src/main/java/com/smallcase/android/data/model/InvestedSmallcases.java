package com.smallcase.android.data.model;

/**
 * Created by shashankm on 06/12/16.
 */

public class InvestedSmallcases {
    private String source;

    private String _id;

    private String status;

    private Stats stats;

    private String name;

    private Returns returns;

    private String date;

    private String scid;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Returns getReturns() {
        return returns;
    }

    public void setReturns(Returns returns) {
        this.returns = returns;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getScid() {
        return scid;
    }

    public void setScid(String scid) {
        this.scid = scid;
    }

    @Override
    public String toString() {
        return "ClassPojo [source = " + source + ", _id = " + _id + ", status = " + status + ", stats = " + stats + ", name = " + name + ", returns = " + returns + ", date = " + date + ", scid = " + scid + "]";
    }
}
