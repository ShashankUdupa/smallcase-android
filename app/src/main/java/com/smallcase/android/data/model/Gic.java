package com.smallcase.android.data.model;

/**
 * Created by shashankm on 19/12/16.
 */

public class Gic {
    private String sector;

    private String industrygroup;

    private String subindustry;

    private String industry;

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getIndustrygroup() {
        return industrygroup;
    }

    public void setIndustrygroup(String industrygroup) {
        this.industrygroup = industrygroup;
    }

    public String getSubindustry() {
        return subindustry;
    }

    public void setSubindustry(String subindustry) {
        this.subindustry = subindustry;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    @Override
    public String toString() {
        return "ClassPojo [sector = " + sector + ", industrygroup = " + industrygroup + ", subindustry = " + subindustry + ", industry = " + industry + "]";
    }
}
