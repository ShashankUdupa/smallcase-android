package com.smallcase.android.api;


import com.smallcase.android.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

/**
 * Created by shashankm on 08/11/16.
 */
public class ApiClient {
    private static final String BASE_URL = BuildConfig.BASE_URL;

    private static OkHttpClient.Builder httpClient;

    public static Retrofit.Builder getClient(final String jwt, final String csrf) {
        if (httpClient == null) {
            httpClient = new OkHttpClient.Builder();
        }

        if (!httpClient.interceptors().isEmpty()) {
            httpClient.interceptors().clear();
        }
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        // add logging as last interceptor
        httpClient.addInterceptor(logging);

        if (jwt != null && csrf != null) {
            httpClient.addInterceptor(chain -> {
                Request.Builder builder = chain.request().newBuilder();
                String cookie = "jwt=" + jwt;
                builder.header("Cookie", cookie);
                builder.header("X-CSRF-Token", csrf);
                builder.header("x-sc-source", "android");
                return chain.proceed(builder.build());
            });
        }

        return new Retrofit.Builder().baseUrl(BASE_URL)
                .client(httpClient.build());
    }

    public static Retrofit.Builder getPlainClient(String baseUrl, final String jwt) {
        if (httpClient == null) {
            httpClient = new OkHttpClient.Builder();
        }

        if (!httpClient.interceptors().isEmpty()) {
            httpClient.interceptors().clear();
        }
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        // add logging as last interceptor
        httpClient.addInterceptor(logging);

        if (jwt != null) {
            httpClient.addInterceptor(chain -> {
                Request.Builder builder = chain.request().newBuilder();
                String cookie = "jwt_trd=" + jwt;
                builder.header("Cookie", cookie);
                return chain.proceed(builder.build());
            });
        }

        return new Retrofit.Builder().baseUrl(baseUrl).client(httpClient.build());
    }
}
