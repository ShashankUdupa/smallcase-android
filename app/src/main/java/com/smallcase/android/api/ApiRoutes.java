package com.smallcase.android.api;

import android.support.annotation.Nullable;

import com.smallcase.android.data.model.Actions;
import com.smallcase.android.data.model.Collection;
import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.model.ExitedSmallcase;
import com.smallcase.android.data.model.Historical;
import com.smallcase.android.data.model.ImageUploadOptions;
import com.smallcase.android.data.model.Investments;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.Nifty;
import com.smallcase.android.data.model.Notification;
import com.smallcase.android.data.model.Order;
import com.smallcase.android.data.model.SearchResult;
import com.smallcase.android.data.model.Similar;
import com.smallcase.android.data.model.SipDetails;
import com.smallcase.android.data.model.Smallcase;
import com.smallcase.android.data.model.SmallcaseDetail;
import com.smallcase.android.data.model.SmallcaseLedger;
import com.smallcase.android.data.model.SmallcaseOrder;
import com.smallcase.android.data.model.StockInfo;
import com.smallcase.android.data.model.StockPriceAndChange;
import com.smallcase.android.data.model.TotalInvestment;
import com.smallcase.android.data.model.UnInvestedSmallcase;
import com.smallcase.android.data.model.User;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Contains all the routes for communicating with server.
 */
public interface ApiRoutes {

    @POST("auth/brokerLogin")
    Observable<ResponseBody> brokerLogin(@Body HashMap<String, String> body);

    @GET("auth/getRequestToken")
    Observable<ResponseBody> getRequestToken(@Query("app") String app);

    @POST("auth/smallcase")
    Observable<ResponseBody> loginSmallcase(@Body HashMap<String, String> body);

    @POST("auth/applyForAccount")
    Observable<ResponseBody> requestAccount(@Body HashMap<String, Object> body);

    @GET("user/sc/getUser")
    Observable<User> getUserDetails();

    @GET("market/priceandchange")
    Observable<Nifty> getMarketPriceAndChange(@Query("stocks") String stocks);

    @GET("user/sc/investments/total")
    Observable<TotalInvestment> getInvestmentsTotal();

    @GET("user/sc/investments")
    Observable<Investments> getInvestedScallcases();

    @GET("news/getNews")
    Observable<List<News>> getNewsForInvestedSmallcase(@Query("scids") List<String> scids, @Query("offset")
            int offset, @Query("count") int count, @Query("sids") List<String> sids);

    @GET("user/sc/investments/exited")
    Observable<List<ExitedSmallcase>> getExitedSmallcases();

    @GET("user/sc/investments/smallcase")
    Observable<SmallcaseDetail> getInvestedSmallcaseDetails(@Query("iscid") String iScid);

    @GET("market/price")
    Observable<ResponseBody> getStocksPrice(@Query("stocks") List<String> sids);

    @GET("market/stocks/getInfoAndHistorical")
    Observable<StockInfo> getStockInfoAndHistorical(@Query("sid") String sid);

    @GET("market/stocks/info")
    Observable<ResponseBody> getStockInfo(@Query("stocks") List<String> stocks);

    @GET("market/priceAndChange")
    Observable<StockPriceAndChange> getStockPriceAndChange(@Query("stocks") String sid);

    @POST("user/sc/registerDevice")
    Observable<ResponseBody> registerDeviceForPushNotification(@Body HashMap<String, String> body);

    @POST("user/sc/deregisterDevice")
    Observable<ResponseBody> deRegisterDeviceFromPushNotification(@Body HashMap<String, String> body);

    @GET("user/sc/getSetting")
    Observable<ResponseBody> getSettings(@Query("setting") String settingsType);

    @POST("user/sc/setSetting")
    Observable<ResponseBody> setSettings(@Body HashMap<String, Object> body);

    @GET("smallcases/highlighted")
    Observable<List<Smallcase>> getFeaturedSmallcases();

    @GET("smallcases/discover")
    Observable<List<Smallcase>> getSmallcases(@Query("type") List<String> types, @Query("scids") List<String>
            scids, @Query("sortBy") String sortBy, @Query("sortOrder") int sortOrder, @Query("offset") int offSet,
            @Query("count") int count, @Query("minMinInvestAmount") @Nullable Integer minMinInvestAmount, @Query("maxMinInvestAmount")
            @Nullable Integer maxMinInvestAmount, @Query("searchString") String searchString, @Query("sids") String
            sid, @Query("tier") @Nullable String tier);

    @GET("smallcases/collections")
    Observable<List<Collection>> getCollections();

    @POST("auth/logout")
    Observable<ResponseBody> logoutUser();

    @GET("smallcases/smallcase")
    Observable<UnInvestedSmallcase> getSmallcaseDetails(@Query("scid") String scid);

    @GET("smallcases/historical")
    Observable<Historical> getSmallcaseHistorical(@Query("scid") String scid, @Query("benchmarkType")
            String benchmarkType, @Query("benchmarkId") String benchmarkId, @Query("duration") String duration);

    @GET("user/sc/funds")
    Observable<ResponseBody> getAvailableFunds();

    @POST("user/sc/placeOrders")
    Observable<ResponseBody> placeOrder(@Body SmallcaseOrder smallcaseOrder);

    @GET("market/status")
    Observable<ResponseBody> getIfMarketIsOpen();

    @POST("user/sc/watchlist/add")
    Observable<ResponseBody> addToWatchlist(@Body HashMap<String, String> body);

    @POST("user/sc/watchlist/remove")
    Observable<ResponseBody> removeFromWatchlist(@Body HashMap<String, String> body);

    @GET("user/sc/orders")
    Observable<List<Order>> getOrderDetails(@Query("iscid") String iScid, @Query("batchId") String batchId,
                                            @Query("onlyOne") boolean onlyOne, @Query("noDetails") boolean noDetails);

    @POST("user/sc/cancelBatch")
    Observable<ResponseBody> cancelBatch(@Body HashMap<String, String> body);

    @POST("user/sc/fixBatch")
    Observable<ResponseBody> fixBatch(@Body HashMap<String, String> body);

    @GET("market/search")
    Observable<List<SearchResult>> getStocksInfo(@Query("text") String searchText);

    @GET("market/stocks/similar")
    Observable<List<Similar>> getSimilarStocks(@Query("stocks") List<String> stocks);

    @GET("user/sc/fees")
    Observable<List<SmallcaseLedger>> getFees();

    @GET("user/sc/notifications")
    Observable<List<Notification>> getNotifications();

    @POST("user/sc/notifications/read")
    Observable<ResponseBody> markNotificationAsRead(@Body HashMap<String, String> body);

    @POST("user/sc/notifications/readAll")
    Observable<ResponseBody> markAllNotificationAsRead();

    @POST("user/sc/addReminder")
    Observable<ResponseBody> addReminder(@Body HashMap<String, String> body);

    @GET("user/sc/actions")
    Observable<Actions> getUserUpdates();

    @POST("user/sc/actions/rebalance/cancel")
    Observable<ResponseBody> cancelUpdates(@Body HashMap<String, String> body);

    @POST("user/sc/actions/rebalance/noOrders")
    Observable<ResponseBody> noOrders(@Body HashMap<String, Object> body);

    @GET("user/sc/drafts")
    Observable<List<DraftSmallcase>> getDrafts();

    @GET("user/sc/drafts/draft")
    Observable<DraftSmallcase> getDraft(@Query("did") String did);

    @POST("user/sc/drafts/save")
    Observable<ResponseBody> saveDrafts(@Body DraftSmallcase draftSmallcase);

    @POST("user/sc/drafts/delete")
    Observable<ResponseBody> deleteDraft(@Body HashMap<String, String> body);

    @POST("user/sc/actions/reminders/ignoreAll")
    Observable<ResponseBody> ignoreAllBuyReminders();

    @POST("user/sc/setFlag")
    Observable<ResponseBody> setFlag(@Body HashMap<String, Object> body);

    @POST("user/sc/updateProfile")
    Observable<ResponseBody> updateProfile(@Body HashMap<String, Object> body);

    @GET("user/sc/actions/sip/get")
    Observable<SipDetails> getSipDetails(@Query("iscid") String iscid);

    @POST("user/sc/actions/sip/create")
    Observable<ResponseBody> setUpSip(@Body HashMap<String, String> body);

    @POST("user/sc/actions/sip/manage")
    Observable<ResponseBody> manageSip(@Body HashMap<String, String> body);

    @POST("user/sc/actions/sip/end")
    Observable<ResponseBody> endSip(@Body HashMap<String, String> body);

    @POST("user/sc/actions/sip/ignore")
    Observable<ResponseBody> skipSip(@Body HashMap<String, String> body);

    @GET("market/historical")
    Observable<ResponseBody> getStockHistorical(@Query("stocks") List<String> stocks, @Query("duration") String duration);

    @GET("external/app/forceUpdate")
    Observable<ResponseBody> getUpdateStatus(@Query("app") String app, @Query("version") Integer version, @Query("os") String os);

    @GET("user/sc/drafts/getImageUploadConfig")
    Observable<ImageUploadOptions> getImageUploadConfig(@Query("scid") String scid);

    @Multipart
    @POST("/")
    Observable<ResponseBody> uploadDraftImage(@PartMap LinkedHashMap<String, RequestBody> partMap, @Part MultipartBody.Part file);
}
