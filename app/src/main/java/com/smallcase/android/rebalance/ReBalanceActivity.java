package com.smallcase.android.rebalance;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.repository.AuthRepository;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.kite.KiteHelper;
import com.smallcase.android.onboarding.LoginActivity;
import com.smallcase.android.smallcase.LoginRequest;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.stock.StockInfo;
import com.smallcase.android.user.ContainerActivity;
import com.smallcase.android.user.order.BatchDetailsActivity;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.CustomTypefaceSpan;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.ReBalanceStock;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.smallcase.android.onboarding.LoginActivity.REQUEST_TOKEN_KEY;

public class ReBalanceActivity extends AppCompatActivity implements ReBalanceContract.View, SmallcaseHelper.LoginResponse {
    public static final String SCID = "scid";
    public static final String ISCID = "iscid";
    public static final String TITLE = "title";
    public static final String DATE = "date";
    public static final String LABEL = "label";
    public static final String VERSION = "version";
    public static final String SOURCE = "source";

    private static final String TAG = ReBalanceActivity.class.getSimpleName();
    private static final int LOGIN_REQUEST_CODE = 1;

    @BindView(R.id.text_tool_bar_title)
    GraphikText toolBarTitle;
    @BindView(R.id.loading_indicator)
    View loadingIndicator;
    @BindView(R.id.nested_scroll_view)
    NestedScrollView nestedScrollView;
    @BindView(R.id.re_balance_title)
    GraphikText reBalanceTitle;
    @BindView(R.id.re_balance_date)
    GraphikText reBalanceDate;
    @BindView(R.id.re_balance_rationale)
    GraphikText reBalanceRationale;
    @BindView(R.id.smallcase_name)
    GraphikText smallcaseNameText;
    @BindView(R.id.stocks_change_list)
    RecyclerView reBalanceChangesList;
    @BindView(R.id.parent)
    View parent;
    @BindView(R.id.no_updates)
    GraphikText noUpdates;
    @BindView(R.id.smallcase_image)
    ImageView smallcaseImage;

    @BindView(R.id.add_funds_layout)
    View addFundsLayout;
    @BindView(R.id.required_amount)
    GraphikText requiredAmountText;
    @BindView(R.id.add_funds_blur)
    View addFundsBlur;
    @BindView(R.id.confirm_container)
    View confirmContainer;
    @BindView(R.id.confirm_description)
    GraphikText confrimDescription;
    @BindView(R.id.confirm)
    GraphikText confirm;
    @BindView(R.id.blur)
    View blur;
    @BindView(R.id.batches_unfilled_popup)
    View batchesUnfilledPopUp;
    @BindView(R.id.rebalance_list_card)
    View reBalanceListCard;
    @BindView(R.id.positive_button_text)
    GraphikText positiveButtonText;
    @BindView(R.id.skip_update)
    View skipUpdate;

    private ReBalanceContract.Presenter presenter;

    private List<Object> reBalanceList;
    private String smallcaseName, scid, iscid, source;
    private ProgressDialog progressDialog;
    private LoginRequest loginRequest;
    private BottomSheetBehavior<View> requiredAmountBottomSheet, confirmBottomSheet;
    private StockInfo stockInfo;
    private AnalyticsContract analyticsContract;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_re_balance_update);

        ButterKnife.bind(this);

        toolBarTitle.setText("Apply Rebalance Update");
        reBalanceChangesList.setLayoutManager(new LinearLayoutManager(this));
        reBalanceChangesList.setHasFixedSize(true);
        reBalanceChangesList.setNestedScrollingEnabled(false);
        reBalanceListCard.setVisibility(View.GONE);

        requiredAmountBottomSheet = BottomSheetBehavior.from(addFundsLayout);
        requiredAmountBottomSheet.setHideable(true);
        requiredAmountBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);

        confirmBottomSheet = BottomSheetBehavior.from(confirmContainer);
        confirmBottomSheet.setHideable(true);
        confirmBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);

        smallcaseName = getIntent().getStringExtra(TITLE);
        scid = getIntent().getStringExtra(SCID);
        iscid = getIntent().getStringExtra(ISCID);
        source = getIntent().getStringExtra(SOURCE);

        SharedPrefService sharedPrefService = new SharedPrefService(this);
        KiteRepository kiteRepository = new KiteRepository();
        StockHelper stockHelper = new StockHelper();
        MarketRepository marketRepository = new MarketRepository();
        presenter = new ReBalancePresenter(this, new SmallcaseRepository(), sharedPrefService, this, marketRepository,
                stockHelper, scid, iscid, getIntent().getIntExtra(VERSION, -1), new KiteHelper(sharedPrefService, kiteRepository),
                source, new UserSmallcaseRepository());
        loginRequest = new SmallcaseHelper(new AuthRepository(), this, sharedPrefService);
        stockInfo = new StockInfo(this, stockHelper, marketRepository, sharedPrefService);

        analyticsContract = new AnalyticsManager(getApplicationContext());
        presenter.fetchSmallcaseDetails();

        smallcaseNameText.setText(smallcaseName);
        reBalanceTitle.setText(getIntent().getStringExtra(LABEL));
        reBalanceDate.setText("Came on: " + new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault()).format(AppUtils
                .getInstance().getPlainDate(getIntent().getStringExtra(DATE))));
        Glide.with(this)
                .load(AppConstants.SMALLCASE_IMAGE_URL + "80/" + scid + ".png")
                .into(smallcaseImage);

        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.PLACED_ORDER_BROADCAST));
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (stockInfo.isBottomSheetOpen()) {
            stockInfo.setBottomSheetState(BottomSheetBehavior.STATE_HIDDEN);
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        stockInfo.unBind();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
        presenter.destroySubscriptions();
        if (snackbar != null) snackbar.dismiss();
        super.onDestroy();
    }

    @Override
    public void showSnackBar(@StringRes int resId) {
        snackbar = Snackbar.make(parent, resId, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @OnClick(R.id.confirm_changes)
    public void confirmChanges() {
        if (reBalanceListCard.getVisibility() == View.GONE) {
            new Animations().expand(reBalanceListCard, new AccelerateDecelerateInterpolator(), 300);
            positiveButtonText.setText("CONFIRM CHANGES");
            return;
        }

        Map<String, Object> metaData = new HashMap<>();
        metaData.put("updateDate", reBalanceDate.getText().toString());
        metaData.put("updateType", reBalanceTitle.getText().toString());
        metaData.put("smallcaseName", smallcaseName);
        metaData.put("smallcaseTier", presenter.getSmallcaseTier());
        analyticsContract.sendEvent(metaData, "Applied Rebalance Update", Analytics.MIXPANEL, Analytics.INTERCOM, Analytics.CLEVERTAP);

        progressDialog = ProgressDialog.show(this, "", "Loading...");
        progressDialog.show();
        presenter.checkMarketStatus();
    }

    @OnClick(R.id.customize_stocks)
    public void customizeStocksClicked() {
        Intent intent = new Intent(this, CustomizeReBalanceActivity.class);
        intent.putParcelableArrayListExtra(CustomizeReBalanceActivity.RE_BALANCE_STOCKS, (ArrayList<? extends Parcelable>) presenter.getOnlyReBalanceStocks(reBalanceList));
        intent.putExtra(CustomizeReBalanceActivity.SMALLCASE_NAME, smallcaseName);
        intent.putExtra(CustomizeReBalanceActivity.SCID, presenter.getScid());
        intent.putExtra(CustomizeReBalanceActivity.ISCID, presenter.getIScid());
        intent.putExtra(CustomizeReBalanceActivity.SOURCE, presenter.getSource());
        startActivity(intent);
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (AppConstants.PLACED_ORDER_BROADCAST.equals(intent.getAction())) {
                if (!isFinishing()) finish();
            }
        }
    };

    @Override
    public List<Object> getReBalanceStocks() {
        return reBalanceList;
    }

    @Override
    public String getSmallcaseName() {
        return smallcaseName;
    }

    @Override
    public void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            //Received request_token from Kite login. Request authToken from server.
            HashMap<String, String> body = new HashMap<>();
            body.put("broker", "kite");
            body.put("reqToken", data.getStringExtra(REQUEST_TOKEN_KEY));
            body.put("app", "platform");

            loginRequest.getJwtToken(body);
        }
    }

    @Override
    public void showNoUpdatesAvailable() {
        loadingIndicator.setVisibility(View.GONE);
        noUpdates.setVisibility(View.VISIBLE);
    }

    @Override
    public void sendUserToKiteLogin() {
        startActivityForResult(new Intent(this, LoginActivity.class),
                LOGIN_REQUEST_CODE);
        overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    }

    @OnClick(R.id.add_funds)
    public void redirectUserToAddFunds() {
        analyticsContract.sendEvent(null, "Proceeded To Adding Funds", Analytics.INTERCOM, Analytics.CLEVERTAP, Analytics.MIXPANEL);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(AppConstants.KITE_ADD_FUNDS));
        startActivity(intent);
    }

    @Override
    public void showNeedMoreFunds(SpannableString spannableString) {
        hideProgressDialog();
        addFundsBlur.setClickable(true);
        addFundsBlur.setVisibility(View.VISIBLE);
        addFundsBlur.setOnClickListener(v -> requiredAmountBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN));

        requiredAmountBottomSheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    addFundsBlur.setClickable(false);
                    addFundsBlur.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        requiredAmountText.setText(spannableString);
        requiredAmountBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    public void setRationale(String rationale) {
        reBalanceRationale.setText(rationale);
    }

    @Override
    public void showBatchesRemainingToBeFixed() {
        blur.setVisibility(View.VISIBLE);
        blur.setClickable(true);
        batchesUnfilledPopUp.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.fix)
    public void onFixClicked() {
        Intent intent = new Intent(ReBalanceActivity.this, BatchDetailsActivity.class);
        intent.putExtra(BatchDetailsActivity.SMALLCASE_NAME, smallcaseName);
        intent.putExtra(BatchDetailsActivity.SCID, scid);
        intent.putExtra(BatchDetailsActivity.ISCID, iscid);
        intent.putExtra(BatchDetailsActivity.SOURCE, source);
        startActivity(intent);
        finish();
    }

    @Override
    public void onShowReBalancedList(List<Object> reBalanceList) {
        loadingIndicator.setVisibility(View.GONE);
        nestedScrollView.setVisibility(View.VISIBLE);
        this.reBalanceList = reBalanceList;
        reBalanceChangesList.setAdapter(new ReBalanceAdapter());
    }

    @OnClick(R.id.confirm_card)
    public void confirmIgnoringUpdateClicked() {
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("updateDate", reBalanceDate.getText().toString());
        eventProperties.put("updateLabel", reBalanceTitle.getText().toString());
        eventProperties.put("smallcaseName", reBalanceTitle.getText().toString());
        eventProperties.put("smallcaseTier", presenter.getSmallcaseTier());
        analyticsContract.sendEvent(eventProperties, "Skipped Rebalance Update", Analytics.MIXPANEL, Analytics.INTERCOM, Analytics.CLEVERTAP);

        confirmBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
        progressDialog = ProgressDialog.show(ReBalanceActivity.this, "", "Loading...");
        progressDialog.show();
        presenter.ignoreReBalance();
    }

    @OnClick(R.id.skip_update)
    public void onIgnoreReBalanceClicked() {
        addFundsBlur.setClickable(true);
        addFundsBlur.setVisibility(View.VISIBLE);
        addFundsBlur.setOnClickListener(v -> confirmBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN));

        confirmBottomSheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    addFundsBlur.setClickable(false);
                    addFundsBlur.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        confrimDescription.setText("Skipping the rebalance update does not remove the rebalance update and only will remove the " +
                "pending notification\n\nYou can apply the last available rebalance update whenever you like on web (even after skipping)");
        confirm.setText("SKIP UPDATE");
        confirmBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    public void onJwtSavedSuccessfully() {
        confirmChanges();
    }

    @Override
    public void onFailedFetchingJwt() {
        showSnackBar(R.string.could_not_login);
    }

    @Override
    public void onDifferentUserLoggedIn() {
        Intent intent = new Intent(this, ContainerActivity.class);
        intent.putExtra(ContainerActivity.NEW_USER, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    class ReBalanceAdapter extends RecyclerView.Adapter<ReBalanceAdapter.ViewHolder> {
        private boolean isShares = true;

        @Override
        public ReBalanceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.re_balance_stock_card,
                    parent, false));
        }

        @Override
        public void onBindViewHolder(final ReBalanceAdapter.ViewHolder holder, int position) {
            if (position == 0) {
                holder.segmentColor.setVisibility(View.GONE);
                holder.segmentTitle.setVisibility(View.GONE);
                holder.stockName.setVisibility(View.VISIBLE);
                holder.oldConfig.setVisibility(View.VISIBLE);
                holder.newConfig.setVisibility(View.VISIBLE);
                holder.moreIcon.setVisibility(View.VISIBLE);

                holder.stockName.setText("Stock");
                holder.oldConfig.setText("Current Qty");
                holder.newConfig.setText("New Qty");

                holder.moreIcon.setOnClickListener(v -> {
                    PopupMenu popupMenu = new PopupMenu(ReBalanceActivity.this, holder.moreIcon, Gravity.NO_GRAVITY,
                            R.attr.actionOverflowMenuStyle, 0);
                    popupMenu.inflate(R.menu.notification_menu);

                    MenuItem menuItem = popupMenu.getMenu().getItem(0);
                    SpannableString title = new SpannableString(isShares ? "Show weightage" : "Show shares");
                    title.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ReBalanceActivity.this, R.color.primary_text)), 0, title.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                    title.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(ReBalanceActivity.this, AppConstants.FontType.REGULAR)), 0, title.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                    title.setSpan(new RelativeSizeSpan(0.9f), 0, title.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                    menuItem.setTitle(title);

                    popupMenu.setOnMenuItemClickListener(item -> {
                        switch (item.getItemId()) {
                            case R.id.mark_all_as_read:
                                isShares = !isShares;
                                notifyDataSetChanged();
                                return true;
                        }
                        return false;
                    });
                    popupMenu.show();
                });

                holder.stockName.setTextColor(ContextCompat.getColor(ReBalanceActivity.this, R.color.secondary_text));
                holder.oldConfig.setTextColor(ContextCompat.getColor(ReBalanceActivity.this, R.color.secondary_text));
                holder.newConfig.setTextColor(ContextCompat.getColor(ReBalanceActivity.this, R.color.secondary_text));

                holder.reBalanceStockCard.setBackgroundColor(ContextCompat.getColor(ReBalanceActivity.this, R.color.white));
                return;
            }

            if (reBalanceList.get(position - 1) instanceof String) {
                String title = (String) reBalanceList.get(position - 1);

                holder.segmentColor.setVisibility(View.VISIBLE);
                holder.segmentTitle.setVisibility(View.VISIBLE);
                holder.stockName.setVisibility(View.GONE);
                holder.oldConfig.setVisibility(View.GONE);
                holder.newConfig.setVisibility(View.GONE);
                holder.moreIcon.setVisibility(View.GONE);

                holder.reBalanceStockCard.setOnClickListener(null);
                holder.segmentTitle.setText(title);
                GradientDrawable drawable = (GradientDrawable) holder.segmentColor.getBackground();
                switch (title) {
                    case "Stocks you will buy":
                        drawable.setColor(ContextCompat.getColor(ReBalanceActivity.this, R.color.green_600));
                        holder.reBalanceStockCard.setBackgroundColor(ContextCompat.getColor(ReBalanceActivity.this, R.color.success_green));
                        break;

                    case "Stocks you will sell":
                        drawable.setColor(ContextCompat.getColor(ReBalanceActivity.this, R.color.red_600));
                        holder.reBalanceStockCard.setBackgroundColor(ContextCompat.getColor(ReBalanceActivity.this, R.color.error_red));
                        break;

                    case "Stocks unchanged":
                        drawable.setColor(ContextCompat.getColor(ReBalanceActivity.this, R.color.secondary_text));
                        holder.reBalanceStockCard.setBackgroundColor(ContextCompat.getColor(ReBalanceActivity.this, R.color.grey_100));
                        break;
                }
                return;
            }

            if (reBalanceList.get(position - 1) instanceof ReBalanceStock) {
                final ReBalanceStock reBalanceStock = (ReBalanceStock) reBalanceList.get(position - 1);

                holder.segmentColor.setVisibility(View.INVISIBLE);
                holder.segmentTitle.setVisibility(View.GONE);
                holder.stockName.setVisibility(View.VISIBLE);
                holder.oldConfig.setVisibility(View.VISIBLE);
                holder.newConfig.setVisibility(View.VISIBLE);
                holder.moreIcon.setVisibility(View.INVISIBLE);

                holder.stockName.setText(reBalanceStock.getStockName());
                if (isShares) {
                    holder.oldConfig.setText(String.valueOf((int) reBalanceStock.getOldShares()));
                    holder.newConfig.setText(String.valueOf((int) reBalanceStock.getNewShares()));
                } else {
                    holder.oldConfig.setText(String.format(Locale.getDefault(), "%.2f", reBalanceStock.getOldWeight()) + "%");
                    holder.newConfig.setText(String.format(Locale.getDefault(), "%.2f", reBalanceStock.getNewWeight()) + "%");
                }

                holder.stockName.setTextColor(ContextCompat.getColor(ReBalanceActivity.this, R.color.primary_text));
                holder.oldConfig.setTextColor(ContextCompat.getColor(ReBalanceActivity.this, R.color.primary_text));
                holder.newConfig.setTextColor(ContextCompat.getColor(ReBalanceActivity.this, R.color.primary_text));

                holder.reBalanceStockCard.setBackgroundColor(ContextCompat.getColor(ReBalanceActivity.this, R.color.white));
                holder.reBalanceStockCard.setOnClickListener(v -> stockInfo.showStockInfo(reBalanceStock.getSid()));
            }
        }

        @Override
        public int getItemCount() {
            return reBalanceList.size() + 1;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.segment_color)
            View segmentColor;
            @BindView(R.id.segment_title)
            GraphikText segmentTitle;
            @BindView(R.id.stock_name)
            GraphikText stockName;
            @BindView(R.id.old_config)
            GraphikText oldConfig;
            @BindView(R.id.new_config)
            GraphikText newConfig;
            @BindView(R.id.more_icon)
            View moreIcon;
            @BindView(R.id.re_balance_stock_card)
            View reBalanceStockCard;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
