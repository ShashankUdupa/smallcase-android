package com.smallcase.android.rebalance;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smallcase.android.R;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.repository.AuthRepository;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.kite.KiteHelper;
import com.smallcase.android.onboarding.LoginActivity;
import com.smallcase.android.smallcase.LoginRequest;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.stock.SearchStockPopUp;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.stock.StockInfo;
import com.smallcase.android.user.ContainerActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.ReBalanceStock;
import com.smallcase.android.view.model.Stock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.smallcase.android.onboarding.LoginActivity.REQUEST_TOKEN_KEY;

public class CustomizeReBalanceActivity extends AppCompatActivity implements SearchStockPopUp.AddStockPopUpSubscriber,
        CustomizeReBalanceContract.View, SmallcaseHelper.LoginResponse {
    public static final String RE_BALANCE_STOCKS = "re_balance_stocks";
    public static final String SMALLCASE_NAME = "smallcase_name";
    public static final String SCID = "scid";
    public static final String ISCID = "iscid";
    public static final String SOURCE = "source";
    private static final int LOGIN_REQUEST_CODE = 1;
    private static final String TAG = CustomizeReBalanceActivity.class.getSimpleName();

    @BindView(R.id.text_tool_bar_title)
    GraphikText toolBarTitle;
    @BindView(R.id.stock_change_list)
    RecyclerView stockChangesList;
    @BindView(R.id.parent)
    View parent;
    @BindView(R.id.add_funds_layout)
    View addFundsLayout;
    @BindView(R.id.required_amount)
    GraphikText requiredAmountText;
    @BindView(R.id.blur)
    View blur;

    private List<ReBalanceStock> reBalanceStocks;
    private CustomizeAdapter customizeAdapter;
    private SearchStockPopUp searchStockPopUp;
    private CustomizeReBalanceContract.Presenter presenter;
    private List<ReBalanceStock> originalReBalanceStocks;
    private BottomSheetBehavior<View> requiredAmountBottomSheet;
    private ProgressDialog progressDialog;
    private LoginRequest loginRequest;
    private StockInfo stockInfo;
    private AnalyticsContract analyticsContract;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customize_re_balance);

        ButterKnife.bind(this);
        toolBarTitle.setText("Edit Changes");

        originalReBalanceStocks = getIntent().getParcelableArrayListExtra(RE_BALANCE_STOCKS);
        reBalanceStocks = new ArrayList<>();
        for (ReBalanceStock originalReBalanceStock : originalReBalanceStocks) {
            reBalanceStocks.add(new ReBalanceStock(originalReBalanceStock));
        }

        stockChangesList.setLayoutManager(new LinearLayoutManager(this));
        stockChangesList.setNestedScrollingEnabled(false);
        RecyclerView.ItemAnimator animator = stockChangesList.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        customizeAdapter = new CustomizeAdapter();
        stockChangesList.setAdapter(customizeAdapter);

        requiredAmountBottomSheet = BottomSheetBehavior.from(addFundsLayout);
        requiredAmountBottomSheet.setHideable(true);
        requiredAmountBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);

        analyticsContract = new AnalyticsManager(getApplicationContext());

        SharedPrefService sharedPrefService = new SharedPrefService(this);
        KiteRepository kiteRepository = new KiteRepository();
        MarketRepository marketRepository = new MarketRepository();
        StockHelper stockHelper = new StockHelper();
        presenter = new CustomizeReBalancePresenter(this, marketRepository, sharedPrefService, this, stockHelper,
                getIntent().getStringExtra(SCID), getIntent().getStringExtra(ISCID), getIntent().getStringExtra(SMALLCASE_NAME),
                new KiteHelper(sharedPrefService, kiteRepository), getIntent().getStringExtra(SOURCE), analyticsContract);
        searchStockPopUp = new SearchStockPopUp(this, this);
        loginRequest = new SmallcaseHelper(new AuthRepository(), this, sharedPrefService);
        stockInfo = new StockInfo(this, stockHelper, marketRepository, sharedPrefService);

        searchStockPopUp.setStockList(presenter.convertToStockModelList(reBalanceStocks));
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.PLACED_ORDER_BROADCAST));
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.HARD_RELOAD_BROADCAST));
    }

    @Override
    public void onStockSelected(Stock stock) {
        boolean isStockPresent = false;
        for (ReBalanceStock availableStock : reBalanceStocks) {
            if (availableStock.getSid().equals(stock.getSid())) {
                isStockPresent = true;
                break;
            }
        }

        if (isStockPresent) {
            showSnackBar(R.string.stock_already_present);
            return;
        }

        if (reBalanceStocks.size() >= 20) {
            showSnackBar(R.string.not_more_than_twenty);
            return;
        }

        ReBalanceStock reBalanceStock = new ReBalanceStock();
        reBalanceStock.setSid(stock.getSid());
        reBalanceStock.setStockName(stock.getStockName());
        reBalanceStock.setOldShares(0);
        reBalanceStock.setNewShares(1);
        reBalanceStocks.add(reBalanceStock);
        customizeAdapter.notifyItemInserted(reBalanceStocks.size());
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (stockInfo.isBottomSheetOpen()) {
            stockInfo.setBottomSheetState(BottomSheetBehavior.STATE_HIDDEN);
            return;
        }

        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in_with_scale, R.anim.right_to_left);
    }

    @Override
    protected void onDestroy() {
        stockInfo.unBind();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
        presenter.destroySubscriptions();
        super.onDestroy();
    }

    @OnClick(R.id.add_stock)
    public void addStockClicked() {
        searchStockPopUp.showStockPopUp();
    }

    @OnClick(R.id.reset_changes)
    public void resetChanges() {
        reBalanceStocks.clear();
        for (ReBalanceStock originalReBalanceStock : originalReBalanceStocks) {
            reBalanceStocks.add(new ReBalanceStock(originalReBalanceStock));
        }
        customizeAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.confirm_changes)
    public void confirmChanges() {
        progressDialog = ProgressDialog.show(this, "", "Loading...");
        progressDialog.show();
        presenter.checkMarketStatus();
    }

    @Override
    public void showSnackBar(@StringRes int resId) {
        AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_LONG, resId);
    }

    @Override
    public List<ReBalanceStock> getReBalanceStocks() {
        return reBalanceStocks;
    }

    @Override
    public void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            //Received request_token from Kite login. Request authToken from server.
            HashMap<String, String> body = new HashMap<>();
            body.put("broker", "kite");
            body.put("reqToken", data.getStringExtra(REQUEST_TOKEN_KEY));
            body.put("app", "platform");

            loginRequest.getJwtToken(body);
        }
    }

    @Override
    public void sendUserToKiteLogin() {
        startActivityForResult(new Intent(this, LoginActivity.class),
                LOGIN_REQUEST_CODE);
        overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (isFinishing()) return;

            String action = intent.getAction();
            if (AppConstants.PLACED_ORDER_BROADCAST.equals(action)) {
                finish();
            }

            if (AppConstants.HARD_RELOAD_BROADCAST.equals(action)) {
                Intent homeIntent = new Intent(CustomizeReBalanceActivity.this, ContainerActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                finish();
            }
        }
    };

    @Override
    public void showNeedMoreFunds(SpannableString spannableString) {
        hideProgressDialog();
        blur.setClickable(true);
        blur.setVisibility(View.VISIBLE);
        blur.setOnClickListener(v -> requiredAmountBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN));

        requiredAmountBottomSheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    blur.setClickable(false);
                    blur.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        requiredAmountText.setText(spannableString);
        requiredAmountBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    public void onJwtSavedSuccessfully() {
        confirmChanges();
    }

    @Override
    public void onFailedFetchingJwt() {
        showSnackBar(R.string.could_not_login);
    }

    @Override
    public void onDifferentUserLoggedIn() {
        Intent intent = new Intent(this, ContainerActivity.class);
        intent.putExtra(ContainerActivity.NEW_USER, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    class CustomizeAdapter extends RecyclerView.Adapter<CustomizeAdapter.ViewHolder> {

        @Override
        public CustomizeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.customize_re_balance_card, parent, false));
        }

        @Override
        public void onBindViewHolder(final CustomizeAdapter.ViewHolder holder, int position) {
            if (position == 0) {
                holder.plusIcon.setVisibility(View.INVISIBLE);
                holder.minusIcon.setVisibility(View.INVISIBLE);

                holder.stockName.setText("Stock");
                holder.oldShares.setText("Old Shares");
                holder.newShares.setText("New Shares");
                return;
            }

            holder.plusIcon.setVisibility(View.VISIBLE);
            holder.minusIcon.setVisibility(View.VISIBLE);

            final ReBalanceStock reBalanceStock = reBalanceStocks.get(position - 1);
            holder.stockName.setText(reBalanceStock.getStockName());
            holder.oldShares.setText(String.valueOf((int) reBalanceStock.getOldShares()));
            holder.newShares.setText(String.valueOf((int) reBalanceStock.getNewShares()));
            holder.plusIcon.setOnClickListener(v -> {
                reBalanceStock.setNewShares(reBalanceStock.getNewShares() + 1);
                notifyItemChanged(holder.getAdapterPosition());
            });

            holder.minusIcon.setOnClickListener(v -> {
                if (reBalanceStock.getNewShares() == 0) return;

                reBalanceStock.setNewShares(reBalanceStock.getNewShares() - 1);
                notifyItemChanged(holder.getAdapterPosition());
            });

            holder.stockName.setOnClickListener(v -> stockInfo.showStockInfo(reBalanceStock.getSid()));
        }

        @Override
        public int getItemCount() {
            return reBalanceStocks.size() + 1;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.stock_name)
            GraphikText stockName;
            @BindView(R.id.old_config)
            GraphikText oldShares;
            @BindView(R.id.new_config)
            GraphikText newShares;
            @BindView(R.id.plus_icon)
            View plusIcon;
            @BindView(R.id.minus_icon)
            View minusIcon;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
