package com.smallcase.android.rebalance;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;

import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Constituents;
import com.smallcase.android.data.model.SmallcaseDetail;
import com.smallcase.android.data.model.SmallcaseUpdate;
import com.smallcase.android.data.model.StockListWithMinAmount;
import com.smallcase.android.data.model.UnInvestedSmallcase;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.kite.KiteContract;
import com.smallcase.android.kite.KiteHelper;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.user.order.ReviewOrderActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.OrderLabel;
import com.smallcase.android.util.StockArrayList;
import com.smallcase.android.view.model.ReBalanceStock;
import com.smallcase.android.view.model.Stock;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by shashankm on 01/05/17.
 */

@SuppressWarnings("SuspiciousMethodCalls")
class ReBalancePresenter implements ReBalanceContract.Presenter, StockHelper.StockPriceCallBack, KiteContract.Response {
    private static final String TAG = "ReBalancePresenter";

    private ReBalanceContract.View view;
    private ReBalanceContract.Service service;
    private StockHelper stockHelper;
    private MarketRepository marketRepository;
    private SharedPrefService sharedPrefService;
    private StockArrayList<Stock> requiredStocks;
    private StockArrayList<Stock> investedStocks;
    private List<Stock> finalStockList;
    private String scid, iScid, source;
    private Integer version;
    private Activity activity;
    private KiteHelper kiteHelper;
    private double investmentAmount;
    private String smallcaseTier;

    ReBalancePresenter(Activity activity, SmallcaseRepository smallcaseRepository, SharedPrefService sharedPrefService, ReBalanceContract
            .View view, MarketRepository marketRepository, StockHelper stockHelper, String scid, String iScid, int
            version, KiteHelper kiteHelper, String source, UserSmallcaseRepository userSmallcaseRepository) {
        this.activity = activity;
        this.view = view;
        this.sharedPrefService = sharedPrefService;
        this.marketRepository = marketRepository;
        this.stockHelper = stockHelper;
        this.scid = scid;
        this.iScid = iScid;
        this.version = version;
        this.kiteHelper = kiteHelper;
        this.source = source;
        this.service = new ReBalanceService(smallcaseRepository, sharedPrefService, this, marketRepository, userSmallcaseRepository);
        investedStocks = new StockArrayList<>();
        requiredStocks = new StockArrayList<>();
    }

    @Override
    public void fetchSmallcaseDetails() {
        service.getSmallcaseDetails(scid);
        service.getSmallcaseInvestmentDetails(iScid);
    }

    @Override
    public void onFailedFetchingSmallcaseInvestmentDetails() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onSmallcaseInvestmentDetailsFetched(SmallcaseDetail smallcaseDetail, ResponseBody responseBody) throws
            IOException, JSONException {
        if (!"VALID".equalsIgnoreCase(smallcaseDetail.getStatus())) {
            view.showBatchesRemainingToBeFixed();
            return;
        }

        JSONObject response = new JSONObject(responseBody.string());
        JSONObject data = response.getJSONObject("data");
        for (Constituents constituent : smallcaseDetail.getCurrentConfig().getConstituents()) {
            Stock stock = new Stock();
            stock.setSid(constituent.getSid());
            stock.setShares(constituent.getShares());
            stock.setStockName(constituent.getSidInfo().getName());
            stock.setPrice(data.getDouble(constituent.getSid()));
            investedStocks.add(stock);
        }

        stockHelper.calculateWeightageFromShares(investedStocks);

        calculateReBalanceStocksWithShares();
    }

    @Override
    public void onFailedFetchingSmallcaseDetails() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onSmallcaseDetailsFetched(UnInvestedSmallcase unInvestedSmallcase, ResponseBody responseBody) throws
            IOException, JSONException {
        JSONObject response = new JSONObject(responseBody.string());
        JSONObject data = response.getJSONObject("data");
        for (Constituents constituent : unInvestedSmallcase.getConstituents()) {
            Stock stock = new Stock();
            stock.setSid(constituent.getSid());
            stock.setPrescribedWeight(constituent.getWeight());
            stock.setWeight(constituent.getWeight());
            stock.setStockName(constituent.getSidInfo().getName());
            stock.setPrice(data.getDouble(constituent.getSid()));
            requiredStocks.add(stock);
        }

        int version = unInvestedSmallcase.getVersion();
        for (SmallcaseUpdate smallcaseUpdate : unInvestedSmallcase.getUpdates()) {
            if (version == smallcaseUpdate.getVersion()) {
                view.setRationale(smallcaseUpdate.getRationale());
            }
        }

        smallcaseTier = unInvestedSmallcase.getInfo().getTier();
        calculateReBalanceStocksWithShares();
    }

    private void createStocks(List<Object> reBalanceList) {
        finalStockList = new ArrayList<>();
        finalStockList.clear();
        List<String> sids = new ArrayList<>();
        for (Object object : reBalanceList) {
            if (object instanceof ReBalanceStock) {
                ReBalanceStock reBalanceStock = (ReBalanceStock) object;
                if (reBalanceStock.getNewShares() != reBalanceStock.getOldShares()) {
                    Stock stock = new Stock();
                    stock.setSid(reBalanceStock.getSid());
                    stock.setStockName(reBalanceStock.getStockName());
                    int diff = (int) (reBalanceStock.getNewShares() - reBalanceStock.getOldShares());
                    stock.setShares(Math.abs(diff));
                    stock.setBuy(diff > 0);
                    sids.add(stock.getSid());
                    finalStockList.add(stock);
                }
            }
        }
        stockHelper.getStockPrices(marketRepository, sharedPrefService, sids, this);
    }

    @Override
    public void checkMarketStatus() {
        if ("leprechaun".equals(sharedPrefService.getUserInfo().get("brokerName"))) {
            createStocks(view.getReBalanceStocks());
        } else {
            service.getIfMarketIsOpen();
        }
    }

    @Override
    public void onMarketStatusReceived(ResponseBody responseBody) {
        try {
            JSONObject response = new JSONObject(responseBody.string());
            if (response.getString("data").equals("closed")) {
                view.hideProgressDialog();
                view.showSnackBar(R.string.market_closed_now);
            } else {
                createStocks(view.getReBalanceStocks());
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
            SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; onMarketStatusReceived()");
            view.hideProgressDialog();
            view.showSnackBar(R.string.something_wrong);
        }
    }

    @Override
    public void onFailedFetchingMarketStatus() {
        view.hideProgressDialog();
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void ignoreReBalance() {
        HashMap<String, String> body = new HashMap<>();
        body.put("iscid", iScid);
        service.ignoreReBalance(body);
    }

    @Override
    public void onFailedCancellingReBalance() {
        view.hideProgressDialog();
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onFailedUpdatingVersion() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onUpdateCanceledSuccessfully() {
        view.hideProgressDialog();
        Intent intent = new Intent(AppConstants.RE_BALANCE_IGNORED_BROADCAST);
        intent.putExtra("smallcaseName", view.getSmallcaseName());
        LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
        activity.finish();
    }

    @Override
    public List<ReBalanceStock> getOnlyReBalanceStocks(List<Object> reBalanceList) {
        List<ReBalanceStock> reBalanceStockList = new ArrayList<>();
        for (Object object : reBalanceList) {
            if (object instanceof ReBalanceStock) {
                reBalanceStockList.add((ReBalanceStock) object);
            }
        }
        return reBalanceStockList;
    }

    @Override
    public String getScid() {
        return scid;
    }

    @Override
    public String getIScid() {
        return iScid;
    }

    @Override
    public void destroySubscriptions() {
        service.destroySubscriptions();
    }

    @Override
    public void noScidsPresent() {
        view.showBatchesRemainingToBeFixed();
    }

    @Override
    public void onFailedFetchingStockPrices() {
        view.showSnackBar(R.string.something_wrong);
    }

    private void calculateReBalanceStocksWithShares() {
        if (investedStocks.isEmpty() || requiredStocks.isEmpty()) return;

        List<ReBalanceStock> reBalanceStockList = new ArrayList<>();
        // Calculate the minimum investment amount as per new configuration after re-balance
        StockListWithMinAmount stockListWithMinAmount = stockHelper.calculateSharesAndMinAmountFromWeight(requiredStocks, -1);

        // Compare the minimum investment amount calculated in last step with the users current netWorth in the same smallcase
        double maxAmount = 0;
        for (Stock investedStock : investedStocks) {
            maxAmount += (investedStock.getShares() * investedStock.getPrice());
        }
        maxAmount = Math.max(maxAmount, stockListWithMinAmount.getMaxPByW());

        // Calculate number of shares for the new configuration with Round(weight of the stock * maxAmount / price of the stock)
        for (Stock requiredStock : requiredStocks) {
            requiredStock.setShares(AppUtils.getInstance().round(((requiredStock.getWeight() * maxAmount) / requiredStock.getPrice()), 0));
        }
        stockHelper.calculateWeightageFromShares(requiredStocks);

        /*
        For dropped, new number of shares is 0. For newly added, new number of shares is what we calculated in the last step.
        For retained stock we find value difference. Value difference = Abs (no of stocks calculated in last step
        - no of stocks currently held) * current price.

        If the Value difference is less than or equal to 500, new configuration shares = shares currently held (new bracket condition)
        If the Value difference is more than 500, new configuration shares = no shares calculated in step 3
        */
        for (Stock requiredStock : requiredStocks) {
            int investedIndex = investedStocks.indexOf(requiredStock.getSid());
            if (investedIndex == -1) {
                ReBalanceStock reBalanceStock = new ReBalanceStock();
                reBalanceStock.setSid(requiredStock.getSid());
                reBalanceStock.setStockName(requiredStock.getStockName());
                reBalanceStock.setNewShares(requiredStock.getShares());
                reBalanceStock.setNewWeight(requiredStock.getWeight() * 100);
                reBalanceStock.setOldShares(0);
                reBalanceStock.setOldWeight(0);
                reBalanceStockList.add(reBalanceStock);
            } else {
                Stock presentStock = investedStocks.get(investedIndex);
                ReBalanceStock reBalanceStock = new ReBalanceStock();
                reBalanceStock.setSid(presentStock.getSid());
                reBalanceStock.setStockName(presentStock.getStockName());
                double valueDifference = Math.abs((requiredStock.getShares() - presentStock.getShares()) * requiredStock.getPrice());
                if (requiredStock.getShares() < presentStock.getShares() && valueDifference <= 500) {
                    reBalanceStock.setNewShares(presentStock.getShares());
                    reBalanceStock.setNewWeight(presentStock.getWeight() * 100);
                } else {
                    reBalanceStock.setNewShares(requiredStock.getShares());
                    reBalanceStock.setNewWeight(requiredStock.getWeight() * 100);
                }
                reBalanceStock.setOldShares(presentStock.getShares());
                reBalanceStock.setOldWeight(presentStock.getWeight() * 100);
                reBalanceStockList.add(reBalanceStock);
            }
        }

        for (Stock investedStock : investedStocks) {
            int index = requiredStocks.indexOf(investedStock.getSid());
            if (index == -1) {
                ReBalanceStock reBalanceStock = new ReBalanceStock();
                reBalanceStock.setSid(investedStock.getSid());
                reBalanceStock.setStockName(investedStock.getStockName());
                reBalanceStock.setNewShares(0);
                reBalanceStock.setNewWeight(0);
                reBalanceStock.setOldShares(investedStock.getShares());
                reBalanceStock.setOldWeight(investedStock.getWeight() * 100);
                reBalanceStockList.add(reBalanceStock);
            }
        }

        convertToPresentableLists(reBalanceStockList);
    }

    private void convertToPresentableLists(List<ReBalanceStock> reBalanceStockList) {
        List<Object> reBalanceList = new ArrayList<>();
        List<ReBalanceStock> stocksBeingBought = new ArrayList<>();
        List<ReBalanceStock> stocksBeingSold = new ArrayList<>();
        List<ReBalanceStock> stocksUnchanged = new ArrayList<>();
        for (ReBalanceStock reBalanceStock : reBalanceStockList) {
            if (reBalanceStock.getOldShares() < reBalanceStock.getNewShares()) {
                stocksBeingBought.add(reBalanceStock);
            } else if (reBalanceStock.getOldShares() > reBalanceStock.getNewShares()) {
                stocksBeingSold.add(reBalanceStock);
            } else {
                stocksUnchanged.add(reBalanceStock);
            }
        }

        // If no updates are available
        if (stocksBeingBought.isEmpty() && stocksBeingSold.isEmpty()) {
            HashMap<String, Object> body = new HashMap<>();
            body.put("iscid", iScid);
            body.put("version", version);
            service.noOrders(body);
            view.showNoUpdatesAvailable();
            return;
        }

        if (!stocksBeingBought.isEmpty()) reBalanceList.add("Stocks you will buy");
        reBalanceList.addAll(stocksBeingBought);

        if (!stocksBeingSold.isEmpty()) reBalanceList.add("Stocks you will sell");
        reBalanceList.addAll(stocksBeingSold);

        if (!stocksUnchanged.isEmpty()) reBalanceList.add("Stocks unchanged");
        reBalanceList.addAll(stocksUnchanged);

        view.onShowReBalancedList(reBalanceList);
    }

    @Override
    public void onStockPricesReceived(HashMap<String, Double> stockMap) {
        view.hideProgressDialog();
        investmentAmount = 0;
        for (Stock stock : finalStockList) {
            stock.setPrice(stockMap.get(stock.getSid()));
            if (stock.isBuy()) {
                investmentAmount += (stock.getPrice() * stock.getShares());
            } else {
                investmentAmount -= (stock.getPrice() * stock.getShares());
            }
        }

        kiteHelper.checkSufficientFunds(investmentAmount, this);
    }

    @Override
    public void onFailedFetchingStockPrice(Throwable throwable, @Nullable String sid) {
        if (throwable != null && throwable instanceof HttpException && ((HttpException) throwable).code() == 401) {
            view.sendUserToKiteLogin();
            return;
        }

        view.hideProgressDialog();
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onFailedFetchingFunds(@Nullable Throwable throwable) {
        if (throwable != null && throwable instanceof HttpException && ((HttpException) throwable).code() == 401) {
            view.sendUserToKiteLogin();
            return;
        }

        view.hideProgressDialog();
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void sufficientFundsAvailable() {
        Intent intent = new Intent(activity, ReviewOrderActivity.class);
        intent.putExtra(ReviewOrderActivity.ISCID, iScid);
        intent.putExtra(ReviewOrderActivity.SCID, scid);
        intent.putExtra(ReviewOrderActivity.SMALLCASE_NAME, view.getSmallcaseName());
        intent.putExtra(ReviewOrderActivity.ORDER_LABEL, OrderLabel.RE_BALANCE);
        intent.putExtra(ReviewOrderActivity.INVESTMENT_AMOUNT, investmentAmount);
        intent.putExtra(ReviewOrderActivity.SOURCE, source);
        intent.putExtra(ReviewOrderActivity.RE_BALANCE_VERSION, version);
        intent.putParcelableArrayListExtra(ReviewOrderActivity.STOCK_LIST, (ArrayList<? extends Parcelable>) finalStockList);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
        activity.finish();
    }

    @Override
    public void needMoreFunds(double availableFunds) {
        view.hideProgressDialog();
        String requiredFunds = String.format(Locale.getDefault(), "%.2f", investmentAmount - availableFunds);
        SpannableString spannableString = new SpannableString("You need atleast " + requiredFunds + " to continue");
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primary_text)),
                16, 17 + requiredFunds.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new RelativeSizeSpan(1.4f), 16, 16 + requiredFunds.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        view.showNeedMoreFunds(spannableString);
    }

    @Override
    public String getSource() {
        return source;
    }

    @Override
    public String getSmallcaseTier() {
        return smallcaseTier;
    }
}
