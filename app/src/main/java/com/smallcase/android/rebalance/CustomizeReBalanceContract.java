package com.smallcase.android.rebalance;

import android.support.annotation.StringRes;
import android.text.SpannableString;

import com.smallcase.android.view.model.ReBalanceStock;
import com.smallcase.android.view.model.Stock;

import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by shashankm on 05/05/17.
 */

interface CustomizeReBalanceContract {
    interface View {

        void showSnackBar(@StringRes int resId);

        List<ReBalanceStock> getReBalanceStocks();

        void hideProgressDialog();

        void sendUserToKiteLogin();

        void showNeedMoreFunds(SpannableString spannableString);
    }

    interface Presenter {

        List<Stock> convertToStockModelList(List<ReBalanceStock> reBalanceStocks);

        void checkMarketStatus();

        void failedFetchingMarketStatus();

        void onMarketStatusReceived(ResponseBody responseBody);

        void destroySubscriptions();

        String getSmallcaseName();

        String getSource();
    }

    interface Service {

        void getMarketStatus();

        void destroySubscriptions();
    }
}
