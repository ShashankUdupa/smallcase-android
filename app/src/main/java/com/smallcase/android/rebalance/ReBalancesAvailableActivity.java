package com.smallcase.android.rebalance;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.model.ReBalanceUpdate;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.onboarding.LandingActivity;
import com.smallcase.android.smallcase.smallcasedetail.SmallcaseDetailActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.view.android.GraphikText;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;

public class ReBalancesAvailableActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener,
        ReBalancesAvailableService.ReBalanceResponse {
    public static final String RE_BALANCES = "re_balances";

    @BindView(R.id.text_tool_bar_title)
    GraphikText toolBarTitle;
    @BindView(R.id.re_balances_list)
    RecyclerView reBalancesList;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.empty_state_container)
    View emptyStateContainer;
    @BindView(R.id.text_empty_state)
    GraphikText noReBalanceText;
    @BindView(R.id.parent)
    View parent;
    @BindView(R.id.loading_indicator)
    View loadingIndicator;
    @BindView(R.id.empty_state_icon)
    ImageView noReBalancesIcon;

    private List<ReBalanceUpdate> reBalanceUpdates;
    private ReBalancesAvailableService reBalancesAvailableService;
    private ReBalanceAdapter reBalanceAdapter;
    private AnalyticsContract analyticsContract;
    private String reBalanceIgnored;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_re_balances_available);

        ButterKnife.bind(this);

        analyticsContract = new AnalyticsManager(getApplicationContext());

        toolBarTitle.setText("Rebalance Updates");
        reBalancesList.setLayoutManager(new LinearLayoutManager(this));
        reBalancesList.setHasFixedSize(true);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.blue_800));


        loadingIndicator.setVisibility(View.VISIBLE);
        reBalancesAvailableService = new ReBalancesAvailableService(new UserRepository(), new SharedPrefService(this));
        reBalancesAvailableService.getReBalanceUpdates(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.RE_BALANCE_IGNORED_BROADCAST));
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (AppConstants.RE_BALANCE_IGNORED_BROADCAST.equals(intent.getAction()) && !isFinishing()) {
                reBalanceIgnored = intent.getStringExtra("smallcaseName");
            }
        }
    };


    @OnClick(R.id.back)
    void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if (reBalancesAvailableService != null) {
            reBalancesAvailableService.destroySubscriptions();
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        if (reBalancesAvailableService == null) {
            reBalancesAvailableService = new ReBalancesAvailableService(new UserRepository(), new SharedPrefService(this));
        }
        reBalancesAvailableService.getReBalanceUpdates(this);
    }

    @Override
    public void onReBalancesReceived(List<ReBalanceUpdate> reBalanceUpdates) {
        swipeRefreshLayout.setRefreshing(false);
        if (reBalanceUpdates.isEmpty()) {
            emptyStateContainer.setVisibility(View.VISIBLE);
            Glide.with(this)
                    .load(R.drawable.no_rebalance)
                    .into(noReBalancesIcon);
            noReBalanceText.setText("No rebalances pending");
            swipeRefreshLayout.setVisibility(View.GONE);
            loadingIndicator.setVisibility(View.INVISIBLE);
            return;
        }

        if (loadingIndicator.getVisibility() == View.VISIBLE) {
            loadingIndicator.setVisibility(View.GONE);
        }
        this.reBalanceUpdates = reBalanceUpdates;

        if (reBalanceAdapter == null) {
            reBalanceAdapter = new ReBalanceAdapter();
            reBalancesList.setAdapter(reBalanceAdapter);
            return;
        }
        reBalanceAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (!isFinishing() && reBalanceAdapter != null && reBalanceUpdates != null) {
            loadingIndicator.setVisibility(View.VISIBLE);
            reBalanceUpdates.clear();
            reBalanceAdapter.notifyDataSetChanged();
            if (reBalancesAvailableService == null) {
                reBalancesAvailableService = new ReBalancesAvailableService(new UserRepository(), new SharedPrefService(this));
            }
            reBalancesAvailableService.getReBalanceUpdates(this);

            if (reBalanceIgnored != null) {
                Snackbar.make(parent, "Rebalance for " + reBalanceIgnored + " skipped", Snackbar.LENGTH_LONG).show();
                reBalanceIgnored = null;
            }
        }
    }

    @Override
    public void onFailedFetchingReBalanceUpdates(Throwable throwable) {
        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) {
            startActivity(new Intent(this, LandingActivity.class));
            finish();
        }

        swipeRefreshLayout.setRefreshing(false);
        AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_LONG, R.string.something_wrong);
    }

    class ReBalanceAdapter extends RecyclerView.Adapter<ReBalanceAdapter.ViewHolder> {
        private final String TAG = ReBalanceAdapter.class.getSimpleName();

        private SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault());

        @Override
        public ReBalanceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.re_balance_card, parent, false));
        }

        @Override
        public void onBindViewHolder(final ReBalanceAdapter.ViewHolder holder, int position) {
            final ReBalanceUpdate reBalanceUpdate = reBalanceUpdates.get(position);
            Glide.with(ReBalancesAvailableActivity.this)
                    .load(AppConstants.SMALLCASE_IMAGE_URL + "80/" + reBalanceUpdate.getScid() + ".png")
                    .into(holder.smallcaseImage);
            holder.smallcaseName.setText(reBalanceUpdate.getName());
            holder.reBalanceDate.setText("Came on: " + sdf.format(AppUtils.getInstance().getPlainDate(reBalanceUpdate.getDate())));

            holder.smallcaseCard.setOnClickListener(v -> {
                Intent intent = new Intent(ReBalancesAvailableActivity.this, SmallcaseDetailActivity.class);
                intent.putExtra(SmallcaseDetailActivity.SCID, reBalanceUpdate.getScid());
                intent.putExtra(SmallcaseDetailActivity.TITLE, reBalanceUpdate.getName());
                intent.putExtra(SmallcaseDetailActivity.ACCESSED_FROM, "Available rebalances");
                startActivity(intent);
            });

            holder.applyUpdate.setOnClickListener(v -> {
                Map<String, Object> eventProperties = new HashMap<>();
                eventProperties.put("accessedFrom", "Rebalances List");
                eventProperties.put("updateDate", reBalanceUpdate.getDate());
                eventProperties.put("updateName", reBalanceUpdate.getLabel());
                eventProperties.put("smallcaseName", reBalanceUpdate.getName());
                analyticsContract.sendEvent(eventProperties, "Viewed Rebalance Update", Analytics.MIXPANEL, Analytics.CLEVERTAP);

                Intent intent = new Intent(ReBalancesAvailableActivity.this, ReBalanceActivity.class);
                intent.putExtra(ReBalanceActivity.SCID, reBalanceUpdate.getScid());
                intent.putExtra(ReBalanceActivity.ISCID, reBalanceUpdate.getIscid());
                intent.putExtra(ReBalanceActivity.TITLE, reBalanceUpdate.getName());
                intent.putExtra(ReBalanceActivity.DATE, reBalanceUpdate.getDate());
                intent.putExtra(ReBalanceActivity.LABEL, reBalanceUpdate.getLabel());
                intent.putExtra(ReBalanceActivity.VERSION, reBalanceUpdate.getVersion());
                intent.putExtra(ReBalanceActivity.SOURCE, SmallcaseSource.PROFESSIONAL);
                startActivity(intent);
            });
        }

        @Override
        public int getItemCount() {
            return reBalanceUpdates.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.smallcase_image)
            ImageView smallcaseImage;
            @BindView(R.id.smallcase_name)
            GraphikText smallcaseName;
            @BindView(R.id.re_balance_date)
            GraphikText reBalanceDate;
            @BindView(R.id.apply_update)
            View applyUpdate;
            @BindView(R.id.smallcase_card)
            View smallcaseCard;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
