package com.smallcase.android.rebalance;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;

import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.kite.KiteContract;
import com.smallcase.android.kite.KiteHelper;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.user.order.ReviewOrderActivity;
import com.smallcase.android.util.OrderLabel;
import com.smallcase.android.view.model.ReBalanceStock;
import com.smallcase.android.view.model.Stock;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.intercom.retrofit2.HttpException;
import okhttp3.ResponseBody;

/**
 * Created by shashankm on 05/05/17.
 */

class CustomizeReBalancePresenter implements CustomizeReBalanceContract.Presenter, StockHelper.StockPriceCallBack,
        KiteContract.Response {
    private static final String TAG = "CustomizeReBalancePresenter";
    private CustomizeReBalanceContract.Service service;
    private CustomizeReBalanceContract.View view;
    private List<Stock> stocks;
    private StockHelper stockHelper;
    private MarketRepository marketRepository;
    private Activity activity;
    private SharedPrefService sharedPrefService;
    private String scid, iScid, name, source;
    private KiteHelper kiteHelper;
    private double investmentAmount;
    private AnalyticsContract analyticsContract;

    CustomizeReBalancePresenter(Activity activity, MarketRepository marketRepository, SharedPrefService sharedPrefService,
                                CustomizeReBalanceContract.View view, StockHelper stockHelper, String scid,
                                String iScid, String name, KiteHelper kiteHelper, String source, AnalyticsContract analyticsContract) {
        this.activity = activity;
        this.marketRepository = marketRepository;
        this.sharedPrefService = sharedPrefService;
        this.view = view;
        this.stockHelper = stockHelper;
        this.scid = scid;
        this.iScid = iScid;
        this.name = name;
        this.source = source;
        this.kiteHelper = kiteHelper;
        this.analyticsContract = analyticsContract;
        service = new CustomizeReBalanceService(marketRepository, sharedPrefService, this);
    }

    @Override
    public List<Stock> convertToStockModelList(List<ReBalanceStock> reBalanceStocks) {
        List<Stock> stocks = new ArrayList<>();
        for (ReBalanceStock reBalanceStock : reBalanceStocks) {
            Stock stock = new Stock();
            stock.setSid(reBalanceStock.getSid());
            stock.setShares(reBalanceStock.getNewShares());
            stock.setStockName(reBalanceStock.getStockName());
            stocks.add(stock);
        }
        return stocks;
    }

    @Override
    public void checkMarketStatus() {
        if ("leprechaun".equals(sharedPrefService.getUserInfo().get("brokerName"))) {
            createStocks(view.getReBalanceStocks());
        } else {
            service.getMarketStatus();
        }
    }

    @Override
    public void failedFetchingMarketStatus() {
        view.hideProgressDialog();
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onMarketStatusReceived(ResponseBody responseBody) {
        try {
            JSONObject response = new JSONObject(responseBody.string());
            if (response.getString("data").equals("closed")) {
                view.hideProgressDialog();
                view.showSnackBar(R.string.market_closed_now);
            } else {
                createStocks(view.getReBalanceStocks());
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
            SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; onMarketStatusReceived()");
            view.hideProgressDialog();
            view.showSnackBar(R.string.something_wrong);
        }
    }

    @Override
    public void destroySubscriptions() {
        service.destroySubscriptions();
    }

    @Override
    public String getSmallcaseName() {
        return name;
    }

    @Override
    public String getSource() {
        return source;
    }

    private void createStocks(List<ReBalanceStock> reBalanceStocks) {
        stocks = new ArrayList<>();
        List<String> sids = new ArrayList<>();
        for (ReBalanceStock reBalanceStock : reBalanceStocks) {
            int sharesDifference = (int) (reBalanceStock.getNewShares() - reBalanceStock.getOldShares());
            if (sharesDifference == 0) continue;
            Stock stock = new Stock();
            stock.setShares(Math.abs(sharesDifference));
            stock.setBuy(sharesDifference > 0);
            stock.setSid(reBalanceStock.getSid());
            stock.setStockName(reBalanceStock.getStockName());
            sids.add(stock.getSid());
            stocks.add(stock);
        }
        stockHelper.getStockPrices(marketRepository, sharedPrefService, sids, this);
    }

    @Override
    public void onStockPricesReceived(HashMap<String, Double> stockMap) {
        view.hideProgressDialog();
        investmentAmount = 0;
        for (Stock stock : stocks) {
            stock.setPrice(stockMap.get(stock.getSid()));
            if (stock.isBuy()) {
                investmentAmount += (stock.getPrice() * stock.getShares());
            } else {
                investmentAmount -= (stock.getPrice() * stock.getShares());
            }
        }

        kiteHelper.checkSufficientFunds(investmentAmount, this);
    }

    @Override
    public void onFailedFetchingStockPrice(Throwable throwable, @Nullable String sid) {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onFailedFetchingFunds(@Nullable Throwable throwable) {
        if (throwable != null && throwable instanceof HttpException && ((HttpException) throwable).code() == 401) {
            view.sendUserToKiteLogin();
            return;
        }
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void sufficientFundsAvailable() {
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseType", source);
        eventProperties.put("smallcaseName", name);
        analyticsContract.sendEvent(eventProperties, "Confirmed Rebalance Changes", Analytics.MIXPANEL);

        Intent intent = new Intent(activity, ReviewOrderActivity.class);
        intent.putExtra(ReviewOrderActivity.ISCID, iScid);
        intent.putExtra(ReviewOrderActivity.SCID, scid);
        intent.putExtra(ReviewOrderActivity.SMALLCASE_NAME, name);
        intent.putExtra(ReviewOrderActivity.ORDER_LABEL, OrderLabel.RE_BALANCE);
        intent.putExtra(ReviewOrderActivity.INVESTMENT_AMOUNT, investmentAmount);
        intent.putExtra(ReviewOrderActivity.SOURCE, source);
        intent.putParcelableArrayListExtra(ReviewOrderActivity.STOCK_LIST, (ArrayList<? extends Parcelable>) stocks);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    }

    @Override
    public void needMoreFunds(double availableFunds) {
        String requiredFunds = String.format(Locale.getDefault(), "%.2f", investmentAmount - availableFunds);
        SpannableString spannableString = new SpannableString("You need atleast " + requiredFunds + "to continue");
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primary_text)),
                16, 16 + requiredFunds.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new RelativeSizeSpan(1.4f), 16, 16 + requiredFunds.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        view.showNeedMoreFunds(spannableString);
    }
}
