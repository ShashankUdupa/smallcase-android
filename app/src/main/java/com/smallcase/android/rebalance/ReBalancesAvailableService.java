package com.smallcase.android.rebalance;

import com.google.gson.Gson;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Actions;
import com.smallcase.android.data.model.ReBalanceUpdate;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;

import java.util.HashSet;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 10/05/17.
 */

class ReBalancesAvailableService {
    private static final String TAG = "ReBalancesAvailableService";
    private UserRepository userRepository;
    private SharedPrefService sharedPrefService;
    private CompositeSubscription compositeSubscription;

    interface ReBalanceResponse {
        void onReBalancesReceived(List<ReBalanceUpdate> reBalanceUpdates);

        void onFailedFetchingReBalanceUpdates(Throwable throwable);
    }

    ReBalancesAvailableService(UserRepository userRepository, SharedPrefService sharedPrefService) {
        this.userRepository = userRepository;
        this.sharedPrefService = sharedPrefService;
        compositeSubscription = new CompositeSubscription();
    }

    void getReBalanceUpdates(final ReBalanceResponse reBalanceResponse) {
        compositeSubscription.add(userRepository.getPendingActions(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(new Action1<Actions>() {
                    @Override
                    public void call(Actions actions) {
                        cacheReBalanceUpdates(actions.getReBalanceUpdates());
                        reBalanceResponse.onReBalancesReceived(actions.getReBalanceUpdates());
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        reBalanceResponse.onFailedFetchingReBalanceUpdates(throwable);
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getReBalanceUpdates()");
                    }
                }));
    }

    private void cacheReBalanceUpdates(List<ReBalanceUpdate> reBalanceUpdates) {
        HashSet<String> reBalanceStrings = new HashSet<>();
        Gson gson = new Gson();
        for (ReBalanceUpdate reBalanceUpdate : reBalanceUpdates) {
            reBalanceStrings.add(gson.toJson(reBalanceUpdate));
        }
        sharedPrefService.cacheReBalanceUpdates(reBalanceStrings);
    }

    void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }
}
