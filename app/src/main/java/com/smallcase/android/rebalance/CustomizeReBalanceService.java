package com.smallcase.android.rebalance;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;

import okhttp3.ResponseBody;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 05/05/17.
 */

class CustomizeReBalanceService implements CustomizeReBalanceContract.Service {
    private static final String TAG = "CustomizeReBalanceService";
    private MarketRepository marketRepository;
    private SharedPrefService sharedPrefService;
    private CustomizeReBalanceContract.Presenter presenter;
    private CompositeSubscription compositeSubscription;

    CustomizeReBalanceService(MarketRepository marketRepository, SharedPrefService sharedPrefService, CustomizeReBalanceContract
            .Presenter presenter) {
        this.marketRepository = marketRepository;
        this.sharedPrefService = sharedPrefService;
        this.presenter = presenter;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getMarketStatus() {
        compositeSubscription.add(marketRepository.getIfMarketIsOpen(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        presenter.onMarketStatusReceived(responseBody);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getMarketStatus()");
                        presenter.failedFetchingMarketStatus();
                    }
                }));
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }
}
