package com.smallcase.android.rebalance;

import android.support.annotation.StringRes;
import android.text.SpannableString;

import com.smallcase.android.data.model.SmallcaseDetail;
import com.smallcase.android.data.model.UnInvestedSmallcase;
import com.smallcase.android.view.model.ReBalanceStock;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by shashankm on 01/05/17.
 */

interface ReBalanceContract {
    interface View {

        void showSnackBar(@StringRes int resId);

        void onShowReBalancedList(List<Object> reBalanceList);

        List<Object> getReBalanceStocks();

        String getSmallcaseName();

        void hideProgressDialog();

        void showNoUpdatesAvailable();

        void sendUserToKiteLogin();

        void showNeedMoreFunds(SpannableString spannableString);

        void setRationale(String rationale);

        void showBatchesRemainingToBeFixed();
    }

    interface Presenter {

        void fetchSmallcaseDetails();

        void onFailedFetchingSmallcaseInvestmentDetails();

        void onSmallcaseInvestmentDetailsFetched(SmallcaseDetail smallcaseDetail, ResponseBody responseBody) throws IOException, JSONException;

        void onFailedFetchingSmallcaseDetails();

        void onSmallcaseDetailsFetched(UnInvestedSmallcase unInvestedSmallcase, ResponseBody responseBody) throws IOException, JSONException;

        void checkMarketStatus();

        void onMarketStatusReceived(ResponseBody responseBody);

        void onFailedFetchingMarketStatus();

        void ignoreReBalance();

        void onFailedCancellingReBalance();

        void onFailedUpdatingVersion();

        void onUpdateCanceledSuccessfully();

        List<ReBalanceStock> getOnlyReBalanceStocks(List<Object> reBalanceList);

        String getScid();

        String getIScid();

        String getSource();

        String getSmallcaseTier();

        void destroySubscriptions();

        void noScidsPresent();

        void onFailedFetchingStockPrices();
    }

    interface Service {

        void getSmallcaseInvestmentDetails(final String iScid);

        void getSmallcaseDetails(final String scid);

        void getIfMarketIsOpen();

        void ignoreReBalance(HashMap<String, String> body);

        void noOrders(HashMap<String, Object> body);

        void destroySubscriptions();
    }
}
