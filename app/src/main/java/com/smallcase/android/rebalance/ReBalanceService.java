package com.smallcase.android.rebalance;

import android.util.Pair;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Constituents;
import com.smallcase.android.data.model.SmallcaseDetail;
import com.smallcase.android.data.model.UnInvestedSmallcase;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.intercom.retrofit2.HttpException;
import io.sentry.event.Breadcrumb;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 01/05/17.
 */

class ReBalanceService implements ReBalanceContract.Service {
    private static final String TAG = "ReBalanceService";
    private SmallcaseRepository smallcaseRepository;
    private MarketRepository marketRepository;
    private UserSmallcaseRepository userSmallcaseRepository;
    private SharedPrefService sharedPrefService;
    private ReBalanceContract.Presenter presenter;
    private CompositeSubscription compositeSubscription;

    ReBalanceService(SmallcaseRepository smallcaseRepository, SharedPrefService sharedPrefService, ReBalanceContract
            .Presenter presenter, MarketRepository marketRepository, UserSmallcaseRepository userSmallcaseRepository) {
        this.smallcaseRepository = smallcaseRepository;
        this.sharedPrefService = sharedPrefService;
        this.presenter = presenter;
        this.marketRepository = marketRepository;
        this.userSmallcaseRepository = userSmallcaseRepository;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getSmallcaseInvestmentDetails(final String iScid) {
        final HashMap<String, String> data = new HashMap<>();
        data.put("iScid", iScid);
        compositeSubscription.add(userSmallcaseRepository.getInvestedSmallcaseDetails(sharedPrefService.getAuthorizationToken(), iScid,
                sharedPrefService.getCsrfToken())
                .subscribe(new Action1<SmallcaseDetail>() {
                    @Override
                    public void call(SmallcaseDetail smallcaseDetail) {
                        getStockPrices(smallcaseDetail, data);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedFetchingSmallcaseInvestmentDetails();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401)
                            return;

                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed getting investment", data);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getSmallcaseInvestmentDetails()");
                    }
                }));
    }

    @Override
    public void getSmallcaseDetails(final String scid) {
        final HashMap<String, String> data = new HashMap<>();
        data.put("scid", scid);
        compositeSubscription.add(smallcaseRepository.getSmallcaseDetails(sharedPrefService.getAuthorizationToken(), scid,
                sharedPrefService.getCsrfToken())
                .flatMap(new Func1<UnInvestedSmallcase, Observable<ResponseBody>>() {
                    @Override
                    public Observable<ResponseBody> call(UnInvestedSmallcase unInvestedSmallcase) {
                        List<Constituents> constituentsList = new ArrayList<>(unInvestedSmallcase.getConstituents());
                        List<String> sids = new ArrayList<>();
                        for (Constituents constituents : constituentsList) {
                            sids.add(constituents.getSid());
                        }
                        return marketRepository.getStocksPrice(sharedPrefService.getAuthorizationToken(), sids,
                                sharedPrefService.getCsrfToken());
                    }
                }, new Func2<UnInvestedSmallcase, ResponseBody, Pair<UnInvestedSmallcase, ResponseBody>>() {
                    @Override
                    public Pair<UnInvestedSmallcase, ResponseBody> call(UnInvestedSmallcase unInvestedSmallcase, ResponseBody responseBody) {
                        return new Pair<>(unInvestedSmallcase, responseBody);
                    }
                })
                .subscribe(new Action1<Pair<UnInvestedSmallcase, ResponseBody>>() {
                    @Override
                    public void call(Pair<UnInvestedSmallcase, ResponseBody> unInvestedSmallcaseResponseBodyPair) {
                        try {
                            presenter.onSmallcaseDetailsFetched(unInvestedSmallcaseResponseBodyPair.first,
                                    unInvestedSmallcaseResponseBodyPair.second);
                        } catch (IOException | JSONException e) {
                            SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed getting smallcase Details", data);
                            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; getInvestedSmallcaseDetails()");
                            presenter.onFailedFetchingSmallcaseDetails();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedFetchingSmallcaseDetails();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401)
                            return;

                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed getting smallcase Details", data);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getInvestedSmallcaseDetails()");
                    }
                }));
    }

    @Override
    public void getIfMarketIsOpen() {
        compositeSubscription.add(marketRepository.getIfMarketIsOpen(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        presenter.onMarketStatusReceived(responseBody);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedFetchingMarketStatus();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401)
                            return;

                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getIfMarketIsOpen()");
                    }
                }));
    }

    @Override
    public void ignoreReBalance(final HashMap<String, String> body) {
        final HashMap<String, String> data = new HashMap<>();
        data.put("iScid", body.get("iscid"));
        compositeSubscription.add(userSmallcaseRepository.cancelUpdates(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken(), body)
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        try {
                            JSONObject jsonObject = new JSONObject(responseBody.string());
                            if (jsonObject.getBoolean("success")) {
                                presenter.onUpdateCanceledSuccessfully();
                            } else {
                                SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed ignoring Rebalance", data);
                                SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), new Exception("Failure in ignoring rebalance"), TAG + "; ignoreReBalance()");

                                presenter.onFailedCancellingReBalance();
                            }
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();

                            SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed ignoring Rebalance", data);
                            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; ignoreReBalance()");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedCancellingReBalance();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401)
                            return;

                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed ignoring Rebalance", data);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; ignoreReBalance()");
                    }
                }));
    }

    @Override
    public void noOrders(final HashMap<String, Object> body) {
        final HashMap<String, String> data = new HashMap<>();
        data.put("iScid", (String) body.get("iscid"));
        compositeSubscription.add(userSmallcaseRepository.noOrders(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken(), body)
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedUpdatingVersion();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401)
                            return;

                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed noOrders()", data);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; noOrders()");
                    }
                }));
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }

    private void getStockPrices(final SmallcaseDetail smallcaseDetail, final HashMap<String, String> data) {
        List<Constituents> constituentsList = new ArrayList<>(smallcaseDetail.getCurrentConfig().getConstituents());

        if (constituentsList.isEmpty()) {
            presenter.noScidsPresent();
            return;
        }

        List<String> sids = new ArrayList<>();
        for (Constituents constituents : constituentsList) {
            sids.add(constituents.getSid());
        }

        marketRepository.getStocksPrice(sharedPrefService.getAuthorizationToken(), sids,
                sharedPrefService.getCsrfToken())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        try {
                            presenter.onSmallcaseInvestmentDetailsFetched(smallcaseDetail, responseBody);
                        } catch (IOException | JSONException e) {
                            SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed getting investment", data);
                            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; getSmallcaseInvestmentDetails()");

                            presenter.onFailedFetchingSmallcaseInvestmentDetails();
                            e.printStackTrace();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedFetchingStockPrices();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401)
                            return;

                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed getting investment", data);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getSmallcaseInvestmentDetails()");
                    }
                });
    }
}
