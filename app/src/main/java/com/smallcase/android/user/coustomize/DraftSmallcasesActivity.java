package com.smallcase.android.user.coustomize;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.create.CreateSmallcaseActivity;
import com.smallcase.android.data.model.Constituents;
import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.onboarding.LoginActivity;
import com.smallcase.android.smallcase.smallcasedetail.SmallcaseDetailActivity;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.user.order.ReviewOrderActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.OrderLabel;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.util.SmallcaseTier;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.buy.InvestContract;
import com.smallcase.android.view.buy.InvestAmountPopUp;
import com.smallcase.android.view.model.Stock;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.smallcase.android.onboarding.LoginActivity.REQUEST_TOKEN_KEY;


public class DraftSmallcasesActivity extends AppCompatActivity implements DraftContract.View, InvestContract
        .Response, SwipeRefreshLayout.OnRefreshListener {
    private static final int LOGIN_REQUEST_CODE = 1;
    private static final String TAG = "DraftSmallcasesActivity";

    @BindView(R.id.text_tool_bar_title)
    GraphikText toolBarTitle;
    @BindView(R.id.drafts_list)
    RecyclerView draftsList;
    @BindView(R.id.smallcase_saved_title)
    View smallcaseSavedTitle;
    @BindView(R.id.loading_indicator)
    View loadingIndicator;
    @BindView(R.id.parent)
    View parent;
    @BindView(R.id.blur)
    View blur;
    @BindView(R.id.empty_state_container)
    View noDraftsContainer;
    @BindView(R.id.empty_state_icon)
    ImageView emptyStateIcon;
    @BindView(R.id.text_empty_state)
    GraphikText noDraftsText;
    @BindView(R.id.action_icon)
    ImageView actionIcon;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private DraftContract.Presenter presenter;
    private InvestContract.View investContract;
    private AnalyticsContract analyticsContract;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draft_smallcases);

        ButterKnife.bind(this);

        toolBarTitle.setText("Drafts");
        draftsList.setLayoutManager(new LinearLayoutManager(this));
        draftsList.setHasFixedSize(true);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.blue_800));

        SharedPrefService sharedPrefService = new SharedPrefService(this);

        analyticsContract = new AnalyticsManager(this);
        investContract = new InvestAmountPopUp(this, sharedPrefService, new MarketRepository(), new StockHelper(), new
                KiteRepository(), this, false, analyticsContract);

        analyticsContract.sendEvent(null, "Viewed Drafts", Analytics.INTERCOM, Analytics.CLEVERTAP);
        presenter = new DraftPresenter(new UserSmallcaseRepository(), sharedPrefService, this, new NetworkHelper(this), new MarketRepository());
        presenter.getDraftSmallcases();

        Glide.with(this)
                .load(R.drawable.add_white)
                .into(actionIcon);
        actionIcon.setVisibility(View.VISIBLE);

        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.PLACED_ORDER_BROADCAST));
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.DRAFT_DELETED));
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (investContract.isInvestCardVisible()) {
            investContract.hideInvestCard();
            return;
        }

        super.onBackPressed();
    }

    @OnClick(R.id.action_icon)
    public void createSmallcase() {
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("accessedFrom", "Drafts");
        analyticsContract.sendEvent(eventProperties, "Viewed Create", Analytics.MIXPANEL, Analytics.CLEVERTAP);
        startActivity(new Intent(this, CreateSmallcaseActivity.class));
    }

    @Override
    protected void onDestroy() {
        investContract.unBind();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            //Received request_token from Kite login. Request authToken from server.
            HashMap<String, String> body = new HashMap<>();
            body.put("broker", "kite");
            body.put("reqToken", data.getStringExtra(REQUEST_TOKEN_KEY));
            body.put("app", "platform");

            InvestContract.Authenticate authenticate = (InvestContract.Authenticate) investContract;
            authenticate.getAuthToken(body);
        }
    }

    @Override
    public void showSnackBar(@StringRes int resId) {
        Snackbar snackbar = Snackbar.make(parent, resId, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void showNoDraftsAvailable() {
        swipeRefreshLayout.setRefreshing(false);
        Glide.with(this)
                .load(R.drawable.draft_empty)
                .into(emptyStateIcon);

        loadingIndicator.setVisibility(View.GONE);
        noDraftsContainer.setVisibility(View.VISIBLE);
        noDraftsText.setText(R.string.drafts_empty);
    }

    @OnClick(R.id.create_smallcase)
    public void createSmallcaseClicked() {
        createSmallcase();
    }

    @Override
    public void onDraftsReceived() {
        swipeRefreshLayout.setRefreshing(false);
        loadingIndicator.setVisibility(View.GONE);
        smallcaseSavedTitle.setVisibility(View.VISIBLE);
        draftsList.setAdapter(new DraftsAdapter());
    }

    @Override
    public void marketIsOpen() {
        if (presenter.isDraftShareBased()) {
            investContract.getAmountFromShares(presenter.getStocksList());
            return;
        }
        investContract.getMinAmount(presenter.getStocksList());
    }

    @Override
    public void showBlur() {
        blur.setVisibility(View.VISIBLE);
        blur.setClickable(true);
        blur.setOnClickListener(v -> {
            investContract.hideInvestCard();
        });
    }

    @Override
    public void hideBlur() {
        blur.setVisibility(View.GONE);
    }

    @Override
    public void sendUserToKiteLogin() {
        startActivityForResult(new Intent(this, LoginActivity.class),
                LOGIN_REQUEST_CODE);
        overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (AppConstants.PLACED_ORDER_BROADCAST.equals(intent.getAction())) {
                if (!isFinishing()) {
                    finish();
                }
            } else if (AppConstants.DRAFT_DELETED.equals(intent.getAction())) {
                onRefresh();
            }
        }
    };

    @Override
    public void canContinueTransaction(List<Stock> stockList) {
        investContract.hideInvestCard();
        DraftSmallcase smallcaseToBuy = presenter.getSelectedSmallcase();

        Intent intent = new Intent(this, ReviewOrderActivity.class);
        intent.putExtra(ReviewOrderActivity.SOURCE, smallcaseToBuy.getSource());
        intent.putExtra(ReviewOrderActivity.SMALLCASE_NAME, smallcaseToBuy.getInfo().getName());
        intent.putExtra(ReviewOrderActivity.INVESTMENT_AMOUNT, investContract.getInvestmentAmount());
        intent.putExtra(ReviewOrderActivity.SCID, smallcaseToBuy.getScid());
        intent.putExtra(ReviewOrderActivity.ORDER_LABEL, OrderLabel.BUY);
        intent.putParcelableArrayListExtra(ReviewOrderActivity.STOCK_LIST, (ArrayList<Stock>) stockList);
        intent.putExtra(ReviewOrderActivity.DID, smallcaseToBuy.get_id());
        startActivity(intent);
        overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    }

    @Override
    public void onConfirmAmountClicked(double investmentAmount, double minInvestmentAmount) {
        DraftSmallcase draftSmallcase = presenter.getSelectedSmallcase();
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", draftSmallcase.getInfo().getName());
        eventProperties.put("smallcaseType", draftSmallcase.getSource());
        eventProperties.put("amountConfirmed", investmentAmount);
        eventProperties.put("minInvestAmount", minInvestmentAmount);
        analyticsContract.sendEvent(eventProperties, "Confirmed Amount", Analytics.MIXPANEL, Analytics.CLEVERTAP);
    }

    @Override
    public void onAddFundsClicked(double investmentAmount, double minInvestmentAmount) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(AppConstants.KITE_ADD_FUNDS));
        startActivity(intent);

        DraftSmallcase draftSmallcase = presenter.getSelectedSmallcase();
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", draftSmallcase.getInfo().getName());
        eventProperties.put("smallcaseType", draftSmallcase.getSource());
        eventProperties.put("amountConfirmed", investmentAmount);
        eventProperties.put("minInvestAmount", minInvestmentAmount);
        analyticsContract.sendEvent(eventProperties, "Proceeded To Adding Funds", Analytics.MIXPANEL, Analytics.CLEVERTAP);
    }

    @Override
    public void onConfirmShownAgain() {
        DraftSmallcase draftSmallcase = presenter.getSelectedSmallcase();
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", draftSmallcase.getInfo().getName());
        eventProperties.put("smallcaseType", draftSmallcase.getSource());
        analyticsContract.sendEvent(eventProperties, "Changed Amount", Analytics.MIXPANEL, Analytics.CLEVERTAP);
    }

    @Override
    public void onRefresh() {
        presenter.getDraftSmallcases();
    }

    class DraftsAdapter extends RecyclerView.Adapter<DraftsAdapter.ViewHolder> {
        private SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault());

        @Override
        public DraftsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.smallcase_draft_card, parent, false));
        }

        @Override
        public void onBindViewHolder(DraftsAdapter.ViewHolder holder, int position) {
            final DraftSmallcase draftSmallcase = presenter.getDraftSmallcasesList().get(position);

            String imageUrl;
            if (SmallcaseSource.CREATED.equalsIgnoreCase(draftSmallcase.getSource())) {
                imageUrl = AppConstants.CREATED_SMALLCASE_IMAGE_URL + "80/" + draftSmallcase.getScid() + ".png";
                holder.customTag.setVisibility(View.GONE);
            } else {
                imageUrl = AppConstants.SMALLCASE_IMAGE_URL + "80/" + draftSmallcase.getScid() + ".png";
                holder.customTag.setVisibility(View.VISIBLE);
            }

            Glide.with(DraftSmallcasesActivity.this)
                    .load(imageUrl)
                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                    .placeholder(getResources().getDrawable(R.color.grey_300))
                    .into(holder.smallcaseImage);

            if (SmallcaseTier.PREMIUM.equals(draftSmallcase.getInfo().getTier())) {
                holder.premiumLabel.setVisibility(View.VISIBLE);
                Glide.with(DraftSmallcasesActivity.this)
                        .load(R.drawable.premium_label)
                        .into(holder.premiumLabel);
            } else {
                holder.premiumLabel.setVisibility(View.GONE);
            }

            holder.smallcaseName.setText(draftSmallcase.getInfo().getName());
            holder.smallcaseDescription.setText(draftSmallcase.getInfo().getShortDescription());
            holder.indexValue.setText(String.format(Locale.getDefault(), "%.2f", draftSmallcase.getStats().getIndexValue()));
            Glide.with(DraftSmallcasesActivity.this)
                    .load(draftSmallcase.getStats().getIndexValue() >= 100 ?
                            R.drawable.up_arrow : R.drawable.down_arrow_red)
                    .into(holder.upOrDown);
            holder.savedOnDate.setText(sdf.format(AppUtils.getInstance().getPlainDate(draftSmallcase.getInfo().getDateUpdated())));

            holder.smallcaseCard.setOnClickListener(v -> {
                ArrayList<String> sids = new ArrayList<>();
                for (Constituents constituent : draftSmallcase.getConstituents()) {
                    sids.add(constituent.getSid());
                }

                Intent intent = new Intent(DraftSmallcasesActivity.this, SmallcaseDetailActivity.class);
                intent.putExtra(SmallcaseDetailActivity.TITLE, draftSmallcase.getInfo().getName());
                intent.putExtra(SmallcaseDetailActivity.DID, draftSmallcase.getDid() == null ? draftSmallcase
                        .get_id() : draftSmallcase.getDid());
                intent.putExtra(SmallcaseDetailActivity.SCID, draftSmallcase.getScid());
                intent.putStringArrayListExtra(SmallcaseDetailActivity.SIDS, sids);
                startActivity(intent);
            });

            holder.buyNow.setOnClickListener(v -> {
                presenter.getDraftSmallcase(draftSmallcase.get_id());

                Map<String, Object> eventProperties = new HashMap<>();
                eventProperties.put("smallcaseName", draftSmallcase.getInfo().getName());
                eventProperties.put("smallcaseType", draftSmallcase.getSource());
                eventProperties.put("accessedFrom", "Draft smallcase");
                eventProperties.put("smallcaseTier", draftSmallcase.getInfo().getTier());
                eventProperties.put("isInvested", false);
                eventProperties.put("isWatchlisted", false);
                analyticsContract.sendEvent(eventProperties, "Viewed Invest Popup", Analytics.MIXPANEL, Analytics.CLEVERTAP);
            });

            holder.customize.setOnClickListener(v -> {
                Map<String, Object> eventProperties = new HashMap<>();
                eventProperties.put("accessedFrom", "Drafts page");
                eventProperties.put("smallcaseName", draftSmallcase.getInfo().getName());
                eventProperties.put("smallcaseType", draftSmallcase.getSource());
                eventProperties.put("smallcaseTier", draftSmallcase.getInfo().getTier());
                analyticsContract.sendEvent(eventProperties, "Viewed Customize smallcase", Analytics.MIXPANEL, Analytics.CLEVERTAP);

                Intent intent = new Intent(DraftSmallcasesActivity.this, CreateSmallcaseActivity.class);
                intent.putExtra(CreateSmallcaseActivity.DRAFT_SMALLCASE, draftSmallcase);
                intent.putExtra(CreateSmallcaseActivity.SMALLCASE_NAME, draftSmallcase.getInfo().getName());
                intent.putExtra(CreateSmallcaseActivity.SMALLCASE_DESCRIPTION, draftSmallcase.getInfo().getShortDescription());
                intent.putExtra(CreateSmallcaseActivity.SMALLCASE_SCID, draftSmallcase.getScid());
                startActivity(intent);
            });
        }

        @Override
        public int getItemCount() {
            List<DraftSmallcase> draftSmallcases = presenter.getDraftSmallcasesList();
            if (draftSmallcases == null) return 0;
            return draftSmallcases.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.image_smallcase)
            ImageView smallcaseImage;
            @BindView(R.id.text_smallcase_name)
            GraphikText smallcaseName;
            @BindView(R.id.text_smallcase_description)
            GraphikText smallcaseDescription;
            @BindView(R.id.text_index)
            GraphikText indexText;
            @BindView(R.id.text_index_value)
            GraphikText indexValue;
            @BindView(R.id.image_up_or_down)
            ImageView upOrDown;
            @BindView(R.id.smallcase_card)
            View smallcaseCard;
            @BindView(R.id.buy_now)
            View buyNow;
            @BindView(R.id.customize)
            View customize;
            @BindView(R.id.saved_on_date)
            GraphikText savedOnDate;
            @BindView(R.id.custom_tag)
            View customTag;
            @BindView(R.id.premium_label)
            ImageView premiumLabel;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
