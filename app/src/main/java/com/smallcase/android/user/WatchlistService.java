package com.smallcase.android.user;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Smallcase;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.view.model.PExpandedSmallcase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import io.intercom.retrofit2.HttpException;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 11/04/17.
 */

class WatchlistService implements WatchlistContract.Service {
    private static final String TAG = "WatchlistService";
    private SharedPrefService sharedPrefService;
    private SmallcaseRepository smallcaseRepository;
    private WatchlistContract.View view;
    private CompositeSubscription compositeSubscription;

    WatchlistService(SharedPrefService sharedPrefService, SmallcaseRepository smallcaseRepository, WatchlistContract.View view) {
        this.sharedPrefService = sharedPrefService;
        this.smallcaseRepository = smallcaseRepository;
        this.view = view;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getWatchlistSmallcases() {
        String auth = sharedPrefService.getAuthorizationToken();
        String csrf = sharedPrefService.getCsrfToken();

        if (null == auth || null == csrf) {
            sharedPrefService.clearSharedPreference();
            view.redirectToLogin();
            return;
        }

        List<String> watchlist = new ArrayList<>(sharedPrefService.getCachedWatchlist());
        if (watchlist.isEmpty()) {
            view.noWatchlist();
            return;
        }

        compositeSubscription.add(smallcaseRepository.getSmallcases(auth, null, watchlist, null, 0, 0, 25, null, null, null, csrf, null, null)
                .subscribe(this::convertToViewModel, throwable -> {
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401)
                        return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getWatchlistSmallcases()");
                }));
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }

    private void convertToViewModel(List<Smallcase> smallcases) {
        Set<String> scids = new HashSet<>();
        for (String s : sharedPrefService.getInvestedScidsWithIScids()) {
            scids.add(s.split("#")[0]);
        }

        List<PExpandedSmallcase> smallcaseList = new ArrayList<>();
        for (Smallcase smallcase : smallcases) {

            String minInvestment = AppConstants.RUPEE_SYMBOL + AppConstants.getNumberFormatter().format(smallcase.getStats().getMinInvestAmount());
            String indexValue = String.format(Locale.getDefault(), "%.2f", smallcase.getStats().getIndexValue());
            boolean isIndexUp = smallcase.getStats().getIndexValue() > smallcase.getStats().getLastCloseIndex();
            String oneMonth = String.format(Locale.getDefault(), "%.2f", (smallcase.getStats().getReturns().getMonthly() * 100)) + "%";
            String oneYear = String.format(Locale.getDefault(), "%.2f", (smallcase.getStats().getReturns().getYearly() * 100)) + "%";
            boolean isMonthPositive = smallcase.getStats().getReturns().getMonthly() > 0;
            boolean isYearPositive = smallcase.getStats().getReturns().getYearly() > 0;
            PExpandedSmallcase expandedSmallcase = new PExpandedSmallcase("100", smallcase.getInfo().getName(), smallcase.getInfo()
                    .getShortDescription(), minInvestment, indexValue, isIndexUp, oneMonth, oneYear, isMonthPositive, isYearPositive,
                    smallcase.getScid(), scids.contains(smallcase.getScid()), smallcase.getInfo().getTier());
            smallcaseList.add(expandedSmallcase);
        }

        view.showSmallcases(smallcaseList);
    }
}
