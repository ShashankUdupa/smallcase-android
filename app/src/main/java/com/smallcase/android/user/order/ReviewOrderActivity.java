package com.smallcase.android.user.order;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.repository.AuthRepository;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.investedsmallcase.InvestedSmallcaseDetailActivity;
import com.smallcase.android.onboarding.LoginActivity;
import com.smallcase.android.rebalance.ReBalancesAvailableActivity;
import com.smallcase.android.smallcase.LoginRequest;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.user.ContainerActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.OrderLabel;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.android.ProgressBarAnimation;
import com.smallcase.android.view.model.Stock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.smallcase.android.onboarding.LoginActivity.REQUEST_TOKEN_KEY;

public class ReviewOrderActivity extends AppCompatActivity implements ReviewOrderContract.View, SmallcaseHelper.LoginResponse {
    public static final String STOCK_LIST = "stock_list"; // Mandatory
    public static final String SCID = "scid"; // Mandatory
    public static final String SMALLCASE_NAME = "smallcase_name"; // Mandatory
    public static final String INVESTMENT_AMOUNT = "investment_amount"; // Mandatory
    public static final String ISCID = "iScid";
    public static final String ORDER_LABEL = "order_label"; // Mandatory
    public static final String IS_REPAIR = "is_repair";
    public static final String BATCH_ID = "batch_id";
    public static final String SOURCE = "source"; // Mandatory
    public static final String DID = "did"; // Draft id for custom and created smallcase
    public static final String RE_BALANCE_VERSION = "rebalance_version";

    private static final String TAG = "ReviewOrderActivity";
    private static final AnticipateOvershootInterpolator ANTICIPATED_OVERSHOOT = new AnticipateOvershootInterpolator();
    private static final int LOGIN_REQUEST_CODE = 1;

    @BindView(R.id.order_status_description)
    GraphikText orderStatusDescription;
    @BindView(R.id.stocks_list)
    RecyclerView stocksList;
    @BindView(R.id.confirm_order)
    GraphikText confirmOrder;
    @BindView(R.id.confirmation_text)
    GraphikText orderConfirmation;
    @BindView(R.id.placing_order_container)
    CardView placingOrderContainer;
    @BindView(R.id.placing_order_text)
    GraphikText placingOrderText;
    @BindView(R.id.text_tool_bar_title)
    GraphikText toolbarTitle;
    @BindView(R.id.secondary_action_button)
    GraphikText secondaryActionButton;
    @BindView(R.id.order_status_icon)
    ImageView orderStatusIcon;
    @BindView(R.id.recycler_view_divider)
    View recyclerViewDivider;
    @BindView(R.id.confirm_order_card)
    View confirmOrderCard;
    @BindView(R.id.primary_action_text)
    GraphikText primaryActionText;
    @BindView(R.id.parent)
    CoordinatorLayout parent;
    @BindView(R.id.blur)
    View blur;
    @BindView(R.id.confirm_description)
    GraphikText confirmDescription;
    @BindView(R.id.confirm_card)
    CardView confirmCard;
    @BindView(R.id.confirm_container)
    View confirmContainer;
    @BindView(R.id.confirm)
    GraphikText confirm;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.circular_reveal_blue)
    View circularRevealBlue;
    @BindView(R.id.smallcase_title_card)
    View orderCard;
    @BindView(R.id.smallcase_image)
    ImageView smallcaseImage;
    @BindView(R.id.smallcase_name)
    GraphikText nameOfSmallcase;
    @BindView(R.id.batch_type)
    GraphikText batchType;
    @BindView(R.id.status)
    GraphikText status;
    @BindView(R.id.filled_quantity)
    GraphikText filledQuantity;
    @BindView(R.id.percent)
    ProgressBar percent;
    @BindView(R.id.get_order_details)
    GraphikText orderDetailsText;
    @BindView(R.id.order_status_layout)
    View orderStatusLayout;
    @BindView(R.id.bottom_bar)
    CardView bottomBar;
    @BindView(R.id.batch_details_layout)
    View batchDetailsLayout;
    @BindView(R.id.order_status_divider)
    View orderStatusDivider;
    @BindView(R.id.waiting_order)
    View waitingOrder;
    @BindView(R.id.custom_tag)
    View customTag;
    @BindView(R.id.order_flow_actions_layout)
    View orderFlowActionsLayout;

    private ReviewOrderContract.Presenter presenter;
    private String smallcaseName;
    private boolean shouldLetUserBack = true;
    private boolean isRepair = false;
    private boolean isPlaceOrder = true;
    private String orderLabel, source;
    private StocksAdapter stocksAdapter;
    private BottomSheetBehavior<View> bottomSheetBehavior;
    private boolean isAnimating = false;
    private boolean isPlacingOrder = true;
    private LoginRequest loginRequest;
    private ProgressDialog progressDialog;

    private final int marginLeft = (int) AppUtils.getInstance().dpToPx(82);
    private AnalyticsContract analyticsContract;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_order);

        ButterKnife.bind(this);
        back.setImageResource(R.drawable.ic_cancel);
        int padding = (int) AppUtils.getInstance().dpToPx(16);
        back.setPadding(padding, padding, padding, padding);

        List<Stock> stocks = getIntent().getParcelableArrayListExtra(STOCK_LIST);
        smallcaseName = getIntent().getStringExtra(SMALLCASE_NAME);
        orderLabel = getIntent().getStringExtra(ORDER_LABEL);
        isRepair = getIntent().getBooleanExtra(IS_REPAIR, false);
        String batchId = getIntent().getStringExtra(BATCH_ID);
        source = getIntent().getStringExtra(SOURCE);

        if (Build.VERSION.SDK_INT < 21) {
            bottomBar.setCardElevation(0f);
            bottomBar.setMaxCardElevation(0f);
        }

        orderDetailsText.setVisibility(View.GONE);

        toolbarTitle.setText("Review Order");

        SharedPrefService sharedPrefService = new SharedPrefService(this);
        Integer version = getIntent().getIntExtra(RE_BALANCE_VERSION, -1);
        presenter = new ReviewOrderPresenter(this, this, sharedPrefService, new MarketRepository(), stocks,
                new NetworkHelper(this), getIntent().getStringExtra(ISCID), getIntent().getDoubleExtra(INVESTMENT_AMOUNT, 0),
                getIntent().getStringExtra(SCID), batchId, source, getIntent().getStringExtra(DID), version == -1 ? null
                : version, new UserSmallcaseRepository());
        loginRequest = new SmallcaseHelper(new AuthRepository(), this, sharedPrefService);

        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(GradientDrawable.RECTANGLE);
        gradientDrawable.setCornerRadius(AppUtils.getInstance().dpToPx(3));
        gradientDrawable.setColor(ContextCompat.getColor(this, R.color.blue_800));
        confirmOrder.setBackground(gradientDrawable);
        orderConfirmation.setText(presenter.getInvestmentAmountText());

        stocksList.setNestedScrollingEnabled(false);
        stocksList.setLayoutManager(new LinearLayoutManager(this));
        stocksList.setHasFixedSize(true);

        if (Build.VERSION.SDK_INT < 21) {
            placingOrderContainer.setCardElevation(0);
            placingOrderContainer.setMaxCardElevation(0);
        }

        analyticsContract = new AnalyticsManager(getApplicationContext());
        bottomSheetBehavior = BottomSheetBehavior.from(confirmContainer);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        if (stocks != null) {
            presenter.setNumberOfStocks(stocks.size());
            stocksAdapter = new StocksAdapter(stocks);
            stocksList.setAdapter(stocksAdapter);
        }

        if (SmallcaseSource.CUSTOM.equals(source)) {
            customTag.setVisibility(View.VISIBLE);
        }

        String imageUrl;
        if (SmallcaseSource.CREATED.equals(source)) {
            imageUrl = AppConstants.CREATED_SMALLCASE_IMAGE_URL + "80/" + presenter.getScid() + ".png";
        } else {
            imageUrl = AppConstants.SMALLCASE_IMAGE_URL + "80/" + presenter.getScid() + ".png";
        }

        nameOfSmallcase.setText(smallcaseName);
        Glide.with(this)
                .load(imageUrl)
                .into(smallcaseImage);

        if (stocks != null) {
            confirmOrder.setText(stocks.size() == 1 ? "PLACE ORDER" : "PLACE ORDERS");
        }
    }

    @Override
    public void onBackPressed() {
        if (shouldLetUserBack) {
            // Show popup if repair
            if (isRepair) {
                final AlertDialog alertDialog = new AlertDialog.Builder(this)
                        .setMessage(R.string.repair_or_archive_popup)
                        .setPositiveButton(R.string.stay, (dialog, which) -> {

                        })
                        .setNegativeButton(R.string.leave, (dialog, which) -> killActivity()).create();

                alertDialog.setOnShowListener(dialog -> {
                    if (alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE) == null || alertDialog.getButton(AlertDialog
                            .BUTTON_POSITIVE) == null) return;

                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor
                            (ReviewOrderActivity.this, R.color.secondary_text));
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor
                            (ReviewOrderActivity.this, R.color.blue_800));
                });

                TextView message = alertDialog.findViewById(android.R.id.message);

                if (message != null) {
                    message.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR));
                    float space = AppUtils.getInstance().dpToPx(4);
                    message.setLineSpacing(space, 1);
                }

                alertDialog.show();
                return;
            }

            killActivity();
        }
    }

    private void killActivity() {
        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in_with_scale, R.anim.top_to_down);
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            //Received request_token from Kite login. Request authToken from server.

            showProgressDialog();
            HashMap<String, String> body = new HashMap<>();
            body.put("broker", "kite");
            body.put("reqToken", data.getStringExtra(REQUEST_TOKEN_KEY));
            body.put("app", "platform");

            loginRequest.getJwtToken(body);
        }
    }

    @Override
    protected void onDestroy() {
        presenter.destroySubscriptions();
        super.onDestroy();
    }

    private void showProgressDialog() {
        progressDialog = ProgressDialog.show(this, "", "Loading...");
        progressDialog.show();
    }

    @OnClick(R.id.primary_action_button)
    public void orderDetailsOrRepair() {
        if (isRepair) {
            // Repair
            if (!presenter.isMarketOpen()) {
                showMarketClosed();
                return;
            }

            confirmDescription.setText(presenter.getRepairDescription());
            showBlur();
            confirm.setText(getString(R.string.repair_batch));

            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("smallcaseName", smallcaseName);
            eventProperties.put("smallcaseType", (null == source || "".equals(source)) ? SmallcaseSource.PROFESSIONAL : source);
            eventProperties.put("accessedFrom", "Order Page");
            analyticsContract.sendEvent(eventProperties, "Viewed Repair Order Popup", Analytics.MIXPANEL);

            confirmCard.setOnClickListener(v -> {
                Map<String, Object> metaData = new HashMap<>();
                metaData.put("smallcaseName", smallcaseName);
                metaData.put("smallcaseType", (null == source || "".equals(source)) ? SmallcaseSource.PROFESSIONAL : source);
                metaData.put("orderType", orderLabel);
                metaData.put("accessedFrom", "Order Page");
                analyticsContract.sendEvent(metaData, "Repaired Order", Analytics.MIXPANEL, Analytics.INTERCOM);

                isPlacingOrder = true;
                batchDetailsLayout.setVisibility(View.GONE);
                orderStatusLayout.setVisibility(View.GONE);
                orderStatusDivider.setVisibility(View.GONE);
                bottomBar.setVisibility(View.VISIBLE);
                orderFlowActionsLayout.setVisibility(View.GONE);
                orderDetailsText.setVisibility(View.GONE);
                confirmOrderCard.setVisibility(View.VISIBLE);
                orderConfirmation.setVisibility(View.VISIBLE);

                orderConfirmation.setTextColor(ContextCompat.getColor(ReviewOrderActivity.this, R.color.secondary_text));
                orderConfirmation.setText(presenter.getInvestmentAmountText());
                GradientDrawable gradientDrawable = (GradientDrawable) confirmOrder.getBackground();
                gradientDrawable.setShape(GradientDrawable.RECTANGLE);
                gradientDrawable.setColor(ContextCompat.getColor(ReviewOrderActivity.this, R.color.blue_800));
                circularRevealBlue.setVisibility(View.INVISIBLE);
                isPlaceOrder = true;
                confirmOrder.setVisibility(View.VISIBLE);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                stocksAdapter.notifyDataSetChanged();
                toolbarTitle.setText("Review Order");
                confirmOrder.setText(stocksAdapter.stocks.size() == 1 ? "PLACE ORDER" : "PLACE ORDERS");
            });

            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            Intent intent;
            switch (orderLabel) {
                case OrderLabel.RE_BALANCE:
                    // Done
                    intent = new Intent(this, ReBalancesAvailableActivity.class);
                    break;

                case OrderLabel.MANAGE:
                    // Track
                    intent = new Intent(this, InvestedSmallcaseDetailActivity.class);
                    intent.putExtra(InvestedSmallcaseDetailActivity.ISCID, presenter.getIScid());
                    intent.putExtra(InvestedSmallcaseDetailActivity.SCID, presenter.getScid());
                    intent.putExtra(InvestedSmallcaseDetailActivity.SOURCE, source);
                    break;

                case OrderLabel.SELL_ALL:
                    // Investments and scroll to bottom
                    intent = new Intent(this, ContainerActivity.class);
                    intent.putExtra(ContainerActivity.INVESTMENTS, true);
                    intent.putExtra(ContainerActivity.REFRESH_INVESTMENT, true);
                    intent.putExtra(ContainerActivity.SCROLL_TO_BOTTOM, true);
                    break;

                default:
                    // Investments
                    intent = new Intent(this, ContainerActivity.class);
                    intent.putExtra(ContainerActivity.INVESTMENTS, true);
                    intent.putExtra(ContainerActivity.REFRESH_INVESTMENT, true);
                    break;
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.left_to_right, R.anim.fade_out_with_scale);
            finish();
        }
    }

    private void showBlur() {
        blur.setVisibility(View.VISIBLE);
        blur.setClickable(true);
        blur.setOnClickListener(v -> bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN));

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    blur.setClickable(false);
                    blur.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    @OnClick(R.id.secondary_action_button)
    public void archiveOrShare() {
        if (isRepair) {
            // Archive
            confirmDescription.setText(presenter.getArchiveDescription());
            showBlur();
            confirm.setText(getString(R.string.archive));

            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("smallcaseName", smallcaseName);
            eventProperties.put("smallcaseType", (null == source || "".equals(source)) ? SmallcaseSource.PROFESSIONAL : source);
            eventProperties.put("accessedFrom", "Order Page");
            analyticsContract.sendEvent(eventProperties, "Viewed Archive Order Popup", Analytics.MIXPANEL);

            confirmCard.setOnClickListener(v -> {
                Map<String, Object> metaData = new HashMap<>();
                metaData.put("smallcaseName", smallcaseName);
                metaData.put("smallcaseType", (null == source || "".equals(source)) ? SmallcaseSource.PROFESSIONAL : source);
                metaData.put("orderType", orderLabel);
                metaData.put("accessedFrom", "Order Page");
                analyticsContract.sendEvent(metaData, "Archived Order", Analytics.MIXPANEL, Analytics.INTERCOM);

                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                showProgressDialog();
                presenter.archiveOrder();
            });

            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            // Order details
            Intent intent = new Intent(this, BatchDetailsActivity.class);
            intent.putExtra(BatchDetailsActivity.ISCID, presenter.getIScid());
            intent.putExtra(BatchDetailsActivity.SCID, presenter.getScid());
            intent.putExtra(BatchDetailsActivity.SMALLCASE_NAME, smallcaseName);
            intent.putExtra(BatchDetailsActivity.SOURCE, source);
            startActivity(intent);
            overridePendingTransition(R.anim.left_to_right, R.anim.fade_out_with_scale);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @OnClick(R.id.confirm_order)
    public void placeOrConfirmOrder() {
        if (isAnimating) return;

        if (isPlaceOrder) {
            isPlaceOrder = false;

            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("smallcaseName", smallcaseName);
            eventProperties.put("smallcaseType", (null == source || "".equals(source)) ? SmallcaseSource.PROFESSIONAL : source);
            eventProperties.put("orderType", isRepair ? "FIX" : orderLabel);
            eventProperties.put("orderValue", presenter.getInvestmentAmount());
            analyticsContract.sendEvent(eventProperties, "Place Order", Analytics.MIXPANEL);

            if (Build.VERSION.SDK_INT < 21) {
                circularRevealBlue.setVisibility(View.VISIBLE);
                showPlaceOrder();
                return;
            }

            animatePlaceOrderConfirm();
        } else {
            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("smallcaseName", smallcaseName);
            eventProperties.put("smallcaseType", (null == source || "".equals(source)) ? SmallcaseSource.PROFESSIONAL : source);
            eventProperties.put("orderType", isRepair ? "FIX" : orderLabel);
            eventProperties.put("orderValue", presenter.getInvestmentAmount());

            analyticsContract.sendEvent(eventProperties, "Confirm Place Order", Analytics.MIXPANEL, Analytics.CLEVERTAP, Analytics.INTERCOM);

            isPlacingOrder = false;
            if (isRepair) {
                presenter.fixBatch();
            } else {
                presenter.placeOrder();
            }
            showPlacingOrderLoader();
            if (Build.VERSION.SDK_INT > 20) {
                int cx = placingOrderContainer.getMeasuredWidth();
                int cy = placingOrderContainer.getMeasuredHeight();

                // get the final radius for the clipping circle
                int finalRadius = Math.max(placingOrderContainer.getWidth(), placingOrderContainer.getHeight());

                // create the animator for this view (the start radius is zero)
                final Animator anim =
                        ViewAnimationUtils.createCircularReveal(placingOrderContainer, cx, cy, 0, finalRadius);

                // make the view visible and start the animation
                anim.setDuration(350);
                anim.start();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void animatePlaceOrderConfirm() {
        isAnimating = true;
        int cx = circularRevealBlue.getMeasuredWidth() - marginLeft;
        int cy = circularRevealBlue.getMeasuredHeight() / 2;

        // get the final radius for the clipping circle
        int finalRadius = Math.max(circularRevealBlue.getWidth(), circularRevealBlue.getHeight());

        // create the animator for this view (the start radius is zero)
        final Animator anim =
                ViewAnimationUtils.createCircularReveal(circularRevealBlue, cx, cy, 0, finalRadius);

        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        // make the view visible and start the animation
        circularRevealBlue.setVisibility(View.VISIBLE);
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                isAnimating = false;
                showPlaceOrder();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        anim.setDuration(500);

        final ObjectAnimator fadeOutText = ObjectAnimator.ofFloat(orderConfirmation, "alpha", 1f, 0f);
        fadeOutText.setDuration(100);
        fadeOutText.start();

        GradientDrawable gradientDrawable = (GradientDrawable) confirmOrder.getBackground();
        gradientDrawable.setShape(GradientDrawable.OVAL);
        confirmOrder.setText("");
        ObjectAnimator scale =
                ObjectAnimator.ofPropertyValuesHolder(confirmOrder,
                        PropertyValuesHolder.ofFloat("scaleX", 0.25f));
        scale.setDuration(300);
        scale.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                confirmOrder.setVisibility(View.GONE);
                anim.start();
            }
        });
        scale.start();
    }

    private void showPlaceOrder() {
        orderConfirmation.setText(presenter.getOrderConfirmationText());
        orderConfirmation.setTextColor(ContextCompat.getColor(this, R.color.white));
        confirmOrder.setText("CONFIRM");
        GradientDrawable gradientDrawable = (GradientDrawable) confirmOrder.getBackground();
        gradientDrawable.setShape(GradientDrawable.RECTANGLE);
        gradientDrawable.setColor(ContextCompat.getColor(this, R.color.green_800));
        confirmOrder.setVisibility(View.VISIBLE);
        ObjectAnimator scale =
                ObjectAnimator.ofPropertyValuesHolder(confirmOrder,
                        PropertyValuesHolder.ofFloat("scaleX", 1f));
        scale.setDuration(100);
        scale.start();

        ObjectAnimator fadeInText = ObjectAnimator.ofFloat(orderConfirmation, "alpha", 0f, 1f);
        fadeInText.setDuration(100);
        fadeInText.start();
    }

    @Override
    public void showSnackBar(int errId) {
        AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_LONG, errId);
    }

    @Override
    public void showMarketClosed() {
        Snackbar.make(parent, R.string.market_closed_now, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void allOrdersFilled(String batchType, String status, String filledVsQuantity, String source, int percent) {
        hideReviewContent();

        onOrderPlaced(status, filledVsQuantity, percent, source, batchType);

        stocksList.setVisibility(View.GONE);
        orderStatusIcon.setVisibility(View.VISIBLE);
        orderStatusLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.success_green));
        Glide.with(this)
                .load(R.drawable.icon_up)
                .into(orderStatusIcon);

        this.status.setText(status);
        filledQuantity.setText(filledVsQuantity);

        this.percent.setProgressDrawable(getResources().getDrawable(R.drawable.progress_green));
        ProgressBarAnimation anim = new ProgressBarAnimation(this.percent, this.percent.getProgress(), percent);
        anim.setInterpolator(ANTICIPATED_OVERSHOOT);
        anim.setDuration(500);
        this.percent.startAnimation(anim);

        this.batchType.setText(batchType);
        waitingOrder.setVisibility(View.GONE);

        orderStatusDescription.setText("All orders have been filled");

        bottomBar.setVisibility(View.VISIBLE);
        orderFlowActionsLayout.setVisibility(View.VISIBLE);
        orderDetailsText.setVisibility(View.VISIBLE);

        secondaryActionButton.setText("ORDER DETAILS");

        switch (orderLabel) {
            case OrderLabel.BUY:
                orderDetailsText.setText("You successfully bought " + smallcaseName + "\nThe index value for your smallcase has been set to 100 for easy tracking");
                primaryActionText.setText("SEE INVESTMENTS");
                break;

            case OrderLabel.INVEST_MORE:
                orderDetailsText.setText("You successfully invested more in " + smallcaseName);
                primaryActionText.setText("SEE INVESTMENTS");
                break;

            case OrderLabel.FIX:
                orderDetailsText.setText("You successfully fixed " + smallcaseName);
                primaryActionText.setText("SEE INVESTMENTS");
                break;

            case OrderLabel.MANAGE:
                orderDetailsText.setText("You successfully managed " + smallcaseName);
                primaryActionText.setText("TRACK");
                break;

            case OrderLabel.RE_BALANCE:
                orderDetailsText.setText("You successfully rebalanced " + smallcaseName);
                primaryActionText.setText("VIEW UPDATE");
                break;

            case OrderLabel.SELL_ALL:
                orderDetailsText.setText("You successfully exited " + smallcaseName);
                primaryActionText.setText("SEE INVESTMENTS");
                break;

            case OrderLabel.PARTIAL_EXIT:
                orderDetailsText.setText("You have partially exited from " + smallcaseName);
                primaryActionText.setText("SEE INVESTMENTS");
                break;

            case OrderLabel.SIP:
                orderDetailsText.setText("You successfully invested more in " + smallcaseName);
                primaryActionText.setText("SEE INVESTMENTS");
                break;

            default:
                primaryActionText.setText("SEE INVESTMENTS");
                break;
        }
    }

    @Override
    public void showContainerActivity() {
        Intent intent = new Intent(this, ContainerActivity.class);
        intent.putExtra(ContainerActivity.INVESTMENTS, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public String getOrderLabel() {
        return orderLabel;
    }

    @Override
    public void hidePlacingOrderLoader() {
        placingOrderContainer.setVisibility(View.INVISIBLE);
        placingOrderContainer.setClickable(false);
        shouldLetUserBack = true;
    }

    @Override
    public void showPlacingOrderLoader() {
        placingOrderText.setText("Placing your orders...");
        placingOrderContainer.setVisibility(View.VISIBLE);
        placingOrderContainer.setClickable(true);

        shouldLetUserBack = false;
        toolbarTitle.setText("Order Status");
    }

    @Override
    public void showAddFunds() {
        confirm.setText("Add funds");
        confirmCard.setCardBackgroundColor(ContextCompat.getColor(this, R.color.purple_600));

        confirmCard.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(AppConstants.KITE_ADD_FUNDS));
            startActivity(intent);
        });
    }

    @Override
    public void sendUserToKiteLogin() {
        startActivityForResult(new Intent(this, LoginActivity.class), LOGIN_REQUEST_CODE);
    }

    @Override
    public void onOrderPlaced(String status, String filledVsQuantity, int percent, String source, String batchType) {
        hideReviewContent();

        stocksList.setVisibility(View.GONE);

        this.percent.setProgress(0);
        bottomBar.setVisibility(View.GONE);
        orderFlowActionsLayout.setVisibility(View.GONE);
        orderStatusIcon.setVisibility(View.GONE);
        orderStatusLayout.setVisibility(View.VISIBLE);
        orderStatusDivider.setVisibility(View.VISIBLE);
        batchDetailsLayout.setVisibility(View.VISIBLE);
        orderStatusLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        waitingOrder.setVisibility(View.VISIBLE);

        orderStatusDescription.setText("Orders placed. Waiting for order status...");
        shouldLetUserBack = true;
        this.status.setText(status);
        filledQuantity.setText(filledVsQuantity);

        String imageUrl;
        if (SmallcaseSource.CREATED.equals(source)) {
            imageUrl = AppConstants.CREATED_SMALLCASE_IMAGE_URL + "80/" + presenter.getScid() + ".png";
        } else {
            imageUrl = AppConstants.SMALLCASE_IMAGE_URL + "80/" + presenter.getScid() + ".png";
        }
        Glide.with(this)
                .load(imageUrl)
                .into(smallcaseImage);
        this.batchType.setText(batchType);

        isRepair = false;
    }

    @Override
    public void sendUserToPreviousScreenAndReLoad() {
        final AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage(R.string.order_not_sure_text)
                .setPositiveButton("GO BACK", (dialog, which) -> {
                    LocalBroadcastManager.getInstance(ReviewOrderActivity.this).sendBroadcast(new Intent(AppConstants.HARD_RELOAD_BROADCAST));
                    finish();
                })
                .setCancelable(false)
                .create();

        alertDialog.setOnShowListener(dialog -> {
            if (alertDialog.getButton(AlertDialog.BUTTON_POSITIVE) == null) return;
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(ReviewOrderActivity.this,
                    R.color.blue_800));
        });
        alertDialog.show();
    }

    @Override
    public String getSmallcaseName() {
        return smallcaseName;
    }

    @Override
    public List<Stock> getRepairableStocks() {
        return stocksAdapter.stocks;
    }

    @Override
    public void showRepairOrders(List<Stock> stocks, String status, String filledVsQuantity, int percent, String source,
                                 String batchType) {
        hideReviewContent();
        hideOrders();

        stocksList.setVisibility(View.VISIBLE);
        shouldLetUserBack = true;

        recyclerViewDivider.setVisibility(View.VISIBLE);
        orderStatusIcon.setVisibility(View.VISIBLE);
        orderStatusDivider.setVisibility(View.VISIBLE);
        orderStatusLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.error_yellow));
        Glide.with(this)
                .load(R.drawable.failure)
                .into(orderStatusIcon);


        int percentColor;
        if (percent <= 50) {
            percentColor = R.drawable.progress_red;
        } else {
            percentColor = R.drawable.progress_purple;
        }

        this.status.setText(status);
        filledQuantity.setText(filledVsQuantity);
        this.percent.setProgressDrawable(getResources().getDrawable(percentColor));
        ProgressBarAnimation anim = new ProgressBarAnimation(this.percent, this.percent.getProgress(), percent);
        anim.setInterpolator(ANTICIPATED_OVERSHOOT);
        anim.setDuration(500);
        this.percent.startAnimation(anim);
        this.batchType.setText(batchType);

        orderStatusDescription.setText(percent == 0 ? "The batch is unfilled" : "The batch is partially filled");
        orderDetailsText.setText(percent == 0 ? "All" : "Some" + " orders in this batch are not filled\n\nYou can repair the batch (place fresh orders) " +
                "or archive the unfilled orders (only retains filled orders) to proceed");
        orderDetailsText.setVisibility(View.VISIBLE);

        isRepair = true;
        stocksAdapter.changeStocks(stocks);
        stocksAdapter.notifyDataSetChanged();
        waitingOrder.setVisibility(View.GONE);

        bottomBar.setVisibility(View.VISIBLE);
        orderFlowActionsLayout.setVisibility(View.VISIBLE);
        primaryActionText.setText(R.string.repair_batch);
        secondaryActionButton.setText(R.string.archive);

        presenter.setNumberOfStocks(stocks.size());
    }

    private void hideOrders() {
        orderDetailsText.setVisibility(View.GONE);
    }

    private void hideReviewContent() {
        placingOrderContainer.setVisibility(View.GONE);
        orderConfirmation.setVisibility(View.GONE);
        confirmOrderCard.setVisibility(View.GONE);
        recyclerViewDivider.setVisibility(View.GONE);
    }

    @Override
    public void onJwtSavedSuccessfully() {
        hideProgressDialog();
        showSnackBar(R.string.logged_in_successfully);
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onFailedFetchingJwt() {
        hideProgressDialog();
        showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onDifferentUserLoggedIn() {
        Intent intent = new Intent(this, ContainerActivity.class);
        intent.putExtra(ContainerActivity.NEW_USER, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    class StocksAdapter extends RecyclerView.Adapter<StocksAdapter.ViewHolder> {
        private List<Stock> stocks;

        StocksAdapter(List<Stock> stocks) {
            this.stocks = new ArrayList<>(stocks);
        }

        void changeStocks(List<Stock> stocks) {
            this.stocks.clear();
            this.stocks.addAll(stocks);
        }

        @Override
        public StocksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.stock_card, parent, false));
        }

        @Override
        public void onBindViewHolder(StocksAdapter.ViewHolder holder, int position) {
            if (position == 0) {
                holder.stockName.setText("Stock");
                holder.stockName.setTextColor(ContextCompat.getColor(ReviewOrderActivity.this, R.color.secondary_text));

                if (isPlacingOrder) {
                    holder.stockPrice.setVisibility(View.VISIBLE);
                    holder.stockPrice.setText("Price");
                    holder.stockPrice.setTextColor(ContextCompat.getColor(ReviewOrderActivity.this, R.color.secondary_text));
                } else {
                    holder.stockPrice.setVisibility(View.GONE);
                }

                holder.stockQuantity.setText("Qty");
                holder.stockQuantity.setTextColor(ContextCompat.getColor(ReviewOrderActivity.this, R.color.secondary_text));

                holder.stockType.setText("Type");
                holder.stockType.setTextColor(ContextCompat.getColor(ReviewOrderActivity.this, R.color.secondary_text));
                return;
            }

            Stock stock = stocks.get(position - 1);

            holder.stockName.setText(stock.getStockName());
            holder.stockName.setTextColor(ContextCompat.getColor(ReviewOrderActivity.this, R.color.primary_text));

            if (isPlacingOrder) {
                holder.stockPrice.setVisibility(View.VISIBLE);
                holder.stockPrice.setText(String.format(Locale.getDefault(), "%.1f", stock.getPrice()));
                holder.stockPrice.setTextColor(ContextCompat.getColor(ReviewOrderActivity.this, R.color.primary_text));
                if (isRepair) {
                    holder.stockQuantity.setText(String.valueOf((int) stock.getShares() - (int) stock.getFilled()));
                } else {
                    holder.stockQuantity.setText(String.valueOf((int) stock.getShares()));
                }
            } else {
                holder.stockPrice.setVisibility(View.GONE);
                holder.stockQuantity.setText(String.valueOf((int) stock.getFilled()) + "/" + String.valueOf((int) stock.getShares()));
            }

            holder.stockQuantity.setTextColor(ContextCompat.getColor(ReviewOrderActivity.this, R.color.primary_text));

            holder.stockType.setText(stock.isBuy() ? "Buy" : "Sell");
            holder.stockType.setTextColor(ContextCompat.getColor(ReviewOrderActivity.this, stock.isBuy() ?
                    R.color.green_600 : R.color.red_600));

            if (holder.getAdapterPosition() == stocks.size()) {
                holder.divider.setVisibility(View.GONE);
            } else {
                holder.divider.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public int getItemCount() {
            return stocks.size() + 1;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.stock_name)
            GraphikText stockName;
            @BindView(R.id.stock_price)
            GraphikText stockPrice;
            @BindView(R.id.stock_quantity)
            GraphikText stockQuantity;
            @BindView(R.id.type)
            GraphikText stockType;
            @BindView(R.id.stock_container)
            View stockContainer;
            @BindView(R.id.divider)
            View divider;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
