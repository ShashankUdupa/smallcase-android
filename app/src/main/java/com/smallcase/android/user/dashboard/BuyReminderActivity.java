package com.smallcase.android.user.dashboard;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Buy;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.onboarding.LandingActivity;
import com.smallcase.android.smallcase.smallcasedetail.SmallcaseDetailActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.CustomTypefaceSpan;
import com.smallcase.android.view.android.GraphikText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;

public class BuyReminderActivity extends AppCompatActivity implements BuyReminderService.BuyReminderContract {
    public static final String BUY_LIST = "buy_list";
    private static final String TAG = "BuyReminderActivity";

    @BindView(R.id.buy_remainder_list)
    RecyclerView buysReminderList;
    @BindView(R.id.text_tool_bar_title)
    GraphikText toolBarTitle;
    @BindView(R.id.empty_state_container)
    View noRemindersContainer;
    @BindView(R.id.text_empty_state)
    GraphikText noRemindersText;
    @BindView(R.id.loading_indicator)
    View loadingIndicator;
    @BindView(R.id.action_icon)
    ImageView actionIcon;
    @BindView(R.id.parent)
    View parent;
    @BindView(R.id.markets_closed)
    View marketClosed;

    private List<Buy> buys;
    private BuyAdapter buyAdapter;
    private AnalyticsContract analyticsContract;
    private BuyReminderService service;
    private AppConstants.MarketStatus marketStatus = AppConstants.MarketStatus.NOT_FETCHED;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_reminder);

        ButterKnife.bind(this);

        toolBarTitle.setText("smallcase Reminders");
        buys = getIntent().getParcelableArrayListExtra(BUY_LIST);

        actionIcon.setVisibility(View.VISIBLE);
        actionIcon.setImageResource(R.drawable.ic_more_vert_white_24dp);

        analyticsContract = new AnalyticsManager(getApplicationContext());
        SharedPrefService sharedPrefService = new SharedPrefService(this);
        service = new BuyReminderService(new UserRepository(), sharedPrefService, this, new MarketRepository());

        if ("leprechaun".equals(sharedPrefService.getUserInfo().get("brokerName"))) {
            marketStatus = AppConstants.MarketStatus.OPEN;
        } else {
            service.getMarketStatus();
        }
        if (buys != null) {
            setUpAdapter();
            return;
        }
        service.getBuyReminders();
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.action_icon)
    public void showOverFlowMenu() {
        PopupMenu popupMenu = new PopupMenu(this, actionIcon, Gravity.NO_GRAVITY, R.attr.actionOverflowMenuStyle, 0);
        popupMenu.inflate(R.menu.notification_menu);

        MenuItem menuItem = popupMenu.getMenu().findItem(R.id.mark_all_as_read);
        SpannableString title = new SpannableString("Remove all reminders");
        title.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.primary_text)), 0, title.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        title.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR)), 0, title.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        menuItem.setTitle(title);

        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.mark_all_as_read:
                    if (buys == null || buys.isEmpty()) return true;

                    service.ignoreAllBuyReminders();

                    int size = buys.size();
                    buys.clear();
                    buyAdapter.notifyItemRangeRemoved(0, size);
                    buyAdapter.notifyItemRangeChanged(0, buyAdapter.getItemCount());

                    marketClosed.setVisibility(View.GONE);
                    loadingIndicator.setVisibility(View.INVISIBLE);
                    buysReminderList.setVisibility(View.GONE);
                    noRemindersContainer.setVisibility(View.VISIBLE);
                    noRemindersText.setText("Done with all reminders");

                    Intent intent = new Intent(AppConstants.BUY_REMINDER_BROADCAST);
                    LocalBroadcastManager.getInstance(BuyReminderActivity.this).sendBroadcast(intent);
                    return true;
            }
            return false;
        });
        popupMenu.show();
    }

    @Override
    protected void onDestroy() {
        if (service != null) service.destroySubscriptions();
        super.onDestroy();
    }

    @Override
    public void redirectUserToLogin() {
        startActivity(new Intent(this, LandingActivity.class));
        finish();
    }

    @Override
    public void onBuyRemindersReceived(List<Buy> buys) {
        if (buys.isEmpty()) {
            showNoReminders();
            return;
        }

        this.buys = buys;
        setUpAdapter();
    }

    @Override
    public void onFailedFetchingBuys() {
        somethingWentWrong();
    }

    @Override
    public void onFailedIgnoringBuyReminders() {
        somethingWentWrong();
    }

    @Override
    public void onMarketStatusReceived(ResponseBody responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody.string());
            if (jsonObject.getString("data").equals("closed")) {
                marketStatus = AppConstants.MarketStatus.CLOSED;
                marketClosed.setVisibility(View.VISIBLE);
            } else {
                marketStatus = AppConstants.MarketStatus.OPEN;
                marketClosed.setVisibility(View.GONE);
            }

            if (buyAdapter != null) {
                buyAdapter.notifyDataSetChanged();
            }

        } catch (JSONException | IOException e) {
            e.printStackTrace();
            SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; onMarketStatusReceived()");

            showSnackBar(R.string.something_wrong);
        }
    }

    @Override
    public void onFailedFetchingMarketStatus() {
        if (!isFinishing()) showSnackBar(R.string.something_wrong);
    }

    private void somethingWentWrong() {
        if (!isFinishing()) showSnackBar(R.string.something_wrong);
    }

    private void showSnackBar(@StringRes int resId) {
        AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_INDEFINITE, resId);
    }

    private void setUpAdapter() {
        loadingIndicator.setVisibility(View.GONE);
        buysReminderList.setLayoutManager(new LinearLayoutManager(this));
        buyAdapter = new BuyAdapter();
        buysReminderList.setAdapter(buyAdapter);
    }

    private void showNoReminders() {
        loadingIndicator.setVisibility(View.INVISIBLE);
        buysReminderList.setVisibility(View.GONE);

        noRemindersContainer.setVisibility(View.VISIBLE);
        noRemindersText.setText("All caught up\n\nYou can set reminders to buy smallcases when the markets are closed");
    }

    class BuyAdapter extends RecyclerView.Adapter<BuyAdapter.ViewHolder> {
        private final String TAG = "BuyAdapter";

        @Override
        public BuyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.buy_smallcase_card, parent, false));
        }

        @Override
        public void onBindViewHolder(BuyAdapter.ViewHolder holder, int position) {
            final Buy buy = buys.get(position);

            if (position == buys.size() - 1) {
                holder.smallcaseDivider.setVisibility(View.GONE);
            } else {
                holder.smallcaseDivider.setVisibility(View.VISIBLE);
            }

            Glide.with(BuyReminderActivity.this)
                    .load(AppConstants.SMALLCASE_IMAGE_URL + "80/" + buy.getScid() + ".png")
                    .into(holder.smallcaseImage);
            holder.smallcaseName.setText(buy.getName());
            if (marketStatus == AppConstants.MarketStatus.OPEN) {
                holder.buySmallcase.setClickable(true);
                holder.buySmallcase.setCardBackgroundColor(ContextCompat.getColor(BuyReminderActivity.this, R.color.green_800));

                holder.buySmallcase.setOnClickListener(v -> {
                    Map<String, Object> eventProperties = new HashMap<>();
                    eventProperties.put("reminderAction", "Buy");
                    eventProperties.put("smallcaseName", buy.getName());
                    eventProperties.put("reminderDate", AppUtils.getInstance().getSdf().format(AppUtils.getInstance().getPlainDate(buy.getDate())));
                    analyticsContract.sendEvent(eventProperties, "Clicked Reminder", Analytics.MIXPANEL);

                    Intent intent = new Intent(BuyReminderActivity.this, SmallcaseDetailActivity.class);
                    intent.putExtra(SmallcaseDetailActivity.SCID, buy.getScid());
                    intent.putExtra(SmallcaseDetailActivity.TITLE, buy.getName());
                    intent.putExtra(SmallcaseDetailActivity.OPEN_BUY, true);
                    intent.putExtra(SmallcaseDetailActivity.ACCESSED_FROM, "Buy Reminder");
                    startActivity(intent);
                });
            } else {
                holder.buySmallcase.setClickable(false);
                holder.buySmallcase.setCardBackgroundColor(ContextCompat.getColor(BuyReminderActivity.this, R.color.blue_grey_100));
            }

            holder.smallcaseTitleCard.setOnClickListener(v -> {
                Intent intent = new Intent(BuyReminderActivity.this, SmallcaseDetailActivity.class);
                intent.putExtra(SmallcaseDetailActivity.SCID, buy.getScid());
                intent.putExtra(SmallcaseDetailActivity.TITLE, buy.getName());
                startActivity(intent);
            });
        }

        @Override
        public int getItemCount() {
            return buys.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.smallcase_name)
            GraphikText smallcaseName;
            @BindView(R.id.smallcase_image)
            ImageView smallcaseImage;
            @BindView(R.id.buy_smallcase)
            CardView buySmallcase;
            @BindView(R.id.smallcase_divider)
            View smallcaseDivider;
            @BindView(R.id.smallcase_title_card)
            CardView smallcaseTitleCard;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);

                if (Build.VERSION.SDK_INT < 21) {
                    smallcaseTitleCard.setCardElevation(0);
                    smallcaseTitleCard.setMaxCardElevation(0);
                }
            }
        }
    }
}
