package com.smallcase.android.user;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Notification;
import com.smallcase.android.data.model.User;
import com.smallcase.android.data.repository.AuthRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.investedsmallcase.InvestedSmallcasesFragment;
import com.smallcase.android.news.WebActivity;
import com.smallcase.android.onboarding.LandingActivity;
import com.smallcase.android.onboarding.LoginActivity;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.smallcase.discover.DiscoverFragment;
import com.smallcase.android.smallcase.discover.DiscoverSmallcaseActivity;
import com.smallcase.android.user.account.AccountFragment;
import com.smallcase.android.user.dashboard.DashboardFragment;
import com.smallcase.android.user.notification.NotificationActivity;
import com.smallcase.android.user.notification.NotificationContract;
import com.smallcase.android.user.notification.NotificationService;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.UpdateStatus;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.android.NonSwipeableViewPager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Main activity which contains all tabs. Acts as a container for {@link DashboardFragment},
 * {@link InvestedSmallcasesFragment}. Set's up and controls tab, navigation drawer events.
 *
 * @author shashankm
 */
public class ContainerActivity extends AppCompatActivity implements NotificationContract.View,
        UserService.UpdateStatusCallBack, UserService.UserDataCallback {
    public static final String INVESTMENTS = "investments";
    public static final String REFRESH_INVESTMENT = "refresh_investment";
    public static final String ACCOUNT = "account";
    public static final String SCROLL_TO_BOTTOM = "scroll_to_bottom";
    public static final String NEW_USER = "new_user";

    private static final String TAG = ContainerActivity.class.getSimpleName();
    private static final int LOGIN_REQUEST_CODE = 1;

    @BindView(R.id.navigation_tab)
    TabLayout navTab;
    @BindView(R.id.view_pager)
    NonSwipeableViewPager viewPager;
    @BindView(R.id.tool_bar)
    Toolbar toolbar;
    @BindView(R.id.text_tool_bar_title)
    GraphikText toolbarTitle;
    @BindView(R.id.nav_card)
    CardView bottomBar;
    @BindView(R.id.back)
    ImageView menu;
    @BindView(R.id.pager_container)
    CoordinatorLayout pagerContainer;
    @BindView(R.id.action_icon)
    ImageView primaryAction;
    @BindView(R.id.dirty)
    View dirty;
    @BindView(R.id.experience_pop_up)
    View experiencePopUp;
    @BindView(R.id.terms_privacy)
    GraphikText termsAndPrivacy;
    @BindView(R.id.first_time_investor)
    GraphikText firstTimeInvestor;
    @BindView(R.id.invested_in_mutual_funds)
    GraphikText investedInMutualFunds;
    @BindView(R.id.invested_in_stocks)
    GraphikText investedInStocks;
    @BindView(R.id.pop_up_blur)
    View popUpBlur;
    @BindView(R.id.get_started)
    CardView getStarted;
    @BindView(R.id.opening_card)
    View openingCard;
    @BindView(R.id.opening_title)
    GraphikText openingTitle;
    @BindView(R.id.opening_description)
    GraphikText openingDescription;
    @BindView(R.id.opening_image)
    ImageView openingImage;
    @BindView(R.id.primary_cta_text)
    GraphikText primaryCta;
    @BindView(R.id.secondary_cta_text)
    GraphikText secondaryCta;
    @BindView(R.id.secondary_cta)
    View secondaryCtaCard;
    @BindView(R.id.primary_cta)
    View primaryCtaCard;

    private Tab tab;
    public DashboardFragment dashboardFragment;
    private NetworkHelper networkHelper;
    private int unreadNotificationCount = 0;
    private SmallcaseHelper.LoginResponse loginResponse;
    private NotificationService notificationService;
    private AnalyticsContract analyticsContract;
    private InvestedSmallcasesFragment investedSmallcasesFragment;
    private AccountFragment accountFragment;
    private Animations animations;
    private int experienceSelection = -1;
    private boolean isUpdatePopupShown = false;
    private UserService userService;
    private Snackbar snackBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        ButterKnife.bind(this);

        Intercom.client().handlePushMessage();

        //Set up custom toolbar
        setSupportActionBar(toolbar);

        menu.setVisibility(View.GONE);

        int marginLeft = (int) AppUtils.getInstance().dpToPx(16);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1);
        params.leftMargin = marginLeft;
        params.gravity = Gravity.CENTER_VERTICAL;
        toolbarTitle.setLayoutParams(params);

        //Draw on status bar for lollipop or higher devices and set bottom bar elevation
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        } else {
            bottomBar.setCardElevation(0f);
            bottomBar.setMaxCardElevation(0f);
        }

        //Set up tabs with view pager fragments
        tab = new Tab();
        setUpViewPager();
        navTab.setupWithViewPager(viewPager);
        setUpNavTab();

        SharedPrefService sharedPrefService = new SharedPrefService(this);
        UserRepository userRepository = new UserRepository();
        userService = new UserService();
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            userService.getUpdateStatus(userRepository, sharedPrefService, packageInfo.versionCode, this);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        userService.getUserData(userRepository, sharedPrefService, this);
        networkHelper = new NetworkHelper(this);
        notificationService = new NotificationService(userRepository, sharedPrefService, this);
        if (!sharedPrefService.isDeviceRegisteredForNotification()) {
            notificationService.registerDevice();
        }

        notificationService.getNotifications();
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.NOTIFICATION_BROADCAST));
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.BUY_REMINDER_BROADCAST));
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.FUNDS_REFRESH));

        analyticsContract = new AnalyticsManager(getApplicationContext());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            //Received request_token from Kite login. Request authToken from server.
            HashMap<String, String> body = new HashMap<>();
            body.put("broker", "kite");
            body.put("reqToken", data.getStringExtra(LoginActivity.REQUEST_TOKEN_KEY));
            body.put("app", "platform");

            new SmallcaseHelper(new AuthRepository(), loginResponse, new SharedPrefService(this)).getJwtToken(body);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (!networkHelper.isNetworkAvailable()) showNoInternet();
    }

    @Override
    public void onBackPressed() {
        if (navTab != null && navTab.getSelectedTabPosition() != 0) {
            TabLayout.Tab tab = navTab.getTabAt(0);
            if (tab != null) {
                tab.select();
                return;
            }
        }

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
        notificationService.destroySubscriptions();
        if (snackBar != null) snackBar.dismiss();
        super.onDestroy();
    }

    @OnClick(R.id.action_icon)
    public void onPrimaryActionClicked() {
        if (navTab.getSelectedTabPosition() == 1) {
            Intent intent = new Intent(this, DiscoverSmallcaseActivity.class);
            intent.putExtra(DiscoverSmallcaseActivity.SEARCH, true);
            startActivity(intent);
        } else {
            startActivity(new Intent(this, NotificationActivity.class));
        }
    }

    public void sendUserToLogin(SmallcaseHelper.LoginResponse loginResponse) {
        this.loginResponse = loginResponse;
        startActivityForResult(new Intent(this, LoginActivity.class), LOGIN_REQUEST_CODE);
    }

    public void logoutUser() {
        clearCaches(this);
        clearWebCookies(this);
        startActivity(new Intent(this, LandingActivity.class));
        finish();
    }

    @SuppressWarnings("deprecation")
    private void clearWebCookies(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(activity);
            cookieSyncManager.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncManager.stopSync();
            cookieSyncManager.sync();
        }
    }

    private void clearCaches(Activity activity) {
        Intercom.client().reset();
        UserRepository userRepository = new UserRepository();
        notificationService = new NotificationService(userRepository, new SharedPrefService(this), null);
        SharedPrefService sharedPrefService = new SharedPrefService(activity);
        String auth = sharedPrefService.getAuthorizationToken();
        String csrf = sharedPrefService.getCsrfToken();
        logout(auth, csrf, userRepository);
        notificationService.deRegisterDevice(auth, csrf);
        sharedPrefService.clearSharedPreference();
    }

    private void logout(final String auth, String csrf, UserRepository userRepository) {
        userRepository.logoutUser(auth, csrf)
                .subscribe(responseBody -> {

                }, throwable -> {
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401)
                        return;

                    SentryAnalytics.getInstance().captureEvent(auth, throwable, TAG + "; logout()");
                });
    }

    public void gotoMySmallcases() {
        TabLayout.Tab tab = navTab.getTabAt(2);
        if (tab != null) {
            tab.select();
        }
    }

    public void gotoDiscoverSmallcases() {
        TabLayout.Tab tab = navTab.getTabAt(1);
        if (tab != null) {
            tab.select();
        }
    }

    public void showNoInternet() {
        if (!isFinishing()) {
            snackBar = Snackbar.make(pagerContainer, R.string.no_internet, Snackbar.LENGTH_INDEFINITE);
            snackBar.show();
        }
    }

    public void internetAvailable() {
        if (snackBar != null) snackBar.dismiss();
    }

    private void setUpNavTab() {
        TabLayout.Tab dashBoard = navTab.getTabAt(0);
        TabLayout.Tab discover = navTab.getTabAt(1);
        TabLayout.Tab myInvestments = navTab.getTabAt(2);
        TabLayout.Tab account = navTab.getTabAt(3);

        if (dashBoard != null && myInvestments != null && discover != null && account != null) {
            if (getIntent().getBooleanExtra(INVESTMENTS, false)) {
                dashBoard.setIcon(tab.getInActiveIcon(0));
                myInvestments.setIcon(tab.getActiveIcon(2));
                toolbarTitle.setText(tab.getTitle(2));
                myInvestments.select();
            } else {
                dashBoard.setIcon(tab.getActiveIcon(0));
                myInvestments.setIcon(tab.getInActiveIcon(2));
                toolbarTitle.setText(tab.getTitle(0));
            }
            discover.setIcon(tab.getInActiveIcon(1));
            account.setIcon(tab.getInActiveIcon(3));
        }

        primaryAction.setImageResource(R.drawable.notification);
        primaryAction.setVisibility(View.VISIBLE);
        navTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tabLayout) {
                int position = tabLayout.getPosition();
                tabLayout.setIcon(tab.getActiveIcon(position));
                toolbarTitle.setText(tab.getTitle(position));

                analyticsContract.sendEvent(null, "Viewed " + tab.getTitle(position), Analytics.MIXPANEL, Analytics.CLEVERTAP);

                if (position == 1) {
                    primaryAction.setImageResource(R.drawable.search_icon);
                    dirty.setVisibility(View.GONE);
                } else {
                    primaryAction.setImageResource(R.drawable.notification);
                    if (unreadNotificationCount > 0) {
                        dirty.setVisibility(View.VISIBLE);
                    } else {
                        dirty.setVisibility(View.GONE);
                    }
                }

                if (position == 0) {
                    // Re-fetch numbers for home ( nifty, user investments )
                    if (dashboardFragment != null) dashboardFragment.fetchEssentialDashboardData();
                }

                if (position == 2) {
                    // Re-fetch all data for investments ( because all are numbers :/ )
                    if (investedSmallcasesFragment != null) investedSmallcasesFragment.fetchData();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tabLayout) {
                tabLayout.setIcon(tab.getInActiveIcon(tabLayout.getPosition()));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setUpViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        Bundle bundle = new Bundle();
        bundle.putBoolean(SCROLL_TO_BOTTOM, getIntent().getBooleanExtra(SCROLL_TO_BOTTOM, false));
        investedSmallcasesFragment = new InvestedSmallcasesFragment();
        investedSmallcasesFragment.setArguments(bundle);

        dashboardFragment = new DashboardFragment();
        DiscoverFragment discoverFragment = new DiscoverFragment();
        accountFragment = new AccountFragment();
        adapter.addFragment(dashboardFragment);
        adapter.addFragment(discoverFragment);
        adapter.addFragment(investedSmallcasesFragment);
        adapter.addFragment(accountFragment);
        viewPager.setOffscreenPageLimit(4);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onNotificationsReceived(List<Notification> notifications) {
        for (Notification notification : notifications) {
            if (!notification.isRead()) {
                unreadNotificationCount++;
            }
        }
        if (unreadNotificationCount > 0 && navTab.getSelectedTabPosition() != 1) {
            dirty.setVisibility(View.VISIBLE);
        } else {
            dirty.setVisibility(View.GONE);
        }
    }

    @Override
    public void showSnackBar(int resId) {

    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (AppConstants.BUY_REMINDER_BROADCAST.equals(intent.getAction())) {
                if (null != dashboardFragment) {
                    dashboardFragment.hideMarketOpenIfVisible();
                }
                return;
            }

            if (AppConstants.FUNDS_REFRESH.equals(intent.getAction())) {
                if (null != accountFragment) {
                    accountFragment.refreshFunds();
                }
                return;
            }

            switch (intent.getStringExtra(AppConstants.NOTIFICATION_STATUS)) {
                case AppConstants.MARK_ALL_READ:
                    dirty.setVisibility(View.GONE);
                    unreadNotificationCount = 0;
                    break;

                case AppConstants.READ_ONE:
                    unreadNotificationCount--;
                    if (unreadNotificationCount <= 0) {
                        dirty.setVisibility(View.GONE);
                    }
                    break;

                default:
                    break;
            }
        }
    };

    public void showExperiencePopUp() {
        // Arbitrary delay to prevent jank when all the layouts are inflated at once
        new Handler().postDelayed(() -> {
            animations = new Animations();
            animations.scaleIntoPlaceAnimation(experiencePopUp);

            popUpBlur.setVisibility(View.VISIBLE);
            popUpBlur.setClickable(true);

            String agreeText = "By continuing, you are agreeing to our ";
            String termsOfUse = "terms of use";
            String connector = " and ";
            String privacyPolicy = "privacy policy";
            SpannableString spannableString = new SpannableString(agreeText + termsOfUse + connector + privacyPolicy);
            spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ContainerActivity.this, R.color.blue_800)), agreeText.length(),
                    agreeText.length() + termsOfUse.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ClickableSpan() {
                                        @Override
                                        public void onClick(View widget) {
                                            Intent intent = new Intent(ContainerActivity.this, WebActivity.class);
                                            intent.putExtra(WebActivity.URL, "https://www.smallcase.com/meta/terms");
                                            intent.putExtra(WebActivity.TITLE, "Terms");
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
                                        }
                                    }, agreeText.length(),
                    agreeText.length() + termsOfUse.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(ContainerActivity.this, R.color.blue_800)), agreeText.length()
                    + termsOfUse.length() + connector.length(), spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            spannableString.setSpan(new ClickableSpan() {
                                        @Override
                                        public void onClick(View widget) {
                                            Intent intent = new Intent(ContainerActivity.this, WebActivity.class);
                                            intent.putExtra(WebActivity.URL, "https://www.smallcase.com/meta/privacy");
                                            intent.putExtra(WebActivity.TITLE, "Privacy");
                                            startActivity(intent);
                                            overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
                                        }
                                    }, agreeText.length() + termsOfUse.length() + connector.length(),
                    spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            termsAndPrivacy.setMovementMethod(LinkMovementMethod.getInstance());
            termsAndPrivacy.setText(spannableString);
        }, 200);
    }

    @OnClick({R.id.first_time_investor, R.id.invested_in_mutual_funds, R.id.invested_in_stocks})
    public void experienceClicked(View view) {
        switch (view.getId()) {
            case R.id.first_time_investor:
                if (experienceSelection == 0) return;

                firstTimeInvestor.setTextColor(ContextCompat.getColor(this, R.color.white));
                firstTimeInvestor.setBackgroundColor(ContextCompat.getColor(this, R.color.blue_800));

                if (experienceSelection == 1) {
                    investedInMutualFunds.setTextColor(ContextCompat.getColor(this, R.color.blue_800));
                    investedInMutualFunds.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
                } else {
                    investedInStocks.setTextColor(ContextCompat.getColor(this, R.color.blue_800));
                    investedInStocks.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
                }

                experienceSelection = 0;
                break;

            case R.id.invested_in_mutual_funds:
                if (experienceSelection == 1) return;

                investedInMutualFunds.setTextColor(ContextCompat.getColor(this, R.color.white));
                investedInMutualFunds.setBackgroundColor(ContextCompat.getColor(this, R.color.blue_800));

                if (experienceSelection == 0) {
                    firstTimeInvestor.setTextColor(ContextCompat.getColor(this, R.color.blue_800));
                    firstTimeInvestor.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
                } else {
                    investedInStocks.setTextColor(ContextCompat.getColor(this, R.color.blue_800));
                    investedInStocks.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
                }

                experienceSelection = 1;
                break;

            case R.id.invested_in_stocks:
                if (experienceSelection == 2) return;

                investedInStocks.setTextColor(ContextCompat.getColor(this, R.color.white));
                investedInStocks.setBackgroundColor(ContextCompat.getColor(this, R.color.blue_800));

                if (experienceSelection == 1) {
                    investedInMutualFunds.setTextColor(ContextCompat.getColor(this, R.color.blue_800));
                    investedInMutualFunds.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
                } else {
                    firstTimeInvestor.setTextColor(ContextCompat.getColor(this, R.color.blue_800));
                    firstTimeInvestor.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
                }

                experienceSelection = 2;
                break;
        }

        if (!getStarted.isClickable()) {
            getStarted.setClickable(true);
            getStarted.setCardBackgroundColor(ContextCompat.getColor(this, R.color.blue_800));
            getStarted.setOnClickListener(v -> {
                // Dismiss popup and make network call
                HashMap<String, Object> body = new HashMap<>();
                body.put("q2", experienceSelection);
                accountFragment.updateUserProfile(body);

                animations.scaleOutAnimation(experiencePopUp, popUpBlur);
            });
        }
    }

    @Override
    public void onUpdateStatusReceived(String updateStatus) {
        final SharedPrefService sharedPrefService = new SharedPrefService(ContainerActivity.this);
        if (UpdateStatus.FORCE_UPDATE.equals(updateStatus)) {
            isUpdatePopupShown = true;
            openingCard.setVisibility(View.VISIBLE);
            Glide.with(this)
                    .load(R.drawable.old_android)
                    .into(openingImage);
            openingTitle.setText(R.string.app_old);
            openingDescription.setText(R.string.no_longer_supported);
            primaryCta.setText(R.string.update_now_caps);
            secondaryCtaCard.setVisibility(View.GONE);
            popUpBlur.setVisibility(View.VISIBLE);
            popUpBlur.setClickable(true);

            primaryCtaCard.setOnClickListener(v -> {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                }
            });
        } else if (UpdateStatus.RECOMMENDED_UPDATE.equals(updateStatus) && !sharedPrefService.hasUserIgnoredUpdate()) {
            isUpdatePopupShown = true;
            openingCard.setVisibility(View.VISIBLE);
            Glide.with(this)
                    .load(R.drawable.recommend_update)
                    .into(openingImage);
            openingTitle.setText(R.string.new_version_available);
            openingDescription.setText(R.string.update_app);
            primaryCta.setText(R.string.update_now_caps);
            secondaryCtaCard.setVisibility(View.VISIBLE);
            secondaryCta.setText(R.string.update_later_cta);
            popUpBlur.setVisibility(View.VISIBLE);
            popUpBlur.setClickable(true);

            primaryCtaCard.setOnClickListener(v -> {
                popUpBlur.setVisibility(View.GONE);
                openingCard.setVisibility(View.GONE);
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
                }
            });

            secondaryCtaCard.setOnClickListener(v -> {
                sharedPrefService.setUserIgnoredUpdate();
                popUpBlur.setVisibility(View.GONE);
                openingCard.setVisibility(View.GONE);
            });
        } else {
            isUpdatePopupShown = false;
            openingCard.setVisibility(View.GONE);
            popUpBlur.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFailedFetchingUpdateStatus() {
        showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onUserDataReceived(User user) {
        if (isUpdatePopupShown) return;

        if (user.getFlags().getMarkers() != null && !user.getFlags().getMarkers().isPremiumPopup()) {
            openingCard.setVisibility(View.VISIBLE);
            openingTitle.setText(R.string.introducting_smallcase_select);
            openingDescription.setText(R.string.smallcase_select_description);
            primaryCta.setText(R.string.view_smallcase_caps);
            secondaryCtaCard.setVisibility(View.VISIBLE);
            secondaryCta.setText(R.string.check_later);
            popUpBlur.setVisibility(View.VISIBLE);

            primaryCtaCard.setOnClickListener(v -> {
                Intent intent = new Intent(ContainerActivity.this, DiscoverSmallcaseActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.left_to_right, R.anim.fade_out_with_scale);

                popUpBlur.setVisibility(View.GONE);
                openingCard.setVisibility(View.GONE);
            });

            secondaryCtaCard.setOnClickListener(v -> {
                popUpBlur.setVisibility(View.GONE);
                openingCard.setVisibility(View.GONE);
            });

            Glide.with(this)
                    .load(R.drawable.premium_smallcase_popup)
                    .into(openingImage);
            HashMap<String, Object> body = new HashMap<>();
            body.put("flag", "premiumPopup2");
            body.put("type", "markers");
            body.put("value", true);
            userService.setFlag(new UserRepository(), new SharedPrefService(this), body);
        } else {
            openingCard.setVisibility(View.GONE);
            popUpBlur.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFailedFetchingUserdata() {
        showSnackBar(R.string.something_wrong);
    }

    private static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        void addFragment(Fragment fragment) {
            fragmentList.add(fragment);
        }
    }

    private class Tab {
        private int[] mActiveIcons = {R.drawable.icon_home_active, R.drawable.icon_compass_active, R.drawable.briefcase_active, R.drawable.icon_account_active};
        private int[] mInActiveIcons = {R.drawable.icon_home, R.drawable.icon_compass, R.drawable.briefcase, R.drawable.icon_account};
        private String[] mTitles = {"Home", "Discover", "Investments", "Account"};

        int getActiveIcon(int position) {
            return this.mActiveIcons[position];
        }

        int getInActiveIcon(int position) {
            return this.mInActiveIcons[position];
        }

        String getTitle(int position) {
            return this.mTitles[position];
        }
    }
}
