package com.smallcase.android.user.dashboard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.model.Buy;
import com.smallcase.android.data.model.ReBalanceUpdate;
import com.smallcase.android.data.model.SipDetails;
import com.smallcase.android.data.repository.InvestmentRepository;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.investedsmallcase.sip.AllSipsActivity;
import com.smallcase.android.news.AllNewsActivity;
import com.smallcase.android.rebalance.ReBalancesAvailableActivity;
import com.smallcase.android.smallcase.discover.DiscoverSmallcaseActivity;
import com.smallcase.android.smallcase.discover.SmallcaseNewsAdapter;
import com.smallcase.android.user.ContainerActivity;
import com.smallcase.android.user.WatchlistActivity;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.InvestmentHelper;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.android.AndroidApp;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PSmallcaseWithNews;
import com.squareup.leakcanary.RefWatcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Contains a recycler view for displaying various types of views in dashboard
 * of the app. Resides within {@link ContainerActivity}
 */
public class DashboardFragment extends Fragment implements DashboardContract.View, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "DashboardFragment";

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.action)
    View tryAgain;
    @BindView(R.id.action_text)
    GraphikText actionText;
    @BindView(R.id.empty_state_container)
    View noInvestmentsContainer;
    @BindView(R.id.parent)
    View parentView;
    @BindView(R.id.text_empty_state)
    GraphikText emptyState;

    @BindView(R.id.text_nifty_value)
    GraphikText niftyValue;
    @BindView(R.id.nifty_mood)
    View niftyMood;
    @BindView(R.id.pending_actions_text)
    View pendingActionText;

    @BindView(R.id.image_p_l_today)
    ImageView news;
    @BindView(R.id.text_profit_and_loss)
    GraphikText profitAndLoss;
    @BindView(R.id.p_and_l_container)
    View container;

    @BindView(R.id.greeting)
    GraphikText greeting;
    @BindView(R.id.nifty_loading)
    View niftyLoading;
    @BindView(R.id.p_and_l_loading)
    View pAndLLoading;

    @BindView(R.id.watchlist_news_list)
    RecyclerView watchlist;
    @BindView(R.id.see_all_watchlist_news_container)
    View seeAllWatchlistNews;
    @BindView(R.id.investment_card)
    View investmentCard;

    @BindView(R.id.watchlist_container)
    View watchlistContainer;
    @BindView(R.id.re_balance_container)
    View reBalanceContainer;
    @BindView(R.id.image_re_balance)
    ImageView reBalanceIcon;
    @BindView(R.id.number_of_re_balance)
    GraphikText reBalancesAvailableText;

    @BindView(R.id.discover_smallcases)
    View discoverSmallcases;
    @BindView(R.id.market_open_container)
    View marketOpenContainer;
    @BindView(R.id.re_balance_divider)
    View reBalanceDivider;
    @BindView(R.id.image_market_open)
    ImageView marketOpenImage;
    @BindView(R.id.market_open_text)
    GraphikText marketOpenText;
    @BindView(R.id.pending_actions_card)
    View pendingActionsCard;
    @BindView(R.id.no_watchlist_news_container)
    View noWatchlistNewsContainer;
    @BindView(R.id.no_news_icon)
    ImageView noNewsIcon;
    @BindView(R.id.no_news_title)
    GraphikText noNewsTitle;
    @BindView(R.id.no_news_description)
    GraphikText noNewsDescription;
    @BindView(R.id.see_watchlist_smallcase_card)
    View seeWatchlistSmallcaseCard;

    @BindView(R.id.sip_container)
    View sipContainer;
    @BindView(R.id.image_sip)
    ImageView sipIcon;
    @BindView(R.id.sip_divider)
    View sipDivider;
    @BindView(R.id.sip_pending_text)
    GraphikText pendingSips;

    //Unbind views from ButterKnife when fragment gets destroyed
    private Unbinder unbinder;
    private DashboardContract.Presenter dashboardPresenter;
    private NetworkHelper networkHelper;
    private Animations animation;
    private SmallcaseNewsAdapter smallcaseNewsAdapter;
    private Context context;
    private AnalyticsContract analyticsContract;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        SharedPrefService sharedPrefService = new SharedPrefService(context);
        networkHelper = new NetworkHelper(context);
        dashboardPresenter = new DashboardPresenter((Activity) context, new MarketRepository(), new InvestmentRepository(), this,
                sharedPrefService, networkHelper, new SmallcaseRepository(), new InvestmentHelper(), new UserRepository());
        analyticsContract = new AnalyticsManager(getActivity().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);

        watchlist.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        watchlist.setHasFixedSize(true);

        smallcaseNewsAdapter = new SmallcaseNewsAdapter((Activity) context, analyticsContract, "Home");
        watchlist.setAdapter(smallcaseNewsAdapter);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.blue_800);

        animation = new Animations();
        animation.loadingContent(niftyLoading, pAndLLoading);

        greeting.setText(dashboardPresenter.getGreeting());
        dashboardPresenter.fetchData(0, 5);
        return view;
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        RefWatcher refWatcher = AndroidApp.getRefWatcher(getActivity());
        refWatcher.watch(this);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        dashboardPresenter.destroySubscriptions();
        super.onDestroy();
    }

    @OnClick(R.id.action)
    void actionButtonClicked() {
        if (actionText.getText().toString().equals(getString(R.string.try_again))) {
            // If action button is try again, then fetch data
            hideTryAgainLayout();
            swipeRefreshLayout.setVisibility(View.VISIBLE);
            dashboardPresenter.fetchData(0, 5);
        } else {
            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("accessedFrom", "Home");
            analyticsContract.sendEvent(eventProperties, "Viewed Discover", Analytics.MIXPANEL, Analytics.CLEVERTAP);
            // If action button is not try again, it's "Start Investing" and this opens discover smallcase
            Intent intent = new Intent(context, DiscoverSmallcaseActivity.class);
            intent.putExtra(DiscoverSmallcaseActivity.RECENT, true);
            startActivity(intent);
        }
    }

    @OnClick(R.id.see_all_watchlist_news_container)
    public void seeAllWatchlistedNews() {
        if (context == null || null == smallcaseNewsAdapter.getNews() || smallcaseNewsAdapter.getNews().isEmpty()) {
            return;
        }

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("accessedFrom", "Home Watchlist See All");
        analyticsContract.sendEvent(eventProperties, "Viewed News Feed", Analytics.MIXPANEL, Analytics.CLEVERTAP);

        Intent intent = new Intent(context, AllNewsActivity.class);
        intent.putExtra(AllNewsActivity.SCREEN_TITLE, "News for your watchlist");
        intent.putParcelableArrayListExtra(AllNewsActivity.DATA_LIST, (ArrayList<? extends Parcelable>) smallcaseNewsAdapter.getNews());
        intent.putStringArrayListExtra(AllNewsActivity.SCID_LIST, dashboardPresenter.getWatchlist());
        intent.putExtra(AllNewsActivity.TYPE, AppConstants.NEWS_TYPE_WATCHLISTED);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);

        greeting.setText(dashboardPresenter.getGreeting());
        dashboardPresenter.fetchData(0, 5);
    }

    @Override
    public void invalidAuthToken() {
        // Log user out user
        if (context == null) return;

        ((ContainerActivity) context).logoutUser();
    }

    @Override
    public void onWatchlistedSmallcaseReceived(List<PSmallcaseWithNews> smallcaseWithNewses) {
        if (!isAdded()) return;

        stopRefreshing();
        hideTryAgainLayout();
        smallcaseNewsAdapter.onNewsReceived(smallcaseWithNewses);
    }

    @Override
    public void showNoWatchlist() {
        seeAllWatchlistNews.setVisibility(View.GONE);
        watchlist.setVisibility(View.GONE);

        noWatchlistNewsContainer.setVisibility(View.VISIBLE);
        noNewsTitle.setText("No smallcases in your watchlist");
        noNewsDescription.setText("Click the Watch icon on the smallcase page to see news about it here");
        Glide.with(this)
                .load(R.drawable.no_watchlist_with_icon)
                .into(noNewsIcon);
    }

    @Override
    public void hideReBalanceUpdatesIfVisible() {
        if (reBalanceContainer.getVisibility() == View.VISIBLE) {
            reBalanceContainer.setVisibility(View.GONE);
        }

        if (areOtherPendingActionsVisible()) {
            pendingActionText.setVisibility(View.GONE);
            pendingActionsCard.setVisibility(View.GONE);
        }
    }

    @Override
    public void showReBalanceUpdates(@NonNull final List<ReBalanceUpdate> reBalanceUpdates) {
        pendingActionsCard.setVisibility(View.VISIBLE);
        pendingActionText.setVisibility(View.VISIBLE);
        reBalanceContainer.setVisibility(View.VISIBLE);

        Glide.with(context)
                .load(R.drawable.notify_rebalance)
                .into(reBalanceIcon);
        String reBalanceText = reBalanceUpdates.size() == 1 ? ("You have rebalance update for 1 smallcase") :
                ("You have rebalance updates for " + reBalanceUpdates.size() + " smallcases");
        reBalancesAvailableText.setText(reBalanceText);

        reBalanceContainer.setOnClickListener(v -> {
            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("accessedFrom", "Home");
            analyticsContract.sendEvent(eventProperties, "Viewed Rebalance List", Analytics.MIXPANEL);

            startActivity(new Intent(context, ReBalancesAvailableActivity.class));
        });
    }

    @Override
    public void niftyValueMessage(@ColorRes int resId, String message) {
        if (!isAdded()) return;

        hideTryAgainLayout();
        stopRefreshing();
        niftyLoading.setVisibility(View.GONE);
        animation.stopAnimation();

        if (networkHelper.isNetworkAvailable()) {
            ((ContainerActivity) getActivity()).internetAvailable();
        }

        niftyValue.setText(message);
        niftyMood.setBackgroundColor(ContextCompat.getColor(context, resId));
    }

    @Override
    public void showWatchListIfInvisible() {
        if (watchlist.getVisibility() == View.GONE) {
            seeAllWatchlistNews.setVisibility(View.VISIBLE);
            watchlist.setVisibility(View.VISIBLE);
            noWatchlistNewsContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void hideMarketOpenIfVisible() {
        if (!isAdded()) return;

        if (marketOpenContainer.getVisibility() == View.VISIBLE) {
            marketOpenContainer.setVisibility(View.GONE);
            reBalanceDivider.setVisibility(View.GONE);
        }

        if (areOtherPendingActionsVisible()) {
            pendingActionText.setVisibility(View.GONE);
            pendingActionsCard.setVisibility(View.GONE);
        }
    }

    private boolean areOtherPendingActionsVisible() {
        return reBalanceContainer.getVisibility() != View.VISIBLE && marketOpenContainer.getVisibility() != View.VISIBLE &&
                sipContainer.getVisibility() != View.VISIBLE;
    }

    @Override
    public void showMarketOpenReminder(final List<Buy> buys) {
        pendingActionsCard.setVisibility(View.VISIBLE);
        pendingActionText.setVisibility(View.VISIBLE);
        marketOpenContainer.setVisibility(View.VISIBLE);

        if (reBalanceContainer.getVisibility() == View.VISIBLE) {
            reBalanceDivider.setVisibility(View.VISIBLE);
        }

        Glide.with(this)
                .load(R.drawable.opening_bell)
                .into(marketOpenImage);
        String marketOpenReminder = buys.size() == 1 ? "You have set reminder for 1 smallcase" : "You have set reminders for "
                + buys.size() + " smallcases";
        marketOpenText.setText(marketOpenReminder);
        marketOpenContainer.setOnClickListener(v -> {
            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("accessedFrom", "Home");
            analyticsContract.sendEvent(eventProperties, "Viewed Reminder List", Analytics.MIXPANEL, Analytics.CLEVERTAP);

            Intent intent = new Intent(context, BuyReminderActivity.class);
            intent.putParcelableArrayListExtra(BuyReminderActivity.BUY_LIST, (ArrayList<Buy>) buys);
            startActivity(intent);
        });
    }

    @Override
    public void showNoWatchlistNews() {
        seeAllWatchlistNews.setVisibility(View.GONE);
        watchlist.setVisibility(View.GONE);

        noWatchlistNewsContainer.setVisibility(View.VISIBLE);
        noNewsTitle.setText(R.string.no_news_is_good_news);
        noNewsDescription.setText(R.string.you_can_see_amp_read_news_that_impact_your_watchlisted_smallcases_here);
        Glide.with(this)
                .load(R.drawable.no_news_for_watchlist)
                .into(noNewsIcon);
    }

    @Override
    public void toggleNewsSeeAll(int visibility) {
        if (!isAdded()) return;

        seeAllWatchlistNews.setVisibility(visibility);
    }

    @Override
    public void showWatchingSmallcasesLink() {
        if (seeWatchlistSmallcaseCard.getVisibility() == View.GONE) {
            seeWatchlistSmallcaseCard.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideWatchingSmallcasesLink() {
        if (seeWatchlistSmallcaseCard.getVisibility() == View.VISIBLE) {
            seeWatchlistSmallcaseCard.setVisibility(View.GONE);
        }
    }

    @Override
    public void hideSipsIfVisible() {
        if (!isAdded()) return;

        if (sipContainer.getVisibility() == View.VISIBLE) {
            sipContainer.setVisibility(View.GONE);
            sipDivider.setVisibility(View.GONE);
        }

        if (areOtherPendingActionsVisible()) {
            pendingActionText.setVisibility(View.GONE);
            pendingActionsCard.setVisibility(View.GONE);
        }
    }

    @Override
    public void showSipsRemaining(final List<SipDetails> sips) {
        pendingActionsCard.setVisibility(View.VISIBLE);
        pendingActionText.setVisibility(View.VISIBLE);
        sipContainer.setVisibility(View.VISIBLE);

        if (reBalanceContainer.getVisibility() == View.VISIBLE || marketOpenContainer.getVisibility() == View.VISIBLE) {
            sipDivider.setVisibility(View.VISIBLE);
        }

        Glide.with(this)
                .load(R.drawable.sip_icon)
                .into(sipIcon);
        String marketOpenReminder = sips.size() == 1 ? "You have 1 SIP instalment due" : "You have "
                + sips.size() + " SIP instalments due";
        pendingSips.setText(marketOpenReminder);
        sipContainer.setOnClickListener(v -> {
            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("accessedFrom", "Home");
            analyticsContract.sendEvent(eventProperties, "Viewed SIP Reminder List", Analytics.MIXPANEL, Analytics.CLEVERTAP);

            Intent intent = new Intent(context, AllSipsActivity.class);
            startActivity(intent);
        });
    }

    @OnClick(R.id.see_watchlist_smallcase_card)
    public void seeWatchlistClicked() {
        if (!isAdded()) return;

        startActivity(new Intent(context, WatchlistActivity.class));
    }

    @Override
    public void noInvestments() {
        if (!isAdded()) return;

        investmentCard.setVisibility(View.GONE);
        discoverSmallcases.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.discover_smallcases)
    public void seeAllSmallcases() {
        if (!isAdded()) return;

        ((ContainerActivity) context).gotoDiscoverSmallcases();
    }

    @Override
    public void onPAndLReceived(SpannableString currentPAndL) {
        if (!isAdded()) return;

        if (discoverSmallcases.getVisibility() == View.VISIBLE) {
            discoverSmallcases.setVisibility(View.GONE);
            investmentCard.setVisibility(View.VISIBLE);
        }

        hideTryAgainLayout();
        stopRefreshing();
        pAndLLoading.setVisibility(View.GONE);
        animation.stopAnimation();

        if (networkHelper.isNetworkAvailable()) {
            ((ContainerActivity) getActivity()).internetAvailable();
        }

        news.setBackgroundColor(0);
        profitAndLoss.setVisibility(View.VISIBLE);

        Glide.with(getContext())
                .load(R.drawable.ic_pltrack)
                .asBitmap()
                .placeholder(getResources().getDrawable(R.color.grey_300))
                .into(news);
        profitAndLoss.setText(currentPAndL);
        container.setOnClickListener(v -> ((ContainerActivity) getActivity()).gotoMySmallcases());
    }

    @Override
    public void showNoNetworkAvailable() {
        if (!isAdded()) return;

        ((ContainerActivity) getActivity()).showNoInternet();
    }

    @Override
    public void onFailure() {
        if (!isAdded()) return;

        stopRefreshing();
        swipeRefreshLayout.setVisibility(View.GONE);
        tryAgain.setVisibility(View.VISIBLE);
        noInvestmentsContainer.setVisibility(View.VISIBLE);
        emptyState.setText(getString(R.string.something_wrong));
    }

    @Override
    public void showSnackBar(int resId) {
        if (!isAdded()) return;

        Snackbar.make(swipeRefreshLayout, resId, Snackbar.LENGTH_LONG).show();
    }

    @OnClick(R.id.see_invested_news)
    public void seeAllNews() {
        if (context == null) return;

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("accessedFrom", "Home Invested See All");
        analyticsContract.sendEvent(eventProperties, "Viewed News Feed", Analytics.MIXPANEL, Analytics.CLEVERTAP);

        dashboardPresenter.onSeeAllClicked();
    }

    private void hideTryAgainLayout() {
        if (tryAgain.getVisibility() == View.VISIBLE || noInvestmentsContainer.getVisibility() == View.VISIBLE) {
            noInvestmentsContainer.setVisibility(View.GONE);
            tryAgain.setVisibility(View.GONE);
        }
    }

    private void stopRefreshing() {
        if (null != swipeRefreshLayout && swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    public void fetchEssentialDashboardData() {
        if (!isAdded()) return;

        dashboardPresenter.fetchNifty();
        dashboardPresenter.fetchTotalInvestment();
        dashboardPresenter.fetchPendingActions();
    }
}
