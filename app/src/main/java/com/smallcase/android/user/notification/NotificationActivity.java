package com.smallcase.android.user.notification;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.model.Notification;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.investedsmallcase.sip.AllSipsActivity;
import com.smallcase.android.rebalance.ReBalancesAvailableActivity;
import com.smallcase.android.user.order.BatchDetailsActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NotificationType;
import com.smallcase.android.view.android.CustomTypefaceSpan;
import com.smallcase.android.view.android.GraphikText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NotificationActivity extends AppCompatActivity implements NotificationContract.View {
    @BindView(R.id.text_tool_bar_title)
    GraphikText toolbarTitle;
    @BindView(R.id.notifications_list)
    RecyclerView notificationsList;
    @BindView(R.id.parent)
    View parent;
    @BindView(R.id.loading_indicator)
    View loadingIndicator;
    @BindView(R.id.action_icon)
    ImageView actionIcon;
    @BindView(R.id.empty_state_container)
    View noNotificationContainer;
    @BindView(R.id.text_empty_state)
    GraphikText emptyStateText;
    @BindView(R.id.empty_state_icon)
    ImageView noNotificationsIcon;

    private NotificationContract.Service service;
    private NotificationAdapter notificationAdapter;
    private AnalyticsContract analyticsContract;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        ButterKnife.bind(this);
        toolbarTitle.setText("Notifications");
        notificationsList.setLayoutManager(new LinearLayoutManager(this));
        notificationsList.setHasFixedSize(true);

        actionIcon.setImageResource(R.drawable.ic_more_vert_white_24dp);

        analyticsContract = new AnalyticsManager(getApplicationContext());
        analyticsContract.sendEvent(null, "Viewed Notifications", Analytics.MIXPANEL);
        service = new NotificationService(new UserRepository(), new SharedPrefService(this), this);
        service.getNotifications();
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        service.destroySubscriptions();
        super.onDestroy();
    }

    @OnClick(R.id.action_icon)
    public void showOverFlowMenu() {
        PopupMenu popupMenu = new PopupMenu(this, actionIcon, Gravity.NO_GRAVITY, R.attr.actionOverflowMenuStyle, 0);
        popupMenu.inflate(R.menu.notification_menu);

        MenuItem menuItem = popupMenu.getMenu().findItem(R.id.mark_all_as_read);
        SpannableString title = new SpannableString("Mark all as read");
        title.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.primary_text)), 0, title.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        title.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR)), 0, title.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        title.setSpan(new RelativeSizeSpan(0.9f), 0, title.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        menuItem.setTitle(title);

        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.mark_all_as_read:
                    service.markAllAsRead();

                    if (notificationAdapter != null) {
                        notificationAdapter.markAllNotificationsAsRead();
                        Intent intent = new Intent(AppConstants.NOTIFICATION_BROADCAST);
                        intent.putExtra(AppConstants.NOTIFICATION_STATUS, AppConstants.MARK_ALL_READ);
                        LocalBroadcastManager.getInstance(NotificationActivity.this).sendBroadcast(intent);
                    }
                    return true;
            }
            return false;
        });
        popupMenu.show();
    }

    @Override
    public void onNotificationsReceived(List<Notification> notifications) {
        if (notifications.isEmpty()) {
            loadingIndicator.setVisibility(View.INVISIBLE);
            notificationsList.setVisibility(View.GONE);
            noNotificationContainer.setVisibility(View.VISIBLE);
            Glide.with(this)
                    .load(R.drawable.no_notifications)
                    .into(noNotificationsIcon);
            emptyStateText.setText("There’s nothing here\n\nYou’ll receive order updates and reminders for your smallcases here");
            return;
        }

        actionIcon.setVisibility(View.VISIBLE);
        loadingIndicator.setVisibility(View.GONE);
        notificationAdapter = new NotificationAdapter(notifications);
        notificationsList.setAdapter(notificationAdapter);
    }

    @Override
    public void showSnackBar(int resId) {
        AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_LONG, resId);
    }

    class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
        private final String TAG = NotificationAdapter.class.getSimpleName();

        private List<Notification> notifications;
        private final float TWO_DP = AppUtils.getInstance().dpToPx(2);

        NotificationAdapter(List<Notification> notifications) {
            this.notifications = new ArrayList<>(notifications);
        }

        @Override
        public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_card,
                    parent, false));
        }

        @Override
        public void onBindViewHolder(final NotificationAdapter.ViewHolder holder, int position) {
            final Notification notification = notifications.get(holder.getAdapterPosition());
            if (notification.isRead()) {
                holder.notificationCard.setBackgroundColor(ContextCompat.getColor(NotificationActivity.this, R.color.white));
            } else {
                holder.notificationCard.setBackgroundColor(ContextCompat.getColor(NotificationActivity.this, R.color.blue_100));
            }

            holder.notificationTitle.setText(notification.getHeader());

            int drawableIcon = 0;
            int notificationImage = 0;
            switch (notification.getType()) {
                case NotificationType.ORDER_UPDATE:
                    drawableIcon = R.drawable.icon_up;
                    holder.notificationMessageIcon.setVisibility(View.VISIBLE);
                    break;

                case NotificationType.REBALANCE_UPDATE:
                    notificationImage = R.drawable.notify_rebalance;
                    holder.notificationMessageIcon.setVisibility(View.GONE);
                    break;

                case NotificationType.MARKET_OPEN_REMINDER:
                    notificationImage = R.drawable.opening_bell;
                    holder.notificationMessageIcon.setVisibility(View.GONE);
                    break;

                case NotificationType.ALERT:
                    notificationImage = R.mipmap.ic_launcher;
                    holder.notificationMessageIcon.setVisibility(View.GONE);
                    break;

                case NotificationType.SIP_UPDATE:
                    notificationImage = R.drawable.sip_icon;
                    holder.notificationMessageIcon.setVisibility(View.GONE);
                    break;

                default:
                    holder.notificationMessageIcon.setVisibility(View.VISIBLE);
                    break;
            }

            Glide.with(NotificationActivity.this)
                    .load(notificationImage == 0 ? notification.getImage("80") : notificationImage)
                    .asBitmap()
                    .placeholder(getResources().getDrawable(R.color.grey_300))
                    .into(new BitmapImageViewTarget(holder.notificationPic) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable roundImage = RoundedBitmapDrawableFactory.create(getResources(), resource);
                            roundImage.setCornerRadius(TWO_DP);
                            holder.notificationPic.setImageDrawable(roundImage);
                        }
                    });

            if (drawableIcon != 0) {
                Glide.with(NotificationActivity.this)
                        .load(drawableIcon)
                        .into(holder.notificationMessageIcon);
            }
            holder.message.setText(notification.getText());
            holder.timeAgo.setText(AppUtils.getInstance().getTimeAgoDate(notification.getTime()));

            holder.notificationCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!notification.isRead()) {
                        service.markNotificationAsRead(notification.get_id());

                        notification.setRead(true);
                        notifyItemChanged(holder.getAdapterPosition());

                        Intent intent = new Intent(AppConstants.NOTIFICATION_BROADCAST);
                        intent.putExtra(AppConstants.NOTIFICATION_STATUS, AppConstants.READ_ONE);
                        LocalBroadcastManager.getInstance(NotificationActivity.this).sendBroadcast(intent);
                    }

                    if (NotificationType.SIP_UPDATE.equals(notification.getType())) {
                        startActivity(new Intent(NotificationActivity.this, AllSipsActivity.class));
                        overridePendingTransition(R.anim.left_to_right, R.anim.fade_out_with_scale);
                        return;
                    }

                    if (NotificationType.REBALANCE_UPDATE.equals(notification.getType())) {
                        Map<String, Object> eventProperties = new HashMap<>();
                        eventProperties.put("accessedFrom", "Notification");
                        analyticsContract.sendEvent(eventProperties, "Viewed Rebalance List", Analytics.MIXPANEL);

                        startActivity(new Intent(NotificationActivity.this, ReBalancesAvailableActivity.class));
                        overridePendingTransition(R.anim.left_to_right, R.anim.fade_out_with_scale);
                        return;
                    }

                    if (NotificationType.ALERT.equals(notification.getType())) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(notification.getLink()));
                        startActivity(intent);
                        return;
                    }

                    Intent intent = new Intent(NotificationActivity.this, BatchDetailsActivity.class);
                    intent.putExtra(BatchDetailsActivity.ISCID, notification.getIscid());
                    intent.putExtra(BatchDetailsActivity.SCID, notification.getScid());
                    intent.putExtra(BatchDetailsActivity.SMALLCASE_NAME, notification.getHeader());
                    intent.putExtra(BatchDetailsActivity.SOURCE, notification.getSource());
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_to_right, R.anim.fade_out_with_scale);
                }
            });
        }

        void markAllNotificationsAsRead() {
            for (Notification notification : notifications) {
                notification.setRead(true);
            }
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return notifications.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.notification_card)
            View notificationCard;
            @BindView(R.id.notification_pic)
            ImageView notificationPic;
            @BindView(R.id.notification_title)
            GraphikText notificationTitle;
            @BindView(R.id.notification_message_icon)
            ImageView notificationMessageIcon;
            @BindView(R.id.message)
            GraphikText message;
            @BindView(R.id.time_ago)
            GraphikText timeAgo;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
