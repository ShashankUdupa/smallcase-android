package com.smallcase.android.user.coustomize;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;

import java.util.List;

import io.sentry.event.Breadcrumb;
import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 29/05/17.
 */

class DraftService implements DraftContract.Service {
    private static final String TAG = "DraftService";

    private UserSmallcaseRepository userSmallcaseRepository;
    private SharedPrefService sharedPrefService;
    private DraftContract.Presenter presenter;
    private CompositeSubscription compositeSubscription;
    private MarketRepository marketRepository;

    DraftService(UserSmallcaseRepository userSmallcaseRepository, SharedPrefService sharedPrefService, DraftContract
            .Presenter presenter, MarketRepository marketRepository) {
        this.userSmallcaseRepository = userSmallcaseRepository;
        this.sharedPrefService = sharedPrefService;
        this.presenter = presenter;
        this.marketRepository = marketRepository;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getDraftSmallcases() {
        compositeSubscription.add(userSmallcaseRepository.getDrafts(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(new Action1<List<DraftSmallcase>>() {
                    @Override
                    public void call(List<DraftSmallcase> draftSmallcases) {
                        presenter.onDraftSmallcasesFetched(draftSmallcases);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedFetchingDraftSmallcases();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getDraftSmallcases()");
                    }
                }));
    }

    @Override
    public void getIfMarketIsOpen() {
        compositeSubscription.add(marketRepository.getIfMarketIsOpen(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        presenter.onMarketStatusReceived(responseBody);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedFetchingMarketStatus();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getIfMarketIsOpen()");
                    }
                }));
    }

    @Override
    public void getDraftSmallcase(String did) {
        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.USER, "Did: " + did, null);
        compositeSubscription.add(userSmallcaseRepository.getDraft(sharedPrefService.getAuthorizationToken(), sharedPrefService
                .getCsrfToken(), did)
                .subscribe(new Action1<DraftSmallcase>() {
                    @Override
                    public void call(DraftSmallcase draftSmallcase) {
                        presenter.onDraftSmallcaseFetched(draftSmallcase);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedFetchingDraftSmallcase();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getDraftSmallcase()");
                    }
                }));
    }

    @Override
    public void destroySubscriptions() {
        if (compositeSubscription == null) return;

        compositeSubscription.unsubscribe();
    }
}
