package com.smallcase.android.user.coustomize;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.smallcase.android.data.model.DraftSmallcase;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by shashankm on 29/05/17.
 */

public class DraftConverter implements JsonDeserializer<List<DraftSmallcase>> {

    @Override
    public List<DraftSmallcase> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return new Gson().fromJson(json.getAsJsonObject().getAsJsonObject("data").get("draftSmallcases"), typeOfT);
    }
}
