package com.smallcase.android.user.notification;

import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import io.sentry.event.Breadcrumb;
import okhttp3.ResponseBody;
import rx.functions.Action1;

/**
 * Created by shashankm on 13/01/17.
 */

class NotificationsPresenter {
    private static final String TAG = "NotificationsPresenter";
    private final String notificationSetting = "pnlUpdateNotification";

    private UserRepository userRepository;
    private SharedPrefService sharedPrefService;
    private NotificationsContract notificationsContract;

    NotificationsPresenter(UserRepository userRepository, SharedPrefService sharedPrefService, NotificationsContract notificationsContract) {
        this.userRepository = userRepository;
        this.sharedPrefService = sharedPrefService;
        this.notificationsContract = notificationsContract;
    }

    void changeNotificationSubscription(final boolean shouldSubscribe) {
        final String auth = sharedPrefService.getAuthorizationToken();
        String csrf = sharedPrefService.getCsrfToken();

        if (auth == null || csrf == null) return;

        HashMap<String, Object> body = new HashMap<>();
        body.put("setting", notificationSetting);
        body.put("value", shouldSubscribe);
        userRepository.setSettings(auth, body, csrf)
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        try {
                            JSONObject response = new JSONObject(responseBody.string());
                            if (response.getBoolean("success")) {
                                sharedPrefService.updateWeeklyNotificationCache(shouldSubscribe);
                            }
                        } catch (IOException | JSONException e) {
                            SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed parsing notification", null);
                            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; changeNotificationSubscription()");
                            e.printStackTrace();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; changeNotificationSubscription()");
                    }
                });
    }

    void getSettings() {
        final String auth = sharedPrefService.getAuthorizationToken();
        String csrf = sharedPrefService.getCsrfToken();

        if (auth == null || csrf == null) return;

        notificationsContract.onNotificationStatusReceived(sharedPrefService.getWeeklyNotificationPreference());

        userRepository.getSettings(auth, notificationSetting, csrf)
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        if (responseBody != null) {
                            try {
                                JSONObject response = new JSONObject(responseBody.string());
                                boolean isEnabled = response.getJSONObject("data").getBoolean("value");
                                sharedPrefService.cacheWeeklyNotificationPreference(isEnabled);
                            } catch (JSONException | IOException e) {
                                SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed parsing settings", null);
                                SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; getSettings()");
                                e.printStackTrace();
                                notificationsContract.showSnackBar(R.string.something_wrong);
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getSettings()");
                    }
                });
    }
}
