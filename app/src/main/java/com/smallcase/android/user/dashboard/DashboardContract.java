package com.smallcase.android.user.dashboard;

import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.text.SpannableString;

import com.smallcase.android.data.model.Buy;
import com.smallcase.android.data.model.Investments;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.Nifty;
import com.smallcase.android.data.model.ReBalanceUpdate;
import com.smallcase.android.data.model.SipDetails;
import com.smallcase.android.data.model.TotalInvestment;
import com.smallcase.android.view.model.PSmallcaseWithNews;

import java.util.ArrayList;
import java.util.List;


interface DashboardContract {

    interface View {
        void invalidAuthToken();

        void noInvestments();

        void onPAndLReceived(SpannableString currentPAndL);

        void showNoNetworkAvailable();

        void onFailure();

        void showSnackBar(int resId);

        void onWatchlistedSmallcaseReceived(List<PSmallcaseWithNews> smallcaseWithNewses);

        void showNoWatchlist();

        void hideReBalanceUpdatesIfVisible();

        void showReBalanceUpdates(@NonNull List<ReBalanceUpdate> reBalanceUpdates);

        void niftyValueMessage(@ColorRes int redId, String message);

        void showWatchListIfInvisible();

        void hideMarketOpenIfVisible();

        void showMarketOpenReminder(List<Buy> buys);

        void showNoWatchlistNews();

        void toggleNewsSeeAll(int visibility);

        void showWatchingSmallcasesLink();

        void hideWatchingSmallcasesLink();

        void hideSipsIfVisible();

        void showSipsRemaining(List<SipDetails> sips);
    }

    interface Service {
        void getInvestments();

        String getAuthToken();

        Nifty getCachedNifty();

        TotalInvestment getCachedTotalInvestments();

        ArrayList<String> getInvestedScids();

        void getPendingActions();

        void getNiftyData();

        void getTotalInvestments();

        void getSmallcaseNews(int offSet, int count, List<String> scidsList);

        @NonNull List<String> getWatchlist();

        List<News> getCachedWatchlistNews();

        String getName();

        void destroySubscriptions();

        void getUserWatchlist();
    }

    interface Presenter {
        void fetchData(int newsItemOffSet, int count);

        void onInvestmentsReceived(Investments investments);

        void onFailedFetchingInvestments(Throwable throwable);

        void onNiftyReceived(Nifty nifty);

        void onTotalInvestmentsReceived(TotalInvestment totalInvestment);

        void onSeeAllClicked();

        void onFailedFetchingNifty();

        void onFailedFetchingTotalInvestments();

        void onWatchlistNewsReceived(List<News> newses);

        void onFailedFetchingWatchlistNews();

        ArrayList<String> getWatchlist();

        void onFailedFetchingReBalanceUpdates();

        void onReBalanceUpdatesFetched(List<ReBalanceUpdate> reBalanceUpdates);

        String getGreeting();

        void destroySubscriptions();

        void onBuyRemindersFetched(List<Buy> buys);

        void emptyWatchlist();

        void fetchedNewsWatchlist();

        void onFailedFetchingWatchlist();

        void fetchNifty();

        void fetchTotalInvestment();

        void fetchPendingActions();

        void onSipsFetched(List<SipDetails> sips);

        boolean isSmallcaseWatchlisted(String scid);
    }
}
