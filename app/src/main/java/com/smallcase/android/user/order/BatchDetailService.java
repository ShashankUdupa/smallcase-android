package com.smallcase.android.user.order;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;

import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Batch;
import com.smallcase.android.data.model.Order;
import com.smallcase.android.data.model.Orders;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.kite.KiteContract;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.OrderLabel;
import com.smallcase.android.util.OrderStatus;
import com.smallcase.android.view.model.PBatch;
import com.smallcase.android.view.model.POrderStock;
import com.smallcase.android.view.model.Stock;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import io.sentry.event.Breadcrumb;
import retrofit2.adapter.rxjava.HttpException;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 12/04/17.
 */

class BatchDetailService implements BatchDetailContract.Service, StockHelper.StockPriceCallBack, KiteContract.Response {
    private static final String TAG = "BatchDetailService";

    private SharedPrefService sharedPrefService;
    private BatchDetailContract.View view;
    private NetworkHelper networkHelper;
    private MarketRepository marketRepository;
    private StockHelper stockHelper;
    private KiteContract.Request response;
    private List<Stock> stockList;
    private double neededFunds;
    private Activity activity;
    private String scid, batchLabel;
    private UserSmallcaseRepository userSmallcaseRepository;
    private CompositeSubscription compositeSubscription;

    BatchDetailService(Activity activity, SharedPrefService sharedPrefService, BatchDetailContract.View view, NetworkHelper
            networkHelper, MarketRepository marketRepository, KiteContract.Request response, String scid, UserSmallcaseRepository userSmallcaseRepository) {
        this.sharedPrefService = sharedPrefService;
        this.view = view;
        this.networkHelper = networkHelper;
        this.marketRepository = marketRepository;
        this.response = response;
        this.activity = activity;
        this.scid = scid;
        this.userSmallcaseRepository = userSmallcaseRepository;
        stockList = new ArrayList<>();
        stockHelper = new StockHelper();
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getBatchDetails(final String iScid) {
        if (!networkHelper.isNetworkAvailable()) {
            view.showSnackBar(R.string.no_internet);
            return;
        }

        final String auth = sharedPrefService.getAuthorizationToken();
        String csrf = sharedPrefService.getCsrfToken();
        if (null == auth || null == csrf) {
            sharedPrefService.clearSharedPreference();
            view.redirectToLogin();
            return;
        }

        compositeSubscription.add(userSmallcaseRepository.getOrderDetails(auth, iScid, null, csrf, false, false)
                .subscribe(orders -> view.onBatchDetailsReceived(convertToBatchViewModel(orders)), throwable -> {
                    view.onFailedFetchingBatches();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    HashMap<String, String> data = new HashMap<>();
                    data.put("iScid", iScid);
                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "getBatchDetails", data);
                    SentryAnalytics.getInstance().captureEvent(auth, throwable, TAG + "; getBatchDetails()");
                }));
    }

    @Override
    public String getStatusText(String batchStatus) {
        String status;
        switch (batchStatus) {
            case OrderStatus.COMPLETED:
                status = "Filled";
                break;

            case OrderStatus.FIXED:
                status = "Repaired";
                break;

            case OrderStatus.MARKED_COMPLETE:
                status = "Archived";
                break;

            case OrderStatus.PARTIALLY_FILLED:
            case OrderStatus.PARTIALLY_PLACED:
            case OrderStatus.UNFILLED:
                status = "Partial";
                break;

            case OrderStatus.PLACED:
                status = "Placed";
                break;

            default:
                status = "Unknown";
                break;
        }
        return status;
    }

    @Override
    public boolean shouldShowRepairAndArchive(String orderStatus) {
        return OrderStatus.UNPLACED.equals(orderStatus) || OrderStatus.PARTIALLY_FILLED.equals(orderStatus)
                || OrderStatus.UNFILLED.equals(orderStatus);
    }

    @Override
    public Set<String> getExitedIScids() {
        return sharedPrefService.getExitedIScids();
    }

    @Override
    public String getBatchLabel() {
        return batchLabel;
    }

    @Override
    public void cancelBatch(final HashMap<String, String> body) {
        final HashMap<String, String> data = new HashMap<>();
        data.put("batchId", body.get("batchId"));
        data.put("iScid", body.get("iscid"));
        compositeSubscription.add(userSmallcaseRepository.cancelBatch(sharedPrefService.getAuthorizationToken(), body, sharedPrefService.getCsrfToken())
                .subscribe(responseBody -> {
                    try {
                        JSONObject response = new JSONObject(responseBody.string());
                        if (response.getBoolean("success")) {
                            view.onBatchCanceledSuccessfully();
                            HashSet<String> iScids = new HashSet<>(sharedPrefService.getInvestedScidsWithIScids());
                            if (!iScids.contains(scid + "#" + body.get("iscid"))) {
                                iScids.add(scid + "#" + body.get("iscid"));
                                sharedPrefService.saveInvestedScidWithIscid(iScids);
                            }
                            sharedPrefService.saveInvestedScidWithIscid(iScids);
                            return;
                        }
                        view.onFailedCancelingBatch(null);
                    } catch (JSONException | IOException e) {

                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "cancelBatch", data);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; cancelBatch()");

                        e.printStackTrace();
                        view.onFailedCancelingBatch(e);
                    }
                }, throwable -> {
                    view.onFailedCancelingBatch(throwable);
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "cancelBatch", data);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; cancelBatch()");
                }));
    }

    @Override
    public void fixBatch(List<Stock> stocks) {
        stockList.clear();
        stockList.addAll(stocks);
        view.showPlacingOrderLoader();
        if ("leprechaun".equals(sharedPrefService.getUserInfo().get("brokerName"))) {
            getRepairableStocks();
        } else {
            getIfMarketIsOpen();
        }
    }

    private void getRepairableStocks() {
        List<String> sids = new ArrayList<>();
        for (Stock stock : stockList) {
            sids.add(stock.getSid());
        }
        stockHelper.getStockPrices(marketRepository, sharedPrefService, sids, this);
    }

    private List<PBatch> convertToBatchViewModel(List<Order> orders) {
        List<PBatch> batches = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());
        for (Batch batch : orders.get(0).getBatches()) {
            PBatch pBatch = new PBatch();
            Date date = AppUtils.getInstance().getPlainDate(batch.getDate());
            pBatch.setPlacedOn(simpleDateFormat.format(date));
            pBatch.setOrderStatus(batch.getStatus());
            pBatch.setBatchId(batch.getBatchId());

            batchLabel = batch.getLabel();
            String batchType;
            switch (batch.getLabel()) {
                case OrderLabel.BUY:
                    batchType = "Buy";
                    break;

                case OrderLabel.FIX:
                    batchType = "Repair";
                    break;

                case OrderLabel.INVEST_MORE:
                    batchType = "Invest More";
                    break;

                case OrderLabel.MANAGE:
                    batchType = "Manage";
                    break;
                
                case OrderLabel.RE_BALANCE:
                    batchType = "Rebalance";
                    break;

                case OrderLabel.SELL_ALL:
                    batchType = "Exit";
                    break;

                case OrderLabel.SIP:
                    batchType = "SIP";
                    break;

                case OrderLabel.PARTIAL_EXIT:
                    batchType = "Partial Exit";
                    break;

                default:
                    batchType = batch.getLabel();
                    break;
            }
            pBatch.setBatchType(batchType);

            if (batch.getBuyAmount() != 0) {
                pBatch.setBuyValue(String.format(Locale.getDefault(), "%.2f", batch.getBuyAmount()));
            } else {
                pBatch.setBuyValue("");
            }

            if (batch.getSellAmount() != 0) {
                pBatch.setSellValue(String.format(Locale.getDefault(), "%.2f", batch.getSellAmount()));
            } else {
                pBatch.setSellValue("");
            }

            List<POrderStock> orderStockList = new ArrayList<>();
            int filled = 0;
            for (Orders order : batch.getOrders()) {
                filled = addToBatch(orderStockList, filled, order);
            }

            for (Orders order : batch.getUnplaced()) {
                filled = addToBatch(orderStockList, filled, order);
            }

            int remainingOrders = (int) batch.getQuantity() - filled;
            String fillerText;
            switch (batch.getStatus()) {
                case OrderStatus.COMPLETED:
                    fillerText = " filled";
                    break;

                case OrderStatus.MARKED_COMPLETE:
                case OrderStatus.FIXED:
                    fillerText = " filled, " + remainingOrders + " archived";
                    break;

                case OrderStatus.PARTIALLY_FILLED:
                case OrderStatus.PARTIALLY_PLACED:
                case OrderStatus.UNFILLED:
                    fillerText = " filled, " + remainingOrders + " unfilled";
                    break;

                case OrderStatus.PLACED:
                    fillerText = " placed";
                    break;

                default:
                    fillerText = "";
                    break;
            }

            pBatch.setFilledVsQuantity(filled + "/" + (int) batch.getQuantity() + fillerText);
            pBatch.setPercentFilled((int) (((double) filled / batch.getQuantity()) * 100));
            pBatch.setStocks(orderStockList);
            pBatch.setShouldShowOrders(false);
            batches.add(0, pBatch);
        }
        return batches;
    }

    private int addToBatch(List<POrderStock> orderStockList, int filled, Orders order) {
        POrderStock orderStock = new POrderStock();
        orderStock.setType(order.getTransactionType().substring(0, 1) + order.getTransactionType().substring(1).toLowerCase());
        orderStock.setAveragePrice(String.format(Locale.getDefault(), "%.2f", order.getAveragePrice()));
        orderStock.setQuantityFilled((int) order.getFilledQuantity() + "/" + (int) order.getQuantity());
        orderStock.setStockTicker(order.getTradingsymbol());
        orderStock.setRemainingShares(order.getQuantity() - order.getFilledQuantity());
        orderStock.setSid(order.getSid());
        orderStock.setFilled(order.getFilledQuantity() == order.getQuantity());
        orderStock.setExchangeOrderId(order.getExchangeOrderId());
        orderStock.setOrderId(order.getOrderId());
        orderStock.setOrderType(order.getOrderType());
        orderStock.setProductValidity(order.getValidity());
        orderStock.setOrderStatus(order.getStatus());
        orderStock.setOrderType(order.getOrderType());
        orderStock.setStatusMessage(order.getStatusMessage());
        orderStock.setTriggeredPrice(String.format(Locale.getDefault(), "%.2f", order.getTriggeredPrice()));
        if (orderStock.isFilled()) filled++;
        orderStockList.add(orderStock);
        return filled;
    }

    @Override
    public void onStockPricesReceived(HashMap<String, Double> stockMap) {
        neededFunds = 0;
        for (Stock stock : stockList) {
            double price = stockMap.get(stock.getSid());
            stock.setPrice(price);
            if (stock.isBuy()) {
                neededFunds += (price * (stock.getShares() - stock.getFilled()));
            } else {
                neededFunds -= (price * (stock.getShares() - stock.getFilled()));
            }
        }
        Log.d(TAG, "onStockPricesReceived: " + neededFunds);
        response.checkSufficientFunds(neededFunds, this);
    }

    @Override
    public void onFailedFetchingStockPrice(Throwable throwable, @Nullable String sid) {
        view.hidePlacingOrderLoader();
        if (throwable instanceof HttpException && (((HttpException) throwable).code() == 401)) {
            view.sendUserToKiteLogin();
            return;
        }

        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onFailedFetchingFunds(@Nullable Throwable throwable) {
        view.hidePlacingOrderLoader();
        if (throwable instanceof HttpException && (((HttpException) throwable).code() == 401)) {
            view.sendUserToKiteLogin();
            return;
        }

        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void sufficientFundsAvailable() {
        view.sendUserToReviewOrder(stockList, neededFunds);
    }

    @Override
    public void needMoreFunds(double availableFunds) {
        String needed = AppConstants.RUPEE_SYMBOL + AppConstants.getDecimalNumberFormatter().format((neededFunds - availableFunds));
        SpannableString neededFunds = new SpannableString("Oops, looks like you don’t have enough funds. Please add at " +
                "least " + needed + " to continue.");
        neededFunds.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primary_text))
                , 66, (66 + needed.length()), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        view.hidePlacingOrderLoader();
        view.showNeedFunds(neededFunds);
    }

    @Override
    public void addMarketOpenReminder(String scid) {
        HashMap<String, String> body = new HashMap<>();
        body.put("scid", scid);
        body.put("action", "BUY");
        compositeSubscription.add(userSmallcaseRepository.addReminder(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken(), body)
                .subscribe(responseBody -> {

                }, throwable -> {
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; addMarketOpenReminder()");
                }));
    }

    @Override
    public String getKiteId() {
        return sharedPrefService.getKiteId();
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }

    private void getIfMarketIsOpen() {
        compositeSubscription.add(marketRepository.getIfMarketIsOpen(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(responseBody -> {
                    try {
                        JSONObject response = new JSONObject(responseBody.string());
                        if (response.getString("data").equalsIgnoreCase("open")) {
                            getRepairableStocks();
                        } else {
                            view.hidePlacingOrderLoader();
                            view.showMarketClosed();
                        }
                    } catch (JSONException | IOException e) {
                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.DEFAULT, "Parsing failure for market open", null);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; getIfMarketIsOpen()");
                        e.printStackTrace();
                        view.showSnackBar(R.string.something_wrong);
                    }
                }, throwable -> {
                    view.onFailedRepairingOrders(throwable);

                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getIfMarketIsOpen()");
                }));
    }
}
