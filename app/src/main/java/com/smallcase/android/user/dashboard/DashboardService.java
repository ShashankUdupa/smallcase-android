package com.smallcase.android.user.dashboard;


import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.InvestedSmallcases;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.Nifty;
import com.smallcase.android.data.model.ReBalanceUpdate;
import com.smallcase.android.data.model.SipDetails;
import com.smallcase.android.data.model.TotalInvestment;
import com.smallcase.android.data.repository.InvestmentRepository;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import retrofit2.adapter.rxjava.HttpException;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 07/12/16.
 */

class DashboardService implements DashboardContract.Service {
    private static final String TAG = "DashboardService";

    private SharedPrefService sharedPrefService;
    private Gson gson;
    private InvestmentRepository investmentRepository;
    private DashboardContract.Presenter dashboardPresenterContract;
    private SmallcaseRepository smallcaseRepository;
    private MarketRepository marketRepository;
    private UserRepository userRepository;
    private CompositeSubscription compositeSubscription;

    DashboardService(SharedPrefService sharedPrefService, InvestmentRepository investmentRepository, SmallcaseRepository smallcaseRepository,
                     DashboardContract.Presenter dashboardPresenterContract, MarketRepository marketRepository, UserRepository userRepository) {
        this.sharedPrefService = sharedPrefService;
        this.smallcaseRepository = smallcaseRepository;
        this.investmentRepository = investmentRepository;
        this.dashboardPresenterContract = dashboardPresenterContract;
        this.marketRepository = marketRepository;
        this.userRepository = userRepository;
        this.gson = new Gson();
        compositeSubscription = new CompositeSubscription();
    }

    DashboardService(SharedPrefService sharedPrefService) {
        this.sharedPrefService = sharedPrefService;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public String getAuthToken() {
        return sharedPrefService.getAuthorizationToken();
    }

    @Override
    public Nifty getCachedNifty() {
        return gson.fromJson(sharedPrefService.getNifty(), Nifty.class);
    }

    @Override
    public TotalInvestment getCachedTotalInvestments() {
        return gson.fromJson(sharedPrefService.getTotalInvestments(), TotalInvestment.class);
    }

    @Override
    public void getSmallcaseNews(int offSet, int count, List<String> scidsList) {
        compositeSubscription.add(smallcaseRepository.getSmallcaseNews(getAuthToken(), scidsList, offSet, count,
                sharedPrefService.getCsrfToken(), null)
                .subscribe(newses -> {
                    cacheNews(newses);
                    dashboardPresenterContract.onWatchlistNewsReceived(newses);
                }, throwable -> {
                    dashboardPresenterContract.onFailedFetchingWatchlistNews();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getWatchlistedSmallcasesNews()");
                }));
    }

    @NonNull
    @Override
    public List<String> getWatchlist() {
        Set<String> watchlist = sharedPrefService.getCachedWatchlist();
        return new ArrayList<>(watchlist);
    }

    @Override
    public List<News> getCachedWatchlistNews() {
        HashSet<String> smallcasesHash = new HashSet<>(sharedPrefService.getCachedWatchlistNews());
        List<News> watchlistedSmallcase = new ArrayList<>();
        for (String smallcaseString : smallcasesHash) {
            watchlistedSmallcase.add(gson.fromJson(smallcaseString, News.class));
        }
        return watchlistedSmallcase;
    }

    @Override
    public String getName() {
        return sharedPrefService.getUserInfo().get("name");
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }

    @Override
    public void getUserWatchlist() {
        compositeSubscription.add(userRepository.getUserInfo(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(user -> {
                    String[] splitName = user.getBroker().getUserName().trim().split(" ");
                    StringBuilder name = new StringBuilder();
                    for (String split : splitName) {
                        if (split == null || split.trim().equals("")) continue;
                        name.append(split.substring(0, 1).toUpperCase(Locale.getDefault())).append(split.substring(1)
                                .toLowerCase(Locale.getDefault())).append(" ");
                    }
                    sharedPrefService.cacheUserInfo(name.toString(), user.getBroker().getUserId(), user.getBroker().getName());

                    if (user.getSmallcaseWatchlist().isEmpty()) {
                        dashboardPresenterContract.emptyWatchlist();
                        return;
                    }
                    sharedPrefService.cacheWatchlist(user.getSmallcaseWatchlist());
                    dashboardPresenterContract.fetchedNewsWatchlist();
                }, throwable -> {
                    dashboardPresenterContract.onFailedFetchingWatchlist();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getUserWatchlist()");
                }));
    }

    @Override
    public ArrayList<String> getInvestedScids() {
        return new ArrayList<>(sharedPrefService.getInvestedScidsWithIScids());
    }

    @Override
    public void getPendingActions() {
        compositeSubscription.add(userRepository.getPendingActions(getAuthToken(), sharedPrefService.getCsrfToken())
                .subscribe(actions -> {
                    cacheReBalanceUpdates(actions.getReBalanceUpdates());
                    cacheSipUpdates(actions.getSips());

                    dashboardPresenterContract.onReBalanceUpdatesFetched(actions.getReBalanceUpdates());
                    dashboardPresenterContract.onBuyRemindersFetched(actions.getBuys());
                    dashboardPresenterContract.onSipsFetched(actions.getSips());
                }, throwable -> {
                    dashboardPresenterContract.onFailedFetchingReBalanceUpdates();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getBuyReminders()");
                }));
    }

    @Override
    public void getNiftyData() {
        compositeSubscription.add(marketRepository.getNifty(".NSEI", getAuthToken(), sharedPrefService.getCsrfToken())
                .subscribe(nifty -> {
                    cacheNifty(nifty);
                    dashboardPresenterContract.onNiftyReceived(nifty);
                }, throwable -> {
                    dashboardPresenterContract.onFailedFetchingNifty();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getNiftyData()");
                }));
    }

    @Override
    public void getTotalInvestments() {
        compositeSubscription.add(investmentRepository.getTotalInvestments(getAuthToken(), sharedPrefService.getCsrfToken())
                .subscribe(totalInvestment -> {
                    cacheTotalInvestments(totalInvestment);
                    dashboardPresenterContract.onTotalInvestmentsReceived(totalInvestment);
                }, throwable -> {
                    dashboardPresenterContract.onFailedFetchingTotalInvestments();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getTotalInvestments()");
                }));
    }

    @Override
    public void getInvestments() {
        compositeSubscription.add(investmentRepository.getInvestments(getAuthToken(), sharedPrefService.getCsrfToken())
                .subscribe(investments -> {
                    cacheInvestedSmallcases(investments.getInvestedSmallcases());
                    dashboardPresenterContract.onInvestmentsReceived(investments);
                }, throwable -> {
                    dashboardPresenterContract.onFailedFetchingInvestments(throwable);
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getInvestments()");
                }));
    }

    private void cacheInvestedSmallcases(List<InvestedSmallcases> investedSmallcases) {
        HashSet<String> scids = new HashSet<>();
        HashSet<String> investedSmallcaseList = new HashSet<>();
        for (InvestedSmallcases investedSmallcase : investedSmallcases) {
            scids.add(investedSmallcase.getScid() + "#" + investedSmallcase.get_id());
            investedSmallcaseList.add(gson.toJson(investedSmallcase));
        }
        sharedPrefService.saveInvestedScidWithIscid(scids);
        sharedPrefService.cacheInvestedSmallcases(investedSmallcaseList);
    }

    private void cacheNews(List<News> investedSmallcaseWithNews) {
        HashSet<String> smallcasesWithNewsSet = new HashSet<>();
        for (News smallcaseWithNews : investedSmallcaseWithNews) {
            String gsonData = gson.toJson(smallcaseWithNews);
            smallcasesWithNewsSet.add(gsonData);
        }
        sharedPrefService.cacheWatchlistNews(smallcasesWithNewsSet);
    }

    private void cacheTotalInvestments(TotalInvestment totalInvestment) {
        sharedPrefService.cacheTotalInvestments(gson.toJson(totalInvestment));
    }

    private void cacheReBalanceUpdates(List<ReBalanceUpdate> reBalanceUpdates) {
        HashSet<String> reBalanceUpdateList = new HashSet<>();
        for (ReBalanceUpdate reBalanceUpdate : reBalanceUpdates) {
            String gsonData = gson.toJson(reBalanceUpdate);
            reBalanceUpdateList.add(gsonData);
        }
        sharedPrefService.cacheReBalanceUpdates(reBalanceUpdateList);
    }

    private void cacheSipUpdates(List<SipDetails> sips) {
        HashSet<String> sipsSet = new HashSet<>();
        for (SipDetails sip : sips) {
            sipsSet.add(gson.toJson(sip));
        }
        sharedPrefService.cacheSipUpdates(sipsSet);
    }

    private void cacheNifty(Nifty nifty) {
        sharedPrefService.cacheNifty(gson.toJson(nifty));
    }
}
