package com.smallcase.android.user.account;

import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.TextView;

import com.smallcase.android.R;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.GraphikText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AboutActivity extends AppCompatActivity {
    private final String DISCLOSURE = "Investing involves risks and investments may lose value<br>" +
            "Past performance does not guarantee future returns and performance of smallcases are subject to market risks.<br><br>" +
            "<b>RETURN CALCULATION METHODOLOGY</b><br>" +
            "<br>" +
            "smallcase performance does not include transaction fees and other related costs. No actual money was invested or trades were executed while calculating smallcase performances. Returns are based on end of day prices of stocks in a smallcase, provided by exchange approved third party vendors. All smallcases are reviewed and rebalanced as per a schedule depending upon the smallcase. As a result of this review process, some stocks may be added, some may be removed and some may undergo weight changes. smallcase returns reflect all these changes<br>" +
            "<br>" +
            "We assign an index value of 100 at the smallcase inception date. This means that we assume a hypothetical investment of INR 100 in the smallcase at the inception date. Individual stock investments are calculated as per their weights in the smallcase. Thus, a stock having a weight of 10% in the smallcase will receive a hypothetical investment of INR 10 ( 10% * INR 100) on the inception date. After calculating individual stock investments, we calculate the individual number of shares that could be bought using hypothetical investments determined in the last step. If the same stock which received a hypothetical investment of INR 10 has an end of day stock price of INR 2 on the inception date, it will have 5 shares in the smallcase (INR 10/ INR 2). In this way we determine the individual number of shares of each stock in the smallcase, on the inception date. Multiplication of individual number of shares of different stocks in the smallcase with respective end of day share prices on the inception date, results in a total hypothetical investment of INR 100 (index value at the inception date)<br>" +
            "<br>" +
            "Once the individual number of shares are decided for a stock in the smallcase, it remains constant unless changed in a review/rebalance process. Every day index value is calculated by multiplying no of shares of each stock with respective end of day prices of stocks on that particular day. If the index value of the smallcase after 30 days is 110, it means that your hypothetical investment of INR 100 is worth INR 110 now. Thus, smallcase is worth INR 10 more compared to the investment date and has generated a return on 10% (INR 10/ INR 100). smallcase returns for any time period (week/ quarter/ annual) is determined in the same manner by comparing beginning and end of period index values<br>" +
            "<br>" +
            "<b>HISTORICAL TIME PERIODS INCLUDING PRE-INCEPTION</b><br>" +
            "<br>" +
            "Assuming a hypothetical investment of INR 100 at the beginning of the period under focus, we calculate the individual stock investments and number of shares in the same manner as described earlier. This is done, assuming the same weighting scheme as it was on the inception date. Returns are calculated in the similar fashion using index values. For a time period extending before the inception date, changes resulting from review/rebalance process are incorporated only for the period after inception date<br>" +
            "<br>" +
            "<b>GENERAL INVESTMENT DISCLOSURE</b><br>" +
            "<br>" +
            "The content and data available on the smallcase platform, including but not limited to smallcase index value, smallcase return numbers and smallcase rationale are for information and no_watchlist purposes only. Charts and performance numbers are backtested/simulated results calculated via a standard methodology and do not include the impact of transaction fee and other related costs. Data used for calculation of historical returns and other information is provided by exchange approved third party data vendors, and has neither been audited nor validated by smallcase<br>" +
            "<br>" +
            "All information present on smallcase platform is to help investors in their decision making process and shall not be considered as a recommendation or solicitation of an investment or investment strategy. Investors are responsible for their investment decisions and are responsible to validate all the information used to make the investment decision. Investor should understand that his/her investment decision is based on personal investment needs and risk tolerance, and performance information available on smallcase platform is one among many other things that should be considered while making an investment decision. Past performance does not guarantee future returns and performance of smallcases are subject to market risk";

    private final String TERMS = "<b>INTRODUCTION</b><br>" +
            "<br>" +
            "Welcome to smallcase. smallcase is an online platform to invest in thematic portfolios of stocks (or as we call them, smallcases). Your access and use of smallcase are subject to the Terms of Use set forth below.<br>" +
            "<br>" +
            "<b>ACCEPTING TERMS</b><br>" +
            "<br>" +
            "smallcase.com is a product by smallcase Technologies Pvt. Ltd. (called smallcase from here onwards). <br>" +
            "  • In order to use the Services, you must first agree to the Terms. You may not use the Services if you do not accept the Terms. <br>" +
            "  • You accept the Terms by creating an account for Services on any smallcase website.<br>" +
            "<br>" +
            "<b>PROVISION OF THE SERVICES BY SMALLCASE</b><br>" +
            "<br>" +
            "  • smallcase is constantly improving and innovating in order to provide the most effortless experience for its users. You acknowledge and agree that the form and nature of the Services which smallcase provides may change from time to time without prior notice to you. <br>" +
            "  • As part of this continuing innovation, you acknowledge and agree that smallcase may stop (permanently or temporarily) providing the Services (or any features within the Services) to you or to users generally at smallcase's sole discretion, without prior notice to you. You may stop using the Services at any time. You do not need to specifically inform smallcase when you stop using the Services. <br>" +
            "  • You acknowledge and agree that if smallcase disables access to your account, you may be prevented from accessing the Services, your account details or any files or other content which is contained in your account. <br>" +
            "  • You acknowledge and agree that while smallcase may not currently have set a fixed upper limit on the number of transmissions you may send or receive through the Services or on the amount of storage space used for the provision of any Service, such fixed upper limits may be set by smallcase at any time, at smallcase’s discretion.<br>" +
            "<br>" +
            "<b>USE OF THE SERVICES BY YOU</b><br>" +
            "<br>" +
            "  • In order to use smallcase Services, you will need to provide your personal information. You agree that any information you give to smallcase will always be accurate, correct and up to date. <br>" +
            "  • You agree to use the Services only for purposes that are permitted by (a) the Terms and (b) any applicable law, regulation or generally accepted practices or guidelines in the relevant jurisdictions. <br>" +
            "  • You agree not to access (or attempt to access) any of the Services by any means other than through the interface that is provided by smallcase, unless you have been specifically allowed to do so in a separate agreement with smallcase. You specifically agree not to access (or attempt to access) any of the Services through any automated means (including use of scripts or web crawlers) and shall ensure that you comply with the instructions set out in any robots.txt file present on the Services. <br>" +
            "  • You agree that you will not engage in any activity that interferes with or disrupts the Services (or the servers and networks which are connected to the Services). <br>" +
            "  • Unless you have been specifically permitted to do so in a separate agreement with smallcase, you agree that you will not reproduce, duplicate, copy, sell, trade or resell the Services for any purpose. <br>" +
            "  • You agree that you are solely responsible for (and that smallcase has no responsibility to you or to any third party for) any breach of your obligations under the Terms and for the consequences (including any loss or damage which smallcase may suffer) of any such breach.<br>" +
            "<br>" +
            "<b>PRIVACY POLICY</b><br>" +
            "<br>" +
            "For information about smallcase’s data protection practices, please read smallcase’s privacy policy at https://www.smallcase.com/meta/privacy. This policy explains how smallcase treats your personal information, and protects your privacy, when you use the Services.<br>" +
            "<br>" +
            "<b>GENERAL INVESTMENT DISCLOSURE</b><br>" +
            "<br>" +
            "All information present on smallcase platform is to help investors in their decision making process and shall not be considered as a recommendation or solicitation of an investment or investment strategy. Investors are responsible for their investment decisions and are responsible to validated all the information used to make the investment decision. Investor should understand that his/her investment decision is based on personal investment needs and risk tolerance, and performance information available on smallcase platform is one among many other things that should be considered while making an investment decision. Past performance does not guarantee future returns and performance of smallcases are subjected to market risk. <br>" +
            "The contents and data available on smallcase platform, including but not limited to smallcase index value, smallcase return numbers and smallcase rationale are for information and no_watchlist purposes only. Charts and performance numbers are backtested/simulated results calculated via a standard methodology and do not include the impact of transaction fee and other related costs. Data used for calculation of historical returns and other information is provided by exchange approved third party data vendors, and has neither been audited nor validated by smallcase. <br>" +
            "Investors should be aware that system responses, execution price, speed, liquidity, market data, and account access times are affected by many factors, including market volatility, size and type of order, market conditions, system performance, and other factors.<br>" +
            "<br>" +
            "<b>LIMITATION OF LIABILITY</b><br>" +
            "<br>" +
            "Subject to overall provision in paragraph above, you expressly understand and agree that smallcase, its subsidiaries and affiliates, and its licensors shall not be liable to you for: <br>" +
            "  • Any direct, indirect, incidental, special consequential or exemplary damages which may be incurred by you, however caused and under any theory of liability. This shall include, but not be limited to, any loss of profit (whether incurred directly or indirectly), any loss of goodwill or business reputation, any loss of data suffered, cost of procurement of substitute goods or services, or other intangible loss; <br>" +
            "  • Any loss or damage which may be incurred by you, including but not limited to loss or damage as a result of: <br>" +
            "          • Any reliance placed by you on the completeness, accuracy or existence of any advertising, or as a result of any relationship or transaction between you and any advertiser or sponsor whose advertising appears on the services; <br>" +
            "          • Any changes which smallcase may make to the services, or for any permanent or temporary cessation in the provision of the services (or any features within the services); <br>" +
            "          • The deletion of, corruption of, or failure to store, any content and other communications data maintained or transmitted by or through your use of the services; <br>" +
            "          • Your failure to provide smallcase with accurate account information<br>" +
            "<br>" +
            "<b>EXCLUSION OF WARRANTIES</b><br>" +
            "<br>" +
            "  • Nothing in these terms, shall exclude or limit smallcase’s warranty or liability for losses which may not be lawfully excluded or limited by applicable law. Some jurisdictions do not allow the exclusion of certain warranties or conditions or the limitation or exclusion of liability for loss or damage caused by negligence, breach of contract or breach of implied terms, or incidental or consequential damages. Accordingly, only the limitations which are lawful in your jurisdiction will apply to you and our liability will be limited to the maximum extent permitted by law. <br>" +
            "  • You expressly understand and agree that your use of the services is at your sole risk and that the services are provided \"As is\" and “as available.” <br>" +
            "  • In particular, smallcase, its subsidiaries and affiliates, and its licensors do not represent or warrant to you that: <br>" +
            "  - Your use of the services will meet your requirements, <br>" +
            "  - Your use of the services will be uninterrupted, timely, secure or free from error, <br>" +
            "  - Any information obtained by you as a result of your use of the services will be accurate or reliable, and <br>" +
            "  - That defects in the operation or functionality of any software provided to you as part of the services will be corrected. <br>" +
            "  • Any material downloaded or otherwise obtained through the use of the services is done at your own discretion and risk and that you will be solely responsible for any damage to your computer system or other device or loss of data that results from the download of any such material. <br>" +
            "  • No advice or information, whether oral or written, obtained by you from smallcase or through or from the services shall create any warranty not expressly stated in the terms. <br>" +
            "  • smallcase further expressly disclaims all warranties and conditions of any kind, whether express or implied, including, but not limited to the implied warranties and conditions of merchantability, fitness for a particular purpose and non-infringement.<br>" +
            "<br>" +
            "<b>JURISDICTION</b><br>" +
            "<br>" +
            "The terms of this agreement are exclusively based on and subject to Indian law. You hereby consent to the exclusive jurisdiction and venue of courts in Bangalore, Karnataka, India in all disputes arising out of or relating to the use of this website. Use of this website is unauthorized in any jurisdiction that does not give effect to all provisions of these terms and conditions, including without limitation this paragraph.";

    private final String PRIVACY =
            "Privacy matters. Privacy is what allows us to determine who we are and what we do - Edward Snowden<br>" +
            "<br>" +
            "smallcase.com is a product by smallcase Technologies Pvt. Ltd. (called smallcase from here onwards).<br>" +
            "<br>" +
            "<b>USER CONSENT</b><br>" +
            "<br>" +
            "By submitting Personal Information through our Site or Services, you agree to the terms of this Privacy Policy and you expressly consent to the collection, use and disclosure of the Personal Information in accordance with this Privacy Policy.<br>" +
            "<br>" +
            "<b>INFORMATION YOU PROVIDE TO US</b><br>" +
            "<br>" +
            "  • If you provide us feedback or contact us via e-mail, we will collect your name and e-mail address, as well as any other content included in the e-mail, in order to send you a reply.<br>" +
            "  • When you participate in one of our surveys, we may collect additional information.<br>" +
            "  • We may also collect Personal Information on the Site or through the Services when we make clear that are we are collecting it and you voluntarily provide it, for example when you submit an application for employment.<br>" +
            "  • We may collect Personal Information from you, such as your first and last name, phone, e-mail address, age when you create an Account.<br>" +
            "<br>" +
            "<b>INFORMATION COLLECTED VIA TECHNOLOGY</b><br>" +
            "<br>" +
            "  • To make our Site and Services more useful to you, our servers (which may be hosted by a third party service provider) collect information from you, including your browser type, operating system, Internet Protocol (IP) address (a number that is automatically assigned to your computer when you use the Internet, which may vary from session to session), and domain name.<br>" +
            "  • We may use third party service providers to help us analyze certain online activities. For example, these service providers may help us analyze visitor activity on the Site. We may permit these service providers to use cookies and other technologies to perform these services for us.<br>" +
            "  • If you are logged into the Site or Services, the information collected via technology as described in this Section may be associated with your Personal Information and will, accordingly, be treated as Personal Information in accordance with this Privacy Policy.<br>" +
            "<br>" +
            "<b>USE OF PERSONAL INFORMATION</b><br>" +
            "<br>" +
            "In general, Personal Information you submit to us regarding you or your company is used either to respond to requests that you make, or to aid us in serving you better. We use such Personal Information in the following ways:<br>" +
            "  • to facilitate the creation of and secure your Account on our network;<br>" +
            "  • to identify you as a user in our system; <br>" +
            "  • to provide improved administration of our Site and Services;<br>" +
            "  • to provide the Services you request;<br>" +
            "  • to improve the quality of experience when you interact with our Site and Services;<br>" +
            "  • to send you transactional e-mail notifications<br>" +
            "  • to send newsletters, surveys, offers, and other promotional materials related to our Services and for other marketing purposes;<br>" +
            "<br>" +
            "<b>CREATION OF ANONYMOUS INFORMATION</b><br>" +
            "<br>" +
            "We may create Anonymous Information records from Personal Information by excluding information (such as the name) that makes the information personally identifiable to the data subject. We use this Anonymous Information to analyze request and usage patterns so that we may enhance the content of our Services and improve Site navigation. smallcase reserves the right to use Anonymous Information for any purpose and disclose Anonymous Information to third parties at its sole discretion.<br>" +
            "<br>" +
            "<b>THIRD PARTY WEBSITES</b><br>" +
            "<br>" +
            "When you click on a link to any other website or location, you will leave our Site and go to another site and another entity may collect Personal Information or Anonymous Information from you. We have no control over, do not review, and cannot be responsible for, these outside websites or their content. Please be aware that the terms of this Privacy Policy do not apply to these outside websites or content, or to any collection of information after you click on links to such outside websites.<br>" +
            "<br>" +
            "<b>SECURITY OF YOUR INFORMATION</b><br>" +
            "<br>" +
            "smallcase is committed to protecting the security of your Personal Information. We use a variety of industry-standard security technologies and procedures to help protect your Personal Information from unauthorized access, use, or disclosure. No method of transmission over the Internet, or method of electronic storage, is 100% secure, however. Therefore, while smallcase uses reasonable efforts to protect your Personal Information, smallcase cannot guarantee its absolute security.<br>" +
            "<br>" +
            "<b>CONTACT INFORMATION</b><br>" +
            "<br>" +
            "smallcase welcomes your comments or questions regarding this Privacy Policy. Please e-mail us at contact@smallcase.com or contact us at the following address: <br>" +
            "<br>" +
            "Smallcase Technologies Private Limited <br>" +
            "Regus-EGL, 1st Floor, Pine Valley Building <br>" +
            "Embassy Golf Links, Domlur <br>" +
            "Bangalore - 560008 <br>" +
            "Karnataka, India<br>" +
            "<br>" +
            "<b>CHANGES TO THIS PRIVACY POLICY</b><br>" +
            "<br>" +
            "Please note that this Privacy Policy may change from time to time. We will post any Privacy Policy changes on this page and, if the changes are significant, we will provide a more prominent notice (including, for certain services, email notification of Privacy Policy changes).<br>" +
            "<br>" +
            "<b>LAST UPDATED ON 5TH MAY 2016</b>";

    @BindView(R.id.text_about) GraphikText about;
    @BindView(R.id.tool_bar) Toolbar toolbar;
    @BindView(R.id.text_tool_bar_title) GraphikText toolbarTitle;
    @BindView(R.id.version) GraphikText version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        toolbarTitle.setText(getString(R.string.about));

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String versionCode = "Version: " + pInfo.versionName;
            version.setText(versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @OnClick(R.id.disclosures)
    public void showDisclosures() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.disclosures))
                .setMessage(Html.fromHtml(DISCLOSURE))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();

        TextView message = (TextView) dialog.findViewById(android.R.id.message);

        if (message != null) {
            message.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR));
            message.setTextSize(14f);
            float space = AppUtils.getInstance().dpToPx(4);
            message.setLineSpacing(space, 1);
        }
    }

    @OnClick(R.id.tos)
    public void onTosClicked() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.tos))
                .setMessage(Html.fromHtml(TERMS))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();

        TextView message = (TextView) dialog.findViewById(android.R.id.message);

        if (message != null) {
            message.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR));
            message.setTextSize(14f);
            float space = AppUtils.getInstance().dpToPx(4);
            message.setLineSpacing(space, 1);
        }
    }

    @OnClick(R.id.privacy)
    public void onPrivacyClicked() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.privacy))
                .setMessage(Html.fromHtml(PRIVACY))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();

        TextView message = (TextView) dialog.findViewById(android.R.id.message);

        if (message != null) {
            message.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR));
            message.setTextSize(14f);
            float space = AppUtils.getInstance().dpToPx(4);
            message.setLineSpacing(space, 1);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
