package com.smallcase.android.user;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.smallcase.android.data.model.Actions;

import java.lang.reflect.Type;

/**
 * Created by shashankm on 01/05/17.
 */

public class ReBalanceParser implements JsonDeserializer<Actions> {

    @Override
    public Actions deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return new Gson().fromJson(json.getAsJsonObject().getAsJsonObject("data").get("actions"), typeOfT);
    }
}