package com.smallcase.android.user.order;


import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;

import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Batch;
import com.smallcase.android.data.model.Order;
import com.smallcase.android.data.model.Orders;
import com.smallcase.android.data.model.SmallcaseOrder;
import com.smallcase.android.data.model.SmallcaseOrders;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.kite.KiteContract;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.OrderLabel;
import com.smallcase.android.util.OrderStatus;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.view.model.Stock;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by shashankm on 07/03/17.
 */

class ReviewOrderPresenter implements ReviewOrderContract.Presenter, KiteContract.Response {
    private static final String TAG = "ReviewOrderPresenter";
    private static final long ORDER_FETCH_DELAY = 3000;

    private ReviewOrderContract.View view;
    private ReviewOrderContract.Service service;
    private List<Stock> stockList;
    private NetworkHelper networkHelper;
    private double investmentAmount;
    private int numberOfStocks;
    private String iScid, scid, batchId;
    private Activity activity;
    private Handler handler = new Handler();
    private AppConstants.MarketStatus marketStatus = AppConstants.MarketStatus.NOT_FETCHED;
    private String smallcaseSource;
    private String did;
    private Integer version;

    ReviewOrderPresenter(Activity activity, ReviewOrderContract.View view, SharedPrefService sharedPrefService, MarketRepository
            marketRepository, List<Stock> stockList, NetworkHelper networkHelper, String iScid, double investmentAmount, String
            scid, String batchId, String smallcaseSource, String did, Integer version, UserSmallcaseRepository userSmallcaseRepository) {
        this.activity = activity;
        this.view = view;
        this.stockList = new ArrayList<>(stockList);
        this.networkHelper = networkHelper;
        this.investmentAmount = investmentAmount;
        this.iScid = iScid;
        this.scid = scid;
        this.batchId = batchId;
        this.smallcaseSource = smallcaseSource;
        this.did = did;
        this.version = version;
        this.service = new ReviewOrderService(marketRepository, sharedPrefService, this, scid, userSmallcaseRepository);
        if ("leprechaun".equals(sharedPrefService.getUserInfo().get("brokerName"))) {
            marketStatus = AppConstants.MarketStatus.OPEN;
        } else {
            service.getIfMarketIsOpen();
        }
    }

    @Override
    public void onOrdersPlaced(JSONObject response) {
        try {
            if (response.getJSONObject("data").has("iscid")) {
                iScid = response.getJSONObject("data").getString("iscid");
            }
            batchId = response.getJSONObject("data").getString("batchId");
            service.getOrderDetails(iScid, batchId);

            Intent intent = new Intent(AppConstants.PLACED_ORDER_BROADCAST);
            LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
        } catch (JSONException e) {
            SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; onOrdersPlaced()");
            e.printStackTrace();
        }
    }

    @Override
    public void onFailedPlacingOrder(Throwable throwable) {
        view.hidePlacingOrderLoader();

        if (throwable instanceof HttpException) {
            HttpException httpException = ((HttpException) throwable);
            if ((httpException.code() == 401)) {
                view.sendUserToKiteLogin();
                return;
            }
            view.showSnackBar(R.string.something_wrong);
        } else {
            // Panic mode! (Other exceptions like SocketTimeout) don't worry we are working on an alternative
            view.sendUserToPreviousScreenAndReLoad();
        }
    }

    @Override
    public SpannableString getInvestmentAmountText() {
        String investmentString;
        switch (view.getOrderLabel()) {
            case OrderLabel.BUY:
            case OrderLabel.INVEST_MORE:
                investmentString = "You will invest\n";
                break;

            case OrderLabel.SELL_ALL:
            case OrderLabel.PARTIAL_EXIT:
                investmentString = "You will receive\n";
                break;

            case OrderLabel.MANAGE:
            case OrderLabel.FIX:
            case OrderLabel.RE_BALANCE:
                if (investmentAmount > 0) {
                    investmentString = "You will invest\n";
                } else {
                    investmentString = "You will receive\n";
                }
                break;

            default:
                if (investmentAmount > 0) {
                    investmentString = "You will invest\n";
                } else {
                    investmentString = "You will receive\n";
                }
                break;
        }

        return getSpannableString(investmentString);
    }

    @NonNull
    private SpannableString getSpannableString(String investmentString) {
        SpannableString spannableString = new SpannableString(investmentString + AppConstants.RUPEE_SYMBOL +
                AppConstants.getDecimalNumberFormatter().format(Math.abs(investmentAmount)));
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primary_text)), investmentString
                .length(), spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new RelativeSizeSpan(1.25f), investmentString.length(), spannableString.length(),
                Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        return spannableString;
    }

    @Override
    public SpannableString getOrderConfirmationText() {
        String confirmationString = numberOfStocks == 1? "Placing regular IOC orders at market prices for 1 stock" :
                "Placing regular IOC orders at market prices for " + numberOfStocks + " stocks";
        return new SpannableString(confirmationString);
    }

    @Override
    public void setNumberOfStocks(int size) {
        numberOfStocks = size;
    }

    @Override
    public void placeOrder() {
        if (!networkHelper.isNetworkAvailable()) {
            view.showSnackBar(R.string.no_internet);
            return;
        }

        if (investmentAmount == 0) {
            view.showSnackBar(R.string.cannot_be_xero);
            return;
        }

        service.placeOrder(createOrder());
    }

    @Override
    public void onOrderDetailsFetched(List<Order> orders) {
        Order order = orders.get(0);
        Batch batch = order.getBatches().get(0);
        Log.d(TAG, "onOrderDetailsFetched: " + batch.getStatus());
        int filled = 0;
        for (Orders orderBatch : batch.getOrders()) {
            if (orderBatch.getFilledQuantity() == orderBatch.getQuantity()) {
                filled++;
            }
        }

        String filledVsQuantity = String.valueOf(filled) + "/" + String.valueOf(batch.getOrders().size()) +
                (batch.getOrders().size() == 1? " order filled" : " orders filled");
        int percent = (int) (((float) filled / (float) batch.getOrders().size()) * 100);

        String status;
        switch (batch.getStatus()) {
            case OrderStatus.COMPLETED:
                status = "Filled";
                break;

            case OrderStatus.MARKED_COMPLETE:
            case OrderStatus.FIXED:
                status = "Archived";
                break;

            case OrderStatus.PARTIALLY_FILLED:
            case OrderStatus.PARTIALLY_PLACED:
                status = "Partial";
                break;

            case OrderStatus.PLACED:
                status = "Placed";
                break;

            default:
                status = "Placed";
                break;
        }

        String batchType;
        switch (batch.getLabel()) {
            case OrderLabel.BUY:
                batchType = "Buy";
                break;

            case OrderLabel.FIX:
                batchType = "Repair";
                break;

            case OrderLabel.INVEST_MORE:
                batchType = "Invest More";
                break;

            case OrderLabel.MANAGE:
                batchType = "Manage";
                break;

            case OrderLabel.RE_BALANCE:
                batchType = "Rebalance";
                break;

            case OrderLabel.SELL_ALL:
                batchType = "Exit";
                break;

            case OrderLabel.PARTIAL_EXIT:
                batchType = "Partial Exit";
                break;

            default:
                batchType = batch.getLabel();
                break;
        }

        switch (batch.getStatus()) {
            case OrderStatus.COMPLETED:
            case OrderStatus.MARKED_COMPLETE:
            case OrderStatus.FIXED:
                switch (view.getOrderLabel()) {
                    case OrderLabel.BUY:
                        service.saveScid(iScid);
                        break;

                    case OrderLabel.SELL_ALL:
                        service.removeScid(iScid);
                        service.addExited(iScid);
                        break;

                    case OrderLabel.SIP:
                        service.removeSipDue(iScid);
                        break;
                }

                view.allOrdersFilled(batchType, status, filledVsQuantity, order.getSource(), percent);
                break;

            case OrderStatus.UNPLACED:
            case OrderStatus.PARTIALLY_FILLED:
            case OrderStatus.UNFILLED:
                List<Stock> stocks = new ArrayList<>();
                for (Orders stockOrder : batch.getOrders()) {
                    // Return only unfilled stocks
                    if (stockOrder.getQuantity() == stockOrder.getFilledQuantity()) continue;

                    Stock stock = new Stock();
                    stock.setBuy(stockOrder.getTransactionType().equals("BUY"));
                    stock.setPrice(stockOrder.getAveragePrice());
                    stock.setShares(stockOrder.getQuantity());
                    stock.setFilled(stockOrder.getFilledQuantity());
                    stock.setSid(stockOrder.getSid());
                    stock.setStockName(stockOrder.getTradingsymbol());
                    stocks.add(stock);
                }

                double investmentAmount = 0;
                for (Stock stock : stocks) {
                    for (Stock originalStock : stockList) {
                        if (stock.getSid().equals(originalStock.getSid())) {
                            if (originalStock.isBuy()) {
                                investmentAmount += (originalStock.getPrice() * originalStock.getShares());
                            } else {
                                investmentAmount -= (originalStock.getPrice() * originalStock.getShares());
                            }
                            stock.setPrice(originalStock.getPrice());
                            break;
                        }
                    }
                }
                this.investmentAmount = investmentAmount;
                view.showRepairOrders(stocks, status, filledVsQuantity, percent, order.getSource(), batchType);
                break;

            case OrderStatus.ERROR:
            case OrderStatus.PLACED:
            case OrderStatus.PARTIALLY_PLACED:
                view.hidePlacingOrderLoader();
                view.onOrderPlaced(status, filledVsQuantity, percent, order.getSource(), batchType);
                handler.postDelayed(() -> {
                    if (!activity.isFinishing()) {
                        service.getOrderDetails(iScid, batchId);
                    }
                }, ORDER_FETCH_DELAY);
                break;
        }

        if (OrderStatus.COMPLETED.equals(batch.getStatus()) || OrderStatus.PARTIALLY_PLACED.equals(batch.getStatus())) {
            Intent intent = new Intent(AppConstants.FUNDS_REFRESH);
            LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
        }
    }

    @Override
    public void onFailedFetchingOrderDetails() {
        view.hidePlacingOrderLoader();
        view.onOrderPlaced("Unknown", "", 0, SmallcaseSource.PROFESSIONAL, "Unknown");
        if (!activity.isFinishing()) service.getOrderDetails(iScid, batchId);
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onFailedCancelingBatch() {
        view.hidePlacingOrderLoader();
        view.showSnackBar(R.string.failed_archiving);
    }

    @Override
    public void onFailedFixingBatch(Throwable throwable) {
        view.hidePlacingOrderLoader();

        if (throwable instanceof HttpException) {
            HttpException httpException = ((HttpException) throwable);
            if ((httpException.code() == 401)) {
                view.sendUserToKiteLogin();
                return;
            }
            view.showSnackBar(R.string.failed_fixing_batch);
        } else {
            // Panic mode! (Other exceptions like SocketTimeout) don't worry we are working on an alternative
            view.sendUserToPreviousScreenAndReLoad();
        }
    }

    @Override
    public void onBatchCanceledSuccessfully() {
        view.hidePlacingOrderLoader();
        view.showContainerActivity();
    }

    @Override
    public void onBatchFixedSuccessfully(JSONObject response) {
        try {
            batchId = response.getJSONObject("data").getString("batchId");
            service.getOrderDetails(iScid, batchId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void archiveOrder() {
        service.cancelBatch(getBatchBody());
    }

    @NonNull
    private HashMap<String, String> getBatchBody() {
        HashMap<String, String> body = new HashMap<>();
        body.put("batchId", batchId);
        body.put("iscid", iScid);
        return body;
    }


    @Override
    public void fixBatch() {
        HashMap<String, String> body = new HashMap<>();
        body.put("batchId", batchId);
        body.put("iscid", iScid);
        service.fixBatch(body);
    }

    @Override
    public String getArchiveDescription() {
        return "Archiving unfilled orders retains only the filled orders in this batch. The stocks/shares in the unfilled " +
                "orders will not be part of this smallcase anymore" + (OrderLabel.BUY.equalsIgnoreCase(view.getOrderLabel())?
                "\n\nYour smallcase will not reflect the constituents & returns of the original smallcase if you archive the orders. To ensure they match, repair the batch instead" : "");
    }

    @Override
    public String getRepairDescription() {
        return "Repairing this order will place fresh orders for the unfilled orders" +
                (OrderLabel.BUY.equalsIgnoreCase(view.getOrderLabel())?
                "\n\nRepairing this batch ensures your smallcase matches the original smallcase in stock constituents" +
                        " & their weights" : "");
    }

    @Override
    public void onMarketStatusReceived(ResponseBody responseBody) {
        try {
            JSONObject response = new JSONObject(responseBody.string());
            if (response.getString("data").equals("open")) {
                marketStatus = AppConstants.MarketStatus.OPEN;
            } else {
                marketStatus = AppConstants.MarketStatus.CLOSED;
            }
        } catch (JSONException | IOException e) {
            SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; onMarketStatusReceived()");
            e.printStackTrace();
            view.showSnackBar(R.string.something_wrong);
        }
    }

    @Override
    public void onFailedFetchingMarketStatus() {
        view.showSnackBar(R.string.unable_to_fetch_market_status);
    }

    @Override
    public boolean isMarketOpen() {
        return marketStatus == AppConstants.MarketStatus.OPEN;
    }

    @Override
    public String getScid() {
        return scid;
    }

    @Override
    public String getIScid() {
        return iScid;
    }

    @Override
    public void getOrderDetails() {
//        if (isFetchingOrderDetails) return;
//        service.getOrderDetails(iScid, batchId);
    }

    @Override
    public void addReminder() {
        HashMap<String, String> body = new HashMap<>();
        body.put("scid", scid);
        body.put("action", "BUY");
        service.addMarketOpenReminder(body);
    }

    @Override
    public void destroySubscriptions() {
        service.destroySubscriptions();
    }

    @Override
    public double getInvestmentAmount() {
        return investmentAmount;
    }

    private SmallcaseOrder createOrder() {
        SmallcaseOrder smallcaseOrder = new SmallcaseOrder();
        smallcaseOrder.setIscid(iScid);
        smallcaseOrder.setScid(scid);
        smallcaseOrder.setSource((null == smallcaseSource || "".equals(smallcaseSource))? SmallcaseSource.PROFESSIONAL : smallcaseSource);
        smallcaseOrder.setLabel(view.getOrderLabel());
        smallcaseOrder.setDid(did);
        smallcaseOrder.setVersion(version);
        List<SmallcaseOrders> smallcaseOrdersList = new ArrayList<>();
        for (Stock stock : stockList) {
            SmallcaseOrders smallcaseOrders = new SmallcaseOrders();
            smallcaseOrders.setQuantity((int) stock.getShares());
            smallcaseOrders.setSid(stock.getSid());
            smallcaseOrders.setTransactionType(stock.isBuy() ? "BUY" : "SELL");
            smallcaseOrdersList.add(smallcaseOrders);
        }
        smallcaseOrder.setOrders(smallcaseOrdersList);
        return smallcaseOrder;
    }

    @Override
    public void onFailedFetchingFunds(@Nullable Throwable throwable) {
        view.showSnackBar(R.string.could_not_retreive_funds);
    }

    @Override
    public void sufficientFundsAvailable() {
        service.fixBatch(getBatchBody());
    }

    @Override
    public void needMoreFunds(double availableFunds) {
        view.hidePlacingOrderLoader();
        view.showAddFunds();
    }
}
