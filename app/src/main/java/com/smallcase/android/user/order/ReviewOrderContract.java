package com.smallcase.android.user.order;


import android.text.SpannableString;

import com.smallcase.android.data.model.Order;
import com.smallcase.android.data.model.SmallcaseOrder;
import com.smallcase.android.view.model.Stock;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by shashankm on 07/03/17.
 */

interface ReviewOrderContract {
    interface View {

        void showSnackBar(int errId);

        void showMarketClosed();

        void allOrdersFilled(String batchType, String status, String filledVsQuantity, String source, int percent);

        void showRepairOrders(List<Stock> stocks, String status, String filledVsQuantity, int percent, String source, String batchType);

        void showContainerActivity();

        String getOrderLabel();

        void hidePlacingOrderLoader();

        String getSmallcaseName();

        List<Stock> getRepairableStocks();

        void showPlacingOrderLoader();

        void showAddFunds();

        void sendUserToKiteLogin();

        void onOrderPlaced(String status, String filledVsQuantity, int percent, String source, String batchType);

        void sendUserToPreviousScreenAndReLoad();
    }

    interface Presenter {
        void onOrdersPlaced(JSONObject response);

        void onFailedPlacingOrder(Throwable throwable);

        SpannableString getInvestmentAmountText();

        SpannableString getOrderConfirmationText();

        void setNumberOfStocks(int size);

        void placeOrder();

        void onOrderDetailsFetched(List<Order> orders);

        void onFailedFetchingOrderDetails();

        void onFailedCancelingBatch();

        void onFailedFixingBatch(Throwable throwable);

        void onBatchCanceledSuccessfully();

        void onBatchFixedSuccessfully(JSONObject response);

        void archiveOrder();

        void fixBatch();

        String getArchiveDescription();

        String getRepairDescription();

        void onMarketStatusReceived(ResponseBody responseBody);

        void onFailedFetchingMarketStatus();

        boolean isMarketOpen();

        String getScid();

        String getIScid();

        void getOrderDetails();

        void addReminder();

        void destroySubscriptions();

        double getInvestmentAmount();
    }

    interface Service {

        void placeOrder(SmallcaseOrder smallcaseOrder);

        void getOrderDetails(String iScid, String batchId);

        void cancelBatch(HashMap<String, String> body);

        void fixBatch(HashMap<String, String> body);

        void getIfMarketIsOpen();

        void saveScid(String iScid);

        void removeScid(String iScid);

        void addMarketOpenReminder(HashMap<String, String> body);

        void destroySubscriptions();

        void addExited(String iScid);

        void removeSipDue(String iScid);
    }
}
