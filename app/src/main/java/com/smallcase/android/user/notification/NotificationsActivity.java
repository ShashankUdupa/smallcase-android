package com.smallcase.android.user.notification;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Switch;

import com.smallcase.android.R;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.GraphikText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationsActivity extends AppCompatActivity implements NotificationsContract {
    private static final String TAG = NotificationsActivity.class.getSimpleName();
    @BindView(R.id.tool_bar)
    Toolbar toolbar;
    @BindView(R.id.text_tool_bar_title)
    GraphikText toolBarTitle;
    @BindView(R.id.notification_switch)
    Switch notificationToggle;
    @BindView(R.id.activity_settings)
    View settingsContainer;
    @BindView(R.id.notification_container)
    View notificationContainer;
    @BindView(R.id.loading_indicator)
    View loadingIndicator;

    private NotificationsPresenter notificationsPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolBarTitle.setText(getString(R.string.notification));

        notificationsPresenter = new NotificationsPresenter(new UserRepository(), new SharedPrefService(this), this);
        notificationsPresenter.getSettings();
    }

    @OnClick(R.id.back)
    void onBackClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onNotificationStatusReceived(boolean value) {
        loadingIndicator.setVisibility(View.GONE);
        notificationContainer.setVisibility(View.VISIBLE);
        notificationToggle.setChecked(value);

        notificationToggle.setOnCheckedChangeListener((buttonView, isChecked) -> {
            notificationsPresenter.changeNotificationSubscription(isChecked);
        });
    }

    @Override
    public void showSnackBar(int resId) {
        AppUtils.getInstance().showSnackBar(settingsContainer, Snackbar.LENGTH_LONG, resId);
    }
}
