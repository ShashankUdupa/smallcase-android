package com.smallcase.android.user.account;

import android.content.Context;
import android.content.pm.PackageManager;

import com.clevertap.android.sdk.CleverTapAPI;
import com.clevertap.android.sdk.exceptions.CleverTapMetaDataNotFoundException;
import com.clevertap.android.sdk.exceptions.CleverTapPermissionsNotSatisfied;
import com.smallcase.android.analytics.MixPanelAnalytics;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.ExitedSmallcase;
import com.smallcase.android.data.model.InvestedSmallcases;
import com.smallcase.android.data.model.User;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.user.ContainerActivity;
import com.smallcase.android.util.SmallcaseSource;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UserAttributes;
import io.intercom.android.sdk.identity.Registration;
import retrofit2.adapter.rxjava.HttpException;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 26/12/16.
 */

class AccountPresenter implements AccountContract.Presenter {
    private static final String TAG = "AccountPresenter";

    private UserRepository userRepository;
    private AccountContract.View containerContract;
    private SharedPrefService sharedPrefService;
    private CompositeSubscription compositeSubscription;
    private Context context;

    AccountPresenter(UserRepository userRepository, AccountContract.View containerContract, SharedPrefService sharedPrefService, Context context) {
        this.userRepository = userRepository;
        this.containerContract = containerContract;
        this.sharedPrefService = sharedPrefService;
        this.context = context;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void fetchUserDetails() {
        String auth = sharedPrefService.getAuthorizationToken();
        String csrf = sharedPrefService.getCsrfToken();
        if (auth == null || csrf == null) return;

        Map<String, String> userInfo = new HashMap<>(sharedPrefService.getUserInfo());

        if (!userInfo.isEmpty()) {
            containerContract.userData(userInfo.get("name"), userInfo.get("kiteId"));
        }

        compositeSubscription.add(userRepository.getUserInfo(auth, csrf)
                .subscribe(this::sendUserDataIntercom, throwable -> {
                    containerContract.onFailedFetchingUserDetails();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + ": fetchUserDetails()");
                }));
    }

    @Override
    public void updateProfile(HashMap<String, Object> body) {
        compositeSubscription.add(userRepository.updateProfile(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken(), body)
                .subscribe(responseBody -> {

                }, throwable -> {
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + ": fetchUserDetails()");
                }));
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }

    private void sendUserDataIntercom(User user) {
        Registration registration = Registration.create().withUserId(user.get_id());
        Intercom.client().registerIdentifiedUser(registration);

        String[] splitName = user.getBroker().getUserName().trim().split(" ");
        StringBuilder name = new StringBuilder();
        String firstName;
        try {
            firstName = splitName.length > 0? splitName[0].substring(0, 1).toUpperCase() + splitName[0]
                    .substring(1).toLowerCase() : "";
        } catch (IndexOutOfBoundsException e) {
            firstName = splitName[0].substring(0, 1).toUpperCase();
            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; sendUserDataIntercom()");
        }

        for (String split : splitName) {
            if (split.trim().equals("")) continue;
            name.append(split.substring(0, 1).toUpperCase(Locale.getDefault())).append(split.substring(1)
                    .toLowerCase(Locale.getDefault())).append(" ");
        }

        UserAttributes userAttributes =
                new UserAttributes.Builder()
                        .withName(firstName)
                        .withUserId(user.get_id())
                        .withEmail(user.getMeta().getProfile().getEmail())
                        .withCustomAttribute("fullName", name.toString())
                        .withCustomAttribute("created_at", user.getMeta().getDateCreated())
                        .withCustomAttribute("investedSmallcasesCount", user.getInvestedSmallcases().size())
                        .withCustomAttribute("investedSmallcasesNames", getInvestedSmallcases(user.getInvestedSmallcases()))
                        .withCustomAttribute("exitedSmallcasesCount", user.getExitedSmallcases().size())
                        .withCustomAttribute("exitedSmallcasesNames", getExitedSmallcases(user.getExitedSmallcases()))
                        .withCustomAttribute("brokerId", user.getBroker().getUserId())
                        .withCustomAttribute("accountType", user.getBroker().getName())
                        .withCustomAttribute("createdAndCustomizedCount", getCreatedAndCustomizedCount(user.getInvestedSmallcases(),
                                user.getExitedSmallcases()))
                        .withCustomAttribute("draftsCount", user.getDraftSmallcases() != null? user.getDraftSmallcases().size() : 0)
                        .withCustomAttribute("watchlistsCount", user.getSmallcaseWatchlist() != null? user.getSmallcaseWatchlist().size() : 0)
                        .withCustomAttribute("watchlistedSmallcasesNames", getWatchlistedSmallcases(user.getSmallcaseWatchlist()))
                        .withCustomAttribute("oldExperience", user.getMeta().getDna() != null? user.getMeta().getDna().getQ1() : "")
                        .withCustomAttribute("experience", user.getMeta().getDna() != null? user.getMeta().getDna().getQ2() : "")
                        .withCustomAttribute("phone", user.getMeta().getProfile().getPhone()).build();

        MixPanelAnalytics mixPanelAnalytics = new MixPanelAnalytics(context.getApplicationContext());
        mixPanelAnalytics.identify(user.get_id());
        mixPanelAnalytics.getPeople().identify(user.get_id());
        mixPanelAnalytics.getPeople().set("$name", name.toString());
        mixPanelAnalytics.getPeople().set("$email", user.getMeta().getProfile().getEmail() == null? "" : user.getMeta().getProfile().getEmail());
        mixPanelAnalytics.getPeople().set("brokerId", user.getBroker().getUserId());
        mixPanelAnalytics.getPeople().set("accountType", user.getBroker().getName());
        mixPanelAnalytics.getPeople().set("phone", user.getMeta().getProfile().getPhone());
        mixPanelAnalytics.getPeople().set("investedSmallcasesCount", user.getInvestedSmallcases().size());
        mixPanelAnalytics.getPeople().set("exitedSmallcasesCount", user.getExitedSmallcases().size());
        mixPanelAnalytics.getPeople().set("investedSmallcasesNames", getInvestedSmallcases(user.getInvestedSmallcases()));
        mixPanelAnalytics.getPeople().set("watchlistsCount", user.getSmallcaseWatchlist() != null? user.getSmallcaseWatchlist().size() : 0);
        mixPanelAnalytics.getPeople().set("createdAt", user.getMeta().getDateCreated());
        mixPanelAnalytics.getPeople().set("createdAndCustomizedCount", getCreatedAndCustomizedCount(user.getInvestedSmallcases(),
                user.getExitedSmallcases()));
        mixPanelAnalytics.getPeople().set("draftsCount", user.getDraftSmallcases() != null? user.getDraftSmallcases().size() : 0);
        mixPanelAnalytics.getPeople().set("watchlistedSmallcasesNames", getWatchlistedSmallcases(user.getSmallcaseWatchlist()));
        mixPanelAnalytics.getPeople().set("exitedSmallcasesNames", getExitedSmallcases(user.getExitedSmallcases()));
        if (user.getMeta().getDna() != null) {
            mixPanelAnalytics.getPeople().set("oldExperience", user.getMeta().getDna().getQ1());
            mixPanelAnalytics.getPeople().set("experience", user.getMeta().getDna().getQ2());
        }
        try {
            mixPanelAnalytics.getPeople().set("androidAppVersion", context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Intercom.client().updateUser(userAttributes);

        HashMap<String, Object> profileUpdate = new HashMap<>();
        profileUpdate.put("Name", firstName);
        profileUpdate.put("Identity", user.get_id());
        profileUpdate.put("fullName", name.toString());
        profileUpdate.put("Email", user.getMeta().getProfile().getEmail() == null? "" : user.getMeta().getProfile().getEmail());
        profileUpdate.put("Phone", user.getMeta().getProfile().getPhone());
        profileUpdate.put("brokerId", user.getBroker().getUserId());
        profileUpdate.put("accountType", user.getBroker().getName());
        profileUpdate.put("investedSmallcasesCount", user.getInvestedSmallcases().size());
        profileUpdate.put("exitedSmallcasesCount", user.getExitedSmallcases().size());
        profileUpdate.put("investedSmallcasesNames", getInvestedSmallcases(user.getInvestedSmallcases()));
        profileUpdate.put("watchlistsCount", user.getSmallcaseWatchlist() != null? user.getSmallcaseWatchlist().size() : 0);
        profileUpdate.put("createdAt", user.getMeta().getDateCreated());
        profileUpdate.put("createdAndCustomizedCount", getCreatedAndCustomizedCount(user.getInvestedSmallcases(),
                user.getExitedSmallcases()));
        profileUpdate.put("draftsCount", user.getDraftSmallcases() != null? user.getDraftSmallcases().size() : 0);
        profileUpdate.put("watchlistedSmallcasesNames", getWatchlistedSmallcases(user.getSmallcaseWatchlist()));
        profileUpdate.put("exitedSmallcasesNames", getExitedSmallcases(user.getExitedSmallcases()));
        if (user.getMeta().getDna() != null) {
            profileUpdate.put("oldExperience", user.getMeta().getDna().getQ1());
            profileUpdate.put("experience", user.getMeta().getDna().getQ2());
        }
        try {
            CleverTapAPI.getInstance(context.getApplicationContext()).onUserLogin(profileUpdate);
        } catch (CleverTapMetaDataNotFoundException | CleverTapPermissionsNotSatisfied e) {
            SentryAnalytics.getInstance().captureEvent(null, e, "Failed to get cleaver tap instance");
        }

        containerContract.userData(name.toString(), user.getBroker().getUserId());

        if ((user.getMeta().getDna() == null || user.getMeta().getDna().getQ2() == null) && context != null) {
            ((ContainerActivity) context).showExperiencePopUp();
        }

        sharedPrefService.userViewedIndexTutorial(user.getFlags().getMarkers().isMobileIndexValue());
        sharedPrefService.userViewedWatchlistTutorial(user.getFlags().getMarkers().isMobileWatchlist());
        sharedPrefService.userViewedSmallcaseStocks(user.getFlags().getMarkers().isMobileStockInfo());
    }

    private int getCreatedAndCustomizedCount(List<InvestedSmallcases> investedSmallcases, List<ExitedSmallcase> exitedSmallcases) {
        int count = 0;
        for (InvestedSmallcases investedSmallcase : investedSmallcases) {
            if (SmallcaseSource.CUSTOM.equals(investedSmallcase.getSource()) || SmallcaseSource.CREATED
                    .equals(investedSmallcase.getSource())) count++;
        }

        for (ExitedSmallcase exitedSmallcase : exitedSmallcases) {
            if (SmallcaseSource.CREATED.equals(exitedSmallcase.getSource()) || SmallcaseSource.CUSTOM
                    .equals(exitedSmallcase.getSource())) count++;
        }

        return count;
    }

    private String getWatchlistedSmallcases(List<String> smallcaseWatchlist) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String exitedSmallcase : smallcaseWatchlist) {
            stringBuilder.append(exitedSmallcase).append(",");
        }
        return stringBuilder.toString();
    }

    private String getExitedSmallcases(List<ExitedSmallcase> exitedSmallcases) {
        StringBuilder stringBuilder = new StringBuilder();
        for (ExitedSmallcase exitedSmallcase : exitedSmallcases) {
            stringBuilder.append(exitedSmallcase.getScid()).append(",");
        }
        return stringBuilder.toString();
    }

    private String getInvestedSmallcases(List<InvestedSmallcases> investedSmallcases) {
        StringBuilder stringBuilder = new StringBuilder();
        for (InvestedSmallcases investedSmallcase : investedSmallcases) {
            stringBuilder.append(investedSmallcase.getScid()).append(",");
        }
        return stringBuilder.toString();
    }
}
