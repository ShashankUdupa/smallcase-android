package com.smallcase.android.user.order;

import android.support.annotation.Nullable;
import android.text.SpannableString;

import com.smallcase.android.view.model.PBatch;
import com.smallcase.android.view.model.Stock;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by shashankm on 12/04/17.
 */

interface BatchDetailContract {
    interface View {

        void showSnackBar(int resId);

        void onBatchDetailsReceived(List<PBatch> pBatches);

        void onFailedFetchingBatches();

        void onBatchCanceledSuccessfully();

        void onFailedCancelingBatch(@Nullable Throwable throwable);

        void showPlacingOrderLoader();

        void hidePlacingOrderLoader();

        void sendUserToReviewOrder(List<Stock> stockList, double investmentAmount);

        void sendUserToKiteLogin();

        void showNeedFunds(SpannableString requiredFunds);

        void showMarketClosed();

        void redirectToLogin();

        void onFailedRepairingOrders(Throwable throwable);
    }

    interface Service {
        void getBatchDetails(String iScid);

        String getStatusText(String batchStatus);

        boolean shouldShowRepairAndArchive(String orderStatus);

        void cancelBatch(final HashMap<String, String> body);

        void fixBatch(List<Stock> stocks);

        void addMarketOpenReminder(String scid);

        String getKiteId();

        void destroySubscriptions();

        Set<String> getExitedIScids();

        String getBatchLabel();
    }
}
