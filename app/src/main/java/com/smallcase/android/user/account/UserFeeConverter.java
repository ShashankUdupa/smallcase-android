package com.smallcase.android.user.account;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.smallcase.android.data.model.SmallcaseLedger;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by shashankm on 11/04/17.
 */

public class UserFeeConverter implements JsonDeserializer<List<SmallcaseLedger>> {

    @Override
    public List<SmallcaseLedger> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return new Gson().fromJson(json.getAsJsonObject().getAsJsonObject("data").getAsJsonArray("smallcaseLedger"), typeOfT);
    }
}