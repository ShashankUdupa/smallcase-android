package com.smallcase.android.user;

import android.support.annotation.Nullable;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.ExitedSmallcase;
import com.smallcase.android.data.model.User;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by shashankm on 16/06/17.
 */

public class UserService {
    private static final String TAG = "UserService";

    public interface UpdateStatusCallBack {

        void onUpdateStatusReceived(String updateStatus);

        void onFailedFetchingUpdateStatus();
    }

    public interface UserDataCallback {
        void onUserDataReceived(User user);

        void onFailedFetchingUserdata();
    }

    public void getUserData(UserRepository userRepository, final SharedPrefService sharedPrefService, @Nullable final
    UserDataCallback userDataCallback) {
        userRepository.getUserInfo(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(user -> {
                    StringBuilder name = new StringBuilder();
                    if (user.getBroker().getUserName() != null) {
                        String[] splitName = user.getBroker().getUserName().trim().split(" ");

                        for (String split : splitName) {
                            if (split.trim().equals("")) continue;
                            name.append(split.substring(0, 1).toUpperCase(Locale.getDefault())).append(split.substring(1)
                                    .toLowerCase(Locale.getDefault())).append(" ");
                        }
                    } else {
                        name.append("");
                    }

                    Set<String> exitedIScids = new HashSet<>();
                    for (ExitedSmallcase exitedSmallcase : user.getExitedSmallcases()) {
                        exitedIScids.add(exitedSmallcase.get_id());
                    }

                    sharedPrefService.cacheWatchlist(user.getSmallcaseWatchlist());
                    sharedPrefService.userViewedIndexTutorial(user.getFlags().getMarkers().isMobileIndexValue());
                    sharedPrefService.userViewedWatchlistTutorial(user.getFlags().getMarkers().isMobileWatchlist());
                    sharedPrefService.userViewedSmallcaseStocks(user.getFlags().getMarkers().isMobileStockInfo());
                    sharedPrefService.cacheUserInfo(name.toString(), user.getBroker().getUserId(), user.getBroker().getName());
                    sharedPrefService.cacheExitedIScids(exitedIScids);

                    if (userDataCallback != null) userDataCallback.onUserDataReceived(user);
                }, throwable -> {
                    if (userDataCallback != null) userDataCallback.onFailedFetchingUserdata();

                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getUserData()");
                });
    }

    void getUpdateStatus(UserRepository userRepository, final SharedPrefService sharedPrefService,
                         int versionCode, final UpdateStatusCallBack updateStatusCallBack) {
        userRepository.getUpdateStatus("smallcase", versionCode, "android")
                .subscribe(responseBody -> {
                    try {
                        JSONObject response = new JSONObject(responseBody.string());
                        updateStatusCallBack.onUpdateStatusReceived(response.getString("data"));
                    } catch (JSONException | IOException e) {
                        updateStatusCallBack.onFailedFetchingUpdateStatus();
                        e.printStackTrace();
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; getUpdateStatus()");
                    }
                }, throwable -> {
                    updateStatusCallBack.onFailedFetchingUpdateStatus();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getUpdateStatus()");
                });
    }

    void setFlag(UserRepository userRepository, final SharedPrefService sharedPrefService, HashMap<String, Object> body) {
        userRepository.setFlag(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), body)
                .subscribe(responseBody -> {

                }, throwable -> SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; setFlag()"));
    }
}
