package com.smallcase.android.user;

import com.smallcase.android.view.model.PExpandedSmallcase;

import java.util.List;

/**
 * Created by shashankm on 11/04/17.
 */

interface WatchlistContract {
    interface View {

        void showSmallcases(List<PExpandedSmallcase> smallcaseList);

        void noWatchlist();

        void redirectToLogin();
    }

    interface Service {

        void getWatchlistSmallcases();

        void destroySubscriptions();
    }
}
