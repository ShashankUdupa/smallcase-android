package com.smallcase.android.user.coustomize;

import android.support.annotation.StringRes;

import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.view.model.Stock;

import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by shashankm on 29/05/17.
 */

interface DraftContract {
    interface View {

        void showSnackBar(@StringRes int resId);

        void showNoDraftsAvailable();

        void onDraftsReceived();

        void marketIsOpen();
    }

    interface Presenter {

        void getDraftSmallcases();

        void onDraftSmallcasesFetched(List<DraftSmallcase> draftSmallcases);

        void onFailedFetchingDraftSmallcases();

        List<DraftSmallcase> getDraftSmallcasesList();

        void createStocksList(DraftSmallcase draftSmallcase);

        void checkMarketStatus();

        List<Stock> getStocksList();

        DraftSmallcase getSelectedSmallcase();

        void onMarketStatusReceived(ResponseBody responseBody);

        void onFailedFetchingMarketStatus();

        void getDraftSmallcase(String did);

        void onDraftSmallcaseFetched(DraftSmallcase draftSmallcase);

        void onFailedFetchingDraftSmallcase();

        boolean isDraftShareBased();
    }

    interface Service {

        void getDraftSmallcases();

        void destroySubscriptions();

        void getIfMarketIsOpen();

        void getDraftSmallcase(String did);
    }
}
