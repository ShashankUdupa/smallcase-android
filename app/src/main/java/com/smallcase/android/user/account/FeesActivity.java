package com.smallcase.android.user.account;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PSmallcaseFees;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeesActivity extends AppCompatActivity implements FeeContract.View {
    private static final String FEES_URL = "http://help.smallcase.com/fees-on-smallcase";

    @BindView(R.id.fee_list) RecyclerView feeList;
    @BindView(R.id.text_tool_bar_title) GraphikText toolbarTitle;
    @BindView(R.id.parent) View parent;
    @BindView(R.id.no_smallcases) GraphikText noSmallcases;
    @BindView(R.id.ghost_icon) View ghostIcon;
    @BindView(R.id.fee_charged_text) View feesChargedText;
    @BindView(R.id.fees_card) View feesCard;

    private List<PSmallcaseFees> feesList;
    private AnalyticsContract analyticsContract;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fees);

        ButterKnife.bind(this);
        toolbarTitle.setText("Fees");

        feeList.setLayoutManager(new LinearLayoutManager(this));
        feeList.setHasFixedSize(true);

        analyticsContract = new AnalyticsManager(getApplicationContext());
        if (!new NetworkHelper(this).isNetworkAvailable()) {
            AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_LONG, R.string.no_internet);
            return;
        }


        new FeeService(new UserRepository(), new SharedPrefService(this), this).getUserFees();
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onFeesFetched(List<PSmallcaseFees> feesList) {
        this.feesList = new ArrayList<>(feesList);
        feeList.setAdapter(new FeeAdapter());
    }

    @Override
    public void onFailedFetchingFees() {

    }

    @Override
    public void noFees() {
        SpannableString spannableString = new SpannableString("Fees for buying smallcases\nare updated here at the end of the day\nLearn more here");
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.blue_800)), spannableString.length()
                - 14, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Map<String, Object> metaData = new HashMap<>();
                metaData.put("accessedFrom", "Fees");
                analyticsContract.sendEvent(metaData, "Clicked FAQs", Analytics.INTERCOM);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(FEES_URL)));
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        spannableString.setSpan(clickableSpan, spannableString.length() - 15, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        noSmallcases.setText(spannableString);
        noSmallcases.setMovementMethod(LinkMovementMethod.getInstance());

        noSmallcases.setVisibility(View.VISIBLE);
        ghostIcon.setVisibility(View.VISIBLE);
        feesChargedText.setVisibility(View.INVISIBLE);
        feesCard.setVisibility(View.GONE);
    }

    class FeeAdapter extends RecyclerView.Adapter<FeeAdapter.ViewHolder> {

        @Override
        public FeeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.fee_card,
                    parent, false));
        }

        @Override
        public void onBindViewHolder(FeeAdapter.ViewHolder holder, int position) {
            final PSmallcaseFees smallcaseFees = feesList.get(position);
            holder.smallcaseName.setText(smallcaseFees.getSmallcaseName());
            holder.boughtOn.setText(smallcaseFees.getBroughtOn());
            holder.feeAmount.setText(smallcaseFees.getFeesPaid());

            if (position < feesList.size() - 1) {
                holder.divider.setVisibility(View.VISIBLE);
            } else {
                holder.divider.setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
            return feesList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.smallcase_name) GraphikText smallcaseName;
            @BindView(R.id.bought_on) GraphikText boughtOn;
            @BindView(R.id.fee_amount) GraphikText feeAmount;
            @BindView(R.id.divider) View divider;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
