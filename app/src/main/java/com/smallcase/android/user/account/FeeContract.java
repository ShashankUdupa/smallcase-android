package com.smallcase.android.user.account;

import com.smallcase.android.view.model.PSmallcaseFees;

import java.util.List;

/**
 * Created by shashankm on 11/04/17.
 */

interface FeeContract {
    interface View {
        void onFeesFetched(List<PSmallcaseFees> feesList);

        void onFailedFetchingFees();

        void noFees();
    }

    interface Service {
        void getUserFees();
    }
}
