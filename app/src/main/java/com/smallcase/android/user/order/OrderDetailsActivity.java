package com.smallcase.android.user.order;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PBatch;
import com.smallcase.android.view.model.POrder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderDetailsActivity extends AppCompatActivity implements OrderDetailsContract.View, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = "OrderDetailsActivity";
    @BindView(R.id.text_tool_bar_title)
    GraphikText toolBarTitle;
    @BindView(R.id.orders_list)
    RecyclerView ordersList;
    @BindView(R.id.loading_indicator)
    View loadingIndicator;
    @BindView(R.id.empty_state_container)
    View noOrdersContainer;
    @BindView(R.id.text_empty_state)
    GraphikText emptyStateText;
    @BindView(R.id.parent)
    View parent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private List<POrder> orders;
    private OrderDetailsContract.Presenter presenter;
    private AnalyticsContract analyticsContract;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        ButterKnife.bind(this);

        toolBarTitle.setText("Orders");

        ordersList.setLayoutManager(new LinearLayoutManager(this));
        ordersList.setHasFixedSize(true);

        swipeRefreshLayout.setColorSchemeResources(R.color.blue_800);
        swipeRefreshLayout.setOnRefreshListener(this);

        analyticsContract = new AnalyticsManager(getApplicationContext());
        analyticsContract.sendEvent(null, "Viewed All Orders", Analytics.MIXPANEL, Analytics.CLEVERTAP);
        presenter = new OrderDetailsPresenter(new NetworkHelper(this), this, new SharedPrefService(this),
                new UserSmallcaseRepository());

        presenter.getOrderDetails();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        presenter.destroySubscriptions();
        super.onDestroy();
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    public void showSnackBar(int errResId) {
        AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_LONG, errResId);
    }

    @Override
    public void showNoOrders() {
        noOrdersContainer.setVisibility(View.VISIBLE);
        emptyStateText.setText("Order details & history for your smallcases will show up here");

        swipeRefreshLayout.setVisibility(View.GONE);
        loadingIndicator.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onOrdersReceived(List<POrder> orderList) {
        swipeRefreshLayout.setRefreshing(false);
        loadingIndicator.setVisibility(View.GONE);
        orders = new ArrayList<>(orderList);
        ordersList.setAdapter(new OrdersAdapter());
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        presenter.getOrderDetails();
    }

    class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

        @Override
        public OrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.order_card, parent, false));
        }

        @Override
        public void onBindViewHolder(OrdersAdapter.ViewHolder holder, int position) {
            final POrder order = orders.get(position);
            PBatch batch = order.getBatches().get(0);
            Glide.with(OrderDetailsActivity.this)
                    .load(order.getSmallcaseImage())
                    .into(holder.smallcaseImage);
            holder.smallcaseName.setText(order.getSmallcaseName());
            holder.batchType.setText(batch.getBatchType());
            holder.placedOn.setText(batch.getPlacedOn());
            holder.status.setText(batch.getOrderStatus());
            holder.filledQuantity.setText(batch.getFilledVsQuantity());
            holder.percent.setProgress(batch.getPercentFilled());

            if (SmallcaseSource.CUSTOM.equals(order.getSource())) {
                holder.customTag.setVisibility(View.VISIBLE);
            } else {
                holder.customTag.setVisibility(View.GONE);
            }

            int progressColor;
            if (batch.getPercentFilled() <= 50) {
                progressColor = R.drawable.progress_red;
            } else if (batch.getPercentFilled() == 100) {
                progressColor = R.drawable.progress_green;
            } else {
                progressColor = R.drawable.progress_purple;
            }

            holder.percent.setProgressDrawable(ContextCompat.getDrawable(OrderDetailsActivity.this, progressColor));

            holder.seeAllOrders.setOnClickListener(v -> sendUserToAllBatchesOfSmallcase(order));

            if ("Partial".equalsIgnoreCase(batch.getOrderStatus())) {
                // Show repair order layout
                holder.repairDivider.setVisibility(View.VISIBLE);
                holder.repairArchiveStocksLayout.setVisibility(View.VISIBLE);
                holder.repairArchiveStocksLayout.setOnClickListener(v -> sendUserToAllBatchesOfSmallcase(order));
            } else {
                holder.repairDivider.setVisibility(View.GONE);
                holder.repairArchiveStocksLayout.setVisibility(View.GONE);
            }
        }

        private void sendUserToAllBatchesOfSmallcase(POrder order) {
            Intent intent = new Intent(OrderDetailsActivity.this, BatchDetailsActivity.class);
            intent.putExtra(BatchDetailsActivity.SMALLCASE_NAME, order.getSmallcaseName());
            intent.putExtra(BatchDetailsActivity.ISCID, order.getiScid());
            intent.putExtra(BatchDetailsActivity.SCID, order.getScid());
            intent.putExtra(BatchDetailsActivity.SOURCE, order.getSource());
            startActivity(intent);
        }

        @Override
        public int getItemCount() {
            return orders.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.smallcase_image)
            ImageView smallcaseImage;
            @BindView(R.id.smallcase_name)
            GraphikText smallcaseName;
            @BindView(R.id.batch_type)
            GraphikText batchType;
            @BindView(R.id.placed_on)
            GraphikText placedOn;
            @BindView(R.id.status)
            GraphikText status;
            @BindView(R.id.filled_quantity)
            GraphikText filledQuantity;
            @BindView(R.id.percent)
            ProgressBar percent;
            @BindView(R.id.order_card)
            View orderCard;
            @BindView(R.id.see_all_orders)
            View seeAllOrders;
            @BindView(R.id.repair_batch_text)
            GraphikText repairBatch;
            @BindView(R.id.repair_divider)
            View repairDivider;
            @BindView(R.id.repair_archive_stocks_layout)
            View repairArchiveStocksLayout;
            @BindView(R.id.custom_tag)
            View customTag;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);

                String notFilledString = "Latest batch is not filled. ";
                SpannableString spannableString = new SpannableString(notFilledString + "Repair/Archive");
                spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(OrderDetailsActivity.this, R.color.blue_800)),
                        notFilledString.length(), spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                repairBatch.setText(spannableString);
            }
        }
    }
}
