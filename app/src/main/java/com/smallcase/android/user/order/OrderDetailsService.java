package com.smallcase.android.user.order;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Order;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;

import java.util.List;

import io.intercom.retrofit2.HttpException;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 07/04/17.
 */

class OrderDetailsService implements OrderDetailsContract.Service {
    private static final String TAG = "OrderDetailsService";

    private SharedPrefService sharedPrefService;
    private OrderDetailsContract.Presenter presenter;
    private UserSmallcaseRepository userSmallcaseRepository;
    private CompositeSubscription compositeSubscription;

    OrderDetailsService(SharedPrefService sharedPrefService, OrderDetailsContract.Presenter presenter, UserSmallcaseRepository
            userSmallcaseRepository) {
        this.sharedPrefService = sharedPrefService;
        this.presenter = presenter;
        this.userSmallcaseRepository = userSmallcaseRepository;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getOrderDetails() {
        compositeSubscription.add(userSmallcaseRepository.getOrderDetails(sharedPrefService.getAuthorizationToken(), null, null, sharedPrefService.getCsrfToken(), true, true)
                .subscribe(new Action1<List<Order>>() {
                    @Override
                    public void call(List<Order> orders) {
                        presenter.onOrderDetailsFetched(orders);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedFetchingOrderDetails();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getOrderDetails()");
                    }
                }));
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }
}
