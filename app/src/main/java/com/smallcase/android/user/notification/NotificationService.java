package com.smallcase.android.user.notification;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Notification;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import io.sentry.event.Breadcrumb;
import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 29/12/16.
 */

public class NotificationService implements NotificationContract.Service {
    private static final String TAG = "NotificationService";
    private UserRepository userRepository;
    private SharedPrefService sharedPrefService;
    private NotificationContract.View view;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    public NotificationService(UserRepository userRepository, SharedPrefService sharedPrefService, NotificationContract.View view) {
        this.userRepository = userRepository;
        this.sharedPrefService = sharedPrefService;
        this.view = view;
    }

    public void registerDevice() {
        String deviceId = FirebaseInstanceId.getInstance().getToken();
        String auth = sharedPrefService.getAuthorizationToken();
        String csrf = sharedPrefService.getCsrfToken();
        HashMap<String, String> body = new HashMap<>();
        body.put("deviceId", deviceId);
        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Device id", body);
        compositeSubscription.add(userRepository
                .registerDeviceForNotification(auth, body, csrf)
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        if (responseBody != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(responseBody.string());
                                if (jsonObject.getBoolean("success")) {
                                    sharedPrefService.deviceRegisteredForNotification();
                                    return;
                                }
                                sharedPrefService.registrationNotComplete();
                            } catch (JSONException | IOException e) {
                                SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed parsing register device", null);
                                SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; registerDevice()");
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        sharedPrefService.registrationNotComplete();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; registerDevice()");
                    }
                }));
    }

    public void deRegisterDevice(String auth, String csrf) {
        String deviceId = FirebaseInstanceId.getInstance().getToken();
        HashMap<String, String> body = new HashMap<>();
        body.put("deviceId", deviceId);
        compositeSubscription.add(userRepository
                .deRegisterDeviceForNotification(auth, body, csrf)
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        try {
                            Log.d(TAG, "call: Success - " + responseBody.string());
                        } catch (IOException e) {
                            SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed parsing de register device", null);
                            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; deRegisterDevice()");

                            e.printStackTrace();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; deRegisterDevice()");
                    }
                }));
    }

    @Override
    public void getNotifications() {
        compositeSubscription.add((userRepository.getNotifications(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(new Action1<List<Notification>>() {
                    @Override
                    public void call(List<Notification> notifications) {
                        view.onNotificationsReceived(notifications);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        view.showSnackBar(R.string.something_wrong);
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getNotifications()");
                    }
                })));
    }

    @Override
    public void markNotificationAsRead(final String notificationId) {
        HashMap<String, String> body = new HashMap<>();
        body.put("notificationId", notificationId);
        compositeSubscription.add(userRepository.markNotificationAsRead(sharedPrefService.getAuthorizationToken(), sharedPrefService
                .getCsrfToken(), body)
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        HashMap<String, String> data = new HashMap<>();
                        data.put("notificationId", notificationId);
                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "markNotificationAsRead", data);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; markNotificationAsRead()");
                    }
                }));
    }

    @Override
    public void markAllAsRead() {
        compositeSubscription.add(userRepository.markAllNotificationAsRead(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; markAllAsRead()");
                    }
                }));
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }
}
