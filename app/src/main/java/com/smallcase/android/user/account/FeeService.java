package com.smallcase.android.user.account;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.SmallcaseLedger;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.model.PSmallcaseFees;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rx.functions.Action1;

/**
 * Created by shashankm on 11/04/17.
 */

class FeeService implements FeeContract.Service {
    private static final String TAG = "FeeService";
    private UserRepository userRepository;
    private SharedPrefService sharedPrefService;
    private FeeContract.View view;

    FeeService(UserRepository userRepository, SharedPrefService sharedPrefService, FeeContract.View view) {
        this.sharedPrefService = sharedPrefService;
        this.userRepository = userRepository;
        this.view = view;
    }

    @Override
    public void getUserFees() {
        userRepository.getFees(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(new Action1<List<SmallcaseLedger>>() {
                    @Override
                    public void call(List<SmallcaseLedger> smallcaseLedgers) {
                        if (smallcaseLedgers.isEmpty()) {
                            view.noFees();
                            return;
                        }
                        view.onFeesFetched(convertToViewModel(smallcaseLedgers));
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getUserFees()");
                        view.onFailedFetchingFees();
                    }
                });
    }

    private List<PSmallcaseFees> convertToViewModel(List<SmallcaseLedger> smallcaseLedgers) {
        List<PSmallcaseFees> feesList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault());
        for (SmallcaseLedger smallcaseLedger : smallcaseLedgers) {
            PSmallcaseFees smallcaseFees = new PSmallcaseFees();
            smallcaseFees.setScid(smallcaseLedger.getScid());
            smallcaseFees.setSmallcaseName(smallcaseLedger.getName());
            smallcaseFees.setiScid(smallcaseLedger.get_id());
            String amount = AppConstants.RUPEE_SYMBOL + String.format(Locale.getDefault(), "%.2f", smallcaseLedger.getAmount());
            smallcaseFees.setFeesPaid(amount);
            Date broughtDate = AppUtils.getInstance().getPlainDate(smallcaseLedger.getCompletedDate());
            smallcaseFees.setBroughtOn("Bought on " + sdf.format(broughtDate));
            feesList.add(smallcaseFees);
        }

        return feesList;
    }
}
