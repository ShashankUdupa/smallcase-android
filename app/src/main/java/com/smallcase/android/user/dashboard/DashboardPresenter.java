package com.smallcase.android.user.dashboard;

import android.app.Activity;
import android.content.Intent;
import android.text.SpannableString;
import android.view.View;

import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Buy;
import com.smallcase.android.data.model.InvestedSmallcases;
import com.smallcase.android.data.model.Investments;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.Nifty;
import com.smallcase.android.data.model.ReBalanceUpdate;
import com.smallcase.android.data.model.SipDetails;
import com.smallcase.android.data.model.Smallcases;
import com.smallcase.android.data.model.TotalInvestment;
import com.smallcase.android.data.repository.InvestmentRepository;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.news.AllNewsActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.InvestmentHelper;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.model.PInvestedSmallcase;
import com.smallcase.android.view.model.PInvestedSmallcaseWithNews;
import com.smallcase.android.view.model.PNewsSmallcase;
import com.smallcase.android.view.model.PSmallcaseWithNews;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by shashankm on 06/12/16.
 */

public class DashboardPresenter implements DashboardContract.Presenter {
    private static final String TAG = "DashboardPresenter";

    private DashboardContract.Service dashboardServiceContract;
    private DashboardContract.View dashboardViewContract;
    private NetworkHelper networkHelper;
    private InvestmentHelper investmentHelper;
    private int offSet, count;
    // If all five are false, show try again layout
    private boolean fetchedNifty = true, fetchedInvestment = true, fetchedWatchlist = true;
    private Activity activity;
    private boolean areInvestmentPresent;

    public DashboardPresenter(InvestmentHelper investmentHelper, SharedPrefService sharedPrefService) {
        this.investmentHelper = investmentHelper;
        this.dashboardServiceContract = new DashboardService(sharedPrefService);
    }

    DashboardPresenter(Activity activity, MarketRepository marketRepository, InvestmentRepository investmentRepository, DashboardContract
            .View dashboardViewContract, SharedPrefService sharedPrefService, NetworkHelper networkHelper,
                       SmallcaseRepository smallcaseRepository, InvestmentHelper investmentHelper, UserRepository userRepository) {
        this.dashboardViewContract = dashboardViewContract;
        this.networkHelper = networkHelper;
        this.investmentHelper = investmentHelper;
        this.activity = activity;
        this.dashboardServiceContract = new DashboardService(sharedPrefService, investmentRepository, smallcaseRepository, this,
                marketRepository, userRepository);
    }

    @Override
    public void fetchData(int newsItemOffSet, int count) {
        String auth = dashboardServiceContract.getAuthToken();
        this.offSet = newsItemOffSet;
        this.count = count;

        if (auth == null) {
            dashboardViewContract.invalidAuthToken();
            return;
        }

        if (!networkHelper.isNetworkAvailable()) {
            dashboardViewContract.showNoNetworkAvailable();
            onNiftyReceived(dashboardServiceContract.getCachedNifty());
            onTotalInvestmentsReceived(dashboardServiceContract.getCachedTotalInvestments());
            onWatchlistNewsReceived(dashboardServiceContract.getCachedWatchlistNews());
            return;
        }

        dashboardServiceContract.getInvestments();
    }

    @Override
    public void onInvestmentsReceived(Investments investments) {
        areInvestmentPresent = true;
        if (investments.getInvestedSmallcases().isEmpty()) {
            areInvestmentPresent = false;
            dashboardViewContract.noInvestments();
        }

        fetchDashboardData(areInvestmentPresent);
    }

    @Override
    public void onNiftyReceived(Nifty nifty) {
        fetchedNifty = true;

        if (nifty == null) return;

        String changePercent = String.format(Locale.getDefault(), "%.2f", Math.abs((nifty.getChange() / nifty.getClose()) * 100)) + "%";
        String indexValue = String.format(Locale.getDefault(), "%.2f", nifty.getPrice());
        if (nifty.getChange() < 0) {
            String message = "Nifty is at " + indexValue + ", down by " + changePercent;
            dashboardViewContract.niftyValueMessage(R.color.red_600, message);
            return;
        }

        String message = "Nifty is at " + indexValue + ", up by " + changePercent;
        dashboardViewContract.niftyValueMessage(R.color.green_800, message);
    }

    @Override
    public void onTotalInvestmentsReceived(TotalInvestment totalInvestment) {
        fetchedInvestment = true;

        if (null == totalInvestment) {
            dashboardViewContract.noInvestments();
            return;
        }

        String networth = AppConstants.getDecimalNumberFormatter().format(totalInvestment.getNetworth());

        String pAndLText = "Current value is ";
        SpannableString spannableString = new SpannableString(pAndLText + AppConstants.RUPEE_SYMBOL + networth);
        dashboardViewContract.onPAndLReceived(spannableString);
    }

    @Override
    public void onSeeAllClicked() {
        ArrayList<String> scids = new ArrayList<>();
        for (String string : dashboardServiceContract.getInvestedScids()) {
            scids.add(string.split("#")[0]);
        }

        Intent intent = new Intent(activity, AllNewsActivity.class);
        intent.putExtra(AllNewsActivity.SCREEN_TITLE, "News for your smallcases");
        intent.putStringArrayListExtra(AllNewsActivity.SCID_LIST, scids);
        intent.putExtra(AllNewsActivity.TYPE, AppConstants.NEWS_TYPE_INVESTED);
        activity.startActivity(intent);
    }

    @Override
    public void onFailedFetchingInvestments(Throwable throwable) {
        if (throwable instanceof HttpException) {
            HttpException exception = (HttpException) throwable;
            if (exception.code() == 401) {
                dashboardViewContract.invalidAuthToken();
                return;
            }
        }
        SentryAnalytics.getInstance().captureEvent(null, throwable, TAG + "; onFailedFetchingInvestments()");
        dashboardViewContract.onFailure();
    }

    @Override
    public void onFailedFetchingNifty() {
        fetchedNifty = false;
        if (fetchedInvestment || fetchedWatchlist) {
            dashboardViewContract.showSnackBar(R.string.could_not_fetch_nifty);
            return;
        }
        dashboardViewContract.onFailure();
    }

    @Override
    public void onFailedFetchingTotalInvestments() {
        fetchedInvestment = false;
        if (fetchedNifty || fetchedWatchlist) {
            dashboardViewContract.showSnackBar(R.string.could_not_fetch_p_and_l);
            return;
        }
        dashboardViewContract.onFailure();
    }

    @Override
    public void onFailedFetchingWatchlistNews() {
        fetchedWatchlist = false;
        if (fetchedNifty || fetchedInvestment) {
            dashboardViewContract.showSnackBar(R.string.could_not_fetch_news);
            return;
        }
        dashboardViewContract.onFailure();
    }

    @Override
    public ArrayList<String> getWatchlist() {
        return new ArrayList<>(dashboardServiceContract.getWatchlist());
    }

    @Override
    public void onFailedFetchingReBalanceUpdates() {
        dashboardViewContract.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onReBalanceUpdatesFetched(List<ReBalanceUpdate> reBalanceUpdates) {
        if (reBalanceUpdates == null || reBalanceUpdates.isEmpty()) {
            dashboardViewContract.hideReBalanceUpdatesIfVisible();
            return;
        }

        dashboardViewContract.showReBalanceUpdates(reBalanceUpdates);
    }

    @Override
    public String getGreeting() {
        String name = dashboardServiceContract.getName();
        String niftyDate = AppUtils.getInstance().getSdf().format(System.currentTimeMillis());
        if (niftyDate.equals("")) return activity.getString(R.string.generic_greeting);
        int hour;
        try {
            hour = Integer.parseInt(niftyDate.split(" ")[1].split(":")[0]);
        } catch (Exception e) {
            e.printStackTrace();
            return activity.getString(R.string.generic_greeting);
        }
        String greeting;
        if (hour >= 0 && hour <= 11) greeting = activity.getString(R.string.morning_greeting);
        else if (hour <= 15) greeting = activity.getString(R.string.afternoon_greeting);
        else if (hour != -1) greeting = activity.getString(R.string.evening_greeting);
        else greeting = activity.getString(R.string.generic_greeting);

        if (name == null) return greeting;

        return greeting + name;
    }

    @Override
    public void destroySubscriptions() {
        dashboardServiceContract.destroySubscriptions();
    }

    @Override
    public void onBuyRemindersFetched(List<Buy> buys) {
        if (buys.isEmpty()) {
            dashboardViewContract.hideMarketOpenIfVisible();
            return;
        }

        dashboardViewContract.showMarketOpenReminder(buys);
    }

    @Override
    public void emptyWatchlist() {
        dashboardViewContract.showNoWatchlist();
    }

    @Override
    public void fetchedNewsWatchlist() {
        List<String> watchlistedScids = dashboardServiceContract.getWatchlist();
        dashboardViewContract.showWatchListIfInvisible();
        dashboardServiceContract.getSmallcaseNews(offSet, count, watchlistedScids);
    }

    @Override
    public void onFailedFetchingWatchlist() {
        dashboardViewContract.showNoWatchlist();
        dashboardViewContract.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void fetchNifty() {
        if (!networkHelper.isNetworkAvailable()) {
            dashboardViewContract.showNoNetworkAvailable();
            onNiftyReceived(dashboardServiceContract.getCachedNifty());
            return;
        }

        dashboardServiceContract.getNiftyData();
    }

    @Override
    public void fetchTotalInvestment() {
        if (!networkHelper.isNetworkAvailable()) {
            dashboardViewContract.showNoNetworkAvailable();
            onTotalInvestmentsReceived(dashboardServiceContract.getCachedTotalInvestments());
            return;
        }

        if (areInvestmentPresent) {
            dashboardServiceContract.getTotalInvestments();
        } else {
            dashboardServiceContract.getInvestments();
        }
    }

    @Override
    public void fetchPendingActions() {
        if (!networkHelper.isNetworkAvailable()) {
            dashboardViewContract.showNoNetworkAvailable();
            onTotalInvestmentsReceived(dashboardServiceContract.getCachedTotalInvestments());
            return;
        }

        dashboardServiceContract.getPendingActions();
    }

    @Override
    public void onSipsFetched(List<SipDetails> sips) {
        if (sips == null || sips.isEmpty()) {
            dashboardViewContract.hideSipsIfVisible();
            return;
        }

        dashboardViewContract.showSipsRemaining(sips);
    }

    @Override
    public boolean isSmallcaseWatchlisted(String scid) {
        List<String> watchlistedScids = dashboardServiceContract.getWatchlist();
        boolean isWatchlisted = false;
        for (String watchlistedScid : watchlistedScids) {
            if (watchlistedScid.equals(scid)) {
                isWatchlisted = true;
                break;
            }
        }
        return isWatchlisted;
    }

    @Override
    public void onWatchlistNewsReceived(List<News> newses) {
        fetchedWatchlist = true;
        if (newses.isEmpty()) {
            dashboardViewContract.showNoWatchlistNews();
            return;
        }

        if (newses.size() < 5) {
            dashboardViewContract.toggleNewsSeeAll(View.GONE);
        } else {
            dashboardViewContract.toggleNewsSeeAll(View.VISIBLE);
        }

        dashboardViewContract.showWatchingSmallcasesLink();
        dashboardViewContract.onWatchlistedSmallcaseReceived(createWatchlistedSmallcaseWithNewsViewModel(newses));
    }

    private List<PSmallcaseWithNews> createWatchlistedSmallcaseWithNewsViewModel(List<News> newses) {
        List<PSmallcaseWithNews> smallcaseWithNewses = new ArrayList<>();
        List<String> watchlistedScids = dashboardServiceContract.getWatchlist();
        for (News news : newses) {
            for (Smallcases smallcase : news.getSmallcases()) {
                if (watchlistedScids.contains(smallcase.getScid())) {
                    List<PNewsSmallcase> newsSmallcaseList = new ArrayList<>();
                    double change = ((smallcase.getStats().getIndexValue() - smallcase.getInitialIndex()) / smallcase.getInitialIndex()) * 100;
                    boolean isChangePositive = change > 0;
                    String changePercent = String.format(Locale.getDefault(), "%.2f", change) + "%";
                    PNewsSmallcase newsSmallcase = new PNewsSmallcase("80", smallcase.getInfo().getName(), changePercent,
                            smallcase.get_id(), smallcase.getScid(), isChangePositive, smallcase.getInfo().getTier());
                    newsSmallcaseList.add(newsSmallcase);
                    PSmallcaseWithNews smallcaseWithNews = new PSmallcaseWithNews(news.getImageUrl(), news.getHeadline(),
                            news.getDate(), newsSmallcaseList, news.getLink());
                    smallcaseWithNewses.add(smallcaseWithNews);
                }
            }
        }
        return smallcaseWithNewses;
    }

    public List<PInvestedSmallcaseWithNews> createInvestedSmallcaseWithNewsViewModel(List<News> newsList, HashMap
            <String, InvestedSmallcases> investedSmallcases) {
        List<PInvestedSmallcaseWithNews> newsWithSmallcaseList = new ArrayList<>();
        for (News news : newsList) {
            // Change this some time! Will be a problem if LARGE number of scids are
            // associated with a news
            PInvestedSmallcaseWithNews investedSmallcaseWithNews = new PInvestedSmallcaseWithNews();
            List<PInvestedSmallcase> investedSmallcaseList = new ArrayList<>();
            for (Smallcases smallcases : news.getSmallcases()) {
                if (null == investedSmallcases.get(smallcases.getScid())) continue;

                InvestedSmallcases investedSmallcase = investedSmallcases.get(smallcases.getScid());
                double netWorth = investedSmallcase.getReturns().getNetworth();
                double unRealizedInvestments = investedSmallcase.getReturns().getUnrealizedInvestment();

                String smallcaseIndex = String.format(Locale.getDefault(), "%.2f", investedSmallcase.getStats().getIndexValue());
                String amount = AppConstants.RUPEE_SYMBOL + AppConstants.getNumberFormatter().format(netWorth - unRealizedInvestments);
                boolean isIndexUp = investedSmallcase.getStats().getIndexValue() > investedSmallcase.getStats().getLastCloseIndex();
                String percentage = String.format(Locale.getDefault(), "%.2f", investmentHelper.calculateProfitOrLossPercent
                        (smallcases.getStats().getIndexValue(), smallcases.getInitialIndex())) + "%";
                boolean isProfit = netWorth > unRealizedInvestments;
                PInvestedSmallcase smallcase = new PInvestedSmallcase(smallcases.getInfo().getName(), investedSmallcase.getSource(),
                        "80", smallcaseIndex, isIndexUp, amount, new SpannableString(percentage), isProfit, smallcases.getScid(), investedSmallcase.getDate(),
                        investedSmallcases.get(smallcases.getScid()).get_id());
                investedSmallcaseList.add(smallcase);
            }
            investedSmallcaseWithNews.setNewsHeadline(news.getHeadline());
            investedSmallcaseWithNews.setDate(news.getDate());
            investedSmallcaseWithNews.setNewsImage(news.getImageUrl());
            investedSmallcaseWithNews.setNewsLink(news.getLink());
            investedSmallcaseWithNews.setInvestedSmallcase(investedSmallcaseList);
            newsWithSmallcaseList.add(investedSmallcaseWithNews);
        }
        return newsWithSmallcaseList;
    }

    private void fetchDashboardData(boolean areInvestmentPresent) {
        dashboardServiceContract.getNiftyData();
        if (areInvestmentPresent) dashboardServiceContract.getTotalInvestments();

        dashboardServiceContract.getPendingActions();
        List<String> watchlistedScids = dashboardServiceContract.getWatchlist();
        if (watchlistedScids.isEmpty()) {
            dashboardViewContract.hideWatchingSmallcasesLink();
            dashboardServiceContract.getUserWatchlist();
            return;
        }

        dashboardViewContract.showWatchingSmallcasesLink();
        dashboardViewContract.showWatchListIfInvisible();

        dashboardServiceContract.getSmallcaseNews(offSet, count, watchlistedScids);
    }
}
