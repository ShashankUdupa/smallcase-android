package com.smallcase.android.user.order;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.transition.TransitionManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.SpannableString;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableRow;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.repository.AuthRepository;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.investedsmallcase.InvestedSmallcaseDetailActivity;
import com.smallcase.android.kite.KiteHelper;
import com.smallcase.android.onboarding.LandingActivity;
import com.smallcase.android.onboarding.LoginActivity;
import com.smallcase.android.smallcase.LoginRequest;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.user.ContainerActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.OrderLabel;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PBatch;
import com.smallcase.android.view.model.POrderStock;
import com.smallcase.android.view.model.Stock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;

import static com.smallcase.android.onboarding.LoginActivity.REQUEST_TOKEN_KEY;

public class BatchDetailsActivity extends AppCompatActivity implements BatchDetailContract.View, SmallcaseHelper.LoginResponse {
    public static final String ISCID = "iscid";
    public static final String SMALLCASE_NAME = "smallcase_name";
    public static final String SCID = "scid";
    public static final String SOURCE = "source";

    private static final String TAG = "BatchDetailsActivity";
    private static final int LOGIN_REQUEST_CODE = 1;

    @BindView(R.id.batch_list)
    RecyclerView batchList;
    @BindView(R.id.parent)
    View parent;
    @BindView(R.id.text_tool_bar_title)
    GraphikText toolbarTitle;
    @BindView(R.id.loading_indicator)
    View loading;
    @BindView(R.id.confirm)
    GraphikText bottomSheetPrimaryActionText;
    @BindView(R.id.confirm_description)
    GraphikText addFundsDescription;
    @BindView(R.id.blur)
    View blur;
    @BindView(R.id.confirm_container)
    View confirmCard;
    @BindView(R.id.confirm_card)
    View bottomSheetPrimaryAction;
    @BindView(R.id.smallcase_name)
    GraphikText smallcaseNameText;
    @BindView(R.id.smallcase_image)
    ImageView smallcaseImage;
    @BindView(R.id.smallcase_title_card)
    View smallcaseTitleCard;
    @BindView(R.id.details_text)
    GraphikText smallcaseDetailsText;
    @BindView(R.id.smallcase_details)
    View smallcaseDetails;
    @BindView(R.id.custom_tag)
    View customTag;

    private List<PBatch> pBatches;
    private BatchDetailContract.Service service;
    private String iscid, smallcaseName, originalLabel, scid, batchId, source;
    private BatchAdapter batchAdapter;
    private ProgressDialog progressDialog;
    private LoginRequest loginRequest;
    private BottomSheetBehavior bottomSheetBehavior;
    private AnalyticsContract analyticsContract;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_batch_details);

        ButterKnife.bind(this);

        smallcaseTitleCard.setVisibility(View.GONE);
        toolbarTitle.setText("smallcase Batches");
        iscid = getIntent().getStringExtra(ISCID);
        scid = getIntent().getStringExtra(SCID);
        smallcaseName = getIntent().getStringExtra(SMALLCASE_NAME);
        source = getIntent().getStringExtra(SOURCE);

        bottomSheetBehavior = BottomSheetBehavior.from(confirmCard);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        String imageUrl;
        if (SmallcaseSource.CREATED.equals(source)) {
            imageUrl = AppConstants.CREATED_SMALLCASE_IMAGE_URL + "80/" + scid + ".png";
        } else {
            imageUrl = AppConstants.SMALLCASE_IMAGE_URL + "80/" + scid + ".png";
        }

        if (SmallcaseSource.CUSTOM.equals(source)) {
            customTag.setVisibility(View.VISIBLE);
        }

        smallcaseNameText.setText(smallcaseName);
        Glide.with(this)
                .load(imageUrl)
                .into(smallcaseImage);

        analyticsContract = new AnalyticsManager(getApplicationContext());
        batchList.setLayoutManager(new LinearLayoutManager(this));
        batchList.setNestedScrollingEnabled(false);
        batchList.setHasFixedSize(false);
        RecyclerView.ItemAnimator animator = batchList.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(true);
            animator.setChangeDuration(600);
        }

        SharedPrefService sharedPrefService = new SharedPrefService(this);
        KiteRepository kiteRepository = new KiteRepository();
        service = new BatchDetailService(this, sharedPrefService, this, new NetworkHelper(this), new MarketRepository(),
                new KiteHelper(sharedPrefService, kiteRepository), scid, new UserSmallcaseRepository());
        loginRequest = new SmallcaseHelper(new AuthRepository(), this, sharedPrefService);

        setUpCallToAction();

        service.getBatchDetails(iscid);
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.HARD_RELOAD_BROADCAST));
    }

    private void setUpCallToAction() {
        if (!service.getExitedIScids().contains(iscid)) {
            smallcaseDetails.setVisibility(View.VISIBLE);
            smallcaseDetailsText.setText("TRACK");
            smallcaseDetailsText.setOnClickListener(v -> {
                Intent intent = new Intent(BatchDetailsActivity.this, InvestedSmallcaseDetailActivity.class);
                intent.putExtra(InvestedSmallcaseDetailActivity.ISCID, iscid);
                intent.putExtra(InvestedSmallcaseDetailActivity.SCID, scid);
                intent.putExtra(InvestedSmallcaseDetailActivity.SOURCE, source);
                startActivity(intent);
            });
        }
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
        service.destroySubscriptions();
        super.onDestroy();
    }

    @Override
    public void showSnackBar(int resId) {
        AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_LONG, resId);
    }

    @Override
    public void onBatchDetailsReceived(List<PBatch> pBatches) {
        loading.setVisibility(View.GONE);
        smallcaseTitleCard.setVisibility(View.VISIBLE);
        batchList.setVisibility(View.VISIBLE);
        this.pBatches = new ArrayList<>(pBatches);

        if (batchAdapter == null) {
            batchAdapter = new BatchAdapter();
            batchList.setAdapter(batchAdapter);
        } else {
            batchAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onFailedFetchingBatches() {
        showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onBatchCanceledSuccessfully() {
        service.getBatchDetails(iscid);
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (AppConstants.HARD_RELOAD_BROADCAST.equals(intent.getAction())) {
                if (!isFinishing()) {
                    loading.setVisibility(View.VISIBLE);
                    smallcaseTitleCard.setVisibility(View.GONE);
                    batchList.setVisibility(View.GONE);
                    service.getBatchDetails(iscid);
                }
            }
        }
    };

    @Override
    public void onFailedCancelingBatch(Throwable throwable) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        if (throwable != null && throwable instanceof HttpException && ((HttpException) throwable).code() == 401) {
            sendUserToKiteLogin();
            return;
        }

        showSnackBar(R.string.something_wrong);
    }

    @Override
    public void showPlacingOrderLoader() {
        progressDialog = ProgressDialog.show(this, "", "Loading...");
        progressDialog.show();
    }

    private List<Stock> getRepairableStocks(List<POrderStock> orderStocks) {
        List<Stock> stocks = new ArrayList<>();
        for (POrderStock orderStock : orderStocks) {
            if (orderStock.getRemainingShares() == 0) continue;
            Stock stock = new Stock();
            stock.setShares(orderStock.getRemainingShares());
            stock.setSid(orderStock.getSid());
            stock.setBuy("Buy".equalsIgnoreCase(orderStock.getType()));
            stock.setStockName(orderStock.getStockTicker());
            stocks.add(stock);
        }
        return stocks;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            //Received request_token from Kite login. Request authToken from server.
            showPlacingOrderLoader();
            HashMap<String, String> body = new HashMap<>();
            body.put("broker", "kite");
            body.put("reqToken", data.getStringExtra(REQUEST_TOKEN_KEY));
            body.put("app", "platform");

            loginRequest.getJwtToken(body);
        }
    }

    @Override
    public void hidePlacingOrderLoader() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showMarketClosed() {
        Snackbar.make(parent, R.string.market_closed_now, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void redirectToLogin() {
        startActivity(new Intent(this, LandingActivity.class));
        finish();
    }

    @Override
    public void onFailedRepairingOrders(Throwable throwable) {
        if (throwable instanceof HttpException && (((HttpException) throwable).code() == 401)) {
            sendUserToKiteLogin();
            return;
        }

        showSnackBar(R.string.something_wrong);
    }

    @Override
    public void sendUserToReviewOrder(List<Stock> stockList, double investmentAmount) {
        hidePlacingOrderLoader();

        Intent intent = new Intent(this, ReviewOrderActivity.class);
        intent.putExtra(ReviewOrderActivity.ORDER_LABEL, service.getBatchLabel());
        intent.putExtra(ReviewOrderActivity.INVESTMENT_AMOUNT, investmentAmount);
        intent.putExtra(ReviewOrderActivity.ISCID, iscid);
        intent.putExtra(ReviewOrderActivity.SMALLCASE_NAME, smallcaseName);
        intent.putExtra(ReviewOrderActivity.SCID, scid);
        intent.putExtra(ReviewOrderActivity.IS_REPAIR, true);
        intent.putExtra(ReviewOrderActivity.BATCH_ID, batchId);
        intent.putExtra(ReviewOrderActivity.SOURCE, source);
        intent.putParcelableArrayListExtra(ReviewOrderActivity.STOCK_LIST, (ArrayList<? extends Parcelable>) stockList);
        startActivity(intent);
        overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
        finish();
    }

    @Override
    public void sendUserToKiteLogin() {
        startActivityForResult(new Intent(this, LoginActivity.class), LOGIN_REQUEST_CODE);
        overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    }

    @Override
    public void showNeedFunds(SpannableString requiredFunds) {
        showBlur();

        addFundsDescription.setText(requiredFunds);
        bottomSheetPrimaryAction.setOnClickListener(v -> {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(AppConstants.KITE_ADD_FUNDS));
            startActivity(intent);
        });
        bottomSheetPrimaryActionText.setText(R.string.add_funds);

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private void showProgressDialog() {
        progressDialog = ProgressDialog.show(this, "", "Loading...");
        progressDialog.show();
    }

    @Override
    public void onJwtSavedSuccessfully() {
        hidePlacingOrderLoader();
        showSnackBar(R.string.logged_in_successfully);
    }

    @Override
    public void onFailedFetchingJwt() {
        hidePlacingOrderLoader();
        showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onDifferentUserLoggedIn() {
        Intent intent = new Intent(this, ContainerActivity.class);
        intent.putExtra(ContainerActivity.NEW_USER, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void showArchiveConfirmationBottomSheet(final String batchId, List<POrderStock> stocks) {
        showBlur();

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", smallcaseName);
        eventProperties.put("smallcaseType", source);
        eventProperties.put("accessedFrom", "Batch");
        analyticsContract.sendEvent(eventProperties, "Viewed Archive Order Popup", Analytics.MIXPANEL);

        String archiveConfirmationString = areFailedOrdersGreaterThanOne(stocks) ? "Archiving unfilled orders retains only the filled " +
                "orders in this batch. The stocks/shares in the unfilled orders will not be part of this smallcase anymore" + (OrderLabel.BUY.equalsIgnoreCase(service.getBatchLabel()) ?
                "\n\nYour smallcase will not reflect the constituents & returns of the original smallcase if you archive the orders. To ensure they match, repair the batch instead" : "")
                : "Archiving unfilled order retains only the filled " +
                "orders in this batch. The stocks/shares in the unfilled orders will not be part of this smallcase anymore" + (OrderLabel.BUY.equalsIgnoreCase(service.getBatchLabel()) ?
                "\n\nYour smallcase will not reflect the constituents & returns of the original smallcase if you archive the order. To ensure they match, repair the batch instead" : "");

        addFundsDescription.setText(archiveConfirmationString);
        bottomSheetPrimaryAction.setOnClickListener(v -> {
            Map<String, Object> metaData = new HashMap<>();
            metaData.put("smallcaseName", smallcaseName);
            metaData.put("smallcaseType", source);
            metaData.put("orderType", originalLabel);
            metaData.put("accessedFrom", "Order Page");
            analyticsContract.sendEvent(metaData, "Archived Order", Analytics.MIXPANEL, Analytics.INTERCOM);

            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            showProgressDialog();
            HashMap<String, String> body = new HashMap<>();
            body.put("batchId", batchId);
            body.put("iscid", iscid);
            service.cancelBatch(body);
        });
        bottomSheetPrimaryActionText.setText(R.string.archive);

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private void showRepairConfirmationBottomSheet(final String batchType, final String batchId, final List<POrderStock> stocks) {
        showBlur();

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", smallcaseName);
        eventProperties.put("smallcaseType", source);
        eventProperties.put("accessedFrom", "Batch");
        analyticsContract.sendEvent(eventProperties, "Viewed Repair Order Popup", Analytics.MIXPANEL);

        boolean areFailedOrdersGreaterThanOne = areFailedOrdersGreaterThanOne(stocks);
        addFundsDescription.setText("Repairing this order will place fresh " + (areFailedOrdersGreaterThanOne ? "order" : "orders") +
                " for the unfilled " + (areFailedOrdersGreaterThanOne ? "order" : "orders") +
                (OrderLabel.BUY.equalsIgnoreCase(service.getBatchLabel()) ?
                        "\n\nRepairing this batch ensures your smallcase matches the original smallcase in stock constituents" +
                                " & their weights" : ""));
        bottomSheetPrimaryAction.setOnClickListener(v -> {
            Map<String, Object> metaData = new HashMap<>();
            metaData.put("smallcaseName", smallcaseName);
            metaData.put("smallcaseType", source);
            metaData.put("orderType", batchType);
            metaData.put("accessedFrom", "Order Page");
            analyticsContract.sendEvent(metaData, "Repaired Order", Analytics.MIXPANEL);

            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            originalLabel = batchType;
            BatchDetailsActivity.this.batchId = batchId;
            service.fixBatch(getRepairableStocks(stocks));
        });
        bottomSheetPrimaryActionText.setText(R.string.repair_batch);

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private boolean areFailedOrdersGreaterThanOne(List<POrderStock> stocks) {
        int failedOrders = 0;
        for (POrderStock stock : stocks) {
            if (stock.getRemainingShares() == 0) continue;
            if (failedOrders == 1)
                return true; // Some shares not filled and already have one more failed order
            failedOrders++;
        }
        return false;
    }

    private void showBlur() {
        blur.setVisibility(View.VISIBLE);
        blur.setOnClickListener(v -> bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN));

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    blur.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    class BatchAdapter extends RecyclerView.Adapter<BatchAdapter.ViewHolder> {

        @Override
        public BatchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.batch_card,
                    parent, false));
        }

        @Override
        public void onBindViewHolder(BatchAdapter.ViewHolder holder, int position) {
            final int adapterPosition = holder.getAdapterPosition();
            final PBatch batch = pBatches.get(adapterPosition);
            holder.batchType.setText(batch.getBatchType());
            Glide.with(BatchDetailsActivity.this)
                    .load(R.drawable.arrow_right)
                    .into(holder.showHideBatchOrders);
            holder.placedOn.setText(batch.getPlacedOn());
            holder.filledQuantity.setText(batch.getFilledVsQuantity());

            if ("".equals(batch.getBuyValue())) {
                holder.buyValue.setVisibility(View.GONE);
            } else {
                holder.buyValue.setVisibility(View.VISIBLE);
                holder.buyValue.setText("Total buy value is: " + AppConstants.RUPEE_SYMBOL + batch.getBuyValue());
            }

            if ("".equals(batch.getSellValue())) {
                holder.sellValue.setVisibility(View.GONE);
            } else {
                holder.sellValue.setVisibility(View.VISIBLE);
                holder.sellValue.setText("Total sell value is: " + AppConstants.RUPEE_SYMBOL + batch.getSellValue());
            }

            holder.batchCard.setOnClickListener(v -> {
                boolean change = !batch.isShouldShowOrders();

                batch.setShouldShowOrders(change);
                if (!batch.isShouldShowOrders())
                    TransitionManager.beginDelayedTransition(batchList);
                notifyItemChanged(adapterPosition);
            });

            holder.status.setText(service.getStatusText(batch.getOrderStatus()));
            holder.percent.setProgress(batch.getPercentFilled());
            int progressColor;
            if (batch.getPercentFilled() <= 50) {
                progressColor = R.drawable.progress_red;
            } else if (batch.getPercentFilled() == 100) {
                progressColor = R.drawable.progress_green;
            } else {
                progressColor = R.drawable.progress_purple;
            }

            holder.percent.setProgressDrawable(ContextCompat.getDrawable(BatchDetailsActivity.this, progressColor));

            if (batch.isShouldShowOrders()) {
                holder.stocksList.setVisibility(View.VISIBLE);
                holder.dividerOne.setVisibility(View.VISIBLE);
                holder.dividerTwo.setVisibility(View.VISIBLE);
                holder.buySellContainer.setVisibility(View.VISIBLE);
                holder.showHideBatchOrders.setRotation(270);
            } else {
                holder.stocksList.setVisibility(View.GONE);
                holder.dividerOne.setVisibility(View.GONE);
                holder.dividerTwo.setVisibility(View.GONE);
                holder.buySellContainer.setVisibility(View.GONE);
                holder.showHideBatchOrders.setRotation(90);
            }

            if ("".equals(batch.getBuyValue()) && "".equals(batch.getSellValue())) {
                holder.buySellContainer.setVisibility(View.GONE);
            }

            holder.stocksList.setLayoutManager(new LinearLayoutManager(BatchDetailsActivity.this));
            holder.stocksList.setHasFixedSize(true);
            holder.stocksList.setAdapter(new StocksAdapter(batch.getStocks(), batch.getPlacedOn()));

            if (service.shouldShowRepairAndArchive(batch.getOrderStatus())) {
                holder.dividerTwo.setVisibility(View.VISIBLE);
                holder.actionButtons.setVisibility(View.VISIBLE);
                holder.archive.setOnClickListener(v -> showArchiveConfirmationBottomSheet(batch.getBatchId(), batch.getStocks()));

                holder.repair.setOnClickListener(v -> showRepairConfirmationBottomSheet(batch.getBatchType(), batch.getBatchId(),
                        batch.getStocks()));
            } else {
                holder.actionButtons.setVisibility(View.GONE);
            }
        }

        @Override
        public int getItemCount() {
            return pBatches.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.batch_type)
            GraphikText batchType;
            @BindView(R.id.show_hide_batch_orders)
            ImageView showHideBatchOrders;
            @BindView(R.id.placed_on)
            GraphikText placedOn;
            @BindView(R.id.filled_quantity)
            GraphikText filledQuantity;
            @BindView(R.id.percent)
            ProgressBar percent;
            @BindView(R.id.divider_one)
            View dividerOne;
            @BindView(R.id.divider_two)
            View dividerTwo;
            @BindView(R.id.stocks_list)
            RecyclerView stocksList;
            @BindView(R.id.buy_value)
            GraphikText buyValue;
            @BindView(R.id.sell_value)
            GraphikText sellValue;
            @BindView(R.id.batch_card)
            View batchCard;
            @BindView(R.id.action_buttons)
            View actionButtons;
            @BindView(R.id.archive)
            View archive;
            @BindView(R.id.repair)
            View repair;
            @BindView(R.id.status)
            GraphikText status;
            @BindView(R.id.buy_sell_container)
            View buySellContainer;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }

    class StocksAdapter extends RecyclerView.Adapter<StocksAdapter.ViewHolder> {
        private List<POrderStock> stocks;
        private String placedOn;

        StocksAdapter(List<POrderStock> stocks, String placedOn) {
            this.stocks = new ArrayList<>(stocks);
            this.placedOn = placedOn;
        }

        @Override
        public StocksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.stock_card, parent, false));
        }

        @Override
        public void onBindViewHolder(final StocksAdapter.ViewHolder holder, int position) {
            if (position == 0) {
                holder.stockName.setText("Stock");
                holder.stockName.setTextColor(ContextCompat.getColor(BatchDetailsActivity.this, R.color.secondary_text));

                holder.stockPrice.setText("Avg. Price");
                holder.stockPrice.setTextColor(ContextCompat.getColor(BatchDetailsActivity.this, R.color.secondary_text));

                holder.stockQuantity.setText("Qty Filled");
                holder.stockQuantity.setTextColor(ContextCompat.getColor(BatchDetailsActivity.this, R.color.secondary_text));

                holder.stockContainer.setBackgroundColor(ContextCompat.getColor(BatchDetailsActivity.this, R.color.white));
                holder.stockType.setText("Type");
                holder.stockType.setTextColor(ContextCompat.getColor(BatchDetailsActivity.this, R.color.secondary_text));
                return;
            }

            final POrderStock stock = stocks.get(position - 1);

            holder.stockName.setText(stock.getStockTicker());
            holder.stockName.setTextColor(ContextCompat.getColor(BatchDetailsActivity.this, R.color.primary_text));

            holder.stockPrice.setText(stock.getAveragePrice());
            holder.stockPrice.setTextColor(ContextCompat.getColor(BatchDetailsActivity.this, R.color.primary_text));

            holder.stockQuantity.setText(String.valueOf(stock.getQuantityFilled()));
            holder.stockQuantity.setTextColor(ContextCompat.getColor(BatchDetailsActivity.this, R.color.primary_text));

            if (stock.isFilled()) {
                holder.stockContainer.setBackgroundColor(ContextCompat.getColor(BatchDetailsActivity.this, R.color.white));
            } else {
                holder.stockContainer.setBackgroundColor(ContextCompat.getColor(BatchDetailsActivity.this, R.color.grey_200));
            }

            holder.stockType.setText(stock.getType());
            holder.stockType.setTextColor(ContextCompat.getColor(BatchDetailsActivity.this, ("Buy").equalsIgnoreCase(stock.getType()) ?
                    R.color.green_600 : R.color.red_600));

            holder.stockCard.setOnClickListener(v -> {
                // Show pop up
                showOrderDetails(stock);
            });
        }

        private void showOrderDetails(POrderStock stock) {
            AlertDialog.Builder builder = new AlertDialog.Builder(BatchDetailsActivity.this);
            // Get the layout inflater
            LayoutInflater inflater = getLayoutInflater();
            // Inflate and set the layout for the dialog
            builder.setCancelable(true);

            View view = inflater.inflate(R.layout.order_details, (ViewGroup) parent, false);
            GraphikText stockTicker = view.findViewById(R.id.stock_ticker);
            GraphikText type = view.findViewById(R.id.type);
            GraphikText avgPrice = view.findViewById(R.id.average_price);
            GraphikText triggeredPrice = view.findViewById(R.id.trigger_price);
            GraphikText quantity = view.findViewById(R.id.quantity);
            GraphikText orderType = view.findViewById(R.id.order_type);
            GraphikText productValidity = view.findViewById(R.id.product_validity);
            GraphikText orderPlacedBy = view.findViewById(R.id.order_placed_by);
            GraphikText orderId = view.findViewById(R.id.order_id);
            GraphikText exchangeOrderId = view.findViewById(R.id.exchange_order_id);
            GraphikText date = view.findViewById(R.id.date);
            GraphikText orderStatus = view.findViewById(R.id.order_status);
            GraphikText statusMessage = view.findViewById(R.id.status_message);

            stockTicker.setText(stock.getStockTicker());
            type.setText(stock.getType());
            type.setTextColor("Buy".equalsIgnoreCase(stock.getType()) ? ContextCompat.getColor(BatchDetailsActivity.this,
                    R.color.green_800) : ContextCompat.getColor(BatchDetailsActivity.this, R.color.red_600));
            avgPrice.setText(AppConstants.RUPEE_SYMBOL + stock.getAveragePrice());
            triggeredPrice.setText(AppConstants.RUPEE_SYMBOL + stock.getTriggeredPrice());
            quantity.setText(stock.getQuantityFilled());
            orderType.setText(stock.getOrderType());
            productValidity.setText(stock.getProductValidity());
            orderPlacedBy.setText(service.getKiteId());
            orderId.setText(stock.getOrderId());
            exchangeOrderId.setText(stock.getExchangeOrderId());
            date.setText(placedOn);
            orderStatus.setText(stock.getOrderStatus());
            statusMessage.setText(stock.getStatusMessage());
            builder.setView(view)
                    // Add action buttons
                    .setPositiveButton("OK", (dialog, id) -> {

                    });
            final AlertDialog alertDialog = builder.create();
            alertDialog.setOnShowListener(dialog -> {
                if (alertDialog.getButton(AlertDialog.BUTTON_POSITIVE) == null) return;
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTypeface(AppUtils.getInstance().getFont(BatchDetailsActivity.this, AppConstants.FontType.MEDIUM));
            });

            alertDialog.show();
        }

        @Override
        public int getItemCount() {
            return stocks.size() + 1;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.stock_name)
            GraphikText stockName;
            @BindView(R.id.stock_price)
            GraphikText stockQuantity;
            @BindView(R.id.stock_quantity)
            GraphikText stockPrice;
            @BindView(R.id.type)
            GraphikText stockType;
            @BindView(R.id.stock_container)
            View stockContainer;
            @BindView(R.id.stock_card)
            View stockCard;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);

                TableRow.LayoutParams param = new TableRow.LayoutParams(
                        0,
                        TableRow.LayoutParams.WRAP_CONTENT,
                        1f
                );
                param.leftMargin = (int) AppUtils.getInstance().dpToPx(10f);
                stockName.setLayoutParams(param);

                stockName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.secondary_text_small));
                stockQuantity.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.secondary_text_small));
                stockPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.secondary_text_small));
                stockType.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.secondary_text_small));
            }
        }
    }
}
