package com.smallcase.android.user.order;

import com.smallcase.android.data.model.Order;
import com.smallcase.android.view.model.POrder;

import java.util.List;

/**
 * Created by shashankm on 07/04/17.
 */

interface OrderDetailsContract {
    interface View {

        void showSnackBar(int errResId);

        void showNoOrders();

        void onOrdersReceived(List<POrder> orderList);
    }

    interface Presenter {

        void getOrderDetails();

        void onOrderDetailsFetched(List<Order> orders);

        void onFailedFetchingOrderDetails();

        void destroySubscriptions();
    }

    interface Service {

        void getOrderDetails();

        void destroySubscriptions();
    }
}
