package com.smallcase.android.user.account;

import java.util.HashMap;

/**
 * Created by shashankm on 26/12/16.
 */

interface AccountContract {

    interface View {
        void onFailedFetchingUserDetails();

        void userData(String userName, String userId);
    }

    interface Presenter {
        void fetchUserDetails();

        void updateProfile(HashMap<String, Object> body);

        void destroySubscriptions();
    }
}
