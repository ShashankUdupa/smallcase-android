package com.smallcase.android.user.dashboard;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.smallcase.android.data.model.Nifty;

import java.lang.reflect.Type;

/**
 * Created by shashankm on 06/12/16.
 */

public class NiftyDeserializer implements JsonDeserializer<Nifty> {
    @Override
    public Nifty deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        Gson gson = new Gson();
        return gson.fromJson(object.getAsJsonObject("data").getAsJsonObject(".NSEI"), Nifty.class);
    }
}
