package com.smallcase.android.user.account;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.kite.KiteContract;
import com.smallcase.android.kite.KiteHelper;
import com.smallcase.android.news.WebActivity;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.user.ContainerActivity;
import com.smallcase.android.user.WatchlistActivity;
import com.smallcase.android.user.coustomize.DraftSmallcasesActivity;
import com.smallcase.android.user.notification.NotificationsActivity;
import com.smallcase.android.user.order.OrderDetailsActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.AndroidApp;
import com.smallcase.android.view.android.GraphikText;
import com.squareup.leakcanary.RefWatcher;

import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.intercom.android.sdk.Intercom;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by shashankm on 10/04/17.
 */

public class AccountFragment extends Fragment implements AccountContract.View, KiteContract.FundResponse,
        SmallcaseHelper.LoginResponse, SwipeRefreshLayout.OnRefreshListener {
    public static final String FAQ_URL = "http://help.smallcase.com/smallcase-for-android";

    private static final String TAG = AccountFragment.class.getSimpleName();

    @BindView(R.id.user_name) GraphikText userName;
    @BindView(R.id.kite_id) GraphikText kiteId;
    @BindView(R.id.available_funds) GraphikText availableFunds;
    @BindView(R.id.add_funds) GraphikText addFunds;
    @BindView(R.id.funds_text) GraphikText fundsText;
    @BindView(R.id.initial) GraphikText initial;
    @BindView(R.id.swipe_refresh) SwipeRefreshLayout swipeRefreshLayout;

    private Activity context;
    private Unbinder unbinder;
    private AccountContract.Presenter accountPresenter;
    private KiteHelper kiteHelper;
    private AnalyticsContract analyticsContract;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = (Activity) context;
        accountPresenter = new AccountPresenter(new UserRepository(), this, new SharedPrefService(context), context);
        kiteHelper = new KiteHelper(new SharedPrefService(context), new KiteRepository());
        analyticsContract = new AnalyticsManager(getActivity().getApplication());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.account_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);

        accountPresenter.fetchUserDetails();
        kiteHelper.getFunds(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.blue_800);
        swipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    @Override
    public void onDestroy() {
        RefWatcher refWatcher = AndroidApp.getRefWatcher(getActivity());
        refWatcher.watch(this);
        if (unbinder != null) {
            unbinder.unbind();
        }
        super.onDestroy();
    }

    @OnClick(R.id.logout)
    public void logout() {
        if (context == null) return;

        final AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setMessage(R.string.are_you_sure)
                .setPositiveButton(R.string.log_out, (dialog, which) -> {
                    analyticsContract.sendEvent(null, "Logged Out", Analytics.MIXPANEL, Analytics.CLEVERTAP);
                    ((ContainerActivity) context).logoutUser();
                })
                .setNegativeButton(R.string.stay, (dialog, which) -> {

                }).create();

        alertDialog.setOnShowListener(dialog -> {
            if (alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE) == null) return;
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context, R.color.secondary_text));
        });
        alertDialog.show();
    }

    @Override
    public void onFailedFetchingUserDetails() {

    }

    @Override
    public void userData(String userName, String userId) {
        if (!isAdded()) return;

        swipeRefreshLayout.setRefreshing(false);
        this.userName.setText(userName);
        initial.setText(userName.substring(0, 1));
        kiteId.setText("Zerodha Kite ID: " + userId);
    }

    public AccountContract.Presenter getPresenter() {
        return accountPresenter;
    }

    @Override
    public void onFundsReceived(double availableFunds) {
        if (!isAdded()) return;

        swipeRefreshLayout.setRefreshing(false);
        addFunds.setText("ADD FUNDS");
        fundsText.setText("Funds Available");
        this.availableFunds.setText(AppConstants.RUPEE_SYMBOL + String.format(Locale.getDefault(), "%.2f", availableFunds));
    }

    @Override
    public void onFailedFetchingFunds(@Nullable Throwable throwable) {
        if (!isAdded()) return;

        if (throwable instanceof HttpException) {
            if (((HttpException) throwable).code() == 401) {
                addFunds.setText("LOGIN TO KITE");
                fundsText.setText("Please login to refresh funds");
            }
        }
    }

    @OnClick(R.id.your_billings)
    public void showFees() {
        analyticsContract.sendEvent(null, "Viewed Fees", Analytics.MIXPANEL, Analytics.CLEVERTAP);
        startNewActivity(FeesActivity.class);
    }

    @OnClick(R.id.your_watchlist)
    public void showWatchlist() {
        startNewActivity(WatchlistActivity.class);
    }

    @OnClick(R.id.your_drafts)
    public void showDrafts() {
        startNewActivity(DraftSmallcasesActivity.class);
    }

    @OnClick(R.id.add_funds)
    public void addFunds() {
        if (context != null && availableFunds.getText().toString().equals(getString(R.string.no_rupee_fetched))) {
            // Send user to login
            ((ContainerActivity) context).sendUserToLogin(this);
            return;
        }

        // Send user to add funds
        analyticsContract.sendEvent(null, "Proceeded To Adding Funds", Analytics.INTERCOM, Analytics.CLEVERTAP, Analytics.MIXPANEL);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(AppConstants.KITE_ADD_FUNDS));
        startActivity(intent);
    }

    @OnClick(R.id.your_orders)
    public void showYourOrders() {
        startNewActivity(OrderDetailsActivity.class);
    }

    @OnClick(R.id.faq)
    public void showFaqs() {
        HashMap<String, Object> metaData = new HashMap<>();
        metaData.put("accessedFrom", "Account");
        analyticsContract.sendEvent(metaData, "Viewed FAQs", Analytics.INTERCOM, Analytics.MIXPANEL);

        Intent intent = new Intent(context, WebActivity.class);
        intent.putExtra(WebActivity.TITLE, getString(R.string.faqs));
        intent.putExtra(WebActivity.URL, FAQ_URL);
        startActivity(intent);
    }

    @OnClick(R.id.chat)
    public void showChat() {
        analyticsContract.sendEvent(null, "Opened Chat", Analytics.MIXPANEL);
        Intercom.client().displayMessenger();
    }

    public void sendFeedback() {
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        PackageInfo pInfo;
        String version = "NA";
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        boolean isDeviceVirtual = Build.PRODUCT.equals("sdk");
        String feedbackBody = "Platform: Android\n" + "Version: " + Build.VERSION.SDK_INT + "\n" +
                "App Version: " + version + "\n" + "Device Model: " + AppUtils.getInstance().getDeviceName() + "\n" +
                "Virtual Device: " + isDeviceVirtual + "\n" + "Type your feedback here: " + "\n";
        emailIntent.setType("plain/text");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"mobile@smallcase.com"});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "smallcase android app feedback");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, feedbackBody);

        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    @OnClick(R.id.rate)
    public void rateUs() {
        analyticsContract.sendEvent(null, "Clicked RateUs", Analytics.MIXPANEL);
        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    @OnClick(R.id.about)
    public void showAbout() {
        analyticsContract.sendEvent(null, "Viewed About", Analytics.MIXPANEL);
        startNewActivity(AboutActivity.class);
    }

    @OnClick(R.id.notification_pref)
    public void showNotificationPreferences() {
        startNewActivity(NotificationsActivity.class);
    }

    @Override
    public void onJwtSavedSuccessfully() {
        kiteHelper.getFunds(this);
        accountPresenter.fetchUserDetails();
    }

    @Override
    public void onFailedFetchingJwt() {
        if (context == null) return;

        final AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setMessage("Failed logging in, please try again")
                .setPositiveButton(android.R.string.ok, (dialog, which) -> ((ContainerActivity) context).logoutUser()).create();

        alertDialog.setOnShowListener(dialog -> {
            if (alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE) == null) return;
            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context, R.color.secondary_text));
        });
        alertDialog.show();
    }

    @Override
    public void onDifferentUserLoggedIn() {

    }

    private void startNewActivity(Class className) {
        startActivity(new Intent(context, className));
    }

    @Override
    public void onRefresh() {
        accountPresenter.fetchUserDetails();
        kiteHelper.getFunds(this);
    }

    public void refreshFunds() {
        if (!isAdded()) return;

        kiteHelper.getFunds(this);
    }

    public void updateUserProfile(HashMap<String, Object> body) {
        if (!isAdded() || accountPresenter == null) return;

        accountPresenter.updateProfile(body);
    }
}
