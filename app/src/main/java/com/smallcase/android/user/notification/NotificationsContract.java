package com.smallcase.android.user.notification;

/**
 * Created by shashankm on 13/01/17.
 */

interface NotificationsContract {

    void onNotificationStatusReceived(boolean value);

    void showSnackBar(int resId);
}
