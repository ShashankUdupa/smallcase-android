package com.smallcase.android.user.notification;

import com.smallcase.android.data.model.Notification;

import java.util.List;

/**
 * Created by shashankm on 14/04/17.
 */

public interface NotificationContract {
    interface View {

        void onNotificationsReceived(List<Notification> notifications);

        void showSnackBar(int resId);
    }

    interface Service {
        void getNotifications();

        void markNotificationAsRead(String notificationId);

        void markAllAsRead();

        void destroySubscriptions();
    }
}
