package com.smallcase.android.user.dashboard;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Buy;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 24/05/17.
 */

class BuyReminderService {
    private static final String TAG = "BuyReminderService";
    private UserRepository userRepository;
    private MarketRepository marketRepository;
    private SharedPrefService sharedPrefService;
    private BuyReminderContract view;
    private CompositeSubscription compositeSubscription;

    interface BuyReminderContract {

        void redirectUserToLogin();

        void onBuyRemindersReceived(List<Buy> buys);

        void onFailedFetchingBuys();

        void onFailedIgnoringBuyReminders();

        void onMarketStatusReceived(ResponseBody responseBody);

        void onFailedFetchingMarketStatus();
    }

    BuyReminderService(UserRepository userRepository, SharedPrefService sharedPrefService, BuyReminderContract view,
                       MarketRepository marketRepository) {
        this.userRepository = userRepository;
        this.sharedPrefService = sharedPrefService;
        this.marketRepository = marketRepository;
        this.view = view;
        compositeSubscription = new CompositeSubscription();
    }

    void getBuyReminders() {
        if (sharedPrefService.getAuthorizationToken() == null) {
            view.redirectUserToLogin();
            return;
        }

        compositeSubscription.add(userRepository.getPendingActions(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(actions -> view.onBuyRemindersReceived(actions.getBuys()), throwable -> {
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) {
                        view.redirectUserToLogin();
                        return;
                    }

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getBuyReminders()");
                    view.onFailedFetchingBuys();
                }));

    }

    void getMarketStatus() {
        compositeSubscription.add(marketRepository.getIfMarketIsOpen(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(responseBody -> view.onMarketStatusReceived(responseBody), throwable -> {
                    view.onFailedFetchingMarketStatus();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getMarketStatus()");
                }));
    }

    boolean isSmallcaseWatchlisted(String scid) {
        List<String> watchlistedScids = new ArrayList<>(sharedPrefService.getCachedWatchlist());
        boolean isWatchlisted = false;
        for (String watchlistedScid : watchlistedScids) {
            if (watchlistedScid.equals(scid)) {
                isWatchlisted = true;
                break;
            }
        }
        return isWatchlisted;
    }

    void ignoreAllBuyReminders() {
        compositeSubscription.add(userRepository.ignoreAllBuyReminder(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(responseBody -> {
                    try {
                        JSONObject jsonObject = new JSONObject(responseBody.string());
                        if (!jsonObject.getBoolean("success")) {
                            view.onFailedIgnoringBuyReminders();
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                }, throwable -> {
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) {
                        view.redirectUserToLogin();
                        return;
                    }

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; ignoreAllBuyReminders()");
                    view.onFailedFetchingBuys();
                }));
    }

    void destroySubscriptions() {
        if (compositeSubscription != null) compositeSubscription.unsubscribe();
    }
}
