package com.smallcase.android.user;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.onboarding.LandingActivity;
import com.smallcase.android.smallcase.discover.SmallcasesAdapter;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PExpandedSmallcase;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WatchlistActivity extends AppCompatActivity implements WatchlistContract.View {
    private static final String TAG = "WatchlistActivity";

    @BindView(R.id.watch_list) RecyclerView watchlist;
    @BindView(R.id.text_tool_bar_title) GraphikText toolbarTitle;
    @BindView(R.id.empty_state_container) View noWatchListContainer;
    @BindView(R.id.text_empty_state) GraphikText emptyStateText;
    @BindView(R.id.empty_state_icon) ImageView noWatchlistIcon;

    private SmallcasesAdapter smallcasesAdapter;
    private WatchlistContract.Service service;
    private String scidToBeRemoved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watchlist);

        ButterKnife.bind(this);

        toolbarTitle.setText("Watchlist");

        AnalyticsContract analyticsContract = new AnalyticsManager(getApplicationContext());
        watchlist.setLayoutManager(new LinearLayoutManager(this));
        watchlist.setHasFixedSize(true);
        smallcasesAdapter = new SmallcasesAdapter(this, null, analyticsContract, "Watchlist");
        watchlist.setAdapter(smallcasesAdapter);

        analyticsContract.sendEvent(null, "Viewed Watchlist", Analytics.MIXPANEL, Analytics.INTERCOM, Analytics.CLEVERTAP);
        service = new WatchlistService(new SharedPrefService(this), new SmallcaseRepository(), this);
        service.getWatchlistSmallcases();

        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.WATCHLIST_REMOVED_BROADCAST));
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.WATCHLIST_RE_ADDED_BROADCAST));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Clear previously saved scidToBeRemoved if any
        scidToBeRemoved = null;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart: " + scidToBeRemoved);
        if (scidToBeRemoved == null || smallcasesAdapter == null) {
            return;
        }
        // Remove scid if user's last action before he/she re-enters this activity is remove watchlist
        smallcasesAdapter.removeSmallcase(scidToBeRemoved);

        if ((smallcasesAdapter.getSmallcases() == null || smallcasesAdapter.getSmallcases().isEmpty())) {
            noWatchlist();
        }
    }

    @Override
    protected void onDestroy() {
        service.destroySubscriptions();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
        super.onDestroy();
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (smallcasesAdapter == null) return;

            Log.d(TAG, "onReceive: " + intent.getAction());
            if (AppConstants.WATCHLIST_REMOVED_BROADCAST.equals(intent.getAction())) {
                // Store scid of smallcase on receiving remove watchlist.
                // The smallcase is not immediately removed because it needs to be re-added and re-removed
                // if user continues to toggle watchlist. Instead the scid is stored and decision to remove or not
                // is taken onRestart
                scidToBeRemoved = intent.getStringExtra("scid");
            } else if (AppConstants.WATCHLIST_RE_ADDED_BROADCAST.equals(intent.getAction())) {
                // Remove scid when user re-adds the smallcase to watchlist
                scidToBeRemoved = null;
            }
        }
    };

    @Override
    public void showSmallcases(List<PExpandedSmallcase> smallcaseList) {
        smallcasesAdapter.onSmallcasesReceived(smallcaseList);
        smallcasesAdapter.noMoreSmallcases();
    }

    @Override
    public void noWatchlist() {
        noWatchListContainer.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(R.drawable.no_watchlist)
                .into(noWatchlistIcon);
        emptyStateText.setText("You are not watching any smallcases\n\nClick on the Watch icon to add & track the smallcase here before you buy it");
        watchlist.setVisibility(View.GONE);
    }

    @Override
    public void redirectToLogin() {
        startActivity(new Intent(this, LandingActivity.class));
        finish();
    }
}
