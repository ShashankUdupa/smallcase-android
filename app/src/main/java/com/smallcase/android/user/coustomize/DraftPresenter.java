package com.smallcase.android.user.coustomize;

import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Constituents;
import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.SmallcaseCompositionScheme;
import com.smallcase.android.view.model.Stock;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by shashankm on 29/05/17.
 */

class DraftPresenter implements DraftContract.Presenter {
    private static final String TAG = "DraftPresenter";

    private DraftContract.View view;
    private DraftContract.Service service;
    private NetworkHelper networkHelper;
    private List<DraftSmallcase> draftSmallcases;
    private List<Stock> stockList;
    private DraftSmallcase selectedSmallcaseToBuy;
    private AppConstants.MarketStatus marketStatus = AppConstants.MarketStatus.NOT_FETCHED;

    DraftPresenter(UserSmallcaseRepository userSmallcaseRepository, SharedPrefService sharedPrefService, DraftContract.View view,
                   NetworkHelper networkHelper, MarketRepository marketRepository) {
        this.service = new DraftService(userSmallcaseRepository, sharedPrefService, this, marketRepository);
        this.view = view;
        this.networkHelper = networkHelper;
        if ("leprechaun".equals(sharedPrefService.getUserInfo().get("brokerName"))) {
            marketStatus = AppConstants.MarketStatus.OPEN;
        } else {
            service.getIfMarketIsOpen();
        }
    }

    @Override
    public void getDraftSmallcases() {
        if (!networkHelper.isNetworkAvailable()) {
            view.showSnackBar(R.string.no_internet);
            return;
        }

        service.getDraftSmallcases();
    }

    @Override
    public void onDraftSmallcasesFetched(List<DraftSmallcase> draftSmallcases) {
        if (draftSmallcases.isEmpty()) {
            view.showNoDraftsAvailable();
            return;
        }

        this.draftSmallcases = new ArrayList<>(draftSmallcases);
        Collections.reverse(this.draftSmallcases);
        view.onDraftsReceived();
    }

    @Override
    public void onFailedFetchingDraftSmallcases() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public List<DraftSmallcase> getDraftSmallcasesList() {
        return draftSmallcases;
    }

    @Override
    public void createStocksList(DraftSmallcase draftSmallcase) {
        if (stockList == null) {
            stockList = new ArrayList<>();
        } else {
            stockList.clear();
        }

        selectedSmallcaseToBuy = draftSmallcase;

        List<Constituents> constituentsList = draftSmallcase.getConstituents();
        for (Constituents constituent : constituentsList) {
            if (constituent.getWeight() <= 0) continue;

            Stock stock = new Stock();
            stock.setBuy(true);
            stock.setWeight(constituent.getWeight());
            stock.setSid(constituent.getSid());
            stock.setStockName(constituent.getSidInfo().getName());
            stock.setShares(constituent.getShares());
            stockList.add(stock);
        }

        checkMarketStatus();
    }

    @Override
    public void checkMarketStatus() {
        switch (marketStatus) {
            case OPEN:
                view.marketIsOpen();
                break;

            case CLOSED:
                view.showSnackBar(R.string.market_closed_now);
                break;

            case NOT_FETCHED:
                service.getIfMarketIsOpen();
                view.showSnackBar(R.string.something_wrong);
                break;
        }
    }

    @Override
    public List<Stock> getStocksList() {
        return stockList;
    }

    @Override
    public DraftSmallcase getSelectedSmallcase() {
        return selectedSmallcaseToBuy;
    }

    @Override
    public void onMarketStatusReceived(ResponseBody responseBody) {
        try {
            JSONObject response = new JSONObject(responseBody.string());
            if (response.getString("data").equals("closed")) {
                marketStatus = AppConstants.MarketStatus.CLOSED;
            } else {
                marketStatus = AppConstants.MarketStatus.OPEN;
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
            SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; onMarketStatusReceived()");
            view.showSnackBar(R.string.something_wrong);
        }
    }

    @Override
    public void onFailedFetchingMarketStatus() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void getDraftSmallcase(String did) {
        if (marketStatus == AppConstants.MarketStatus.CLOSED) {
            view.showSnackBar(R.string.market_closed_now);
            return;
        }

        service.getDraftSmallcase(did);
    }

    @Override
    public void onDraftSmallcaseFetched(DraftSmallcase draftSmallcase) {
        createStocksList(draftSmallcase);
    }

    @Override
    public void onFailedFetchingDraftSmallcase() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public boolean isDraftShareBased() {
        return SmallcaseCompositionScheme.SHARES.equals(selectedSmallcaseToBuy.getCompositionScheme());
    }
}
