package com.smallcase.android.user.order;

import com.smallcase.android.R;
import com.smallcase.android.data.model.Order;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.OrderLabel;
import com.smallcase.android.util.OrderStatus;
import com.smallcase.android.view.model.PBatch;
import com.smallcase.android.view.model.POrder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by shashankm on 07/04/17.
 */

class OrderDetailsPresenter implements OrderDetailsContract.Presenter {
    private NetworkHelper networkHelper;
    private OrderDetailsContract.View view;
    private OrderDetailsContract.Service service;

    OrderDetailsPresenter(NetworkHelper networkHelper, OrderDetailsContract.View view, SharedPrefService sharedPrefService,
                          UserSmallcaseRepository userSmallcaseRepository) {
        this.networkHelper = networkHelper;
        this.view = view;
        this.service = new OrderDetailsService(sharedPrefService, this, userSmallcaseRepository);
    }

    @Override
    public void getOrderDetails() {
        if (!networkHelper.isNetworkAvailable()) {
            view.showSnackBar(R.string.no_internet);
            return;
        }

        service.getOrderDetails();
    }

    @Override
    public void onOrderDetailsFetched(List<Order> orders) {
        if (orders.isEmpty()) {
            view.showNoOrders();
            return;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM", Locale.getDefault());
        List<POrder> orderList = new ArrayList<>();
        for (Order order : orders) {
            POrder pOrder = new POrder();
            pOrder.setSource(order.getSource());
            pOrder.setiScid(order.get_id());
            pOrder.setScid(order.getScid());
            pOrder.setSmallcaseName(order.getName());
            pOrder.setSmallcaseImage("80");

            PBatch pBatch = new PBatch();
            String batchType;
            switch (order.getLastBatch().getLabel()) {
                case OrderLabel.BUY:
                    batchType = "Buy";
                    break;

                case OrderLabel.FIX:
                    batchType = "Repair";
                    break;

                case OrderLabel.INVEST_MORE:
                    batchType = "Invest More";
                    break;

                case OrderLabel.MANAGE:
                    batchType = "Manage";
                    break;

                case OrderLabel.RE_BALANCE:
                    batchType = "Rebalance";
                    break;

                case OrderLabel.SELL_ALL:
                    batchType = "Exit";
                    break;

                case OrderLabel.SIP:
                    batchType = "SIP";
                    break;

                case OrderLabel.PARTIAL_EXIT:
                    batchType = "Partial Exit";
                    break;

                default:
                    batchType = order.getLastBatch().getLabel();
                    break;
            }
            pBatch.setBatchType(batchType);

            String status;
            int remainingOrders = (int) (order.getLastBatch().getQuantity() - order.getLastBatch().getFilled());
            String fillerText;
            switch (order.getLastBatch().getStatus()) {
                case OrderStatus.COMPLETED:
                    status = "Filled";
                    fillerText = " filled";
                    break;

                case OrderStatus.MARKED_COMPLETE:
                    status = "Archived";
                    fillerText = " filled, " + remainingOrders + " archived";
                    break;

                case OrderStatus.FIXED:
                    status = "Repaired";
                    fillerText = " filled, " + remainingOrders + " archived";
                    break;

                case OrderStatus.PARTIALLY_FILLED:
                case OrderStatus.PARTIALLY_PLACED:
                case OrderStatus.UNFILLED:
                    status = "Partial";
                    fillerText = " filled, " + remainingOrders + " unfilled";
                    break;

                case OrderStatus.PLACED:
                    status = "Placed";
                    fillerText = " placed";
                    break;

                default:
                    status = "Unknown";
                    fillerText = "";
                    break;

            }
            pBatch.setOrderStatus(status);

            pBatch.setFilledVsQuantity(((int) order.getLastBatch().getFilled() + "/" + (int)order.getLastBatch().getQuantity()) + fillerText);

            pBatch.setPercentFilled((int) ((order.getLastBatch().getFilled() / order.getLastBatch().getQuantity()) * 100));
            pBatch.setPlacedOn(sdf.format(AppUtils.getInstance().getPlainDate(order.getLastBatch().getDate())));

            List<PBatch> batches = new ArrayList<>();
            batches.add(pBatch);
            pOrder.setBatches(batches);
            orderList.add(pOrder);
        }
        view.onOrdersReceived(orderList);
    }

    @Override
    public void onFailedFetchingOrderDetails() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void destroySubscriptions() {
        service.destroySubscriptions();
    }
}
