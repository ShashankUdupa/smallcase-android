package com.smallcase.android.user.order;

import android.util.Log;

import com.google.gson.Gson;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Order;
import com.smallcase.android.data.model.SipDetails;
import com.smallcase.android.data.model.SmallcaseOrder;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.sentry.event.Breadcrumb;
import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 07/03/17.
 */

class ReviewOrderService implements ReviewOrderContract.Service {
    private static final String TAG = "ReviewOrderService";
    private MarketRepository marketRepository;
    private SharedPrefService sharedPrefService;
    private ReviewOrderContract.Presenter presenter;
    private String scid;
    private UserSmallcaseRepository userSmallcaseRepository;
    private CompositeSubscription compositeSubscription;

    ReviewOrderService(MarketRepository marketRepository, SharedPrefService sharedPrefService, ReviewOrderContract
            .Presenter presenter, String scid, UserSmallcaseRepository userSmallcaseRepository) {
        this.marketRepository = marketRepository;
        this.sharedPrefService = sharedPrefService;
        this.userSmallcaseRepository = userSmallcaseRepository;
        this.presenter = presenter;
        this.scid = scid;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void placeOrder(final SmallcaseOrder smallcaseOrder) {
        final HashMap<String, String> data = new HashMap<>();
        data.put("did", smallcaseOrder.getDid());
        data.put("iScid", smallcaseOrder.getIscid());
        data.put("label", smallcaseOrder.getLabel());
        data.put("scid", smallcaseOrder.getScid());
        data.put("source", smallcaseOrder.getSource());
        data.put("ordersSize", String.valueOf(smallcaseOrder.getOrders().size()));
        data.put("version", String.valueOf(smallcaseOrder.getVersion()));
        compositeSubscription.add(userSmallcaseRepository.placeOrder(sharedPrefService.getAuthorizationToken(), smallcaseOrder, sharedPrefService.getCsrfToken())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        try {
                            JSONObject response = new JSONObject(responseBody.string());
                            if (response.getBoolean("success")) {
                                presenter.onOrdersPlaced(response);
                                return;
                            }
                            presenter.onFailedPlacingOrder(new JSONException("Could not place order, api success is false"));
                        } catch (JSONException | IOException e) {
                            SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.USER, "placeOrder", data);
                            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; placeOrder()");
                            e.printStackTrace();
                            presenter.onFailedPlacingOrder(e);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedPlacingOrder(throwable);
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.USER, "placeOrder", data);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; placeOrder()");
                    }
                }));
    }

    @Override
    public void getOrderDetails(final String iScid, final String batchId) {
        compositeSubscription.add(userSmallcaseRepository.getOrderDetails(sharedPrefService.getAuthorizationToken(), iScid, batchId, sharedPrefService.getCsrfToken(), false, false)
                .subscribe(new Action1<List<Order>>() {
                    @Override
                    public void call(List<Order> orders) {
                        presenter.onOrderDetailsFetched(orders);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedFetchingOrderDetails();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        HashMap<String, String> data = new HashMap<>();
                        data.put("batchId", batchId);
                        data.put("iScid", iScid);
                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.USER, "getOrderDetails", data);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getOrderDetails()");
                    }
                }));
    }

    @Override
    public void cancelBatch(final HashMap<String, String> body) {
        final HashMap<String, String> data = new HashMap<>();
        data.put("batchId", body.get("batchId"));
        data.put("iScid", body.get("iscid"));
        compositeSubscription.add(userSmallcaseRepository.cancelBatch(sharedPrefService.getAuthorizationToken(), body, sharedPrefService.getCsrfToken())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        try {
                            JSONObject response = new JSONObject(responseBody.string());
                            if (response.getBoolean("success")) {
                                presenter.onBatchCanceledSuccessfully();
                                HashSet<String> scids = new HashSet<>(sharedPrefService.getInvestedScidsWithIScids());
                                if (!scids.contains(scid + "#" + body.get("iscid"))) {
                                    scids.add(scid + "#" + body.get("iscid"));
                                    sharedPrefService.saveInvestedScidWithIscid(scids);
                                }
                                sharedPrefService.saveInvestedScidWithIscid(scids);
                                return;
                            }
                            presenter.onFailedCancelingBatch();
                        } catch (JSONException | IOException e) {
                            SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.USER, "placeOrder", data);
                            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; cancelBatch()");

                            e.printStackTrace();
                            presenter.onFailedCancelingBatch();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedCancelingBatch();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.USER, "placeOrder", data);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; cancelBatch()");
                    }
                }));
    }

    @Override
    public void fixBatch(HashMap<String, String> body) {
        final HashMap<String, String> data = new HashMap<>();
        data.put("batchId", body.get("batchId"));
        data.put("iScid", body.get("iscid"));
        compositeSubscription.add(userSmallcaseRepository.fixBatch(sharedPrefService.getAuthorizationToken(), body, sharedPrefService.getCsrfToken())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        try {
                            JSONObject response = new JSONObject(responseBody.string());
                            if (response.getBoolean("success")) {
                                presenter.onBatchFixedSuccessfully(response);
                                return;
                            }
                            JSONException jsonException = new JSONException("Success false while fixing batch");
                            presenter.onFailedFixingBatch(jsonException);

                            SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.USER, "placeOrder", data);
                            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), jsonException, TAG + "; fixBatch()");
                        } catch (JSONException | IOException e) {
                            data.put("Type", "Parse error");
                            SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.USER, "placeOrder", data);
                            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; fixBatch()");

                            e.printStackTrace();
                            presenter.onFailedFixingBatch(e);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedFixingBatch(throwable);
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.USER, "placeOrder", data);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; fixBatch()");
                    }
                }));
    }

    @Override
    public void getIfMarketIsOpen() {
        compositeSubscription.add(marketRepository.getIfMarketIsOpen(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        presenter.onMarketStatusReceived(responseBody);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedFetchingMarketStatus();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getIfMarketIsOpen()");
                    }
                }));
    }

    @Override
    public void saveScid(String iScid) {
        HashSet<String> scids = new HashSet<>(sharedPrefService.getInvestedScidsWithIScids());
        if (!scids.contains(scid + "#" + iScid)) {
            scids.add(scid + "#" + iScid);
            sharedPrefService.saveInvestedScidWithIscid(scids);
        }
    }

    @Override
    public void removeScid(String iScid) {
        HashSet<String> scids = new HashSet<>(sharedPrefService.getInvestedScidsWithIScids());
        if (scids.contains(scid + "#" + iScid)) {
            scids.remove(scid + "#" + iScid);
            for (String s : scids) {
                Log.d(TAG, "after: " + s);
            }
            sharedPrefService.saveInvestedScidWithIscid(scids);
        }
    }

    @Override
    public void addMarketOpenReminder(HashMap<String, String> body) {
        compositeSubscription.add(userSmallcaseRepository.addReminder(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken(), body)
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; addMarketOpenReminder()");
                    }
                }));
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }

    @Override
    public void addExited(String iScid) {
        HashSet<String> scids = new HashSet<>(sharedPrefService.getExitedIScids());
        scids.add(iScid);
        sharedPrefService.cacheExitedIScids(scids);
    }

    @Override
    public void removeSipDue(String iScid) {
        Gson gson = new Gson();
        Set<String> sipUpdates = sharedPrefService.getCachedSipUpdates();
        HashSet<String> updatedSips = new HashSet<>();
        for (String sipUpdate : sipUpdates) {
            SipDetails sipDetails = gson.fromJson(sipUpdate, SipDetails.class);
            if (!iScid.equals(sipDetails.getIscid())) updatedSips.add(sipUpdate);
        }
        sharedPrefService.cacheSipUpdates(updatedSips);
    }
}
