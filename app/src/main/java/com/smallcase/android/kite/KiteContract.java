package com.smallcase.android.kite;

import android.support.annotation.Nullable;

/**
 * Created by shashankm on 22/03/17.
 */
public interface KiteContract {
    interface Request {
        void checkSufficientFunds(final double requiredAmount, final KiteContract.Response response);
    }

    interface FundRequest {
        void getFunds(FundResponse fundResponse);
    }

    interface FundResponse {
        void onFundsReceived(double availableFunds);

        void onFailedFetchingFunds(@Nullable Throwable throwable);
    }

    interface Response {
        void onFailedFetchingFunds(@Nullable Throwable throwable);

        void sufficientFundsAvailable();

        void needMoreFunds(double availableFunds);
    }
}
