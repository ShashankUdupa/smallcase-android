package com.smallcase.android.kite;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.SharedPrefService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import io.sentry.event.Breadcrumb;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by shashankm on 22/03/17.
 */

public class KiteHelper implements KiteContract.Request, KiteContract.FundRequest {
    private static final String TAG = "KiteHelper";

    private KiteRepository kiteRepository;
    private SharedPrefService sharedPrefService;

    public KiteHelper(SharedPrefService sharedPrefService, KiteRepository kiteRepository) {
        this.sharedPrefService = sharedPrefService;
        this.kiteRepository = kiteRepository;
    }

    @Override
    public void checkSufficientFunds(final double requiredAmount, final KiteContract.Response response) {
        final Map<String, String> data = new HashMap<>();
        data.put("requiredAmount", String.valueOf(requiredAmount));
        kiteRepository.getAvailableFunds(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(responseBody -> {
                    try {
                        JSONObject responseObj = new JSONObject(responseBody.string());
                        double availableFunds = responseObj.getJSONObject("data").getDouble("net");
                        if (availableFunds >= requiredAmount) {
                            response.sufficientFundsAvailable();
                            return;
                        }
                        response.needMoreFunds(availableFunds);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();

                        data.put("response", responseBody.toString());
                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.USER, "Failed parsing funds", data);
                        SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; checkSufficientFunds() ");

                        response.onFailedFetchingFunds(null);
                    }
                }, throwable -> {
                    response.onFailedFetchingFunds(throwable);
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed in funds", data);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; checkSufficientFunds()");
                });
    }

    @Override
    public void getFunds(final KiteContract.FundResponse fundResponse) {
        kiteRepository.getAvailableFunds(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(responseBody -> {
                    try {
                        JSONObject response = new JSONObject(responseBody.string());
                        double availableFunds = response.getJSONObject("data").getDouble("net");
                        fundResponse.onFundsReceived(availableFunds);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();

                        Map<String, String> data = new HashMap<>();
                        data.put("response", responseBody.toString());
                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.USER, "Failed in funds", data);
                        SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; getFunds()");

                        fundResponse.onFailedFetchingFunds(null);
                    }
                }, throwable -> {
                    fundResponse.onFailedFetchingFunds(throwable);
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getFunds()");
                });
    }
}
