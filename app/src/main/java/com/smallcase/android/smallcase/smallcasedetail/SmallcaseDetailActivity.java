package com.smallcase.android.smallcase.smallcasedetail;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.create.CreateInfoActivity;
import com.smallcase.android.create.CreateSmallcaseActivity;
import com.smallcase.android.data.model.Constituents;
import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.model.Info;
import com.smallcase.android.data.model.ReBalanceUpdate;
import com.smallcase.android.data.model.Segments;
import com.smallcase.android.data.model.SidInfo;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.onboarding.LandingActivity;
import com.smallcase.android.onboarding.LoginActivity;
import com.smallcase.android.rebalance.ReBalanceActivity;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.stock.StockInfo;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.SmallcaseCompositionScheme;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.util.SmallcaseTier;
import com.smallcase.android.view.adapter.NewsAdapter;
import com.smallcase.android.view.android.CustomTabLayout;
import com.smallcase.android.view.android.CustomTypefaceSpan;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.android.WrapContentViewPager;
import com.smallcase.android.view.buy.InvestAmountPopUp;
import com.smallcase.android.view.buy.InvestContract;
import com.smallcase.android.view.model.PPlainNews;
import com.smallcase.android.view.model.PPoints;
import com.smallcase.android.view.model.PSegment;
import com.smallcase.android.view.model.PSmallcaseDetail;
import com.smallcase.android.view.model.PSmallcaseInvestment;
import com.smallcase.android.view.model.PStockWeight;
import com.smallcase.android.view.model.Stock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import io.sentry.event.Breadcrumb;

import static com.smallcase.android.onboarding.LoginActivity.REQUEST_TOKEN_KEY;

public class SmallcaseDetailActivity extends AppCompatActivity implements SmallcaseDetailContract.View,
        InvestContract.Response, SwipeRefreshLayout.OnRefreshListener {
    public static final String SCID = "scid"; // required
    public static final String TITLE = "title";
    public static final String OPEN_BUY = "open_buy";
    public static final String DID = "did";
    public static final String SIDS = "sids";
    public static final String ACCESSED_FROM = "accessed_from"; // required

    private static final String TAG = "SmallcaseDetailActivity";
    private static final int LOGIN_REQUEST_CODE = 2;

    @BindView(R.id.text_tool_bar_title)
    GraphikText toolBarTitle;
    @BindView(R.id.loading_indicator)
    View loadingIndicator;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.primary_action_container)
    CardView buyWatchlistContainer;
    @BindView(R.id.smallcase_image)
    ImageView smallcaseImage;
    @BindView(R.id.smallcase_name)
    GraphikText smallcaseName;
    @BindView(R.id.smallcase_description)
    GraphikText smallcaseDescription;
    @BindView(R.id.type)
    GraphikText smallcaseType;
    @BindView(R.id.index)
    GraphikText index;
    @BindView(R.id.index_up_or_down)
    ImageView indexUpOrDown;
    @BindView(R.id.one_month_return)
    GraphikText oneMonthReturn;
    @BindView(R.id.one_year_return)
    GraphikText oneYearReturn;
    @BindView(R.id.minimum_investment_amount)
    GraphikText minInvestmentAmount;
    @BindView(R.id.negative_container)
    View negativeContainer;
    @BindView(R.id.negative_title)
    GraphikText negativeTitle;
    @BindView(R.id.negative_reason)
    GraphikText negativeReason;
    @BindView(R.id.invested_container)
    View investedContainer;
    @BindView(R.id.brought_on)
    GraphikText broughtOn;
    @BindView(R.id.p_and_l)
    GraphikText pAndL;
    @BindView(R.id.rationale)
    GraphikText rationale;
    @BindView(R.id.tab_layout)
    CustomTabLayout tabLayout;
    @BindView(R.id.news_list)
    RecyclerView newsList;
    @BindView(R.id.buy_smallcase)
    GraphikText buySmallcase;
    @BindView(R.id.view_pager)
    WrapContentViewPager viewPager;
    @BindView(R.id.news_container)
    View newsContainer;
    @BindView(R.id.rationale_arrow)
    View rationaleArrow;
    @BindView(R.id.activity_smallcase_detail)
    CoordinatorLayout parentLayout;
    @BindView(R.id.action_icon)
    ImageView actionIcon;
    @BindView(R.id.invest_amount_blur)
    View investAmountBlur;
    @BindView(R.id.negative_layout_divider)
    View negativeLayoutDivider;
    @BindView(R.id.smallcase_meta_info_container)
    View smallcaseMetaInfoContainer;
    @BindView(R.id.smallcase_meta_info)
    GraphikText smallcaseMetaInfo;
    @BindView(R.id.arrow)
    View arrow;
    @BindView(R.id.premium_label)
    ImageView premiumLabel;
    @BindView(R.id.customize_info_container)
    View customizeInfoContainer;
    @BindView(R.id.got_customize_info)
    View gotCustomizeInfo;
    @BindView(R.id.dont_show_again)
    AppCompatCheckBox dontShowAgain;
    @BindView(R.id.read_rationale_container)
    View readRationaleContainer;
    @BindView(R.id.delete_draft_card)
    View deleteDraftCard;
    @BindView(R.id.dialog_confirmation)
    View dialogConformation;
    @BindView(R.id.custom_tag)
    View customTag;

    private SmallcaseDetailContract.Presenter presenter;
    private NewsAdapter newsAdapter;
    private String[] durations = new String[]{"1m", "3m", "6m", "1y", "2y"};
    private PerformanceFragment performanceFragment;
    private StocksFragment stocksFragment;
    private boolean isWatchlisted = false, isSmallcasePlayedOut = false, shouldOpenBuy;
    private StockInfo stockInfo;
    private String title, scid, did;
    private InvestContract.View investContract;
    private AnalyticsContract analyticsContract;
    private Animations animations;
    private SpannableString smallcaseInceptionInfo;
    private List<String> sids;
    private ProgressDialog progressDialog;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smallcase_detail);

        ButterKnife.bind(this);

        sids = getIntent().getStringArrayListExtra(SIDS);
        did = getIntent().getStringExtra(DID);
        title = getIntent().getStringExtra(TITLE);
        scid = getIntent().getStringExtra(SCID);
        shouldOpenBuy = getIntent().getBooleanExtra(OPEN_BUY, false);

        try {
            if ((scid == null || title == null || scid.equals("") || title.equals("")) && (getIntent().getData() != null)) {
                if (getIntent().getData().getPathSegments() != null && getIntent().getData()
                        .getPathSegments().size() > 1) {
                    scid = getIntent().getData().getPathSegments().get(1);
                }
            }
        } catch (UnsupportedOperationException | NullPointerException | ArrayIndexOutOfBoundsException e) {
            if (getIntent().getData() != null) {
                HashMap<String, String> url = new HashMap<>();
                url.put("url", getIntent().getData().toString());
                SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Link", url);
            }
            SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; onCreate()");
        }

        if (null != title) {
            toolBarTitle.setText(title);
        }

        actionIcon.setVisibility(View.VISIBLE);
        newsList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        newsList.setHasFixedSize(true);
        newsList.setNestedScrollingEnabled(false);

        if (Build.VERSION.SDK_INT < 21) {
            buyWatchlistContainer.setCardElevation(0);
            buyWatchlistContainer.setMaxCardElevation(0);
        }

        animations = new Animations();
        analyticsContract = new AnalyticsManager(getApplicationContext());
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.blue_800));

        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(GradientDrawable.RECTANGLE);
        gradientDrawable.setCornerRadius(AppUtils.getInstance().dpToPx(2f));
        gradientDrawable.setColor(ContextCompat.getColor(this, R.color.blue_grey_100));
        buySmallcase.setBackground(gradientDrawable);

        setUpViewPager();
        tabLayout.setupWithViewPager(viewPager);

        MarketRepository marketRepository = new MarketRepository();
        NetworkHelper networkHelper = new NetworkHelper(this);
        SharedPrefService sharedPrefService = new SharedPrefService(this);
        StockHelper stockHelper = new StockHelper();
        KiteRepository kiteRepository = new KiteRepository();
        presenter = new SmallcaseDetailPresenter(this, new SmallcaseRepository(), sharedPrefService, this, networkHelper,
                marketRepository, stockHelper, new UserRepository(), scid, analyticsContract, new UserSmallcaseRepository(), did != null);
        stockInfo = new StockInfo(this, stockHelper, marketRepository, sharedPrefService);
        // Presenter object should be created first before investContract to access isInvested method
        investContract = new InvestAmountPopUp(this, sharedPrefService, marketRepository, stockHelper, kiteRepository,
                this, presenter.isInvested(), analyticsContract);

        if (did != null && sids != null) {
            presenter.getDraftDetailData(did, sids);
        } else {
            presenter.getSmallcaseDetailData();
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.PLACED_ORDER_BROADCAST));
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.HARD_RELOAD_BROADCAST));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Map<String, Object> eventProperties = new HashMap<>();
                eventProperties.put("smallcaseName", title);
                eventProperties.put("smallcaseType", smallcaseType.getText().toString());
                if (tab.getPosition() == 1) {
                    analyticsContract.sendEvent(eventProperties, "Viewed Stocks & Weights", Analytics.MIXPANEL);
                } else if (tab.getPosition() == 0) {
                    analyticsContract.sendEvent(eventProperties, "Viewed Past Performance", Analytics.MIXPANEL);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (stockInfo.isBottomSheetOpen()) {
            stockInfo.setBottomSheetState(BottomSheetBehavior.STATE_HIDDEN);
            return;
        }

        if (investContract.isInvestCardVisible()) {
            investContract.hideInvestCard();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        investContract.unBind();
        stockInfo.unBind();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
        presenter.destroySubscriptions();
        if (snackbar != null) snackbar.dismiss();
        super.onDestroy();
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @OnClick(R.id.premium_label)
    public void onPremiumLabelClicked() {
        animations.scaleIntoPlaceAnimation(smallcaseMetaInfoContainer);
        investAmountBlur.setVisibility(View.VISIBLE);
        investAmountBlur.setOnClickListener(v -> animations.scaleOutAnimation(smallcaseMetaInfoContainer, investAmountBlur));

        String question = "What is a Select smallcase?";
        SpannableString spannableString = new SpannableString(getSmallcaseName() + " is a select smallcase\n\n" + question
                + "\n\nA Select smallcase is a new type of smallcase that is built by cherry picking the common stocks from all available smallcases\n\n"
                + "You will be charged a one-time flat smallcase fee of " + AppConstants.RUPEE_SYMBOL + "200 + GST on buying a Select smallcase");
        spannableString.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM)),
                getSmallcaseName().length() + 24, getSmallcaseName().length() + 24 + question.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        smallcaseMetaInfo.setText(spannableString);
    }

    @OnClick(R.id.buy_smallcase)
    public void buySmallcase() {
        if (!presenter.isSmallcaseValid()) return;

        if (presenter.isReBalancePending()) {
            ReBalanceUpdate reBalanceUpdate = presenter.getReBalanceUpdate();
            Intent intent = new Intent(this, ReBalanceActivity.class);
            intent.putExtra(ReBalanceActivity.SOURCE, SmallcaseSource.PROFESSIONAL);
            intent.putExtra(ReBalanceActivity.VERSION, reBalanceUpdate.getVersion());
            intent.putExtra(ReBalanceActivity.LABEL, reBalanceUpdate.getLabel());
            intent.putExtra(ReBalanceActivity.DATE, reBalanceUpdate.getDate());
            intent.putExtra(ReBalanceActivity.ISCID, reBalanceUpdate.getIscid());
            intent.putExtra(ReBalanceActivity.SCID, reBalanceUpdate.getScid());
            intent.putExtra(ReBalanceActivity.TITLE, reBalanceUpdate.getName());
            startActivity(intent);
            return;
        }

        presenter.checkMarketStatus();
    }

    @OnClick(R.id.index_container)
    public void indexContainer() {
        animations.scaleIntoPlaceAnimation(smallcaseMetaInfoContainer);
        investAmountBlur.setVisibility(View.VISIBLE);
        investAmountBlur.setOnClickListener(v -> animations.scaleOutAnimation(smallcaseMetaInfoContainer, investAmountBlur));
        if (smallcaseInceptionInfo != null) smallcaseMetaInfo.setText(smallcaseInceptionInfo);
    }

    @OnClick(R.id.action_icon)
    public void toggleWatchlist() {
        if (presenter.isDraft()) {
            if (presenter.getDraftSmallcase() == null) {
                showSimpleSnackbar(R.string.something_wrong);
                return;
            }

            Intent intent = new Intent(this, CreateInfoActivity.class);
            intent.putExtra(CreateInfoActivity.IS_EDIT, true);
            intent.putExtra(CreateInfoActivity.DRAFT_SMALLCASE, presenter.getDraftSmallcase());
            startActivity(intent);
            return;
        }

        isWatchlisted = !isWatchlisted;

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", title);
        eventProperties.put("smallcaseType", smallcaseType.getText().toString());
        eventProperties.put("addedToWatchlist", isWatchlisted);
        eventProperties.put("isInvested", presenter.isInvested());
        eventProperties.put("smallcaseTier", presenter.isSmallcasePremium()? SmallcaseTier.PREMIUM : SmallcaseTier.BASIC);
        analyticsContract.sendEvent(eventProperties, "Watchlist Toggled", Analytics.MIXPANEL, Analytics.INTERCOM, Analytics.CLEVERTAP);

        presenter.toggleWatchlist(isWatchlisted);
        actionIcon.setImageResource(isWatchlisted ? R.drawable.watchlist_selected : R.drawable.watchlist);
        showSimpleSnackbar(isWatchlisted ? R.string.added_to_watclist : R.string.removed_from_watchlist);

        // Send watchlist added/removed broadcast
        Intent intent = new Intent(isWatchlisted ? AppConstants.WATCHLIST_RE_ADDED_BROADCAST : AppConstants.WATCHLIST_REMOVED_BROADCAST);
        intent.putExtra("scid", scid);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        if (did != null && sids != null) {
            presenter.getDraftDetailData(did, sids);
        } else {
            presenter.getSmallcaseDetailData();
        }
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (AppConstants.PLACED_ORDER_BROADCAST.equals(action)) {
                if (!isFinishing()) finish();
            }

            if (AppConstants.HARD_RELOAD_BROADCAST.equals(action)) {
                if (!isFinishing()) {
                    swipeRefreshLayout.setVisibility(View.GONE);
                    loadingIndicator.setVisibility(View.VISIBLE);
                    buyWatchlistContainer.setVisibility(View.GONE);
                    presenter.getSmallcaseDetailData();
                }
            }

        }
    };

    @Override
    public void hideBlur() {
        investAmountBlur.setClickable(false);
        investAmountBlur.setVisibility(View.GONE);
    }

    @Override
    public void showMarketClosed() {
        if (presenter.isInvested()) {
            showSimpleSnackbar(R.string.market_closed_now);
            return;
        }

        final Snackbar snackbar = Snackbar.make(parentLayout, R.string.market_closed, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.remind, v -> {
            snackbar.dismiss();

            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("smallcaseName", title);
            eventProperties.put("smallcaseType", smallcaseType.getText().toString());
            eventProperties.put("reminderName", "Buy");
            analyticsContract.sendEvent(eventProperties, "Set Reminder", Analytics.MIXPANEL);

            presenter.addReminder();
            showSimpleSnackbar(R.string.reminder_added);
        }).show();
    }

    @Override
    public void setInitialMinInvestmentAmount(SpannableString minInvestAmount) {
        if (!presenter.isReBalancePending()) minInvestmentAmount.setText(minInvestAmount);
    }

    @Override
    public void hideStockBlurIfVisible() {
        if (stocksFragment != null) {
            stocksFragment.hideStocksBlurIfVisible();
        }
    }

    @Override
    public void showStocksBlur() {
        if (stocksFragment != null) {
            stocksFragment.showStocksBlur();
        }
    }

    @Override
    public void sendUserToLoginOrSignUp() {
        Intercom.client().reset();
        new SharedPrefService(this).clearSharedPreference();
        startActivity(new Intent(this, LandingActivity.class));
        finish();
    }

    @Override
    public void showWatchlistTutorial() {
        TapTargetView.showFor(this,
                TapTarget.forView(actionIcon, "Watch this smallcase", "Click here to add this smallcase to " +
                        "your watchlist\n\nYou can access your Watchlist from your Profile")
                        .textTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR))
                        .descriptionTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .cancelable(true)
                        .drawShadow(true)
                        .outerCircleColor(R.color.blue_800)
                        .outerCircleAlpha(0.9f)
                        .targetCircleColor(R.color.white)
                        .transparentTarget(false)
                        .targetRadius(24)
        );
    }

    @Override
    public void showIndexTutorial() {
        TapTargetView.showFor(this,
                TapTarget.forView(index, "This is the index value of a smallcase", "It shows the performance of a smallcase from " +
                        "its inception date, the green/red represents the day’s change\n\nTap on the index value to see more details")
                        .textTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR))
                        .descriptionTextColor(R.color.white)
                        .descriptionTextSize(14)
                        .cancelable(true)
                        .outerCircleColor(R.color.colorPrimary)
                        .outerCircleAlpha(1f)
                        .transparentTarget(false));
    }

    @Override
    public void showReBalancePending(SpannableString label) {
        buySmallcase.setText("VIEW UPDATE");
        minInvestmentAmount.setText(label);
    }

    @Override
    public void showPremium() {
        premiumLabel.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(R.drawable.premium_label)
                .into(premiumLabel);
    }

    @Override
    public void onDraftDetailsReceived(PSmallcaseDetail smallcaseDetail) {
        swipeRefreshLayout.setRefreshing(false);

        actionIcon.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(R.drawable.edit)
                .into(actionIcon);

        loadingIndicator.setVisibility(View.GONE);
        smallcaseType.setVisibility(View.GONE);
        buyWatchlistContainer.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
        readRationaleContainer.setVisibility(View.GONE);

        if (!isFinishing()) {
            Glide.with(this)
                    .load(smallcaseDetail.getSmallcaseImage())
                    .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
                    .placeholder(getResources().getDrawable(R.color.grey_300))
                    .into(smallcaseImage);
        }

        smallcaseName.setText(smallcaseDetail.getSmallcaseName());
        smallcaseDescription.setText(smallcaseDetail.getSmallcaseDescription());
        index.setText(smallcaseDetail.getSmallcaseIndex());
        Glide.with(this)
                .load(smallcaseDetail.isIndexUp() ? R.drawable.up_arrow : R.drawable.down_arrow_red)
                .into(indexUpOrDown);

        smallcaseInceptionInfo = smallcaseDetail.getSmallcaseInceptionInfo();

        if (SmallcaseSource.CUSTOM.equals(smallcaseDetail.getSource())) {
            customTag.setVisibility(View.VISIBLE);
        } else {
            customTag.setVisibility(View.GONE);
        }

        oneMonthReturn.setText(smallcaseDetail.getOnMonthReturn());
        oneYearReturn.setText(smallcaseDetail.getOneYearReturn());

        int red = R.color.red_600;
        int green = R.color.green_600;
        oneMonthReturn.setTextColor(ContextCompat.getColor(this, smallcaseDetail.isOneMonthPositive() ? green : red));
        oneYearReturn.setTextColor(ContextCompat.getColor(this, smallcaseDetail.isOneYearPositive() ? green : red));

        buySmallcase.setText("BUY SMALLCASE");
        deleteDraftCard.setVisibility(View.VISIBLE);

        stocksFragment.onDataReceived(smallcaseDetail.getRebalanceDetail(), smallcaseDetail.getSegmentMap());
        stocksFragment.hideLastReBalanceCard();
    }

    @OnClick(R.id.delete_draft_card)
    public void onDeleteDraftClicked() {
        animations.scaleIntoPlaceAnimation(dialogConformation);
        investAmountBlur.setVisibility(View.VISIBLE);
        investAmountBlur.setOnClickListener(v -> animations.scaleOutAnimation(dialogConformation, investAmountBlur));
    }

    @OnClick(R.id.dialog_primary_button)
    public void dialogPrimaryButton() {
        progressDialog = ProgressDialog.show(this, "", "Deleting...");
        progressDialog.show();

        animations.scaleOutAnimation(dialogConformation, investAmountBlur);
        presenter.deleteDraft();
    }

    @Override
    public void deleteSuccessful() {
        hideLoader();
        // Send message
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(AppConstants.DRAFT_DELETED));
        finish();
    }

    @Override
    public void hideLoader() {
        if (progressDialog != null) progressDialog.dismiss();
        animations.scaleOutAnimation(dialogConformation, investAmountBlur);
    }

    @OnClick(R.id.dialog_secondary_button)
    public void dialogSecondaryButton() {
        animations.scaleOutAnimation(dialogConformation, investAmountBlur);
    }

    @Override
    public void showReturns(String oneMonth, String oneYear, boolean isMonthPositive, boolean isYearPositive) {
        oneMonthReturn.setText(oneMonth);
        oneYearReturn.setText(oneYear);

        int red = R.color.red_600;
        int green = R.color.green_600;
        oneMonthReturn.setTextColor(ContextCompat.getColor(this, isMonthPositive ? green : red));
        oneYearReturn.setTextColor(ContextCompat.getColor(this, isYearPositive ? green : red));
    }

    @Override
    public void showBlur() {
        investAmountBlur.setVisibility(View.VISIBLE);
        investAmountBlur.setClickable(true);
        investAmountBlur.setOnClickListener(v -> {
            investContract.hideInvestCard();
        });
    }

    @Override
    public void invalidHistorical() {

    }

    @Override
    public void onHistoricalReceived(List<PPoints> smallcasePoints) {
        if (swipeRefreshLayout.getVisibility() != View.VISIBLE) {
            swipeRefreshLayout.setVisibility(View.VISIBLE);
            loadingIndicator.setVisibility(View.GONE);
        }
        performanceFragment.onDataReceived(smallcasePoints);
    }

    @Override
    public String getDuration() {
        return durations[performanceFragment.getSelectedDuration()];
    }

    @Override
    public void showSnackBar(int errId) {
        final Snackbar snackbar = Snackbar.make(parentLayout, errId, Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.retry, v -> {
            snackbar.dismiss();
            presenter.getSmallcaseDetailData();
        }).show();
    }

    @Override
    public void onNewsReceived(List<PPlainNews> newsList) {
        newsAdapter = new NewsAdapter(this, newsList, analyticsContract, false);
        this.newsList.setAdapter(newsAdapter);
    }

    @Override
    public void noNewsForSmallcase() {
        newsContainer.setVisibility(View.GONE);
    }

    @Override
    public void onSmallcaseInvestmentsReceived(PSmallcaseInvestment smallcaseInvestment) {
        if (presenter.isSmallcaseValid() && !isSmallcasePlayedOut) {
            negativeContainer.setVisibility(View.GONE);
        } else {
            greyBuyOrInvestMore();
        }

        presenter.setSource(smallcaseInvestment.getSource());
        investedContainer.setVisibility(View.VISIBLE);
        presenter.setIScid(smallcaseInvestment.getiScid());
        broughtOn.setText(smallcaseInvestment.getBroughtOn());
        pAndL.setText(smallcaseInvestment.getpAndL());

        if (!presenter.isReBalancePending()) {
            minInvestmentAmount.setText(presenter.getMinInvestment());
        }
    }

    @OnClick(R.id.read_rationale_container)
    public void readOrHideRationale() {
        boolean isExpanded = rationale.getMaxLines() == 100;
        ObjectAnimator animation = ObjectAnimator.ofInt(
                rationale,
                "maxLines",
                isExpanded ? 3 : 100);
        animation.setDuration(500);
        animation.start();
        rationaleArrow.setRotationX(isExpanded ? 0 : 180);

        if (!isExpanded) {
            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("smallcaseName", smallcaseName.getText().toString());
            eventProperties.put("smallcaseType", smallcaseType.getText().toString());
            eventProperties.put("smallcaseTier", presenter.isSmallcasePremium()? SmallcaseTier.PREMIUM : SmallcaseTier.BASIC);
            analyticsContract.sendEvent(eventProperties, "Read Rationale", Analytics.MIXPANEL, Analytics.CLEVERTAP);
        }
    }

    @OnClick(R.id.invested_container)
    public void investmentDetail() {
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("accessedFrom", "smallcase Profile");
        analyticsContract.sendEvent(eventProperties, "Viewed Investment Details", Analytics.MIXPANEL, Analytics.CLEVERTAP);

        presenter.onInvestmentDetailsClicked();
    }

    void showStockInfo(String sid, String stockName) {
        stockInfo.showStockInfo(sid);
    }

    @Override
    public void onSmallcaseDetailsReceived(PSmallcaseDetail smallcaseDetail) {
        swipeRefreshLayout.setRefreshing(false);

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("accessedFrom", getIntent().getStringExtra(ACCESSED_FROM));
        eventProperties.put("smallcaseName", smallcaseDetail.getSmallcaseName());
        eventProperties.put("smallcaseType", smallcaseDetail.getType());
        eventProperties.put("isInvested", presenter.isInvested());
        eventProperties.put("tier", presenter.isSmallcasePremium()? SmallcaseTier.PREMIUM : SmallcaseTier.BASIC);
        eventProperties.put("isWatchlisted", smallcaseDetail.isWatchListed());
        analyticsContract.sendEvent(eventProperties, "Viewed smallcase Profile", Analytics.INTERCOM, Analytics.MIXPANEL, Analytics.CLEVERTAP);

        isSmallcasePlayedOut = smallcaseDetail.isPlayedOut();

        loadingIndicator.setVisibility(View.GONE);
        buyWatchlistContainer.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setVisibility(View.VISIBLE);

        if (title == null) {
            title = smallcaseDetail.getSmallcaseName();
            toolBarTitle.setText(title);
        }

        presenter.setSource(SmallcaseSource.PROFESSIONAL);

        isWatchlisted = smallcaseDetail.isWatchListed();
        if (!isFinishing()) {
            Glide.with(this)
                    .load(smallcaseDetail.getSmallcaseImage())
                    .placeholder(getResources().getDrawable(R.color.grey_300))
                    .into(smallcaseImage);
        }
        smallcaseName.setText(smallcaseDetail.getSmallcaseName());
        smallcaseType.setText(smallcaseDetail.getType());
        smallcaseDescription.setText(smallcaseDetail.getSmallcaseDescription());
        index.setText(smallcaseDetail.getSmallcaseIndex());
        Glide.with(this)
                .load(smallcaseDetail.isIndexUp() ? R.drawable.up_arrow : R.drawable.down_arrow_red)
                .into(indexUpOrDown);

        // Hack to remove links in rationale. This SHOULD change
        // ----------------------------------------------------- //
        int lowCost = smallcaseDetail.getRationale().indexOf("This is a low-cost version");
        int highCost = smallcaseDetail.getRationale().indexOf("A low-cost version");
        int midCost = smallcaseDetail.getRationale().indexOf("This is a Mid-Cap version");
        int highWithMidCost = smallcaseDetail.getRationale().indexOf("A Mid-Cap version");

        int cutShortIndex = -1;
        if (lowCost != -1) {
            cutShortIndex = lowCost;
        } else if (highCost != -1) {
            cutShortIndex = highCost;
        } else if (midCost != -1) {
            cutShortIndex = midCost;
        } else if (highWithMidCost != -1) {
            cutShortIndex = highWithMidCost;
        }

        smallcaseInceptionInfo = smallcaseDetail.getSmallcaseInceptionInfo();

        String rationaleString = cutShortIndex == -1 ? smallcaseDetail.getRationale() : smallcaseDetail
                .getRationale().substring(0, cutShortIndex);
        // ----------------------------------------------------- //
        if (Build.VERSION.SDK_INT >= 24) {
            rationale.setText(Html.fromHtml(rationaleString, Html.FROM_HTML_SEPARATOR_LINE_BREAK_HEADING));
        } else {
            rationale.setText(Html.fromHtml(rationaleString));
        }
        rationale.setMovementMethod(LinkMovementMethod.getInstance());
        oneMonthReturn.setText(smallcaseDetail.getOnMonthReturn());
        oneYearReturn.setText(smallcaseDetail.getOneYearReturn());

        int red = R.color.red_600;
        int green = R.color.green_600;
        oneMonthReturn.setTextColor(ContextCompat.getColor(this, smallcaseDetail.isOneMonthPositive() ? green : red));
        oneYearReturn.setTextColor(ContextCompat.getColor(this, smallcaseDetail.isOneYearPositive() ? green : red));

        if (isSmallcasePlayedOut) {
            negativeLayoutDivider.setVisibility(View.VISIBLE);
            negativeContainer.setVisibility(View.VISIBLE);
            negativeContainer.setBackgroundColor(ContextCompat.getColor(this, R.color.error_red));
            negativeTitle.setText(smallcaseDetail.getPlayedOutTitle());
            negativeReason.setText(smallcaseDetail.getReasonForPlayedOut());
        }

        actionIcon.setImageResource(isWatchlisted ? R.drawable.watchlist_selected : R.drawable.watchlist);
        stocksFragment.onDataReceived(smallcaseDetail.getRebalanceDetail(), smallcaseDetail.getSegmentMap());

        if (shouldOpenBuy) {
            buySmallcase();
            shouldOpenBuy = false;
        }
    }

    @Override
    public void marketIsOpen() {
        List<Stock> stockList = presenter.getStockList();
        if (stockList.isEmpty()) return;
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", smallcaseName.getText().toString());
        eventProperties.put("smallcaseType", smallcaseType.getText().toString());
        eventProperties.put("accessedFrom", "Smallcase Profile");
        eventProperties.put("isInvested", presenter.isInvested());
        eventProperties.put("tier", presenter.isSmallcasePremium()? SmallcaseTier.PREMIUM : SmallcaseTier.BASIC);
        eventProperties.put("isWatchlisted", isWatchlisted);
        analyticsContract.sendEvent(eventProperties, "Viewed Invest Popup", Analytics.MIXPANEL, Analytics.CLEVERTAP);
        investContract.getMinAmount(stockList);
    }

    @Override
    public String getSmallcaseName() {
        return title;
    }

    @Nullable
    @Override
    public List<PPlainNews> getNewsList() {
        if (newsAdapter == null) return null;
        return newsAdapter.getNewsList();
    }

    @Override
    public void showSimpleSnackbar(@StringRes int resId) {
        snackbar = Snackbar.make(parentLayout, resId, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void sendUserToKiteLogin() {
        startActivityForResult(new Intent(this, LoginActivity.class),
                LOGIN_REQUEST_CODE);
        overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    }

    @Override
    public void showBuy() {
        buySmallcase.setText("BUY SMALLCASE");
        if (investedContainer.getVisibility() == View.VISIBLE) {
            investedContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void showInvestMore() {
        buySmallcase.setText("INVEST MORE");
    }

    @Override
    public void unGreyBuyOrInvestMore() {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(GradientDrawable.RECTANGLE);
        gradientDrawable.setCornerRadius(AppUtils.getInstance().dpToPx(2));
        gradientDrawable.setColor(ContextCompat.getColor(this, R.color.green_800));
        buySmallcase.setBackground(gradientDrawable);
    }

    private void greyBuyOrInvestMore() {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(GradientDrawable.RECTANGLE);
        gradientDrawable.setCornerRadius(AppUtils.getInstance().dpToPx(2));
        gradientDrawable.setColor(ContextCompat.getColor(this, R.color.blue_grey_100));
        buySmallcase.setBackground(gradientDrawable);
    }

    @Override
    public void showNegativeContainer(SpannableString title, @Nullable String description, boolean showArrow) {
        negativeLayoutDivider.setVisibility(View.VISIBLE);
        negativeContainer.setVisibility(View.VISIBLE);
        negativeTitle.setText(title);
        if (description != null) {
            negativeReason.setText(description);
        } else {
            negativeReason.setVisibility(View.GONE);
        }
        negativeContainer.setBackgroundColor(ContextCompat.getColor(this, R.color.error_yellow));
        arrow.setVisibility(showArrow ? View.VISIBLE : View.GONE);
    }

    @Override
    public void hideInvestCard() {
        investContract.hideInvestCard();
    }

    @Override
    public double getInvestmentAmount() {
        return investContract.getInvestmentAmount();
    }

    @Override
    public void onMinPriceReceived(SpannableString minPrice) {
        if (!presenter.isReBalancePending()) minInvestmentAmount.setText(minPrice);
    }

    @Override
    public void canContinueTransaction(List<Stock> stockList) {
        presenter.canContinueTransaction(stockList);
    }


    @Override
    public void onConfirmAmountClicked(double investmentAmount, double minInvestmentAmount) {
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", getSmallcaseName());
        eventProperties.put("smallcaseType", smallcaseType.getText().toString());
        eventProperties.put("amountConfirmed", investmentAmount);
        eventProperties.put("minInvestAmount", minInvestmentAmount);
        analyticsContract.sendEvent(eventProperties, "Confirmed Amount", Analytics.MIXPANEL, Analytics.CLEVERTAP);
    }

    @Override
    public void onAddFundsClicked(double investmentAmount, double minInvestmentAmount) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(AppConstants.KITE_ADD_FUNDS));
        startActivity(intent);
    }

    @Override
    public void onConfirmShownAgain() {
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", getSmallcaseName());
        eventProperties.put("smallcaseType", smallcaseType.getText().toString());
        analyticsContract.sendEvent(eventProperties, "Changed Amount", Analytics.MIXPANEL, Analytics.CLEVERTAP);
    }

    @OnClick(R.id.negative_container)
    public void negativeContainerClicked() {
        presenter.onNegativeContainerClicked();
    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            //Received request_token from Kite login. Request authToken from server.
            HashMap<String, String> body = new HashMap<>();
            body.put("broker", "kite");
            body.put("reqToken", data.getStringExtra(REQUEST_TOKEN_KEY));
            body.put("app", "platform");

            InvestContract.Authenticate authenticate = (InvestContract.Authenticate) investContract;
            authenticate.getAuthToken(body);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.see_all_news)
    public void seeAllNews() {
        presenter.onSeeAllNewsClicked();
    }

    void fetchHistorics(String duration) {
        presenter.fetchHistorical();
    }

    private void setUpViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        performanceFragment = new PerformanceFragment();
        stocksFragment = new StocksFragment();

        adapter.addFragment(performanceFragment, "Past Performance");
        adapter.addFragment(stocksFragment, "Stocks");
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1 && presenter.shouldStockTutorialBeShown()) {
                    stocksFragment.showTutorial();
                }
                viewPager.reMeasureCurrentPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setAdapter(adapter);
    }

    @OnClick(R.id.dont_show_again_text)
    public void dontShowMessageAgain() {
        dontShowAgain.setChecked(!dontShowAgain.isChecked());
    }

    @Override
    public void onRefresh() {
        if (did != null && sids != null) {
            presenter.getDraftDetailData(did, sids);
        } else {
            presenter.getSmallcaseDetailData();
        }
    }

    private void sendUserToCustomizeSmallcase(List<Object> stocksAndSegmentsList) {
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("accessedFrom", "smallcase Profile");
        eventProperties.put("smallcaseName", getSmallcaseName());
        eventProperties.put("smallcaseType", smallcaseType.getText().toString());
        eventProperties.put("smallcaseTier", presenter.isSmallcasePremium() ? SmallcaseTier.PREMIUM : SmallcaseTier.BASIC);
        analyticsContract.sendEvent(eventProperties, "Viewed Customize smallcase", Analytics.MIXPANEL, Analytics.CLEVERTAP);

        DraftSmallcase draftSmallcase = new DraftSmallcase();
        draftSmallcase.set_id(did);
        draftSmallcase.setScid(scid);
        draftSmallcase.setCompositionScheme(SmallcaseCompositionScheme.WEIGHTS);
        draftSmallcase.setSource(presenter.getSource());

        List<Segments> segmentsList = new ArrayList<>();
        List<String> sidConstituents = new ArrayList<>();
        List<Constituents> constituents = new ArrayList<>();
        Segments segment = null;
        for (Object object : stocksAndSegmentsList) {
            if (object instanceof PSegment) {
                if (segment != null) {
                    segment.setConstituents(new ArrayList<>(sidConstituents));
                    segmentsList.add(segment);
                    sidConstituents.clear();
                }

                PSegment pSegment = (PSegment) object;
                segment = new Segments();
                segment.setLabel(pSegment.getName());
            } else {
                PStockWeight stockWeight = (PStockWeight) object;
                sidConstituents.add(stockWeight.getSid());

                Constituents constituent = new Constituents();
                constituent.setSid(stockWeight.getSid());
                constituent.setWeight(stockWeight.getWeightage() / 100);
                constituent.setLocked(stockWeight.isLocked());
                SidInfo sidInfo = new SidInfo();
                sidInfo.setName(stockWeight.getStockName());
                sidInfo.setSector(stockWeight.getSector());
                constituent.setSidInfo(sidInfo);
                constituents.add(constituent);
            }
        }

        if (segment != null) {
            segment.setConstituents(new ArrayList<>(sidConstituents));
            segmentsList.add(segment);
        }

        Info info = new Info();
        info.setName(getSmallcaseName());
        info.setShortDescription(smallcaseDescription != null ? smallcaseDescription.getText().toString() : "");
        info.setTier(presenter.isSmallcasePremium() ? SmallcaseTier.PREMIUM : SmallcaseTier.BASIC);
        draftSmallcase.setInfo(info);

        draftSmallcase.setConstituents(constituents);
        draftSmallcase.setSegments(segmentsList);

        Intent intent = new Intent(this, CreateSmallcaseActivity.class);
        intent.putExtra(CreateSmallcaseActivity.SMALLCASE_NAME, getSmallcaseName());
        intent.putExtra(CreateSmallcaseActivity.SMALLCASE_DESCRIPTION, smallcaseDescription != null ?
                smallcaseDescription.getText().toString() : "");
        intent.putExtra(CreateSmallcaseActivity.DRAFT_SMALLCASE, draftSmallcase);
        intent.putExtra(CreateSmallcaseActivity.SMALLCASE_SCID, scid);
        startActivity(intent);
    }

    public void onCustomizeSmallcaseClicked(final List<Object> stocksAndSegmentsList) {
        if (!presenter.shouldShowCustomizePopUp()) {
            sendUserToCustomizeSmallcase(stocksAndSegmentsList);
            return;
        }

        animations.scaleIntoPlaceAnimation(customizeInfoContainer);
        investAmountBlur.setVisibility(View.VISIBLE);
        investAmountBlur.setOnClickListener(v -> animations.scaleOutAnimation(customizeInfoContainer, investAmountBlur));

        gotCustomizeInfo.setOnClickListener(v -> {
            animations.scaleOutAnimation(customizeInfoContainer, investAmountBlur);
            if (dontShowAgain.isChecked()) {
                presenter.dontShowCustomizeInfoPopup();
            }
            sendUserToCustomizeSmallcase(stocksAndSegmentsList);
        });
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<SpannableString> titleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);
        }

        void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);

            SpannableString spannableString = new SpannableString(title);
            spannableString.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(SmallcaseDetailActivity.this, AppConstants.FontType.MEDIUM)), 0, title.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            titleList.add(spannableString);
        }
    }
}
