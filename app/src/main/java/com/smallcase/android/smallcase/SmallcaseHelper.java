package com.smallcase.android.smallcase;

import android.support.annotation.Nullable;
import android.util.LongSparseArray;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Historical;
import com.smallcase.android.data.model.Points;
import com.smallcase.android.data.repository.AuthRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.model.Stock;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import io.sentry.event.Breadcrumb;

/**
 * Created by shashankm on 18/04/17.
 */

public class SmallcaseHelper implements LoginRequest {
  private static final String TAG = "SmallcaseHelper";

  private AuthRepository authRepository;
  private LoginResponse loginResponse;
  private SharedPrefService sharedPrefService;

  public interface LoginResponse {
    void onJwtSavedSuccessfully();

    void onFailedFetchingJwt();

    /**
     * If a user's Kite auth has expired (but not smallcase auth) and he logs in to a different
     * account other than the one he had used to previously get smallcase auth, then this method is called
     * to restart user flow. (Home screen and refresh everything)
     */
    void onDifferentUserLoggedIn();
  }

  public SmallcaseHelper() {
  }

  public SmallcaseHelper(AuthRepository authRepository, LoginResponse loginResponse, SharedPrefService sharedPrefService) {
    this.authRepository = authRepository;
    this.loginResponse = loginResponse;
    this.sharedPrefService = sharedPrefService;
  }

  @Override
  public void getJwtToken(HashMap<String, String> body) {
    authRepository.getAuthToken(body)
        .subscribe(responseBody -> {
          try {
            JSONObject object = new JSONObject(responseBody.string());
            if (!object.getBoolean("success")) return;
            String jwtTrading = object.getJSONObject("data").getString("jwt_trd");
            fetchRequestToken(jwtTrading);
          } catch (JSONException | IOException e) {
            e.printStackTrace();
            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; getJwtToken()");
            loginResponse.onFailedFetchingJwt();
          }
        }, throwable -> {
          SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getJwtToken()");
          loginResponse.onFailedFetchingJwt();
        });

  }

  private void fetchRequestToken(String jwtTrading) {
    authRepository.getRequestToken("platform", jwtTrading)
        .subscribe(responseBody -> {
          try {
            JSONObject object = new JSONObject(responseBody.string());
            if (!object.getBoolean("success")) return;
            String requestToken = object.getJSONObject("data").getString("reqToken");
            loginToSmallcase(requestToken);
          } catch (JSONException | IOException e) {
            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; fetchRequestToken()");
            loginResponse.onFailedFetchingJwt();
            e.printStackTrace();
          }
        }, throwable -> {
          SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; fetchRequestToken()");
          loginResponse.onFailedFetchingJwt();
        });
  }

  private void loginToSmallcase(String requestToken) {
    HashMap<String, String> body = new HashMap<>();
    body.put("reqToken", requestToken);
    authRepository.loginSmallcase(body)
        .subscribe(responseBody -> {
          try {
            if (responseBody != null && loginResponse != null) {
              JSONObject object = new JSONObject(responseBody.string());
              if (!object.getBoolean("success")) return;
              String jwt = object.getJSONObject("data").getString("jwt");
              String csrf = object.getJSONObject("data").getString("x-csrf-token");
              String previousJwt = sharedPrefService.getAuthorizationToken();

              sharedPrefService.clearSharedPreference();

              if (jwt == null) {
                loginResponse.onFailedFetchingJwt();
                return;
              }

              sharedPrefService.saveAuthorizationToken(jwt);
              sharedPrefService.saveCsrf(csrf);
              if (previousJwt == null) {
                // First time login / logout -> login
                loginResponse.onJwtSavedSuccessfully();
                return;
              }

              if (AppUtils.getInstance().getUserId(jwt).equals(AppUtils.getInstance().getUserId(previousJwt))) {
                loginResponse.onJwtSavedSuccessfully();
              } else {
                // Different user from the one which is already logged in
                loginResponse.onDifferentUserLoggedIn();
              }
            }
          } catch (IOException | JSONException e) {
            SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.DEFAULT, "Failed parsing data", null);
            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; loginToSmallcase()");
            loginResponse.onFailedFetchingJwt();
            e.printStackTrace();
          }
        }, throwable -> {
          SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; loginToSmallcase()");
          loginResponse.onFailedFetchingJwt();
        });
  }

  public List<Points> createHistoricalForStocks(List<Historical> historicalList, List<Stock> stockList, @Nullable List<Points> niftyPoints) {
    if (historicalList.isEmpty()) return Collections.emptyList();

    List<Points> referenceList = niftyPoints == null ? historicalList.get(0).getPoint() : niftyPoints;
    int expectedSize = referenceList.size();

    if (expectedSize == 0) return Collections.emptyList();

    List<String> acceptedDates = new ArrayList<>();
    for (Points point : referenceList) {
      acceptedDates.add(point.getDate());
    }

    // Create sid and shares map
    HashMap<String, Integer> sharesMap = new HashMap<>();
    for (Stock stock : stockList) {
      sharesMap.put(stock.getSid(), (int) stock.getShares());
    }

    List<Points> pointsList = new ArrayList<>();
    LongSparseArray<Double> datesMap = new LongSparseArray<>();
    for (Historical historical : historicalList) {
      int i = 0, j = 0;
      List<Points> historicalPoints = historical.getPoint();
      while (i < acceptedDates.size()) {
        long date = AppUtils.getInstance().getPlainDate(acceptedDates.get(i)).getTime();
        if (j < historicalPoints.size() && historicalPoints.get(j) != null) {
          long givenDate = AppUtils.getInstance().getPlainDate(historicalPoints.get(j).getDate()).getTime();
          if (givenDate == date) {
            int shares = sharesMap.get(historical.get_id()) == null? 1 : sharesMap.get(historical.get_id());
            addPoint(date, historicalPoints.get(j).getPrice(), datesMap, shares, i, pointsList, acceptedDates.get(i));
            i++;
            j++;
          } else if (givenDate < date) {
            j++;
          } else {
            double price;
            if (j - 1 > -1) {
              price = historicalPoints.get(j - 1).getPrice();
            } else {
              price = historicalPoints.get(j).getPrice();
            }
            int shares = sharesMap.get(historical.get_id()) == null? 1 : sharesMap.get(historical.get_id());
            addPoint(date, price, datesMap, shares, i, pointsList, acceptedDates.get(i));
            i++;
          }
        } else {
          i++;
          j++;
        }
      }
    }

    return pointsList;
  }

  private void addPoint(long date, double price, LongSparseArray<Double> datesMap, int shares, int i, List<Points> points, String dateInString) {
    double amount = shares * price;
    if (datesMap.get(date, -1d) != -1) {
      datesMap.put(date, datesMap.get(date) + amount);
      points.get(i).setIndex(datesMap.get(date));
    } else {
      datesMap.put(date, amount);
      Points point = new Points();
      point.setIndex(amount);
      point.setDate(dateInString);
      points.add(point);
    }
  }
}
