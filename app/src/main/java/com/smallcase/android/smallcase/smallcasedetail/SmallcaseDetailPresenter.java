package com.smallcase.android.smallcase.smallcasedetail;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;

import com.google.gson.Gson;
import com.smallcase.android.R;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Constituents;
import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.model.Historical;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.Points;
import com.smallcase.android.data.model.ReBalanceUpdate;
import com.smallcase.android.data.model.Segments;
import com.smallcase.android.data.model.SmallcaseDetail;
import com.smallcase.android.data.model.StockListWithMinAmount;
import com.smallcase.android.data.model.UnInvestedSmallcase;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.investedsmallcase.InvestedSmallcaseDetailActivity;
import com.smallcase.android.news.NewsActivity;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.user.order.BatchDetailsActivity;
import com.smallcase.android.user.order.ReviewOrderActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.OrderLabel;
import com.smallcase.android.util.SmallcaseCompositionScheme;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.util.SmallcaseTier;
import com.smallcase.android.view.android.CustomTypefaceSpan;
import com.smallcase.android.view.model.PPlainNews;
import com.smallcase.android.view.model.PPoints;
import com.smallcase.android.view.model.PSmallcaseDetail;
import com.smallcase.android.view.model.PSmallcaseInvestment;
import com.smallcase.android.view.model.PStockWeight;
import com.smallcase.android.view.model.Stock;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.sentry.event.Breadcrumb;
import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by shashankm on 28/02/17.
 */

class SmallcaseDetailPresenter implements SmallcaseDetailContract.Presenter, StockHelper.StockPriceCallBack {
  private static final String TAG = "SmallcaseDetaiPresenter";

  private SmallcaseDetailContract.Service serviceContract;
  private SmallcaseDetailContract.View viewContract;
  private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
  private NetworkHelper networkHelper;
  private Activity activity;
  private MarketRepository marketRepository;
  private StockHelper stockHelper;
  private List<Stock> unInvestedStockList;
  private List<Stock> investedStockList;
  private AppConstants.MarketStatus marketStatus = AppConstants.MarketStatus.NOT_FETCHED;
  private String scid, iScid, source, did;
  private String smallcaseStatus = "", compositionScheme = "";
  private double minInvestment;
  private SharedPrefService sharedPrefService;
  private boolean isCalculatingPrice = false;
  private ReBalanceUpdate reBalanceUpdate;
  private boolean isSmallcasePremium = false, isDraft = false;
  private double oneMonthReturn = -999, oneYearReturn = -999;
  private DraftSmallcase draftSmallcase;
  private boolean isUserTryingToBuy = false;

  SmallcaseDetailPresenter(Activity activity, SmallcaseRepository smallcaseRepository, SharedPrefService sharedPrefService, SmallcaseDetailContract
      .View viewContract, NetworkHelper networkHelper, MarketRepository marketRepository, StockHelper stockHelper, UserRepository
                               userRepository, String scid, AnalyticsContract analyticsContract, UserSmallcaseRepository userSmallcaseRepository,
                           boolean isCustom) {
    this.activity = activity;
    serviceContract = new SmallcaseDetailService(smallcaseRepository, sharedPrefService, this, userRepository,
        marketRepository, userSmallcaseRepository);
    this.viewContract = viewContract;
    this.networkHelper = networkHelper;
    this.sharedPrefService = sharedPrefService;
    this.marketRepository = marketRepository;
    this.stockHelper = stockHelper;
    this.scid = scid;
    if (!isCustom) iScid = serviceContract.getIScid(scid);

    if ("leprechaun".equals(sharedPrefService.getUserInfo().get("brokerName"))) {
      marketStatus = AppConstants.MarketStatus.OPEN;
    } else {
      serviceContract.getIfMarketIsOpen();
    }
  }

  @Override
  public void getSmallcaseDetailData() {
    isDraft = false;
    if (null == scid) {
      viewContract.showSnackBar(R.string.something_wrong);
      return;
    }

    if (!networkHelper.isNetworkAvailable()) {
      viewContract.showSnackBar(R.string.no_internet);
      return;
    }

    serviceContract.getSmallcaseDetail(scid);
    if (isInvested()) {
      viewContract.showInvestMore();
      serviceContract.getInvestedSmallcaseDetail(iScid);
      serviceContract.getPendingActions();
    } else {
      smallcaseStatus = "VALID";
      viewContract.showBuy();
      viewContract.unGreyBuyOrInvestMore();
      reBalanceUpdate = null;
    }

    serviceContract.getUserData();
    serviceContract.getSmallcaseHistorical(scid, "index", ".NSEI", viewContract.getDuration());
    serviceContract.getSmallcaseNews(scid);
  }

  @Override
  public void onStockPricesReceived(HashMap<String, Double> stockMap) {
    viewContract.hideStockBlurIfVisible();
    final List<Stock> stockList = isInvested() ? investedStockList : unInvestedStockList;

    if (stockList == null) {
      viewContract.showSimpleSnackbar(R.string.something_wrong);
      return;
    }

    List<String> sids = new ArrayList<>();
    for (Stock stock : stockList) {
      sids.add(stock.getSid());
      if (!stockMap.containsKey(stock.getSid())) {
        viewContract.showSimpleSnackbar(R.string.something_wrong);
        return;
      }

      double price = stockMap.get(stock.getSid());
      stock.setPrice(price);
    }

    if (isDraft) {
      sids.add(".NSEI");
      serviceContract.getStockHistorical(sids, viewContract.getDuration());

      minInvestment = SmallcaseCompositionScheme.SHARES.equals(compositionScheme) ? stockHelper
          .calculateAmountFromShares(stockList) : stockHelper.calculateSharesAndMinAmountFromWeight
          (stockList, -1).getMinAmount();
      SpannableString spannableString = new SpannableString("Minimum amount \n" + AppConstants
          .RUPEE_SYMBOL + String.format(Locale.getDefault(), "%.2f", minInvestment));
      spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primary_text)), 14,
          spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
      spannableString.setSpan(new RelativeSizeSpan(1.25f), 14,
          spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
      viewContract.onMinPriceReceived(spannableString);
      return;
    }

    // Calculate weightage only for invest more && if weight isn't already calculated
    if (investedStockList != null && !investedStockList.isEmpty() && investedStockList.get(0).getWeight() <= 0) {
      stockHelper.calculateWeightageFromShares(stockList);
    }

    if (isCalculatingPrice) return;

    // To prevent double calculation when user refreshes
    isCalculatingPrice = true;

    final HashMap<String, String> data = new HashMap<>();
    for (Stock stock : stockList) {
      data.put("sid", stock.getSid());
      data.put("price", String.valueOf(stock.getPrice()));
    }

    Observable.fromCallable(() -> stockHelper.calculateSharesAndMinAmountFromWeight(stockList, -1))
        .subscribeOn(Schedulers.computation())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(stockListWithMinAmount -> {
          if (stockListWithMinAmount == null || activity == null) return;

          isCalculatingPrice = false;
          minInvestment = AppUtils.getInstance().round(stockListWithMinAmount.getMinAmount(), 2);
          SpannableString spannableString = new SpannableString("Minimum amount \n" + AppConstants
              .RUPEE_SYMBOL + minInvestment);
          spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primary_text)), 14,
              spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
          spannableString.setSpan(new RelativeSizeSpan(1.25f), 14,
              spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
          viewContract.onMinPriceReceived(spannableString);
        }, throwable -> {
          isCalculatingPrice = false;

          SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.USER, "Stocks and prices", data);
          SentryAnalytics.getInstance().captureEvent(null, throwable, TAG + "; onStockPricesReceived()");

          viewContract.showSimpleSnackbar(R.string.something_wrong);
        });
  }

  @Override
  public void onFailedFetchingStockPrice(Throwable throwable, @Nullable String sid) {
    if (throwable != null && throwable instanceof HttpException && ((HttpException) throwable).code() == 401) {
      viewContract.showStocksBlur();
      return;
    }

    SentryAnalytics.getInstance().captureEvent(null, throwable, TAG + "; onFailedFetchingStockPrice()");
    viewContract.hideStockBlurIfVisible();
  }

  @Override
  public void addReminder() {
    HashMap<String, String> body = new HashMap<>();
    body.put("scid", scid);
    body.put("action", "BUY");
    serviceContract.addMarketOpenReminder(body);
  }

  @Override
  public void destroySubscriptions() {
    activity = null;
    serviceContract.destroySubscriptions();
  }

  @Override
  public boolean isSmallcasePremium() {
    return isSmallcasePremium;
  }

  @Override
  public boolean shouldStockTutorialBeShown() {
    boolean hasUserNotViewedTutorial = !sharedPrefService.hasUserViewedStocks();
    if (hasUserNotViewedTutorial) {
      sharedPrefService.userViewedSmallcaseStocks(true);
      HashMap<String, Object> body = new HashMap<>();
      body.put("flag", "mobileStockInfo");
      body.put("type", "markers");
      body.put("value", true);
      serviceContract.setFlag(body);
    }
    return hasUserNotViewedTutorial;
  }


  @Override
  public void onReBalanceUpdatesFetched(List<ReBalanceUpdate> reBalanceUpdates) {
    if (!isInvested()) {
      reBalanceUpdate = null;
      viewContract.showBuy();
      return;
    }

    for (ReBalanceUpdate reBalanceUpdate : reBalanceUpdates) {
      if (iScid.equals(reBalanceUpdate.getIscid())) {
        String cameOn = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault()).format(AppUtils.getInstance().getPlainDate(reBalanceUpdate.getDate()));
        SpannableString spannableString = new SpannableString("Rebalance pending\n" + cameOn);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primary_text)),
            spannableString.length() - cameOn.length(), spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new RelativeSizeSpan(1.25f), spannableString.length() - cameOn.length(),
            spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        this.reBalanceUpdate = reBalanceUpdate;
        viewContract.showReBalancePending(spannableString);
        return;
      }
    }

    reBalanceUpdate = null;
    viewContract.showInvestMore();
  }

  @Override
  public void onFailedFetchingReBalanceUpdates() {
    reBalanceUpdate = null;
    if (!isInvested()) {
      viewContract.showBuy();
      return;
    }

    viewContract.showInvestMore();
    viewContract.showSimpleSnackbar(R.string.something_wrong);
  }

  @Override
  public void canContinueTransaction(List<Stock> stockList) {
    for (Stock stock : stockList) {
      stock.setBuy(true);
    }
    Intent intent = new Intent(activity, ReviewOrderActivity.class);
    intent.putParcelableArrayListExtra(ReviewOrderActivity.STOCK_LIST, (ArrayList<? extends Parcelable>) stockList);
    intent.putExtra(ReviewOrderActivity.SCID, scid);
    intent.putExtra(ReviewOrderActivity.INVESTMENT_AMOUNT, viewContract.getInvestmentAmount());
    intent.putExtra(ReviewOrderActivity.SMALLCASE_NAME, viewContract.getSmallcaseName());
    intent.putExtra(ReviewOrderActivity.ISCID, iScid);
    intent.putExtra(ReviewOrderActivity.ORDER_LABEL, isInvested() ? OrderLabel.INVEST_MORE : OrderLabel.BUY);
    intent.putExtra(ReviewOrderActivity.SOURCE, source);
    intent.putExtra(ReviewOrderActivity.DID, did);
    activity.startActivity(intent);
    activity.overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    viewContract.hideInvestCard();
  }

  @Override
  public void onMarketStatusReceived(ResponseBody responseBody) {
    try {
      JSONObject response = new JSONObject(responseBody.string());
      if (response.getString("data").equals("closed")) {
        marketStatus = AppConstants.MarketStatus.CLOSED;
      } else {
        marketStatus = AppConstants.MarketStatus.OPEN;
      }
    } catch (JSONException | IOException e) {
      e.printStackTrace();
      SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; onMarketStatusReceived()");
      viewContract.showSnackBar(R.string.something_wrong);
    }
  }

  @Override
  public void onFailedFetchingMarketStatus(Throwable throwable) {
    if (isUserTryingToBuy) {
      viewContract.sendUserToLoginOrSignUp();
      return;
    }
    viewContract.showSnackBar(R.string.something_wrong);
  }

  @Override
  public void checkMarketStatus() {
    switch (marketStatus) {
      case OPEN:
        viewContract.marketIsOpen();
        break;

      case CLOSED:
        viewContract.showMarketClosed();
        break;

      case NOT_FETCHED:
        serviceContract.getIfMarketIsOpen();
        isUserTryingToBuy = true;
        viewContract.showSimpleSnackbar(R.string.something_wrong);
        break;
    }
  }

  @Override
  public boolean isInvested() {
    return iScid != null;
  }

  @Override
  public void onInvestmentDetailsClicked() {
    Intent intent = new Intent(activity, InvestedSmallcaseDetailActivity.class);
    intent.putExtra(InvestedSmallcaseDetailActivity.SCID, scid);
    intent.putExtra(InvestedSmallcaseDetailActivity.ISCID, iScid);
    intent.putExtra(InvestedSmallcaseDetailActivity.SOURCE, source);
    activity.startActivity(intent);
  }

  @Override
  public void setIScid(String iScid) {
    this.iScid = iScid;
  }

  @Override
  public void onSeeAllNewsClicked() {
    if (viewContract.getNewsList() == null) return;

    Intent intent = new Intent(activity, NewsActivity.class);
    intent.putParcelableArrayListExtra(NewsActivity.NEWS_LIST, (ArrayList<? extends Parcelable>) viewContract.getNewsList());
    intent.putExtra(NewsActivity.SMALLCASE_NAME, viewContract.getSmallcaseName());
    intent.putExtra(NewsActivity.SCID, scid);
    activity.startActivity(intent);
  }

  @Override
  public boolean isSmallcaseValid() {
    return "VALID".equalsIgnoreCase(smallcaseStatus);
  }

  @Override
  public void onNegativeContainerClicked() {
    if (!"VALID".equalsIgnoreCase(smallcaseStatus)) {
      Intent intent = new Intent(activity, BatchDetailsActivity.class);
      intent.putExtra(BatchDetailsActivity.ISCID, iScid);
      intent.putExtra(BatchDetailsActivity.SCID, scid);
      intent.putExtra(BatchDetailsActivity.SMALLCASE_NAME, viewContract.getSmallcaseName());
      intent.putExtra(BatchDetailsActivity.SOURCE, source);
      activity.startActivity(intent);
    }
  }

  @Override
  public List<Stock> getStockList() {
    if (isInvested()) {
      if (investedStockList == null) {
        serviceContract.getInvestedSmallcaseDetail(iScid);
        viewContract.showSnackBar(R.string.no_investment_fetching);
        return Collections.emptyList();
      }
      return investedStockList;
    }

    if (unInvestedStockList == null) {
      serviceContract.getSmallcaseDetail(scid);
      viewContract.showSnackBar(R.string.no_stocks_fetching);
      return Collections.emptyList();
    }
    return unInvestedStockList;
  }

  @Override
  public void toggleWatchlist(boolean shouldWatchlist) {
    if (!networkHelper.isNetworkAvailable()) {
      viewContract.showSimpleSnackbar(R.string.no_internet);
      return;
    }

    if (serviceContract.getAuth() == null) {
      // Send user to login or sign up
      viewContract.sendUserToLoginOrSignUp();
      return;
    }

    if (shouldWatchlist) {
      serviceContract.addToWatchlist(scid);
    } else {
      serviceContract.removeFromWatchlist(scid);
    }
  }

  @Override
  public void onSmallcaseDetailsReceived(UnInvestedSmallcase unInvestedSmallcase) {
    unInvestedStockList = new ArrayList<>();

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
    String index = String.format(Locale.getDefault(), "%.2f", unInvestedSmallcase.getStats().getIndexValue());
    boolean isIndexUp = unInvestedSmallcase.getStats().getIndexValue() > unInvestedSmallcase.getStats().getLastCloseIndex();
    String oneMonth = String.format(Locale.getDefault(), "%.2f", (unInvestedSmallcase.getStats().getReturns().getMonthly() * 100)) + "%";
    String oneYear = String.format(Locale.getDefault(), "%.2f", (unInvestedSmallcase.getStats().getReturns().getYearly() * 100)) + "%";
    boolean isMonthPositive = unInvestedSmallcase.getStats().getReturns().getMonthly() > 0;
    boolean isYearPositive = unInvestedSmallcase.getStats().getReturns().getYearly() > 0;
    String minAmount = AppConstants.RUPEE_SYMBOL + AppConstants.getNumberFormatter().format(unInvestedSmallcase
        .getStats().getMinInvestAmount());
    boolean isPlayedOut = null != unInvestedSmallcase.getInfo().getPlayedOut();
    boolean isWatchlisted = serviceContract.isSmallcaseWatchlisted(unInvestedSmallcase.getScid());
    String nextRebalanced = simpleDateFormat.format(AppUtils.getInstance().getPlainDate(unInvestedSmallcase.getInfo().getNextUpdate()));
    String lastRebalanced = simpleDateFormat.format(AppUtils.getInstance().getPlainDate(unInvestedSmallcase.getInfo().getUpdated()));

    if (minInvestment == 0) minInvestment = unInvestedSmallcase.getStats().getMinInvestAmount();

    SpannableString spannableString = new SpannableString("Minimum amount \n" + AppConstants
        .RUPEE_SYMBOL + String.format(Locale.getDefault(), "%.2f", minInvestment));
    spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primary_text)), 14,
        spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    spannableString.setSpan(new RelativeSizeSpan(1.25f), 14,
        spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    viewContract.setInitialMinInvestmentAmount(spannableString);
    String playedOutRationaleTitle = "";
    String playedOutRationale = "";
    if (isPlayedOut) {
      playedOutRationaleTitle = unInvestedSmallcase.getInfo().getPlayedOut().getLabel();
      playedOutRationale = unInvestedSmallcase.getInfo().getPlayedOut().getRationale();
    }

    PSmallcaseDetail smallcaseDetail = new PSmallcaseDetail();
    smallcaseDetail.setSmallcaseName(unInvestedSmallcase.getInfo().getName());
    smallcaseDetail.setSmallcaseImage("187", SmallcaseSource.PROFESSIONAL, unInvestedSmallcase.getScid());
    smallcaseDetail.setType(unInvestedSmallcase.getInfo().getType());
    smallcaseDetail.setSmallcaseDescription(unInvestedSmallcase.getInfo().getShortDescription());
    smallcaseDetail.setSmallcaseIndex(index);
    smallcaseDetail.setRationale(unInvestedSmallcase.getRationale());
    smallcaseDetail.setIndexUp(isIndexUp);
    smallcaseDetail.setOnMonthReturn(oneMonth);
    smallcaseDetail.setOneYearReturn(oneYear);
    smallcaseDetail.setOneMonthPositive(isMonthPositive);
    smallcaseDetail.setOneYearPositive(isYearPositive);
    smallcaseDetail.setMinAmount(minAmount);
    smallcaseDetail.setPlayedOut(isPlayedOut);
    smallcaseDetail.setReasonForPlayedOut(playedOutRationale + ". This smallcase is not being tracked and will not have any further rebalance updates.");
    smallcaseDetail.setPlayedOutTitle(playedOutRationaleTitle);
    smallcaseDetail.setWatchListed(isWatchlisted);
    smallcaseDetail.setRebalanceDetail("Last rebalanced on " + lastRebalanced + "\nNext rebalance expected on " + nextRebalanced);

    HashMap<String, List<PStockWeight>> segments = new HashMap<>();
    HashMap<String, String> constituents = new HashMap<>();
    for (Segments segment : unInvestedSmallcase.getSegments()) {
      for (String constituent : segment.getConstituents()) {
        constituents.put(constituent, segment.getLabel());
      }
    }

    List<String> sids = new ArrayList<>();
    for (int i = 0; i < unInvestedSmallcase.getConstituents().size(); i++) {
      Constituents constituent = unInvestedSmallcase.getConstituents().get(i);
      PStockWeight PStockWeight = new PStockWeight();
      Stock stock = new Stock();
      stock.setStockName(constituent.getSidInfo().getName());
      stock.setWeight(constituent.getWeight());
      PStockWeight.setStockName(constituent.getSidInfo().getName());
      PStockWeight.setWeightage(constituent.getWeight() * 100);
      PStockWeight.setSid(constituent.getSid());
      PStockWeight.setSector(constituent.getSidInfo().getSector());
      PStockWeight.setLocked(false);
      stock.setSid(constituent.getSid());
      sids.add(stock.getSid());
      unInvestedStockList.add(stock);

      String label = constituents.get(constituent.getSid());

      if (segments.containsKey(label)) {
        List<PStockWeight> PStockWeights = segments.get(label);
        PStockWeights.add(PStockWeight);
      } else {
        List<PStockWeight> PStockWeights = new ArrayList<>();
        PStockWeights.add(PStockWeight);
        segments.put(label, PStockWeights);
      }
    }

    if (SmallcaseTier.PREMIUM.equals(unInvestedSmallcase.getInfo().getTier())) {
      isSmallcasePremium = true;
      viewContract.showPremium();
    }

    double indexDifference = ((unInvestedSmallcase.getStats().getIndexValue() - unInvestedSmallcase.getStats().getLastCloseIndex())
        / unInvestedSmallcase.getStats().getLastCloseIndex()) * 100;
    String currentIndex = "Current Index Value: " + index;
    String today = "\n\nToday’s Change: " + (indexDifference >= 0 ? "Up " : "Down ") + String.format(Locale.getDefault(), "%.2f", Math.abs(indexDifference)) + "%";
    String inception = "\n\nThis smallcase has moved " + ((unInvestedSmallcase.getStats().getIndexValue() - 100) >= 0 ? "up " : "down ")
        + String.format(Locale.getDefault(), "%.2f", unInvestedSmallcase.getStats().getIndexValue() - 100) + "% since its inception on " +
        simpleDateFormat.format(AppUtils.getInstance().getPlainDate(unInvestedSmallcase.getInfo().getCreated()));

    SpannableString smallcaseInceptionInfo = new SpannableString(currentIndex + today + inception);
    smallcaseInceptionInfo.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(activity, AppConstants.FontType.MEDIUM)),
        0, 21, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    smallcaseInceptionInfo.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(activity, AppConstants.FontType.MEDIUM)),
        currentIndex.length(), currentIndex.length() + 17, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

    smallcaseDetail.setSmallcaseInceptionInfo(smallcaseInceptionInfo);
    smallcaseDetail.setSegmentMap(segments);
    viewContract.onSmallcaseDetailsReceived(smallcaseDetail);

    // Show index tutorial first and the next time user come to smallcase details page, show watchlist tutorial
    // (Will also be shown if he user refreshes screen the first time itself, but meh)
    HashMap<String, Object> body = new HashMap<>();
    if (!sharedPrefService.hasUserViewedIndexTutorial()) {
      viewContract.showIndexTutorial();
      body.put("flag", "mobileIndexValue");
      body.put("type", "markers");
      body.put("value", true);
      serviceContract.setFlag(body);
      sharedPrefService.userViewedIndexTutorial(true);
    } else if (!sharedPrefService.hasUserViewedWatchlistTutorial()) {
      // Do not show tutorial if user has already watchlisted the smallcase
      if (!isWatchlisted) {
        viewContract.showWatchlistTutorial();
        body.put("flag", "mobileWatchlist");
        body.put("type", "markers");
        body.put("value", true);
        serviceContract.setFlag(body);
        sharedPrefService.userViewedWatchlistTutorial(true);
      }
    }

    if (!isInvested())
      stockHelper.getStockPrices(marketRepository, sharedPrefService, sids, this);
  }

  @Override
  public void onSmallcaseHistoricalReceived(Historical historical) {
    if (historical.getPoints().isEmpty() || historical.getBenchmark().getPoints().isEmpty()) {
      viewContract.showSimpleSnackbar(R.string.could_not_fetch_historic);
      return;
    }

    List<PPoints> smallcasePoints = new ArrayList<>();
    double baseSmallcaseIndex = 100 / historical.getPoints().get(0).getIndex(); // Convert index to be base 100
    double baseNiftyIndex = 100 / historical.getBenchmark().getPoints().get(0).getPrice(); // Convert price to be base 100
    for (int i = 0; i < historical.getPoints().size(); i++) {
      Points smallcasePoint = historical.getPoints().get(i);
      Points niftyPoint;
      try {
        niftyPoint = historical.getBenchmark().getPoints().get(i);
      } catch (ArrayIndexOutOfBoundsException e) {
        SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; onSmallcaseHistoricalReceived()");
        // Break out if nifty price points size is not equal to smallcase index points (Should always be equal)
        viewContract.invalidHistorical();
        return;
      }

      PPoints presentablePoint = new PPoints();
      presentablePoint.setDate(simpleDateFormat.format(AppUtils.getInstance().getPlainDate(smallcasePoint.getDate())));
      presentablePoint.setSmallcaseIndex(baseSmallcaseIndex * smallcasePoint.getIndex());
      presentablePoint.setNiftyIndex(baseNiftyIndex * niftyPoint.getPrice());
      smallcasePoints.add(presentablePoint);
    }
    viewContract.onHistoricalReceived(smallcasePoints);
  }

  @Override
  public void onSmallcaseInvestmentsReceived(SmallcaseDetail smallcaseDetail) {
    investedStockList = new ArrayList<>();

    PSmallcaseInvestment smallcaseInvestment = new PSmallcaseInvestment();
    smallcaseInvestment.setiScid(smallcaseDetail.get_id());

    String pAndL = "Your current P&L is " + AppConstants.RUPEE_SYMBOL + AppConstants.getNumberFormatter().format(smallcaseDetail.getReturns()
        .getNetworth() - smallcaseDetail.getReturns().getUnrealizedInvestment()) + ". Investment Details";
    boolean isProfit = smallcaseDetail.getReturns().getNetworth() > smallcaseDetail.getReturns().getUnrealizedInvestment();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
    String broughtOn = "You bought this smallcase on " + simpleDateFormat.format(AppUtils.getInstance()
        .getPlainDate(smallcaseDetail.getDate()));
    SpannableString pAndLSpan = new SpannableString(pAndL);
    pAndLSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, isProfit ? R.color.green_600 : R.color.red_600)),
        20, (pAndL.length() - 18), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    pAndLSpan.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.blue_800)),
        (pAndL.length() - 18), pAndL.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

    smallcaseStatus = smallcaseDetail.getStatus();
    SpannableString spannableString;
    switch (smallcaseDetail.getStatus()) {
      case "VALID":
        viewContract.unGreyBuyOrInvestMore();
        break;

      case "INVALID":
        String notFilledString = "Latest batch is not filled. ";
        spannableString = new SpannableString(notFilledString + "Repair/Archive");
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.blue_800)), notFilledString.length(),
            spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        viewContract.showNegativeContainer(spannableString, null, true);
        break;

      case "PLACED":
        spannableString = new SpannableString("Awaiting order status");
        viewContract.showNegativeContainer(spannableString, null, false);
        break;

      default:
        spannableString = new SpannableString("Awaiting order status");
        viewContract.showNegativeContainer(spannableString, null, false);
        break;
    }

    smallcaseInvestment.setpAndL(pAndLSpan);
    smallcaseInvestment.setBroughtOn(broughtOn);
    smallcaseInvestment.setProfit(isProfit);
    smallcaseInvestment.setSource(smallcaseDetail.getSource());

    // Add stock constituents in a map for simplifying and speeding fetch based on sid
    List<String> sids = new ArrayList<>();
    for (Constituents constituents : smallcaseDetail.getCurrentConfig().getConstituents()) {
      Stock stock = new Stock();
      stock.setSid(constituents.getSid());
      stock.setStockName(constituents.getSidInfo().getName());
      stock.setShares(constituents.getShares());
      investedStockList.add(stock);
      sids.add(stock.getSid());
    }

    viewContract.onSmallcaseInvestmentsReceived(smallcaseInvestment);
    stockHelper.getStockPrices(marketRepository, sharedPrefService, sids, this);
  }

  @Override
  public void onSmallcaseNewsReceived(List<News> newses) {
    if (null == newses || newses.isEmpty()) {
      viewContract.noNewsForSmallcase();
      return;
    }

    List<PPlainNews> newsList = new ArrayList<>();
    for (News news : newses) {
      PPlainNews plainNews = new PPlainNews();
      plainNews.setDate(AppUtils.getInstance().getTimeAgoDate(news.getDate()));
      plainNews.setLink(news.getLink());
      plainNews.setNewsHeading(news.getHeadline());
      plainNews.setNewsImage(news.getImageUrl());
      newsList.add(plainNews);
    }
    viewContract.onNewsReceived(newsList);
  }

  @Override
  public void onFailedFetchingSmallcaseDetails() {
    viewContract.showSnackBar(R.string.something_wrong);
  }

  @Override
  public void onFailedFetchingSmallcaseHistorical() {
    viewContract.showSnackBar(R.string.something_wrong);
  }

  @Override
  public void onFailedFetchingSmallcaseInvestments() {
    viewContract.showSnackBar(R.string.something_wrong);
  }

  @Override
  public void onFailedFetchingSmallcaseNews() {
    viewContract.showSnackBar(R.string.something_wrong);
  }

  @Override
  public void fetchHistorical() {
    if (!networkHelper.isNetworkAvailable()) {
      viewContract.showSnackBar(R.string.no_internet);
      return;
    }

    if (isDraft) {
      List<String> sids = new ArrayList<>();
      for (Stock stock : unInvestedStockList) {
        sids.add(stock.getSid());
      }

      sids.add(".NSEI");
      serviceContract.getStockHistorical(sids, viewContract.getDuration());
      return;
    }

    if (null == scid) {
      viewContract.showSnackBar(R.string.something_wrong);
      return;
    }

    serviceContract.getSmallcaseHistorical(scid, "index", ".NSEI", viewContract.getDuration());
  }

  @Override
  public SpannableString getMinInvestment() {
    SpannableString spannableString = new SpannableString("Minimum amount \n" + AppConstants
        .RUPEE_SYMBOL + String.format(Locale.getDefault(), "%.2f", minInvestment));
    spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primary_text)), 14,
        spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    spannableString.setSpan(new RelativeSizeSpan(1.25f), 14,
        spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    return spannableString;
  }

  @Override
  public DraftSmallcase getDraftSmallcase() {
    return draftSmallcase;
  }

  @Override
  public void setSource(String source) {
    this.source = source;
  }

  @Nullable
  @Override
  public String getSource() {
    return SmallcaseSource.PROFESSIONAL.equals(source) ? SmallcaseSource.CUSTOM : source;
  }

  @Override
  public ReBalanceUpdate getReBalanceUpdate() {
    return reBalanceUpdate;
  }

  @Override
  public void onFailedFetchingUserData() {
    viewContract.showSimpleSnackbar(R.string.something_wrong);
  }

  @Override
  public void onUserDataCached() {
    if (scid == null) {
      viewContract.showSimpleSnackbar(R.string.something_wrong);
      return;
    }

    if (!isInvested()) {
      // Previous assumption was that this smallcase is un-invested
      iScid = serviceContract.getIScid(scid);
      // Check if it's invested now (Number of reasons why this might happen. Like, the user brought
      // this smallcase on web, or status of brought smallcase was not received etc...)
      if (iScid != null) {
        viewContract.showInvestMore();
        serviceContract.getInvestedSmallcaseDetail(iScid);
        serviceContract.getPendingActions();
      }
    } else {
      // Previous assumption was that this smallcase is invested
      iScid = serviceContract.getIScid(scid);
      // Check if it's un-invested now
      if (iScid == null) {
        smallcaseStatus = "VALID";
        viewContract.showBuy();
        viewContract.unGreyBuyOrInvestMore();
        reBalanceUpdate = null;
      }
    }
  }

  @Override
  public void dontShowCustomizeInfoPopup() {
    sharedPrefService.dontShowCustomizeInfoPopup();
  }

  @Override
  public boolean shouldShowCustomizePopUp() {
    return !isDraft && sharedPrefService.shouldShowCustomizeInfoPopup();
  }

  @Override
  public void getDraftDetailData(String did, List<String> sids) {
    isDraft = true;
    serviceContract.getDraftDetails(did);
  }

  @Override
  public void onDraftDeleted() {
    viewContract.deleteSuccessful();
  }

  @Override
  public void onFailedDeletingDraft() {
    viewContract.hideLoader();
    viewContract.showSimpleSnackbar(R.string.something_wrong);
  }

  @Override
  public boolean isDraft() {
    return isDraft;
  }

  @Override
  public void onDraftFetchedSuccessfully(DraftSmallcase draftSmallcase) {
    this.draftSmallcase = draftSmallcase;
    unInvestedStockList = new ArrayList<>();
    did = draftSmallcase.getDid() == null ? draftSmallcase.get_id() : draftSmallcase.getDid();
    source = draftSmallcase.getSource();

    String index = String.format(Locale.getDefault(), "%.2f", draftSmallcase.getStats().getIndexValue());
    boolean isIndexUp = draftSmallcase.getStats().getIndexValue() >= 100;

    compositionScheme = draftSmallcase.getCompositionScheme();
    PSmallcaseDetail smallcaseDetail = new PSmallcaseDetail();
    smallcaseDetail.setSmallcaseName(draftSmallcase.getInfo().getName());
    smallcaseDetail.setSmallcaseImage("187", draftSmallcase.getSource(), draftSmallcase.getScid());
    smallcaseDetail.setSmallcaseDescription(draftSmallcase.getInfo().getShortDescription());
    smallcaseDetail.setSmallcaseIndex(index);
    smallcaseDetail.setIndexUp(isIndexUp);
    smallcaseDetail.setSource(draftSmallcase.getSource());

    HashMap<String, List<PStockWeight>> segments = new HashMap<>();
    HashMap<String, String> constituents = new HashMap<>();
    for (Segments segment : draftSmallcase.getSegments()) {
      for (String constituent : segment.getConstituents()) {
        constituents.put(constituent, segment.getLabel());
      }
    }

    List<String> sids = new ArrayList<>();
    for (int i = 0; i < draftSmallcase.getConstituents().size(); i++) {
      Constituents constituent = draftSmallcase.getConstituents().get(i);

      Stock stock = new Stock();
      stock.setStockName(constituent.getSidInfo().getName());
      stock.setWeight(constituent.getWeight());
      stock.setShares(constituent.getShares());
      stock.setSid(constituent.getSid());
      stock.setLocked(constituent.isLocked());

      sids.add(stock.getSid());

      PStockWeight PStockWeight = new PStockWeight();
      PStockWeight.setStockName(constituent.getSidInfo().getName());
      PStockWeight.setWeightage(constituent.getWeight() * 100 > 100 ? 100 : constituent.getWeight() * 100);
      PStockWeight.setSid(constituent.getSid());
      PStockWeight.setSector(constituent.getSidInfo().getSector());
      PStockWeight.setLocked(constituent.isLocked());
      unInvestedStockList.add(stock);

      String label = constituents.get(constituent.getSid());

      if (segments.containsKey(label)) {
        List<PStockWeight> PStockWeights = segments.get(label);
        PStockWeights.add(PStockWeight);
      } else {
        List<PStockWeight> PStockWeights = new ArrayList<>();
        PStockWeights.add(PStockWeight);
        segments.put(label, PStockWeights);
      }
    }

    stockHelper.getStockPrices(marketRepository, sharedPrefService, sids, this);

    if (SmallcaseTier.PREMIUM.equals(draftSmallcase.getInfo().getTier())) {
      isSmallcasePremium = true;
      viewContract.showPremium();
    }

    double indexDifference = draftSmallcase.getStats().getIndexValue() - 100;
    String currentIndex = "Current Index Value: " + index;
    String inception = "\n\nThis smallcase has moved " + (indexDifference >= 0 ? "up " : "down ")
        + String.format(Locale.getDefault(), "%.2f", indexDifference) + "% since its inception on " +
        simpleDateFormat.format(AppUtils.getInstance().getPlainDate(draftSmallcase.getInfo().getDateUpdated()));

    SpannableString smallcaseInceptionInfo = new SpannableString(currentIndex + inception);
    smallcaseInceptionInfo.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(activity, AppConstants.FontType.MEDIUM)),
        0, 21, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    smallcaseDetail.setSmallcaseInceptionInfo(smallcaseInceptionInfo);

    smallcaseStatus = "VALID";
    smallcaseDetail.setSegmentMap(segments);
    viewContract.onDraftDetailsReceived(smallcaseDetail);
    viewContract.unGreyBuyOrInvestMore();
    viewContract.noNewsForSmallcase();
  }

  @Override
  public void onFailedFetchingDraft() {
    viewContract.showSnackBar(R.string.something_wrong);
  }

  @Override
  public void onStockHistoricalReceived(ResponseBody responseBody) throws JSONException, IOException {
    JSONObject response = new JSONObject(responseBody.string());
    JSONObject data = response.getJSONObject("data");
    Gson gson = new Gson();

    JSONObject niftySid = data.getJSONObject(".NSEI");
    Historical niftyHistorical = gson.fromJson(niftySid.toString(), Historical.class);
    List<Points> niftyPointsList = new ArrayList<>(niftyHistorical.getPoint());

    SmallcaseHelper smallcaseHelper = new SmallcaseHelper();
    List<Historical> historicalList = new ArrayList<>();
    List<Stock> stockList = new ArrayList<>();

    for (Stock stock : unInvestedStockList) {
      JSONObject stockSid = data.getJSONObject(stock.getSid());
      Historical historical = gson.fromJson(stockSid.toString(), Historical.class);
      historicalList.add(historical);
      stockList.add(stock);
    }
    StockListWithMinAmount stockListWithMinAmount = stockHelper.calculateSharesAndMinAmountFromWeight(stockList, -1);
    calculateHistorical(smallcaseHelper.createHistoricalForStocks(historicalList, stockListWithMinAmount.getStockList(), niftyPointsList), niftyPointsList);
  }

  private void calculateHistorical(List<Points> pointsList, List<Points> niftyPointsList) {
    if (pointsList.isEmpty() || niftyPointsList.isEmpty() || pointsList.get(0).getIndex() == 0)
      return;

    List<PPoints> smallcasePoints = new ArrayList<>();
    double baseSmallcaseIndex = 100 / pointsList.get(0).getIndex(); // Convert index to be base 100
    double baseNiftyIndex = 100 / niftyPointsList.get(0).getPrice(); // Convert price to be base 100

    long daysInMilli = 1000 * 60 * 60 * 24; // 1000 millSec; 60 sec; 60 mints; 24 hours
    Calendar calendar = Calendar.getInstance();
    int lastPosition = pointsList.size() - 1;
    calendar.setTime(AppUtils.getInstance().getPlainDate(pointsList.get(lastPosition).getDate()));
    calendar.add(Calendar.MONTH, -1);

    for (int i = lastPosition; i >= 0; i--) {
      Points smallcasePoint = pointsList.get(i);
      Date date = AppUtils.getInstance().getPlainDate(smallcasePoint.getDate());

      long difference = date.getTime() - calendar.getTime().getTime();
      long elapsedDays = difference / daysInMilli;
      if (elapsedDays < 0) {
        if (oneMonthReturn == -999) {
          oneMonthReturn = ((pointsList.get(lastPosition).getIndex() - smallcasePoint
              .getIndex()) / smallcasePoint.getIndex()) * 100;
          calendar.add(Calendar.MONTH, -11);
        } else if (oneYearReturn == -999) {
          oneYearReturn = ((pointsList.get(lastPosition).getIndex() - smallcasePoint
              .getIndex()) / smallcasePoint.getIndex()) * 100;
        }
      }

      Points niftyPoint;
      try {
        niftyPoint = niftyPointsList.get(i);
      } catch (ArrayIndexOutOfBoundsException e) {
        // Break out if nifty price points size is not equal to smallcase index points (Should always be equal)
        return;
      }

      PPoints presentablePoint = new PPoints();
      presentablePoint.setOriginalDate(date);
      presentablePoint.setDate(simpleDateFormat.format(date));
      presentablePoint.setSmallcaseIndex(baseSmallcaseIndex * smallcasePoint.getIndex());
      presentablePoint.setNiftyIndex(baseNiftyIndex * niftyPoint.getPrice());
      smallcasePoints.add(0, presentablePoint);
    }

    String oneMonth = String.format(Locale.getDefault(), "%.2f", oneMonthReturn) + "%";
    String oneYear = String.format(Locale.getDefault(), "%.2f", oneYearReturn) + "%";
    boolean isMonthPositive = oneMonthReturn > 0;
    boolean isYearPositive = oneYearReturn > 0;

    viewContract.showReturns(oneMonth, oneYear, isMonthPositive, isYearPositive);
    viewContract.onHistoricalReceived(smallcasePoints);
  }

  @Override
  public void onFailedFetchingStockHistorical() {
    viewContract.showSnackBar(R.string.something_wrong);
  }

  @Override
  public void deleteDraft() {
    HashMap<String, String> body = new HashMap<>();
    body.put("did", did);
    serviceContract.deleteDraft(body);
  }

  @Override
  public boolean isReBalancePending() {
    return reBalanceUpdate != null;
  }
}
