package com.smallcase.android.smallcase.discover;

import android.support.annotation.Nullable;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;

import java.util.List;
import java.util.Set;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 03/02/17.
 */

class DiscoverSmallcaseService implements DiscoverSmallcaseContract.Service {
    private static final String TAG = "DiscoverSmallcaseService";

    private SmallcaseRepository smallcaseRepository;
    private DiscoverSmallcaseContract.Presenter presenterContract;
    private SharedPrefService sharedPrefService;
    private CompositeSubscription compositeSubscription;

    DiscoverSmallcaseService(SmallcaseRepository smallcaseRepository, DiscoverSmallcaseContract.Presenter presenterContract,
                             SharedPrefService sharedPrefService) {
        this.smallcaseRepository = smallcaseRepository;
        this.presenterContract = presenterContract;
        this.sharedPrefService = sharedPrefService;
        compositeSubscription = new CompositeSubscription();
    }

    @Nullable
    @Override
    public String getAuth() {
        return sharedPrefService.getAuthorizationToken();
    }

    @Override
    public void getSmallcases(final String auth, @Nullable List<String> types, @Nullable List<String> scids, @Nullable String sortBy, int
            sortOrder, int offSet, int count, @Nullable Integer minMinInvestAmount, @Nullable Integer maxMinInvestAmount, @Nullable String
            searchString, @Nullable String sid, @Nullable String tier) {
        compositeSubscription.add(smallcaseRepository.getSmallcases(auth, types, scids, sortBy, sortOrder, offSet, count, minMinInvestAmount,
                maxMinInvestAmount, searchString, sharedPrefService.getCsrfToken(), sid, tier)
                .subscribe(smallcases -> presenterContract.onSmallcasesReceived(smallcases), throwable -> {
                    SentryAnalytics.getInstance().captureEvent(auth, throwable, TAG + "; getSmallcases()");
                    presenterContract.onFailedFetchingSmallcases();
                }));
    }

    @Override
    public Set<String> getInvestedScids() {
        return sharedPrefService.getInvestedScidsWithIScids();
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }
}
