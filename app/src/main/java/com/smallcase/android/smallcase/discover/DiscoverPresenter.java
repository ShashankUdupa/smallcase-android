package com.smallcase.android.smallcase.discover;

import android.content.Context;
import android.support.annotation.VisibleForTesting;

import com.smallcase.android.R;
import com.smallcase.android.data.model.Collection;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.Smallcase;
import com.smallcase.android.data.model.Smallcases;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.model.PCollection;
import com.smallcase.android.view.model.PFeaturedSmallcase;
import com.smallcase.android.view.model.PNewsSmallcase;
import com.smallcase.android.view.model.PSmallcase;
import com.smallcase.android.view.model.PSmallcaseWithNews;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by shashankm on 24/01/17.
 */

class DiscoverPresenter implements DiscoverContract.Presenter {
    private static final String TAG = "DiscoverPresenter";

    private DiscoverContract.Service serviceContract;
    private DiscoverContract.View viewContract;
    private List<String> investedScids;
    private NetworkHelper networkHelper;

    DiscoverPresenter(Context context, DiscoverContract.View viewContract, NetworkHelper networkHelper) {
        this.serviceContract = new DiscoverService(new SmallcaseRepository(), new SharedPrefService(context), this);
        this.viewContract = viewContract;
        this.networkHelper = networkHelper;
    }

    /**
     * Used in testing to mock service contract. Do not use anywhere else
     * @param serviceContract mocked service interface
     * @param viewContract mocked view interface
     * @param networkHelper mocked network helper
     */
    @VisibleForTesting
    DiscoverPresenter(DiscoverContract.Service serviceContract, DiscoverContract.View viewContract, NetworkHelper networkHelper) {
        this.serviceContract = serviceContract;
        this.viewContract = viewContract;
        this.networkHelper = networkHelper;
    }

    @Override
    public void getDiscoverPageItems() {
        String auth = serviceContract.getAuthToken();
        if (null == auth) return;

        investedScids = new ArrayList<>(serviceContract.getInvestedScids());
        if (!networkHelper.isNetworkAvailable()) {
            viewContract.showNoNetwork();
            onFeaturedSmallcasesReceived(serviceContract.getCachedFeaturedSmallcases());
            onCollectionsReceived(serviceContract.getCachedCollections());
            onRecentSmallcasesReceived(serviceContract.getCachedRecentSmallcases());
            onRecentNewsFetched(serviceContract.getCachedRecentNews());
            onPopularSmallcasesFetched(serviceContract.getCachedPopularSmallcases());
            return;
        }

        serviceContract.getFeaturedSmallcases(auth);
        serviceContract.getCollections(auth);
        serviceContract.getSmallcases(null, null, "uploadDate", -1, 0, 10, null, null, null, null);
        serviceContract.getRecentNews(auth, 5, 0);
        serviceContract.getPopularSmallcases(auth, 10, 0);
    }

    @Override
    public void onFeaturedSmallcasesReceived(final List<Smallcase> smallcases) {
        List<PFeaturedSmallcase> pFeaturedSmallcases = new ArrayList<>();
        for (Smallcase smallcase : smallcases) {
            List<String> scids = new ArrayList<>();
            for (String scid : investedScids) {
                scids.add(scid.split("#")[0]);
            }
            boolean isInvested = scids.contains(smallcase.getScid());
            String indexValue = String.format(Locale.getDefault(), "%.2f", smallcase.getStats().getIndexValue());
            boolean isIndexUp = smallcase.getStats().getIndexValue() > smallcase.getStats().getLastCloseIndex();

            PFeaturedSmallcase featuredViewModel = new PFeaturedSmallcase("187", smallcase.getInfo().getName(),
                    smallcase.getFlags().getHighlighted().getDescription(), smallcase.getFlags()
                    .getHighlighted().getLabel().toUpperCase(), indexValue, isInvested, isIndexUp, smallcase.getScid());
            pFeaturedSmallcases.add(featuredViewModel);
        }

        viewContract.onFeaturedSmallcaseReceived(pFeaturedSmallcases);
    }

    @Override
    public void onCollectionsReceived(List<Collection> collections) {
        List<PCollection> pCollections = new ArrayList<>();
        for (Collection collection : collections) {
            List<String> scids = new ArrayList<>();

            if (null != collection.getGroups() && !collection.getGroups().isEmpty()) {
                for (Smallcases smallcase : collection.getGroups().get(0).getSmallcases()) {
                    scids.add(smallcase.getScid());
                }
            }

            String smallcasesCount = String.valueOf(collection.getSmallcaseCount()) + " smallcases";
            PCollection pCollection = new PCollection(collection.getName(), collection.getCid(),
                    collection.getDescription(), smallcasesCount, scids);
            pCollections.add(pCollection);
        }
        viewContract.onCollectionsReceived(pCollections);
    }

    @Override
    public void onRecentSmallcasesReceived(List<Smallcase> smallcases) {
        List<PSmallcase> pSmallcases = new ArrayList<>();
        for (Smallcase smallcase : smallcases) {
            List<String> scids = new ArrayList<>();
            for (String scid : investedScids) {
                scids.add(scid.split("#")[0]);
            }
            boolean isInvested = scids.contains(smallcase.getScid());
            String indexValue = String.format(Locale.getDefault(), "%.2f", smallcase.getStats().getIndexValue());
            boolean isIndexUp = smallcase.getStats().getIndexValue() > smallcase.getStats().getLastCloseIndex();

            pSmallcases.add(new PSmallcase(smallcase.getScid(), "130", smallcase.getInfo().getName(), indexValue,
                    isIndexUp, isInvested, smallcase.getInfo().getType(), smallcase.getInfo().getTier()));
        }
        viewContract.onRecentSmallcasesReceived(pSmallcases);
    }

    @Override
    public void onRecentNewsFetched(List<News> newses) {
        if (newses.isEmpty()) {
            viewContract.hideRecentNews();
            return;
        }

        List<PSmallcaseWithNews> smallcaseWithNewses = new ArrayList<>();
        for (News news : newses) {
            List<PNewsSmallcase> newsSmallcases = new ArrayList<>();
            for (Smallcases smallcase : news.getSmallcases()) {
                double change = ((smallcase.getStats().getIndexValue() - smallcase.getInitialIndex()) / smallcase.getInitialIndex()) * 100;
                boolean isChangePositive = change > 0;
                String changePercent = String.format(Locale.getDefault(), "%.2f", change) + "%";
                PNewsSmallcase newsSmallcase = new PNewsSmallcase("80", smallcase.getInfo().getName(), changePercent,
                        smallcase.get_id(), smallcase.getScid(), isChangePositive, smallcase.getInfo().getTier());
                newsSmallcases.add(newsSmallcase);
            }
            PSmallcaseWithNews smallcaseWithNews = new PSmallcaseWithNews(news.getImageUrl(), news.getHeadline(),
                    news.getDate(), newsSmallcases, news.getLink());
            smallcaseWithNewses.add(smallcaseWithNews);
        }
        viewContract.onRecentNewsReceived(smallcaseWithNewses);
    }

    @Override
    public void onPopularSmallcasesFetched(List<Smallcase> smallcases) {
        List<PSmallcase> pSmallcases = new ArrayList<>();
        List<String> scids = new ArrayList<>();
        for (String scidWithIscid : serviceContract.getInvestedScids()) {
            scids.add(scidWithIscid.split("#")[0]);
        }
        for (Smallcase smallcase : smallcases) {
            boolean isInvested = scids.contains(smallcase.getScid());
            String indexValue = String.format(Locale.getDefault(), "%.2f", smallcase.getStats().getIndexValue());
            boolean isIndexUp = smallcase.getStats().getIndexValue() > smallcase.getStats().getLastCloseIndex();

            pSmallcases.add(new PSmallcase(smallcase.getScid(), "130", smallcase.getInfo().getName(), indexValue,
                    isIndexUp, isInvested, smallcase.getInfo().getType(), smallcase.getInfo().getTier()));
        }
        viewContract.onPopularSmallcasesReceived(pSmallcases);
    }

    @Override
    public void onFailedFetchingFeaturedSmallcases() {
        if (!serviceContract.fetchedPopularSmallcases() && !serviceContract.fetchedCollection() && !serviceContract.fetchedNews()) {
            viewContract.showTryAgain();
            return;
        }
        viewContract.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onFailedFetchingPopularSmallcases() {
        if (!serviceContract.fetchedFeaturedSmallcases() && !serviceContract.fetchedCollection() && !serviceContract.fetchedNews()) {
            viewContract.showTryAgain();
            return;
        }
        viewContract.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void destroySubscriptions() {
        serviceContract.destroySubscriptions();
    }

    @Override
    public void onFailedFetchingCollection() {
        if (!serviceContract.fetchedNews() && !serviceContract.fetchedPopularSmallcases() && !serviceContract.fetchedFeaturedSmallcases()) {
            viewContract.showTryAgain();
            return;
        }
        viewContract.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onFailedFetchingRecentSmallcases() {
        if (!serviceContract.fetchedNews() && !serviceContract.fetchedPopularSmallcases() && !serviceContract.fetchedFeaturedSmallcases()) {
            viewContract.showTryAgain();
            return;
        }
        viewContract.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onFailedFetchingNews() {
        if (!serviceContract.fetchedCollection() && !serviceContract.fetchedPopularSmallcases() && !serviceContract.fetchedFeaturedSmallcases()) {
            viewContract.showTryAgain();
            return;
        }
        viewContract.hideRecentNews();
        viewContract.showSnackBar(R.string.something_wrong);
    }
}
