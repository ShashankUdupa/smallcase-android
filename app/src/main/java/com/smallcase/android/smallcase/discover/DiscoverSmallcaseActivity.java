package com.smallcase.android.smallcase.discover;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.model.SearchResult;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.SmallcaseTier;
import com.smallcase.android.view.android.DiscoverScrollListener;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PCollection;
import com.smallcase.android.view.model.PExpandedSmallcase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DiscoverSmallcaseActivity extends AppCompatActivity implements DiscoverSmallcaseContract.View,
        DiscoverScrollListener.ScrollInterface, SearchStocksAdapter.ItemClickListener {
    public static final String SORT_ORDER = "sort_order";
    public static final String SORT_BY = "sort_by";
    public static final String POPULARITY = "popularity";
    public static final String RECENT = "recent";
    public static final String TIER = "tier";
    public static final String RE_BALANCE_DATE = "rebalanceDate";
    public static final String ONE_YEAR_RETURN = "yearlyReturns";
    public static final String ONE_MONTH_RETURN = "monthlyReturns";
    public static final String MIN_INVESTMENT = "minInvestAmount";
    public static final String THEMATIC = "Thematic";
    public static final String SECTOR_TRACKERS = "Sector Trackers";
    public static final String MODEL_BASED = "Model-based";
    public static final String SCIDS = "scids";
    public static final String COLLECTION = "collection";
    public static final String SEARCH = "search";

    private static final String TAG = "DiscoverSmallcaseActivity";
    private static final int SPEECH_INPUT = 1;
    private static final AccelerateDecelerateInterpolator ACCELERATE_DECELERATE_INTERPOLATOR = new AccelerateDecelerateInterpolator();

    @BindView(R.id.text_tool_bar_title)
    GraphikText toolBarTitle;
    @BindView(R.id.action_icon)
    ImageView searchIcon;
    @BindView(R.id.smallcases_list)
    RecyclerView smallcasesList;
    @BindView(R.id.filter_sort_card)
    CardView filterSortCard;
    @BindView(R.id.sort_card)
    CardView sortCard;
    @BindView(R.id.filter_image)
    ImageView filterImage;
    @BindView(R.id.filter_text)
    GraphikText filterText;
    @BindView(R.id.filter_card)
    CardView filterCard;
    @BindView(R.id.thematic)
    AppCompatCheckBox thematic;
    @BindView(R.id.model_based)
    AppCompatCheckBox modelBased;
    @BindView(R.id.sector_trackers)
    AppCompatCheckBox sectorTracker;
    @BindView(R.id.blur)
    View blur;
    @BindView(R.id.search_layout)
    CardView searchContainer;
    @BindView(R.id.search_text)
    EditText searchText;
    @BindView(R.id.empty_state_container)
    View noSmallcasesContainer;
    @BindView(R.id.text_empty_state)
    GraphikText noSmallcasesText;
    @BindView(R.id.discover_smallcase_parent)
    CoordinatorLayout parent;
    @BindView(R.id.min_investment_container)
    RadioGroup minInvestmentGroup;
    @BindView(R.id.hide_invested_smallcase)
    AppCompatCheckBox hideInvestedSmallcases;
    @BindView(R.id.reverse_switch)
    Switch reverseSwitch;
    @BindView(R.id.latest)
    GraphikText latest;
    @BindView(R.id.one_m_return)
    GraphikText oneMonthReturn;
    @BindView(R.id.one_y_return)
    GraphikText oneYearReturn;
    @BindView(R.id.popularity)
    GraphikText popularity;
    @BindView(R.id.min_investment)
    GraphikText minInvestment;
    @BindView(R.id.empty_state_icon)
    ImageView noSearchResultIcon;
    @BindView(R.id.recently_rebalanced)
    GraphikText recentlyReBalanced;
    @BindView(R.id.search_stock_result_layout)
    CardView searchStockResultLayout;
    @BindView(R.id.search_stock_list)
    RecyclerView searchStocksList;
    @BindView(R.id.select_smallcase)
    AppCompatCheckBox selectSmallcase;

    private SmallcasesAdapter smallcasesAdapter;
    private DiscoverSmallcaseContract.Presenter presenterContract;
    private int sortOrder = -1;
    private String sortBy;
    private boolean fetching = true;
    private String searchString = null;
    private List<String> types;
    private List<String> scids;
    private Animations animations;
    private int range = -1;
    private GraphikText selectedSort;
    private final int marginLeft = (int) AppUtils.getInstance().dpToPx(14);
    private AnalyticsContract analyticsContract;
    private SearchStocksAdapter searchStocksAdapter;
    private String sid;
    private int offSet = 0;
    private boolean shouldFetchStocks = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover_smallcase);
        ButterKnife.bind(this);

        // Set typeface for radio button and edit texts.
        setTypeface();

        animations = new Animations();
        types = new ArrayList<>();
        scids = new ArrayList<>();

        analyticsContract = new AnalyticsManager(getApplicationContext());
        PCollection collection = getIntent().getParcelableExtra(COLLECTION);
        toolBarTitle.setText(null == collection ? "All smallcases" : collection.getCollectionName());
        searchIcon.setVisibility(View.VISIBLE);
        searchIcon.setImageResource(R.drawable.search_icon);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        smallcasesList.setLayoutManager(layoutManager);
        smallcasesList.setHasFixedSize(true);

        searchStocksList.setLayoutManager(new LinearLayoutManager(this));
        searchStocksList.setHasFixedSize(true);

        sortOrder = getIntent().getIntExtra(SORT_ORDER, -1);
        sortBy = null == getIntent().getStringExtra(SORT_BY) ? RECENT : getIntent().getStringExtra(SORT_BY);

        setSortBy();
        scids = getIntent().getStringArrayListExtra(SCIDS);

        thematic.setChecked(getIntent().getBooleanExtra(THEMATIC, false));
        modelBased.setChecked(getIntent().getBooleanExtra(MODEL_BASED, false));
        sectorTracker.setChecked(getIntent().getBooleanExtra(SECTOR_TRACKERS, false));

        smallcasesList.addOnScrollListener(new DiscoverScrollListener(layoutManager, this) {
            @Override
            public void onHide() {
                hideShowFilterSortCard(200f);
            }

            @Override
            public void onShow() {
                hideShowFilterSortCard(0);
            }
        });

        RecyclerView.ItemAnimator animator = smallcasesList.getItemAnimator();

        //Disable default grey flicker "animation" when an item changes.
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        minInvestmentGroup.setOnCheckedChangeListener((group, checkedId) -> range = checkedId);

        reverseSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) sortOrder = 1;
            else sortOrder = -1;
        });

        if (Build.VERSION.SDK_INT < 21) removeCardElevation();

        smallcasesAdapter = new SmallcasesAdapter(this, collection, analyticsContract, "");
        smallcasesList.setAdapter(smallcasesAdapter);

        presenterContract = new DiscoverSmallcasePresenter(this, new SmallcaseRepository(), new SharedPrefService(this),
                this, new NetworkHelper(this), new StockHelper(), new MarketRepository());

        if (getIntent().getBooleanExtra(SEARCH, false)) search();

        if (!getTypes().isEmpty()) {
            // Filters applied
            filterText.setTextColor(ContextCompat.getColor(this, R.color.blue_800));
            filterImage.setImageResource(R.drawable.filter_selected);
        }

        if (getIntent().getData() != null && getIntent().getData().getQueryParameter("stock") != null &&
                getIntent().getData().getQueryParameter("sName") != null) {
            searchContainer.setVisibility(View.VISIBLE);
            showSearch();
            onItemClicked(getIntent().getData().getQueryParameter("stock"), getIntent().getData().getQueryParameter("sName"));
            return;
        }

        presenterContract.fetchSmallcases(null);
    }

    private void setTypeface() {
        RadioButton belowFive = findViewById(R.id.below_five);
        RadioButton betweenFiveAndTen = findViewById(R.id.between_five_and_ten);
        RadioButton betweenTenAndTwenty = findViewById(R.id.between_ten_and_twenty);
        RadioButton aboveTwenty = findViewById(R.id.above_twenty);
        belowFive.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM));
        betweenFiveAndTen.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM));
        betweenTenAndTwenty.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM));
        aboveTwenty.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM));
        searchText.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR));
    }

    private void removeCardElevation() {
        filterSortCard.setMaxCardElevation(0);
        filterSortCard.setCardElevation(0);

        searchContainer.setMaxCardElevation(0);
        searchContainer.setCardElevation(0);

        searchStockResultLayout.setMaxCardElevation(0);
        searchStockResultLayout.setCardElevation(0);

        sortCard.setCardElevation(0);
        filterCard.setCardElevation(0);

        sortCard.setMaxCardElevation(0);
        filterCard.setMaxCardElevation(0);
    }

    private void setSortBy() {
        switch (sortBy) {
            case RECENT:
                selectedSort = latest;
                break;

            case POPULARITY:
                selectedSort = popularity;
                break;

            case ONE_MONTH_RETURN:
                selectedSort = oneMonthReturn;
                break;

            case ONE_YEAR_RETURN:
                selectedSort = oneYearReturn;
                break;

            case MIN_INVESTMENT:
                selectedSort = minInvestment;
                break;

            case RE_BALANCE_DATE:
                selectedSort = recentlyReBalanced;
                break;
        }
        selectedSort.setTextColor(ContextCompat.getColor(this, R.color.blue_800));
    }

    @OnClick(R.id.back)
    void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (searchContainer.getVisibility() == View.VISIBLE) {
            if (Build.VERSION.SDK_INT >= 21) searchContainer.post(animateHide);
            else closeSearch();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        presenterContract.destroySubscriptions();
        super.onDestroy();
    }

    @OnClick(R.id.action_icon)
    public void search() {
        if (Build.VERSION.SDK_INT >= 21) {
            searchContainer.post(animateShow);
        } else {
            searchContainer.setVisibility(View.VISIBLE);
            showSearch();
        }
    }

    @OnClick(R.id.speech_icon)
    public void speechIcon() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak Now");
        try {
            startActivityForResult(intent, SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(), "Not supported", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.back_search)
    public void backInSearch() {
        if (Build.VERSION.SDK_INT >= 21) searchContainer.post(animateHide);
        else closeSearch();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && null != data && requestCode == SPEECH_INPUT) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            searchText.setText(result.get(0));
            InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (keyboard != null) keyboard.showSoftInput(searchText, 0);
            searchText.selectAll();
        }
    }

    @OnClick(R.id.sort)
    public void sortCard() {
        //Check if filter card is visible and close it
        if (filterCard.getVisibility() == View.VISIBLE) {
            toggleFilterCard(View.GONE);
        }

        if (sortCard.getVisibility() == View.VISIBLE) {
            blur.setClickable(false);
            blur.setVisibility(View.GONE);
            toggleSortCard(View.GONE);
        } else {
            blur.setVisibility(View.VISIBLE);
            blur.setClickable(true);
            blur.setOnClickListener(v -> sortCard());
            toggleSortCard(View.VISIBLE);
        }
    }

    @OnClick(R.id.filter)
    public void filterCard() {
        //Check if sort card is visible and close it
        if (sortCard.getVisibility() == View.VISIBLE) {
            toggleSortCard(View.GONE);
        }

        if (filterCard.getVisibility() == View.VISIBLE) {
            blur.setClickable(false);
            blur.setVisibility(View.GONE);
            toggleFilterCard(View.GONE);
        } else {
            blur.setVisibility(View.VISIBLE);
            blur.setClickable(true);
            blur.setOnClickListener(v -> filterCard());
            toggleFilterCard(View.VISIBLE);
        }
    }

    @OnClick(R.id.apply)
    public void applyFilter() {
        blur.setClickable(false);
        blur.setVisibility(View.GONE);
        toggleFilterCard(View.GONE);
    }

    @OnClick(R.id.clear)
    public void clearFilter() {
        thematic.setChecked(false);
        modelBased.setChecked(false);
        sectorTracker.setChecked(false);
        selectSmallcase.setChecked(false);

        minInvestmentGroup.clearCheck();

        hideInvestedSmallcases.setChecked(false);
        blur.setClickable(false);
        blur.setVisibility(View.GONE);
        toggleFilterCard(View.GONE);
    }

    @OnClick({R.id.latest, R.id.one_m_return, R.id.popularity, R.id.one_y_return, R.id.min_investment, R.id.recently_rebalanced})
    public void sortItemClicked(View view) {
        selectedSort.setTextColor(ContextCompat.getColor(this, R.color.primary_text));
        switch (view.getId()) {
            case R.id.latest:
                sortBy = RECENT;
                selectedSort = latest;
                break;

            case R.id.one_m_return:
                sortBy = ONE_MONTH_RETURN;
                selectedSort = oneMonthReturn;
                break;

            case R.id.popularity:
                sortBy = POPULARITY;
                selectedSort = popularity;
                break;

            case R.id.one_y_return:
                sortBy = ONE_YEAR_RETURN;
                selectedSort = oneYearReturn;
                break;

            case R.id.min_investment:
                sortBy = MIN_INVESTMENT;
                selectedSort = minInvestment;
                break;

            case R.id.recently_rebalanced:
                sortBy = RE_BALANCE_DATE;
                selectedSort = recentlyReBalanced;
                break;
        }

        selectedSort.setTextColor(ContextCompat.getColor(this, R.color.blue_800));
        blur.setClickable(false);
        blur.setVisibility(View.GONE);
        toggleSortCard(View.GONE);
    }

    @OnClick({R.id.thematic_text, R.id.model_text, R.id.sector_text, R.id.hide_invested_smallcase_text,
            R.id.select_smallcase_text})
    public void onCheckBoxTextClicked(View view) {
        switch (view.getId()) {
            case R.id.thematic_text:
                thematic.setChecked(!thematic.isChecked());
                break;

            case R.id.model_text:
                modelBased.setChecked(!modelBased.isChecked());
                break;

            case R.id.sector_text:
                sectorTracker.setChecked(!sectorTracker.isChecked());
                break;

            case R.id.hide_invested_smallcase_text:
                hideInvestedSmallcases.setChecked(!hideInvestedSmallcases.isChecked());
                break;

            case R.id.select_smallcase_text:
                selectSmallcase.setChecked(!selectSmallcase.isChecked());
                break;
        }
    }

    @Override
    public List<String> getTypes() {
        types.clear();
        if (thematic.isChecked()) {
            types.add(THEMATIC);
        }

        if (modelBased.isChecked()) {
            types.add(MODEL_BASED);
        }

        if (sectorTracker.isChecked()) {
            types.add(SECTOR_TRACKERS);
        }


        return types;
    }

    @Override
    public String getSortBy() {
        return sortBy;
    }

    @Override
    public int getSortOrder() {
        if (sortBy.equals(POPULARITY) || sortBy.equals(MIN_INVESTMENT)) {
            return sortOrder == 1 ? -1 : 1; //Return opposite for popularity and min investment ( Api :( )
        }
        return sortOrder;
    }

    @Override
    public int getOffSet() {
        return offSet;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public Integer getMinInvestedAmount() {
        switch (range) {
            case R.id.between_five_and_ten:
                return 5000;

            case R.id.between_ten_and_twenty:
                return 10000;

            case R.id.above_twenty:
                return 20000;

            case R.id.below_five:
            default:
                return null;
        }
    }

    @Override
    public Integer getMaxInvestedAmount() {
        switch (range) {
            case R.id.between_five_and_ten:
                return 10000;

            case R.id.between_ten_and_twenty:
                return 20000;

            case R.id.below_five:
                return 5000;

            case R.id.above_twenty:
            default:
                return null;
        }
    }

    @Override
    public String getSearchString() {
        return searchString;
    }

    @Override
    public boolean shouldHideInvestedSmallcase() {
        return hideInvestedSmallcases.isChecked();
    }

    @Override
    public List<String> getScids() {
        return scids;
    }

    @Override
    public void smallcasesRemaining() {
        smallcasesAdapter.setItemsRemaining();
    }

    @Override
    public void onStocksReceivedFromSearch(List<SearchResult> searchResults) {
        searchStockResultLayout.setVisibility(View.VISIBLE);
        if (searchStocksAdapter == null) {
            searchStocksAdapter = new SearchStocksAdapter(searchResults, this);
            searchStocksList.setAdapter(searchStocksAdapter);
        } else {
            searchStocksAdapter.updateList(searchResults);
        }
    }

    @Override
    public void updateOffSet(int size) {
        offSet += size;
    }

    @Override
    public String getTier() {
        return selectSmallcase.isChecked() ? SmallcaseTier.PREMIUM : null;
    }

    @Override
    public void showSmallcases(List<PExpandedSmallcase> smallcaseList) {
        if (noSmallcasesContainer.getVisibility() == View.VISIBLE) {
            noSmallcasesContainer.setVisibility(View.GONE);
        }
        fetching = false;
        shouldFetchStocks = true;
        smallcasesAdapter.onSmallcasesReceived(smallcaseList);
    }

    @Override
    public void noMoreSmallcases() {
        shouldFetchStocks = true;
        smallcasesAdapter.noMoreSmallcases();
    }

    @Override
    public void noSmallcases(SpannableString resonString) {
        shouldFetchStocks = true;
        smallcasesAdapter.finishedFetchingSmallcases();
        noSmallcasesContainer.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(R.drawable.no_search_result)
                .into(noSearchResultIcon);
        noSmallcasesText.setText(resonString);
    }

    @Override
    public List<PExpandedSmallcase> getSmallcases() {
        return null == smallcasesAdapter.getSmallcases() ? Collections.<PExpandedSmallcase>emptyList() :
                smallcasesAdapter.getSmallcases();
    }

    @Override
    public void showSnackBar(int errId) {
        AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_LONG, errId);
    }

    @Override
    public void onListEndReached() {
        if (!fetching) {
            fetching = true;
            presenterContract.fetchSmallcases(sid);
        }
    }

    private void toggleSortCard(int visibility) {
        if (visibility == View.VISIBLE) {
            presenterContract.savePreviousSortSelection();
            animations.expand(sortCard, ACCELERATE_DECELERATE_INTERPOLATOR, 250);
        } else {
            animations.collapse(sortCard, ACCELERATE_DECELERATE_INTERPOLATOR, 250);
            if (presenterContract.hasSortNotChanged()) {
                return;
            }

            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("sortedBy", getSortBy());
            eventProperties.put("sortOrder", getSortOrder());
            analyticsContract.sendEvent(eventProperties, "Sorted smallcases", Analytics.MIXPANEL);

            fetching = true;
            if (null != smallcasesAdapter.getSmallcases()) {
                smallcasesAdapter.getSmallcases().clear();
                smallcasesAdapter.notifyDataSetChanged();
            }

            offSet = 0;
            presenterContract.fetchSmallcases(null);
        }
    }

    private void hideShowFilterSortCard(float y) {
        filterSortCard.animate()
                .translationY(y)
                .setInterpolator(ACCELERATE_DECELERATE_INTERPOLATOR)
                .setDuration(350).start();
    }

    private void closeSearch() {
        sid = null;
        if (noSmallcasesContainer.getVisibility() == View.VISIBLE) {
            noSmallcasesContainer.setVisibility(View.GONE);
        }

        searchText.setText("");
        searchContainer.setVisibility(View.INVISIBLE);
        searchContainer.setClickable(false);
        searchStockResultLayout.setVisibility(View.GONE);

        blur.setVisibility(View.GONE);
        blur.setClickable(false);
        filterSortCard.setVisibility(View.VISIBLE);

        if (!smallcasesAdapter.reloadFetchedSmallcases()) {
            offSet = 0;
            presenterContract.fetchSmallcases(null);
            fetching = true;
        } else {
            offSet = smallcasesAdapter.getItemCount() - 1;
            fetching = false;
        }
        searchString = null;
        smallcasesAdapter.setItemsRemaining();
        shouldFetchStocks = true;
    }

    private void showSearch() {
        blur.setVisibility(View.VISIBLE);
        blur.setClickable(true);
        filterSortCard.setVisibility(View.GONE);

        searchText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) imm.showSoftInput(searchText, InputMethodManager.SHOW_IMPLICIT);

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s == null) return;

                String searchString = s.toString().trim();
                if (searchString.length() > 1 && shouldFetchStocks) {
                    if (searchStocksAdapter == null) {
                        searchStocksAdapter = new SearchStocksAdapter(new ArrayList<>(), DiscoverSmallcaseActivity.this);
                        searchStocksList.setAdapter(searchStocksAdapter);
                    }
                    searchStocksAdapter.updateSearchString(searchString);
                    presenterContract.fetchStocks(searchString);
                } else {
                    searchStockResultLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        searchText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (searchSmallcase()) return true;
                return true;
            }
            return false;
        });
    }

    @Override
    public boolean searchSmallcase() {
        if (searchText.getText().toString().trim().isEmpty()) return true;

        searchStockResultLayout.setVisibility(View.GONE);
        sid = null;
        // Hide no smallcases view if visible
        if (noSmallcasesContainer.getVisibility() == View.VISIBLE) {
            noSmallcasesContainer.setVisibility(View.GONE);
        }

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("searchQuery", searchText.getText().toString());
        analyticsContract.sendEvent(eventProperties, "Searched smallcase", Analytics.MIXPANEL, Analytics.CLEVERTAP);

        AppUtils.getInstance().hideKeyboard(DiscoverSmallcaseActivity.this);
        blur.setVisibility(View.GONE);
        blur.setClickable(false);

        searchStockResultLayout.setVisibility(View.GONE);
        shouldFetchStocks = true;

        //save already loaded smallcases so they don't have to be fetched again.
        smallcasesAdapter.saveSmallcases();
        searchString = searchText.getText().toString().trim();
        fetching = true;
        offSet = 0;
        presenterContract.fetchSmallcases(null);
        return false;
    }

    private void toggleFilterCard(int visibility) {
        if (visibility == View.VISIBLE) {
            animations.expand(filterCard, ACCELERATE_DECELERATE_INTERPOLATOR, 300);
            presenterContract.savePreviousFilterSelections(types, range, selectSmallcase.isChecked());
        } else {
            if (!getTypes().isEmpty() || range != -1 || hideInvestedSmallcases.isChecked() || selectSmallcase.isChecked()) {
                filterText.setTextColor(ContextCompat.getColor(this, R.color.blue_800));
                filterImage.setImageResource(R.drawable.filter_selected);
            } else {
                filterText.setTextColor(ContextCompat.getColor(this, R.color.secondary_text));
                filterImage.setImageResource(R.drawable.filter_grey);
            }

            animations.collapse(filterCard, ACCELERATE_DECELERATE_INTERPOLATOR, 300);
            if (presenterContract.hasFilterNotChanged(range)) {
                return;
            }

            Map<String, Object> metaData = new HashMap<>();
            metaData.put("thematic", thematic.isChecked());
            metaData.put("modelBased", modelBased.isChecked());
            metaData.put("sectorTracker", sectorTracker.isChecked());
            metaData.put("minimumInvestment", getMaxInvestedAmount());
            metaData.put("hideInvestedSmallcases", hideInvestedSmallcases.isChecked());
            analyticsContract.sendEvent(metaData, "Filtered smallcase", Analytics.MIXPANEL, Analytics.INTERCOM, Analytics.CLEVERTAP);

            fetching = true;
            if (null != smallcasesAdapter.getSmallcases()) {
                smallcasesAdapter.getSmallcases().clear();
                smallcasesAdapter.notifyDataSetChanged();
            }

            offSet = 0;
            presenterContract.fetchSmallcases(null);
        }
    }

    private final Runnable animateShow = new Runnable() {
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void run() {
            int cx = searchContainer.getMeasuredWidth() - marginLeft;
            int cy = searchContainer.getMeasuredHeight() / 2;

            // get the final radius for the clipping circle
            int finalRadius = Math.max(searchContainer.getWidth(), searchContainer.getHeight());

            // create the animator for this view (the start radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(searchContainer, cx, cy, 0, finalRadius);

            anim.setInterpolator(new AccelerateDecelerateInterpolator());
            // make the view visible and start the animation
            searchContainer.setVisibility(View.VISIBLE);
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    showSearch();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            anim.setDuration(300);
            anim.start();
        }
    };

    private final Runnable animateHide = new Runnable() {
        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void run() {
            int cx = searchContainer.getMeasuredWidth() - marginLeft;
            int cy = searchContainer.getMeasuredHeight() / 2;

            // get the final radius for the clipping circle
            int finalRadius = Math.max(searchContainer.getWidth(), searchContainer.getHeight());

            // create the animator for this view (the start radius is zero)
            Animator anim =
                    ViewAnimationUtils.createCircularReveal(searchContainer, cx, cy, finalRadius, 0);

            anim.setInterpolator(new AccelerateDecelerateInterpolator());
            // make the view visible and start the animation
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    AppUtils.getInstance().hideKeyboard(DiscoverSmallcaseActivity.this);
                    closeSearch();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            anim.setDuration(300);
            anim.start();
        }
    };

    @Override
    public void onItemClicked(String sid, String ticker) {
        this.sid = sid;

        searchStockResultLayout.setVisibility(View.GONE);
        // Hide no smallcases view if visible
        if (noSmallcasesContainer.getVisibility() == View.VISIBLE) {
            noSmallcasesContainer.setVisibility(View.GONE);
        }

        searchString = null;
        shouldFetchStocks = false;
        AppUtils.getInstance().hideKeyboard(DiscoverSmallcaseActivity.this);
        blur.setVisibility(View.GONE);
        blur.setClickable(false);
        searchText.setText(ticker);

        //save already loaded smallcases so they don't have to be fetched again.
        smallcasesAdapter.saveSmallcases();
        fetching = true;

        offSet = 0;
        presenterContract.fetchSmallcases(sid);
    }
}
