package com.smallcase.android.smallcase;

import java.util.HashMap;

/**
 * Created by shashankm on 18/04/17.
 */

public interface LoginRequest {
    void getJwtToken(HashMap<String, String> body);
}
