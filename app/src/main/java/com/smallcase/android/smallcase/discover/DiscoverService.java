package com.smallcase.android.smallcase.discover;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Collection;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.Smallcase;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 24/01/17.
 */

class DiscoverService implements DiscoverContract.Service {
    private static final String TAG = "DiscoverService";

    private SmallcaseRepository smallcaseRepository;
    private SharedPrefService sharedPrefService;
    private DiscoverContract.Presenter discoverPresenterContract;
    private CompositeSubscription compositeSubscription;
    private Gson gson;
    private boolean fetchedFeaturedSmallcase = true;
    private boolean fetchedPopularSmallcase = true;
    private boolean fetchedCollection = true;
    private boolean fetchedNews = true;

    DiscoverService(SmallcaseRepository smallcaseRepository, SharedPrefService sharedPrefService, DiscoverContract
            .Presenter discoverPresenterContract) {
        this.smallcaseRepository = smallcaseRepository;
        this.sharedPrefService = sharedPrefService;
        this.discoverPresenterContract = discoverPresenterContract;
        gson = new Gson();
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getFeaturedSmallcases(final String auth) {
        compositeSubscription.add(smallcaseRepository.getFeaturedSmallcases(auth, sharedPrefService.getCsrfToken())
                .subscribe(smallcases -> {
                    fetchedFeaturedSmallcase = true;
                    cacheFeaturedSmallcases(smallcases);
                    discoverPresenterContract.onFeaturedSmallcasesReceived(smallcases);
                }, throwable -> {
                    SentryAnalytics.getInstance().captureEvent(auth, throwable, TAG + "; getFeaturedSmallcases()");
                    fetchedFeaturedSmallcase = false;
                    discoverPresenterContract.onFailedFetchingFeaturedSmallcases();
                }));
    }

    @Nullable
    @Override
    public String getAuthToken() {
        return sharedPrefService.getAuthorizationToken();
    }

    @NonNull
    @Override
    public Set<String> getInvestedScids() {
        return sharedPrefService.getInvestedScidsWithIScids();
    }

    @Override
    public void getCollections(@NonNull final String auth) {
        compositeSubscription.add(smallcaseRepository.getCollections(auth, sharedPrefService.getCsrfToken())
                .subscribe(collectionList -> {
                    fetchedCollection = true;
                    cacheCollections(collectionList);
                    discoverPresenterContract.onCollectionsReceived(collectionList);
                }, throwable -> {
                    SentryAnalytics.getInstance().captureEvent(auth, throwable, TAG + "; getCollections()");
                    fetchedCollection = false;
                    discoverPresenterContract.onFailedFetchingCollection();
                }));
    }

    @Override
    public void getRecentNews(@NonNull final String auth, final int count, final int offSet) {
        compositeSubscription.add(smallcaseRepository.getSmallcaseNews(getAuthToken(), null, offSet, count,
                sharedPrefService.getCsrfToken(), null)
                .subscribe(newses -> {
                    fetchedNews = true;
                    cacheRecentNews(newses);
                    discoverPresenterContract.onRecentNewsFetched(newses);
                }, throwable -> {
                    SentryAnalytics.getInstance().captureEvent(auth, throwable, TAG + "; getRecentNews()");
                    fetchedNews = false;
                    discoverPresenterContract.onFailedFetchingNews();
                }));
    }

    @Override
    public void getPopularSmallcases(@NonNull final String auth, final int count, final int offSet) {
        compositeSubscription.add(smallcaseRepository.getSmallcases(auth, null, null, "popularity", 1, offSet, count,
                null, null, null, sharedPrefService.getCsrfToken(), null, null)
                .subscribe(smallcases -> {
                    fetchedPopularSmallcase = true;
                    cachePopularSmallcases(smallcases);
                    discoverPresenterContract.onPopularSmallcasesFetched(smallcases);
                }, throwable -> {
                    SentryAnalytics.getInstance().captureEvent(auth, throwable, TAG + "; getPopularSmallcases()");
                    Log.d(TAG, "call: " + throwable.toString());
                    fetchedPopularSmallcase = false;
                    discoverPresenterContract.onFailedFetchingPopularSmallcases();
                }));
    }

    @Override
    public List<Smallcase> getCachedFeaturedSmallcases() {
        List<Smallcase> smallcaseList = new ArrayList<>();
        for (String smallcase : sharedPrefService.getCachedFeaturedSmallcases()) {
            smallcaseList.add(gson.fromJson(smallcase, Smallcase.class));
        }
        return smallcaseList;
    }

    @Override
    public List<Collection> getCachedCollections() {
        List<Collection> collectionList = new ArrayList<>();
        for (String smallcase : sharedPrefService.getCachedCollection()) {
            collectionList.add(gson.fromJson(smallcase, Collection.class));
        }
        return collectionList;
    }

    @Override
    public List<News> getCachedRecentNews() {
        List<News> newsList = new ArrayList<>();
        for (String smallcase : sharedPrefService.getCachedRecentNews()) {
            newsList.add(gson.fromJson(smallcase, News.class));
        }
        return newsList;
    }

    @Override
    public List<Smallcase> getCachedRecentSmallcases() {
        List<Smallcase> smallcaseList = new ArrayList<>();
        for (String smallcase : sharedPrefService.getCachedRecentSmallcases()) {
            smallcaseList.add(gson.fromJson(smallcase, Smallcase.class));
        }
        return smallcaseList;
    }

    @Override
    public List<Smallcase> getCachedPopularSmallcases() {
        List<Smallcase> popularSmallcases = new ArrayList<>();
        for (String smallcase : sharedPrefService.getCachedPopularSmallcases()) {
            popularSmallcases.add(gson.fromJson(smallcase, Smallcase.class));
        }
        return popularSmallcases;
    }

    @Override
    public void getSmallcases(@Nullable List<String> types, @Nullable List<String> scids, @Nullable String sortBy, int sortOrder, int offSet, int
            count, @Nullable Integer minMinInvestAmount, @Nullable Integer maxMinInvestAmount, @Nullable String searchString, @Nullable String sid) {
        compositeSubscription.add(smallcaseRepository.getSmallcases(getAuthToken(), types, scids, sortBy, sortOrder, offSet, count, minMinInvestAmount,
                maxMinInvestAmount, searchString, sharedPrefService.getCsrfToken(), sid, null)
                .subscribe(smallcases -> {
                    cacheSmallcases(smallcases);
                    discoverPresenterContract.onRecentSmallcasesReceived(smallcases);
                }, throwable -> {
                    SentryAnalytics.getInstance().captureEvent(getAuthToken(), throwable, TAG + "; getSmallcases()");
                    discoverPresenterContract.onFailedFetchingRecentSmallcases();
                }));
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }

    @Override
    public boolean fetchedFeaturedSmallcases() {
        return fetchedFeaturedSmallcase;
    }

    @Override
    public boolean fetchedPopularSmallcases() {
        return fetchedPopularSmallcase;
    }

    @Override
    public boolean fetchedCollection() {
        return fetchedCollection;
    }

    @Override
    public boolean fetchedNews() {
        return fetchedNews;
    }

    private void cacheRecentNews(List<News> newses) {
        HashSet<String> newsSet = new HashSet<>();
        for (News news : newses) {
            newsSet.add(gson.toJson(news));
        }
        sharedPrefService.cacheRecentNews(newsSet);
    }

    private void cacheCollections(List<Collection> collections) {
        HashSet<String> collectionSet = new HashSet<>();
        for (Collection collection : collections) {
            collectionSet.add(gson.toJson(collection));
        }
        sharedPrefService.cacheCollection(collectionSet);
    }

    private void cacheFeaturedSmallcases(List<Smallcase> smallcases) {
        HashSet<String> smallcaseSet = new HashSet<>();
        for (Smallcase smallcase : smallcases) {
            smallcaseSet.add(gson.toJson(smallcase));
        }

        sharedPrefService.cacheFeaturedSmallcases(smallcaseSet);
    }

    private void cacheSmallcases(List<Smallcase> smallcases) {
        HashSet<String> smallcaseSet = new HashSet<>();
        for (Smallcase smallcase : smallcases) {
            smallcaseSet.add(gson.toJson(smallcase));
        }
        sharedPrefService.cacheRecentSmallcases(smallcaseSet);
    }

    private void cachePopularSmallcases(List<Smallcase> smallcases) {
        HashSet<String> smallcaseSet = new HashSet<>();
        for (Smallcase smallcase : smallcases) {
            smallcaseSet.add(gson.toJson(smallcase));
        }
        sharedPrefService.cachePopularSmallcases(smallcaseSet);
    }

}
