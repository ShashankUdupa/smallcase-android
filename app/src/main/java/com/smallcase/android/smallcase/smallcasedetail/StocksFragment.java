package com.smallcase.android.smallcase.smallcasedetail;

import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.smallcase.android.R;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PSegment;
import com.smallcase.android.view.model.PStockWeight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by shashankm on 03/03/17.
 */

public class StocksFragment extends Fragment {
  private static final String TAG = "StocksFragment";

  @BindView(R.id.stocks_list)
  RecyclerView stocksList;
  @BindView(R.id.last_rebalanced)
  GraphikText lastRebalanced;
  @BindView(R.id.login_blur)
  View loginBlur;
  @BindView(R.id.login_layout)
  View loginLayout;

  private final int[] stocksColorPallet = {R.color.first, R.color.second, R.color.third, R.color.fourth, R.color.fifth,
      R.color.sixth, R.color.seventh, R.color.eighth, R.color.ninth, R.color.tenth, R.color.eleventh, R.color.twelfth};

  private Unbinder unbinder;
  private LinearLayoutManager layoutManager;
  private StocksAdapter stocksAdapter;

  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.stocks, container, false);
    unbinder = ButterKnife.bind(this, view);

    layoutManager = new LinearLayoutManager(getActivity());
    stocksList.setLayoutManager(layoutManager);
    stocksList.setHasFixedSize(true);
    stocksList.setNestedScrollingEnabled(false);

    return view;
  }

  @Override
  public void onDestroyView() {
    if (null != unbinder) {
      unbinder.unbind();
    }
    super.onDestroyView();
  }

  public void showStocksBlur() {
    if (!isAdded()) return;

    loginBlur.setVisibility(View.VISIBLE);
    loginLayout.setVisibility(View.VISIBLE);
  }

  public void hideStocksBlurIfVisible() {
    if (!isAdded()) return;

    if (loginBlur.getVisibility() == View.VISIBLE) {
      loginBlur.setVisibility(View.GONE);
      loginLayout.setVisibility(View.GONE);
    }
  }

  @OnClick(R.id.login_or_sign_up)
  public void onLoginOrSignUpClicked() {
    if (getContext() == null) return;

    ((SmallcaseDetailActivity) getContext()).sendUserToLoginOrSignUp();
  }

  @OnClick(R.id.customize_smallcase)
  public void onCustomizeSmallcaseClicked() {
    if (getContext() == null || stocksAdapter == null) return;

    ((SmallcaseDetailActivity) getContext()).onCustomizeSmallcaseClicked(stocksAdapter.getStocksAndSegmentsList());
  }

  public void onDataReceived(String lastReBalanced, HashMap<String, List<PStockWeight>> constituentsMap) {
    if (!isAdded()) return;

    this.lastRebalanced.setText(lastReBalanced);

    List<Object> stocksAndSegmentsList = new ArrayList<>();
    int i = 0;
    for (Map.Entry<String, List<PStockWeight>> entry : constituentsMap.entrySet()) {
      PSegment segment = new PSegment();
      segment.setName(entry.getKey());
      segment.setColorResId(stocksColorPallet[i++]);
      double weight = 0;
      int startIndexForLabel = stocksAndSegmentsList.size();
      for (PStockWeight PStockWeight : entry.getValue()) {
        weight += PStockWeight.getWeightage();
        stocksAndSegmentsList.add(PStockWeight);
      }
      segment.setWeightage(String.format(Locale.getDefault(), "%.2f", weight > 100 ? 100 : weight));
      stocksAndSegmentsList.add(startIndexForLabel, segment);
      i = i > 11 ? (i % 11) : i;
    }
    stocksAdapter = new StocksAdapter(stocksAndSegmentsList);
    stocksList.setAdapter(stocksAdapter);
  }

  void showTutorial() {
    if (!isAdded()) return;

    int visiblePosition = layoutManager.findFirstVisibleItemPosition();
    if (visiblePosition == -1) return; // Views not laid out
    View view = layoutManager.getChildAt(visiblePosition + 1);
    final int[] location = new int[2];
    view.getLocationOnScreen(location);
    Rect bounds = new Rect(100, location[1], 200, location[1] + view.getHeight());

    TapTargetView.showFor(getActivity(),
        TapTarget.forBounds(bounds, "Stock information & ratios", "Tap on a stock name to see more information about the stock")
            .textTypeface(AppUtils.getInstance().getFont(getContext(), AppConstants.FontType.REGULAR))
            .descriptionTextColor(R.color.white)
            .descriptionTextSize(14)
            .cancelable(true)
            .outerCircleColor(R.color.blue_800)
            .outerCircleAlpha(0.9f)
            .transparentTarget(true)
            .targetRadius(30));
  }

  public void hideLastReBalanceCard() {
    if (!isAdded()) return;

    lastRebalanced.setVisibility(View.GONE);
  }

  class StocksAdapter extends RecyclerView.Adapter<StocksAdapter.StockViewHolder> {
    private List<Object> stocksAndSegmentsList;

    StocksAdapter(List<Object> stocksAndSegmentsList) {
      this.stocksAndSegmentsList = new ArrayList<>(stocksAndSegmentsList);
    }

    @NonNull
    @Override
    public StocksAdapter.StockViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.smallcase_stock_layout, parent, false);

      return new StockViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StocksAdapter.StockViewHolder holder, int position) {
      if (stocksAndSegmentsList.get(position) instanceof PSegment) {
        PSegment segment = (PSegment) stocksAndSegmentsList.get(position);

        holder.segmentName.applyCustomFont(getActivity(), 3);
        holder.stockContainer.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.grey_100));
        holder.segmentColor.setVisibility(View.VISIBLE);
        GradientDrawable drawable = (GradientDrawable) holder.segmentColor.getBackground();
        drawable.setColor(ContextCompat.getColor(getContext(), segment.getColorResId()));
        holder.weightage.setTextColor(ContextCompat.getColor(getContext(), R.color.secondary_text));

        holder.segmentName.setText(segment.getName());
        holder.weightage.setText(segment.getWeightage() + "%");
        return;
      }

      if (stocksAndSegmentsList.get(position) instanceof PStockWeight) {
        final PStockWeight PStockWeight = (PStockWeight) stocksAndSegmentsList.get(position);

        holder.segmentName.applyCustomFont(getContext(), 0);
        holder.stockContainer.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
        holder.weightage.setTextColor(ContextCompat.getColor(getActivity(), R.color.primary_text));

        holder.segmentColor.setVisibility(View.INVISIBLE);
        holder.weightage.setText(String.format(Locale.getDefault(), "%.1f", PStockWeight.getWeightage()) + "%");
        holder.segmentName.setText(PStockWeight.getStockName());

        holder.segmentName.setOnClickListener(v -> ((SmallcaseDetailActivity) getActivity()).showStockInfo(PStockWeight.getSid(), PStockWeight.getStockName()));
      }
    }

    @Override
    public int getItemCount() {
      return stocksAndSegmentsList.size();
    }

    List<Object> getStocksAndSegmentsList() {
      return stocksAndSegmentsList;
    }

    class StockViewHolder extends RecyclerView.ViewHolder {
      @BindView(R.id.segment_color)
      View segmentColor;
      @BindView(R.id.segment_name)
      GraphikText segmentName;
      @BindView(R.id.weightage)
      GraphikText weightage;
      @BindView(R.id.stock_container)
      View stockContainer;

      StockViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
      }
    }
  }
}
