package com.smallcase.android.smallcase.discover;

import android.support.annotation.NonNull;

import com.smallcase.android.data.model.Collection;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.Smallcase;
import com.smallcase.android.view.model.PCollection;
import com.smallcase.android.view.model.PFeaturedSmallcase;
import com.smallcase.android.view.model.PSmallcase;
import com.smallcase.android.view.model.PSmallcaseWithNews;

import java.util.List;
import java.util.Set;

/**
 * Created by shashankm on 24/01/17.
 */

interface DiscoverContract {

    interface View {
        void onFeaturedSmallcaseReceived(List<PFeaturedSmallcase> featuredSmallcases);

        void showSnackBar(int resId);

        void onCollectionsReceived(List<PCollection> pCollections);

        void onRecentNewsReceived(List<PSmallcaseWithNews> smallcaseWithNewses);

        void showTryAgain();

        void showNoNetwork();

        void onPopularSmallcasesReceived(List<PSmallcase> pSmallcases);

        void hideRecentNews();

        void onRecentSmallcasesReceived(List<PSmallcase> pSmallcases);
    }

    interface Presenter {
        void getDiscoverPageItems();

        void onFailedFetchingFeaturedSmallcases();

        void onRecentSmallcasesReceived(List<Smallcase> smallcases);

        void onFeaturedSmallcasesReceived(List<Smallcase> smallcases);

        void onFailedFetchingCollection();

        void onCollectionsReceived(List<Collection> collections);

        void onRecentNewsFetched(List<News> newses);

        void onFailedFetchingNews();

        void onFailedFetchingRecentSmallcases();

        void onPopularSmallcasesFetched(List<Smallcase> smallcases);

        void onFailedFetchingPopularSmallcases();

        void destroySubscriptions();
    }

    interface Service {
        void getFeaturedSmallcases(String auth);

        String getAuthToken();

        Set<String> getInvestedScids();

        void getCollections(@NonNull String auth);

        void getRecentNews(@NonNull String auth, int count, int offSet);

        void getPopularSmallcases(@NonNull String auth, int count, int offSet);

        List<Smallcase> getCachedFeaturedSmallcases();

        List<Collection> getCachedCollections();

        List<News> getCachedRecentNews();

        List<Smallcase> getCachedRecentSmallcases();

        List<Smallcase> getCachedPopularSmallcases();

        boolean fetchedFeaturedSmallcases();

        boolean fetchedPopularSmallcases();

        boolean fetchedCollection();

        boolean fetchedNews();

        void getSmallcases(List<String> types, List<String> scids, String sortBy, int sortOrder, int offSet, int count, Integer
                minInvestmentAmount, Integer maxInvestmentAmount, String searchString, String sid);

        void destroySubscriptions();
    }
}
