package com.smallcase.android.smallcase.discover;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.create.CreateSmallcaseActivity;
import com.smallcase.android.news.AllNewsActivity;
import com.smallcase.android.user.ContainerActivity;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.adapter.CompactSmallcasesAdapter;
import com.smallcase.android.view.android.AndroidApp;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PCollection;
import com.smallcase.android.view.model.PFeaturedSmallcase;
import com.smallcase.android.view.model.PSmallcase;
import com.smallcase.android.view.model.PSmallcaseWithNews;
import com.squareup.leakcanary.RefWatcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by shashankm on 24/01/17.
 */

public class DiscoverFragment extends Fragment implements DiscoverContract.View, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = DiscoverFragment.class.getSimpleName();

    @BindView(R.id.featured_smallcase_list)
    RecyclerView featuredSmallcaseList;
    @BindView(R.id.popular_smallcases_list)
    RecyclerView popularSmallcaseList;
    @BindView(R.id.collections_list)
    RecyclerView collectionsList;
    @BindView(R.id.recent_news_list)
    RecyclerView recentNewsList;
    @BindView(R.id.discover_container)
    View discoveryContainer;
    @BindView(R.id.swipe_refresh_discover)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.empty_state_container)
    View noInvestments;
    @BindView(R.id.no_investment_layout)
    View noInvestmentLayout;
    @BindView(R.id.text_empty_state)
    GraphikText errorText;
    @BindView(R.id.action)
    View action;
    @BindView(R.id.action_text)
    GraphikText actionText;
    @BindView(R.id.image_collection)
    ImageView collectionsImage;
    @BindView(R.id.text_collection_name)
    GraphikText collectionName;
    @BindView(R.id.text_collection_description)
    GraphikText collectionDescription;
    @BindView(R.id.collection_name_loading)
    View collectionNameLoading;
    @BindView(R.id.description_loading)
    View descriptionLoading;
    @BindView(R.id.description_two_loading)
    View descriptionTwoLoading;
    @BindView(R.id.smallcase_number_loading)
    View smallcaseNumberLoading;
    @BindView(R.id.image_arrow)
    ImageView arrow;
    @BindView(R.id.recent_news_container)
    View recentNewsContainer;
    @BindView(R.id.recent_smallcases_list)
    RecyclerView recentSmallcaseList;
    @BindView(R.id.see_collection)
    GraphikText seeCollection;
    @BindView(R.id.create_smallcase_image)
    ImageView createSmallcaseImage;

    private Unbinder unbinder;
    private DiscoverContract.Presenter discoverPresenterContract;
    private FeaturedSmallcaseAdapter featuredSmallcaseAdapter;
    private CompactSmallcasesAdapter popularSmallcaseAdapter, recentSmallcasesAdapter;
    private CollectionsAdapter collectionsAdapter;
    private SmallcaseNewsAdapter smallcaseNewsAdapter;
    private Animations animations;
    private PCollection featuredCollection;
    private AnalyticsContract analyticsContract;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        discoverPresenterContract = new DiscoverPresenter(context, this, new NetworkHelper(context));
        analyticsContract = new AnalyticsManager(context.getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.discover_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);

        setLayoutManager(recentSmallcaseList, featuredSmallcaseList, popularSmallcaseList, collectionsList, recentNewsList);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.blue_800);

        featuredSmallcaseAdapter = new FeaturedSmallcaseAdapter(getActivity(), analyticsContract);
        featuredSmallcaseList.setAdapter(featuredSmallcaseAdapter);

        recentSmallcasesAdapter = new CompactSmallcasesAdapter(getActivity(), AppConstants.RECENT_SMALLCASES, analyticsContract);
        recentSmallcaseList.setAdapter(recentSmallcasesAdapter);

        popularSmallcaseAdapter = new CompactSmallcasesAdapter(getActivity(), AppConstants.POPULAR_SMALLCASES, analyticsContract);
        popularSmallcaseList.setAdapter(popularSmallcaseAdapter);

        collectionsAdapter = new CollectionsAdapter(getActivity(), analyticsContract);
        collectionsList.setAdapter(collectionsAdapter);

        smallcaseNewsAdapter = new SmallcaseNewsAdapter(getActivity(), analyticsContract, "Discover");
        recentNewsList.setAdapter(smallcaseNewsAdapter);

        animations = new Animations();
        animations.loadingContent(collectionNameLoading, descriptionLoading, descriptionTwoLoading, smallcaseNumberLoading);

        Glide.with(getActivity())
                .load(R.drawable.smallcase_create)
                .into(createSmallcaseImage);
        discoverPresenterContract.getDiscoverPageItems();
        return view;
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        RefWatcher refWatcher = AndroidApp.getRefWatcher(getActivity());
        refWatcher.watch(this);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        discoverPresenterContract.destroySubscriptions();
        super.onDestroy();
    }

    @OnClick(R.id.see_all_news)
    public void seeAllNews() {
        if (!isAdded() || null == smallcaseNewsAdapter.getNews() || smallcaseNewsAdapter.getNews().isEmpty()) {
            return;
        }

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("accessedFrom", "Discover See All");
        analyticsContract.sendEvent(eventProperties, "Viewed News Feed", Analytics.MIXPANEL);

        Intent intent = new Intent(getActivity(), AllNewsActivity.class);
        intent.putExtra(AllNewsActivity.SCREEN_TITLE, "News for smallcases");
        intent.putParcelableArrayListExtra(AllNewsActivity.DATA_LIST, (ArrayList<? extends Parcelable>) smallcaseNewsAdapter.getNews());
        intent.putExtra(AllNewsActivity.TYPE, AppConstants.NEWS_TYPE_UN_INVESTED);
        startActivity(intent);
    }

    @OnClick(R.id.see_all_smallcase_card)
    public void seeAllSmallcases() {
        if (!isAdded()) return;

        startActivity(new Intent(getActivity(), DiscoverSmallcaseActivity.class));
    }

    @OnClick(R.id.thematic_layout)
    public void seeThematicSmallcases() {
        if (!isAdded()) return;

        Intent intent = new Intent(getActivity(), DiscoverSmallcaseActivity.class);
        intent.putExtra(DiscoverSmallcaseActivity.THEMATIC, true);
        startActivity(intent);
    }

    @OnClick(R.id.sector_layout)
    public void seeSectorSmallcases() {
        if (!isAdded()) return;

        Intent intent = new Intent(getActivity(), DiscoverSmallcaseActivity.class);
        intent.putExtra(DiscoverSmallcaseActivity.SECTOR_TRACKERS, true);
        startActivity(intent);
    }

    @OnClick(R.id.model_layout)
    public void seeModelSmallcases() {
        if (!isAdded()) return;

        Intent intent = new Intent(getActivity(), DiscoverSmallcaseActivity.class);
        intent.putExtra(DiscoverSmallcaseActivity.MODEL_BASED, true);
        startActivity(intent);
    }

    @OnClick(R.id.create_your_smallcase)
    public void createYourSmallcase() {
        if (!isAdded()) return;

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("accessedFrom", "Discover");
        analyticsContract.sendEvent(eventProperties, "Viewed Create", Analytics.MIXPANEL, Analytics.CLEVERTAP);
        startActivity(new Intent(getActivity(), CreateSmallcaseActivity.class));
    }

    @Override
    public void onFeaturedSmallcaseReceived(List<PFeaturedSmallcase> featuredSmallcases) {
        if (!isAdded()) return;

        hideTryAgain();
        featuredSmallcaseAdapter.onFeaturedSmallcasesReceived(featuredSmallcases);
    }

    @Override
    public void onRecentSmallcasesReceived(List<PSmallcase> pSmallcases) {
        if (!isAdded()) return;

        stopRefreshing();
        hideTryAgain();
        recentSmallcasesAdapter.onSmallcaseReceived(pSmallcases);
    }

    @Override
    public void showSnackBar(int resId) {
        AppUtils.getInstance().showSnackBar(discoveryContainer, Snackbar.LENGTH_LONG, resId);
    }

    @OnClick(R.id.featured_collection_card)
    public void featuredCollectionCard() {
        if (null == featuredCollection) {
            return;
        }

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("collectionName", featuredCollection.getCollectionName());
        analyticsContract.sendEvent(eventProperties, "Viewed Collection", Analytics.MIXPANEL, Analytics.CLEVERTAP);

        Intent intent = new Intent(getActivity(), DiscoverSmallcaseActivity.class);
        intent.putExtra(DiscoverSmallcaseActivity.COLLECTION, featuredCollection);
        intent.putStringArrayListExtra(DiscoverSmallcaseActivity.SCIDS, (ArrayList<String>) featuredCollection.getScids());
        startActivity(intent);
    }

    @Override
    public void onCollectionsReceived(List<PCollection> pCollections) {
        if (!isAdded() || pCollections.isEmpty()) return;

        stopRefreshing();

        animations.stopAnimation();
        hideViews(collectionNameLoading, descriptionLoading, descriptionTwoLoading, smallcaseNumberLoading);
        collectionsImage.setBackgroundColor(0);

        featuredCollection = pCollections.get(0);
        collectionName.setText(featuredCollection.getCollectionName());
        collectionDescription.setText(featuredCollection.getCollectionDescription());
        seeCollection.setText("See " + featuredCollection.getSmallcasesNumber());

        Glide.with(this)
                .load(featuredCollection.getCollectionImage())
                .asBitmap()
                .placeholder(ContextCompat.getColor(getContext(), R.color.grey_300))
                .into(collectionsImage);
        Glide.with(this)
                .load(R.drawable.arrow_right)
                .asBitmap()
                .into(arrow);
        collectionsAdapter.onCollectionReceived(pCollections.subList(1, pCollections.size()));
    }

    @Override
    public void onRecentNewsReceived(List<PSmallcaseWithNews> smallcaseWithNewses) {
        if (!isAdded()) return;

        if (recentNewsContainer.getVisibility() == View.GONE || recentNewsList.getVisibility() == View.GONE) {
            recentNewsContainer.setVisibility(View.VISIBLE);
            recentNewsList.setVisibility(View.VISIBLE);
        }
        stopRefreshing();
        smallcaseNewsAdapter.onNewsReceived(smallcaseWithNewses);
    }

    @Override
    public void showTryAgain() {
        if (!isAdded()) return;

        swipeRefreshLayout.setVisibility(View.GONE);
        noInvestments.setVisibility(View.VISIBLE);
        noInvestmentLayout.setVisibility(View.VISIBLE);
        action.setVisibility(View.VISIBLE);
        actionText.setText(getString(R.string.try_again));
        errorText.setText(getString(R.string.something_wrong));
    }

    @Override
    public void showNoNetwork() {
        if (!isAdded()) return;

        ((ContainerActivity) getActivity()).showNoInternet();
    }

    @Override
    public void onPopularSmallcasesReceived(List<PSmallcase> pSmallcases) {
        if (!isAdded()) return;

        hideTryAgain();
        stopRefreshing();
        popularSmallcaseAdapter.onSmallcaseReceived(pSmallcases);
    }

    @Override
    public void hideRecentNews() {
        recentNewsContainer.setVisibility(View.GONE);
        recentNewsList.setVisibility(View.GONE);
    }

    @OnClick(R.id.action)
    public void tryAgainClicked() {
        hideTryAgain();
        discoverPresenterContract.getDiscoverPageItems();
    }

    private void hideTryAgain() {
        if (noInvestmentLayout.getVisibility() == View.GONE) return;

        noInvestments.setVisibility(View.GONE);
        noInvestmentLayout.setVisibility(View.GONE);
        action.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        discoverPresenterContract.getDiscoverPageItems();
    }

    private void stopRefreshing() {
        if (null != swipeRefreshLayout && swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void setLayoutManager(RecyclerView... recyclerViews) {
        for (RecyclerView recyclerView : recyclerViews) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setHasFixedSize(true);
        }
    }

    private void hideViews(View... views) {
        for (View view : views) view.setVisibility(View.GONE);
    }
}
