package com.smallcase.android.smallcase.smallcasedetail;

import android.support.annotation.Nullable;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.ExitedSmallcase;
import com.smallcase.android.data.model.InvestedSmallcases;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import io.sentry.event.Breadcrumb;
import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 28/02/17.
 */

class SmallcaseDetailService implements SmallcaseDetailContract.Service {
    private static final String TAG = "SmallcaseDetailService";

    private SmallcaseRepository smallcaseRepository;
    private SharedPrefService sharedPrefService;
    private SmallcaseDetailContract.Presenter presenterContract;
    private UserRepository userRepository;
    private UserSmallcaseRepository userSmallcaseRepository;
    private MarketRepository marketRepository;
    private CompositeSubscription compositeSubscription;

    SmallcaseDetailService(SmallcaseRepository smallcaseRepository, SharedPrefService sharedPrefService, SmallcaseDetailContract
            .Presenter presenterContract, UserRepository userRepository, MarketRepository marketRepository, UserSmallcaseRepository userSmallcaseRepository) {
        this.smallcaseRepository = smallcaseRepository;
        this.sharedPrefService = sharedPrefService;
        this.presenterContract = presenterContract;
        this.userRepository = userRepository;
        this.marketRepository = marketRepository;
        this.userSmallcaseRepository = userSmallcaseRepository;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getSmallcaseDetail(final String scid) {
        compositeSubscription.add(smallcaseRepository.getSmallcaseDetails(sharedPrefService.getAuthorizationToken(), scid, sharedPrefService.getCsrfToken())
                .subscribe(unInvestedSmallcase -> presenterContract.onSmallcaseDetailsReceived(unInvestedSmallcase), throwable -> {
                    presenterContract.onFailedFetchingSmallcaseDetails();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    Map<String, String> data = new HashMap<>();
                    data.put("scid", scid);
                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "State", data);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getSmallcaseDetail()");
                }));
    }

    @Override
    public void getInvestedSmallcaseDetail(final String iScid) {
        compositeSubscription.add(userSmallcaseRepository.getInvestedSmallcaseDetails(sharedPrefService.getAuthorizationToken(), iScid, sharedPrefService.getCsrfToken())
                .subscribe(smallcaseDetail -> presenterContract.onSmallcaseInvestmentsReceived(smallcaseDetail), throwable -> {
                    presenterContract.onFailedFetchingSmallcaseInvestments();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    Map<String, String> data = new HashMap<>();
                    data.put("iScid", iScid);
                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "State", data);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getInvestedSmallcaseDetail()");
                }));
    }

    @Override
    public void getSmallcaseNews(final String scid) {
        List<String> scids = new ArrayList<>();
        scids.add(scid);
        compositeSubscription.add(smallcaseRepository.getSmallcaseNews(sharedPrefService.getAuthorizationToken(), scids,
                0, 10, sharedPrefService.getCsrfToken(), null)
                .subscribe(newses -> presenterContract.onSmallcaseNewsReceived(newses), throwable -> {
                    presenterContract.onFailedFetchingSmallcaseNews();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    Map<String, String> data = new HashMap<>();
                    data.put("scid", scid);
                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "State", data);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getSmallcaseNews()");
                }));
    }

    @Override
    public void getPendingActions() {
        compositeSubscription.add(userRepository.getPendingActions(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken())
                .subscribe(actions -> presenterContract.onReBalanceUpdatesFetched(actions.getReBalanceUpdates()), throwable -> {
                    presenterContract.onFailedFetchingReBalanceUpdates();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getBuyReminders()");
                }));
    }

    @Override
    public void getUserData() {
        compositeSubscription.add(userRepository.getUserInfo(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken())
                .subscribe(user -> {
                    String[] splitName = user.getBroker().getUserName() == null? new String[0] : user.getBroker().getUserName().trim().split(" ");
                    StringBuilder name = new StringBuilder();
                    for (String split : splitName) {
                        if (split ==null || split.trim().equals("")) continue;
                        name.append(split.substring(0, 1).toUpperCase(Locale.getDefault())).append(split.substring(1)
                                .toLowerCase(Locale.getDefault())).append(" ");
                    }

                    Set<String> exitedIScids = new HashSet<>();
                    for (ExitedSmallcase exitedSmallcase : user.getExitedSmallcases()) {
                        exitedIScids.add(exitedSmallcase.get_id());
                    }

                    HashSet<String> scidWithIScidList = new HashSet<>();
                    for (InvestedSmallcases investedSmallcase : user.getInvestedSmallcases()) {
                        scidWithIScidList.add(investedSmallcase.getScid() + "#" + investedSmallcase.get_id());
                    }

                    sharedPrefService.saveInvestedScidWithIscid(scidWithIScidList);
                    sharedPrefService.cacheWatchlist(user.getSmallcaseWatchlist());
                    sharedPrefService.cacheUserInfo(name.toString(), user.getBroker().getUserId(), user.getBroker().getName());
                    sharedPrefService.cacheExitedIScids(exitedIScids);

                    presenterContract.onUserDataCached();
                }, throwable -> {
                    presenterContract.onFailedFetchingUserData();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getUserData()");
                }));
    }

    @Override
    public void setFlag(HashMap<String, Object> body) {
        compositeSubscription.add(userRepository.setFlag(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), body)
                .subscribe(responseBody -> {

                }, throwable -> SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; setFlag()")));
    }

    @Override
    public void getSmallcaseHistorical(String scid, String benchmarkType, String benchmarkId, String duration) {
        final Map<String, String> data = new HashMap<>();
        data.put("scid", scid);
        data.put("benchmarkType", benchmarkType);
        data.put("benchmarkId", benchmarkId);
        data.put("duration", duration);
        compositeSubscription.add(smallcaseRepository.getSmallcaseHistorical(sharedPrefService.getAuthorizationToken(), scid, benchmarkType,
                benchmarkId, duration, sharedPrefService.getCsrfToken())
                .subscribe(historical -> presenterContract.onSmallcaseHistoricalReceived(historical), throwable -> {
                    presenterContract.onFailedFetchingSmallcaseHistorical();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "State", data);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getSmallcaseHistorical()");
                }));
    }

    @Override
    public boolean isSmallcaseWatchlisted(String scid) {
        for (String watchlistScid : sharedPrefService.getCachedWatchlist()) {
            if (scid.equals(watchlistScid)) return true;
        }
        return false;
    }

    @Nullable
    @Override
    public String getIScid(String scid) {
        String iScid = null;
        Set<String> investedSmallcasesSet = sharedPrefService.getInvestedScidsWithIScids();
        for (String string : investedSmallcasesSet) {
            String[] scidWithIscid = string.split("#");
            if (scid.equals(scidWithIscid[0])) {
                iScid = scidWithIscid[1];
                break;
            }
        }

        return iScid;
    }

    @Override
    public String getAuth() {
        return sharedPrefService.getAuthorizationToken();
    }

    @Override
    public void addToWatchlist(final String scid) {
        final HashMap<String, String> body = new HashMap<>();
        body.put("scid", scid);
        compositeSubscription.add(userSmallcaseRepository.addToWatchlist(getAuth(), body, sharedPrefService.getCsrfToken())
                .subscribe(responseBody -> {
                    try {
                        addOrRemoveScidFromCache(responseBody, scid, false);
                    } catch (IOException | JSONException e) {
                        body.put("Type", "Parse error");
                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.DEFAULT, "State", body);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; addToWatchlist()");

                        e.printStackTrace();
                    }
                }, throwable -> {
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "State", body);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; addToWatchlist()");
                }));
    }

    @Override
    public void removeFromWatchlist(final String scid) {
        final HashMap<String, String> body = new HashMap<>();
        body.put("scid", scid);
        compositeSubscription.add(userSmallcaseRepository.removeFromWatchlist(getAuth(), body, sharedPrefService.getCsrfToken())
                .subscribe(responseBody -> {
                    try {
                        addOrRemoveScidFromCache(responseBody, scid, true);
                    } catch (IOException | JSONException e ) {
                        body.put("Type", "Parse error");
                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.DEFAULT, "State", body);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; removeFromWatchlist()");

                        e.printStackTrace();
                    }
                }, throwable -> {
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "State", body);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; removeFromWatchlist()");
                }));
    }

    @Override
    public void getIfMarketIsOpen() {
        compositeSubscription.add(marketRepository.getIfMarketIsOpen(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(responseBody -> presenterContract.onMarketStatusReceived(responseBody), throwable -> {
                    presenterContract.onFailedFetchingMarketStatus(throwable);
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getIfMarketIsOpen()");
                }));
    }

    @Override
    public void getDraftDetails(final String did) {
        compositeSubscription.add(userSmallcaseRepository.getDraft(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), did)
                .subscribe(draftSmallcase -> presenterContract.onDraftFetchedSuccessfully(draftSmallcase), throwable -> {
                    presenterContract.onFailedFetchingDraft();
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable,
                            TAG + "; getStocksInfo(); did - " + did);
                }));
    }

    @Override
    public void getStockHistorical(final List<String> sids, String duration) {
        compositeSubscription.add(marketRepository.getStockHistorical(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), sids, duration)
                .subscribe(responseBody -> {
                    try {
                        presenterContract.onStockHistoricalReceived(responseBody);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; getStocksInfo()");
                    }
                }, throwable -> {
                    presenterContract.onFailedFetchingStockHistorical();

                    HashMap<String, String> data = new HashMap<>();
                    for (String sid : sids) {
                        data.put("stock", sid);
                    }
                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, TAG + "; getStocksInfo()", data);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getStocksInfo()");
                }));
    }

    @Override
    public void addMarketOpenReminder(HashMap<String, String> body) {
        compositeSubscription.add(userSmallcaseRepository.addReminder(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken(), body)
                .subscribe(responseBody -> {

                }, throwable -> {
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; addMarketOpenReminder()");
                }));
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }

    @Override
    public void deleteDraft(HashMap<String, String> body) {
        compositeSubscription.add(userSmallcaseRepository.deleteDraft(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), body)
                .subscribe(responseBody -> presenterContract.onDraftDeleted(), throwable -> {
                    presenterContract.onFailedDeletingDraft();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; deleteDraft()");
                }));
    }

    private void addOrRemoveScidFromCache(ResponseBody responseBody, String scid, boolean shouldRemove)
            throws JSONException, IOException {
        JSONObject response = new JSONObject(responseBody.string());
        if (response.getBoolean("success")) {
            List<String> watchlist = new ArrayList<>(sharedPrefService.getCachedWatchlist());
            if (shouldRemove) {
                watchlist.remove(scid);
            } else {
                watchlist.add(scid);
            }
            sharedPrefService.cacheWatchlist(watchlist);
        }
    }
}
