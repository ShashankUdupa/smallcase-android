package com.smallcase.android.smallcase.discover;

import android.support.annotation.Nullable;
import android.text.SpannableString;

import com.smallcase.android.data.model.SearchResult;
import com.smallcase.android.data.model.Smallcase;
import com.smallcase.android.view.model.PExpandedSmallcase;

import java.util.List;
import java.util.Set;

/**
 * Created by shashankm on 03/02/17.
 */

interface DiscoverSmallcaseContract {
    interface View {
        List<String> getTypes();

        String getSortBy();

        int getSortOrder();

        int getOffSet();

        int getCount();

        Integer getMinInvestedAmount();

        Integer getMaxInvestedAmount();

        String getSearchString();

        boolean shouldHideInvestedSmallcase();

        void showSmallcases(List<PExpandedSmallcase> smallcaseList);

        void noMoreSmallcases();

        void noSmallcases(SpannableString resonString);

        List<PExpandedSmallcase> getSmallcases();

        void showSnackBar(int errId);

        List<String> getScids();

        void smallcasesRemaining();

        void onStocksReceivedFromSearch(List<SearchResult> searchResults);

        void updateOffSet(int size);

        String getTier();
    }

    interface Presenter {
        void fetchSmallcases(@Nullable String sid);

        void onSmallcasesReceived(List<Smallcase> smallcases);

        void onFailedFetchingSmallcases();

        boolean hasFilterNotChanged(int range);

        void savePreviousFilterSelections(List<String> types, int range, boolean selectSmallcase);

        void savePreviousSortSelection();

        boolean hasSortNotChanged();

        void destroySubscriptions();

        void fetchStocks(String searchString);
    }

    interface Service {
        String getAuth();

        void getSmallcases(String auth, @Nullable List<String> types, @Nullable List<String> scids, @Nullable String sortBy, int
                sortOrder, int offSet, int count, @Nullable Integer minMinInvestAmount, @Nullable Integer maxMinInvestAmount, @Nullable String
                                   searchString, @Nullable String sid, @Nullable String tier);

        Set<String> getInvestedScids();

        void destroySubscriptions();
    }
}
