package com.smallcase.android.smallcase.discover;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smallcase.android.R;
import com.smallcase.android.data.model.SearchResult;
import com.smallcase.android.view.android.GraphikText;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shashankm on 31/08/17.
 */

class SearchStocksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<SearchResult> searchResults;
    private ItemClickListener itemClickListener;
    private String searchString = "";

    interface ItemClickListener {
        void onItemClicked(String sid, String ticker);

        boolean searchSmallcase();
    }

    SearchStocksAdapter(List<SearchResult> searchResults, ItemClickListener itemClickListener) {
        this.searchResults = searchResults;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return 0;
        return 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == 0) {
            viewHolder = new SearchViewHolder(inflater.inflate(R.layout.search_smallcase_layout, parent, false));
        } else {
            viewHolder = new ViewHolder(inflater.inflate(R.layout.search_stock_card, parent, false));
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position == 0) {
            SearchViewHolder searchViewHolder = (SearchViewHolder) holder;
            searchViewHolder.suggestionText.setText("Search smallcases for '" + searchString + "'");
            searchViewHolder.suggestionCard.setOnClickListener(v -> itemClickListener.searchSmallcase());
            return;
        }

        ViewHolder viewHolder = (ViewHolder) holder;
        final SearchResult searchResult = searchResults.get(position - 1);
        viewHolder.stockTicker.setText(searchResult.getStock().getInfo().getTicker());
        viewHolder.stockName.setText(searchResult.getStock().getInfo().getName());
        viewHolder.stockSuggestionCard.setOnClickListener(v -> itemClickListener.onItemClicked(searchResult.getSid(), searchResult.getStock().getInfo().getTicker()));
    }

    @Override
    public int getItemCount() {
        if ("".equals(searchString)) return 0;

        return searchResults.size() + 1;
    }

    void updateList(List<SearchResult> searchResults) {
        this.searchResults.clear();
        this.searchResults.addAll(searchResults);
        notifyDataSetChanged();
    }

    void updateSearchString(String searchString) {
        if (searchString == null) return;

        this.searchString = searchString;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.stock_ticker)
        GraphikText stockTicker;
        @BindView(R.id.stock_name)
        GraphikText stockName;
        @BindView(R.id.stock_suggestion_card)
        View stockSuggestionCard;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class SearchViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.suggestion_text) GraphikText suggestionText;
        @BindView(R.id.smallcase_suggestion_card) View suggestionCard;

        SearchViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
