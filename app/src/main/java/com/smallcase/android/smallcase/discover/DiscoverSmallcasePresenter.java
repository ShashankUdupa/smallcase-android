package com.smallcase.android.smallcase.discover;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import com.smallcase.android.R;
import com.smallcase.android.data.model.SearchResult;
import com.smallcase.android.data.model.Similar;
import com.smallcase.android.data.model.Smallcase;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.model.PExpandedSmallcase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by shashankm on 03/02/17.
 */

class DiscoverSmallcasePresenter implements DiscoverSmallcaseContract.Presenter, StockHelper.StockSearchCallback {
    private static final String TAG = "DiscoverSmallcasePresenter";

    private DiscoverSmallcaseContract.Service serviceContract;
    private DiscoverSmallcaseContract.View viewContract;
    private NetworkHelper networkHelper;
    private int prevRange = -1;
    private boolean prevSelectSmallcaseSelection = false;
    private boolean prevShouldHideInvestedSmallcase = false;
    private List<String> prevTypes;
    private int prevSortOrder;
    private String prevSortBy;
    private Activity activity;
    private StockHelper stockHelper;
    private SharedPrefService sharedPrefService;
    private MarketRepository marketRepository;

    DiscoverSmallcasePresenter(Activity activity, SmallcaseRepository smallcaseRepository, SharedPrefService sharedPrefService, DiscoverSmallcaseContract
            .View viewContract, NetworkHelper networkHelper, StockHelper stockHelper, MarketRepository marketRepository) {
        this.activity = activity;
        this.viewContract = viewContract;
        this.networkHelper = networkHelper;
        this.stockHelper = stockHelper;
        this.sharedPrefService = sharedPrefService;
        this.marketRepository = marketRepository;
        this.serviceContract = new DiscoverSmallcaseService(smallcaseRepository, this, sharedPrefService);
    }

    @Override
    public void fetchSmallcases(@Nullable String sid) {
        String auth = serviceContract.getAuth();

        if (!networkHelper.isNetworkAvailable()) {
            viewContract.showSnackBar(R.string.no_internet);
            return;
        }

        serviceContract.getSmallcases(auth, viewContract.getTypes(), viewContract.getScids(), viewContract.getSearchString() ==
                null ? viewContract.getSortBy() : null, viewContract.getSortOrder(), viewContract.getOffSet(), viewContract.getCount(),
                viewContract.getMinInvestedAmount(), viewContract.getMaxInvestedAmount(), viewContract.getSearchString(), sid, viewContract.getTier());
    }

    @Override
    public void onSmallcasesReceived(List<Smallcase> smallcases) {
        Set<String> scids = new HashSet<>();
        for (String s : serviceContract.getInvestedScids()) {
            scids.add(s.split("#")[0]);
        }

        List<PExpandedSmallcase> smallcaseList = new ArrayList<>();
        for (Smallcase smallcase : smallcases) {
            if (viewContract.shouldHideInvestedSmallcase() && scids.contains(smallcase.getScid())) continue;

            String minInvestment = AppConstants.RUPEE_SYMBOL + AppConstants.getNumberFormatter().format(smallcase.getStats().getMinInvestAmount());
            String indexValue = String.format(Locale.getDefault(), "%.2f", smallcase.getStats().getIndexValue());
            boolean isIndexUp = smallcase.getStats().getIndexValue() > smallcase.getStats().getLastCloseIndex();
            String oneMonth = String.format(Locale.getDefault(), "%.2f", (smallcase.getStats().getReturns().getMonthly() * 100)) + "%";
            String oneYear = String.format(Locale.getDefault(), "%.2f", (smallcase.getStats().getReturns().getYearly() * 100)) + "%";
            boolean isMonthPositive = smallcase.getStats().getReturns().getMonthly() > 0;
            boolean isYearPositive = smallcase.getStats().getReturns().getYearly() > 0;
            PExpandedSmallcase expandedSmallcase = new PExpandedSmallcase("100", smallcase.getInfo().getName(), smallcase.getInfo()
                    .getShortDescription(), minInvestment, indexValue, isIndexUp, oneMonth, oneYear, isMonthPositive, isYearPositive,
                    smallcase.getScid(), scids.contains(smallcase.getScid()), smallcase.getInfo().getTier());
            smallcaseList.add(expandedSmallcase);
        }

        viewContract.showSmallcases(smallcaseList);
        viewContract.updateOffSet(smallcases.size());

        if (smallcases.isEmpty() || smallcases.size() < 10) {
            viewContract.noMoreSmallcases();
            if (viewContract.getSmallcases().isEmpty() && (viewContract.getSearchString() == null || "".equals(viewContract.getSearchString()))) {
                viewContract.noSmallcases(new SpannableString("No smallcases found"));
            } else if (viewContract.getSmallcases().isEmpty()) {
                String noSmallcases = "No smallcases found for \"" + viewContract.getSearchString() + "\"";
                SpannableString spannableString = new SpannableString(noSmallcases);
                spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primary_text)), noSmallcases
                        .length() - (viewContract.getSearchString().length() + 2), noSmallcases.length(), SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
                viewContract.noSmallcases(spannableString);
            }
        } else {
            viewContract.smallcasesRemaining();
        }
    }

    @Override
    public void onFailedFetchingSmallcases() {
        viewContract.showSnackBar(R.string.something_wrong);
    }

    @Override
    public boolean hasFilterNotChanged(int range) {
        return hasTypesNotChanged() && range == prevRange && prevShouldHideInvestedSmallcase
                == viewContract.shouldHideInvestedSmallcase() && hasTierNotChanged();
    }

    @Override
    public void savePreviousFilterSelections(List<String> types, int range, boolean selectSmallcase) {
        prevTypes = new ArrayList<>(types);
        prevRange = range;
        prevSelectSmallcaseSelection = selectSmallcase;
        prevShouldHideInvestedSmallcase = viewContract.shouldHideInvestedSmallcase();
    }

    @Override
    public void savePreviousSortSelection() {
        prevSortBy = viewContract.getSortBy();
        prevSortOrder = viewContract.getSortOrder();
    }

    @Override
    public boolean hasSortNotChanged() {
        return prevSortBy.equals(viewContract.getSortBy()) && prevSortOrder == viewContract.getSortOrder();
    }

    @Override
    public void destroySubscriptions() {
        serviceContract.destroySubscriptions();
    }

    @Override
    public void fetchStocks(String searchString) {
        stockHelper.searchStock(marketRepository, sharedPrefService, searchString, this);
    }

    private boolean hasTypesNotChanged() {
        if (viewContract.getTypes() != null || viewContract.getTypes().isEmpty()) {
            return prevTypes.isEmpty(); // Types have not changed if prev types was also empty
        }

        for (String type : viewContract.getTypes()) {
            if (!prevTypes.contains(type)) return false;
        }
        return true;
    }

    @Override
    public void onStocksReceived(List<SearchResult> searchResults) {
        viewContract.onStocksReceivedFromSearch(searchResults);
    }

    @Override
    public void onFailedFetchingStocks() {
        viewContract.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onFailedFetchingSimilarStocks() {
        // Do nothing, not needed here
    }

    @Override
    public void onSimilarStocksReceived(List<Similar> similarList) {
        // Do nothing, not needed here
    }

    private boolean hasTierNotChanged() {
        return (viewContract.getTier() == null && !prevSelectSmallcaseSelection) || (viewContract
                .getTier() != null && prevSelectSmallcaseSelection);
    }
}
