package com.smallcase.android.smallcase.discover;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.smallcase.android.R;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.smallcase.smallcasedetail.SmallcaseDetailActivity;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.SmallcaseTier;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PCollection;
import com.smallcase.android.view.model.PExpandedSmallcase;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shashankm on 03/02/17.
 */

public class SmallcasesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "SmallcasesAdapter";

    private Activity activity;
    private Animations animations;
    private List<PExpandedSmallcase> smallcases;
    private List<PExpandedSmallcase> savedSmallcases;
    private PCollection collection;
    private boolean fetchedSmallcases;
    private boolean itemsRemaining;
    private boolean prevStateOfItemsRemaining = true;
    private AnalyticsContract analyticsContract;
    private String accessedFrom;

    private final float TWO_DP = AppUtils.getInstance().dpToPx(2);

    public SmallcasesAdapter(Activity activity, PCollection collection, AnalyticsContract analyticsContract, String accessedFrom) {
        this.activity = activity;
        this.collection = collection;
        this.accessedFrom = accessedFrom;
        animations = new Animations();
        fetchedSmallcases = false;
        itemsRemaining = true;
        this.analyticsContract = analyticsContract;
    }

    @Override
    public int getItemViewType(int position) {
        if (null != smallcases && position == (getItemCount() - 1) && itemsRemaining) {
            return 1;
        } else if (null != collection && position == 0) {
            return 2;
        } else {
            return 0;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == 0) {
            viewHolder = new ViewHolder(inflater.inflate(R.layout.expanded_smallcase_card, parent, false));
        } else if (viewType == 2) {
            viewHolder = new CollectionViewHolder(inflater.inflate(R.layout.collection_title, parent, false));
        } else {
            viewHolder = new LoadingHolder(inflater.inflate(R.layout.loading_indicator, parent, false));
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder.getItemViewType() == 0) {
            final ViewHolder holder = (ViewHolder) viewHolder;

            if (null == smallcases || !fetchedSmallcases) {
                holder.smallcaseImage.setImageResource(0);
                holder.smallcaseImage.setBackgroundColor(ContextCompat.getColor(activity, R.color.grey_300));
                toggleView(View.VISIBLE, holder.smallcaseNameLoading, holder.smallcaseDescriptionLoading, holder
                        .smallcaseDescriptionTwoLoading, holder.smallcaseDescriptionThreeLoading, holder.smallcaseDetailsLoading);
                toggleView(View.GONE, holder.smallcaseName, holder.minAmount, holder.indexText, holder.oneMonthText, holder
                        .oneYearText, holder.oneMonthValue, holder.oneYearValue, holder.indexValue, holder.upOrDown, holder.investedIcon);
                holder.smallcaseDescription.setVisibility(View.INVISIBLE); //To maintain smallcase size (divider has margin from description)
                animations.loadingContent(holder.smallcaseNameLoading, holder.smallcaseDescriptionLoading, holder
                        .smallcaseDescriptionTwoLoading, holder.smallcaseDescriptionThreeLoading, holder.smallcaseDetailsLoading);
                return;
            }

            toggleView(View.GONE, holder.smallcaseNameLoading, holder.smallcaseDescriptionLoading, holder
                    .smallcaseDescriptionTwoLoading, holder.smallcaseDescriptionThreeLoading, holder.smallcaseDetailsLoading);
            toggleView(View.VISIBLE, holder.smallcaseName, holder.minAmount, holder.indexText, holder.oneMonthText, holder
                            .oneYearText, holder.smallcaseDescription, holder.oneMonthValue, holder.oneYearValue, holder.indexValue,
                    holder.upOrDown);
            holder.smallcaseImage.setBackgroundColor(0);

            final PExpandedSmallcase smallcase = smallcases.get(null == collection ? position : (position - 1));
            Glide.with(activity)
                    .load(smallcase.getSmallcaseImage())
                    .asBitmap()
                    .placeholder(ContextCompat.getColor(activity, R.color.grey_300))
                    .into(new BitmapImageViewTarget(holder.smallcaseImage) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable roundImage = RoundedBitmapDrawableFactory.create(activity.getResources(), resource);
                            roundImage.setCornerRadius(TWO_DP);
                            holder.smallcaseImage.setImageDrawable(roundImage);
                        }
                    });
            holder.smallcaseName.setText(smallcase.getSmallcaseName());
            holder.minAmount.setText(smallcase.getMinAmount());
            holder.smallcaseDescription.setText(smallcase.getSmallcaseDescription());
            holder.indexValue.setText(smallcase.getIndexValue());
            holder.oneMonthValue.setText(smallcase.getOneMonth());
            holder.oneYearValue.setText(smallcase.getOneYear());
            Glide.with(activity)
                    .load(smallcase.isIndexUp() ? R.drawable.up_arrow : R.drawable.down_arrow_red)
                    .asBitmap()
                    .into(holder.upOrDown);

            if (smallcase.getTier() != null && SmallcaseTier.PREMIUM.equals(smallcase.getTier())) {
                holder.premiumLabel.setVisibility(View.VISIBLE);
                Glide.with(activity)
                        .load(R.drawable.premium_label)
                        .into(holder.premiumLabel);
            } else {
                holder.premiumLabel.setVisibility(View.GONE);
            }

            if (smallcase.isInvested()) {
                holder.investedIcon.setVisibility(View.VISIBLE);
                Glide.with(activity)
                        .load(R.drawable.invested_green)
                        .asBitmap()
                        .into(holder.investedIcon);
            } else {
                holder.investedIcon.setImageResource(0);
            }

            int monthColor = smallcase.isMonthPositive() ? R.color.green_600 : R.color.red_600;
            int yearColor = smallcase.isYearPositive() ? R.color.green_600 : R.color.red_600;
            holder.oneMonthValue.setTextColor(ContextCompat.getColor(activity, monthColor));
            holder.oneYearValue.setTextColor(ContextCompat.getColor(activity, yearColor));

            holder.smallcaseCard.setOnClickListener(v -> {
                Intent intent = new Intent(activity, SmallcaseDetailActivity.class);
                intent.putExtra(SmallcaseDetailActivity.SCID, smallcase.getScid());
                intent.putExtra(SmallcaseDetailActivity.TITLE, smallcase.getSmallcaseName());
                intent.putExtra(SmallcaseDetailActivity.ACCESSED_FROM, collection != null ? "Collection" : "All smallcases");
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.left_to_right, R.anim.fade_out_with_scale);
            });
        } else if (viewHolder.getItemViewType() == 2) {
            CollectionViewHolder holder = (CollectionViewHolder) viewHolder;
            holder.collectionDescription.setText(collection.getCollectionDescription());
            Glide.with(activity)
                    .load(collection.getCollectionImage())
                    .placeholder(ContextCompat.getColor(activity, R.color.grey_300))
                    .into(holder.collectionImage);
        }
    }

    @Override
    public int getItemCount() {
        if (null == smallcases || !fetchedSmallcases) {
            // Loading card views
            return 4;
        } else if (itemsRemaining) {
            // Smallcases with loading indicators
            return null == collection ? smallcases.size() + 1 : smallcases.size() + 2;
        } else {
            // Smallcases without indicator (when there are no more items to fetch)
            return null == collection ? smallcases.size() : smallcases.size() + 1;
        }
    }

    public void onSmallcasesReceived(List<PExpandedSmallcase> smallcases) {
        fetchedSmallcases = true;

        if (null != this.smallcases && !this.smallcases.isEmpty()) {
            int prevSize = this.smallcases.size();
            this.smallcases.addAll(smallcases);
            notifyItemRangeChanged(prevSize, this.smallcases.size() + 1);
            return;
        }

        this.smallcases = new ArrayList<>(smallcases);
        notifyDataSetChanged();
    }

    void saveSmallcases() {
        if (null == smallcases) return;

        fetchedSmallcases = false;

        // To prevent just the loading indicator being shown
        itemsRemaining = false;

        // If smallcases fetched are already saved, don't save them again (since they would be searched smallcases)
        if (null != savedSmallcases && !savedSmallcases.isEmpty()) {
            clearExistingList();
            return;
        }

        savedSmallcases = new ArrayList<>(smallcases);
        clearExistingList();
    }

    public void noMoreSmallcases() {
        prevStateOfItemsRemaining = itemsRemaining;
        if (!itemsRemaining) return;
        itemsRemaining = false;

        //If smallcases arne't fetched, don't mess with the adapter
        if (smallcases.isEmpty()) return;

        // Remove loading indicator
        notifyItemRemoved(smallcases.size());
        notifyItemChanged(smallcases.size());
    }

    void setItemsRemaining() {
        itemsRemaining = true;
    }

    // Returns if reload was successful
    boolean reloadFetchedSmallcases() {
        // If no smallcases are saved
        if (null == savedSmallcases || savedSmallcases.isEmpty()) {
            if (smallcases != null) clearExistingList();
            return false;
        }

        // Reload saved smallcases (all smallcases fetched before the user hit search).
        smallcases = new ArrayList<>(savedSmallcases);
        savedSmallcases.clear();
        notifyDataSetChanged();
        itemsRemaining = prevStateOfItemsRemaining;
        return true;
    }

    private void clearExistingList() {
        smallcases.clear();
        notifyDataSetChanged();
    }

    void finishedFetchingSmallcases() {
        fetchedSmallcases = true;
        notifyDataSetChanged();
    }

    private void toggleView(int visibility, View... views) {
        for (View view : views) {
            view.setVisibility(visibility);
        }
    }

    @Nullable
    public List<PExpandedSmallcase> getSmallcases() {
        return smallcases;
    }

    public void removeSmallcase(String scid) {
        if (smallcases != null && !smallcases.isEmpty()) {
            int smallcasePosition = -1;
            for (int i = 0; i < smallcases.size(); i++) {
                if (scid.equals(smallcases.get(i).getScid())) {
                    smallcasePosition = i;
                    break;
                }
            }

            if (smallcasePosition != -1) {
                smallcases.remove(smallcasePosition);
                notifyDataSetChanged();
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.smallcase_name_loading)
        View smallcaseNameLoading;
        @BindView(R.id.smallcase_description_loading)
        View smallcaseDescriptionLoading;
        @BindView(R.id.smallcase_description_two_loading)
        View smallcaseDescriptionTwoLoading;
        @BindView(R.id.smallcase_description_three_loading)
        View smallcaseDescriptionThreeLoading;
        @BindView(R.id.smallcase_details_loading)
        View smallcaseDetailsLoading;

        @BindView(R.id.image_smallcase)
        ImageView smallcaseImage;
        @BindView(R.id.text_smallcase_name)
        GraphikText smallcaseName;
        @BindView(R.id.text_smallcase_min_amount)
        GraphikText minAmount;
        @BindView(R.id.text_smallcase_description)
        GraphikText smallcaseDescription;
        @BindView(R.id.text_index)
        GraphikText indexText;
        @BindView(R.id.text_index_value)
        GraphikText indexValue;
        @BindView(R.id.text_one_month_return)
        GraphikText oneMonthText;
        @BindView(R.id.text_return_one_month)
        GraphikText oneMonthValue;
        @BindView(R.id.text_one_year_return)
        GraphikText oneYearText;
        @BindView(R.id.text_return_one_year)
        GraphikText oneYearValue;
        @BindView(R.id.image_up_or_down)
        ImageView upOrDown;
        @BindView(R.id.smallcase_card)
        View smallcaseCard;
        @BindView(R.id.invested_icon)
        ImageView investedIcon;
        @BindView(R.id.premium_label)
        ImageView premiumLabel;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private class LoadingHolder extends RecyclerView.ViewHolder {

        LoadingHolder(View itemView) {
            super(itemView);
        }
    }

    class CollectionViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.collection_image)
        ImageView collectionImage;
        @BindView(R.id.collection_description)
        GraphikText collectionDescription;

        CollectionViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
