package com.smallcase.android.smallcase.discover;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.smallcase.smallcasedetail.SmallcaseDetailActivity;
import com.smallcase.android.util.Animations;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PFeaturedSmallcase;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shashankm on 24/01/17.
 */

class FeaturedSmallcaseAdapter extends RecyclerView.Adapter<FeaturedSmallcaseAdapter.ViewHolder> {
    private static final String TAG = FeaturedSmallcaseAdapter.class.getSimpleName();
    private List<PFeaturedSmallcase> featuredSmallcases;
    private Activity activity;
    private boolean fetchedFeaturedSmallcase = false;
    private Animations animations;
    private AnalyticsContract analyticsContract;

    FeaturedSmallcaseAdapter(@NonNull Activity activity, AnalyticsContract analyticsContract) {
        this.activity = activity;
        this.analyticsContract = analyticsContract;
        animations = new Animations();
    }

    @Override
    public FeaturedSmallcaseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.featured_smallcase_card,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FeaturedSmallcaseAdapter.ViewHolder holder, int position) {
        if (null == featuredSmallcases || !fetchedFeaturedSmallcase) {
            animations.loadingContent(holder.smallcaseNameLoading, holder.smallcaseDescriptionLoading);
            return;
        }

        animations.stopAnimation();
        holder.smallcaseNameLoading.setVisibility(View.GONE);
        holder.smallcaseDescriptionLoading.setVisibility(View.GONE);
        holder.smallcaseName.setVisibility(View.VISIBLE);
        holder.smallcaseDescription.setVisibility(View.VISIBLE);

        holder.featuredSmallcaseImage.setBackgroundColor(0);

        final PFeaturedSmallcase featuredSmallcase = featuredSmallcases.get(position);
        Glide.with(activity)
                .load(featuredSmallcase.getSmallcaseImage())
                .asBitmap()
                .placeholder(activity.getResources().getColor(R.color.grey_300))
                .into(holder.featuredSmallcaseImage);
        holder.smallcaseLabel.setText(featuredSmallcase.getLabel());

        if (featuredSmallcase.isInvested()) {
            holder.investedImage.setVisibility(View.VISIBLE);
            Glide.with(activity)
                    .load(R.drawable.invested_green)
                    .asBitmap()
                    .into(holder.investedImage);
        } else {
            holder.investedImage.setVisibility(View.GONE);
        }

        holder.smallcaseName.setText(featuredSmallcase.getSmallcaseName());
        holder.smallcaseDescription.setText(featuredSmallcase.getDescription());
        holder.smallcaseIndex.setText(featuredSmallcase.getIndex());
        Glide.with(activity)
                .load(featuredSmallcase.isIndexUp() ? R.drawable.up_arrow : R.drawable.down_arrow_red)
                .asBitmap()
                .into(holder.indexUpOrDown);

        holder.featuredContainer.setOnClickListener(v -> {
            Intent intent = new Intent(activity, SmallcaseDetailActivity.class);
            intent.putExtra(SmallcaseDetailActivity.SCID, featuredSmallcase.getScid());
            intent.putExtra(SmallcaseDetailActivity.TITLE, featuredSmallcase.getSmallcaseName());
            intent.putExtra(SmallcaseDetailActivity.ACCESSED_FROM, "Featured smallcases");
            activity.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        if (null == featuredSmallcases || !fetchedFeaturedSmallcase) return 2;
        return featuredSmallcases.size();
    }

    void onFeaturedSmallcasesReceived(@NonNull List<PFeaturedSmallcase> featuredSmallcases) {
        this.featuredSmallcases = new ArrayList<>(featuredSmallcases);
        fetchedFeaturedSmallcase = true;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_featured_smallcase)
        ImageView featuredSmallcaseImage;
        @BindView(R.id.text_smallcase_label)
        GraphikText smallcaseLabel;
        @BindView(R.id.image_invested)
        ImageView investedImage;
        @BindView(R.id.text_smallcase_name)
        GraphikText smallcaseName;
        @BindView(R.id.text_smallcase_description)
        GraphikText smallcaseDescription;
        @BindView(R.id.text_smallcase_index)
        GraphikText smallcaseIndex;
        @BindView(R.id.image_index_up_or_down)
        ImageView indexUpOrDown;
        @BindView(R.id.smallcase_name_loading)
        View smallcaseNameLoading;
        @BindView(R.id.smallcase_description_loading)
        View smallcaseDescriptionLoading;
        @BindView(R.id.featured_container)
        View featuredContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
