package com.smallcase.android.smallcase.smallcasedetail;

import android.support.annotation.Nullable;
import android.text.SpannableString;

import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.model.Historical;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.ReBalanceUpdate;
import com.smallcase.android.data.model.SmallcaseDetail;
import com.smallcase.android.data.model.UnInvestedSmallcase;
import com.smallcase.android.view.model.PPlainNews;
import com.smallcase.android.view.model.PPoints;
import com.smallcase.android.view.model.PSmallcaseDetail;
import com.smallcase.android.view.model.PSmallcaseInvestment;
import com.smallcase.android.view.model.Stock;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by shashankm on 28/02/17.
 */

interface SmallcaseDetailContract {
    interface View {

        void invalidHistorical();

        void onHistoricalReceived(List<PPoints> smallcasePoints);

        String getDuration();

        void showSnackBar(int errId);

        void onNewsReceived(List<PPlainNews> newsList);

        void noNewsForSmallcase();

        void onSmallcaseInvestmentsReceived(PSmallcaseInvestment smallcaseInvestment);

        void onSmallcaseDetailsReceived(PSmallcaseDetail smallcaseDetail);

        void marketIsOpen();

        String getSmallcaseName();

        List<PPlainNews> getNewsList();

        void showSimpleSnackbar(int errId);

        void showBuy();

        void showInvestMore();

        void unGreyBuyOrInvestMore();

        void showNegativeContainer(SpannableString title, @Nullable String description, boolean showArrow);

        void hideInvestCard();

        double getInvestmentAmount();

        void onMinPriceReceived(SpannableString minPrice);

        void showMarketClosed();

        void setInitialMinInvestmentAmount(SpannableString minInvestAmount);

        void hideStockBlurIfVisible();

        void showStocksBlur();

        void sendUserToLoginOrSignUp();

        void showWatchlistTutorial();

        void showIndexTutorial();

        void showReBalancePending(SpannableString label);

        void showPremium();

        void onDraftDetailsReceived(PSmallcaseDetail smallcaseDetail);

        void showReturns(String oneMonth, String oneYear, boolean isMonthPositive, boolean isYearPositive);

        void deleteSuccessful();

        void hideLoader();
    }

    interface Presenter {

        void getSmallcaseDetailData();

        void onSmallcaseDetailsReceived(UnInvestedSmallcase unInvestedSmallcase);

        void onSmallcaseHistoricalReceived(Historical historical);

        void canContinueTransaction(List<Stock> stockList);

        void onSmallcaseInvestmentsReceived(SmallcaseDetail smallcaseDetail);

        void onSmallcaseNewsReceived(List<News> newses);

        void onFailedFetchingSmallcaseDetails();

        void onFailedFetchingSmallcaseHistorical();

        void onFailedFetchingSmallcaseInvestments();

        void onFailedFetchingSmallcaseNews();

        void fetchHistorical();

        void toggleWatchlist(boolean shouldWatchlist);

        void onMarketStatusReceived(ResponseBody responseBody);

        void onFailedFetchingMarketStatus(Throwable throwable);

        void checkMarketStatus();

        boolean isInvested();

        void onInvestmentDetailsClicked();

        void setIScid(String iScid);

        void onSeeAllNewsClicked();

        boolean isSmallcaseValid();

        void onNegativeContainerClicked();

        List<Stock> getStockList();

        SpannableString getMinInvestment();

        DraftSmallcase getDraftSmallcase();

        void setSource(String source);

        @Nullable String getSource();

        void addReminder();

        void destroySubscriptions();

        boolean isSmallcasePremium();

        boolean shouldStockTutorialBeShown();

        void onReBalanceUpdatesFetched(List<ReBalanceUpdate> reBalanceUpdates);

        void onFailedFetchingReBalanceUpdates();

        boolean isReBalancePending();

        ReBalanceUpdate getReBalanceUpdate();

        void onFailedFetchingUserData();

        void onUserDataCached();

        void dontShowCustomizeInfoPopup();

        boolean shouldShowCustomizePopUp();

        void getDraftDetailData(String did, List<String> sids);

        void onDraftFetchedSuccessfully(DraftSmallcase draftSmallcase);

        void onFailedFetchingDraft();

        void onStockHistoricalReceived(ResponseBody responseBody) throws JSONException, IOException;

        void onFailedFetchingStockHistorical();

        void deleteDraft();

        void onDraftDeleted();

        void onFailedDeletingDraft();

        boolean isDraft();
    }

    interface Service {

        void getSmallcaseDetail(String scid);

        void getInvestedSmallcaseDetail(String iScid);

        void getSmallcaseNews(String scid);

        void getSmallcaseHistorical(String scid, String benchmarkType, String benchmarkId, String duration);

        boolean isSmallcaseWatchlisted(String scid);

        String getAuth();

        void addToWatchlist(String scid);

        void removeFromWatchlist(String scid);

        void getIfMarketIsOpen();

        @Nullable String getIScid(String scid);

        void addMarketOpenReminder(HashMap<String, String> body);

        void destroySubscriptions();

        void getPendingActions();

        void getUserData();

        void setFlag(HashMap<String, Object> body);

        void getDraftDetails(String did);

        void getStockHistorical(List<String> sids, String duration);

        void deleteDraft(HashMap<String, String> did);
    }
}
