package com.smallcase.android.smallcase.discover;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.news.WebActivity;
import com.smallcase.android.smallcase.smallcasedetail.SmallcaseDetailActivity;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PNewsSmallcase;
import com.smallcase.android.view.model.PSmallcaseWithNews;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shashankm on 31/01/17.
 */

public class SmallcaseNewsAdapter extends RecyclerView.Adapter<SmallcaseNewsAdapter.ViewHolder> {
    private static final String TAG = "SmallcaseNewsAdapter";
    private Activity activity;
    private List<PSmallcaseWithNews> newsList;
    private boolean fetchedNews = false;
    private Animations animations;
    private AnalyticsContract analyticsContract;
    private String accessedFrom;

    public SmallcaseNewsAdapter(Activity activity, AnalyticsContract analyticsContract, String accessedFrom) {
        this.activity = activity;
        this.analyticsContract = analyticsContract;
        this.accessedFrom = accessedFrom; // Analytics
        this.animations = new Animations();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.smallcase_with_news_card, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.newsDivider.setVisibility(View.VISIBLE);
        if (null == newsList || !fetchedNews) {
            animations.loadingContent(holder.dateLoading, holder.headingLoading, holder.smallcaseNameLoading);
            return;
        }

        animations.stopAnimation();
        toggleView(View.GONE, holder.dateLoading, holder.headingLoading, holder.smallcaseNameLoading);
        toggleView(View.VISIBLE, holder.sinceNewsText, holder.newsHeading, holder.timeAgo);
        holder.smallcaseImage.setBackgroundColor(0);
        holder.newsImage.setBackgroundColor(0);

        final PSmallcaseWithNews smallcaseWithNews = newsList.get(position);
        if (smallcaseWithNews.getInvestedSmallcase() == null || smallcaseWithNews.getInvestedSmallcase().isEmpty()) return;

        final PNewsSmallcase smallcase = smallcaseWithNews.getInvestedSmallcase().get(0);
        holder.smallcaseName.setText(smallcase.getSmallcaseName());
        holder.percentChange.setText(smallcase.getChange());
        int color = smallcase.isChangePositive() ? R.color.green_600 : R.color.red_600;
        holder.percentChange.setTextColor(ContextCompat.getColor(activity, color));
        Glide.with(activity)
                .load(smallcase.isChangePositive() ? R.drawable.up_arrow : R.drawable.down_arrow_red)
                .asBitmap()
                .into(holder.upOrDown);
        Glide.with(activity)
                .load(smallcase.getSmallcaseImage())
                .asBitmap()
                .placeholder(activity.getResources().getDrawable(R.color.grey_300))
                .into(holder.smallcaseImage);

        holder.newsHeading.setText(smallcaseWithNews.getNewsHeadline());
        holder.timeAgo.setText(AppUtils.getInstance().getTimeAgoDate(smallcaseWithNews.getNewsDate()));
        Glide.with(activity)
                .load(smallcaseWithNews.getNewsImage())
                .asBitmap()
                .placeholder(activity.getResources().getDrawable(R.color.grey_300))
                .into(holder.newsImage);

        holder.newsCard.setOnClickListener(v -> {
            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("accessedFrom", accessedFrom);
            eventProperties.put("newsDate", AppUtils.getInstance().getTimeAgoDate(smallcaseWithNews.getNewsDate()));
            analyticsContract.sendEvent(eventProperties, "Viewed News Article", Analytics.MIXPANEL);

            Intent intent = new Intent(activity, WebActivity.class);
            intent.putExtra(WebActivity.TITLE, "smallcase News");
            intent.putExtra(WebActivity.URL, smallcaseWithNews.getNewsLink());
            intent.putExtra(WebActivity.IS_LITE_URL, true);
            activity.startActivity(intent);
            activity.overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
        });

        holder.smallcaseCard.setOnClickListener(v -> {
            Intent intent = new Intent(activity, SmallcaseDetailActivity.class);
            intent.putExtra(SmallcaseDetailActivity.TITLE, smallcase.getSmallcaseName());
            intent.putExtra(SmallcaseDetailActivity.SCID, smallcase.getScid());
            intent.putExtra(SmallcaseDetailActivity.ACCESSED_FROM, accessedFrom + " by news");
            activity.startActivity(intent);
        });
    }

    private void toggleView(int visibility, View... views) {
        for (View view : views) view.setVisibility(visibility);
    }

    @Override
    public int getItemCount() {
        if (null == newsList || !fetchedNews) return 2;
        return newsList.size();
    }

    public void onNewsReceived(List<PSmallcaseWithNews> newsList) {
        this.newsList = new ArrayList<>(newsList);
        fetchedNews = true;
        notifyDataSetChanged();
    }

    public List<PSmallcaseWithNews> getNews() {
        return newsList;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_p_l_today)
        ImageView newsImage;
        @BindView(R.id.text_news_headline)
        TextView newsHeading;
        @BindView(R.id.text_time_ago)
        TextView timeAgo;
        @BindView(R.id.news_card)
        View newsCard;
        @BindView(R.id.divider_news)
        View newsDivider;
        @BindView(R.id.smallcase_name_loading)
        View smallcaseNameLoading;
        @BindView(R.id.headline_loading)
        View headingLoading;
        @BindView(R.id.date_loading)
        View dateLoading;
        @BindView(R.id.text_since_news)
        GraphikText sinceNewsText;
        @BindView(R.id.up_or_down_since_news)
        ImageView upOrDown;
        @BindView(R.id.percent_change)
        GraphikText percentChange;
        @BindView(R.id.image_smallcase)
        ImageView smallcaseImage;
        @BindView(R.id.text_smallcase_name)
        GraphikText smallcaseName;
        @BindView(R.id.smallcase_card_un_invested)
        View smallcaseCard;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
