package com.smallcase.android.smallcase.discover;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.util.Animations;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PCollection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shashankm on 30/01/17.
 */

class CollectionsAdapter extends RecyclerView.Adapter<CollectionsAdapter.ViewHolder> {
    private static final String TAG = CollectionsAdapter.class.getSimpleName();

    private Activity activity;
    private boolean fetchedCollections = false;
    private List<PCollection> collectionList;
    private Animations animations;
    private AnalyticsContract analyticsContract;

    CollectionsAdapter(Activity activity, AnalyticsContract analyticsContract) {
        this.activity = activity;
        this.analyticsContract = analyticsContract;
        this.animations = new Animations();
    }

    @Override
    public CollectionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.collection_card, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CollectionsAdapter.ViewHolder holder, int position) {
        if (null == collectionList || !fetchedCollections) {
            animations.loadingContent(holder.collectionNameLoading, holder.descriptionLoading, holder
                    .descriptionTwoLoading, holder.smallcaseNumberLoading);
            return;
        }

        animations.stopAnimation();
        hideViews(holder.collectionNameLoading, holder.descriptionLoading, holder.descriptionTwoLoading, holder.smallcaseNumberLoading);

        holder.collectionsImage.setBackgroundColor(0);

        final PCollection collection = collectionList.get(position);
        holder.collectionName.setText(collection.getCollectionName());
        holder.collectionDescription.setText(collection.getCollectionDescription());
        Glide.with(activity)
                .load(collection.getCollectionImage())
                .asBitmap()
                .placeholder(activity.getResources().getColor(R.color.grey_300))
                .into(holder.collectionsImage);
        Glide.with(activity)
                .load(R.drawable.arrow_right)
                .asBitmap()
                .into(holder.arrow);

        holder.parent.setOnClickListener(v -> {
            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("collectionName", collection.getCollectionName());
            analyticsContract.sendEvent(eventProperties, "Viewed Collection", Analytics.MIXPANEL, Analytics.CLEVERTAP);

            Intent intent = new Intent(activity, DiscoverSmallcaseActivity.class);
            intent.putExtra(DiscoverSmallcaseActivity.COLLECTION, collection);
            intent.putStringArrayListExtra(DiscoverSmallcaseActivity.SCIDS, (ArrayList<String>) collection.getScids());
            activity.startActivity(intent);
        });

        holder.seeCollection.setText("See " + collection.getSmallcasesNumber());
    }

    @Override
    public int getItemCount() {
        if (!fetchedCollections) return 2;
        return collectionList.size();
    }

    void onCollectionReceived(List<PCollection> collectionList) {
        this.collectionList = new ArrayList<>(collectionList);
        fetchedCollections = true;
        notifyDataSetChanged();
    }

    private void hideViews(View... views) {
        for (View view : views) view.setVisibility(View.GONE);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_collection)
        ImageView collectionsImage;
        @BindView(R.id.text_collection_name)
        GraphikText collectionName;
        @BindView(R.id.text_collection_description)
        GraphikText collectionDescription;
        @BindView(R.id.collection_name_loading)
        View collectionNameLoading;
        @BindView(R.id.description_loading)
        View descriptionLoading;
        @BindView(R.id.description_two_loading)
        View descriptionTwoLoading;
        @BindView(R.id.smallcase_number_loading)
        View smallcaseNumberLoading;
        @BindView(R.id.image_arrow)
        ImageView arrow;
        @BindView(R.id.collection_parent_card)
        View parent;
        @BindView(R.id.see_collection)
        GraphikText seeCollection;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
