package com.smallcase.android.smallcase.smallcasedetail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.smallcase.android.R;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PPoints;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by shashankm on 03/03/17.
 */

public class PerformanceFragment extends Fragment {
    private static final String TAG = "PerformanceFragment";

    @BindView(R.id.performance_chart)
    LineChart performanceChart;
    @BindView(R.id.one_month)
    GraphikText oneMonth;
    @BindView(R.id.three_months)
    GraphikText threeMonths;
    @BindView(R.id.six_months)
    GraphikText sixMonths;
    @BindView(R.id.one_year)
    GraphikText oneYear;
    @BindView(R.id.two_years)
    GraphikText twoYears;
    @BindView(R.id.smallcase_index)
    GraphikText smallcaseIndex;
    @BindView(R.id.nifty_index)
    GraphikText niftyIndex;
    @BindView(R.id.end_date)
    GraphikText endDate;
    @BindView(R.id.start_date)
    GraphikText startDate;

    private int selectedIndexDuration = 4;
    private List<Entry> niftyEntries;
    private List<Entry> smallcaseEntries;
    private List<String> dates;
    private GraphikText prevSelected;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.performance, container, false);
        ButterKnife.bind(this, view);

        prevSelected = twoYears;
        performanceChart.getXAxis().setDrawLabels(false);
        performanceChart.getAxisRight().setEnabled(false);
        performanceChart.getAxisLeft().setEnabled(false);
        performanceChart.getDescription().setEnabled(false);
        performanceChart.setDrawGridBackground(false);
        performanceChart.getAxisRight().setDrawLabels(false);
        performanceChart.getAxisLeft().setDrawLabels(false);
        performanceChart.getXAxis().setDrawAxisLine(false);
        performanceChart.getLegend().setEnabled(false);
        performanceChart.setDrawMarkers(false);
        performanceChart.setTouchEnabled(true);
        performanceChart.setPinchZoom(false);
        performanceChart.setScaleEnabled(false);
        performanceChart.setDoubleTapToZoomEnabled(false);
        performanceChart.getXAxis().setDrawGridLines(false);

        return view;
    }

    @OnClick({R.id.one_month, R.id.three_months, R.id.six_months, R.id.one_year, R.id.two_years})
    public void setTime(View view) {
        if (prevSelected == view) return;

        prevSelected.setTextColor(ContextCompat.getColor(getContext(), R.color.blue_800));
        prevSelected.setBackground(null);

        smallcaseIndex.setText("");
        niftyIndex.setText("");
        endDate.setText("");
        startDate.setText("");

        switch (view.getId()) {
            case R.id.one_month:
                oneMonth.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                oneMonth.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_blue_tab));
                prevSelected = oneMonth;
                selectedIndexDuration = 0;
                break;

            case R.id.three_months:
                threeMonths.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                threeMonths.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_blue_tab));
                prevSelected = threeMonths;
                selectedIndexDuration = 1;
                break;

            case R.id.six_months:
                sixMonths.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                sixMonths.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_blue_tab));
                prevSelected = sixMonths;
                selectedIndexDuration = 2;
                break;

            case R.id.one_year:
                oneYear.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                oneYear.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_blue_tab));
                prevSelected = oneYear;
                selectedIndexDuration = 3;
                break;

            case R.id.two_years:
                twoYears.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                twoYears.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.rounded_blue_tab));
                prevSelected = twoYears;
                selectedIndexDuration = 4;
                break;
        }

        ((SmallcaseDetailActivity) getActivity()).fetchHistorics(prevSelected.getText().toString());
    }

    void onDataReceived(List<PPoints> smallcasePoints) {
        if (!isAdded()) return;

        smallcaseEntries = new ArrayList<>();
        niftyEntries = new ArrayList<>();
        for (int i = 0; i < smallcasePoints.size(); i++) {
            smallcaseEntries.add(new Entry(i, (float) AppUtils.getInstance().round(smallcasePoints.get(i).getSmallcaseIndex(), 2)));
            niftyEntries.add(new Entry(i, (float) AppUtils.getInstance().round(smallcasePoints.get(i).getNiftyIndex(), 2)));
        }

        final LineDataSet smallcaseDataSet = new LineDataSet(smallcaseEntries, "");
        smallcaseDataSet.setDrawValues(false);
        smallcaseDataSet.setDrawCircleHole(false);
        smallcaseDataSet.setDrawCircles(false);
        smallcaseDataSet.setDrawFilled(false);
        smallcaseDataSet.setColor(ContextCompat.getColor(getContext(), R.color.blue_800));
        smallcaseDataSet.setLineWidth(2f);
        smallcaseDataSet.setHighlightLineWidth(1f);
        smallcaseDataSet.setDrawHorizontalHighlightIndicator(false);
        smallcaseDataSet.setHighLightColor(ContextCompat.getColor(getContext(),R.color.grey_300));

        LineDataSet niftyDataSet = new LineDataSet(niftyEntries, "");
        niftyDataSet.setDrawValues(false);
        niftyDataSet.setDrawCircleHole(false);
        niftyDataSet.setDrawCircles(false);
        niftyDataSet.setDrawHorizontalHighlightIndicator(false);
        niftyDataSet.setDrawFilled(false);
        niftyDataSet.setColor(ContextCompat.getColor(getContext(), R.color.red_600));
        niftyDataSet.setLineWidth(2f);
        niftyDataSet.setHighlightLineWidth(1f);
        niftyDataSet.setHighLightColor(ContextCompat.getColor(getContext(),R.color.grey_300));

        List<ILineDataSet> lineDataSets = new ArrayList<>();
        lineDataSets.add(smallcaseDataSet);
        lineDataSets.add(niftyDataSet);

        LineData lineData = new LineData(lineDataSets);

        dates = new ArrayList<>();
        for (PPoints smallcasePoint : smallcasePoints) {
            dates.add(smallcasePoint.getDate());
        }

        startDate.setText(dates.get(0) + " -");

        performanceChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                int pos = (int) e.getX();
                Entry niftyEntry = niftyEntries.get(pos);
                Entry smallcaseEntry = smallcaseEntries.get(pos);
                smallcaseIndex.setText(String.format(Locale.getDefault(), "%.2f", smallcaseEntry.getY() - 100) + "%");
                niftyIndex.setText(String.format(Locale.getDefault(), "%.2f", niftyEntry.getY() - 100) + "%");
                endDate.setText(dates.get(pos));
            }

            @Override
            public void onNothingSelected() {

            }
        });

        performanceChart.setData(lineData);
        performanceChart.highlightValue(smallcaseEntries.size() - 1, 0);

        showChart();
    }

    public int getSelectedDuration() {
        return selectedIndexDuration;
    }

    private void showChart() {
        performanceChart.animateX(500, Easing.EasingOption.EaseOutCubic);
        performanceChart.invalidate();
    }
}
