package com.smallcase.android.create;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.StringSignature;
import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.model.Info;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.user.coustomize.DraftSmallcasesActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.util.SmallcaseTier;
import com.smallcase.android.view.android.GraphikText;
import com.theartofdev.edmodo.cropper.CropImage;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateInfoActivity extends AppCompatActivity implements CreateInfoHelper.View {
  private static final String TAG = "CreateInfoActivity";
  public static final String IS_EDIT = "is_edit";
  public static final String DRAFT_SMALLCASE = "draft_smallcase"; // Required

  @BindView(R.id.smallcase_name)
  EditText smallcaseName;
  @BindView(R.id.smallcase_description)
  EditText smallcaseDescription;
  @BindView(R.id.parent)
  View parent;
  @BindView(R.id.text_tool_bar_title)
  GraphikText toolBarTitle;
  @BindView(R.id.smallcase_name_divider)
  View smallcaseNameDivider;
  @BindView(R.id.smallcase_description_divider)
  View smallcaseDescriptionDivider;
  @BindView(R.id.edit_image_container)
  View editImageContainer;
  @BindView(R.id.smallcase_image)
  ImageView smallcaseImage;

  private CreateInfoHelper createInfoHelper;
  private boolean isImageUploaded = false;
  private boolean isInfoUpdated = false;
  private Uri cropImageUri;
  private ProgressDialog progressDialog;
  private DraftSmallcase draftSmallcase;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_create_info);

    ButterKnife.bind(this);

    draftSmallcase = (DraftSmallcase) getIntent().getSerializableExtra(DRAFT_SMALLCASE);
    boolean isEdit = getIntent().getBooleanExtra(IS_EDIT, false);

    toolBarTitle.setText(isEdit ? "Edit smallcase" : "Create smallcase");
    smallcaseName.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR));
    smallcaseDescription.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR));

    createInfoHelper = new CreateInfoHelper(this, new UserSmallcaseRepository(), new SharedPrefService(this));
    if (isEdit) {
      if (SmallcaseSource.CREATED.equals(draftSmallcase.getSource())) {
        editImageContainer.setVisibility(View.VISIBLE);
        Glide.with(this)
            .load(AppConstants.CREATED_SMALLCASE_IMAGE_URL + "187/" + draftSmallcase.getScid() + ".png")
            .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
            .placeholder(getResources().getDrawable(R.color.grey_300))
            .into(smallcaseImage);
      }
    } else {
      editImageContainer.setVisibility(View.GONE);
    }

    if (draftSmallcase != null) {
      smallcaseName.setText(draftSmallcase.getInfo().getName());
      smallcaseDescription.setText(draftSmallcase.getInfo().getShortDescription());
    }

    smallcaseName.setOnFocusChangeListener((v, hasFocus) -> toggleSelectionColor(smallcaseNameDivider, hasFocus));
    smallcaseDescription.setOnFocusChangeListener((v, hasFocus) -> toggleSelectionColor(smallcaseDescriptionDivider, hasFocus));

    smallcaseDescription.setOnEditorActionListener((v, actionId, event) -> {
      if (actionId == EditorInfo.IME_ACTION_GO) {
        View view = getCurrentFocus();
        if (view != null) {
          InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
          if (imm != null) imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        continueToCreate();
        return true;
      }
      return false;
    });
  }

  @OnClick(R.id.back)
  public void back() {
    onBackPressed();
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
  }

  @SuppressLint("NewApi")
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    // handle result of pick image chooser
    if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
      Uri imageUri = CropImage.getPickImageResultUri(this, data);

      // For API >= 23 we need to check specifically that we have permissions to read external storage.
      if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
        // request permissions and handle the result in onRequestPermissionsResult()
        cropImageUri = imageUri;
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
            CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
      } else {
        // no permissions required or already granted, can start crop image activity
        startCropImageActivity(imageUri);
      }
    }

    // Image cropped or not
    if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
      CropImage.ActivityResult result = CropImage.getActivityResult(data);
      if (resultCode == RESULT_OK) {
        cropImageUri = result.getUri();
        Glide.with(this)
            .load(cropImageUri)
            .signature(new StringSignature(String.valueOf(System.currentTimeMillis())))
            .placeholder(getResources().getDrawable(R.color.grey_300))
            .into(smallcaseImage);
      } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
        Exception error = result.getError();
        SentryAnalytics.getInstance().captureEvent("", error, TAG + "; onActivityResult(); CROP_IMAGE_ACTIVITY_REQUEST_CODE");
      }
    }
    super.onActivityResult(requestCode, resultCode, data);
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
      if (cropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        // required permissions granted, start crop image activity
        startCropImageActivity(cropImageUri);
      } else {
        showSnackBar(R.string.grant_permission);
      }
    }
  }

  @OnClick(R.id.change_image)
  public void changeImage() {
    CropImage.startPickImageActivity(this);
  }

  @OnClick(R.id.continue_create)
  public void continueToCreate() {
    String name = smallcaseName.getText().toString();
    String description = smallcaseDescription.getText().toString();

    if (!createInfoHelper.isInputValid(name, description)) {
      AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_LONG, R.string.give_name_and_description);
      return;
    }

    progressDialog = ProgressDialog.show(this, "", "Saving...");
    progressDialog.show();

    setNewData(name, description);
  }

  @Override
  public void showSnackBar(@StringRes int resId) {
    Snackbar.make(parent, resId, Snackbar.LENGTH_LONG).show();
  }

  @Override
  public void hideLoader() {
    if (progressDialog != null) progressDialog.dismiss();
  }

  @Override
  public void onImageUploadedSuccessfully() {
    isImageUploaded = true;
    if (isInfoUpdated) {
      finishedSavingSmallcase();
    }
  }

  private void finishedSavingSmallcase() {
    hideLoader();
    Intent intent = new Intent(this, DraftSmallcasesActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);

    LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(AppConstants.ON_SMALLCASE_SAVED));

    finish();
  }

  @Override
  public void onFailedUploadingImage() {
    hideLoader();
    showSnackBar(R.string.something_wrong);
  }

  @Override
  public void onFailedUpdatingDraft() {
    hideLoader();
    showSnackBar(R.string.something_wrong);
  }

  @Override
  public void onDraftUpdatedSuccessfully() {
    isInfoUpdated = true;
    if (isImageUploaded) {
      finishedSavingSmallcase();
    }
  }

  private void toggleSelectionColor(View view, boolean hasFocus) {
    if (hasFocus) {
      view.setBackgroundColor(ContextCompat.getColor(CreateInfoActivity.this, R.color.blue_800));
    } else {
      view.setBackgroundColor(ContextCompat.getColor(CreateInfoActivity.this, R.color.grey_300));
    }
  }

  private void startCropImageActivity(Uri imageUri) {
    CropImage.activity(imageUri)
        .setAllowRotation(false)
        .setAllowFlipping(false)
        .setAllowCounterRotation(false)
        .setAspectRatio(1, 1)
        .setActivityTitle("Crop Image")
        .start(this);
  }

  private void setNewData(String name, String description) {
    Info info = new Info();
    info.setName(name);
    info.setShortDescription(description);
    info.setTier(draftSmallcase == null ? SmallcaseTier.BASIC : draftSmallcase.getInfo().getTier());
    draftSmallcase.setInfo(info);
    draftSmallcase.setDid(draftSmallcase.getDid() == null ? draftSmallcase.get_id() : draftSmallcase.getDid());

    if (SmallcaseSource.CREATED.equals(draftSmallcase.getSource()) && cropImageUri != null) {
      createInfoHelper.uploadImage(cropImageUri, draftSmallcase.getScid());
    } else {
      isImageUploaded = true;
    }
    createInfoHelper.updateInfo(draftSmallcase);
  }
}
