package com.smallcase.android.create;

import android.util.Log;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.sentry.event.Breadcrumb;
import retrofit2.adapter.rxjava.HttpException;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 27/09/17.
 */

class CreateSmallcaseService implements CreateSmallcaseContract.Service {
    private static final String TAG = "CreateSmallcaseService";

    private MarketRepository marketRepository;
    private SharedPrefService sharedPrefService;
    private UserSmallcaseRepository userSmallcaseRepository;
    private CreateSmallcaseContract.Presenter presenter;
    private CompositeSubscription compositeSubscription;
    private SmallcaseRepository smallcaseRepository;

    CreateSmallcaseService(UserSmallcaseRepository userSmallcaseRepository, MarketRepository marketRepository,
           SharedPrefService sharedPrefService, CreateSmallcaseContract.Presenter presenter, SmallcaseRepository smallcaseRepository) {
        this.userSmallcaseRepository = userSmallcaseRepository;
        this.marketRepository = marketRepository;
        this.sharedPrefService = sharedPrefService;
        this.presenter = presenter;
        this.smallcaseRepository = smallcaseRepository;
        this.compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getStocksInfo(final List<String> sids) {
        compositeSubscription.add(marketRepository.getStocksInfo(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), sids)
                .subscribe(responseBody -> presenter.onStocksInfoReceived(responseBody), throwable -> {
                    Log.d(TAG, "call: " + throwable.toString());
                    presenter.onFailedFetchingStockInfo();

                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    HashMap<String, String> data = new HashMap<>();
                    for (String sid : sids) {
                        data.put("stock", sid);
                    }
                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, TAG + "; getStocksInfo()", data);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getStocksInfo()");
                }));
    }

    @Override
    public void getStockHistorical(final List<String> sids) {
        compositeSubscription.add(marketRepository.getStockHistorical(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), sids, "1y")
                .subscribe(responseBody -> {
                    try {
                        presenter.onStockHistoricalReceived(responseBody);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; getStocksInfo()");
                    }
                }, throwable -> {
                    presenter.onFailedFetchingStockHistorical();

                    HashMap<String, String> data = new HashMap<>();
                    for (String sid : sids) {
                        data.put("stock", sid);
                    }
                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, TAG + "; getStockHistorical()", data);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getStocksInfo()");
                }));
    }

    @Override
    public void getSmallcaseHistorical(String scid, String benchmarkType, String benchmarkId, String duration) {
        final Map<String, String> data = new HashMap<>();
        data.put("scid", scid);
        data.put("benchmarkType", benchmarkType);
        data.put("benchmarkId", benchmarkId);
        data.put("duration", duration);
        compositeSubscription.add(smallcaseRepository.getSmallcaseHistorical(sharedPrefService.getAuthorizationToken(), scid, benchmarkType,
                benchmarkId, duration, sharedPrefService.getCsrfToken())
                .subscribe(historical -> presenter.onSmallcaseHistoricalReceived(historical), throwable -> {
                    Log.d(TAG, "call: ");
                    presenter.onFailedFetchingSmallcaseHistorical();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "State", data);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getSmallcaseHistorical()");
                }));
    }

    @Override
    public void saveSmallcaseAsDraft(final DraftSmallcase draftSmallcase) {
        compositeSubscription.add(userSmallcaseRepository.saveDraft(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), draftSmallcase)
                .subscribe(responseBody -> presenter.onDraftSavedSuccessfully(responseBody), throwable -> {
                    presenter.onFailedSavingDraft();
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable,
                            TAG + "; saveSmallcaseAsDraft(); smallcase - " + draftSmallcase.toString());
                }));
    }

    @Override
    public void getDraft(final String did) {
        compositeSubscription.add(userSmallcaseRepository.getDraft(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), did)
                .subscribe(draftSmallcase -> presenter.onDraftFetchedSuccessfully(draftSmallcase), throwable -> {
                    presenter.onFailedFetchingDraft();
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable,
                            TAG + "; getDraft(); did - " + did);
                }));
    }

    @Override
    public void destroySubscriptions() {
        if (compositeSubscription != null) compositeSubscription.unsubscribe();
    }
}
