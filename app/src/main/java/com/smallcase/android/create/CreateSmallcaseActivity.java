package com.smallcase.android.create;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.stock.SearchStockAdapter;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.stock.StockInfo;
import com.smallcase.android.user.coustomize.DraftSmallcasesActivity;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.SmallcaseCompositionScheme;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.util.WeighingScheme;
import com.smallcase.android.view.ItemTouchHelperAdapter;
import com.smallcase.android.view.android.CustomTabLayout;
import com.smallcase.android.view.android.CustomTypefaceSpan;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.android.SimpleItemTouchHelperCallback;
import com.smallcase.android.view.model.PPoints;
import com.smallcase.android.view.model.PSegment;
import com.smallcase.android.view.model.Stock;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateSmallcaseActivity extends AppCompatActivity implements CreateSmallcaseContract.View,
    SearchStockAdapter.StockSelectedListener {
  public static final String DRAFT_SMALLCASE = "draft_smallcase";
  public static final String SMALLCASE_NAME = "smallcase_name";
  public static final String SMALLCASE_DESCRIPTION = "smallcase_description";
  public static final String SMALLCASE_SCID = "smallcase_scid";

  private final static int DELAY_BEFORE_SEARCH = 800;
  private static final String TAG = "CreateSmallcaseActivity";
  private static final AccelerateDecelerateInterpolator ACCELERATE_DECELERATE_INTERPOLATOR = new AccelerateDecelerateInterpolator();

  @BindView(R.id.create_smallcase_image)
  ImageView createSmallcaseImage;
  @BindView(R.id.create_smallcase_title)
  View createSmallcaseTitle;
  @BindView(R.id.add_stock_empty)
  View addStockInEmptyState;
  @BindView(R.id.smallcases_private_message)
  View smallcasePrivateMessage;
  @BindView(R.id.add_stock_card)
  View addStockCard;
  @BindView(R.id.stocks_title_card)
  CardView stockTileCard;
  @BindView(R.id.stocks_list_divider)
  View stocksListDivider;
  @BindView(R.id.stocks_list)
  RecyclerView stocksList;
  @BindView(R.id.additional_blur)
  View additionalBlur;
  @BindView(R.id.search_stock_layout)
  View searchStockLayout;
  @BindView(R.id.search_stock)
  EditText searchStock;
  @BindView(R.id.tab_layout)
  CustomTabLayout tabLayout;
  @BindView(R.id.suggested_stocks_list)
  RecyclerView suggestedStockList;
  @BindView(R.id.no_similar_stocks)
  GraphikText noSimilarStocks;
  @BindView(R.id.parent)
  View parent;
  @BindView(R.id.stocks_count)
  GraphikText stocksCount;
  @BindView(R.id.weighing_scheme)
  GraphikText weighingScheme;
  @BindView(R.id.shares_or_weight)
  GraphikText sharesOrWeight;
  @BindView(R.id.stock_list_container)
  View stockListContainer;
  @BindView(R.id.performance_graph)
  ImageView performanceGraph;
  @BindView(R.id.add_segment)
  ImageView addSegment;
  @BindView(R.id.add_segment_title)
  GraphikText addSegmentTitle;
  @BindView(R.id.add_segment_button)
  GraphikText addSegmentButton;
  @BindView(R.id.enter_segment_name)
  EditText enterSegmentName;
  @BindView(R.id.add_segment_layout)
  View addSegmentLayout;
  @BindView(R.id.weight_shares_card)
  CardView weightSharesCard;
  @BindView(R.id.card_title)
  GraphikText cardTitle;
  @BindView(R.id.selected_stock_name)
  GraphikText selectedStockName;
  @BindView(R.id.weight_or_shares)
  EditText weightOrShares;
  @BindView(R.id.dialog_confirmation)
  View dialogConfirmation;
  @BindView(R.id.dialog_description)
  GraphikText dialogDescription;
  @BindView(R.id.dialog_primary_button)
  GraphikText dialogPrimaryButton;
  @BindView(R.id.dialog_secondary_button)
  GraphikText dialogSecondaryButton;
  @BindView(R.id.primary_action_container)
  View primaryActionContainer;
  @BindView(R.id.minimum_investment_amount)
  GraphikText minimumInvestmentAmount;
  @BindView(R.id.performance_card)
  CardView performanceCard;
  @BindView(R.id.smallcase_index)
  GraphikText smallcaseIndex;
  @BindView(R.id.nifty_index)
  GraphikText niftyIndex;
  @BindView(R.id.start_date)
  GraphikText startDate;
  @BindView(R.id.end_date)
  GraphikText endDate;
  @BindView(R.id.performance_chart)
  LineChart performanceChart;
  @BindView(R.id.save_smallcase)
  GraphikText saveSmallcase;
  @BindView(R.id.loading_indicator)
  View loadingIndicator;
  @BindView(R.id.original_smallcase_index)
  GraphikText originalSmallcaseIndex;
  @BindView(R.id.original_smallcase_layout)
  View originalSmallcaseLayout;
  @BindView(R.id.original_vs)
  View originalVs;
  @BindView(R.id.secondary_start_date)
  GraphikText secondaryStartDate;
  @BindView(R.id.secondary_end_date)
  GraphikText secondaryEndDate;
  @BindView(R.id.popup_container)
  View popupContainer;
  @BindView(R.id.text_tool_bar_title)
  GraphikText toolBarTitle;
  @BindView(R.id.search_progress)
  ProgressBar searchProgress;
  @BindView(R.id.clear_search_text)
  View clearSearchText;
  @BindView(R.id.weighing_arrow)
  View weighingArrow;

  CreateSmallcaseContract.Presenter presenter;
  StockInfo stockInfo;
  String selectedWeighingScheme = WeighingScheme.EQUI_WEIGHTED;
  Stock editingStock;

  private Handler messageHandler = new Handler();
  private Animations animations;
  private SearchStockAdapter searchStockAdapter;
  private StockSegmentAdapter stockSegmentAdapter;
  private BottomSheetBehavior sharesOrWeightSheet;
  private boolean isShares = false, isPerformanceChartShowing = false;
  private List<String> dates;
  private List<Entry> smallcaseEntries, niftyEntries, originalSmallcaseEntries;
  private Snackbar snackbar;
  private ProgressDialog progressDialog;
  private AnalyticsContract analyticsContract;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_create_smallcase);
    ButterKnife.bind(this);

    List<Object> stocks = new ArrayList<>();
    String scid = getIntent().getStringExtra(SMALLCASE_SCID);

    DraftSmallcase draftSmallcase = (DraftSmallcase) getIntent().getSerializableExtra(DRAFT_SMALLCASE);

    toolBarTitle.setText(draftSmallcase == null ? "Create smallcase" : "Customize smallcase");

    StockHelper stockHelper = new StockHelper();
    MarketRepository marketRepository = new MarketRepository();
    SharedPrefService sharedPrefService = new SharedPrefService(this);
    searchStockAdapter = new SearchStockAdapter(this, true, null, this);
    presenter = new CreateSmallcasePresenter(stocks, stockHelper, sharedPrefService, marketRepository,
        this, new SmallcaseHelper(), new UserSmallcaseRepository(), getIntent().getStringExtra(SMALLCASE_NAME),
        getIntent().getStringExtra(SMALLCASE_DESCRIPTION), scid, draftSmallcase == null ? SmallcaseSource.CREATED
        : draftSmallcase.getSource(), new SmallcaseRepository(), draftSmallcase == null ? null : draftSmallcase.get_id());
    animations = new Animations();
    stockInfo = new StockInfo(this, stockHelper, marketRepository, sharedPrefService);
    analyticsContract = new AnalyticsManager(this);

    suggestedStockList.setLayoutManager(new LinearLayoutManager(this));
    suggestedStockList.setHasFixedSize(true);
    suggestedStockList.setAdapter(searchStockAdapter);
    weightOrShares.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR));

    stocksList.setLayoutManager(new LinearLayoutManager(this));

    sharesOrWeightSheet = BottomSheetBehavior.from(weightSharesCard);
    sharesOrWeightSheet.setHideable(true);
    sharesOrWeightSheet.setState(BottomSheetBehavior.STATE_HIDDEN);

    addStockSearchListener();

    tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
      @Override
      public void onTabSelected(TabLayout.Tab tab) {
        CharSequence sectorName = tab.getText();
        searchStockAdapter.setSuggestion(true);
        searchStockAdapter.setStockList(presenter.getSectorSuggestions(sectorName));
      }

      @Override
      public void onTabUnselected(TabLayout.Tab tab) {

      }

      @Override
      public void onTabReselected(TabLayout.Tab tab) {

      }
    });

    GradientDrawable gradientDrawable = new GradientDrawable();
    gradientDrawable.setShape(GradientDrawable.RECTANGLE);
    gradientDrawable.setCornerRadius(AppUtils.getInstance().dpToPx(3));
    gradientDrawable.setColor(ContextCompat.getColor(this, R.color.blue_800));
    saveSmallcase.setBackground(gradientDrawable);

    Glide.with(this)
        .load(R.drawable.smallcase_create)
        .into(createSmallcaseImage);
    performanceGraph.setImageResource(R.drawable.stats_white);
    Glide.with(this)
        .load(R.drawable.add_segment_white)
        .into(addSegment);

    stocksCount.setText("Stocks (0/20)");
    weighingScheme.setText(selectedWeighingScheme);
    sharesOrWeight.setText("Weightage");

    setUpChart();

    performanceCard.animate()
        .y(-1500f)
        .setDuration(50)
        .setListener(new AnimatorListenerAdapter() {
          @Override
          public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            performanceCard.setVisibility(View.VISIBLE);
          }
        })
        .start();

    if (draftSmallcase != null) {
      if (SmallcaseCompositionScheme.SHARES.equals(draftSmallcase.getCompositionScheme())) {
        isShares = true;
        sharesOrWeight.setText("Shares");
      } else {
        isShares = false;
        sharesOrWeight.setText("Weightage");
      }

      showStockAddedLayouts();

      addStockCard.setVisibility(View.GONE);
      stockTileCard.setVisibility(View.GONE);
      stockListContainer.setVisibility(View.GONE);
      loadingIndicator.setVisibility(View.VISIBLE);

      selectedWeighingScheme = WeighingScheme.CUSTOM;
      weighingScheme.setText(selectedWeighingScheme);
      presenter.loadDraft(draftSmallcase);
    }

    if (draftSmallcase != null && SmallcaseSource.CUSTOM.equals(draftSmallcase.getSource())) {
      startDate.setVisibility(View.GONE);
      endDate.setVisibility(View.GONE);

      originalSmallcaseLayout.setVisibility(View.VISIBLE);
      originalVs.setVisibility(View.VISIBLE);
      secondaryStartDate.setVisibility(View.VISIBLE);
      secondaryEndDate.setVisibility(View.VISIBLE);
    }

    performanceChart.setNoDataText("Add stocks and assign weights to compare with Nifty");
    performanceChart.setNoDataTextColor(ContextCompat.getColor(this, R.color.secondary_text));

    LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
        new IntentFilter(AppConstants.ON_SMALLCASE_SAVED));
  }

  private void addStockSearchListener() {
    searchStock.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        // Remove previous search if it's in waiting state
        messageHandler.removeCallbacks(searchRunnable);

        int searchLength = s.toString().trim().length();
        if (searchLength == 0) {
          clearSearchText.setVisibility(View.INVISIBLE);
          // Bring back suggestions when no text is entered
          TabLayout.Tab tab = tabLayout.getTabAt(0);
          if (tab != null) {
            tabLayout.setVisibility(View.VISIBLE);
            tab.select();
            searchStockAdapter.setSuggestion(true);
            searchStockAdapter.setStockList(presenter.getSectorSuggestions(tab.getText()));
          } else {
            toggleTabLayout(View.GONE);
          }
        }

        // Search for stock if entered text is greater than 1
        if (searchLength > 1) {
          clearSearchText.setVisibility(View.VISIBLE);
          searchProgress.setVisibility(View.VISIBLE);
          messageHandler.postDelayed(searchRunnable, DELAY_BEFORE_SEARCH);
        }
      }
    });
  }

  private void setUpChart() {
    performanceChart.getXAxis().setDrawLabels(false);
    performanceChart.getAxisRight().setEnabled(false);
    performanceChart.getAxisLeft().setEnabled(false);
    performanceChart.getDescription().setEnabled(false);
    performanceChart.setDrawGridBackground(false);
    performanceChart.getAxisRight().setDrawLabels(false);
    performanceChart.getAxisLeft().setDrawLabels(false);
    performanceChart.getXAxis().setDrawAxisLine(false);
    performanceChart.getLegend().setEnabled(false);
    performanceChart.setDrawMarkers(false);
    performanceChart.setTouchEnabled(true);
    performanceChart.setPinchZoom(false);
    performanceChart.setScaleEnabled(false);
    performanceChart.setDoubleTapToZoomEnabled(false);
    performanceChart.getXAxis().setDrawGridLines(false);
  }

  private void showTapTargets() {
    new TapTargetSequence(this)
        .targets(
            TapTarget.forView(weighingScheme, "Choose Weighting Scheme", "Choose between equi-weighted, " +
                "market cap-weighted or Custom-weighted schemes")
                .textTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR))
                .descriptionTextColor(R.color.white)
                .descriptionTextSize(14)
                .cancelable(true)
                .drawShadow(true)
                .outerCircleColor(R.color.blue_800)
                .outerCircleAlpha(0.9f)
                .targetCircleColor(R.color.white)
                .transparentTarget(false)
                .targetRadius(24),
            TapTarget.forView(performanceGraph, "Past Performance", "Analyse your stocks’ past 1 year " +
                "performance and compare it with Nifty and the original smallcase (if you customize)")
                .textTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR))
                .descriptionTextColor(R.color.white)
                .descriptionTextSize(14)
                .cancelable(true)
                .drawShadow(true)
                .outerCircleColor(R.color.blue_800)
                .outerCircleAlpha(0.9f)
                .targetCircleColor(R.color.white)
                .transparentTarget(false)
                .targetRadius(24),
            TapTarget.forView(addSegment, "Add New Segment", "Tap this icon and add a new segment to organize " +
                "stocks better in your smallcase")
                .textTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR))
                .descriptionTextColor(R.color.white)
                .descriptionTextSize(14)
                .cancelable(true)
                .drawShadow(true)
                .outerCircleColor(R.color.blue_800)
                .outerCircleAlpha(0.9f)
                .targetCircleColor(R.color.white)
                .transparentTarget(false)
                .targetRadius(24)
        ).considerOuterCircleCanceled(false).continueOnCancel(true).start();
    presenter.setSeenTutorial();
  }

  @OnClick(R.id.add_stock_empty)
  public void addStock() {
    analyticsContract.sendEvent(null, "Clicked Add Stock", Analytics.MIXPANEL);

    additionalBlur.setVisibility(View.VISIBLE);
    animations.scaleIntoPlaceAnimation(searchStockLayout);
    additionalBlur.setOnClickListener(v -> hideSearchStock());

    if (presenter.getStockAndSegmentList() == null || presenter.getStockAndSegmentList().isEmpty() || presenter.getStocksSize() == 0) {
      noSimilarStocks.setVisibility(View.VISIBLE);
      noSimilarStocks.setText("Add stocks to see similar stocks");
    } else {
      noSimilarStocks.setVisibility(View.GONE);
    }
  }

  @OnClick(R.id.back)
  public void back() {
    onBackPressed();
  }

  @Override
  public void onBackPressed() {
    if (stockInfo.isBottomSheetOpen()) {
      stockInfo.setBottomSheetState(BottomSheetBehavior.STATE_HIDDEN);
      return;
    }

    if (sharesOrWeightSheet.getState() == BottomSheetBehavior.STATE_EXPANDED) {
      sharesOrWeightSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
      return;
    }

    if (addSegmentLayout.getVisibility() == View.VISIBLE) {
      animations.scaleOutAnimation(addSegmentLayout, additionalBlur);
      enterSegmentName.setText("");
      return;
    }

    if (searchStockLayout.getVisibility() == View.VISIBLE) {
      hideSearchStock();
      return;
    }

    if (isPerformanceChartShowing) {
      hidePerformanceChart();
      return;
    }

    animations.scaleIntoPlaceAnimation(popupContainer);
    additionalBlur.setVisibility(View.VISIBLE);
    additionalBlur.setOnClickListener(v -> stayOnScreen());
  }

  @Override
  protected void onDestroy() {
    if (searchStockAdapter != null) searchStockAdapter.destroyContext();

    if (stockInfo != null) stockInfo.unBind();

    presenter.destroySubscriptions();
    LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
    super.onDestroy();
  }

  @OnClick(R.id.positive_button)
  public void leaveScreen() {
    finish();
  }

  @OnClick(R.id.negative_button)
  public void stayOnScreen() {
    animations.scaleOutAnimation(popupContainer, additionalBlur);
  }

  @OnClick(R.id.performance_graph)
  public void showPerformanceGraph() {
    performanceCard.setVisibility(View.VISIBLE);
    additionalBlur.setVisibility(View.VISIBLE);
    additionalBlur.setOnClickListener(v -> hidePerformanceChart());

    isPerformanceChartShowing = true;
    performanceCard.animate()
        .y(0f)
        .setInterpolator(ACCELERATE_DECELERATE_INTERPOLATOR)
        .setDuration(350)
        .start();
  }

  private void hidePerformanceChart() {
    isPerformanceChartShowing = false;
    performanceCard.animate()
        .y(-1500f)
        .setInterpolator(ACCELERATE_DECELERATE_INTERPOLATOR)
        .setDuration(350)
        .start();
    additionalBlur.setVisibility(View.GONE);
  }

  @OnClick({R.id.shares_or_weight, R.id.shares_arrow})
  public void selectSharesOrWeight() {
    PopupMenu popupMenu = new PopupMenu(this, sharesOrWeight, Gravity.NO_GRAVITY, R.attr.actionOverflowMenuStyle, 0);
    popupMenu.inflate(R.menu.notification_menu);

    MenuItem sharesOrWeightage = popupMenu.getMenu().findItem(R.id.mark_all_as_read);

    SpannableString sharesOrWeightageTitle = new SpannableString(isShares ? "Weightage" : "Shares");
    sharesOrWeightageTitle.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.primary_text)), 0,
        sharesOrWeightageTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    sharesOrWeightageTitle.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR)),
        0, sharesOrWeightageTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    sharesOrWeightageTitle.setSpan(new RelativeSizeSpan(0.9f), 0, sharesOrWeightageTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    sharesOrWeightage.setTitle(sharesOrWeightageTitle);

    popupMenu.setOnMenuItemClickListener(item -> {
      switch (item.getItemId()) {
        case R.id.mark_all_as_read:
          isShares = !isShares;
          sharesOrWeight.setText(isShares ? "Shares" : "Weightage");
          break;
      }
      stockSegmentAdapter.notifyDataSetChanged();
      return false;
    });
    popupMenu.show();
  }

  private Runnable searchRunnable = new Runnable() {
    public void run() {
      presenter.getStocksForSearch();
    }
  };

  @Override
  public String getSearchString() {
    return searchStock.getText().toString().trim();
  }

  @Override
  public void showSnackBar(@StringRes int resId) {
    if (snackbar == null || !snackbar.isShown()) {
      snackbar = Snackbar.make(parent, resId, Snackbar.LENGTH_LONG);
      snackbar.show();
    }
  }

  @Override
  public void showSnackBar(String message) {
    if (snackbar == null || !snackbar.isShown()) {
      snackbar = Snackbar.make(parent, message, Snackbar.LENGTH_LONG);
      snackbar.show();
    }
  }

  private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      if (AppConstants.ON_SMALLCASE_SAVED.equals(intent.getAction())) {
        if (!isFinishing()) finish();
      }
    }
  };

  @Override
  public void addSimilarTab(String sector) {
    if (tabLayout.getVisibility() != View.VISIBLE) tabLayout.setVisibility(View.VISIBLE);
    tabLayout.addTab(tabLayout.newTab().setText(sector));
  }

  @Override
  public void sendUserToDraftSmallcases() {
    Intent intent = new Intent(this, DraftSmallcasesActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    startActivity(intent);
    finish();
  }

  @Override
  public void hideLoading() {
    if (progressDialog != null) progressDialog.dismiss();
  }

  @Override
  public void showLoader() {
    progressDialog = ProgressDialog.show(this, "", "Saving...");
    progressDialog.show();
  }

  @OnClick(R.id.clear_search_text)
  public void clearSearchText() {
    searchStock.setText("");
  }

  @Override
  public void showStocksLayout() {
    loadingIndicator.setVisibility(View.GONE);
    if (stockSegmentAdapter == null) {
      stockSegmentAdapter = new StockSegmentAdapter();
      stocksList.setAdapter(stockSegmentAdapter);
      ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(this, stockSegmentAdapter,
          stockSegmentAdapter.TYPE_SEGMENT);
      ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
      touchHelper.attachToRecyclerView(stocksList);
    }
    showStockAddedLayouts();
    updateStockCount();
  }

  @Override
  public void invalidateGraph() {
    performanceChart.invalidate();
  }

  @Override
  public void removeSimilarTab(String sector) {
    int tabPosition = -1;
    for (int i = 0; i < tabLayout.getTabCount(); i++) {
      if (tabLayout.getTabAt(i) != null && sector.equals(tabLayout.getTabAt(i).getText()))
        tabPosition = i;
    }

    if (tabPosition != -1) {
      tabLayout.removeTabAt(tabPosition);
      searchStockAdapter.clearList();
    }
  }

  @Override
  public void enableWeightSelection() {
    weighingScheme.setAlpha(1f);
    weighingArrow.setAlpha(1f);
  }

  @Override
  public void sendUserToSaveInfo(DraftSmallcase draftSmallcase) {
    Intent intent = new Intent(this, CreateInfoActivity.class);
    intent.putExtra(CreateInfoActivity.DRAFT_SMALLCASE, draftSmallcase);
    startActivity(intent);
  }

  @Override
  public void hideSearchProgress() {
    searchProgress.setVisibility(View.GONE);
  }

  @OnClick({R.id.weighing_scheme, R.id.weighing_arrow})
  public void onWeighingSchemeClicked() {
    if (presenter.getStocksSize() == 0) return;

    PopupMenu popupMenu = new PopupMenu(this, weighingScheme, Gravity.NO_GRAVITY, R.attr.actionOverflowMenuStyle, 0);
    popupMenu.inflate(R.menu.weighing_menu);

    MenuItem equiWeighted = popupMenu.getMenu().findItem(R.id.equi_weighted);
    MenuItem marketCap = popupMenu.getMenu().findItem(R.id.market_cap);
    MenuItem custom = popupMenu.getMenu().findItem(R.id.custom);


    SpannableString equiWeightedTitle = new SpannableString(equiWeighted.getTitle());
    equiWeightedTitle.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, WeighingScheme.EQUI_WEIGHTED
            .equals(selectedWeighingScheme) ? R.color.blue_800 : R.color.primary_text)), 0, equiWeightedTitle.length(),
        Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    equiWeightedTitle.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR)),
        0, equiWeightedTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    equiWeightedTitle.setSpan(new RelativeSizeSpan(0.9f), 0, equiWeightedTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    equiWeighted.setTitle(equiWeightedTitle);

    SpannableString marketCapTitle = new SpannableString(marketCap.getTitle());
    marketCapTitle.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, WeighingScheme.MARKET_CAPPED
            .equals(selectedWeighingScheme) ? R.color.blue_800 : R.color.primary_text)), 0, marketCapTitle.length(),
        Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    marketCapTitle.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR)),
        0, marketCapTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    marketCapTitle.setSpan(new RelativeSizeSpan(0.9f), 0, marketCapTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    marketCap.setTitle(marketCapTitle);

    SpannableString customTitle = new SpannableString(custom.getTitle());
    customTitle.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, WeighingScheme.CUSTOM
            .equals(selectedWeighingScheme) ? R.color.blue_800 : R.color.primary_text)), 0, customTitle.length(),
        Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    customTitle.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR)),
        0, customTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    customTitle.setSpan(new RelativeSizeSpan(0.9f), 0, customTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    custom.setTitle(customTitle);

    popupMenu.setOnMenuItemClickListener(item -> {
      switch (item.getItemId()) {
        case R.id.equi_weighted:
          presenter.clearLockedWeight();
          selectedWeighingScheme = WeighingScheme.EQUI_WEIGHTED;
          break;

        case R.id.market_cap:
          presenter.clearLockedWeight();
          selectedWeighingScheme = WeighingScheme.MARKET_CAPPED;
          break;

        case R.id.custom:
          selectedWeighingScheme = WeighingScheme.CUSTOM;
          break;
      }

      stockSegmentAdapter.onFinishedInteraction(null, 0, 0);
      return false;
    });
    popupMenu.show();
  }

  @Override
  public void toggleTabLayout(int visibility) {
    tabLayout.setVisibility(visibility);
  }

  @Override
  public void showSearchStocks(List<Stock> searchResults) {
    if (noSimilarStocks.getVisibility() == View.VISIBLE) {
      noSimilarStocks.setVisibility(View.GONE);
    }

    searchProgress.setVisibility(View.GONE);
    searchStockAdapter.setSuggestion(false);
    searchStockAdapter.setStockList(searchResults);
  }

  @Override
  public void showNoStocks() {
    searchProgress.setVisibility(View.GONE);
    searchStockAdapter.clearList();
    noSimilarStocks.setVisibility(View.VISIBLE);
    noSimilarStocks.setText("No stocks found");

    smallcaseIndex.setText("");
    niftyIndex.setText("");
    startDate.setText("");
    endDate.setText("");

    performanceChart.setNoDataText("Add stocks and assign weights to compare with Nifty");
    performanceChart.setNoDataTextColor(ContextCompat.getColor(this, R.color.secondary_text));
    performanceChart.clear();
    performanceChart.invalidate();

    selectedWeighingScheme = WeighingScheme.EQUI_WEIGHTED;
    weighingScheme.setText(selectedWeighingScheme);

    weighingScheme.setAlpha(0.5f);
    weighingArrow.setAlpha(0.5f);
  }

  @Override
  public String getWeighingScheme() {
    return selectedWeighingScheme;
  }

  @Override
  public void updateMinimumAmount(double minAmount) {
    primaryActionContainer.setVisibility(View.VISIBLE);

    GradientDrawable gradientDrawable = (GradientDrawable) saveSmallcase.getBackground();
    if (presenter.getStocksSize() < 2) {
      gradientDrawable.setColor(ContextCompat.getColor(this, R.color.blue_grey_100));
    } else {
      gradientDrawable.setColor(ContextCompat.getColor(this, R.color.blue_800));
    }

    SpannableString spannableString = new SpannableString("Minimum amount \n" + AppConstants
        .RUPEE_SYMBOL + AppConstants.getDecimalNumberFormatter().format(minAmount));
    spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.primary_text)), 14,
        spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    spannableString.setSpan(new RelativeSizeSpan(1.25f), 14,
        spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    minimumInvestmentAmount.setText(spannableString);
    if (stockSegmentAdapter != null && !stocksList.isComputingLayout())
      stockSegmentAdapter.notifyDataSetChanged();

    if (presenter.shouldShowTutorial() && presenter.getStocksSize() == 2) {
      showTapTargets();
    }
  }

  @OnClick(R.id.save_smallcase)
  public void saveSmallcase() {
    if (presenter.getStockAndSegmentList() == null || presenter.getStocksSize() < 2) {
      showSnackBar(R.string.need_two_stocks);
      return;
    }

    Map<String, Object> eventProperties = new HashMap<>();
    eventProperties.put("draftName", presenter.getSmallcaseName() == null ? "NA" : presenter.getSmallcaseName());
    eventProperties.put("lastSaved", presenter.getLastSaved() == null ? "NA" : presenter.getLastSaved());
    eventProperties.put("smallcaseTier", presenter.getSmallcaseTier());
    analyticsContract.sendEvent(eventProperties, "Clicked Save For Later", Analytics.MIXPANEL, Analytics.CLEVERTAP);

    presenter.saveSmallcase();
  }

  @Override
  public void updateHistoricalChart(List<PPoints> smallcasePoints, final boolean shouldShowOriginalSmallcase) {
    smallcaseEntries = new ArrayList<>();
    niftyEntries = new ArrayList<>();
    originalSmallcaseEntries = new ArrayList<>();
    for (int i = 0; i < smallcasePoints.size(); i++) {
      smallcaseEntries.add(new Entry(i, (float) AppUtils.getInstance().round(smallcasePoints.get(i).getSmallcaseIndex(), 2)));
      niftyEntries.add(new Entry(i, (float) AppUtils.getInstance().round(smallcasePoints.get(i).getNiftyIndex(), 2)));
      if (shouldShowOriginalSmallcase) {
        originalSmallcaseEntries.add(new Entry(i, (float) AppUtils.getInstance().round(smallcasePoints.get(i)
            .getOriginalSmallcaseIndex(), 2)));
      }
    }

    final LineDataSet smallcaseDataSet = new LineDataSet(smallcaseEntries, "");
    smallcaseDataSet.setDrawValues(false);
    smallcaseDataSet.setDrawCircleHole(false);
    smallcaseDataSet.setDrawCircles(false);
    smallcaseDataSet.setDrawFilled(false);
    smallcaseDataSet.setColor(ContextCompat.getColor(this, R.color.blue_800));
    smallcaseDataSet.setLineWidth(2f);
    smallcaseDataSet.setHighlightLineWidth(1f);
    smallcaseDataSet.setDrawHorizontalHighlightIndicator(false);
    smallcaseDataSet.setHighLightColor(ContextCompat.getColor(this, R.color.grey_300));

    LineDataSet niftyDataSet = new LineDataSet(niftyEntries, "");
    niftyDataSet.setDrawValues(false);
    niftyDataSet.setDrawCircleHole(false);
    niftyDataSet.setDrawCircles(false);
    niftyDataSet.setDrawHorizontalHighlightIndicator(false);
    niftyDataSet.setDrawFilled(false);
    niftyDataSet.setColor(ContextCompat.getColor(this, R.color.red_600));
    niftyDataSet.setLineWidth(2f);
    niftyDataSet.setHighlightLineWidth(1f);
    niftyDataSet.setHighLightColor(ContextCompat.getColor(this, R.color.grey_300));

    List<ILineDataSet> lineDataSets = new ArrayList<>();
    lineDataSets.add(smallcaseDataSet);
    lineDataSets.add(niftyDataSet);

    if (shouldShowOriginalSmallcase) {
      LineDataSet originalSmallcaseSet = new LineDataSet(originalSmallcaseEntries, "");
      originalSmallcaseSet.setDrawValues(false);
      originalSmallcaseSet.setDrawCircleHole(false);
      originalSmallcaseSet.setDrawCircles(false);
      originalSmallcaseSet.setDrawHorizontalHighlightIndicator(false);
      originalSmallcaseSet.setDrawFilled(false);
      originalSmallcaseSet.setColor(ContextCompat.getColor(this, R.color.grey_300));
      originalSmallcaseSet.setLineWidth(2f);
      originalSmallcaseSet.setHighlightLineWidth(1f);
      originalSmallcaseSet.setHighLightColor(ContextCompat.getColor(this, R.color.grey_300));
      lineDataSets.add(originalSmallcaseSet);
    }

    LineData lineData = new LineData(lineDataSets);

    dates = new ArrayList<>();
    for (PPoints smallcasePoint : smallcasePoints) {
      dates.add(smallcasePoint.getDate());
    }

    if (shouldShowOriginalSmallcase) {
      secondaryStartDate.setText(dates.get(0) + " -");
    } else {
      startDate.setText(dates.get(0) + " -");
    }

    performanceChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
      @Override
      public void onValueSelected(Entry e, Highlight h) {
        int pos = (int) e.getX();
        Entry niftyEntry = niftyEntries.get(pos);
        Entry smallcaseEntry = smallcaseEntries.get(pos);
        smallcaseIndex.setText(String.format(Locale.getDefault(), "%.2f", smallcaseEntry.getY() - 100) + "%");
        niftyIndex.setText(String.format(Locale.getDefault(), "%.2f", niftyEntry.getY() - 100) + "%");
        if (shouldShowOriginalSmallcase) {
          secondaryEndDate.setText(dates.get(pos));
          Entry originalSmallcaseEntry = originalSmallcaseEntries.get(pos);
          originalSmallcaseIndex.setText(String.format(Locale.getDefault(), "%.2f", originalSmallcaseEntry.getY() - 100) + "%");
        } else {
          endDate.setText(dates.get(pos));
        }
      }

      @Override
      public void onNothingSelected() {

      }
    });

    performanceChart.setData(lineData);
    performanceChart.highlightValue(smallcaseEntries.size() - 1, 0);
    performanceChart.invalidate();
  }

  @Override
  public boolean isShares() {
    return isShares;
  }

  @OnClick(R.id.add_stock_card)
  public void addStockCardClicked() {
    toggleTabLayout(View.VISIBLE);
    addStock();
    TabLayout.Tab tab = tabLayout.getTabAt(0);

    if (tab != null) {
      tab.select();
      searchStockAdapter.setSuggestion(true);
      searchStockAdapter.setStockList(presenter.getSectorSuggestions(tab.getText()));
    } else {
      toggleTabLayout(View.GONE);
    }
  }


  @Override
  public void onStockSelected(Stock stock) {
    Map<String, Object> eventProperties = new HashMap<>();
    eventProperties.put("source", searchStockAdapter.isSuggestion() ? "Similar Stocks" : "SearchBox");
    eventProperties.put("stockName", stock.getStockName());
    analyticsContract.sendEvent(eventProperties, "Added Stock", Analytics.MIXPANEL);

    // Clear previous search
    searchStockAdapter.clearList();
    AppUtils.getInstance().hideKeyboard(this);
    searchStock.setText("");

    // Add stock to stockList
    hideSearchStock();

    if (presenter.doesStockAlreadyExist(stock)) {
      showSnackBar(stock.getStockName() + " is already in your smallcase");
      return;
    }

    if (AppUtils.getInstance().isDoubleEqual(presenter.getLockedWeight(), 100, 0.9)) {
      showSnackBar(R.string.unlock_stocks);
      return;
    }

    if (presenter.getStockAndSegmentList() == null || presenter.getStockAndSegmentList().isEmpty()) {
      showStockAddedLayouts();
    }

    presenter.onNewStockAdded(stock);

    if (stockSegmentAdapter == null) {
      stockSegmentAdapter = new StockSegmentAdapter();
      stocksList.setAdapter(stockSegmentAdapter);
      ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(this, stockSegmentAdapter,
          stockSegmentAdapter.TYPE_SEGMENT);
      ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
      touchHelper.attachToRecyclerView(stocksList);
      return;
    }
    stockSegmentAdapter.notifyDataSetChanged();
    updateStockCount();
  }

  @OnClick(R.id.add_segment)
  public void addSegment() {
    analyticsContract.sendEvent(null, "Clicked Add Segment", Analytics.MIXPANEL);

    addSegmentTitle.setText(getString(R.string.add_segment));
    addSegmentButton.setText(getString(R.string.confirm));

    animations.scaleIntoPlaceAnimation(addSegmentLayout);
    additionalBlur.setVisibility(View.VISIBLE);
    additionalBlur.setOnClickListener(v -> {
      animations.scaleOutAnimation(addSegmentLayout, additionalBlur);
      enterSegmentName.setText("");
    });

    addSegmentButton.setOnClickListener(v -> {
      String segment = enterSegmentName.getText().toString().trim();
      if ("".equals(segment)) return;

      animations.scaleOutAnimation(addSegmentLayout, additionalBlur);
      presenter.onNewSegmentAdded(segment);
      enterSegmentName.setText("");
      if (stockSegmentAdapter == null) {
        stockSegmentAdapter = new StockSegmentAdapter();
        stocksList.setAdapter(stockSegmentAdapter);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(CreateSmallcaseActivity
            .this, stockSegmentAdapter, stockSegmentAdapter.TYPE_SEGMENT);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(stocksList);
        showStockAddedLayouts();
        return;
      }
      stockSegmentAdapter.notifyDataSetChanged();

      analyticsContract.sendEvent(null, "Added Segment", Analytics.MIXPANEL);
    });
  }

  @OnClick(R.id.confirm_value)
  public void onWeightOrSharesConfirmed() {
    AppUtils.getInstance().hideKeyboard(this);
    sharesOrWeightSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
    if (weightOrShares.getText() == null || "".equals(weightOrShares.getText().toString().trim()))
      return;

    if (isShares) {
      stockSegmentAdapter.editShares(editingStock, Double.parseDouble(weightOrShares.getText().toString()));
    } else {
      stockSegmentAdapter.editWeightage(editingStock, Double.parseDouble(weightOrShares.getText().toString()));
    }
  }

  private void showStockAddedLayouts() {
    createSmallcaseImage.setVisibility(View.GONE);
    createSmallcaseTitle.setVisibility(View.GONE);
    addStockInEmptyState.setVisibility(View.GONE);
    smallcasePrivateMessage.setVisibility(View.GONE);

    addStockCard.setVisibility(View.VISIBLE);
    stockTileCard.setVisibility(View.VISIBLE);
    stockListContainer.setVisibility(View.VISIBLE);
  }

  public void updateStockCount() {
    stocksCount.setText("Stock (" + presenter.getStocksSize() + "/20)");
  }

  private void showSharesOrWeightBottomSheet() {
    additionalBlur.setVisibility(View.VISIBLE);
    additionalBlur.setOnClickListener(v -> {
      sharesOrWeightSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
      additionalBlur.setVisibility(View.GONE);
    });

    sharesOrWeightSheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
      @Override
      public void onStateChanged(@NonNull View bottomSheet, int newState) {
        if (newState == BottomSheetBehavior.STATE_HIDDEN) {
          AppUtils.getInstance().hideKeyboard(CreateSmallcaseActivity.this);
          additionalBlur.setVisibility(View.GONE);
        }
      }

      @Override
      public void onSlide(@NonNull View bottomSheet, float slideOffset) {

      }
    });

    cardTitle.setText(isShares ? "Confirm shares" : "Confirm weightage");
    selectedStockName.setText(editingStock.getStockName());
    weightOrShares.setText(isShares ? (String.format(Locale.getDefault(), "%.0f", editingStock.getShares())) :
        String.format(Locale.getDefault(), "%.1f", editingStock.getWeight()));

    sharesOrWeightSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
  }

  void showSegmentOptions(View view, final int segmentPosition) {
    PopupMenu popupMenu = new PopupMenu(CreateSmallcaseActivity.this, view, Gravity.NO_GRAVITY, R.attr.actionOverflowMenuStyle, 0);
    popupMenu.inflate(R.menu.segment_menu);

    MenuItem editSegment = popupMenu.getMenu().findItem(R.id.edit_segment);
    MenuItem deleteSegment = popupMenu.getMenu().findItem(R.id.delete_segment);

    SpannableString editSegmentTitle = new SpannableString(editSegment.getTitle());
    editSegmentTitle.setSpan(new ForegroundColorSpan(ContextCompat.getColor(CreateSmallcaseActivity.this,
        R.color.primary_text)), 0, editSegmentTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    editSegmentTitle.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(CreateSmallcaseActivity.this,
        AppConstants.FontType.REGULAR)), 0, editSegmentTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    editSegmentTitle.setSpan(new RelativeSizeSpan(0.9f), 0, editSegmentTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    editSegment.setTitle(editSegmentTitle);

    SpannableString deleteSegmentTitle = new SpannableString(deleteSegment.getTitle());
    deleteSegmentTitle.setSpan(new ForegroundColorSpan(ContextCompat.getColor(CreateSmallcaseActivity.this,
        R.color.primary_text)), 0, deleteSegmentTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    deleteSegmentTitle.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(CreateSmallcaseActivity.this,
        AppConstants.FontType.REGULAR)), 0, deleteSegmentTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    deleteSegmentTitle.setSpan(new RelativeSizeSpan(0.9f), 0, deleteSegmentTitle.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
    deleteSegment.setTitle(deleteSegmentTitle);

    popupMenu.setOnMenuItemClickListener(item -> {
      switch (item.getItemId()) {
        case R.id.edit_segment:
          updateSegment(segmentPosition);
          break;

        case R.id.delete_segment:
          analyticsContract.sendEvent(null, "Deleted Segment", Analytics.MIXPANEL);

          final int endPosition = presenter.getSegmentEnd(segmentPosition);
          // If no stocks are present in segment, just delete
          if (endPosition == segmentPosition) {
            presenter.getStockAndSegmentList().remove(endPosition);
            stockSegmentAdapter.notifyItemRemoved(endPosition);
            presenter.recheckSegments();
            break;
          }

          // Else ask for confirmation
          additionalBlur.setVisibility(View.VISIBLE);
          additionalBlur.setOnClickListener(v -> animations.scaleOutAnimation(dialogConfirmation, additionalBlur));

          int difference = endPosition - segmentPosition;
          String description = difference == 1 ? "Deleting this segment from the smallcase will also delete the 1" +
              " stock that belongs to this segment" : "Deleting this segment from the smallcase will also delete the "
              + difference + " stocks that belong to this segment";
          dialogDescription.setText(description);
          dialogPrimaryButton.setText(getString(R.string.confirm));
          dialogSecondaryButton.setText(getString(R.string.cancel));

          dialogPrimaryButton.setOnClickListener(v -> {
            animations.scaleOutAnimation(dialogConfirmation, additionalBlur);
            List<Stock> stocks = new ArrayList<>();
            double oldWeight = 0;
            for (Object o : presenter.getStockAndSegmentList().subList(segmentPosition, endPosition + 1)) {
              if (o instanceof Stock) {
                stocks.add((Stock) o);
                oldWeight += ((Stock) o).getWeight();
              }
            }
            if (!presenter.isDeleteValid(stocks)) {
              showSnackBar(R.string.unlock_stocks);
              return;
            }

            stockSegmentAdapter.deleteRange(segmentPosition, endPosition, oldWeight, stocks);
            presenter.recheckSegments();
            // Remove un-used sectors in similar stocks tab
            presenter.removeUnusedSectors();
            presenter.recalculateLockedWeightage();
          });

          dialogSecondaryButton.setOnClickListener(v -> animations.scaleOutAnimation(dialogConfirmation, additionalBlur));

          animations.scaleIntoPlaceAnimation(dialogConfirmation);
          break;
      }

      return false;
    });
    popupMenu.show();
  }

  private void updateSegment(final int segmentPosition) {
    PSegment segment = (PSegment) presenter.getStockAndSegmentList().get(segmentPosition);
    enterSegmentName.setText(segment.getName());

    addSegmentTitle.setText(getString(R.string.rename_segment));
    addSegmentButton.setText(getString(R.string.rename));

    animations.scaleIntoPlaceAnimation(addSegmentLayout);
    additionalBlur.setVisibility(View.VISIBLE);
    additionalBlur.setOnClickListener(v -> {
      animations.scaleOutAnimation(addSegmentLayout, additionalBlur);
      enterSegmentName.setText("");
    });

    addSegmentButton.setOnClickListener(v -> {
      String enteredSegment = enterSegmentName.getText().toString().trim();
      if ("".equals(enteredSegment)) return;

      animations.scaleOutAnimation(addSegmentLayout, additionalBlur);
      presenter.onSegmentUpdated(enteredSegment, segmentPosition);
      stockSegmentAdapter.notifyDataSetChanged();
      enterSegmentName.setText("");
      analyticsContract.sendEvent(null, "Edited Segment Name", Analytics.MIXPANEL);
    });
  }

  private void hideSearchStock() {
    // Clear previous search
    searchStockAdapter.clearList();
    AppUtils.getInstance().hideKeyboard(this);
    searchStock.setText("");

    animations.scaleOutAnimation(searchStockLayout, additionalBlur);
    searchProgress.setVisibility(View.GONE);
  }

  class StockSegmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements ItemTouchHelperAdapter {
    final int TYPE_SEGMENT = 1;
    final int TYPE_STOCK = 2;

    @Override
    public int getItemViewType(int position) {
      if (presenter.getStockAndSegmentList().get(position) instanceof PSegment) {
        return TYPE_SEGMENT;
      } else if (presenter.getStockAndSegmentList().get(position) instanceof Stock) {
        return TYPE_STOCK;
      }
      return -1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      if (viewType == TYPE_SEGMENT) {
        return new SegmentViewHolder(LayoutInflater.from(parent.getContext())
            .inflate(R.layout.segment_card, parent, false));
      } else {
        return new StockViewHolder(LayoutInflater.from(parent.getContext())
            .inflate(R.layout.create_stock_card, parent, false));
      }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
      if (holder.getItemViewType() == TYPE_STOCK) {
        final StockViewHolder stockViewHolder = (StockViewHolder) holder;
        final Stock stock = (Stock) presenter.getStockAndSegmentList().get(position);
        Glide.with(CreateSmallcaseActivity.this)
            .load(R.drawable.drag_handle_grey)
            .into(stockViewHolder.dragHolderImage);
        stockViewHolder.stockName.setText(stock.getStockName());
        if (isShares) {
          stockViewHolder.stockLock.setVisibility(View.GONE);
        } else {
          stockViewHolder.stockLock.setVisibility(View.VISIBLE);
          stockViewHolder.stockLock.setOnClickListener(v -> {
            stock.setLocked(!stock.isLocked());
            selectedWeighingScheme = WeighingScheme.CUSTOM;
            updateWeightingScheme();
            presenter.recalculateLockedWeightage();
            notifyItemChanged(holder.getAdapterPosition());
          });
        }

        if (stock.isLocked()) {
          Glide.with(CreateSmallcaseActivity.this)
              .load(R.drawable.lock_blue)
              .into(stockViewHolder.stockLock);
        } else {
          Glide.with(CreateSmallcaseActivity.this)
              .load(R.drawable.lock_grey)
              .into(stockViewHolder.stockLock);
        }

        if (isShares && stock.getShares() == -1) {
          stockViewHolder.loadingPrice.setVisibility(View.VISIBLE);
          stockViewHolder.sharesOrWeightage.setVisibility(View.GONE);
        } else {
          stockViewHolder.loadingPrice.setVisibility(View.GONE);
          stockViewHolder.sharesOrWeightage.setVisibility(View.VISIBLE);
        }

        stockViewHolder.sharesOrWeightage.setText(isShares ? String.valueOf((int) stock.getShares()) :
            String.format(Locale.getDefault(), "%.1f", stock.getWeight()));

        stockViewHolder.sharesOrWeightage.setOnClickListener(v -> {
          editingStock = stock;
          showSharesOrWeightBottomSheet();
        });

        stockViewHolder.stockName.setOnClickListener(v -> stockInfo.showStockInfo(stock.getSid()));

        stockViewHolder.plus.setOnClickListener(v -> {
          if (isShares) {
            changeShares(stock, stock.getShares() + 1);
          } else {
            changeWeightage(stock.getWeight() < 100, stock, stock.getWeight() + 1);
            presenter.recalculateLockedWeightage();
          }
        });

        stockViewHolder.minus.setOnClickListener(v -> {
          if (isShares) {
            changeShares(stock, stock.getShares() - 1);
          } else {
            changeWeightage(stock.getWeight() > 0, stock, stock.getWeight() - 1);
            presenter.recalculateLockedWeightage();
          }
        });
      } else if (holder.getItemViewType() == TYPE_SEGMENT) {
        final SegmentViewHolder segmentViewHolder = (SegmentViewHolder) holder;
        PSegment segment = (PSegment) presenter.getStockAndSegmentList().get(position);
        segmentViewHolder.segmentTitle.setText(segment.getName());
        if (isShares) {
          segmentViewHolder.segmentWeight.setVisibility(View.INVISIBLE);
        } else {
          segmentViewHolder.segmentWeight.setVisibility(View.VISIBLE);
          segmentViewHolder.segmentWeight.setText(segment.getWeightage());
        }
        Glide.with(CreateSmallcaseActivity.this)
            .load(R.drawable.ic_dots_vertical)
            .into(segmentViewHolder.moreActionSegment);
        segmentViewHolder.moreActionSegment.setOnClickListener(v -> showSegmentOptions(v, segmentViewHolder.getAdapterPosition()));
      }
    }

    private void changeShares(Stock stock, double shares) {
      if (shares < 0) return;

      stock.setShares(shares);
      selectedWeighingScheme = "Custom";
      onFinishedInteraction(null, 0, 0);
    }

    private void changeWeightage(boolean shouldSetWeight, Stock stock, double weight) {
      if (shouldSetWeight) {
        if (weight < 0 || weight == 0) {
          showSnackBar(R.string.weight_cannot_be_zero);
          return;
        }
        selectedWeighingScheme = "Custom";
        onFinishedInteraction(stock, stock.getWeight(), weight);
      }
    }

    @Override
    public int getItemCount() {
      return presenter.getStockAndSegmentList().size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
      if (presenter.getStockAndSegmentList().get(toPosition) instanceof PSegment && toPosition == 0)
        return;

      if (fromPosition < toPosition) {
        for (int i = fromPosition; i < toPosition; i++) {
          Collections.swap(presenter.getStockAndSegmentList(), i, i + 1);
        }
      } else {
        for (int i = fromPosition; i > toPosition; i--) {
          Collections.swap(presenter.getStockAndSegmentList(), i, i - 1);
        }
      }
      notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemMoved() {
      presenter.recalculateSegmentWeight(-1);
      if (!stocksList.isComputingLayout()) {
        notifyDataSetChanged();
      }
    }

    @Override
    public void onItemDismiss(int position) {
      Stock stockToBeDeleted = ((Stock) presenter.getStockAndSegmentList().get(position));
      List<Stock> stocks = new ArrayList<>();
      stocks.add(stockToBeDeleted);
      if (!presenter.isDeleteValid(stocks)) {
        showSnackBar(presenter.getStocksSize() > 1 ? R.string.unlock_stocks : R.string.atleast_two_stocks);
        notifyItemChanged(position);
        return;
      }

      analyticsContract.sendEvent(null, "Removed Stock", Analytics.MIXPANEL);

      if (presenter.getStockAndSegmentList().get(position) instanceof Stock) {
        String name = ((Stock) presenter.getStockAndSegmentList().get(position)).getStockName();
        showSnackBar("\"" + name + "\" has been removed");
      }

      double oldWeight = stockToBeDeleted.getWeight();

      presenter.removeStock(position);
      notifyItemRemoved(position);
      updateStockCount();
      presenter.recalculateSegmentWeight(position - 1);
      presenter.recalculateLockedWeightage();

      onFinishedInteraction(null, oldWeight, 0);
    }

    @Override
    public void onFinishedInteraction(@Nullable Stock stock, double oldWeight, double newWeight) {
      presenter.calculateWeightAndShares(stock, oldWeight, newWeight, 100);
      weighingScheme.setText(selectedWeighingScheme);
      if (stocksList.isComputingLayout()) return;
      notifyDataSetChanged();
    }

    private void editWeightage(Stock stock, double weight) {
      if (weight > 100 || weight < 0) {
        showSnackBar(R.string.invalid_weight);
        return;
      }

      if (weight == 0) {
        showSnackBar(R.string.weight_cannot_be_zero);
        return;
      }

      selectedWeighingScheme = "Custom";
      onFinishedInteraction(stock, stock.getWeight(), weight);
    }

    private void updateWeightingScheme() {
      weighingScheme.setText(selectedWeighingScheme);
    }

    void deleteRange(int startPosition, int endPosition, double oldWeight, List<Stock> stocks) {
      presenter.getStockAndSegmentList().subList(startPosition, endPosition + 1).clear();
      updateStockCount();

      double remainingWeight = 100;
      double consideredLockedWeight = presenter.getLockedWeight();
      double totalDeleteWeight = 0;

      for (Stock stock : stocks) {
        if (stock.isLocked()) consideredLockedWeight -= stock.getWeight();
        totalDeleteWeight += stock.getWeight();
      }
      remainingWeight = remainingWeight - consideredLockedWeight - totalDeleteWeight;

      presenter.calculateWeightAndShares(null, oldWeight, 0, remainingWeight);
      notifyDataSetChanged();
    }

    void editShares(Stock editingStock, double shares) {
      editingStock.setShares(shares < 0 ? 0 : shares);
      selectedWeighingScheme = "Custom";
      onFinishedInteraction(null, 0, 0);
    }

    class StockViewHolder extends RecyclerView.ViewHolder {
      @BindView(R.id.drag_holder_image)
      ImageView dragHolderImage;
      @BindView(R.id.stock_name)
      GraphikText stockName;
      @BindView(R.id.stock_lock)
      ImageView stockLock;
      @BindView(R.id.minus)
      View minus;
      @BindView(R.id.shares_or_weightage)
      GraphikText sharesOrWeightage;
      @BindView(R.id.plus)
      View plus;
      @BindView(R.id.loading_price)
      View loadingPrice;

      StockViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
      }
    }

    class SegmentViewHolder extends RecyclerView.ViewHolder {
      @BindView(R.id.segment_title)
      GraphikText segmentTitle;
      @BindView(R.id.segment_weight)
      GraphikText segmentWeight;
      @BindView(R.id.more_action_segment)
      ImageView moreActionSegment;

      SegmentViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
      }
    }
  }
}
