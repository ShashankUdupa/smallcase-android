package com.smallcase.android.create;

import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Constituents;
import com.smallcase.android.data.model.CustomizedSmallcaseStats;
import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.model.Historical;
import com.smallcase.android.data.model.Info;
import com.smallcase.android.data.model.Points;
import com.smallcase.android.data.model.SearchResult;
import com.smallcase.android.data.model.Segments;
import com.smallcase.android.data.model.Similar;
import com.smallcase.android.data.model.SimilarStock;
import com.smallcase.android.data.model.StockListWithMinAmount;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.SmallcaseCompositionScheme;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.util.SmallcaseTier;
import com.smallcase.android.util.WeighingScheme;
import com.smallcase.android.view.model.PSegment;
import com.smallcase.android.view.model.Stock;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by shashankm on 27/09/17.
 */

class CreateSmallcasePresenter implements CreateSmallcaseContract.Presenter, StockHelper.StockSearchCallback,
    StockHelper.StockPriceCallBack {
  private static final String TAG = "CreateSmallcasePresent";

  /**
   * Stores user entered {@link Stock} and {@link PSegment} objects
   */
  private List<Object> stockAndSegmentList;

  private StockHelper stockHelper;

  private SharedPrefService sharedPrefService;

  private MarketRepository marketRepository;

  private CreateSmallcaseContract.View view;

  private CreateSmallcaseContract.Helper helper;

  /**
   * Contains list of user added segment strings.
   */
  private List<String> segments = new ArrayList<>();

  /**
   * Contains marketCap of every added Stock so that they don't have to be re-fetched every
   * time. MarketCap value of a Stock will be -1 if it hasn't been fetched or 0 if market cap couldn't
   * be fetched.
   */
  private HashMap<String, Double> marketCapMap = new HashMap<>();

  private CreateSmallcaseContract.Service service;

  /**
   * Total marketCap of available Stocks. This is calculated (or re-calculated) only when marketCap
   * details are fetched, which is after a stock is added, or if a Stock is removed.
   */
  private double netMarketCap;

  private SmallcaseHelper smallcaseHelper;

  /**
   * Contains historical points of Stocks that the user has added. This is fetched after successfully
   * fetching current prices of a stock (which is after every addition of a new stock or in case of
   * customize, when the draft is first loaded).
   */
  private List<Points> pointsList;

  /**
   * Contains historical points for nifty. This is fetched only once in the constructor.
   */
  private List<Points> niftyPointsList;

  /**
   * Contains historical points of Stocks from original smallcase, that the user will now customize.
   * This is fetched only once in the constructor.
   */
  private List<Points> originalSmallcasePointsList;

  /**
   * Maintain historical as it's needed in stockHistorical calculation in {@link SmallcaseHelper}
   */
  private List<Historical> historicalList;

  /**
   * Map of sectorName and list of stocks on that sector
   */
  private Map<String, List<Stock>> similarStocks;

  private Map<String, Stock> stockMap;

  /**
   * Total lockedWeight percent out of 100.
   */
  private double lockedWeight = 0;

  private String did, source, tier = SmallcaseTier.BASIC;

  private String scid, smallcaseName, smallcaseDescription, lastSaved;

  CreateSmallcasePresenter(@Nullable List<Object> stockAndSegmentList, StockHelper stockHelper, SharedPrefService
      sharedPrefService, MarketRepository marketRepository, CreateSmallcaseContract.View view, SmallcaseHelper
                               smallcaseHelper, UserSmallcaseRepository userSmallcaseRepository, String smallcaseName, String
                               smallcaseDescription, String scid, String source, SmallcaseRepository smallcaseRepository, String did) {
    this.stockAndSegmentList = stockAndSegmentList;
    this.stockHelper = stockHelper;
    this.sharedPrefService = sharedPrefService;
    this.marketRepository = marketRepository;
    this.view = view;
    this.smallcaseHelper = smallcaseHelper;
    this.smallcaseName = smallcaseName;
    this.smallcaseDescription = smallcaseDescription;
    this.scid = scid;
    this.source = source;
    this.did = did;
    this.service = new CreateSmallcaseService(userSmallcaseRepository, marketRepository, sharedPrefService, this, smallcaseRepository);
    this.helper = new CreateHelper(view, this);

    if (SmallcaseSource.CUSTOM.equals(source)) {
      service.getSmallcaseHistorical(scid, "index", ".NSEI", "1y");
      return;
    }

    List<String> sids = new ArrayList<>();
    sids.add(".NSEI");
    service.getStockHistorical(sids);
  }

  @Nullable
  @Override
  public List<Object> getStockAndSegmentList() {
    return stockAndSegmentList;
  }

  @Override
  public void getStocksForSearch() {
    stockHelper.searchStock(marketRepository, sharedPrefService, view.getSearchString(), this);
  }

  @Override
  public void onNewStockAdded(Stock stock) {
    stock.setMarketCap(-1);

    if (stockAndSegmentList == null) stockAndSegmentList = new ArrayList<>();

    if (similarStocks == null) similarStocks = new HashMap<>();

    if (getStocksSize() >= 20) {
      view.showSnackBar(R.string.not_more_than_twenty);
      return;
    }

    // Add sector tab in similar smallcases if the sector doesn't already exist
    if (!similarStocks.containsKey(stock.getSector())) {
      view.addSimilarTab(stock.getSector());
    }

    if (stockAndSegmentList.isEmpty() || !isNewStockSegmentPresent()) {
      PSegment segment = new PSegment();
      segment.setName("New Stocks");
      segment.setWeightage("0");
      stockAndSegmentList.add(0, segment);
      segments.add(0, segment.getName());
    }

    // Ask user to unlock weight if total locked weight is 99 or above
    if (lockedWeight >= 99) {
      view.showSnackBar(R.string.unlock_stocks);
      return;
    }

    stockAndSegmentList.add(1, stock);

    List<String> sids = new ArrayList<>();
    for (Object object : stockAndSegmentList) {
      if (object instanceof Stock) {
        String sid = ((Stock) object).getSid();
        sids.add(sid);
        marketCapMap.put(sid, stock.getMarketCap());
      }
    }

    // If weighing scheme is MARKET_CAPPED, the stock's market cap needs to be fetched before
    // calculating it's weighing
    if (WeighingScheme.EQUI_WEIGHTED.equals(view.getWeighingScheme())) {
      calculateNonCustomWeightAndShares();
    }

    if (WeighingScheme.CUSTOM.equals(view.getWeighingScheme())) {
      stock.setWeight(-1); // Set weight to -1 to indicate this stock is newly added
      if (view.isShares()) stock.setShares(1);
      calculateCustomWeightage(null, 0, 1, 100);
    }

    if (getStocksSize() == 1)
      view.enableWeightSelection(); // Would previously be disabled if no stocks are present

    stockHelper.getSimilarStocks(sids, marketRepository, sharedPrefService, this);
    service.getStocksInfo(sids);
    stockHelper.getStockPrices(marketRepository, sharedPrefService, sids, this);
  }

  @Override
  public double getLockedWeight() {
    return lockedWeight;
  }

  @Nullable
  @Override
  public String getLastSaved() {
    return lastSaved;
  }

  @Override
  public String getSmallcaseTier() {
    return tier;
  }

  @Nullable
  @Override
  public String getSmallcaseName() {
    return smallcaseName;
  }

  @Override
  public void calculateWeightAndShares(@Nullable Stock stock, double oldWeight, double newWeight, double remainingWeight) {
    if (WeighingScheme.CUSTOM.equals(view.getWeighingScheme()))
      calculateCustomWeightage(stock, oldWeight, newWeight, remainingWeight);
    else calculateNonCustomWeightAndShares();
  }

  private void calculateNonCustomWeightAndShares() {
    if (stockAndSegmentList == null) return;

    if (areNoStocksPresent()) return;

    List<Stock> stockList = helper.calculateNonCustomWeight();
    calculateSharesAndMinAmount(stockList);

    if (historicalList != null) {
      pointsList = smallcaseHelper.createHistoricalForStocks(historicalList, stockList, niftyPointsList);
      helper.calculateHistorical(pointsList, niftyPointsList, originalSmallcasePointsList, source);
    }
  }

  @Override
  public void removeUnusedSectors() {
    Iterator iterator = similarStocks.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry entry = (Map.Entry) iterator.next();
      boolean isSectorPresent = false;
      for (Object object : stockAndSegmentList) {
        if (object instanceof Stock && entry.getKey().equals(((Stock) object).getSector())) {
          isSectorPresent = true;
          break;
        }
      }
      if (!isSectorPresent && entry.getKey() != null) {
        view.removeSimilarTab((String) entry.getKey());
        iterator.remove();
      }
    }
  }

  @Override
  public double getNetMarketCap() {
    return netMarketCap;
  }

  @Override
  public void calculateAmountFromShares(@Nullable PSegment segment, double segmentWeight, List<Stock> stockList) {
    stockHelper.calculateWeightageFromShares(stockList);
    for (Object object : stockAndSegmentList) {
      if (object instanceof Stock) {
        Stock previousStock = (Stock) object;
        previousStock.setWeight(previousStock.getWeight() * 100);
        segmentWeight += previousStock.getWeight();
      } else {
        if (segment != null) {
          segment.setWeightage(String.format(Locale.getDefault(), "%.1f", segmentWeight));
        }

        segment = (PSegment) object;
        segmentWeight = 0;
      }
    }

    if (segment != null) {
      segment.setWeightage(String.format(Locale.getDefault(), "%.1f", segmentWeight));
    }

    view.updateMinimumAmount(stockHelper.calculateAmountFromShares(stockList));
  }

  /**
   * Check if weights can be redistributed on stock (or stocks) dismiss
   *
   * @param stocks that need to be dismissed
   * @return true if weights can be distributed to the remaining stocks
   */
  @Override
  public boolean isDeleteValid(List<Stock> stocks) {
    if (getStocksSize() == 1 || (getStocksSize() - stocks.size() == 0)) return true;

    double consideredLockedWeight = lockedWeight;
    double remainingWeight = 100;
    double totalDeleteWeight = 0;

    for (Stock stock : stocks) {
      if (stock.isLocked()) consideredLockedWeight -= stock.getWeight();
      totalDeleteWeight += stock.getWeight();
    }
    remainingWeight = remainingWeight - consideredLockedWeight - totalDeleteWeight;
    return remainingWeight > 0.1;
  }

  private void calculateCustomWeightage(@Nullable Stock originalStock, double oldWeight, double newWeight, double remainingWeight) {
    if (areNoStocksPresent()) return;

    List<Stock> stockList = helper.calculateCustomWeightageAndShares(originalStock, oldWeight, newWeight, remainingWeight);

    if (stockList == null) return;

    if (historicalList != null) {
      pointsList = smallcaseHelper.createHistoricalForStocks(historicalList, stockList, niftyPointsList);
      helper.calculateHistorical(pointsList, niftyPointsList, originalSmallcasePointsList, source);
    }
  }

  private boolean areNoStocksPresent() {
    // If no stocks are present (when a segment gets deleted)
    if (getStocksSize() == 0) {
      view.showNoStocks();
      calculateSharesAndMinAmount(new ArrayList<>());
      return true;
    }
    return false;
  }

  @Override
  public void onNewSegmentAdded(String segmentName) {
    if (stockAndSegmentList == null) {
      stockAndSegmentList = new ArrayList<>();
    }

    PSegment segment = new PSegment();
    segment.setName(segmentName);
    segment.setWeightage("0");
    stockAndSegmentList.add(segment);
    segments.add(segmentName);
  }

  @Override
  public void onStocksReceived(List<SearchResult> searchResults) {
    view.toggleTabLayout(View.GONE);

    if (searchResults.isEmpty()) {
      view.showNoStocks();
      return;
    }

    List<Stock> stockList = new ArrayList<>();
    for (SearchResult searchResult : searchResults) {
      Stock stock = new Stock();
      stock.setSid(searchResult.getSid());
      stock.setStockName(searchResult.getStock().getInfo().getName());
      stock.setShares(0);
      stock.setWeight(0);
      stock.setSector(searchResult.getStock().getInfo().getSector());
      stockList.add(stock);
    }

    view.showSearchStocks(stockList);
  }

  @Override
  public void onFailedFetchingStocks() {
    view.hideSearchProgress();
    view.showSnackBar(R.string.something_wrong);
  }

  @Override
  public void onFailedFetchingSimilarStocks() {
    view.hideSearchProgress();
    view.showSnackBar(R.string.something_wrong);
  }

  @Override
  public void onSimilarStocksReceived(List<Similar> similarList) {
    for (Similar similar : similarList) {
      List<Stock> stockList = new ArrayList<>();
      for (SimilarStock similarStock : similar.getStocks()) {
        Stock stock = new Stock();
        stock.setSid(similarStock.getSid());
        stock.setShares(0);
        stock.setWeight(0);
        stock.setStockName(similarStock.getSidInfo().getName());
        stock.setSector(similarStock.getSidInfo().getSector());
        stockList.add(stock);
      }
      if (!stockList.isEmpty()) similarStocks.put(similar.get_id(), stockList);
    }

    Iterator iterator = similarStocks.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry entry = (Map.Entry) iterator.next();
      if (entry.getValue() == null) {
        view.removeSimilarTab(entry.getKey().toString());
        iterator.remove();
      }
    }
    Log.d(TAG, "onSimilarStocksReceived: " + similarStocks.size());
  }

  @Override
  public int getStocksSize() {
    return stockAndSegmentList.isEmpty() ? 0 : stockAndSegmentList.size() - segments.size();
  }

  @Override
  public int getSegmentEnd(int segmentPosition) {
    if (segmentPosition + 1 >= stockAndSegmentList.size()) return segmentPosition;

    int segmentEnd = segmentPosition;
    for (int i = segmentPosition + 1; i < stockAndSegmentList.size(); i++) {
      if (stockAndSegmentList.get(i) instanceof PSegment) {
        return segmentEnd;
      } else {
        segmentEnd++;
      }
    }
    return segmentEnd;
  }

  @Override
  public void recheckSegments() {
    segments.clear();
    for (Object object : stockAndSegmentList) {
      if (object instanceof PSegment) segments.add(((PSegment) object).getName());
    }
  }

  @Override
  public void recalculateSegmentWeight(int position) {
    double segmentWeight = 0;
    if (position < 0) {
      PSegment segment = null;
      for (Object object : stockAndSegmentList) {
        if (object instanceof PSegment) {
          if (segment != null) {
            segment.setWeightage(String.format(Locale.getDefault(), "%.1f", segmentWeight));
          }

          segment = (PSegment) object;
          segmentWeight = 0;
        } else {
          Stock stock = (Stock) object;
          segmentWeight += stock.getWeight();
        }
      }
      if (segment != null) {
        segment.setWeightage(String.format(Locale.getDefault(), "%.1f", segmentWeight > 100 ? 100 : segmentWeight));
      }
      return;
    }

    if (stockAndSegmentList.get(position) instanceof PSegment) {
      PSegment segment = (PSegment) stockAndSegmentList.get(position);
      segment.setWeightage("0");
      return;
    }

    for (int i = position; i >= 0; i--) {
      if (stockAndSegmentList.get(position) instanceof PSegment) {
        PSegment segment = (PSegment) stockAndSegmentList.get(position);
        segment.setWeightage(String.format(Locale.getDefault(), "%.1f", segmentWeight));
        break;
      } else {
        Stock stock = (Stock) stockAndSegmentList.get(position);
        segmentWeight += stock.getWeight();
      }
    }
  }

  @Override
  public void onSegmentUpdated(String segmentName, int segmentPosition) {
    PSegment segment = (PSegment) stockAndSegmentList.get(segmentPosition);
    segment.setName(segmentName);
    recheckSegments();
  }

  @Override
  public void onFailedFetchingStockInfo() {
    view.showSnackBar(R.string.something_wrong);
  }

  @Override
  public void onStocksInfoReceived(ResponseBody responseBody) {
    netMarketCap = 0;
    try {
      JSONObject response = new JSONObject(responseBody.string());
      JSONObject data = response.getJSONObject("data");
      for (Map.Entry<String, Double> entry : marketCapMap.entrySet()) {
        if (data.has(entry.getKey())) {
          JSONObject advancedRatios = data.getJSONObject(entry.getKey()).getJSONObject("stock")
              .getJSONObject("advancedRatios");
          if (advancedRatios.has("marketCap") && advancedRatios.get("marketCap") != null) {
            marketCapMap.put(entry.getKey(), advancedRatios.getDouble("marketCap"));
            netMarketCap += advancedRatios.getDouble("marketCap");
          } else {
            marketCapMap.put(entry.getKey(), 0d);
            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), new Exception(),
                TAG + "; onStocksInfoReceived(); data - " + responseBody.toString());
            view.showSnackBar(R.string.something_wrong);
          }
        }
      }

      boolean isPricesCalculated = true;
      List<Stock> stockList = new ArrayList<>();
      for (Object object : stockAndSegmentList) {
        if (object instanceof Stock) {
          Stock stock = (Stock) object;
          stock.setMarketCap(marketCapMap.get(stock.getSid()));
          stockList.add(stock);
          if ((WeighingScheme.MARKET_CAPPED.equals(view.getWeighingScheme()) || WeighingScheme
              .CUSTOM.equals(view.getWeighingScheme())) && stock.getPrice() <= 0) {
            isPricesCalculated = false;
          }
        }
      }

      if (WeighingScheme.MARKET_CAPPED.equals(view.getWeighingScheme())) {
        calculateNonCustomWeightAndShares();
      }

      if (isPricesCalculated && !WeighingScheme.CUSTOM.equals(view.getWeighingScheme())) {
        calculateSharesAndMinAmount(stockList);
      }
    } catch (JSONException | IOException e) {
      e.printStackTrace();
      SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e,
          TAG + "; onStocksInfoReceived()");
    }
  }

  @Override
  public void onStockHistoricalReceived(ResponseBody responseBody) throws JSONException, IOException {
    JSONObject response = new JSONObject(responseBody.string());
    JSONObject data = response.getJSONObject("data");
    Gson gson = new Gson();

    if (niftyPointsList == null) {
      niftyPointsList = new ArrayList<>();
      JSONObject stockSid = data.getJSONObject(".NSEI");
      Historical historical = gson.fromJson(stockSid.toString(), Historical.class);
      niftyPointsList.addAll(historical.getPoint());
      return;
    }

    if (historicalList == null) {
      historicalList = new ArrayList<>();
    } else {
      historicalList.clear();
    }
    List<Stock> stockList = new ArrayList<>();

    for (Object object : stockAndSegmentList) {
      if (object instanceof Stock) {
        Stock stock = (Stock) object;
        JSONObject stockSid = data.getJSONObject(stock.getSid());
        Historical historical = gson.fromJson(stockSid.toString(), Historical.class);
        historicalList.add(historical);
        stockList.add(stock);
      }
    }
    pointsList = smallcaseHelper.createHistoricalForStocks(historicalList, stockList, niftyPointsList);
    helper.calculateHistorical(pointsList, niftyPointsList, originalSmallcasePointsList, source);
  }

  @Override
  public void onFailedFetchingStockHistorical() {
    view.showSnackBar(R.string.something_wrong);
  }

  /**
   * This method calculates total locked weightage and is called whenever the user messes with
   * the weights or shares of any stock (i.e triggering a lock) or user toggles lock of any stock.
   */
  @Override
  public void recalculateLockedWeightage() {
    lockedWeight = 0;
    for (Object object : stockAndSegmentList) {
      if (object instanceof Stock) {
        Stock stock = (Stock) object;
        if (stock.isLocked())
          lockedWeight += AppUtils.getInstance().round(stock.getWeight(), 2);
      }
    }
  }

  /**
   * Called when user selects equi-weighted of market-cap weighted schemes
   */
  @Override
  public void clearLockedWeight() {
    lockedWeight = 0;
    for (Object object : stockAndSegmentList) {
      if (object instanceof Stock) {
        Stock stock = (Stock) object;
        stock.setLocked(false);
      }
    }
  }

  @Override
  public void removeStock(int position) {
    String sector = ((Stock) stockAndSegmentList.get(position)).getSector();
    String sid = ((Stock) stockAndSegmentList.get(position)).getSid();
    stockAndSegmentList.remove(position);

    // Re-calculate netMarketCap
    netMarketCap = 0;
    boolean isSectorPresent = false;
    for (Object object : stockAndSegmentList) {
      if (object instanceof Stock) {
        Stock existingStock = ((Stock) object);
        netMarketCap += existingStock.getMarketCap();
        if (existingStock.getSector().equals(sector)) isSectorPresent = true;
      }
    }

    // If no other stock of removed stock's sector remains, remove it
    if (!isSectorPresent) {
      similarStocks.remove(sector);
      view.removeSimilarTab(sector);
    }

    // Remove historical point of removed stock
    if (historicalList != null && sid != null) {
      ListIterator<Historical> iterator = historicalList.listIterator();
      while (iterator.hasNext()) {
        if (sid.equals(iterator.next().get_id())) {
          iterator.remove();
          break;
        }
      }
    }

    if (getStocksSize() == 0) view.showNoStocks();
  }

  @Override
  public boolean doesStockAlreadyExist(Stock stock) {
    boolean exists = false;
    for (Object object : stockAndSegmentList) {
      if (object instanceof Stock) {
        Stock existingStock = (Stock) object;
        if (stock.getSid().equals(existingStock.getSid())) {
          exists = true;
          break;
        }
      }
    }
    return exists;
  }

  @Override
  public List<Stock> getSectorSuggestions(CharSequence sector) {
    if (sector == null || similarStocks.get(sector.toString()) == null)
      return Collections.emptyList();
    return new ArrayList<>(similarStocks.get(sector.toString()));
  }

  @Override
  public void saveSmallcase() {
    DraftSmallcase draftSmallcase = new DraftSmallcase();
    draftSmallcase.setDid(did);
    draftSmallcase.setScid(scid == null ? did : scid);
    draftSmallcase.setSource(source);
    List<Segments> segmentsList = new ArrayList<>();
    List<String> constituentSids = new ArrayList<>();
    List<Constituents> constituents = new ArrayList<>();
    PSegment segment = null;
    Segments segments = null;
    List<Stock> stockList = new ArrayList<>();
    for (Object object : stockAndSegmentList) {
      if (object instanceof Stock) {
        Stock stock = ((Stock) object);
        Constituents constituent = new Constituents();
        constituent.setSid(stock.getSid());
        constituent.setLocked(stock.isLocked());
        constituent.setShares(calculateSharesToSave(stock));
        constituent.setWeight(stock.getWeight() / 100d);

        constituents.add(constituent);
        constituentSids.add(stock.getSid());
        stockList.add(stock);
      } else {
        if (!constituentSids.isEmpty() && segments != null) {
          segments.setConstituents(new ArrayList<>(constituentSids));
          segmentsList.add(segments);
          constituentSids.clear();
        }

        segment = (PSegment) object;
        segments = new Segments();
        segments.setLabel(segment.getName());
      }
    }
    if (segment != null) {
      segments.setConstituents(constituentSids);
      segmentsList.add(segments);
    }

    draftSmallcase.setSegments(segmentsList);
    draftSmallcase.setConstituents(constituents);
    draftSmallcase.setCompositionScheme(view.isShares() ? SmallcaseCompositionScheme.SHARES : SmallcaseCompositionScheme.WEIGHTS);
    Info info = new Info();
    info.setName(smallcaseName);
    info.setShortDescription(smallcaseDescription);
    info.setTier(tier);
    draftSmallcase.setInfo(info);
    CustomizedSmallcaseStats stats = new CustomizedSmallcaseStats();
    stats.setInitialValue(view.isShares() ? stockHelper.calculateAmountFromShares(stockList) : 100);
    draftSmallcase.setStats(stats);
    if (did == null) {
      view.sendUserToSaveInfo(draftSmallcase);
    } else {
      view.showLoader();
      service.saveSmallcaseAsDraft(draftSmallcase);
    }
  }

  @Override
  public void onFailedSavingDraft() {
    view.hideLoading();
    view.showSnackBar(R.string.something_wrong);
  }

  @Override
  public void onDraftSavedSuccessfully(ResponseBody responseBody) {
    view.hideLoading();
    view.sendUserToDraftSmallcases();
  }

  private double calculateSharesToSave(Stock stock) {
    if (view.isShares()) return stock.getShares();

    return stock.getWeight() / stock.getPrice();
  }

  @Override
  public void loadDraft(DraftSmallcase draftSmallcase) {
    did = draftSmallcase.get_id();
    tier = draftSmallcase.getInfo().getTier();
    lastSaved = draftSmallcase.getInfo().getDateUpdated();
    stockAndSegmentList = new ArrayList<>();

    List<String> sids = new ArrayList<>();
    stockMap = new HashMap<>();
    for (Segments segment : draftSmallcase.getSegments()) {
      PSegment pSegment = new PSegment();
      pSegment.setName(segment.getLabel());
      stockAndSegmentList.add(pSegment);
      segments.add(segment.getLabel());
      for (String sid : segment.getConstituents()) {
        Stock stock = new Stock();
        stock.setSid(sid);
        sids.add(sid);
        stockAndSegmentList.add(stock);
        stockMap.put(sid, stock);
        marketCapMap.put(sid, -1d);
      }
    }

    for (Constituents constituent : draftSmallcase.getConstituents()) {
      Stock stock = stockMap.get(constituent.getSid());
      stock.setLocked(constituent.isLocked());
      stock.setShares(constituent.getShares());
      stock.setWeight(constituent.getWeight() * 100);

      if (stock.isLocked())
        lockedWeight += AppUtils.getInstance().round(stock.getWeight(), 2);
    }

    if (did == null) {
      similarStocks = new HashMap<>();
      List<String> similarStockSid = new ArrayList<>();
      for (Constituents constituent : draftSmallcase.getConstituents()) {
        Stock stock = stockMap.get(constituent.getSid());
        stock.setStockName(constituent.getSidInfo().getName());
        stock.setSector(constituent.getSidInfo().getSector());
        if (!similarStocks.containsKey(constituent.getSidInfo().getSector())) {
          view.addSimilarTab(constituent.getSidInfo().getSector());
          similarStocks.put(constituent.getSidInfo().getSector(), null);
          similarStockSid.add(stock.getSid());
        }
      }

      stockHelper.getSimilarStocks(similarStockSid, marketRepository, sharedPrefService, this);
      view.showStocksLayout();
    } else {
      service.getDraft(did);
    }
    service.getStocksInfo(sids);
    stockHelper.getStockPrices(marketRepository, sharedPrefService, sids, this);
  }

  @Override
  public void onDraftFetchedSuccessfully(DraftSmallcase draftSmallcase) {
    similarStocks = new HashMap<>();
    List<String> similarStockSid = new ArrayList<>();
    for (Constituents constituent : draftSmallcase.getConstituents()) {
      Stock stock = stockMap.get(constituent.getSid());
      stock.setStockName(constituent.getSidInfo().getName());
      stock.setSector(constituent.getSidInfo().getSector());
      if (!similarStocks.containsKey(constituent.getSidInfo().getSector())) {
        view.addSimilarTab(constituent.getSidInfo().getSector());
        similarStocks.put(constituent.getSidInfo().getSector(), null);
        similarStockSid.add(stock.getSid());
      }
    }

    stockHelper.getSimilarStocks(similarStockSid, marketRepository, sharedPrefService, this);
    view.showStocksLayout();
  }

  @Override
  public void onFailedFetchingDraft() {
    view.showSnackBar(R.string.something_wrong);
  }

  @Override
  public void destroySubscriptions() {
    service.destroySubscriptions();
  }

  @Override
  public void onSmallcaseHistoricalReceived(Historical historical) {
    originalSmallcasePointsList = new ArrayList<>();
    originalSmallcasePointsList.addAll(historical.getPoints());

    niftyPointsList = new ArrayList<>();
    niftyPointsList.addAll(historical.getBenchmark().getPoints());
  }

  @Override
  public void onFailedFetchingSmallcaseHistorical() {
    view.showSnackBar(R.string.something_wrong);
  }

  @Override
  public boolean shouldShowTutorial() {
    return !sharedPrefService.hasUserSeenCreateTutorial();
  }

  @Override
  public void setSeenTutorial() {
    sharedPrefService.setUserSeenCreateTutorial();
  }

  /**
   * @return false if "New Stocks" segment isn't present
   */
  private boolean isNewStockSegmentPresent() {
    for (String segment : segments) {
      if ("New Stocks".equals(segment)) return true;
    }
    return false;
  }

  @Override
  public void onStockPricesReceived(HashMap<String, Double> stockMap) {
    if (stockAndSegmentList == null) return;


    boolean isMarketCapCalculated = true;
    List<Stock> stockList = new ArrayList<>();
    List<String> sids = new ArrayList<>();

    ListIterator<Object> iterator = stockAndSegmentList.listIterator();
    while (iterator.hasNext()) {
      Object object = iterator.next();
      if (object instanceof Stock) {
        Stock stock = (Stock) object;
        if (stockMap.containsKey(stock.getSid())) {
          stock.setPrice(stockMap.get(stock.getSid()));
          stockList.add(stock);
          if (WeighingScheme.MARKET_CAPPED.equals(view.getWeighingScheme()) && stock.getMarketCap() == -1) {
            isMarketCapCalculated = false;
          }
          sids.add(stock.getSid());
        } else {
          iterator.remove();
        }
      }
    }

    if (view.isShares() && WeighingScheme.CUSTOM.equals(view.getWeighingScheme())) {
      calculateAmountFromShares(null, 0, stockList);
    } else if (isMarketCapCalculated) {
      calculateSharesAndMinAmount(stockList);
    }
    service.getStockHistorical(sids);
  }

  @Override
  public void onFailedFetchingStockPrice(Throwable throwable, @Nullable String sid) {
    if (sid != null) {
      ListIterator<Object> iterator = stockAndSegmentList.listIterator();
      while (iterator.hasNext()) {
        Object object = iterator.next();
        if (object instanceof Stock) {
          Stock stock = (Stock) object;
          if (sid.equals(stock.getSid())) {
            iterator.remove();
            similarStocks.remove(stock.getSector());
            view.removeSimilarTab(stock.getSector());
            break;
          }
        }
      }

      view.showSnackBar(sid + " cannot be added at the moment. Please check markets and try again later");
      calculateWeightAndShares(null, 1, 0, 100);
      return;
    }

    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

    view.showSnackBar(R.string.something_wrong);
  }

  @Override
  public void calculateSharesAndMinAmount(List<Stock> stockList) {
    if (stockList == null || stockList.isEmpty()) {
      view.updateMinimumAmount(0);
      return;
    }

    StockListWithMinAmount stockListWithMinAmount = stockHelper.calculateSharesAndMinAmountFromWeight(stockList, -1);
    int i = 0;
    for (Object object : stockAndSegmentList) {
      if (object instanceof Stock) {
        Stock stock = (Stock) object;
        Stock calculatedStock = stockListWithMinAmount.getStockList().get(i);
        stock.setShares(calculatedStock.getShares());
        stock.setWeight(calculatedStock.getWeight());
        i++;
      }
    }

    view.updateMinimumAmount(stockListWithMinAmount.getMinAmount());
  }


}
