package com.smallcase.android.create;

import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.model.Historical;
import com.smallcase.android.data.model.Points;
import com.smallcase.android.view.model.PPoints;
import com.smallcase.android.view.model.PSegment;
import com.smallcase.android.view.model.Stock;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by shashankm on 27/09/17.
 */

interface CreateSmallcaseContract {
  interface View {

    String getSearchString();

    void showSnackBar(@StringRes int resId);

    void toggleTabLayout(int visibility);

    void showSearchStocks(List<Stock> searchResults);

    void showNoStocks();

    String getWeighingScheme();

    void updateMinimumAmount(double minAmount);

    void updateHistoricalChart(List<PPoints> smallcasePoints, boolean shouldShouldOriginalSmallcase);

    boolean isShares();

    void showSnackBar(String message);

    void addSimilarTab(String sector);

    void sendUserToDraftSmallcases();

    void hideLoading();

    void showStocksLayout();

    void invalidateGraph();

    void removeSimilarTab(String sector);

    void hideSearchProgress();

    void enableWeightSelection();

    void sendUserToSaveInfo(DraftSmallcase draftSmallcase);

    void showLoader();
  }

  interface Presenter {

    List<Object> getStockAndSegmentList();

    void getStocksForSearch();

    void onNewStockAdded(Stock stock);

    // Remaining weight is supplied when a segment is deleted along with it's stocks
    void calculateWeightAndShares(@Nullable Stock stock, double oldWeight, double newWeight, double remainingWeight);

    void onNewSegmentAdded(String segment);

    int getStocksSize();

    int getSegmentEnd(int segmentPosition);

    void recheckSegments();

    void recalculateSegmentWeight(int position);

    void onSegmentUpdated(String segmentName, int segmentPosition);

    void onFailedFetchingStockInfo();

    void onStocksInfoReceived(ResponseBody responseBody);

    void onStockHistoricalReceived(ResponseBody responseBody) throws JSONException, IOException;

    void onFailedFetchingStockHistorical();

    void recalculateLockedWeightage();

    void clearLockedWeight();

    double getLockedWeight();

    String getLastSaved();

    String getSmallcaseTier();

    String getSmallcaseName();

    void removeStock(int position);

    boolean doesStockAlreadyExist(Stock stock);

    List<Stock> getSectorSuggestions(CharSequence sector);

    void saveSmallcase();

    void onFailedSavingDraft();

    void onDraftSavedSuccessfully(ResponseBody responseBody);

    void loadDraft(DraftSmallcase draftSmallcase);

    void onDraftFetchedSuccessfully(DraftSmallcase draftSmallcase);

    void onFailedFetchingDraft();

    void destroySubscriptions();

    void onSmallcaseHistoricalReceived(Historical historical);

    void onFailedFetchingSmallcaseHistorical();

    boolean shouldShowTutorial();

    void setSeenTutorial();

    boolean isDeleteValid(List<Stock> stocks);

    void removeUnusedSectors();

    double getNetMarketCap();

    void calculateSharesAndMinAmount(List<Stock> stockList);

    void calculateAmountFromShares(@Nullable PSegment segment, double segmentWeight, List<Stock> stockList);
  }

  interface Service {

    void getStocksInfo(List<String> sids);

    void getStockHistorical(List<String> sids);

    void getSmallcaseHistorical(String scid, String benchmarkType, String benchmarkId, String duration);

    void saveSmallcaseAsDraft(DraftSmallcase draftSmallcase);

    void getDraft(String did);

    void destroySubscriptions();
  }

  interface Helper {

    List<Stock> calculateNonCustomWeight();

    List<Stock> calculateCustomWeightageAndShares(Stock originalStock, double oldWeight, double newWeight, double remainingWeight);

    void calculateHistorical(List<Points> pointsList, List<Points> niftyPointsList, List<Points> originalSmallcasePointsList, String source);
  }
}
