package com.smallcase.android.create;

import android.support.annotation.Nullable;

import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Points;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.util.WeighingScheme;
import com.smallcase.android.view.model.PPoints;
import com.smallcase.android.view.model.PSegment;
import com.smallcase.android.view.model.Stock;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by shashankm on 24/11/17.
 */

class CreateHelper implements CreateSmallcaseContract.Helper {
    private static final String TAG = "CreateHelper";
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault());

    private CreateSmallcaseContract.View view;
    private CreateSmallcaseContract.Presenter presenter;

    CreateHelper(CreateSmallcaseContract.View view, CreateSmallcaseContract.Presenter presenter) {
        this.view = view;
        this.presenter = presenter;
    }

    @Override
    public List<Stock> calculateNonCustomWeight() {
        PSegment segment = null;
        double segmentWeight = 0;

        List<Stock> stockList = new ArrayList<>();

        for (Object object : presenter.getStockAndSegmentList()) {
            if (object instanceof Stock) {
                Stock stock = (Stock) object;
                double weight = 0;
                if (WeighingScheme.EQUI_WEIGHTED.equals(view.getWeighingScheme())) {
                    // Cast to double to make sure correct calculation is carried out
                    weight = 100d / (double) presenter.getStocksSize();
                } else if (WeighingScheme.MARKET_CAPPED.equals(view.getWeighingScheme())) {
                    weight = stock.getMarketCap() * 100 / presenter.getNetMarketCap();
                }
                stock.setWeight(weight);
                segmentWeight += weight;
                stockList.add(stock);
            } else {
                if (segment != null) {
                    // Segment weight calculated with all it's stocks
                    segment.setWeightage(String.format(Locale.getDefault(), "%.1f", segmentWeight));
                }

                segment = (PSegment) object;
                segmentWeight = 0;
            }
        }

        // Last segment's weightage
        if (segment != null) segment.setWeightage(String.format(Locale.getDefault(), "%.1f", segmentWeight >
                100 ? 100 : segmentWeight));

        return stockList;
    }

    @Nullable
    @Override
    public List<Stock> calculateCustomWeightageAndShares(Stock originalStock, double oldWeight, double newWeight, double remainingWeight) {
        double segmentWeight = 0;
        List<Stock> stockList = new ArrayList<>();
        PSegment segment = null;

        double consideredLockedWeight = presenter.getLockedWeight();

        // Prevent double calculation of locked weight
        if (originalStock != null && originalStock.isLocked()) consideredLockedWeight -= originalStock.getWeight();

        double changeInWeight = newWeight - oldWeight;

        // Calculate weight that needs to re-distributed
        if (remainingWeight == 100) remainingWeight = 100 - consideredLockedWeight - oldWeight;


        if (remainingWeight < 0 || AppUtils.getInstance().isZero(remainingWeight)) {
            // Unable to distribute the weight
            view.showSnackBar(presenter.getStocksSize() > 1 ? R.string.unlock_stocks : R.string.atleast_two_stocks);
            return null;
        }

        if (changeInWeight > 0 && changeInWeight > remainingWeight) {
            // Unable to allocate weight
            view.showSnackBar(presenter.getStocksSize() > 1 ? R.string.unlock_stocks : R.string.atleast_two_stocks);
            return null;
        }

        if (originalStock != null) {
            // When weight of a stock is changed
            originalStock.setWeight(newWeight);
            originalStock.setLocked(true);
            presenter.recalculateLockedWeightage();
        }

        for (Object object : presenter.getStockAndSegmentList()) {
            if (object instanceof Stock) {
                Stock stock = (Stock) object;

                if (stock.isLocked()) {
                    segmentWeight += stock.getWeight();
                } else {
                    double weight;
                    if (stock.getWeight() == -1) {
                        weight = 1; // Set stock weight to 1 for newly added stock
                    } else {
                        // Use below algorithm for re-distributing remaining weight
                        weight = stock.getWeight() - (changeInWeight * (stock.getWeight() / remainingWeight));
                    }
                    stock.setWeight(weight);
                    segmentWeight += weight;
                }
                stockList.add(stock);
            } else {
                if (segment != null) segment.setWeightage(String.format(Locale.getDefault(), "%.1f", segmentWeight));

                segment = (PSegment) object;
                segmentWeight = 0;
            }
        }

        // Last segment's weightage
        if (segment != null) segment.setWeightage(String.format(Locale.getDefault(), "%.1f", segmentWeight > 100
                ? 100 : segmentWeight));

        if (view.isShares()) {
            presenter.calculateAmountFromShares(segment, segmentWeight, stockList);
        } else {
            presenter.calculateSharesAndMinAmount(stockList); // From weight
        }

        return stockList;
    }

    @Override
    public void calculateHistorical(List<Points> pointsList, List<Points> niftyPointsList, List<Points>
            originalSmallcasePointsList, String source) {
        if (pointsList.isEmpty() || niftyPointsList.isEmpty()) return;

        if (pointsList.get(0).getIndex() == 0) {
            view.invalidateGraph();
            return;
        }

        List<PPoints> smallcasePoints = new ArrayList<>();
        double baseSmallcaseIndex = 100 / pointsList.get(0).getIndex(); // Convert index to be base 100
        double baseNiftyIndex = 100 / niftyPointsList.get(0).getPrice(); // Convert price to be base 100
        double baseOriginalSmallcaseIndex = -1;
        if (SmallcaseSource.CUSTOM.equals(source)) {
            baseOriginalSmallcaseIndex = 100 / originalSmallcasePointsList.get(0).getIndex(); // Calculate 3rd graph for custom
        }
        for (int i = 0; i < pointsList.size(); i++) {
            Points smallcasePoint = pointsList.get(i);
            Points niftyPoint, originalSmallcasePoint = null;
            try {
                niftyPoint = niftyPointsList.get(i);
                if (SmallcaseSource.CUSTOM.equals(source)) originalSmallcasePoint = originalSmallcasePointsList.get(i);
            } catch (IndexOutOfBoundsException e) {
                SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; calculateHistorical()");
                // Break out if nifty price points size is not equal to smallcase index points (Should always be equal)
                return;
            }

            PPoints presentablePoint = new PPoints();
            presentablePoint.setDate(simpleDateFormat.format(AppUtils.getInstance().getPlainDate(smallcasePoint.getDate())));
            presentablePoint.setSmallcaseIndex(baseSmallcaseIndex * smallcasePoint.getIndex());
            presentablePoint.setNiftyIndex(baseNiftyIndex * niftyPoint.getPrice());
            presentablePoint.setOriginalSmallcaseIndex(originalSmallcasePoint == null ? -1 : baseOriginalSmallcaseIndex *
                    originalSmallcasePoint.getIndex());
            smallcasePoints.add(presentablePoint);
        }
        view.updateHistoricalChart(smallcasePoints, SmallcaseSource.CUSTOM.equals(source));
    }
}
