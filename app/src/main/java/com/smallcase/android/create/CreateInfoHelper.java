package com.smallcase.android.create;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.util.Log;

import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.model.ImageUploadOptions;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;

import java.io.File;
import java.util.LinkedHashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.adapter.rxjava.HttpException;

/**
 * Created by shashankm on 26/09/17.
 */

class CreateInfoHelper {
    private static final String TAG = "CreateInfoHelper";

    private View viewContract;
    private UserSmallcaseRepository userSmallcaseRepository;
    private SharedPrefService sharedPrefService;
    private final long maxFileSize = 1024 / 2; // 500 kb

    interface View {
        void hideLoader();

        void showSnackBar(@StringRes int resId);

        void onImageUploadedSuccessfully();

        void onFailedUploadingImage();

        void onFailedUpdatingDraft();

        void onDraftUpdatedSuccessfully();
    }

    CreateInfoHelper(View viewContract, UserSmallcaseRepository userSmallcaseRepository, SharedPrefService sharedPrefService) {
        this.viewContract = viewContract;
        this.userSmallcaseRepository = userSmallcaseRepository;
        this.sharedPrefService = sharedPrefService;
    }

    boolean isInputValid(String smallcaseName, String description) {
        return (!"".equals(smallcaseName.trim()) && !"".equals(description.trim()));
    }

    void uploadImage(Uri uri, String scid) {
        File file = new File(Uri.parse(uri.toString()).getPath());
        if ((file.length() / 1024) > maxFileSize) {
            viewContract.hideLoader();
            viewContract.showSnackBar(R.string.exceeded_max_size);
            return;
        }

        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", "uploadFile.png", reqFile);
        uploadDraftImage(body, scid);
    }

    void updateInfo(DraftSmallcase draftSmallcase) {
        userSmallcaseRepository.saveDraft(sharedPrefService.getAuthorizationToken(), sharedPrefService
                .getCsrfToken(), draftSmallcase)
                .subscribe(responseBody -> viewContract.onDraftUpdatedSuccessfully(), throwable -> {
                    viewContract.onFailedUpdatingDraft();

                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; updateInfo()");
                });
    }

    private void uploadDraftImage(MultipartBody.Part body, String scid) {
        userSmallcaseRepository.getImageUploadConfig(sharedPrefService.getAuthorizationToken(), sharedPrefService
                .getCsrfToken(), scid)
                .subscribe(imageUploadOptions -> {
                    LinkedHashMap<String, RequestBody> map = getStringRequestBodyMap(imageUploadOptions);
                    userSmallcaseRepository.uploadDraftImage(imageUploadOptions.getEndPoint(), body, map)
                            .subscribe(responseBody -> viewContract.onImageUploadedSuccessfully(), throwable -> {
                                Log.d(TAG, "uploadDraftImage: " + throwable.toString());
                                viewContract.onFailedUploadingImage();
                                if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                                SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; uploadDraftImage()");
                            });
                }, throwable -> {
                    Log.d(TAG, "uploadDraftImage: " + throwable.toString());
                    viewContract.onFailedUploadingImage();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; uploadDraftImage(); getConfig");
                });
    }

    @NonNull
    private LinkedHashMap<String, RequestBody> getStringRequestBodyMap(ImageUploadOptions imageUploadOptions) {
        RequestBody key = RequestBody.create(MediaType.parse("text/plain"), imageUploadOptions.getParams().getKey());
        RequestBody acl = RequestBody.create(MediaType.parse("text/plain"), imageUploadOptions.getParams().getAcl());
        RequestBody policy = RequestBody.create(MediaType.parse("text/plain"), imageUploadOptions.getParams().getPolicy());
        RequestBody successActionStatus = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(imageUploadOptions.getParams().getSuccessActionStatus()));
        RequestBody xAmzAlgorithm = RequestBody.create(MediaType.parse("text/plain"), imageUploadOptions.getParams().getxAmzAlgorithm());
        RequestBody xAmzCredential = RequestBody.create(MediaType.parse("text/plain"), imageUploadOptions.getParams().getxAmzCredential());
        RequestBody xAmzDate = RequestBody.create(MediaType.parse("text/plain"), imageUploadOptions.getParams().getxAmzDate());
        RequestBody xAmzSignature = RequestBody.create(MediaType.parse("text/plain"), imageUploadOptions.getParams().getxAmzSignature());

        LinkedHashMap<String, RequestBody> map = new LinkedHashMap<>();
        map.put("key", key);
        map.put("acl", acl);
        map.put("policy", policy);
        map.put("success_action_status", successActionStatus);
        map.put("x-amz-algorithm", xAmzAlgorithm);
        map.put("x-amz-credential", xAmzCredential);
        map.put("x-amz-date", xAmzDate);
        map.put("x-amz-signature", xAmzSignature);
        return map;
    }
}
