package com.smallcase.android.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Currency;
import java.util.Locale;

/**
 * Created by shashankm on 08/11/16.
 */

public class AppConstants {
    public static final String SMALLCASE_IMAGE_URL = "https://www.smallcase.com/images/smallcases/";

    public static final String CREATED_SMALLCASE_IMAGE_URL = "https://cimg.smallcase.com/images/user/smallcases/";

    public static final int OPTION_CHANGED = 2;
    public static final int STOCK = 4;
    public static final int EXITED_SMALLCASES = 6;
    public static final int RECENT_SMALLCASES = 7;
    public static final int POPULAR_SMALLCASES = 8;
    public static final int NEWS_TYPE_INVESTED = 9;
    public static final int NEWS_TYPE_WATCHLISTED = 10;
    public static final int NEWS_TYPE_UN_INVESTED = 11;
    public static final String KITE_ADD_FUNDS = "https://kite.zerodha.com/funds/";

    // Notification broadcast
    public static final String MARK_ALL_READ = "mark_all_read";
    public static final String READ_ONE = "read_one";
    public static final String NOTIFICATION_STATUS = "notification_status";
    public static final String NOTIFICATION_BROADCAST = "notification_broadcast";
    public static final String RE_BALANCE_IGNORED_BROADCAST = "re_balance_broadcast";
    public static final String PLACED_ORDER_BROADCAST = "placed_order_broadcast";
    public static final String BUY_REMINDER_BROADCAST = "buy_reminder_broadcast";
    public static final String WATCHLIST_REMOVED_BROADCAST = "watchlist_removed_broadcast";
    public static final String WATCHLIST_RE_ADDED_BROADCAST = "watchlist_re_added_broadcast";
    public static final String HARD_RELOAD_BROADCAST = "hard_reload_broadcast";
    public static final String FUNDS_REFRESH = "funds_refresh";
    public static final String SHOW_SIP_CANCELLED = "sip_cancelled";
    public static final String DRAFT_DELETED = "draft_deleted";
    public static final String ON_SMALLCASE_SAVED = "on_smallcase_saved";

    public enum FontType {
        BOLD, LIGHT, MEDIUM, MEDIUM_ITALIC, REGULAR, REGULAR_ITALIC, SEMI_BOLD
    }

    public enum MarketStatus {
        NOT_FETCHED, OPEN, CLOSED
    }

    public static final String RUPEE_SYMBOL = "\u20B9 ";
    public static final String HYPHEN_SYMBOL = "\u2015";

    private static DecimalFormat NUMBER_FORMATTER;
    private static DecimalFormat NUMBER_FORMATTER_WITH_DECIMALS;

    public static DecimalFormat getNumberFormatter() {
        if (NUMBER_FORMATTER == null) {
            NUMBER_FORMATTER = new DecimalFormat("##,##,##,###");
            NUMBER_FORMATTER.setCurrency(Currency.getInstance(Locale.getDefault()));
            NUMBER_FORMATTER.setRoundingMode(RoundingMode.valueOf(BigDecimal.ROUND_FLOOR));
        }
        return NUMBER_FORMATTER;
    }

    public static DecimalFormat getDecimalNumberFormatter() {
        if (NUMBER_FORMATTER_WITH_DECIMALS == null) {
            NUMBER_FORMATTER_WITH_DECIMALS = new DecimalFormat("##,##,##,###.##");
            NUMBER_FORMATTER_WITH_DECIMALS.setCurrency(Currency.getInstance(Locale.getDefault()));
        }
        return NUMBER_FORMATTER_WITH_DECIMALS;
    }
}
