package com.smallcase.android.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.smallcase.android.analytics.SentryAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;
import java.util.TimeZone;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Contains utility methods that will be used throughout the app.
 */

public class AppUtils {
    private static final String TAG = AppUtils.class.getSimpleName();
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd HH:mm", Locale.getDefault());
    private static final SimpleDateFormat plainSdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.getDefault());
    private static final SimpleDateFormat convertIsoSdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.getDefault());
    private static final double threshold = 0.01;

    private static AppUtils instance;
    private static Hashtable<AppConstants.FontType, Typeface> fontCache = new Hashtable<>();
    private Snackbar snackbar;

    public static AppUtils getInstance() {
        if (instance == null) {
            instance = new AppUtils();
            convertIsoSdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        return instance;
    }

    public float dpToPx(float dp) {
        return (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public Typeface getFont(Context context, AppConstants.FontType type) {
        Typeface typeface = fontCache.get(type);
        if (typeface != null) return typeface;

        String fontType = "";
        switch (type) {
            case BOLD:
                fontType = "graphik-bold.ttf";
                break;

            case LIGHT:
                fontType = "graphik-light.ttf";
                break;

            case MEDIUM:
                fontType = "graphik-medium.ttf";
                break;

            case MEDIUM_ITALIC:
                fontType = "graphik-medium-italic.ttf";
                break;

            case REGULAR:
                fontType = "graphik-regular.ttf";
                break;

            case REGULAR_ITALIC:
                fontType = "graphik-regular-italic.ttf";
                break;

            case SEMI_BOLD:
                fontType = "graphik-semi-bold.ttf";
                break;
        }
        typeface = Typeface.createFromAsset(context.getAssets(),
                "font/" + fontType);
        fontCache.put(type, typeface);
        return typeface;
    }

    public double round(double value, int places) {
        if (value == Double.POSITIVE_INFINITY || value == Double.NEGATIVE_INFINITY ||
                value == Double.NaN) {
            return 0;
        }

        if (places < 0) throw new IllegalArgumentException();

        if (places == 0) {
            return (int) Math.round(value);
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public String getUserId(@Nullable String jwt) {
        if (jwt == null || !jwt.contains(".")) return "Error: " + jwt;

        String[] splitJwt = jwt.split("\\.");
        String jwtCsrfString;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            jwtCsrfString = new String(Base64.decode(splitJwt[1], Base64.DEFAULT), StandardCharsets.UTF_8);
        } else {
            jwtCsrfString = new String(Base64.decode(splitJwt[1], Base64.DEFAULT), Charset.defaultCharset());
        }
        try {
            JSONObject jwtObject = new JSONObject(jwtCsrfString);
            return jwtObject.getString("userId");
        } catch (JSONException e) {
            SentryAnalytics.getInstance().captureEvent(jwt, e, TAG + "; getUserId()");
            e.printStackTrace();
            return "";
        }
    }

    public SimpleDateFormat getSdf() {
        return sdf;
    }

    public String getTimeAgoDate(String stringDate) {
        long date = getPlainDate(stringDate).getTime();
        long now = new Date().getTime();

        long difference = (now - date) / 1000;
        int day = 3600 * 24;
        if ((difference > (day * 365))) {
            String time = String.valueOf((int) (difference / (day * 365)));
            return time + "y ago";
        } else if ((difference > (day * 30))) {
            String time = String.valueOf((int) (difference / (day * 30)));
            return time + "mo ago";
        } else if ((difference > (day * 7))) {
            String time = String.valueOf((int) (difference / (day * 7)));
            return time + "w ago";
        } else if (difference > (day)) {
            String time = String.valueOf((int) (difference / day));
            return time + "d ago";
        } else if (difference > (3600)) {
            String time = String.valueOf((int) (difference / (3600)));
            return time + "h ago";
        } else if (difference > 60) {
            String time = String.valueOf((int) (difference / 60));
            return time + "m ago";
        } else {
            String time = String.valueOf(difference);
            return time + "s ago";
        }
    }

    public String getDateInApiSendableFormat(Calendar calendar) {
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return year + "-" + month + "-" + day;
    }

    public Date getPlainDate(String stringDate) {
        try {
            return plainSdf.parse(stringDate.replaceAll("Z$", "+0000"));
        } catch (Exception e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public void showSnackBar(View parent, int duration, @StringRes int resId) {
        snackbar = Snackbar.make(parent, resId, duration);
        if (snackbar.isShown()) return;
        snackbar.show();
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    // Floating point and double values are checked with this
    public boolean isZero(double value) {
        return value >= -threshold && value <= threshold;
    }

    public void hideKeyboard(Activity activity) {
        if (activity == null) return;

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        if (null == activity.getCurrentFocus() || null == inputMethodManager) return;
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public boolean isDoubleEqual(double firstValue, double secondValue, double acceptedError) {
        return Math.abs(firstValue - secondValue) < acceptedError;
    }

    private String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }
}
