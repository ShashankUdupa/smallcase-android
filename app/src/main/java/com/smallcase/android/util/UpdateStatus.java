package com.smallcase.android.util;

/**
 * Created by shashankm on 24/09/17.
 */

public class UpdateStatus {
    public static final String NO_UPDATE = "noUpdate";
    public static final String FORCE_UPDATE = "forceUpdate";
    public static final String RECOMMENDED_UPDATE = "recommendedUpdate";
}
