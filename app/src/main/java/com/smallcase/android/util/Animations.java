package com.smallcase.android.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

/**
 * Contains dynamic animations in app.
 */

public class Animations {
    private static final String TAG = Animations.class.getSimpleName();
    private final static Interpolator ANTICIPATE_INTERPOLATOR = new AnticipateInterpolator(1.5f);
    private final static Interpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator(1.5f);

    private ValueAnimator colorAnim;

    public interface ScaleOutAnimationListener {
        void onAnimationEnd();
    }

    public void expand(final View view, Interpolator interpolator, long duration) {
        view.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout
                .LayoutParams.WRAP_CONTENT);
        final int targetHeight = view.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        view.getLayoutParams().height = 1;
        view.setVisibility(View.VISIBLE);
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                view.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                view.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setInterpolator(interpolator);
        animation.setDuration(duration);
        view.startAnimation(animation);
    }

    public void collapse(final View view, Interpolator interpolator, long duration) {
        final int initialHeight = view.getMeasuredHeight();

        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    view.setVisibility(View.GONE);
                } else {
                    view.getLayoutParams().height = initialHeight - (int) (initialHeight
                            * (interpolatedTime));
                    view.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setInterpolator(interpolator);
        animation.setDuration(duration);

        view.startAnimation(animation);
    }

    public void itemClick(View view, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                view.animate().scaleX(0.8f).scaleY(0.8f).setDuration(150)
                        .setInterpolator(new AccelerateDecelerateInterpolator());
                view.setPressed(true);
                break;
            case MotionEvent.ACTION_MOVE:
                float x = event.getX();
                float y = event.getY();
                boolean isInside = (x > 0 && x < view.getWidth() && y > 0 &&
                        y < view.getHeight());
                if (view.isPressed() != isInside) {
                    view.setPressed(isInside);
                }
                break;
            case MotionEvent.ACTION_UP:
                view.animate().scaleX(1).scaleY(1).setInterpolator
                        (new OvershootInterpolator());
                if (view.isPressed()) {
                    view.performClick();
                    view.setPressed(false);
                }
                break;
        }
    }

    public void loadingContent(final View... views) {
        for (View view : views) {
            colorAnim = ObjectAnimator.ofFloat(view, "alpha", 0.5f, 0.3f);
            colorAnim.setInterpolator(OVERSHOOT_INTERPOLATOR);
            colorAnim.setDuration(450);
            colorAnim.setRepeatMode(ValueAnimator.REVERSE);
            colorAnim.setRepeatCount(20);
            colorAnim.start();
        }
    }

    public void scaleIntoPlaceAnimation(View view) {
        view.setVisibility(View.VISIBLE);
        view.animate()
                .setListener(null)
                .scaleX(1)
                .scaleY(1)
                .setInterpolator(OVERSHOOT_INTERPOLATOR)
                .setDuration(300)
                .start();
    }

    public void scaleOutAnimation(final View view, final View blur) {
        view.animate()
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        view.setVisibility(View.GONE);
                        blur.setVisibility(View.GONE);
                    }
                })
                .scaleX(0)
                .scaleY(0)
                .setInterpolator(ANTICIPATE_INTERPOLATOR)
                .setDuration(200)
                .start();
    }

    public void scaleOutAnimation(final View view) {
        view.animate()
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        view.setVisibility(View.GONE);
                    }
                })
                .scaleX(0)
                .scaleY(0)
                .setInterpolator(ANTICIPATE_INTERPOLATOR)
                .setDuration(200)
                .start();
    }

    public void scaleOutAnimation(final View view, final View blur, final ScaleOutAnimationListener scaleOutAnimationListener) {
        view.animate()
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        view.setVisibility(View.GONE);
                        blur.setVisibility(View.GONE);
                        scaleOutAnimationListener.onAnimationEnd();
                    }
                })
                .scaleX(0)
                .scaleY(0)
                .setInterpolator(ANTICIPATE_INTERPOLATOR)
                .setDuration(200)
                .start();
    }

    public void stopAnimation() {
        if (null == colorAnim) return;

        colorAnim.cancel();
        colorAnim.end();
    }
}
