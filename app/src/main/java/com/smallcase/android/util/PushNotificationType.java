package com.smallcase.android.util;

/**
 * Created by shashankm on 23/05/17.
 */

public class PushNotificationType {
    public static final String WEEKLY_P_N_L = "weekly_pnl";
    public static final String MARKET_OPEN = "market_open";
    public static final String SIP = "sip";
    public static final String ORDER_UPDATE = "order_update";
    public static final String REBALANCE = "rebalance";
    public static final String ALERT = "alert";
    public static final String ACCOUNT = "account";
    public static final String WATCH_LIST = "watchlist";
    public static final String INVESTMENT_DETAILS = "investment_details";
    public static final String PLAYED_OUT = "played_out";
}
