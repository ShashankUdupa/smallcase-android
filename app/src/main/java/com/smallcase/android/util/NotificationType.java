package com.smallcase.android.util;

/**
 * Created by shashankm on 25/04/17.
 */

public class NotificationType {
    public static final String ORDER_UPDATE = "ORDERUPDATE";
    public static final String MARKET_OPEN_REMINDER = "MARKETOPENREMINDER";
    public static final String REBALANCE_UPDATE = "REBALANCEUPDATE";
    public static final String PLAYED_OUT_UPDATE = "PLAYEDOUTUPDATE";
    public static final String ALERT = "ALERT";
    public static final String SIP_UPDATE = "SIPUPDATE";
}
