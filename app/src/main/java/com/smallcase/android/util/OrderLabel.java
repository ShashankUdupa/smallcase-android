package com.smallcase.android.util;

/**
 * Created by shashankm on 22/03/17.
 */

public class OrderLabel {
    public static final String BUY = "BUY";
    public static final String SELL_ALL = "SELLALL";
    public static final String SIP = "SIP";
    public static final String PARTIAL_EXIT = "PARTIALEXIT";
    public static final String RE_BALANCE = "REBALANCE";
    public static final String MANAGE = "MANAGE";
    public static final String INVEST_MORE = "INVESTMORE";
    public static final String FIX = "FIX";
}
