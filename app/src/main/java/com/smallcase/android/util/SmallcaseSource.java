package com.smallcase.android.util;

/**
 * Created by shashankm on 03/04/17.
 */

public class SmallcaseSource {
    public static final String PROFESSIONAL = "PROFESSIONAL";
    public static final String CREATED = "CREATED";
    public static final String CUSTOM = "CUSTOM";
}
