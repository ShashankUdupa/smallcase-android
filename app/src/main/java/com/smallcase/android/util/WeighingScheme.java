package com.smallcase.android.util;

/**
 * Created by shashankm on 04/10/17.
 */

public class WeighingScheme {
    public static final String EQUI_WEIGHTED = "Equi-weighted";
    public static final String MARKET_CAPPED = "Market Cap weighted";
    public static final String CUSTOM = "Custom";
}
