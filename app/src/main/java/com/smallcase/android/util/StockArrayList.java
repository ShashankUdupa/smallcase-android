package com.smallcase.android.util;

import com.smallcase.android.view.model.Stock;

import java.util.ArrayList;

/**
 * Created by shashankm on 03/05/17.
 */

public class StockArrayList<T> extends ArrayList<Stock> {
    private static final String TAG = StockArrayList.class.getSimpleName();

    @Override
    public boolean contains(Object o) {
        if (o != null && o instanceof String) {
            int size = this.size();
            for (int i = 0; i < size; i++) {
                if (this.get(i).getSid().equals(o)) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public int indexOf(Object o) {
        if (o != null && o instanceof String) {
            int size = this.size();
            for (int i = 0; i < size; i++) {
                if (this.get(i).getSid().equals(o)) {
                    return i;
                }
            }
        }

        return -1;
    }
}
