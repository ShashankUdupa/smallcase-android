package com.smallcase.android.util;

/**
 * Created by shashankm on 20/03/17.
 */

public final class OrderStatus {
    public static final String PLACED = "PLACED";
    public static final String PARTIALLY_PLACED = "PARTIALLYPLACED";
    public static final String UNPLACED = "UNPLACED";
    public static final String COMPLETED = "COMPLETED";
    public static final String PARTIALLY_FILLED = "PARTIALLYFILLED";
    public static final String UNFILLED = "UNFILLED";
    public static final String FIXED = "FIXED";
    public static final String MARKED_COMPLETE = "MARKEDCOMPLETE";
    public static final String ERROR = "ERROR";
}
