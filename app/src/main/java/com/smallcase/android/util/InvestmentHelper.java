package com.smallcase.android.util;

/**
 * Created by shashankm on 02/12/16.
 */

public class InvestmentHelper {

    public double calculateProfitOrLossPercent(double networth, double unrealizedInvestment) {
        if (unrealizedInvestment == 0) return 0;
        return ((networth - unrealizedInvestment) / unrealizedInvestment) * 100;
    }
}
