package com.smallcase.android.stock;

import com.smallcase.android.view.model.Stock;

import java.util.List;

/**
 * Created by shashankm on 07/04/17.
 */

public interface AddPopUpObservable {
    void showStockPopUp();

    void setStockList(List<Stock> stockList);

    boolean isPopUpOpen();

    void unBind();

    void hidePopUp();
}
