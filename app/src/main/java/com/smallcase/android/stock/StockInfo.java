package com.smallcase.android.stock;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.smallcase.android.R;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PStockInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by shashankm on 07/05/17.
 */

public class StockInfo implements StockHelper.StockInfoCallback {

  @BindView(R.id.text_no_data)
  GraphikText noData;
  @BindView(R.id.stock_info_sheet)
  RelativeLayout stockInfoSheet;
  @BindView(R.id.text_stock_name)
  GraphikText stockName;
  @BindView(R.id.text_ticker_name)
  GraphikText tickerName;
  @BindView(R.id.text_sector)
  GraphikText sector;
  @BindView(R.id.text_industry)
  GraphikText industry;
  @BindView(R.id.text_description)
  GraphikText description;
  @BindView(R.id.text_current_value_amount)
  GraphikText currentValueAmount;
  @BindView(R.id.image_price_up_or_down)
  ImageView priceUpOrDown;
  @BindView(R.id.text_change)
  GraphikText change;
  @BindView(R.id.text_one_month_return_value)
  GraphikText oneMonthReturnValue;
  @BindView(R.id.text_one_year_return_amount)
  GraphikText oneYearReturnValue;
  @BindView(R.id.stock_change_chart)
  LineChart stockChangeChart;
  @BindView(R.id.loading_content)
  View loadingContent;
  @BindView(R.id.info_container)
  View container;
  @BindView(R.id.blur)
  View blur;
  @BindView(R.id.fifty_two_week_high)
  GraphikText fiftyTwoWeekHigh;
  @BindView(R.id.fifty_two_week_low)
  GraphikText fiftyTwoWeekLow;
  @BindView(R.id.p_e)
  GraphikText pAndE;
  @BindView(R.id.beta)
  GraphikText beta;
  @BindView(R.id.div_yield)
  GraphikText divYield;

  private Unbinder unbinder;
  private BottomSheetBehavior stockInfoBottomSheet;
  private StockHelper stockHelper;
  private MarketRepository marketRepository;
  private SharedPrefService sharedPrefService;
  private Activity activity;

  public StockInfo(Activity activity, StockHelper stockHelper, MarketRepository marketRepository, SharedPrefService sharedPrefService) {
    unbinder = ButterKnife.bind(this, activity);

    this.activity = activity;
    this.stockHelper = stockHelper;
    this.marketRepository = marketRepository;
    this.sharedPrefService = sharedPrefService;

    stockInfoBottomSheet = BottomSheetBehavior.from(stockInfoSheet);
    stockInfoBottomSheet.setHideable(true);
    stockInfoBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
  }

  @Override
  public void onStocksInfoReceived(PStockInfo pStockInfo) {
    if (loadingContent == null) return;

    //If user has already closed the bottom sheet
    if (stockInfoBottomSheet.getState() == BottomSheetBehavior.STATE_HIDDEN) return;

    loadingContent.setVisibility(View.INVISIBLE);
    container.setVisibility(View.VISIBLE);
    stockName.setText(pStockInfo.getStockName());
    tickerName.setText(pStockInfo.getTicker());
    sector.setText(pStockInfo.getSector());
    industry.setText(pStockInfo.getIndustry());
    description.setText(pStockInfo.getDescription());
    currentValueAmount.setText(pStockInfo.getCurrentPrice());
    change.setText(pStockInfo.getChange());
    Glide.with(activity)
        .load(pStockInfo.isChangePositive() ? R.drawable.up_arrow : R.drawable.down_arrow_red)
        .asBitmap()
        .into(priceUpOrDown);
    oneMonthReturnValue.setText(pStockInfo.getOneMonthReturns());
    oneYearReturnValue.setText(pStockInfo.getOneYearReturns());

    if (pStockInfo.isChangePositive())
      change.setTextColor(ContextCompat.getColor(activity, R.color.green_600));
    else change.setTextColor(ContextCompat.getColor(activity, R.color.red_600));

    if (pStockInfo.isOneMonthPositive())
      oneMonthReturnValue.setTextColor(ContextCompat.getColor(activity, R.color.green_600));
    else oneMonthReturnValue.setTextColor(ContextCompat.getColor(activity, R.color.red_600));

    if (pStockInfo.isOneYearPositive())
      oneYearReturnValue.setTextColor(ContextCompat.getColor(activity, R.color.green_600));
    else oneYearReturnValue.setTextColor(ContextCompat.getColor(activity, R.color.red_600));

    fiftyTwoWeekHigh.setText(pStockInfo.getFiftyTwoWeekHigh());
    fiftyTwoWeekLow.setText(pStockInfo.getFiftyTwoWeekLow());
    pAndE.setText(pStockInfo.getpAndE());
    beta.setText(pStockInfo.getBeta());
    divYield.setText(pStockInfo.getDivYield());

    setUpChart(pStockInfo);
    //Recalculate height of bottom sheet
    stockInfoBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
  }

  @Override
  public void onFailedFetchingStockInfo() {
    loadingContent.setVisibility(View.INVISIBLE);
    noData.setVisibility(View.VISIBLE);
  }

  public void unBind() {
    if (unbinder != null) {
      unbinder.unbind();
      activity = null;
    }
  }

  public boolean isBottomSheetOpen() {
    return stockInfoBottomSheet.getState() == BottomSheetBehavior.STATE_EXPANDED;
  }

  public void setBottomSheetState(int state) {
    stockInfoBottomSheet.setState(state);
  }

  public void showStockInfo(String sid) {
    stockHelper.getStockInfo(marketRepository, sharedPrefService, sid, this);
    loadingContent.setVisibility(View.VISIBLE);
    container.setVisibility(View.GONE);
    if (noData.getVisibility() == View.VISIBLE) noData.setVisibility(View.GONE);

    blur.setClickable(true);
    blur.setVisibility(View.VISIBLE);
    blur.setOnClickListener(v -> stockInfoBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN));

    stockInfoBottomSheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
      @Override
      public void onStateChanged(@NonNull View bottomSheet, int newState) {
        if (blur == null) return; // Blur can be null after unbind
        if (newState == BottomSheetBehavior.STATE_HIDDEN) {
          blur.setClickable(false);
          blur.setVisibility(View.GONE);
        }
      }

      @Override
      public void onSlide(@NonNull View bottomSheet, float slideOffset) {

      }
    });

    stockInfoBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
  }

  private void setUpChart(PStockInfo pStockInfo) {
    List<Entry> entries = new ArrayList<>();
    for (int i = 0; i < pStockInfo.getStockHistoricals().size(); i++) {
      entries.add(new Entry(i, pStockInfo.getStockHistoricals().get(i).getPrice()));
    }
    LineDataSet lineDataSet = new LineDataSet(entries, "");
    lineDataSet.setDrawValues(false);
    lineDataSet.setDrawCircleHole(false);
    lineDataSet.setDrawCircles(false);
    lineDataSet.setDrawFilled(false);
    lineDataSet.setColor(ContextCompat.getColor(activity, R.color.blue_800));
    lineDataSet.setLineWidth(2f);
    lineDataSet.setHighlightEnabled(false);

    stockChangeChart.getXAxis().setDrawLabels(false);

    stockChangeChart.getAxisRight().setEnabled(false);
    stockChangeChart.getAxisLeft().setEnabled(false);
    stockChangeChart.getDescription().setEnabled(false);
    stockChangeChart.setDrawGridBackground(false);
    stockChangeChart.getAxisRight().setDrawLabels(false);
    stockChangeChart.getAxisLeft().setDrawLabels(false);
    stockChangeChart.getXAxis().setDrawAxisLine(false);
    stockChangeChart.getLegend().setEnabled(false);
    stockChangeChart.setPinchZoom(false);
    stockChangeChart.setTouchEnabled(false);
    stockChangeChart.getXAxis().setDrawGridLines(false);

    LineData data = new LineData(lineDataSet);
    stockChangeChart.setData(data);
    stockChangeChart.invalidate();
  }
}
