package com.smallcase.android.stock;

import android.support.annotation.Nullable;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.SearchResult;
import com.smallcase.android.data.model.Similar;
import com.smallcase.android.data.model.StockData;
import com.smallcase.android.data.model.StockInfo;
import com.smallcase.android.data.model.StockListWithMinAmount;
import com.smallcase.android.data.model.StockPriceAndChange;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.model.PStockInfo;
import com.smallcase.android.view.model.Stock;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.sentry.event.Breadcrumb;
import rx.Observable;

/**
 * Created by shashankm on 06/03/17.
 */

public class StockHelper {
    private static final String TAG = "StockHelper";

    public interface StockPriceCallBack {
        void onStockPricesReceived(HashMap<String, Double> stockMap);

        void onFailedFetchingStockPrice(Throwable throwable, @Nullable String sid);
    }

    public interface StockInfoCallback {
        void onStocksInfoReceived(PStockInfo pStockInfo);

        void onFailedFetchingStockInfo();
    }

    public interface StockSearchCallback {
        void onStocksReceived(List<SearchResult> searchResults);

        void onFailedFetchingStocks();

        void onFailedFetchingSimilarStocks();

        void onSimilarStocksReceived(List<Similar> similarList);
    }

    public PStockInfo createStockInfoViewModel(StockInfo stockInfo, StockPriceAndChange stockPriceAndChange) {
        PStockInfo pStockInfo = new PStockInfo();

        double stockChange = Double.valueOf(stockPriceAndChange.getChange());
        double percent = stockPriceAndChange.getClose() == 0 ? 0 : (stockChange / stockPriceAndChange.getClose()) * 100;
        String change = String.format(Locale.getDefault(), "%.2f", Math.abs(stockChange)) +
                " (" + String.format(Locale.getDefault(), "%.2f", percent) + "%)";
        String currentPrice = AppConstants.RUPEE_SYMBOL + String.format(Locale.getDefault(), "%.2f",
                stockPriceAndChange.getPrice());
        String oneMonthReturn = String.format(Locale.getDefault(), "%.2f", stockInfo.getStock()
                .getRatios().getFourwpct()) + "%";
        String oneYearReturn = String.format(Locale.getDefault(), "%.2f", stockInfo.getStock()
                .getRatios().getFiftyTwowpct()) + "%";
        String fiftyTwoWeekHigh = String.format(Locale.getDefault(), "%.1f", stockInfo.getStock()
                .getRatios().getFiftyTwowHigh());
        String fiftyTwoWeekLow = String.format(Locale.getDefault(), "%.1f", stockInfo.getStock()
                .getRatios().getFiftyTwowLow());
        String pAndE = String.format(Locale.getDefault(), "%.1f", stockInfo.getStock()
                .getRatios().getPe());
        String beta = String.format(Locale.getDefault(), "%.1f", stockInfo.getStock()
                .getRatios().getBeta());
        String divYield = String.format(Locale.getDefault(), "%.1f", stockInfo.getStock()
                .getRatios().getDivYield()) + "%";

        pStockInfo.setStockName(stockInfo.getStock().getInfo().getName());
        pStockInfo.setTicker(stockInfo.getStock().getInfo().getTicker());
        pStockInfo.setSector(stockInfo.getStock().getGic().getSector());
        pStockInfo.setIndustry(stockInfo.getStock().getGic().getIndustry());
        pStockInfo.setDescription(stockInfo.getStock().getInfo().getDescription());
        pStockInfo.setCurrentPrice(currentPrice);
        pStockInfo.setChangePositive(percent >= 0);
        pStockInfo.setChange(change);
        pStockInfo.setOneMonthReturns(oneMonthReturn);
        pStockInfo.setOneMonthPositive(stockInfo.getStock().getRatios().getFourwpct() >= 0);
        pStockInfo.setOneYearReturns(oneYearReturn);
        pStockInfo.setOneYearPositive(stockInfo.getStock().getRatios().getFiftyTwowpct() >= 0);
        pStockInfo.setStockHistoricals(stockInfo.getStockHistoricals());
        pStockInfo.setFiftyTwoWeekHigh(fiftyTwoWeekHigh);
        pStockInfo.setFiftyTwoWeekLow(fiftyTwoWeekLow);
        pStockInfo.setpAndE(pAndE);
        pStockInfo.setBeta(beta);
        pStockInfo.setDivYield(divYield);

        return pStockInfo;
    }

    /**
     * Mutates stockList given by setting shares and investment amount to every stock given.
     * This method assumes that price and weight for each Stock is already set
     *
     * @param stockList   list to be mutated
     * @param maximumPByW -1 if needs to be calculated
     * @return minimum investment amount based on the weightage scheme
     */
    public StockListWithMinAmount calculateSharesAndMinAmountFromWeight(final List<Stock> stockList, double maximumPByW) {
        List<Stock> stockListWithShares = new ArrayList<>();
        double maxPByW = maximumPByW == -1 ? calculateMaxPByW(stockList) : maximumPByW;
        double minAmount = 0.0;

        // Calculate shares for each stock and round them to whole number by determining
        // how many shares can be brought in the given price of stock so that it's equal to
        // the maxPByW calculated above.
        for (Stock stock : stockList) {
            Stock stockWithShares = new Stock(stock);
            stockWithShares.setShares(stockWithShares.getpByW() == 0? 0 : AppUtils.getInstance()
                    .round(maxPByW / stockWithShares.getpByW(), 0));
            stockWithShares.setInvestmentAmount(stockWithShares.getPrice() * stockWithShares.getShares());
            minAmount += stockWithShares.getInvestmentAmount();
            stockListWithShares.add(stockWithShares);
        }
        return new StockListWithMinAmount(stockListWithShares, minAmount, maxPByW);
    }

    public double calculateMaxPByW(List<Stock> stockList) {
        // Calculate price / weight for each stock and get the share with max p/W
        double maxPByW = 0;
        for (Stock stock : stockList) {
            double pByW = (stock.getPrice() <= 0 || stock.getWeight() <= 0)? 0 : (stock.getPrice() / stock.getWeight());
            stock.setpByW(pByW);
            if (maxPByW < pByW) maxPByW = pByW;
        }
        return maxPByW;
    }

    /**
     * Non mutating method which calculates totalInvestment amount from given shares
     *
     * @param stockList with stocks needed to invest
     * @return investmentAmount based on the current price of stock and number of shares to buy
     */
    public double calculateAmountFromShares(List<Stock> stockList) {
        double amount = 0.0;

        for (Stock stock : stockList) {
            amount += stock.getPrice() * stock.getShares();
        }
        return amount;
    }


    /**
     * Mutates stockList given by setting shares and investment amount to every stock given.
     * This method assumes that price and weight for each Stock is already set
     *
     * @param stockList     list to be mutated
     * @param desiredAmount to be invested
     * @return amount closest to desiredAmount based on the weightage scheme
     */
    public double calculateClosestInvestableAmountFromWeight(final List<Stock> stockList, final double desiredAmount) {
        double closestInvestableAmount = 0;
        // Calculate investment amount for each stock by multiplying weightage with
        // desiredAmount. This will give us the amount needed to be invested on each stock to reach
        // the desiredAmount
        for (Stock stock : stockList) {
            stock.setInvestmentAmount((stock.getWeight()) * desiredAmount);

            // Calculate shared by shares = investmentAmount / stockPrice and round them
            // to be whole numbers
            stock.setShares(AppUtils.getInstance().round(stock.getInvestmentAmount() / stock.getPrice(), 0));

            // Calculate closestInvestableAmount by taking rounded off shares into account
            closestInvestableAmount += (stock.getShares() * stock.getPrice());
        }

        return closestInvestableAmount;
    }

    /**
     * This method will accept a list of {@link Stock} objects and will transform it by setting weightage
     * of each stock based on stock's shares and price. It is important that shares and price of each stock
     * object be set before calling this method.
     * As a side effect, <code>stock.setInvestmentAmount(double)</code> will be set with shares * price
     *
     * @param stockList of Stock objects which need transformation
     */
    public void calculateWeightageFromShares(List<Stock> stockList) {
        double totalAmount = 0;

        // Calculate total amount by summing product of shares and price
        for (Stock stock : stockList) {
            double investedAmount = stock.getShares() < 0? 0 : stock.getShares() * stock.getPrice();
            stock.setInvestmentAmount(investedAmount);
            totalAmount += investedAmount;
        }

        // Calculate weightage of each stock by weightage = (invested / total)
        for (Stock stock : stockList) {
            if (stock.getInvestmentAmount() <= 0) {
                stock.setWeight(0);
                continue;
            }
            stock.setWeight((stock.getInvestmentAmount() / totalAmount));
        }
    }

    public void getStockPrices(MarketRepository marketRepository, final SharedPrefService sharedPrefService,
                               final List<String> sids, final StockPriceCallBack stockPriceCallBack) {
        if (stockPriceCallBack == null) {
            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), new Exception("stockPriceCallback not registered"), TAG + "; getStockPrices()");
            return;
        }

        StringBuilder sidsString = new StringBuilder();
        for (String sid : sids) {
            sidsString.append(sid).append(", ");
        }
        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, sidsString.toString(), null);

        marketRepository.getStocksPrice(sharedPrefService.getAuthorizationToken(), sids, sharedPrefService.getCsrfToken())
                .subscribe(responseBody -> {
                    try {
                        JSONObject response = new JSONObject(responseBody.string());
                        JSONObject data = response.getJSONObject("data");
                        // Get price of each stock
                        HashMap<String, Double> stocksMap = new HashMap<>();
                        for (String sid : sids) {
                            if (!data.has(sid)) {
                                stockPriceCallBack.onFailedFetchingStockPrice(new Exception(), sid);
                                SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Price unavailable for sid : "
                                        + sid, null);
                                continue;
                            }

                            double price = data.getDouble(sid);

                            if (price == 0) {
                                SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Price zero for sid : " + sid, null);
                            }

                            stocksMap.put(sid, price);
                        }
                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Stocks requested: " + response.toString(), null);
                        stockPriceCallBack.onStockPricesReceived(stocksMap);
                    } catch (JSONException | IOException e) {
                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.DEFAULT, "Parse error", null);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; getStockPrices()");
                        e.printStackTrace();
                        stockPriceCallBack.onFailedFetchingStockPrice(e, null);
                    }
                }, throwable -> {
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getStockPrices()");
                    stockPriceCallBack.onFailedFetchingStockPrice(throwable, null);
                });
    }

    public void getStockInfo(MarketRepository marketRepository, final SharedPrefService sharedPrefService, final String sid,
                      final StockInfoCallback stockInfoCallback) {
        String auth = sharedPrefService.getAuthorizationToken();
        String csrf = sharedPrefService.getCsrfToken();

        Observable.combineLatest(marketRepository.getStocksInfo(auth, sid, csrf), marketRepository.getStockPriceAndChange(auth,
                sid, csrf), StockData::new)
                .subscribe(stockData -> stockInfoCallback.onStocksInfoReceived(createStockInfoViewModel(stockData.getStockInfo(),
                        stockData.getStockPriceAndChange())), throwable -> {
                            HashMap<String, String> data = new HashMap<>();
                            data.put("sid", sid);
                            SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "getStockInfoAndHistorical", data);
                            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getStockInfoAndHistorical()");

                            stockInfoCallback.onFailedFetchingStockInfo();
                        });
    }

    public void searchStock(MarketRepository marketRepository, final SharedPrefService sharedPrefService, final String searchString,
                            final StockSearchCallback stockSearchCallback) {
        marketRepository.getStockInfo(sharedPrefService.getAuthorizationToken(), searchString, sharedPrefService.getCsrfToken())
                .subscribe(stockSearchCallback::onStocksReceived, throwable -> {
                    HashMap<String, String> data = new HashMap<>();
                    data.put("searchString", searchString);
                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "searchStock", data);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; searchStock()");

                    stockSearchCallback.onFailedFetchingStocks();
                });
    }

    public void getSimilarStocks(List<String> stocks, MarketRepository marketRepository, final SharedPrefService sharedPrefService,
                          final StockSearchCallback stockSearchCallback) {
        marketRepository.getSimilarStocks(sharedPrefService.getAuthorizationToken(), stocks, sharedPrefService.getCsrfToken())
                .subscribe(stockSearchCallback::onSimilarStocksReceived, throwable -> {
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getSimilarStocks()");

                    stockSearchCallback.onFailedFetchingSimilarStocks();
                });
    }
}
