package com.smallcase.android.stock;

import com.smallcase.android.view.model.Stock;

import java.util.List;

/**
 * Created by shashankm on 07/04/17.
 */

interface AddStockContract {
    interface View {
        void noSuggestionsAvailable();

        void showSnackBar(int errResId);

        void onSimilarStocksFetched(List<Stock> stockList);

        List<Stock> getStocksList();

        String getSearchString();

        void noStocks();

        void onSearchResultsReceived(List<Stock> stockList);
    }

    interface Presenter {
        void getSimilarStocks();

        void getStocks();
    }
}
