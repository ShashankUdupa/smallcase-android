package com.smallcase.android.stock;

import com.smallcase.android.R;
import com.smallcase.android.data.model.SearchResult;
import com.smallcase.android.data.model.Similar;
import com.smallcase.android.data.model.SimilarStock;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.model.Stock;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shashankm on 07/04/17.
 */

class AddStockPresenter implements AddStockContract.Presenter, StockHelper.StockSearchCallback {
    private NetworkHelper networkHelper;
    private AddStockContract.View view;
    private MarketRepository marketRepository;
    private SharedPrefService sharedPrefService;
    private StockHelper stockHelper;

    AddStockPresenter(NetworkHelper networkHelper, MarketRepository marketRepository, SharedPrefService sharedPrefService,
                      AddStockContract.View view, StockHelper stockHelper) {
        this.networkHelper = networkHelper;
        this.view = view;
        this.marketRepository = marketRepository;
        this.sharedPrefService = sharedPrefService;
        this.stockHelper = stockHelper;
    }

    @Override
    public void getSimilarStocks() {
        if (!networkHelper.isNetworkAvailable()) {
            view.showSnackBar(R.string.no_internet);
            return;
        }

        List<String> sids = new ArrayList<>();
        List<Stock> stocks = view.getStocksList();

        if (stocks == null) return;

        for (Stock stock : stocks) {
            sids.add(stock.getSid());
        }
        stockHelper.getSimilarStocks(sids, marketRepository, sharedPrefService, this);
    }

    @Override
    public void getStocks() {
        if (!networkHelper.isNetworkAvailable()) {
            view.showSnackBar(R.string.no_internet);
            return;
        }

        stockHelper.searchStock(marketRepository, sharedPrefService, view.getSearchString(), this);
    }

    @Override
    public void onSimilarStocksReceived(List<Similar> similarList) {
        if (similarList.isEmpty()) {
            view.noSuggestionsAvailable();
            return;
        }

        List<Stock> stockList = new ArrayList<>();
        for (Similar similar : similarList) {
            for (SimilarStock similarStock : similar.getStocks()) {
                Stock stock = new Stock();
                stock.setStockName(similarStock.getSidInfo().getName());
                stock.setSid(similarStock.getSid());
                stock.setShares(1);
                stock.setWeight(0);
                stockList.add(stock);
            }
        }
        view.onSimilarStocksFetched(stockList);
    }

    @Override
    public void onStocksReceived(List<SearchResult> searchResults) {
        if (searchResults.isEmpty()) {
            view.noStocks();
            return;
        }

        List<Stock> stockList = new ArrayList<>();
        for (SearchResult searchResult : searchResults) {
            Stock stock = new Stock();
            stock.setSid(searchResult.getSid());
            stock.setStockName(searchResult.getStock().getInfo().getName());
            stock.setShares(1);
            stock.setWeight(0);
            stockList.add(stock);
        }
        view.onSearchResultsReceived(stockList);
    }

    @Override
    public void onFailedFetchingStocks() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onFailedFetchingSimilarStocks() {
        view.showSnackBar(R.string.something_wrong);
    }
}
