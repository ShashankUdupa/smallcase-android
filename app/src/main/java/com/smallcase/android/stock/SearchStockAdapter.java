package com.smallcase.android.stock;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.smallcase.android.R;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.Stock;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shashankm on 27/09/17.
 */

public class SearchStockAdapter extends RecyclerView.Adapter<SearchStockAdapter.ViewHolder> {
    private List<Stock> stockList;
    private boolean isSuggestion;
    private Activity activity;
    private StockSelectedListener stockSelectedListener;

    public interface StockSelectedListener {
        void onStockSelected(Stock stock);
    }

    public SearchStockAdapter(Activity activity, boolean isSuggestion, @Nullable List<Stock> stockList,
                              StockSelectedListener stockSelectedListener) {
        this.activity = activity;
        this.isSuggestion = isSuggestion;
        this.stockList = stockList;
        this.stockSelectedListener = stockSelectedListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.stock_name, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Stock stock = stockList.get(position);

        if (isSuggestion) {
            holder.stockName.setTextColor(ContextCompat.getColor(activity, R.color.blue_800));
        } else {
            holder.stockName.setTextColor(ContextCompat.getColor(activity, R.color.primary_text));
        }

        holder.stockName.setText(stock.getStockName());
        holder.stockName.setOnClickListener(v -> stockSelectedListener.onStockSelected(stock));
    }

    @Override
    public int getItemCount() {
        if (stockList == null) return 0;
        return stockList.size();
    }

    public void destroyContext() {
        activity = null;
    }

    public void setSuggestion(boolean isSuggestion) {
        this.isSuggestion = isSuggestion;
    }

    public boolean isSuggestion() {
        return isSuggestion;
    }

    public void setStockList(List<Stock> stockList) {
        if (stockList == null || stockList.isEmpty()) return;

        if (this.stockList == null) {
            this.stockList = new ArrayList<>();
        }

        if (!this.stockList.isEmpty()) {
            this.stockList.clear();
        }
        this.stockList = stockList;
        notifyDataSetChanged();
    }

    public void clearList() {
        if (stockList == null) return;

        stockList.clear();
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.stock_name)
        GraphikText stockName;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
