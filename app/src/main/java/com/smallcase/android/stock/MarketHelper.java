package com.smallcase.android.stock;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.util.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import io.intercom.retrofit2.HttpException;
import io.sentry.event.Breadcrumb;
import okhttp3.ResponseBody;
import rx.functions.Action1;

/**
 * Created by shashankm on 05/04/17.
 */

public class MarketHelper {
    private static final String TAG = "MarketHelper";

    private SharedPrefService sharedPrefService;
    private MarketRepository marketRepository;
    private MarketCallback marketCallback;

    public interface MarketCallback {
        void onMarketStatusReceived(AppConstants.MarketStatus marketStatus);

        void onFailedFetchingMarketStatus();
    }

    public MarketHelper(SharedPrefService sharedPrefService, MarketRepository marketRepository, MarketCallback marketCallback) {
        this.sharedPrefService = sharedPrefService;
        this.marketRepository = marketRepository;
        this.marketCallback = marketCallback;
    }

    public void checkMarketStatus() {
        marketRepository.getIfMarketIsOpen(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        try {
                            JSONObject response = new JSONObject(responseBody.string());
                            AppConstants.MarketStatus marketStatus;
                            if (response.getString("data").equals("closed")) {
                                marketStatus = AppConstants.MarketStatus.CLOSED;
                            } else {
                                marketStatus = AppConstants.MarketStatus.OPEN;
                            }
                            marketCallback.onMarketStatusReceived(marketStatus);
                        } catch (JSONException | IOException e) {
                            SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.DEFAULT, "Failed parsing market status", null);
                            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; checkMarketStatus()");
                            e.printStackTrace();
                            marketCallback.onFailedFetchingMarketStatus();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        marketCallback.onFailedFetchingMarketStatus();

                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; checkMarketStatus()");
                    }
                });
    }
}
