package com.smallcase.android.stock;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.smallcase.android.data.model.SearchResult;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by shashankm on 06/04/17.
 */

public class StockSearchConverter implements JsonDeserializer<List<SearchResult>> {

    @Override
    public List<SearchResult> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return new Gson().fromJson(json.getAsJsonObject().getAsJsonObject("data").getAsJsonArray("searchResults"), typeOfT);
    }
}
