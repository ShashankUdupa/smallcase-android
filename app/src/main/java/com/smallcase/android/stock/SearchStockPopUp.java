package com.smallcase.android.stock;

import android.app.Activity;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.Stock;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by shashankm on 07/04/17.
 */

public class SearchStockPopUp implements AddStockContract.View, AddPopUpObservable, SearchStockAdapter.StockSelectedListener {
    private final static int DELAY_BEFORE_SEARCH = 800;
    private static final String TAG = "SearchStockPopUp";

    @BindView(R.id.search_pop_up) View searchPopUp;
    @BindView(R.id.search_query) EditText searchQuery;
    @BindView(R.id.suggestions_text) GraphikText suggestionsText;
    @BindView(R.id.fetching_stocks) View fetchingStocks;
    @BindView(R.id.pop_up_blur) View popUpBlur;
    @BindView(R.id.stocks_list_view) RecyclerView stocksListView;

    private Activity activity;
    private Unbinder unbinder;
    private List<Stock> stockList;
    private AddStockPopUpSubscriber addStockPopUpSubscriber;
    private AddStockContract.Presenter presenter;
    private SearchStockAdapter searchStockAdapter;
    private Handler messageHandler = new Handler();
    private Animations animations;

    public interface AddStockPopUpSubscriber {
        void onStockSelected(Stock stock);
    }

    public SearchStockPopUp(Activity activity, AddStockPopUpSubscriber addStockPopUpSubscriber) {
        this.activity = activity;
        this.addStockPopUpSubscriber = addStockPopUpSubscriber;
        unbinder = ButterKnife.bind(this, activity);

        presenter = new AddStockPresenter(new NetworkHelper(activity), new MarketRepository(), new SharedPrefService(activity),
                this, new StockHelper());
        animations = new Animations();
        fetchingStocks.setVisibility(View.VISIBLE);
        searchQuery.setTypeface(AppUtils.getInstance().getFont(activity, AppConstants.FontType.MEDIUM));
        stocksListView.setLayoutManager(new LinearLayoutManager(activity));
        stocksListView.setHasFixedSize(true);
    }

    @Override
    public void showStockPopUp() {
        animations.scaleIntoPlaceAnimation(searchPopUp);
        popUpBlur.setVisibility(View.VISIBLE);
        presenter.getSimilarStocks();
        searchStockAdapter = new SearchStockAdapter(activity, true, stockList, this);

        searchQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    messageHandler.removeCallbacks(searchRunnable);
                    if (s.length() > 1) {
                        messageHandler.postDelayed(searchRunnable, DELAY_BEFORE_SEARCH);
                    }
                } catch (Exception e) {
                    SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; showStockPopUp()");
                    e.printStackTrace();
                }
            }
        });
    }

    @OnClick(R.id.pop_up_blur)
    public void onBlurClicked() {
        hidePopUp();
    }

    @Override
    public void hidePopUp() {
        searchQuery.setText("");
        hideKeyboard();
        animations.scaleOutAnimation(searchPopUp, popUpBlur);
    }

    @Override
    public void setStockList(List<Stock> stockList) {
        fetchingStocks.setVisibility(View.GONE);
        this.stockList = new ArrayList<>(stockList);
    }

    @Override
    public boolean isPopUpOpen() {
        return searchPopUp.getVisibility() == View.VISIBLE;
    }

    private Runnable searchRunnable = new Runnable() {
        public void run() {
            presenter.getStocks();
        }
    };

    @Override
    public void unBind() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        activity = null;
    }

    @Override
    public void noSuggestionsAvailable() {
        searchStockAdapter.clearList();
    }

    @Override
    public void showSnackBar(int errResId) {

    }

    @Override
    public void onStockSelected(Stock stock) {
        hidePopUp();
        addStockPopUpSubscriber.onStockSelected(stock);
    }

    @Override
    public void onSimilarStocksFetched(List<Stock> stockList) {
        searchStockAdapter.setSuggestion(true);
        suggestionsText.setVisibility(View.VISIBLE);
        searchStockAdapter.setStockList(stockList);
        stocksListView.setAdapter(searchStockAdapter);
    }

    @Override
    public List<Stock> getStocksList() {
        return stockList;
    }

    @Override
    public String getSearchString() {
        return searchQuery.getText().toString();
    }

    @Override
    public void noStocks() {
        searchStockAdapter.clearList();
    }

    @Override
    public void onSearchResultsReceived(List<Stock> stockList) {
        searchStockAdapter.setSuggestion(false);
        suggestionsText.setVisibility(View.GONE);
        searchStockAdapter.setStockList(stockList);
    }

    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        if (null == activity.getCurrentFocus()) return;
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }
}
