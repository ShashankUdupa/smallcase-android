package com.smallcase.android.investedsmallcase.exited;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.smallcase.android.data.model.ExitedSmallcase;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by shashankm on 09/12/16.
 */

public class ExitedSmallcaseConverter implements JsonDeserializer<List<ExitedSmallcase>> {
    private static final String TAG = ExitedSmallcaseConverter.class.getSimpleName();

    @Override
    public List<ExitedSmallcase> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return new Gson().fromJson(json.getAsJsonObject().getAsJsonObject("data").get("exitedSmallcases"), typeOfT);
    }
}
