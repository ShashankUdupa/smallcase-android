package com.smallcase.android.investedsmallcase;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Constituents;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.ReBalanceUpdate;
import com.smallcase.android.data.model.SipDetails;
import com.smallcase.android.data.model.SmallcaseDetail;
import com.smallcase.android.data.model.StockListWithMinAmount;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.view.android.CustomTypefaceSpan;
import com.smallcase.android.view.model.PInvestedSmallcase;
import com.smallcase.android.view.model.PInvestedSmallcaseReturns;
import com.smallcase.android.view.model.PPlainNews;
import com.smallcase.android.view.model.PStock;
import com.smallcase.android.view.model.Stock;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;

/**
 * Created by shashankm on 14/12/16.
 */

class InvestedSmallcaseDetailPresenter implements InvestedSmallcaseDetailContract.Presenter, StockHelper.StockPriceCallBack {
    private static final String TAG = "InvestedSmallDetailPres";

    private InvestedSmallcaseDetailContract.Service service;
    private InvestedSmallcaseDetailContract.View view;
    private MarketRepository marketRepository;
    private NetworkHelper networkHelper;
    private StockHelper stockHelper;
    private List<Stock> stockList = new ArrayList<>();
    private List<Stock> wholeExitStockList = new ArrayList<>(); // Used only for wholeExit smallcase
    private AppConstants.MarketStatus marketStatus = AppConstants.MarketStatus.NOT_FETCHED;
    private boolean isValid = false, isSipDue = false, isReBalancePending = false;
    private Activity activity;
    private int reBalanceVersion = -1;
    private String reBalanceDate, iScid;
    private double maxExitAmount = -1;
    private SpannableString maxExitSpannableString;
    private long nextSip = -1;
    private String sipFrequency;

    InvestedSmallcaseDetailPresenter(Activity activity, SmallcaseRepository smallcaseRepository, InvestedSmallcaseDetailContract.View
            view, SharedPrefService sharedPrefService, MarketRepository marketRepository, NetworkHelper networkHelper, StockHelper
                                             stockHelper, UserRepository userRepository, String iScid, UserSmallcaseRepository userSmallcaseRepository) {
        this.view = view;
        this.service = new InvestedSmallcaseDetailService(sharedPrefService, smallcaseRepository, marketRepository, this,
                userRepository, userSmallcaseRepository);
        this.marketRepository = marketRepository;
        this.networkHelper = networkHelper;
        this.stockHelper = stockHelper;
        this.activity = activity;
        this.iScid = iScid;

        if ("leprechaun".equals(sharedPrefService.getUserInfo().get("brokerName"))) {
            marketStatus = AppConstants.MarketStatus.OPEN;
        } else {
            service.getIfMarketIsOpen();
        }
    }

    @Override
    public void fetchSmallcaseDetails(String scid) {
        if (!networkHelper.isNetworkAvailable()) {
            view.showSnackBar(R.string.no_internet);
            return;
        }

        if (null == service.getAuthToken()) {
            service.clearCache();
            view.redirectToLogin();
        }

        if (service.isSipDue(iScid)) {
            isSipDue = true;
            view.showSipDue();
        } else {
            isSipDue = false;
        }

        service.getSmallcaseDetail(iScid);
        service.getReBalances();
    }

    @Override
    public void onMarketStatusReceived(ResponseBody responseBody) {
        try {
            JSONObject response = new JSONObject(responseBody.string());
            if (response.getString("data").equals("closed")) {
                marketStatus = AppConstants.MarketStatus.CLOSED;
            } else {
                marketStatus = AppConstants.MarketStatus.OPEN;
            }
        } catch (JSONException | IOException e) {
            SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; onMarketStatusReceived()");
            e.printStackTrace();
            view.showSnackBar(R.string.something_wrong);
        }
    }

    @Override
    public void addReminder(String scid) {
        HashMap<String, String> body = new HashMap<>();
        body.put("scid", scid);
        body.put("action", "BUY");
        service.addMarketOpenReminder(body);
    }

    @Override
    public boolean isReBalancePending() {
        return isReBalancePending;
    }

    @Override
    public int getReBalanceVersion() {
        return reBalanceVersion;
    }

    @Override
    public void destroySubscriptions() {
        service.destroySubscriptions();
    }

    @Override
    public String getReBalanceDate() {
        return reBalanceDate;
    }

    @Override
    public void onReBalancesReceived(List<ReBalanceUpdate> reBalanceUpdates) {
        isReBalancePending = false;
        for (ReBalanceUpdate reBalanceUpdate : reBalanceUpdates) {
            if (reBalanceUpdate.getIscid() != null && reBalanceUpdate.getIscid().equals(iScid)) {
                isReBalancePending = true;
                SimpleDateFormat sdf = new SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault());
                String date = sdf.format(AppUtils.getInstance().getPlainDate(reBalanceUpdate.getDate()));

                reBalanceVersion = reBalanceUpdate.getVersion();
                reBalanceDate = reBalanceUpdate.getDate();
                view.showReBalanceAvailable(date, reBalanceUpdate.getLabel());
                break;
            }
        }
    }

    @Override
    public void onFailedFetchingReBalanceUpdates() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void checkMarketStatus() {
        switch (marketStatus) {
            case OPEN:
                view.marketOpen();
                break;

            case CLOSED:
                view.showSnackBar(R.string.market_closed_now);
                view.hideExitCard();
                break;

            case NOT_FETCHED:
                service.getIfMarketIsOpen();
                view.showSnackBar(R.string.something_wrong);
                break;
        }
    }

    @Override
    public boolean isSmallcaseValid() {
        return isValid;
    }

    @Override
    public List<PStock> getSortedStocks(List<PStock> stockList, boolean onPAndL, int numberOfStocks) {
        List<PStock> sortedStocks = new ArrayList<>(stockList);
        Collections.sort(sortedStocks, onPAndL ? getPAndLComparator() : getWeightageComparator());
        return sortedStocks.subList(0, sortedStocks.size() < 6 ? sortedStocks.size() : numberOfStocks);
    }

    @Override
    public List<Stock> getStockList() {
        return stockList;
    }

    @Override
    public void onFailedFetchingMarketStatus() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void getStockPricesToExitSmallcase() {
        List<String> sids = new ArrayList<>();
        for (Stock stock : stockList) {
            sids.add(stock.getSid());
        }
        stockHelper.getStockPrices(marketRepository, service.getSharedPrefService(), sids, this);
    }

    // Call back after fetching stock prices to exit smallcase
    @Override
    public void onStockPricesReceived(HashMap<String, Double> stockMap) {
        double totalAmount = 0;

        // Get price of each stock
        for (int i = 0; i < wholeExitStockList.size(); i++) {
            Stock stock = wholeExitStockList.get(i);
            double price = stockMap.get(stock.getSid());
            stock.setPrice(price);
            totalAmount += (price * stock.getShares());
            if (stockList.get(i) != null) stockList.get(i).setPrice(price);
        }

        stockHelper.calculateWeightageFromShares(stockList);
        stockHelper.calculateWeightageFromShares(wholeExitStockList);

        double minAmount = stockHelper.calculateSharesAndMinAmountFromWeight(wholeExitStockList, -1).getMinAmount();
        maxExitAmount = totalAmount - minAmount;
        showMaxExitAmount(totalAmount);
    }

    private void showMaxExitAmount(double totalAmount) {
        String maxExitAmountString = AppConstants.getDecimalNumberFormatter().format(maxExitAmount);
        maxExitSpannableString = new SpannableString("You can sell upto " + AppConstants.RUPEE_SYMBOL + maxExitAmountString);
        maxExitSpannableString.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(activity, AppConstants.FontType.MEDIUM)),
                16, maxExitSpannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        if (AppUtils.getInstance().isZero(maxExitAmount)) {
            view.noPartialExitAllowed(totalAmount);
            return;
        }

        view.showExitSmallcaseAmount(totalAmount);
    }

    @Override
    public void onFailedFetchingStockPrice(Throwable throwable, @Nullable String sid) {
        view.showSnackBar(R.string.something_wrong);
    }

    @NonNull
    private Comparator<PStock> getWeightageComparator() {
        return (o1, o2) -> Double.compare(o2.getStockWeightage(), o1.getStockWeightage());
    }

    @NonNull
    private Comparator<PStock> getPAndLComparator() {
        return (o1, o2) -> Double.compare(o2.getStockPAndL(), o1.getStockPAndL());
    }

    @Override
    public void onSmallcaseDetailsReceived(SmallcaseDetail smallcaseDetail, ResponseBody responseBody) {
        try {
            createStockViewModel(smallcaseDetail, responseBody);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            view.showSnackBar(R.string.something_wrong);
        }
        SpannableString spannableString;
        switch (smallcaseDetail.getStatus()) {
            case "VALID":
                view.enableReBalance();
                isValid = true;
                break;

            case "INVALID":
                view.disableActions();
                String notFilledString = "Latest batch is not filled. ";
                spannableString = new SpannableString(notFilledString + "Repair/Archive");
                spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.blue_800)), notFilledString.length(),
                        spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                view.showNegativeContainer(spannableString, null, true);
                break;

            case "PLACED":
                spannableString = new SpannableString("Awaiting order status");
                view.disableActions();
                view.showNegativeContainer(spannableString, null, false);
                break;

            default:
                spannableString = new SpannableString("Awaiting order status");
                view.disableActions();
                view.showNegativeContainer(spannableString, null, false);
                break;
        }

        if (isValid && smallcaseDetail.getFlags() != null && smallcaseDetail.getFlags().isSip()) {
            view.hideStartSipButton();

            if (!isSipDue) {
                service.getSipDetails(iScid);
            }
        } else {
            view.showStartSipButton();
            view.hideSipCard();
        }

        createSmallcaseInfo(smallcaseDetail);
        createInvestedSmallcaseReturnsPAndL(smallcaseDetail);
    }

    private void createSmallcaseInfo(SmallcaseDetail smallcaseDetail) {
        String index = String.format(Locale.getDefault(), "%.2f", smallcaseDetail.getStats().getIndexValue());
        boolean isIndexUp = smallcaseDetail.getStats().getIndexValue() > smallcaseDetail.getStats().getLastCloseIndex();

        PInvestedSmallcase investedSmallcase = new PInvestedSmallcase(smallcaseDetail.getName(), smallcaseDetail.getSource(), "80",
                index, isIndexUp, null, null, false, smallcaseDetail.getScid(), smallcaseDetail.getDate(), smallcaseDetail.get_id());
        view.onSmallcaseDataReceived(investedSmallcase);
    }

    @Override
    public void onFailedFetchingSmallcaseDetails() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onSmallcaseNewsReceived(List<News> newses) {
        if (newses == null || newses.isEmpty()) {
            view.noNewsForSmallcase();
            return;
        }
        createNewsViewModel(newses);
    }

    @Override
    public void onFailedFetchingSmallcaseNews() {
        view.failedFetchingNews();
    }

    private void createNewsViewModel(List<News> newses) {
        List<PPlainNews> newsList = new ArrayList<>();
        for (News news : newses) {
            PPlainNews plainNews = new PPlainNews();
            plainNews.setLink(news.getLink());
            plainNews.setNewsHeading(news.getHeadline());
            plainNews.setNewsImage(news.getImageUrl());
            plainNews.setDate(AppUtils.getInstance().getTimeAgoDate(news.getDate()));
            newsList.add(plainNews);
        }
        view.onNewsReceived(newsList);
    }

    private void createStockViewModel(SmallcaseDetail smallcaseDetail, ResponseBody responseBody) throws IOException, JSONException {
        if (smallcaseDetail.getCurrentConfig().getConstituents().isEmpty()) {
            view.onStocksReceived(Collections.<PStock>emptyList());
            return;
        }

        List<String> sids = new ArrayList<>();
        List<String> scids = new ArrayList<>();

        List<Constituents> constituentsList = smallcaseDetail.getCurrentConfig().getConstituents();
        List<PStock> pStocks = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(responseBody.string());
        JSONObject stockPriceObject = jsonObject.getJSONObject("data");
        double netWorth = smallcaseDetail.getReturns().getNetworth();
        boolean isListEmpty = stockList.isEmpty();
        HashMap<String, Double> sharesMap = new HashMap<>();
        for (Constituents constituents : constituentsList) {
            double stockCurrentValue = stockPriceObject.getDouble(constituents.getSid());
            double stockTotalValue = constituents.getShares() * stockCurrentValue;
            double pAndLPercent = ((stockCurrentValue - constituents.getAveragePrice()) / constituents.getAveragePrice()) * 100;
            double weightage = ((netWorth - stockTotalValue) / netWorth) * 100;
            String averagePrice = AppConstants.RUPEE_SYMBOL + String.format(Locale.getDefault(), "%.2f", constituents.getAveragePrice());
            String currentValue = AppConstants.RUPEE_SYMBOL + String.format(Locale.getDefault(), "%.2f", stockCurrentValue);
            boolean isProfit = pAndLPercent >= 0;

            PStock pStock = new PStock();
            pStock.setProfit(isProfit);
            pStock.setStockName(constituents.getSidInfo().getName());
            pStock.setStockPAndL(pAndLPercent);
            pStock.setSid(constituents.getSid());
            pStock.setStockWeightage(100 - weightage);
            pStock.setHeading(false);
            pStock.setShares(constituents.getShares());
            pStock.setAverageBuyPrice(averagePrice);
            pStock.setCurrentPrice(currentValue);
            pStock.setExpanded(false);
            pStocks.add(pStock);

            if (!SmallcaseSource.PROFESSIONAL.equals(smallcaseDetail.getSource())) sids.add(constituents.getSid());

            if (isListEmpty) {
                Stock stock = new Stock();
                stock.setShares(constituents.getShares());
                stock.setWeight(constituents.getWeight());
                stock.setStockName(constituents.getSidInfo().getName());
                stock.setSid(constituents.getSid());
                stockList.add(stock);
                wholeExitStockList.add(new Stock(stock));
            } else {
                sharesMap.put(constituents.getSid(), constituents.getShares());
            }
        }

        if (SmallcaseSource.PROFESSIONAL.equals(smallcaseDetail.getSource())) scids.add(smallcaseDetail.getScid());
        service.getSmallcaseNews(scids, sids);

        if (!isListEmpty) {
            for (Stock stock : stockList) {
                stock.setShares(sharesMap.get(stock.getSid()) == null? 0 : sharesMap.get(stock.getSid()));
            }
        }

        view.onStocksReceived(pStocks);
    }

    private void createInvestedSmallcaseReturnsPAndL(SmallcaseDetail smallcaseDetail) {
        double netWorth = smallcaseDetail.getReturns().getNetworth();
        String networth = AppConstants.RUPEE_SYMBOL + (netWorth == 0 ? AppConstants.HYPHEN_SYMBOL : AppConstants.getNumberFormatter().format(netWorth));
        double unRealized = netWorth - smallcaseDetail.getReturns().getUnrealizedInvestment();
        String unRealizePAndL = AppConstants.RUPEE_SYMBOL + (unRealized == 0 ? AppConstants.HYPHEN_SYMBOL : AppConstants.getNumberFormatter().format(unRealized));
        boolean isUnRealizedProfit = unRealized >= 0;
        String unRealizedPercent = unRealized == 0 ? (AppConstants.HYPHEN_SYMBOL + "%") : String.format(Locale.getDefault(), "%.2f",
                (unRealized / smallcaseDetail.getReturns().getUnrealizedInvestment()) * 100) + "%";

        String currentInvestment = AppConstants.RUPEE_SYMBOL + (smallcaseDetail.getReturns().getUnrealizedInvestment() == 0 ?
                AppConstants.HYPHEN_SYMBOL : AppConstants.getNumberFormatter().format(smallcaseDetail.getReturns().getUnrealizedInvestment()));

        boolean isRealizedProfit = smallcaseDetail.getReturns().getRealizedReturns() >= 0;

        String realizedPAndLAndDividends = "0";
        if (smallcaseDetail.getReturns().getRealizedReturns() != 0) {
            realizedPAndLAndDividends = AppConstants.RUPEE_SYMBOL + AppConstants.getNumberFormatter().format(smallcaseDetail
                    .getReturns().getRealizedReturns());
        }

        String dividend = " + " + AppConstants.RUPEE_SYMBOL + AppConstants.getNumberFormatter().format(smallcaseDetail
                .getReturns().getDivReturns());

        double moneyPutIn = smallcaseDetail.getReturns().getUnrealizedInvestment() + smallcaseDetail
                .getReturns().getRealizedInvestment();
        String totalInvestment = AppConstants.RUPEE_SYMBOL + (moneyPutIn == 0 ? AppConstants.HYPHEN_SYMBOL : AppConstants.getNumberFormatter().format(moneyPutIn));
        double pAndLSum = unRealized + smallcaseDetail.getReturns().getRealizedReturns() + smallcaseDetail.getReturns().getDivReturns();
        String totalPAndL = AppConstants.RUPEE_SYMBOL + (pAndLSum == 0 ? AppConstants.HYPHEN_SYMBOL : AppConstants.getNumberFormatter().format(pAndLSum));
        boolean isTotalProfit = (unRealized + smallcaseDetail.getReturns().getRealizedReturns() + smallcaseDetail.getReturns().getDivReturns()) >= 0;

        view.onSmallcaseReturnsReceived(new PInvestedSmallcaseReturns(networth, unRealizedPercent,
                unRealizePAndL, isUnRealizedProfit, currentInvestment, realizedPAndLAndDividends, isRealizedProfit, totalInvestment,
                totalPAndL, isTotalProfit, dividend));
    }

    @Override
    public SpannableString getMaxExitSpannableString() {
        return maxExitSpannableString;
    }

    @Override
    public double getMaxExitAmount() {
        return maxExitAmount;
    }

    @Override
    public void onConfirmExitClicked(final double exitAmountEntered) {
        Collections.sort(stockList, getAscendingComparator());
        Collections.sort(wholeExitStockList, getAscendingComparator());

        if ((exitAmountEntered - maxExitAmount) >= 0.1) {
            SpannableString spannableString = new SpannableString("Enter an amount lesser than \u20B9 " + AppConstants.getDecimalNumberFormatter()
                    .format(maxExitAmount) + " to maintain the weighting scheme of your smallcase");
            view.showInvalidAmountError(spannableString);
            return;
        }

        if (stockList.size() > 0 && stockList.get(0).getPrice() > exitAmountEntered) {
            SpannableString spannableString = new SpannableString("No stocks found to sell for the amount entered. Try increasing the amount");
            view.showInvalidAmountError(spannableString);
            return;
        }

        // Calculate stocks and shares that need to be sold
        double desiredAmountToExit, amountRemaining;
        double netWorth = stockHelper.calculateAmountFromShares(wholeExitStockList);
        if (AppUtils.getInstance().isDoubleEqual(maxExitAmount, exitAmountEntered, 1)) {
            desiredAmountToExit = stockHelper.calculateMaxPByW(stockList);
            StockListWithMinAmount stockListWithMinAmount = stockHelper.calculateSharesAndMinAmountFromWeight(stockList, desiredAmountToExit);
            amountRemaining = stockListWithMinAmount.getMinAmount();
            stockList = stockListWithMinAmount.getStockList();
        } else {
            desiredAmountToExit = netWorth - exitAmountEntered;
            amountRemaining = stockHelper.calculateClosestInvestableAmountFromWeight(stockList, desiredAmountToExit);
        }

        double calculatedExitAmount = netWorth - amountRemaining;

        double exitRangeMaximum = exitAmountEntered * 1.05;
        double exitRangeMinimum = exitAmountEntered * 0.95;

        int listSize = stockList.size();
        if (calculatedExitAmount < exitRangeMinimum) {
            for (int i = 0; i < listSize; i++) {
                Stock stock = stockList.get(i);
                double newAmount = calculatedExitAmount + stock.getPrice();
                if (newAmount > exitRangeMinimum && newAmount < exitRangeMaximum) {
                    // If amount is in the min, max exit range then break
                    stock.setShares(stock.getShares() - 1);
                    break;
                } else if (exitRangeMinimum > newAmount) {
                    // Need to add more stocks
                    stock.setShares(stock.getShares() - 1);
                    calculatedExitAmount = newAmount;
                } else if (newAmount > exitRangeMaximum) {
                    if (i == 0) break;
                    // Start from beginning as this stock exceeds max range
                    i = -1;
                } else {
                    // Should not come here, but meh!
                    break;
                }
            }
        } else if (calculatedExitAmount > exitRangeMaximum) {
            for (int i = 0; i < listSize; i++) {
                Stock stock = stockList.get(i);
                double newAmount = calculatedExitAmount - stock.getPrice();
                if (newAmount > exitRangeMinimum && newAmount < exitRangeMaximum) {
                    // If amount is in the min, max exit range then break
                    stock.setShares(stock.getShares() + 1);
                    break;
                } else if (newAmount > exitRangeMaximum) {
                    // Need to remove more stocks
                    stock.setShares(stock.getShares() + 1);
                    calculatedExitAmount = newAmount;
                } else if (exitRangeMinimum > newAmount) {
                    if (i == 0) break;
                    // Start from beginning as this stock falls below min range
                    i = -1;
                } else {
                    // Should not come here, but meh! ( Yeah I copy past comments as well 8-) )
                    break;
                }
            }
        }

        double finalAmount = 0;
        List<Stock> stocksToSell = new ArrayList<>();
        try {
            for (int i = 0; i < stockList.size(); i++) {
                Stock modifiedStock = stockList.get(i);
                Stock existingStock = wholeExitStockList.get(i);
                int shares = (int) (existingStock.getShares() - modifiedStock.getShares());
                if (shares != 0) {
                    finalAmount += (modifiedStock.getPrice() * shares);
                    modifiedStock.setShares(shares);
                    modifiedStock.setBuy(false);
                    stocksToSell.add(modifiedStock);
                }
            }
        } catch (ArrayIndexOutOfBoundsException | NullPointerException e) {
            view.showSnackBar(R.string.something_wrong);
            SentryAnalytics.getInstance().captureEvent(service.getAuthToken(), e, TAG + "; onConfirmExitClicked()");
        }

        view.continuePartialExit(finalAmount, stocksToSell);
    }

    @Override
    public void onWholeExitClicked() {
        view.exitWhole(wholeExitStockList, stockHelper.calculateAmountFromShares(wholeExitStockList));
    }

    @Override
    public void onFailedFetchingSipDetails() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onSipDetailsFetched(SipDetails sipDetails) {
        view.hideLoader();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault());
        switch (sipDetails.getFrequency()) {
            case "7d":
                sipFrequency = "weekly";
                break;

            case "14d":
                sipFrequency = "fortnightly";
                break;

            case "1m":
                sipFrequency = "monthly";
                break;

            case "3m":
                sipFrequency = "quarterly";
                break;

            default:
                sipFrequency = "";
                break;
        }

        nextSip = AppUtils.getInstance().getPlainDate(sipDetails.getScheduledDate()).getTime();
        String date = simpleDateFormat.format(AppUtils.getInstance().getPlainDate(sipDetails.getScheduledDate()));
        SpannableString sipString = new SpannableString("You have an active " + sipFrequency + " SIP for this smallcase, next due on "
                + date);
        sipString.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(activity, AppConstants.FontType.MEDIUM)),
                19, 19 + sipFrequency.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        sipString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primary_text)),
                19, 19 + sipFrequency.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        sipString.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(activity, AppConstants.FontType.MEDIUM)),
                sipString.length() - date.length(), sipString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        sipString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, R.color.primary_text)),
                sipString.length() - date.length(), sipString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        view.showSipDetails(sipString);
    }

    @Override
    public boolean isSipDue() {
        return isSipDue;
    }

    @NonNull
    private Comparator<Stock> getAscendingComparator() {
        return (stock1, stock2) -> (int) (stock1.getPrice() - stock2.getPrice());
    }

    @Override
    public long getNextSip() {
        return nextSip;
    }

    @Override
    public String getSipFrequency() {
        return sipFrequency;
    }

    @Override
    public void onSipSkippedSuccessful() {
        view.showSnackBar(R.string.sip_skipped_successfully);
        isSipDue = false;
        service.getSipDetails(iScid);
    }

    @Override
    public void onFailedSkippingSip() {
        view.hideLoader();
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void skipSip(HashMap<String, String> body) {
        service.skipSip(body);
    }
}
