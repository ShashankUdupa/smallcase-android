package com.smallcase.android.investedsmallcase.sip;

import android.util.Log;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Historical;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.sentry.event.Breadcrumb;
import retrofit2.adapter.rxjava.HttpException;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 14/09/17.
 */

class SetUpSipService implements SetUpSipContract.Service {
    private static final String TAG = "SetUpSipService";

    private UserSmallcaseRepository userSmallcaseRepository;
    private SharedPrefService sharedPrefService;
    private CompositeSubscription compositeSubscription;
    private SetUpSipContract.Presenter presenter;
    private SmallcaseRepository smallcaseRepository;
    private MarketRepository marketRepository;

    SetUpSipService(UserSmallcaseRepository userSmallcaseRepository, SharedPrefService sharedPrefService, SetUpSipContract
            .Presenter presenter, SmallcaseRepository smallcaseRepository, MarketRepository marketRepository) {
        this.userSmallcaseRepository = userSmallcaseRepository;
        this.sharedPrefService = sharedPrefService;
        this.presenter = presenter;
        this.smallcaseRepository = smallcaseRepository;
        this.marketRepository = marketRepository;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void setUpSip(final HashMap<String, String> body) {
        compositeSubscription.add(userSmallcaseRepository.setUpSip(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), body)
                .subscribe(responseBody -> {
                    try {
                        JSONObject response = new JSONObject(responseBody.string());
                        if (response.getBoolean("success")) {
                            presenter.onSipSetUpSuccessful();
                        } else {
                            presenter.onFailedSettingUpSip();
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed setting up sip", body);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; setUpSip()");
                    }
                }, throwable -> {
                    Log.d(TAG, "call: " + throwable.toString());
                    presenter.onFailedSettingUpSip();

                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed setting up sip", body);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; setUpSip()");
                }));
    }

    @Override
    public void getSmallcaseHistorical(String scid, String duration) {
        final Map<String, String> data = new HashMap<>();
        data.put("scid", scid);
        data.put("duration", duration);
        compositeSubscription.add(smallcaseRepository.getSmallcaseHistorical(sharedPrefService.getAuthorizationToken(), scid, null,
                null, duration, sharedPrefService.getCsrfToken())
                .subscribe(new Action1<Historical>() {
                    @Override
                    public void call(Historical historical) {
                        presenter.onSmallcaseHistoricalReceived(historical);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        presenter.onFailedFetchingSmallcaseHistorical();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "State", data);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getSmallcaseHistorical()");
                    }
                }));
    }

    @Override
    public void getStockHistorical(final List<String> stocks, final String duration) {
        HashMap<String, String> data = new HashMap<>();
        for (String stock : stocks) {
            data.put("sid", stock);
        }
        data.put("duration", duration);
        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "State", data);
        compositeSubscription.add(marketRepository.getStockHistorical(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), stocks, duration)
                .subscribe(responseBody -> {
                    try {
                        presenter.onStockHistoricalFetched(responseBody);
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; getStockHistorical()");
                    }
                }, throwable -> {
                    presenter.onFailedFetchingStockHistorical();

                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getStockHistorical()");
                }));
    }


    @Override
    public void destroySubscribers() {
        if (compositeSubscription != null) compositeSubscription.unsubscribe();
    }
}
