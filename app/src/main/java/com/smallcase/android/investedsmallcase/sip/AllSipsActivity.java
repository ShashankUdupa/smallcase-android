package com.smallcase.android.investedsmallcase.sip;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.SipDetails;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.investedsmallcase.InvestedSmallcaseDetailActivity;
import com.smallcase.android.onboarding.LandingActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.view.android.GraphikText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;


public class AllSipsActivity extends AppCompatActivity implements AllSipsContract.View {
    private static final String TAG = "AllSipsActivity";

    @BindView(R.id.sip_remainder_list) RecyclerView sipsReminderList;
    @BindView(R.id.text_tool_bar_title) GraphikText toolBarTitle;
    @BindView(R.id.empty_state_container) View noRemindersContainer;
    @BindView(R.id.text_empty_state) GraphikText noRemindersText;
    @BindView(R.id.loading_indicator) View loadingIndicator;
    @BindView(R.id.action_icon) ImageView actionIcon;
    @BindView(R.id.parent) View parent;
    @BindView(R.id.markets_closed) View marketClosed;

    private List<SipDetails> sips;
    private SipAdapter sipAdapter;
    private AllSipsContract.Service service;
    private AppConstants.MarketStatus marketStatus = AppConstants.MarketStatus.NOT_FETCHED;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_sips);

        ButterKnife.bind(this);

        toolBarTitle.setText("SIP Instalments");

        SharedPrefService sharedPrefService = new SharedPrefService(this);
        service = new AllSipsService(new UserRepository(), sharedPrefService, this, new MarketRepository());

        if ("leprechaun".equals(sharedPrefService.getUserInfo().get("brokerName"))) {
            marketStatus = AppConstants.MarketStatus.OPEN;
        } else {
            service.getMarketStatus();
        }
        service.getBuyReminders();
    }

    @Override
    public void onSipsFetched(List<SipDetails> sips) {
        if (sips.isEmpty()) {
            showNoReminders();
            return;
        }

        this.sips = sips;
        setUpAdapter();
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if (service != null) service.destroySubscriptions();
        super.onDestroy();
    }

    @Override
    public void redirectUserToLogin() {
        startActivity(new Intent(this, LandingActivity.class));
        finish();
    }

    @Override
    public void onFailedFetchingSipUpdates() {
        if (!isFinishing()) showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onMarketStatusReceived(ResponseBody responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody.string());
            if (jsonObject.getString("data").equals("closed")) {
                marketStatus = AppConstants.MarketStatus.CLOSED;
                marketClosed.setVisibility(View.VISIBLE);
            } else {
                marketStatus = AppConstants.MarketStatus.OPEN;
                marketClosed.setVisibility(View.GONE);
            }

            if (sipAdapter != null) {
                sipAdapter.notifyDataSetChanged();
            }

        } catch (JSONException | IOException e) {
            e.printStackTrace();
            SentryAnalytics.getInstance().captureEvent(null, e, TAG + "; onMarketStatusReceived()");

            showSnackBar(R.string.something_wrong);
        }
    }

    @Override
    public void onFailedFetchingMarketStatus() {
        if (!isFinishing()) showSnackBar(R.string.something_wrong);
    }

    private void setUpAdapter() {
        loadingIndicator.setVisibility(View.GONE);
        sipsReminderList.setLayoutManager(new LinearLayoutManager(this));
        sipAdapter = new SipAdapter();
        sipsReminderList.setAdapter(sipAdapter);
    }

    private void showSnackBar(@StringRes int resId) {
        AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_INDEFINITE, resId);
    }

    private void showNoReminders() {
        loadingIndicator.setVisibility(View.INVISIBLE);
        sipsReminderList.setVisibility(View.GONE);

        noRemindersContainer.setVisibility(View.VISIBLE);
        noRemindersText.setText("All caught up\n\nYou can set up sips for any smallcase");
    }

    class SipAdapter extends RecyclerView.Adapter<SipAdapter.ViewHolder> {
        private final String TAG = "BuyAdapter";

        @Override
        public SipAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.sip_card, parent, false));
        }

        @Override
        public void onBindViewHolder(SipAdapter.ViewHolder holder, int position) {
            final SipDetails sipDetails = sips.get(position);

            if (position == sips.size() - 1) {
                holder.smallcaseDivider.setVisibility(View.GONE);
            } else {
                holder.smallcaseDivider.setVisibility(View.VISIBLE);
            }

            if (SmallcaseSource.PROFESSIONAL.equals(sipDetails.getSource())) {
                Glide.with(AllSipsActivity.this)
                        .load(AppConstants.SMALLCASE_IMAGE_URL + "80/" + sipDetails.getScid() + ".png")
                        .into(holder.smallcaseImage);
            } else if (SmallcaseSource.CREATED.equals(sipDetails.getSource())) {
                Glide.with(AllSipsActivity.this)
                        .load(AppConstants.CREATED_SMALLCASE_IMAGE_URL + "80/" + sipDetails.getScid() + ".png")
                        .into(holder.smallcaseImage);
            }

            if (SmallcaseSource.CUSTOM.equals(sipDetails.getSource())) {
                holder.cusomTag.setVisibility(View.VISIBLE);
            } else {
                holder.cusomTag.setVisibility(View.GONE);
            }

            holder.smallcaseName.setText(sipDetails.getName());
            if (marketStatus == AppConstants.MarketStatus.OPEN) {
                holder.investMoreCard.setClickable(true);
                holder.investMoreCard.setCardBackgroundColor(ContextCompat.getColor(AllSipsActivity.this, R.color.green_800));

                holder.investMoreCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(AllSipsActivity.this, InvestedSmallcaseDetailActivity.class);
                        intent.putExtra(InvestedSmallcaseDetailActivity.SCID, sipDetails.getScid());
                        intent.putExtra(InvestedSmallcaseDetailActivity.ISCID, sipDetails.getIscid());
                        intent.putExtra(InvestedSmallcaseDetailActivity.SOURCE, sipDetails.getSource());
                        intent.putExtra(InvestedSmallcaseDetailActivity.OPEN_INVEST_MORE, true);
                        startActivity(intent);
                    }
                });
            } else {
                holder.investMoreCard.setClickable(false);
                holder.investMoreCard.setCardBackgroundColor(ContextCompat.getColor(AllSipsActivity.this, R.color.blue_grey_100));
            }

            holder.smallcaseTitleCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AllSipsActivity.this, InvestedSmallcaseDetailActivity.class);
                    intent.putExtra(InvestedSmallcaseDetailActivity.SCID, sipDetails.getScid());
                    intent.putExtra(InvestedSmallcaseDetailActivity.ISCID, sipDetails.getIscid());
                    intent.putExtra(InvestedSmallcaseDetailActivity.SOURCE, sipDetails.getSource());
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return sips.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.smallcase_name) GraphikText smallcaseName;
            @BindView(R.id.smallcase_image) ImageView smallcaseImage;
            @BindView(R.id.invest_more_card) CardView investMoreCard;
            @BindView(R.id.smallcase_divider) View smallcaseDivider;
            @BindView(R.id.smallcase_title_card) CardView smallcaseTitleCard;
            @BindView(R.id.custom_tag) View cusomTag;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);

                if (Build.VERSION.SDK_INT < 21) {
                    smallcaseTitleCard.setCardElevation(0);
                    smallcaseTitleCard.setMaxCardElevation(0);
                }
            }
        }
    }
}
