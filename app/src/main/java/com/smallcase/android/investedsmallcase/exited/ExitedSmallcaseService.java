package com.smallcase.android.investedsmallcase.exited;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.repository.InvestmentRepository;
import com.smallcase.android.data.repository.SharedPrefService;

import io.intercom.retrofit2.HttpException;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 03/04/17.
 */

class ExitedSmallcaseService implements ExitedSmallcasesContract.Service {
    private static final String TAG = "ExitedSmallcaseService";

    private InvestmentRepository investmentRepository;
    private SharedPrefService sharedPrefService;
    private ExitedSmallcasesContract.Presenter presenter;
    private CompositeSubscription compositeSubscription;

    ExitedSmallcaseService(InvestmentRepository investmentRepository, SharedPrefService sharedPrefService, ExitedSmallcasesContract
            .Presenter presenter) {
        this.investmentRepository = investmentRepository;
        this.sharedPrefService = sharedPrefService;
        this.presenter = presenter;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getExitedSmallcases() {
        compositeSubscription.add(investmentRepository.getExitedSmallcases(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(exitedSmallcases -> presenter.onExitedSmallcasesReceived(exitedSmallcases), throwable -> {
                    presenter.onFailedFetchingExitedSmallcases();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;


                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getExitedSmallcases()");
                }));
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }
}
