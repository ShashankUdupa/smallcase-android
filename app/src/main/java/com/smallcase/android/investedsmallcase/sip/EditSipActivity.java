package com.smallcase.android.investedsmallcase.sip;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RadioGroup;

import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.CustomTypefaceSpan;
import com.smallcase.android.view.android.GraphikText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class EditSipActivity extends AppCompatActivity implements EditSipContract.View, Animations.ScaleOutAnimationListener {
    public static final String SCID = "scid";
    public static final String ISCID = "iScid";
    public static final String FREQUENCY = "frequency";
    public static final String NEXT_SIP = "next_sip";
    public static final String SMALLCASE_NAME = "smallcase_name";
    public static final String SOURCE = "source";

    private static final String TAG = "EditSipActivity";

    @BindView(R.id.frequency)
    GraphikText frequency;
    @BindView(R.id.next_sip)
    GraphikText nextSip;
    @BindView(R.id.date_selected)
    GraphikText dateSelected;
    @BindView(R.id.sip_frequency_container)
    RadioGroup sipFrequencyContainer;
    @BindView(R.id.save_frequency)
    View saveFrequency;
    @BindView(R.id.date_selection_layout)
    View dateSelectionLayout;
    @BindView(R.id.date_divider)
    View dateDivider;
    @BindView(R.id.sip_date_description)
    View sipDateDescription;
    @BindView(R.id.save_investment_date)
    View saveInvestmentDate;
    @BindView(R.id.text_tool_bar_title)
    GraphikText toolBarTitle;
    @BindView(R.id.parent)
    View parent;
    @BindView(R.id.every_week)
    AppCompatRadioButton everyWeek;
    @BindView(R.id.every_two_weeks)
    AppCompatRadioButton everyTwoWeeks;
    @BindView(R.id.every_month)
    AppCompatRadioButton everyMonth;
    @BindView(R.id.every_quarter)
    AppCompatRadioButton everyQuarter;
    @BindView(R.id.end_sip_blur)
    View endSipBlur;
    @BindView(R.id.dialog_confirmation)
    View dialogConfirmation;
    @BindView(R.id.edit_sip_date)
    View editSipDate;
    @BindView(R.id.edit_frequency)
    View editFrequency;

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
    private final AccelerateDecelerateInterpolator accelerateDecelerateInterpolator = new AccelerateDecelerateInterpolator();

    private Calendar calendar;
    private DatePickerDialog datePickerDialog;
    private String scid, iScid, previousFrequency, previousDate;
    private EditSipContract.Service service;
    private ProgressDialog progressDialog;
    private Animations animations;
    private AnalyticsContract analyticsContract;
    private String source, smallcaseName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_sip);

        ButterKnife.bind(this);

        scid = getIntent().getStringExtra(SCID);
        iScid = getIntent().getStringExtra(ISCID);
        smallcaseName = getIntent().getStringExtra(SMALLCASE_NAME);
        source = getIntent().getStringExtra(SOURCE);

        toolBarTitle.setText(smallcaseName);
        String frequencyText = "You have chosen to invest " + getIntent().getStringExtra(FREQUENCY);
        frequency.setText(frequencyText);

        Date date = new Date(getIntent().getLongExtra(NEXT_SIP, 1));
        String nextSipDate = simpleDateFormat.format(date);
        dateSelected.setText(nextSipDate);
        calendar = Calendar.getInstance();
        calendar.setTime(date);

        previousDate = AppUtils.getInstance().getDateInApiSendableFormat(calendar);
        previousFrequency = getIntent().getStringExtra(FREQUENCY);

        everyWeek.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM));
        everyTwoWeeks.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM));
        everyMonth.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM));
        everyQuarter.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM));

        nextSip.setText("Your next SIP date is " + nextSipDate);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(this, (view, year1, month1, dayOfMonth) -> {
            calendar.set(year1, month1, dayOfMonth);
            dateSelected.setText(simpleDateFormat.format(new Date(calendar.getTimeInMillis())));
        }, year, month, day);

        calendar.setTime(new Date(System.currentTimeMillis()));
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 1);
        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());

        setFrequencySelection(getIntent().getStringExtra(FREQUENCY));

        service = new EditSipService(new UserSmallcaseRepository(), new SharedPrefService(this), this);
        calendar.setTime(date);

        animations = new Animations();
        analyticsContract = new AnalyticsManager(this);
    }

    private void setFrequencySelection(String frequency) {
        String currentlySelectedText = "(Current selection)";
        switch (frequency) {
            case "weekly":
                sipFrequencyContainer.check(R.id.every_week);
                SpannableString everyWeekString = new SpannableString("Every week  " + currentlySelectedText);
                everyWeekString.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR)),
                        everyWeekString.length() - currentlySelectedText.length(), everyWeekString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                everyWeekString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.secondary_text)),
                        everyWeekString.length() - currentlySelectedText.length(), everyWeekString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                everyWeek.setText(everyWeekString);
                everyTwoWeeks.setText(getString(R.string.every_2_weeks));
                everyMonth.setText(getString(R.string.every_month));
                everyQuarter.setText(getString(R.string.every_quarter));
                break;

            case "fortnightly":
                sipFrequencyContainer.check(R.id.every_two_weeks);
                SpannableString fortnightString = new SpannableString("Every 2 weeks  " + currentlySelectedText);
                fortnightString.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR)),
                        fortnightString.length() - currentlySelectedText.length(), fortnightString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                fortnightString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.secondary_text)),
                        fortnightString.length() - currentlySelectedText.length(), fortnightString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                everyTwoWeeks.setText(fortnightString);
                everyWeek.setText(getString(R.string.every_week));
                everyMonth.setText(getString(R.string.every_month));
                everyQuarter.setText(getString(R.string.every_quarter));
                break;

            case "monthly":
                sipFrequencyContainer.check(R.id.every_month);
                SpannableString monthlyString = new SpannableString("Every month  " + currentlySelectedText);
                monthlyString.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR)),
                        monthlyString.length() - currentlySelectedText.length(), monthlyString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                monthlyString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.secondary_text)),
                        monthlyString.length() - currentlySelectedText.length(), monthlyString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                everyMonth.setText(monthlyString);
                everyWeek.setText(getString(R.string.every_week));
                everyTwoWeeks.setText(getString(R.string.every_2_weeks));
                everyQuarter.setText(getString(R.string.every_quarter));
                break;

            case "quarterly":
                sipFrequencyContainer.check(R.id.every_quarter);
                SpannableString quarterlyString = new SpannableString("Every quarterly  " + currentlySelectedText);
                quarterlyString.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR)),
                        quarterlyString.length() - currentlySelectedText.length(), quarterlyString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                quarterlyString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.secondary_text)),
                        quarterlyString.length() - currentlySelectedText.length(), quarterlyString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                everyQuarter.setText(quarterlyString);
                everyWeek.setText(getString(R.string.every_week));
                everyTwoWeeks.setText(getString(R.string.every_2_weeks));
                everyMonth.setText(getString(R.string.every_month));
                break;
        }
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.edit_frequency)
    public void showEditFrequency() {
        if (sipFrequencyContainer.getVisibility() == View.VISIBLE) return;

        animations.expand(sipFrequencyContainer, accelerateDecelerateInterpolator, 200);
        animations.collapse(dateSelectionLayout, accelerateDecelerateInterpolator, 200);
        showOrHideEditOptions(View.VISIBLE, View.GONE, 1f, 0f);
    }

    @OnClick(R.id.edit_sip_date)
    public void showEditSip() {
        if (dateSelectionLayout.getVisibility() == View.VISIBLE) return;

        animations.expand(dateSelectionLayout, accelerateDecelerateInterpolator, 200);
        animations.collapse(sipFrequencyContainer, accelerateDecelerateInterpolator, 200);
        showOrHideEditOptions(View.GONE, View.VISIBLE, 0f, 1f);
    }

    @OnClick(R.id.date_selection_layout)
    public void showCalender() {
        datePickerDialog.show();
    }

    @OnClick(R.id.save_frequency)
    public void saveNewFrequency() {
        changeSip("frequency", getFrequency(true));
    }

    @OnClick(R.id.save_investment_date)
    public void saveNewInvestmentDate() {
        changeSip("scheduledDate", AppUtils.getInstance().getDateInApiSendableFormat(calendar));
    }

    @Override
    public void onSipSavedSuccessful() {
        dismissProgressBar();

        if (dateSelectionLayout.getVisibility() == View.VISIBLE) {
            animations.collapse(dateSelectionLayout, accelerateDecelerateInterpolator, 200);
        } else {
            animations.collapse(sipFrequencyContainer, accelerateDecelerateInterpolator, 200);
        }
        showOrHideEditOptions(View.GONE, View.GONE, 1f, 1f);

        String nextSipDate = simpleDateFormat.format(new Date(calendar.getTimeInMillis()));
        dateSelected.setText(nextSipDate);

        nextSip.setText("Your next SIP date is " + nextSipDate);

        String frequencyText = "You have chosen to invest " + getFrequency(false);
        frequency.setText(frequencyText);

        setFrequencySelection(getFrequency(false));

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", smallcaseName);
        eventProperties.put("smallcaseType", source);
        eventProperties.put("frequencyFrom", previousFrequency);
        eventProperties.put("frequencyTo", getFrequency(false));
        eventProperties.put("sipDateFrom", previousDate);
        eventProperties.put("sipDateTo", AppUtils.getInstance().getDateInApiSendableFormat(calendar));
        analyticsContract.sendEvent(eventProperties, "Changed SIP Settings", Analytics.MIXPANEL, Analytics.CLEVERTAP);

        previousDate = AppUtils.getInstance().getDateInApiSendableFormat(calendar);
        previousFrequency = getFrequency(false);
    }

    @Override
    public void onFailedSavingSip() {
        dismissProgressBar();
        showSnackBar(R.string.something_wrong);
    }

    @Override
    public void showSnackBar(int resId) {
        AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_LONG, resId);
    }

    @Override
    public void onSipEndedSuccessful() {
        dismissProgressBar();
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(AppConstants.SHOW_SIP_CANCELLED));
        finish();
    }

    @Override
    public void onFailedEndingSip() {
        dismissProgressBar();
        showSnackBar(R.string.something_wrong);
    }

    @OnClick(R.id.end_sip)
    public void endSip() {
        animations.scaleIntoPlaceAnimation(dialogConfirmation);
        endSipBlur.setVisibility(View.VISIBLE);
        endSipBlur.setOnClickListener(v -> animations.scaleOutAnimation(dialogConfirmation, endSipBlur));
    }

    @OnClick(R.id.dialog_primary_button)
    public void endSipConfirmed() {
        animations.scaleOutAnimation(dialogConfirmation, endSipBlur, this);
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", smallcaseName);
        eventProperties.put("smallcaseType", source);
        eventProperties.put("frequency", getFrequency(false));
        eventProperties.put("sipDate", AppUtils.getInstance().getDateInApiSendableFormat(calendar));
        analyticsContract.sendEvent(eventProperties, "Ended SIP", Analytics.MIXPANEL, Analytics.CLEVERTAP);
    }

    @OnClick(R.id.dialog_secondary_button)
    public void hideEndSip() {
        animations.scaleOutAnimation(dialogConfirmation, endSipBlur);
    }

    @Override
    protected void onDestroy() {
        service.destroySubscriptions();
        super.onDestroy();
    }

    private void dismissProgressBar() {
        if (progressDialog != null) progressDialog.dismiss();
    }

    private void changeSip(String fieldChanged, String value) {
        HashMap<String, String> body = new HashMap<>();
        body.put("iscid", iScid);
        body.put("scid", scid);
        body.put(fieldChanged, value);

        progressDialog = ProgressDialog.show(this, "", "Loading...");
        service.setUpSip(body);
    }

    private String getFrequency(boolean isApi) {
        String sip;
        switch (sipFrequencyContainer.getCheckedRadioButtonId()) {
            case R.id.every_week:
                sip = isApi ? "7d" : "weekly";
                break;

            case R.id.every_two_weeks:
                sip = isApi ? "14d" : "fortnightly";
                break;

            case R.id.every_month:
                sip = isApi ? "1m" : "monthly";
                break;

            case R.id.every_quarter:
                sip = isApi ? "3m" : "quarterly";
                break;

            default:
                sip = "";
                break;
        }
        return sip;
    }

    private void showOrHideEditOptions(int frequency, int nextSip, float sipAlpha, float frequencyAlpha) {
        saveFrequency.setVisibility(frequency);

        dateDivider.setVisibility(nextSip);
        sipDateDescription.setVisibility(nextSip);
        saveInvestmentDate.setVisibility(nextSip);

        editFrequency.animate()
                .alpha(frequencyAlpha)
                .setDuration(200).start();
        editSipDate.animate()
                .alpha(sipAlpha)
                .setDuration(200).start();
    }

    @Override
    public void onAnimationEnd() {
        HashMap<String, String> body = new HashMap<>();
        body.put("iscid", iScid);

        progressDialog = ProgressDialog.show(this, "", "Loading...");
        service.endSip(body);
    }
}
