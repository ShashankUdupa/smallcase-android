package com.smallcase.android.investedsmallcase;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.repository.AuthRepository;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.kite.KiteContract;
import com.smallcase.android.kite.KiteHelper;
import com.smallcase.android.onboarding.LoginActivity;
import com.smallcase.android.smallcase.LoginRequest;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.stock.AddPopUpObservable;
import com.smallcase.android.stock.MarketHelper;
import com.smallcase.android.stock.SearchStockPopUp;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.stock.StockInfo;
import com.smallcase.android.user.ContainerActivity;
import com.smallcase.android.user.order.ReviewOrderActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.OrderLabel;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.buy.InvestContract;
import com.smallcase.android.view.model.PStock;
import com.smallcase.android.view.model.Stock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.adapter.rxjava.HttpException;

import static com.smallcase.android.onboarding.LoginActivity.REQUEST_TOKEN_KEY;

public class ManageActivity extends AppCompatActivity implements StockHelper.StockPriceCallBack, KiteContract.Response,
        MarketHelper.MarketCallback, InvestContract.Response, SearchStockPopUp.AddStockPopUpSubscriber,
        SmallcaseHelper.LoginResponse {
    public static final String SMALLCASE_NAME = "smallcase_name";
    public static final String SCID = "scid";
    public static final String ISCID = "iscid";
    public static final String STOCKS_LIST = "stocks_list";
    public static final String BROUGHT_ON = "brought_on";
    public static final String INDEX = "index";
    public static final String IS_UP = "is_up";
    public static final String SOURCE = "source";

    private static final String TAG = "ManageActivity";
    private static final int LOGIN_REQUEST_CODE = 2;

    @BindView(R.id.text_tool_bar_title)
    GraphikText toolbarTitle;
    @BindView(R.id.stocks_list)
    RecyclerView stocksList;
    @BindView(R.id.loading_indicator)
    View loadingIndicator;
    @BindView(R.id.parent)
    View parent;
    @BindView(R.id.image_smallcase)
    ImageView smallcaseImage;
    @BindView(R.id.text_smallcase_name)
    GraphikText smallcaseName;
    @BindView(R.id.text_smallcase_brought_on)
    GraphikText broughtOn;
    @BindView(R.id.text_smallcase_index)
    GraphikText smallcaseIndex;
    @BindView(R.id.image_smallcase_up_or_down)
    ImageView upOrDown;
    @BindView(R.id.nested_scroll)
    View nestedScroll;

    @BindView(R.id.add_funds_layout)
    View addFundsLayout;
    @BindView(R.id.required_amount)
    GraphikText requiredAmountText;
    @BindView(R.id.add_funds_blur)
    View addFundsBlur;
    @BindView(R.id.weight_shares_card)
    View sharesCard;
    @BindView(R.id.card_title)
    GraphikText cardTitle;
    @BindView(R.id.selected_stock_name)
    GraphikText selectedStockName;
    @BindView(R.id.weight_or_shares)
    EditText shares;

    private String name, iScid, scid, source;
    private List<Stock> originalStockList;
    private List<Stock> modifiedStockList;
    private List<Stock> finalStockList;
    private StocksAdapter stocksAdapter;
    private StockHelper stockHelper;
    private MarketRepository marketRepository;
    private SharedPrefService sharedPrefService;
    private ProgressDialog progressDialog;
    private InvestContract.Response responseContract = this;
    private KiteHelper kiteHelper;
    double requiredAmount = 0;
    private BottomSheetBehavior<View> requiredAmountBottomSheet;
    private AddPopUpObservable addPopUpObservable;
    private boolean isConfirmChanges = false;
    private LoginRequest loginRequest;
    private StockInfo stockInfo;
    private AnalyticsContract analyticsContract;
    private BottomSheetBehavior<View> sharesBottomSheet;
    private Stock editingStock;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage);
        ButterKnife.bind(this);

        name = getIntent().getStringExtra(SMALLCASE_NAME);
        List<PStock> stockList = new ArrayList<>(getIntent().<PStock>getParcelableArrayListExtra(STOCKS_LIST));
        scid = getIntent().getStringExtra(SCID);
        iScid = getIntent().getStringExtra(ISCID);
        source = getIntent().getStringExtra(SOURCE);

        originalStockList = new ArrayList<>();
        List<String> sids = new ArrayList<>();
        for (PStock pStock : stockList) {
            if (pStock.isHeading()) continue;

            Stock stock = new Stock();
            stock.setShares(pStock.getShares());
            stock.setWeight(pStock.getStockWeightage() / 100);
            stock.setStockName(pStock.getStockName());
            stock.setSid(pStock.getSid());
            originalStockList.add(stock);
            sids.add(pStock.getSid());
        }
        toolbarTitle.setText("Manage smallcase");
        Glide.with(this)
                .load((SmallcaseSource.CREATED.equals(source) ? AppConstants
                        .CREATED_SMALLCASE_IMAGE_URL : AppConstants.SMALLCASE_IMAGE_URL) + "80/" + scid + ".png")
                .into(smallcaseImage);
        smallcaseName.setText(name);
        broughtOn.setText(getIntent().getStringExtra(BROUGHT_ON));
        smallcaseIndex.setText(getIntent().getStringExtra(INDEX));
        Glide.with(this)
                .load(getIntent().getBooleanExtra(IS_UP, false) ? R.drawable.up_arrow : R.drawable.down_arrow_red)
                .asBitmap()
                .into(upOrDown);

        analyticsContract = new AnalyticsManager(getApplicationContext());

        sharesBottomSheet = BottomSheetBehavior.from(sharesCard);
        sharesBottomSheet.setHideable(true);
        sharesBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", name);
        eventProperties.put("smallcaseType", source);
        analyticsContract.sendEvent(eventProperties, "Viewed Manage smallcase", Analytics.MIXPANEL, Analytics.CLEVERTAP);

        requiredAmountBottomSheet = BottomSheetBehavior.from(addFundsLayout);
        requiredAmountBottomSheet.setHideable(true);
        requiredAmountBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);

        KiteRepository kiteRepository = new KiteRepository();
        marketRepository = new MarketRepository();
        sharedPrefService = new SharedPrefService(this);
        stockHelper = new StockHelper();
        kiteHelper = new KiteHelper(sharedPrefService, kiteRepository);
        loginRequest = new SmallcaseHelper(new AuthRepository(), this, sharedPrefService);
        stockInfo = new StockInfo(this, stockHelper, marketRepository, sharedPrefService);

        stocksList.setLayoutManager(new LinearLayoutManager(this));
        stocksList.setNestedScrollingEnabled(false);
        stocksList.setHasFixedSize(false);
        RecyclerView.ItemAnimator animator = stocksList.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        cardTitle.setText("Confirm shares");

        addPopUpObservable = new SearchStockPopUp(this, this);
        stockHelper.getStockPrices(marketRepository, sharedPrefService, sids, this);
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.PLACED_ORDER_BROADCAST));
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.HARD_RELOAD_BROADCAST));
    }

    @Override
    public void onBackPressed() {
        if (stockInfo.isBottomSheetOpen()) {
            stockInfo.setBottomSheetState(BottomSheetBehavior.STATE_HIDDEN);
            return;
        }

        if (addPopUpObservable.isPopUpOpen()) {
            addPopUpObservable.hidePopUp();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        addPopUpObservable.unBind();
        stockInfo.unBind();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
        if (snackbar != null) snackbar.dismiss();
        super.onDestroy();
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onStockPricesReceived(HashMap<String, Double> stockMap) {
        if (stocksAdapter == null) {
            // Initially receiving prices for each stock
            for (Stock stock : originalStockList) stock.setPrice(stockMap.get(stock.getSid()));
            setUpAdapter();
        } else if (isConfirmChanges) {
            // When user wants to place order by confirming changes
            requiredAmount = 0;
            for (Stock stock : finalStockList) {
                stock.setPrice(stockMap.get(stock.getSid()));
                if (stock.isBuy()) {
                    requiredAmount += stock.getPrice() * stock.getShares();
                } else {
                    requiredAmount -= stock.getPrice() * stock.getShares();
                }
            }
            kiteHelper.checkSufficientFunds(requiredAmount, this);
        } else {
            // When user adds a new stock and the price of that stock is received
            for (int i = modifiedStockList.size() - 1; i >= 0; i--) {
                Stock stock = modifiedStockList.get(i);
                if (stockMap.containsKey(stock.getSid())) {
                    stock.setPrice(stockMap.get(stock.getSid()));
                    break;
                }
            }
            stocksAdapter.calculateNewWeightage();
        }
    }

    @OnClick(R.id.add_stock)
    public void addStock() {
        isConfirmChanges = false;

        if (modifiedStockList == null) {
            showSnackBar(R.string.something_wrong);
            return;
        }

        addPopUpObservable.setStockList(modifiedStockList);
        addPopUpObservable.showStockPopUp();
    }

    @Override
    public void onFailedFetchingStockPrice(Throwable throwable, @Nullable String sid) {
        dismissDialog();
        showSnackBar(R.string.something_wrong);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            //Received request_token from Kite login. Request authToken from server.
            HashMap<String, String> body = new HashMap<>();
            body.put("broker", "kite");
            body.put("reqToken", data.getStringExtra(REQUEST_TOKEN_KEY));
            body.put("app", "platform");

            loginRequest.getJwtToken(body);
        }
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (isFinishing()) return;

            String action = intent.getAction();
            if (AppConstants.PLACED_ORDER_BROADCAST.equals(action)) {
                finish();
                return;
            }

            if (AppConstants.HARD_RELOAD_BROADCAST.equals(action)) {
                // No use reloading , just finish
                finish();
            }
        }
    };

    @OnClick(R.id.reset_card)
    public void restoreOriginal() {
        modifiedStockList.clear();
        for (Stock stock : originalStockList) {
            modifiedStockList.add(new Stock(stock));
        }

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", name);
        eventProperties.put("smallcaseType", source);
        analyticsContract.sendEvent(eventProperties, "Reset Manage Changes", Analytics.MIXPANEL);

        requiredAmount = 0;
        stocksAdapter.notifyDataSetChanged();
    }

    @OnClick(R.id.confirm_changes_card)
    public void confirmChanges() {
        isConfirmChanges = true;
        progressDialog = ProgressDialog.show(this, "", "Loading...");
        progressDialog.show();
        if ("leprechaun".equals(sharedPrefService.getUserInfo().get("brokerName"))) {
            onMarketStatusReceived(AppConstants.MarketStatus.OPEN);
        } else {
            new MarketHelper(sharedPrefService, marketRepository, this).checkMarketStatus();
        }
    }

    @Override
    public void onFailedFetchingFunds(@Nullable Throwable throwable) {
        dismissDialog();
        if (throwable == null) {
            showSnackBar(R.string.something_wrong);
            return;
        }

        if (throwable instanceof HttpException) {
            HttpException exception = (HttpException) throwable;
            if (exception.code() == 401) {
                responseContract.sendUserToKiteLogin();
            }
        }
    }

    @OnClick(R.id.add_funds)
    public void addFunds() {
        requiredAmountBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(AppConstants.KITE_ADD_FUNDS));
        startActivity(intent);
    }

    @Override
    public void sufficientFundsAvailable() {
        dismissDialog();
        Intent intent = new Intent(this, ReviewOrderActivity.class);
        intent.putExtra(ReviewOrderActivity.ISCID, iScid);
        intent.putExtra(ReviewOrderActivity.SCID, scid);
        intent.putExtra(ReviewOrderActivity.INVESTMENT_AMOUNT, requiredAmount);
        intent.putExtra(ReviewOrderActivity.ORDER_LABEL, OrderLabel.MANAGE);
        intent.putParcelableArrayListExtra(ReviewOrderActivity.STOCK_LIST, (ArrayList<? extends Parcelable>) finalStockList);
        intent.putExtra(ReviewOrderActivity.SMALLCASE_NAME, name);
        intent.putExtra(ReviewOrderActivity.SOURCE, source);
        startActivity(intent);
    }

    @Override
    public void needMoreFunds(double availableFunds) {
        dismissDialog();
        addFundsBlur.setClickable(true);
        addFundsBlur.setVisibility(View.VISIBLE);
        addFundsBlur.setOnClickListener(v -> requiredAmountBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN));

        requiredAmountBottomSheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    addFundsBlur.setClickable(false);
                    addFundsBlur.setVisibility(View.GONE);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        String neededAmount = AppConstants.RUPEE_SYMBOL + AppConstants.getDecimalNumberFormatter().format((requiredAmount - availableFunds));
        SpannableString spannableString = new SpannableString("Oops, looks like you don’t have enough funds. Please add at least "
                + neededAmount + " to continue.");
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.primary_text)),
                66, (66 + neededAmount.length()), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        requiredAmountText.setText(spannableString);
        requiredAmountBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private void showSnackBar(int errResId) {
        if (!isFinishing()) {
            snackbar = Snackbar.make(parent, errResId, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    private void setUpAdapter() {
        loadingIndicator.setVisibility(View.GONE);
        nestedScroll.setVisibility(View.VISIBLE);
        modifiedStockList = new ArrayList<>();
        for (Stock stock : originalStockList) {
            modifiedStockList.add(new Stock(stock));
        }

        stocksAdapter = new StocksAdapter();
        stocksList.setAdapter(stocksAdapter);
    }

    private void dismissDialog() {
        if (progressDialog != null) {
            progressDialog.hide();
            progressDialog.cancel();
        }
    }

    @Override
    public void onMarketStatusReceived(AppConstants.MarketStatus marketStatus) {
        if (marketStatus == AppConstants.MarketStatus.OPEN) {
            finalStockList = new ArrayList<>();
            List<String> sids = new ArrayList<>();
            int stocksWithZeroShares = 0;
            for (int i = 0; i < modifiedStockList.size(); i++) {
                Stock stock = new Stock();
                Stock modifiedStock = modifiedStockList.get(i);
                stock.setSid(modifiedStock.getSid());
                stock.setStockName(modifiedStock.getStockName());
                if (modifiedStock.getShares() == 0) stocksWithZeroShares++;
                if (i >= originalStockList.size()) {
                    if (modifiedStock.getShares() == 0)
                        continue; // Don't add stock if number of shares is 0

                    stock.setBuy(true);
                    stock.setShares(modifiedStock.getShares());
                    sids.add(stock.getSid());
                    finalStockList.add(stock);
                    continue;
                }
                double effectiveShares = modifiedStock.getShares() - originalStockList.get(i).getShares();

                if (effectiveShares == 0)
                    continue; // If user hasn't changed the number of shares for that stock

                stock.setBuy(effectiveShares > 0);
                stock.setShares(Math.abs(effectiveShares));
                sids.add(stock.getSid());
                finalStockList.add(stock);
            }

            // smallcase requires at least two stocks with non zero shares
            if ((modifiedStockList.size() - stocksWithZeroShares) < 2) {
                dismissDialog();
                showSnackBar(R.string.need_two_stocks);
                return;
            }

            if (finalStockList.isEmpty()) {
                dismissDialog();
                showSnackBar(R.string.no_change);
                return;
            }

            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("noOfChanges", finalStockList.size());
            eventProperties.put("smallcaseName", name);
            eventProperties.put("smallcaseType", source);
            analyticsContract.sendEvent(eventProperties, "Confirm Manage Changes", Analytics.MIXPANEL);

            stockHelper.getStockPrices(marketRepository, sharedPrefService, sids, this);
            return;
        }

        if (marketStatus == AppConstants.MarketStatus.CLOSED) {
            dismissDialog();
            showSnackBar(R.string.market_closed_now);
        }
    }

    @Override
    public void onFailedFetchingMarketStatus() {
        dismissDialog();
        showSnackBar(R.string.something_wrong);
    }

    @Override
    public void canContinueTransaction(List<Stock> stockList) {
        finalStockList = stockList;
        kiteHelper.checkSufficientFunds(requiredAmount, this);
    }

    @Override
    public void onConfirmAmountClicked(double investmentAmount, double minInvestmentAmount) {
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", name);
        eventProperties.put("smallcaseType", source);
        eventProperties.put("amountConfirmed", investmentAmount);
        eventProperties.put("minInvestAmount", minInvestmentAmount);
        analyticsContract.sendEvent(eventProperties, "Confirmed Amount", Analytics.MIXPANEL, Analytics.CLEVERTAP);
    }

    @Override
    public void onAddFundsClicked(double investmentAmount, double minInvestmentAmount) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(AppConstants.KITE_ADD_FUNDS));
        startActivity(intent);

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", name);
        eventProperties.put("smallcaseType", source);
        eventProperties.put("amountConfirmed", investmentAmount);
        eventProperties.put("minInvestAmount", minInvestmentAmount);
        analyticsContract.sendEvent(eventProperties, "Proceeded To Adding Funds", Analytics.MIXPANEL, Analytics.CLEVERTAP);
    }

    @Override
    public void onConfirmShownAgain() {
        // Don't do anything with this was put in for analytics
    }

    @Override
    public void showBlur() {
        // Don't do anything with this
    }

    @Override
    public void hideBlur() {
        // Don't do anything with this
    }

    @Override
    public void sendUserToKiteLogin() {
        dismissDialog();
        startActivityForResult(new Intent(this, LoginActivity.class),
                LOGIN_REQUEST_CODE);
        overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    }

    @Override
    public void onStockSelected(Stock stock) {
        boolean isStockPresent = false;

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("stockName", stock.getStockName());
        eventProperties.put("smallcaseName", name);
        eventProperties.put("smallcaseType", source);
        analyticsContract.sendEvent(eventProperties, "Added Stock", Analytics.MIXPANEL);

        for (Stock availableStock : modifiedStockList) {
            if (availableStock.getSid().equals(stock.getSid())) {
                isStockPresent = true;
                break;
            }
        }

        if (isStockPresent) {
            showSnackBar(R.string.stock_already_present);
            return;
        }

        if (modifiedStockList.size() >= 20) {
            showSnackBar(R.string.not_more_than_twenty);
            return;
        }

        modifiedStockList.add(stock);
        stocksAdapter.calculateNewWeightage();
        List<String> sids = new ArrayList<>();
        sids.add(stock.getSid());
        stockHelper.getStockPrices(marketRepository, sharedPrefService, sids, this);
    }

    @OnClick(R.id.confirm_value)
    public void onWeightOrSharesConfirmed() {
        AppUtils.getInstance().hideKeyboard(this);
        sharesBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
        if (shares.getText() == null || "".equals(shares.getText().toString().trim()))
            return;

        stocksAdapter.editShares(editingStock, Double.parseDouble(shares.getText().toString()));
    }

    @Override
    public void onJwtSavedSuccessfully() {
        kiteHelper.checkSufficientFunds(requiredAmount, ManageActivity.this);
    }

    @Override
    public void onFailedFetchingJwt() {
        showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onDifferentUserLoggedIn() {
        Intent intent = new Intent(this, ContainerActivity.class);
        intent.putExtra(ContainerActivity.NEW_USER, true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void showSharesOrWeightBottomSheet() {
        addFundsBlur.setVisibility(View.VISIBLE);

        sharesBottomSheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    addFundsBlur.setVisibility(View.GONE);
                    AppUtils.getInstance().hideKeyboard(ManageActivity.this);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        addFundsBlur.setOnClickListener(v -> sharesBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN));

        selectedStockName.setText(editingStock.getStockName());
        shares.setText(String.format(Locale.getDefault(), "%.0f", editingStock.getShares()));

        sharesBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    class StocksAdapter extends RecyclerView.Adapter<StocksAdapter.ViewHolder> {

        @Override
        public StocksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.manage_sock_card, parent, false));
        }

        @Override
        public void onBindViewHolder(StocksAdapter.ViewHolder holder, int position) {
            if (position == 0) {
                holder.quantity.setVisibility(View.VISIBLE);
                holder.addRemoveSharesContainer.setVisibility(View.GONE);

                holder.stockName.setText("Stock");
                holder.quantity.setText("Qty");
                holder.weightage.setText("Weightage");

                holder.stockName.setTextColor(ContextCompat.getColor(ManageActivity.this, R.color.secondary_text));
                holder.weightage.setTextColor(ContextCompat.getColor(ManageActivity.this, R.color.secondary_text));

                holder.stockName.setOnClickListener(null);
                return;
            }

            holder.quantity.setVisibility(View.GONE);
            holder.addRemoveSharesContainer.setVisibility(View.VISIBLE);

            final Stock stock = modifiedStockList.get(position - 1);

            holder.addShares.setVisibility(View.VISIBLE);
            holder.removeShare.setVisibility(View.VISIBLE);

            if (stock.getShares() > 0) {
                holder.removeShare.setOnClickListener(v -> {
                    stock.setShares(stock.getShares() - 1);
                    calculateNewWeightage();
                });
            } else {
                holder.removeShare.setOnClickListener(null);
            }

            holder.addShares.setOnClickListener(v -> {
                stock.setShares(stock.getShares() + 1);
                calculateNewWeightage();
            });

            holder.stockName.setOnClickListener(v -> stockInfo.showStockInfo(stock.getSid()));

            holder.shares.setOnClickListener(v -> {
                editingStock = stock;
                showSharesOrWeightBottomSheet();
            });

            holder.stockName.setTextColor(ContextCompat.getColor(ManageActivity.this, R.color.primary_text));
            holder.weightage.setTextColor(ContextCompat.getColor(ManageActivity.this, R.color.primary_text));

            holder.stockName.setText(stock.getStockName());
            holder.shares.setText(String.valueOf((int) stock.getShares()));

            if (stock.getShares() > 0 && stock.getWeight() == 0) {
                holder.weightage.setText(AppConstants.HYPHEN_SYMBOL + "." + AppConstants.HYPHEN_SYMBOL);
            } else {
                holder.weightage.setText(String.format(Locale.getDefault(), "%.2f", (stock.getWeight() * 100)) + "%");
            }
        }

        void calculateNewWeightage() {
            stockHelper.calculateWeightageFromShares(modifiedStockList);
            notifyItemRangeChanged(1, modifiedStockList.size());
        }

        void editShares(Stock editingStock, double shares) {
            editingStock.setShares(shares < 0 ? 0 : shares);
            if (stocksList.isComputingLayout()) return;
            calculateNewWeightage();
        }

        @Override
        public int getItemCount() {
            return modifiedStockList.size() + 1;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.text_stock_name)
            GraphikText stockName;
            @BindView(R.id.remove_shares)
            View removeShare;
            @BindView(R.id.quantity)
            GraphikText quantity;
            @BindView(R.id.add_shares)
            View addShares;
            @BindView(R.id.weightage)
            GraphikText weightage;
            @BindView(R.id.stock_card)
            View stockCard;
            @BindView(R.id.shares)
            GraphikText shares;
            @BindView(R.id.add_remove_shares_container)
            View addRemoveSharesContainer;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
