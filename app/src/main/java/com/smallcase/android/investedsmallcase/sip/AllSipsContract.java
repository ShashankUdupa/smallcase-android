package com.smallcase.android.investedsmallcase.sip;

import com.smallcase.android.data.model.SipDetails;

import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by shashankm on 16/09/17.
 */

interface AllSipsContract {
    interface View {

        void onSipsFetched(List<SipDetails> sips);

        void onFailedFetchingSipUpdates();

        void onMarketStatusReceived(ResponseBody responseBody);

        void onFailedFetchingMarketStatus();

        void redirectUserToLogin();
    }

    interface Service {

        void getBuyReminders();

        void getMarketStatus();

        void destroySubscriptions();
    }
}
