package com.smallcase.android.investedsmallcase.exited;

import com.smallcase.android.data.model.ExitedSmallcase;

import java.util.List;

/**
 * Created by shashankm on 03/04/17.
 */

interface ExitedSmallcasesContract {
    interface View {

        void showSnackBar(int errResId);

        void onExitedSmallcasesReceived(List<ExitedSmallcase> exitedSmallcases);
    }

    interface Presenter {

        void getExitedSmallcases();

        void onExitedSmallcasesReceived(List<ExitedSmallcase> exitedSmallcases);

        void onFailedFetchingExitedSmallcases();

        void destroySubscriptions();
    }

    interface Service {

        void getExitedSmallcases();

        void destroySubscriptions();
    }
}
