package com.smallcase.android.investedsmallcase.exited;

import com.smallcase.android.R;
import com.smallcase.android.data.model.ExitedSmallcase;
import com.smallcase.android.data.repository.InvestmentRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.util.NetworkHelper;

import java.util.List;

/**
 * Created by shashankm on 03/04/17.
 */

class ExitedSmallcasePresenter implements ExitedSmallcasesContract.Presenter {
    private ExitedSmallcasesContract.View view;
    private NetworkHelper networkHelper;
    private ExitedSmallcaseService exitedSmallcaseService;

    ExitedSmallcasePresenter(ExitedSmallcasesContract.View view, SharedPrefService sharedPrefService, InvestmentRepository
            investmentRepository, NetworkHelper networkHelper) {
        this.view = view;
        this.networkHelper = networkHelper;
        exitedSmallcaseService = new ExitedSmallcaseService(investmentRepository, sharedPrefService, this);
    }

    @Override
    public void getExitedSmallcases() {
        if (!networkHelper.isNetworkAvailable()) {
            view.showSnackBar(R.string.no_internet);
            return;
        }
        exitedSmallcaseService.getExitedSmallcases();
    }

    @Override
    public void onExitedSmallcasesReceived(List<ExitedSmallcase> exitedSmallcases) {
        if (exitedSmallcases.isEmpty()) {
            view.showSnackBar(R.string.no_exited_smallcase);
            return;
        }

        view.onExitedSmallcasesReceived(exitedSmallcases);
    }

    @Override
    public void onFailedFetchingExitedSmallcases() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void destroySubscriptions() {
        exitedSmallcaseService.destroySubscriptions();
    }
}
