package com.smallcase.android.investedsmallcase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.model.ExitedSmallcase;
import com.smallcase.android.data.repository.InvestmentRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.smallcase.discover.DiscoverSmallcaseActivity;
import com.smallcase.android.user.ContainerActivity;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.InvestmentHelper;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.adapter.InvestedSmallcasesAdapter;
import com.smallcase.android.view.android.AndroidApp;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.PInvestedSmallcase;
import com.smallcase.android.view.model.PInvestments;
import com.squareup.leakcanary.RefWatcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by shashankm on 14/11/16.
 */

public class InvestedSmallcasesFragment extends Fragment implements InvestedSmallcasesContract.View,
        SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = InvestedSmallcasesFragment.class.getSimpleName();
    private static final String USING_SMALLCASE_URL = "http://help.smallcase.com/using-smallcase";
    private static final Interpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator();
    private static final Interpolator ANTICIPATE_OVERSHOOT_INTERPOLATOR = new AnticipateInterpolator();

    @BindView(R.id.empty_state_container)
    View noInvestmentsContainer;
    @BindView(R.id.invested_smallcases_list)
    RecyclerView investedSmallcaseList;
    @BindView(R.id.invested_swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.action)
    View tryAgain;
    @BindView(R.id.text_net_worth_amount)
    GraphikText currentValue;
    @BindView(R.id.total_p_and_l_amount)
    GraphikText totalPAndLAmount;
    @BindView(R.id.text_net_worth)
    GraphikText textNetWorth;
    @BindView(R.id.text_total_p_and_l)
    GraphikText textTotalPAndL;
    @BindView(R.id.text_total_p_and_l_percent)
    GraphikText totalPAndLPercent;
    @BindView(R.id.exited_smallcase_number_card)
    View exitedSmallcaseContainer;
    @BindView(R.id.text_exited_smallcase_size)
    GraphikText realisedPAndLText;
    @BindView(R.id.parent)
    View parentView;
    @BindView(R.id.text_empty_state)
    GraphikText emptyState;
    @BindView(R.id.action_text)
    GraphikText actionText;
    @BindView(R.id.investment_helper_text)
    GraphikText investmentHelperText;
    @BindView(R.id.nested_scroll_view)
    NestedScrollView nestedScrollView;
    @BindView(R.id.investments_container)
    RelativeLayout investmentContainer;
    @BindView(R.id.current_investment_amount)
    GraphikText currentInvestmentAmount;
    @BindView(R.id.current_p_and_l)
    GraphikText currentPAndL;
    @BindView(R.id.current_p_and_l_percent)
    GraphikText currentPAndLPercent;
    @BindView(R.id.expand_card)
    View expandArrow;

    @BindView(R.id.title_loading)
    View titleLoading;
    @BindView(R.id.networth_loading)
    View networthLoading;
    @BindView(R.id.exited_smallcase_number_card_no_current)
    View exitedSmallcaseCardWithNoCurrentInvestments;
    @BindView(R.id.text_exited_smallcase_no_current)
    GraphikText exitedSmallcasesWithNoCurrentInvestments;

    private Unbinder unbinder;
    private List<ExitedSmallcase> exitedSmallcases;
    private InvestedSmallcasesContract.Presenter investedSmallcasesPresenter;
    private InvestedSmallcasesAdapter mySmallcasesAdapter;
    private NetworkHelper networkHelper;
    private Animations animations;
    private Context context;
    private AnalyticsContract analyticsContract;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        analyticsContract = new AnalyticsManager(context.getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.invested_smallcases_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);

        investedSmallcaseList.setLayoutManager(new LinearLayoutManager(getContext()));
        investedSmallcaseList.setHasFixedSize(true);
        investedSmallcaseList.setNestedScrollingEnabled(false);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.blue_800);

        mySmallcasesAdapter = new InvestedSmallcasesAdapter(getActivity(), analyticsContract);
        investedSmallcaseList.setAdapter(mySmallcasesAdapter);

        networkHelper = new NetworkHelper(context);
        investedSmallcasesPresenter = new InvestedSmallcasesPresenter((Activity) context, this, new SharedPrefService(context),
                new InvestmentRepository(), new InvestmentHelper(), networkHelper);
        animations = new Animations();

        animations.loadingContent(titleLoading, networthLoading);
        investedSmallcasesPresenter.fetchUserInvestmentsData();
        return view;
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        RefWatcher refWatcher = AndroidApp.getRefWatcher(getActivity());
        refWatcher.watch(this);
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        investedSmallcasesPresenter.destroySubscriptions();
        super.onDestroy();
    }

    @OnClick(R.id.action)
    void actionButtonClicked() {
        if (actionText.getText().toString().equals(getString(R.string.try_again))) {
            hideErrorIfVisible();
            swipeRefreshLayout.setVisibility(View.VISIBLE);
            investedSmallcasesPresenter.fetchUserInvestmentsData();
        } else {
            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("accessedFrom", "Investments");
            analyticsContract.sendEvent(eventProperties, "Viewed Discover", Analytics.MIXPANEL, Analytics.CLEVERTAP);

            Intent intent = new Intent(getActivity(), DiscoverSmallcaseActivity.class);
            intent.putExtra(DiscoverSmallcaseActivity.RECENT, true);
            startActivity(intent);
        }
    }

    @Override
    public void onRefresh() {
        if (tryAgain.getVisibility() == View.VISIBLE) {
            tryAgain.setVisibility(View.GONE);
            noInvestmentsContainer.setVisibility(View.GONE);
        }
        swipeRefreshLayout.setRefreshing(true);
        investedSmallcasesPresenter.fetchUserInvestmentsData();
    }

    @Override
    public void onInvestedSmallcasesReceived(List<PInvestedSmallcase> investedSmallcaseList) {
        if (!isAdded()) return;

        hideErrorIfVisible();
        stopRefreshing();

        if (mySmallcasesAdapter.didFetchInvestments()) {
            mySmallcasesAdapter.updateItems(investedSmallcaseList);
            swipeRefreshLayout.setRefreshing(false);
            return;
        }
        mySmallcasesAdapter.onFetchedInvestments(investedSmallcaseList);
    }

    @OnClick({ R.id.investment_card, R.id.expand_card })
    public void expandCard() {
        if (investmentContainer.getVisibility() == View.GONE) {
            animations.expand(investmentContainer, OVERSHOOT_INTERPOLATOR, 300);
            expandArrow.setRotation(270);
        } else {
            animations.collapse(investmentContainer, ANTICIPATE_OVERSHOOT_INTERPOLATOR, 250);
            expandArrow.setRotation(90);
        }
    }

    @Override
    public void onInvestmentsReceived(final PInvestments pInvestments) {
        if (!isAdded()) return;

        stopRefreshing();
        hideErrorIfVisible();

        if (pInvestments != null) {
            titleLoading.setVisibility(View.GONE);
            networthLoading.setVisibility(View.GONE);

            textTotalPAndL.setVisibility(View.VISIBLE);
            textNetWorth.setVisibility(View.VISIBLE);

            animations.stopAnimation();

            int totalColor = pInvestments.isTotalPAndLProfit() ? R.color.green_600 : R.color.red_600;
            totalPAndLAmount.setTextColor(ContextCompat.getColor(getContext(), totalColor));
            totalPAndLPercent.setTextColor(ContextCompat.getColor(getContext(), totalColor));
            currentValue.setText(pInvestments.getCurrentValue());
            totalPAndLAmount.setText(pInvestments.getTotalPAndL());
            totalPAndLPercent.setText(pInvestments.getTotalPAndLPercent());

            int currentColor = pInvestments.isCurrentPAndLProfit() ? R.color.green_600 : R.color.red_600;
            currentPAndLPercent.setTextColor(ContextCompat.getColor(getContext(), currentColor));
            currentPAndL.setTextColor(ContextCompat.getColor(getContext(), currentColor));
            currentInvestmentAmount.setText(pInvestments.getCurrentInvestment());
            currentPAndL.setText(pInvestments.getCurrentPAndL());
            currentPAndLPercent.setText(pInvestments.getCurrentPAndLPercent());
        }
    }

    @Override
    public void onExitedSmallcasesReceived(final List<ExitedSmallcase> exitedSmallcases) {
        if (!isAdded()) return;

        stopRefreshing();

        if (exitedSmallcases == null || exitedSmallcases.isEmpty()) {
            // Just hide exited smallcases card
            exitedSmallcaseContainer.setVisibility(View.GONE);
            exitedSmallcaseCardWithNoCurrentInvestments.setVisibility(View.GONE);
            return;
        } else {
            exitedSmallcaseContainer.setVisibility(View.VISIBLE);
        }

        InvestedSmallcasesFragment.this.exitedSmallcases = new ArrayList<>(exitedSmallcases);

        if (swipeRefreshLayout.getVisibility() == View.GONE) {
            // No current investments available
            exitedSmallcaseCardWithNoCurrentInvestments.setVisibility(View.VISIBLE);
            investmentHelperText.setVisibility(View.GONE);

            exitedSmallcasesWithNoCurrentInvestments.setText(exitedSmallcases.size() == 1 ? "See 1 exited smallcase" : "See " +
                    String.valueOf(exitedSmallcases.size()) + " exited smallcases");
            exitedSmallcaseCardWithNoCurrentInvestments.setOnClickListener(v -> {
                analyticsContract.sendEvent(null, "Viewed Exited Investments", Analytics.MIXPANEL, Analytics.CLEVERTAP);
                investedSmallcasesPresenter.onExitedSmallcaseClicked(InvestedSmallcasesFragment.this.exitedSmallcases);
            });
        } else {
            exitedSmallcaseCardWithNoCurrentInvestments.setVisibility(View.GONE);
            realisedPAndLText.setText(exitedSmallcases.size() == 1 ? "See 1 exited smallcase" : "See " +
                    String.valueOf(exitedSmallcases.size()) + " exited smallcases");
            exitedSmallcaseContainer.setOnClickListener(v -> {
                analyticsContract.sendEvent(null, "Viewed Exited Investments", Analytics.MIXPANEL, Analytics.CLEVERTAP);
                investedSmallcasesPresenter.onExitedSmallcaseClicked(InvestedSmallcasesFragment.this.exitedSmallcases);
            });
        }

        if (getArguments().getBoolean(ContainerActivity.SCROLL_TO_BOTTOM)) {
            getArguments().clear();
            // Make sure nested scroll view is laid out before scrolling to bottom
            nestedScrollView.post(() -> nestedScrollView.fullScroll(ScrollView.FOCUS_DOWN));
        }
    }

    @Override
    public void showNoInternetMessage() {
        ((ContainerActivity) getActivity()).showNoInternet();
    }

    @Override
    public void showTryAgain() {
        if (!isAdded()) return;

        showTryAgainLayout();
    }

    public void fetchData() {
        if (isAdded() && investedSmallcasesPresenter != null) {
            investedSmallcasesPresenter.fetchUserInvestmentsData();
        }
    }

    @Override
    public void noInvestments() {
        if (!isAdded()) return;

        stopRefreshing();
        swipeRefreshLayout.setVisibility(View.GONE);
        actionText.setText(R.string.find_your_smallcase);
        tryAgain.setVisibility(View.VISIBLE);
        noInvestmentsContainer.setVisibility(View.VISIBLE);
        emptyState.setText("You don't hold any smallcases right now");

        if (exitedSmallcaseCardWithNoCurrentInvestments.getVisibility() == View.VISIBLE) return;

        investmentHelperText.setVisibility(View.VISIBLE);
        setGettingStartedText();
    }

    @Override
    public void showSnackBar(int resId) {
        if (parentView == null) return;

        Snackbar.make(parentView, resId, Snackbar.LENGTH_LONG).show();
    }

    private void setGettingStartedText() {
        SpannableString spannableString = new SpannableString("Need help getting started with smallcase?\n Read the user guide");
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.blue_800)), spannableString.length() - 19,
                spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Map<String, Object> metaData = new HashMap<>();
                metaData.put("accessedFrom", "Investments");
                analyticsContract.sendEvent(metaData, "Clicked FAQs", Analytics.INTERCOM);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(USING_SMALLCASE_URL)));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        spannableString.setSpan(clickableSpan, spannableString.length() - 19, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        investmentHelperText.setMovementMethod(LinkMovementMethod.getInstance());
        investmentHelperText.setText(spannableString);
    }

    private void showTryAgainLayout() {
        if (!isAdded()) return;

        stopRefreshing();
        swipeRefreshLayout.setVisibility(View.GONE);
        noInvestmentsContainer.setVisibility(View.VISIBLE);
        emptyState.setText(getString(R.string.something_wrong));
        tryAgain.setVisibility(View.VISIBLE);
    }

    private void hideErrorIfVisible() {
        if (noInvestmentsContainer.getVisibility() == View.VISIBLE || tryAgain.getVisibility() == View.VISIBLE) {
            noInvestmentsContainer.setVisibility(View.GONE);
            tryAgain.setVisibility(View.GONE);
            swipeRefreshLayout.setVisibility(View.VISIBLE);
            investmentHelperText.setVisibility(View.GONE);
        }

        if (networkHelper.isNetworkAvailable()) {
            ((ContainerActivity) getActivity()).internetAvailable();
        }
    }

    private void stopRefreshing() {
        if (null != swipeRefreshLayout && swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }
}
