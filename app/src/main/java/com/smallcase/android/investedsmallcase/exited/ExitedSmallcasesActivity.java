package com.smallcase.android.investedsmallcase.exited;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.smallcase.android.R;
import com.smallcase.android.data.model.ExitedSmallcase;
import com.smallcase.android.data.repository.InvestmentRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.user.order.BatchDetailsActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.android.GraphikText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExitedSmallcasesActivity extends AppCompatActivity implements ExitedSmallcasesContract.View {
    public static final String EXITED_SMALLCASES = "exited_smallcases"; // Optional

    @BindView(R.id.tool_bar)
    Toolbar toolbar;
    @BindView(R.id.text_tool_bar_title)
    GraphikText toolBarTitle;
    @BindView(R.id.exited_smallcase_list)
    RecyclerView exitedSmallcaseList;
    @BindView(R.id.parent)
    View parent;
    @BindView(R.id.loading_indicator)
    View loadingIndicator;

    private ExitedSmallcasePresenter exitedSmallcasePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exited_smallcases);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }
        toolBarTitle.setText(R.string.investment_history);
        exitedSmallcaseList.setLayoutManager(new LinearLayoutManager(this));
        exitedSmallcaseList.setHasFixedSize(true);

        List<ExitedSmallcase> exitedSmallcases = getIntent().getParcelableArrayListExtra(EXITED_SMALLCASES);
        if (exitedSmallcases != null && !exitedSmallcases.isEmpty()) {
            onExitedSmallcasesReceived(exitedSmallcases);
        } else {
            exitedSmallcasePresenter = new ExitedSmallcasePresenter(this, new SharedPrefService(this), new InvestmentRepository(),
                    new NetworkHelper(this));
            exitedSmallcasePresenter.getExitedSmallcases();
        }
    }

    @OnClick(R.id.back)
    void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if (exitedSmallcasePresenter != null) exitedSmallcasePresenter.destroySubscriptions();
        super.onDestroy();
    }

    @Override
    public void showSnackBar(int errResId) {
        AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_LONG, errResId);
    }

    @Override
    public void onExitedSmallcasesReceived(List<ExitedSmallcase> exitedSmallcases) {
        Collections.reverse(exitedSmallcases);
        loadingIndicator.setVisibility(View.GONE);
        exitedSmallcaseList.setAdapter(new ExitedAdapter(exitedSmallcases));
    }

    class ExitedAdapter extends RecyclerView.Adapter<ExitedAdapter.ExitedViewHolder> {
        private final String TAG = ExitedAdapter.class.getSimpleName();
        private List<ExitedSmallcase> exitedSmallcases;
        private final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault());

        ExitedAdapter(List<ExitedSmallcase> exitedSmallcases) {
            this.exitedSmallcases = new ArrayList<>(exitedSmallcases);
        }

        @Override
        public ExitedAdapter.ExitedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.exited_smallcase_card, parent, false);
            return new ExitedViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ExitedAdapter.ExitedViewHolder holder, int position) {
            final ExitedSmallcase exitedSmallcase = exitedSmallcases.get(position);

            if (position == getItemCount() - 1) {
                holder.divider.setVisibility(View.INVISIBLE);
            } else {
                holder.divider.setVisibility(View.VISIBLE);
            }

            String smallcaseImage = AppConstants.SMALLCASE_IMAGE_URL + "80/" + exitedSmallcase.getScid() + ".png";
            double totalPAndL = exitedSmallcase.getReturns().getRealizedReturns() + exitedSmallcase.getReturns().getDivReturns();
            Glide.with(ExitedSmallcasesActivity.this)
                    .load(smallcaseImage)
                    .asBitmap()
                    .placeholder(getResources().getDrawable(R.color.grey_300))
                    .into(new BitmapImageViewTarget(holder.smallcaseImage) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable roundImage = RoundedBitmapDrawableFactory.create(getResources(), resource);
                            roundImage.setCornerRadius(AppUtils.getInstance().dpToPx(2));
                            holder.smallcaseImage.setImageDrawable(roundImage);
                        }
                    });
            holder.smallcaseName.setText(exitedSmallcase.getName());
            holder.realizedAmount.setText(getRealizedAmount(totalPAndL));
            holder.realizedAmount.setTextColor(totalPAndL >= 0 ? ContextCompat.getColor(ExitedSmallcasesActivity.this,
                    R.color.green_600) : ContextCompat.getColor(ExitedSmallcasesActivity.this, R.color.red_600));
            holder.broughtOn.setText(getReadableDate(exitedSmallcase.getDate(), true));
            holder.soldOn.setText(getReadableDate(exitedSmallcase.getDateSold(), false));
            holder.exitedSmallcaseCard.setOnClickListener(v -> {
                Intent intent = new Intent(ExitedSmallcasesActivity.this, BatchDetailsActivity.class);
                intent.putExtra(BatchDetailsActivity.ISCID, exitedSmallcase.get_id());
                intent.putExtra(BatchDetailsActivity.SCID, exitedSmallcase.getScid());
                intent.putExtra(BatchDetailsActivity.SMALLCASE_NAME, exitedSmallcase.getName());
                intent.putExtra(BatchDetailsActivity.SOURCE, exitedSmallcase.getSource());
                startActivity(intent);
            });
        }

        private String getReadableDate(String stringDate, boolean isBoughtOn) {
            Date date = AppUtils.getInstance().getPlainDate(stringDate);
            return (isBoughtOn ? "Bought on " : "Exited on ") + sdf.format(date);
        }

        private String getRealizedAmount(double realizedReturns) {
            return AppConstants.RUPEE_SYMBOL + String.format(Locale.getDefault(), "%.2f", realizedReturns);
        }

        @Override
        public int getItemCount() {
            return exitedSmallcases.size();
        }

        class ExitedViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.image_smallcase) ImageView smallcaseImage;
            @BindView(R.id.text_realized_amount) GraphikText realizedAmount;
            @BindView(R.id.text_smallcase_name) GraphikText smallcaseName;
            @BindView(R.id.text_bought_on) GraphikText broughtOn;
            @BindView(R.id.text_sold_on) GraphikText soldOn;
            @BindView(R.id.exited_smallcase_card) View exitedSmallcaseCard;
            @BindView(R.id.divider) View divider;

            ExitedViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
