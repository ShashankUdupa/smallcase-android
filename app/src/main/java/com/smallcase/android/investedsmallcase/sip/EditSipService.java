package com.smallcase.android.investedsmallcase.sip;

import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserSmallcaseRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import io.sentry.event.Breadcrumb;
import okhttp3.ResponseBody;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 15/09/17.
 */

class EditSipService implements EditSipContract.Service {
    private static final String TAG = "EditSipService";

    private UserSmallcaseRepository userSmallcaseRepository;
    private SharedPrefService sharedPrefService;
    private EditSipContract.View view;
    private CompositeSubscription compositeSubscription;

    EditSipService(UserSmallcaseRepository userSmallcaseRepository, SharedPrefService sharedPrefService, EditSipContract
            .View view) {
        this.userSmallcaseRepository = userSmallcaseRepository;
        this.sharedPrefService = sharedPrefService;
        this.view = view;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void setUpSip(final HashMap<String, String> body) {
        compositeSubscription.add(userSmallcaseRepository.manageSip(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), body)
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        try {
                            JSONObject response = new JSONObject(responseBody.string());
                            if (response.getBoolean("success")) {
                                view.onSipSavedSuccessful();
                            } else {
                                view.onFailedSavingSip();
                            }
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                            SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed setting up sip", body);
                            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; setUpSip()");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        view.onFailedSavingSip();

                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed setting up sip", body);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; setUpSip()");
                    }
                }));
    }

    @Override
    public void endSip(final HashMap<String, String> body) {
        compositeSubscription.add(userSmallcaseRepository.endSip(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), body)
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        try {
                            JSONObject response = new JSONObject(responseBody.string());
                            if (response.getBoolean("success")) {
                                view.onSipEndedSuccessful();
                            } else {
                                view.onFailedEndingSip();
                            }
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                            SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed ending sip", body);
                            SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; dialogPrimaryButtonClicked()");
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        view.onFailedEndingSip();

                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed ending sip", body);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; dialogPrimaryButtonClicked()");
                    }
                }));
    }

    @Override
    public void destroySubscriptions() {
        if (compositeSubscription != null) compositeSubscription.unsubscribe();
    }
}
