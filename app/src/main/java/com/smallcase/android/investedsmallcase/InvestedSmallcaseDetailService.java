package com.smallcase.android.investedsmallcase;

import com.google.gson.Gson;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Constituents;
import com.smallcase.android.data.model.SipDetails;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.sentry.event.Breadcrumb;
import retrofit2.adapter.rxjava.HttpException;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 14/12/16.
 */

class InvestedSmallcaseDetailService implements InvestedSmallcaseDetailContract.Service {
    private static final String TAG = "InvestedSmallcaseDetailService";
    private SharedPrefService sharedPrefService;
    private SmallcaseRepository smallcaseRepository;
    private MarketRepository marketRepository;
    private UserSmallcaseRepository userSmallcaseRepository;
    private UserRepository userRepository;
    private InvestedSmallcaseDetailContract.Presenter presenter;
    private CompositeSubscription compositeSubscription;

    InvestedSmallcaseDetailService(SharedPrefService sharedPrefService, SmallcaseRepository smallcaseRepository, MarketRepository
            marketRepository, InvestedSmallcaseDetailContract.Presenter presenter, UserRepository
            userRepository, UserSmallcaseRepository userSmallcaseRepository) {
        this.sharedPrefService = sharedPrefService;
        this.smallcaseRepository = smallcaseRepository;
        this.marketRepository = marketRepository;
        this.userRepository = userRepository;
        this.userSmallcaseRepository = userSmallcaseRepository;
        this.presenter = presenter;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public String getAuthToken() {
        return sharedPrefService.getAuthorizationToken();
    }

    @Override
    public void getSmallcaseDetail(final String iScid) {
        Map<String, String> data = new HashMap<>();
        data.put("iscid", iScid);
        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Smallcase detail failure", data);

        compositeSubscription.add(userSmallcaseRepository.getInvestedSmallcaseDetails(getAuthToken(), iScid,
                sharedPrefService.getCsrfToken())
                .subscribe(smallcaseDetail -> {
                    List<Constituents> constituentsList = new ArrayList<>(smallcaseDetail.getCurrentConfig().getConstituents());
                    final List<String> sids = new ArrayList<>();
                    for (Constituents constituents : constituentsList) {
                        sids.add(constituents.getSid());
                    }

                    if (sids.isEmpty()) {
                        presenter.onSmallcaseDetailsReceived(smallcaseDetail, null);
                        return;
                    }

                    compositeSubscription.add(marketRepository.getStocksPrice(getAuthToken(), sids, sharedPrefService.getCsrfToken())
                            .subscribe(responseBody -> presenter.onSmallcaseDetailsReceived(smallcaseDetail, responseBody), throwable -> {
                                presenter.onFailedFetchingSmallcaseDetails();

                                if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401)
                                    return;
                                Map<String, String> data1 = new HashMap<>();
                                for (int i = 0; i < sids.size(); i++) {
                                    data1.put("sid: " + i, sids.get(i));
                                }
                                SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Market price failure", data1);
                                SentryAnalytics.getInstance().captureEvent(getAuthToken(), throwable, TAG + "; getSmallcaseDetail()");
                            }));
                }, throwable -> {
                    presenter.onFailedFetchingSmallcaseDetails();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(getAuthToken(), throwable, TAG + "; getSmallcaseDetail()");
                }));
    }

    @Override
    public void getSmallcaseNews(List<String> scids, List<String> sids) {
        compositeSubscription.add(smallcaseRepository.getSmallcaseNews(getAuthToken(), scids, 0, 5,
                sharedPrefService.getCsrfToken(), sids)
                .subscribe(newses -> presenter.onSmallcaseNewsReceived(newses), throwable -> {
                    presenter.onFailedFetchingSmallcaseNews();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(getAuthToken(), throwable, TAG + "; getSmallcaseNews()");
                }));
    }

    @Override
    public void getIfMarketIsOpen() {
        compositeSubscription.add(marketRepository.getIfMarketIsOpen(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(responseBody -> presenter.onMarketStatusReceived(responseBody), throwable -> {
                    presenter.onFailedFetchingMarketStatus();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(getAuthToken(), throwable, TAG + "; getIfMarketIsOpen()");
                }));
    }

    @Override
    public SharedPrefService getSharedPrefService() {
        return sharedPrefService;
    }

    @Override
    public void addMarketOpenReminder(HashMap<String, String> body) {
        compositeSubscription.add(userSmallcaseRepository.addReminder(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken(), body)
                .subscribe(responseBody -> {}, throwable -> {
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(getAuthToken(), throwable, TAG + "; addMarketOpenReminder()");
                }));
    }

    @Override
    public void getReBalances() {
        compositeSubscription.add(userRepository.getPendingActions(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(actions -> presenter.onReBalancesReceived(actions.getReBalanceUpdates()), throwable -> {
                    presenter.onFailedFetchingReBalanceUpdates();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(getAuthToken(), throwable, TAG + "; getReBalances()");
                }));
    }

    @Override
    public void destroySubscriptions() {
        if (compositeSubscription != null) compositeSubscription.unsubscribe();
    }

    @Override
    public void clearCache() {
        sharedPrefService.clearSharedPreference();
    }

    @Override
    public void getSipDetails(String iScid) {
        Map<String, String> data = new HashMap<>();
        data.put("iscid", iScid);
        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Smallcase detail failure", data);
        compositeSubscription.add(userSmallcaseRepository.getSipDetails(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), iScid)
                .subscribe(sipDetails -> presenter.onSipDetailsFetched(sipDetails), throwable -> {
                    presenter.onFailedFetchingSipDetails();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    SentryAnalytics.getInstance().captureEvent(getAuthToken(), throwable, TAG + "; getSipDetails()");
                }));
    }

    @Override
    public void skipSip(final HashMap<String, String> body) {
        compositeSubscription.add(userSmallcaseRepository.skipSip(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken(), body)
                .subscribe(responseBody -> {
                    try {
                        JSONObject response = new JSONObject(responseBody.string());
                        if (response.getBoolean("success")) {
                            presenter.onSipSkippedSuccessful();

                            Gson gson = new Gson();
                            String iScid = body.get("iscid");
                            Set<String> sipUpdates = sharedPrefService.getCachedSipUpdates();
                            HashSet<String> updatedSips = new HashSet<>();
                            for (String sipUpdate : sipUpdates) {
                                SipDetails sipDetails = gson.fromJson(sipUpdate, SipDetails.class);
                                if (!iScid.equals(sipDetails.getIscid())) updatedSips.add(sipUpdate);
                            }
                            sharedPrefService.cacheSipUpdates(updatedSips);
                        } else {
                            presenter.onFailedSkippingSip();
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                        SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed ending sip", body);
                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), e, TAG + "; dialogPrimaryButtonClicked()");
                    }
                }, throwable -> {
                    presenter.onFailedSkippingSip();

                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "Failed ending sip", body);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; dialogPrimaryButtonClicked()");
                }));
    }

    @Override
    public boolean isSipDue(String iScid) {
        Set<String> sipUpdatesSet = sharedPrefService.getCachedSipUpdates();
        Gson gson = new Gson();
        for (String sipString : sipUpdatesSet) {
            SipDetails sipDetails = gson.fromJson(sipString, SipDetails.class);
            if (iScid.equals(sipDetails.getIscid())) {
                return true;
            }
        }
        return false;
    }
}
