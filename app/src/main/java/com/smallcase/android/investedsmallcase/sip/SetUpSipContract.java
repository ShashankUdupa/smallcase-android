package com.smallcase.android.investedsmallcase.sip;

import android.support.annotation.StringRes;

import com.smallcase.android.data.model.Historical;
import com.smallcase.android.data.model.Points;

import org.json.JSONException;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by shashankm on 14/09/17.
 */

interface SetUpSipContract {
    interface View {

        Calendar getSipStartDate();

        String getFrequency(boolean isApi);

        void dismissLoader();

        void showSnackBar(@StringRes int resId);

        void showSetUpSuccess();

        void showSipGraph(List<Points> investmentPoints, List<Points> sipPoints, double netWorth, double sipAmount);

        Calendar getCalendar();
    }

    interface Presenter {

        void getSipReturnsDetails();

        void setUpSip();

        void onFailedSettingUpSip();

        void onSipSetUpSuccessful();

        void destroySubscribers();

        void onSmallcaseHistoricalReceived(Historical historical);

        void onFailedFetchingSmallcaseHistorical();

        void calculateInvestmentPlot();

        void onStockHistoricalFetched(ResponseBody responseBody) throws JSONException, IOException;

        void onFailedFetchingStockHistorical();

        double getMinimumAmount();
    }

    interface Service {

        void setUpSip(HashMap<String, String> body);

        void destroySubscribers();

        void getSmallcaseHistorical(String scid, String duration);

        void getStockHistorical(List<String> stocks, String duration);
    }
}
