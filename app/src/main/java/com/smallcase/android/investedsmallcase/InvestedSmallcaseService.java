package com.smallcase.android.investedsmallcase;

import com.google.gson.Gson;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.ExitedSmallcase;
import com.smallcase.android.data.model.InvestedSmallcases;
import com.smallcase.android.data.model.Investments;
import com.smallcase.android.data.model.TotalInvestment;
import com.smallcase.android.data.repository.InvestmentRepository;
import com.smallcase.android.data.repository.SharedPrefService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit2.adapter.rxjava.HttpException;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 23/01/17.
 */

class InvestedSmallcaseService implements InvestedSmallcasesContract.Service {
    private static final String TAG = "InvestedSmallcaseService";
    private SharedPrefService sharedPrefService;
    private InvestmentRepository investmentRepository;
    private InvestedSmallcasesContract.Presenter investedSmallcasePresenterContract;
    private Gson gson;
    private CompositeSubscription compositeSubscription;

    InvestedSmallcaseService(SharedPrefService sharedPrefService, InvestmentRepository investmentRepository,
                             InvestedSmallcasesContract.Presenter investedSmallcasePresenterContract) {
        this.sharedPrefService = sharedPrefService;
        this.investmentRepository = investmentRepository;
        this.investedSmallcasePresenterContract = investedSmallcasePresenterContract;
        gson = new Gson();
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public String getAuth() {
        return sharedPrefService.getAuthorizationToken();
    }

    @Override
    public List<InvestedSmallcases> getCachedInvestedSmallcases() {
        Set<String> stringSet = new HashSet<>(sharedPrefService.getInvestedSmallcases());
        List<InvestedSmallcases> investedSmallcases = new ArrayList<>();
        for (String json : stringSet) {
            investedSmallcases.add(gson.fromJson(json, InvestedSmallcases.class));
        }
        return investedSmallcases;
    }

    @Override
    public TotalInvestment getCachedTotalInvestments() {
        return gson.fromJson(sharedPrefService.getTotalInvestments(), TotalInvestment.class);
    }

    @Override
    public void getInvestments() {
        compositeSubscription.add(investmentRepository.getInvestments(getAuth(), sharedPrefService.getCsrfToken())
                .subscribe(new Action1<Investments>() {
                    @Override
                    public void call(Investments investments) {
                        investedSmallcasePresenterContract.onInvestmentsReceived(investments
                                .getInvestedSmallcases());
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        investedSmallcasePresenterContract.onFailedFetchingInvestments();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(getAuth(), throwable, TAG + "; getInvestments()");
                    }
                }));
    }

    @Override
    public void getTotalInvestment() {
        compositeSubscription.add(investmentRepository.getTotalInvestments(getAuth(), sharedPrefService.getCsrfToken())
                .subscribe(new Action1<TotalInvestment>() {
                    @Override
                    public void call(TotalInvestment totalInvestment) {
                        investedSmallcasePresenterContract.onTotalInvestmentReceived(totalInvestment);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        investedSmallcasePresenterContract.onFailedFetchingTotalInvestment();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(getAuth(), throwable, TAG + "; getTotalInvestments()");
                    }
                }));
    }

    @Override
    public void getExitedSmallcases() {
        compositeSubscription.add(investmentRepository.getExitedSmallcases(getAuth(), sharedPrefService.getCsrfToken())
                .subscribe(new Action1<List<ExitedSmallcase>>() {
                    @Override
                    public void call(List<ExitedSmallcase> exitedSmallcases) {
                        investedSmallcasePresenterContract.onExitedSmallcasesReceived(exitedSmallcases);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        investedSmallcasePresenterContract.onFailedFetchingExitedSmallcases();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(getAuth(), throwable, TAG + "; getExitedSmallcases()");
                    }
                }));
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }
}
