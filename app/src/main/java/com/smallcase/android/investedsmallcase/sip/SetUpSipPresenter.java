package com.smallcase.android.investedsmallcase.sip;

import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.smallcase.android.R;
import com.smallcase.android.data.model.Historical;
import com.smallcase.android.data.model.Points;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.view.model.Stock;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;

/**
 * Created by shashankm on 14/09/17.
 */

class SetUpSipPresenter implements SetUpSipContract.Presenter, StockHelper.StockPriceCallBack {
    private static final String TAG = "SetUpSipPresenter";
    private String scid, iScid;
    private SetUpSipContract.Service service;
    private SetUpSipContract.View view;
    private double minInvestmentAmount = -1;
    private StockHelper stockHelper;
    private List<Stock> stockList;
    private List<Points> pointsList;
    private MarketRepository marketRepository;
    private SharedPrefService sharedPrefService;
    private String source;
    private SmallcaseHelper smallcaseHelper;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());

    SetUpSipPresenter(String scid, String iScid, UserSmallcaseRepository userSmallcaseRepository, SharedPrefService sharedPrefService,
            SetUpSipContract.View view, SmallcaseRepository smallcaseRepository, StockHelper stockHelper, MarketRepository
            marketRepository, List<Stock> stockList, String source, SmallcaseHelper smallcaseHelper) {
        this.scid = scid;
        this.iScid = iScid;
        this.view = view;
        this.stockHelper = stockHelper;
        this.sharedPrefService = sharedPrefService;
        this.marketRepository = marketRepository;
        this.stockList = stockList;
        this.source = source;
        this.smallcaseHelper = smallcaseHelper;
        this.service = new SetUpSipService(userSmallcaseRepository, sharedPrefService, this, smallcaseRepository, marketRepository);
    }

    @Override
    public void getSipReturnsDetails() {
        List<String> sids = new ArrayList<>();
        for (Stock stock : stockList) {
            sids.add(stock.getSid());
        }
        if (SmallcaseSource.PROFESSIONAL.equals(source)) {
            service.getSmallcaseHistorical(scid, "3y");
        } else {
            service.getStockHistorical(sids, "3y");
        }
        stockHelper.getStockPrices(marketRepository, sharedPrefService, sids, this);
    }

    @Override
    public void setUpSip() {
        HashMap<String, String> body = new HashMap<>();
        body.put("iscid", iScid);
        body.put("scid", scid);
        body.put("scheduledDate", AppUtils.getInstance().getDateInApiSendableFormat(view.getSipStartDate()));
        body.put("frequency", view.getFrequency(true));
        service.setUpSip(body);
    }

    @Override
    public void onFailedSettingUpSip() {
        view.dismissLoader();
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onSipSetUpSuccessful() {
        view.dismissLoader();
        view.showSetUpSuccess();
    }

    @Override
    public void destroySubscribers() {
        service.destroySubscribers();
    }

    @Override
    public void onSmallcaseHistoricalReceived(Historical historical) {
        pointsList = historical.getPoints();
        calculateInvestmentPlot();
    }

    @Override
    public void onFailedFetchingSmallcaseHistorical() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onStockPricesReceived(HashMap<String, Double> stockMap) {
        for (Stock stock : stockList) {
            double price = stockMap.get(stock.getSid());
            stock.setPrice(price);
        }

        stockHelper.calculateWeightageFromShares(stockList);
        minInvestmentAmount = stockHelper.calculateSharesAndMinAmountFromWeight(stockList, -1).getMinAmount();
        calculateInvestmentPlot();
    }

    @Override
    public void onFailedFetchingStockPrice(Throwable throwable, @Nullable String sid) {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void calculateInvestmentPlot() {
        if (minInvestmentAmount == -1 || pointsList == null) return;

        List<Points> investmentPoints = new ArrayList<>();
        List<Points> sipPoints = new ArrayList<>();

        double netWorth = 0;
        double sipAmount = 0;
        Calendar calendar = view.getCalendar();
        getDateFromFrequency(calendar, false);

        long daysInMilli = 1000 * 60 * 60 * 24; // 1000 millSec; 60 sec; 60 mints; 24 hours

        double previousIndex = pointsList.get(0).getIndex();

        List<Date> acceptedDates = new ArrayList<>();
        /*
         Pick a date in the previous cycle which is just greater than today's date.
         For example if the selected frequency is monthly and current date is Sep 22 2017,
         select a date, for which we have a data point, from Aug 2017 so that it's slightly greater
         than 22 (like 23, 24, 25...)
        */
        for (int i = pointsList.size() - 1; i >= 0; i--) {
            Date date = AppUtils.getInstance().getPlainDate(pointsList.get(i).getDate());
            long difference = date.getTime() - calendar.getTime().getTime();
            long elapsedDays = difference / daysInMilli;
            if (elapsedDays < 0) {
                int nextPoint = i == pointsList.size() - 1? i : i + 1;
                acceptedDates.add(0, AppUtils.getInstance().getPlainDate(pointsList.get(nextPoint).getDate()));
                getDateFromFrequency(calendar, false);
            }
        }

        int j = 0, i = 0;

        if (acceptedDates.isEmpty()) return;

        // Loop till you find the first accepted date
        Date firstAcceptedDate = acceptedDates.get(0);
        while (i < pointsList.size()) {
            Date date = AppUtils.getInstance().getPlainDate(pointsList.get(i).getDate());
            if (date.compareTo(firstAcceptedDate) == 0) break;
            i++;
        }

        while (i < pointsList.size()) {
            Date comparingDate = acceptedDates.get(j);
            Date date = AppUtils.getInstance().getPlainDate(pointsList.get(i).getDate());

            if (date.compareTo(comparingDate) == 0) {
                // Add minimum investment amount if date is the accepted date
                sipAmount += minInvestmentAmount;
                netWorth += minInvestmentAmount;
                // Increment frequency (weekly, fortnightly, monthly or quarterly)
                getDateFromFrequency(calendar, true);
                if (j < acceptedDates.size() - 1) j++;
            }

            double changePercent = (pointsList.get(i).getIndex() - previousIndex) / previousIndex;
            // First investment's netWorth should be same as investment amount
            if (j != 1) {
                netWorth = netWorth * (1 + changePercent);
            }
            previousIndex = pointsList.get(i).getIndex();

            // Add all the points to respective lists
            String dateString = simpleDateFormat.format(date);
            Points investmentPoint = new Points();
            investmentPoint.setDate(dateString);
            investmentPoint.setIndex(netWorth);
            investmentPoints.add(investmentPoint);

            Points sipPoint = new Points();
            sipPoint.setDate(dateString);
            sipPoint.setIndex(sipAmount);
            sipPoints.add(sipPoint);
            i++;
        }

        view.showSipGraph(investmentPoints, sipPoints, netWorth, sipAmount);
    }

    @Override
    public void onStockHistoricalFetched(ResponseBody responseBody) throws JSONException, IOException {
        JSONObject response = new JSONObject(responseBody.string());
        JSONObject data = response.getJSONObject("data");
        List<Historical> historicalList = new ArrayList<>();
        Gson gson = new Gson();
        for (Stock stock : stockList) {
            JSONObject stockSid = data.getJSONObject(stock.getSid());
            Historical historical = gson.fromJson(stockSid.toString(), Historical.class);
            historicalList.add(historical);
        }
        pointsList = smallcaseHelper.createHistoricalForStocks(historicalList, stockList, null);
        calculateInvestmentPlot();
    }

    @Override
    public void onFailedFetchingStockHistorical() {
        view.showSnackBar(R.string.something_wrong);
    }

    @Override
    public double getMinimumAmount() {
        return minInvestmentAmount;
    }

    private void getDateFromFrequency(Calendar calendar, boolean isForward) {
        switch (view.getFrequency(true)) {
            case "7d":
                calendar.add(Calendar.DAY_OF_YEAR, isForward? 7 : -7);
                break;

            case "14d":
                calendar.add(Calendar.DAY_OF_YEAR, isForward? 14 : -14);
                break;

            case "1m":
                calendar.add(Calendar.MONTH, isForward? 1 : -1);
                break;

            case "3m":
                calendar.add(Calendar.MONTH, isForward? 3 : -3);
                break;

            default:
                calendar.add(Calendar.MONTH, isForward? 1 : -1);
                break;
        }
    }
}
