package com.smallcase.android.investedsmallcase.sip;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;

import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.model.Points;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.smallcase.SmallcaseHelper;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.CustomTypefaceSpan;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.model.Stock;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SetUpSipActivity extends AppCompatActivity implements SetUpSipContract.View {
    public static final String SCID = "scid";
    public static final String SMALLCASE_NAME = "smallcase_name";
    public static final String ISCID = "iScid";
    public static final String STOCKS = "stocks";
    public static final String SOURCE = "source";
    private static final String TAG = SetUpSipActivity.class.getSimpleName();

    @BindView(R.id.see_historical_container)
    View seeHistoricalContainer;
    @BindView(R.id.investment_returns_container)
    View investmentReturnsContainer;
    @BindView(R.id.legend_layout)
    View legendLayout;
    @BindView(R.id.investment_segment_color)
    View investmentSegmentColor;
    @BindView(R.id.smallcase_value_segment_color)
    View smallcaseValueSegmentColor;
    @BindView(R.id.expected_performance_chart)
    LineChart expectedPerformanceChart;
    @BindView(R.id.every_week)
    AppCompatRadioButton everyWeek;
    @BindView(R.id.every_two_weeks)
    AppCompatRadioButton everyTwoWeeks;
    @BindView(R.id.every_month)
    AppCompatRadioButton everyMonth;
    @BindView(R.id.every_quarter)
    AppCompatRadioButton everyQuarter;
    @BindView(R.id.minimum_investment_amount_button)
    AppCompatRadioButton minInvestmentButton;
    @BindView(R.id.custom_amount)
    AppCompatRadioButton customAmount;
    @BindView(R.id.date_selected)
    GraphikText dateSelected;
    @BindView(R.id.text_tool_bar_title)
    GraphikText toolBarTitle;
    @BindView(R.id.sip_frequency_container)
    RadioGroup sipFrequency;
    @BindView(R.id.sip_success_icon)
    ImageView sipSuccessIcon;
    @BindView(R.id.sip_success_title)
    GraphikText sipSuccessTitle;
    @BindView(R.id.sip_success_description)
    GraphikText sipSuccessDescription;
    @BindView(R.id.continue_to_investment_detail)
    View continueToInvestmentDetails;
    @BindView(R.id.edit_sip)
    View editSip;
    @BindView(R.id.setup_parent)
    View setUpParent;
    @BindView(R.id.parent)
    View parent;
    @BindView(R.id.total_invested)
    GraphikText totalInvested;
    @BindView(R.id.current_value)
    GraphikText currentValue;
    @BindView(R.id.sip_returns)
    GraphikText sipReturnsPercent;

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());

    private DatePickerDialog datePickerDialog;
    private Calendar calendar;
    private SetUpSipContract.Presenter presenter;
    private ProgressDialog progressDialog;
    private String smallcaseName, scid, iScid, source;
    private int selectedYear, selectedMonth, selectedDay;
    private List<String> dates;
    private AnalyticsContract analyticsContract;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up_sip);
        ButterKnife.bind(this);

        smallcaseName = getIntent().getStringExtra(SMALLCASE_NAME);
        scid = getIntent().getStringExtra(SCID);
        iScid = getIntent().getStringExtra(ISCID);
        source = getIntent().getStringExtra(SOURCE);
        List<Stock> stocks = getIntent().getParcelableArrayListExtra(STOCKS);

        toolBarTitle.setText(smallcaseName);
        GradientDrawable smallcaseDrawable = (GradientDrawable) smallcaseValueSegmentColor.getBackground();
        smallcaseDrawable.setColor(ContextCompat.getColor(this, R.color.red_600));

        GradientDrawable sipDrawable = (GradientDrawable) investmentSegmentColor.getBackground();
        sipDrawable.setColor(ContextCompat.getColor(this, R.color.blue_800));

        everyWeek.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM));
        everyTwoWeeks.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM));
        everyMonth.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM));
        everyQuarter.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM));
        minInvestmentButton.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM));
        customAmount.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM));

        expectedPerformanceChart.getXAxis().setDrawLabels(true);
        expectedPerformanceChart.getAxisRight().setEnabled(false);
        expectedPerformanceChart.getAxisLeft().setEnabled(false);
        expectedPerformanceChart.getDescription().setEnabled(false);
        expectedPerformanceChart.setDrawGridBackground(false);
        expectedPerformanceChart.getAxisRight().setDrawLabels(false);
        expectedPerformanceChart.getAxisLeft().setDrawLabels(false);
        expectedPerformanceChart.getXAxis().setDrawAxisLine(true);
        expectedPerformanceChart.getLegend().setEnabled(false);
        expectedPerformanceChart.setDrawMarkers(false);
        expectedPerformanceChart.setTouchEnabled(true);
        expectedPerformanceChart.setPinchZoom(false);
        expectedPerformanceChart.setScaleEnabled(false);
        expectedPerformanceChart.setDoubleTapToZoomEnabled(false);
        expectedPerformanceChart.getXAxis().setDrawGridLines(false);
        expectedPerformanceChart.setExtraRightOffset(25);
        expectedPerformanceChart.setExtraLeftOffset(25);

        XAxis xAxis = expectedPerformanceChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(7f);
        xAxis.setLabelCount(4, true);
        xAxis.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR));
        xAxis.setTextColor(ContextCompat.getColor(this, R.color.secondary_text));

        minInvestmentButton.setChecked(true);

        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + 1);
        selectedYear = calendar.get(Calendar.YEAR);
        selectedMonth = calendar.get(Calendar.MONTH);
        selectedDay = calendar.get(Calendar.DAY_OF_MONTH);

        analyticsContract = new AnalyticsManager(this);
        presenter = new SetUpSipPresenter(scid, iScid, new UserSmallcaseRepository(), new SharedPrefService(this), this,
                new SmallcaseRepository(), new StockHelper(), new MarketRepository(), stocks, source, new SmallcaseHelper());

        everyMonth.setChecked(true);

        datePickerDialog = new DatePickerDialog(this, (view, year, month, dayOfMonth) -> {
            selectedYear = year;
            selectedMonth = month;
            selectedDay = dayOfMonth;
            calendar.set(year, month, dayOfMonth);
            dateSelected.setText(simpleDateFormat.format(new Date(calendar.getTimeInMillis())));
            presenter.calculateInvestmentPlot();
        }, selectedYear, selectedMonth, selectedDay);

        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
        dateSelected.setText(simpleDateFormat.format(new Date(calendar.getTimeInMillis())));

        sipFrequency.setOnCheckedChangeListener((group, checkedId) -> {
            switch (checkedId) {
                case R.id.every_week:
                    presenter.calculateInvestmentPlot();
                    break;

                case R.id.every_two_weeks:
                    presenter.calculateInvestmentPlot();
                    break;

                case R.id.every_month:
                    presenter.calculateInvestmentPlot();
                    break;

                case R.id.every_quarter:
                    presenter.calculateInvestmentPlot();
                    break;
            }
        });

        String everyWeekText = "(Every week)";
        String everyTwoWeeksText = "(Every 2 weeks)";

        SpannableString weekly = new SpannableString("Weekly  " + everyWeekText);
        weekly.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR)),
                weekly.length() - everyWeekText.length(), weekly.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        weekly.setSpan(new RelativeSizeSpan(0.9f), weekly.length() - everyWeekText.length(), weekly.length(),
                Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        weekly.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.secondary_text)), weekly.length()
                - everyWeekText.length(), weekly.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        SpannableString fortnightly = new SpannableString("Fortnightly  " + everyTwoWeeksText);
        fortnightly.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR)),
                fortnightly.length() - everyTwoWeeksText.length(), fortnightly.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        fortnightly.setSpan(new RelativeSizeSpan(0.9f), fortnightly.length() - everyTwoWeeksText.length(), fortnightly.length(),
                Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        fortnightly.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.secondary_text)), fortnightly
                .length() - everyTwoWeeksText.length(), fortnightly.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        everyWeek.setText(weekly);
        everyTwoWeeks.setText(fortnightly);

        String comingSoon = "COMING SOON";
        SpannableString spannableString = new SpannableString("Custom    " + comingSoon);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.purple_600)),
                spannableString.length() - comingSoon.length(), spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new RelativeSizeSpan(0.7f), spannableString.length() - comingSoon.length(),
                spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        customAmount.setText(spannableString);
        presenter.getSipReturnsDetails();
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.date_selection_layout)
    public void showCalender() {
        datePickerDialog.show();
    }

    @OnClick(R.id.sip_performance_card)
    public void showSipPerformanceCard() {
        if (expectedPerformanceChart.getVisibility() == View.VISIBLE) return;

        legendLayout.setVisibility(View.VISIBLE);
        investmentReturnsContainer.setVisibility(View.VISIBLE);
        expectedPerformanceChart.setVisibility(View.VISIBLE);
        seeHistoricalContainer.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        presenter.destroySubscribers();
        super.onDestroy();
    }

    @OnClick(R.id.set_up_sip)
    public void setUpSip() {
        if (sipFrequency.getCheckedRadioButtonId() == -1) return;

        progressDialog = ProgressDialog.show(this, "", "Loading...");
        presenter.setUpSip();
    }

    @Override
    public Calendar getSipStartDate() {
        return calendar;
    }

    @Override
    public String getFrequency(boolean isApi) {
        String sip;
        switch (sipFrequency.getCheckedRadioButtonId()) {
            case R.id.every_week:
                sip = isApi ? "7d" : "weekly";
                break;

            case R.id.every_two_weeks:
                sip = isApi ? "14d" : "fortnightly";
                break;

            case R.id.every_month:
                sip = isApi ? "1m" : "monthly";
                break;

            case R.id.every_quarter:
                sip = isApi ? "3m" : "quarterly";
                break;

            default:
                sip = "";
                break;
        }
        return sip;
    }

    @Override
    public void dismissLoader() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showSnackBar(@StringRes int resId) {
        AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_LONG, resId);
    }

    @Override
    public void showSetUpSuccess() {
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseType", source);
        eventProperties.put("smallcaseName", smallcaseName);
        eventProperties.put("frequency", getFrequency(false));
        eventProperties.put("amount", presenter.getMinimumAmount());
        eventProperties.put("startDate", AppUtils.getInstance().getDateInApiSendableFormat(calendar));
        analyticsContract.sendEvent(eventProperties, "Started SIP", Analytics.MIXPANEL, Analytics.CLEVERTAP);

        sipSuccessTitle.setText("Great, you have started an SIP on\n" + smallcaseName);
        setUpParent.setVisibility(View.GONE);
        sipSuccessIcon.setVisibility(View.VISIBLE);
        sipSuccessTitle.setVisibility(View.VISIBLE);
        sipSuccessDescription.setVisibility(View.VISIBLE);
        continueToInvestmentDetails.setVisibility(View.VISIBLE);
        editSip.setVisibility(View.VISIBLE);

        Glide.with(this)
                .load(R.drawable.sip_success)
                .asBitmap()
                .into(sipSuccessIcon);

        String frequency = getFrequency(false);
        String date = simpleDateFormat.format(new Date(calendar.getTimeInMillis()));
        SpannableString spannableString = new SpannableString("Your " + frequency + " SIP for the minimum investment " +
                "amount starting " + date + " is now active");

        spannableString.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM)),
                5, 5 + frequency.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.primary_text)),
                5, 5 + frequency.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM)),
                5 + frequency.length() + 13, 5 + frequency.length() + 31, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.primary_text)),
                5 + frequency.length() + 13, 5 + frequency.length() + 31, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM)),
                5 + frequency.length() + 48, 5 + frequency.length() + 48 + date.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.primary_text)),
                5 + frequency.length() + 48, 5 + frequency.length() + 48 + date.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        sipSuccessDescription.setText(spannableString);
    }

    @Override
    public void showSipGraph(List<Points> investmentPoints, List<Points> sipPoints, double netWorth, double sipAmount) {
        calendar.set(selectedYear, selectedMonth, selectedDay); // Reset calendar as it's messed with in the presenter :/ (singletons!)

        String minimumAmountText = "(" + AppConstants.RUPEE_SYMBOL + AppConstants.getDecimalNumberFormatter().format(presenter
                .getMinimumAmount()) + " currently)";
        SpannableString spannableString = new SpannableString("Minimum Investment Amount\n" + minimumAmountText);
        spannableString.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR)),
                spannableString.length() - minimumAmountText.length(), spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new RelativeSizeSpan(0.9f), spannableString.length() - minimumAmountText.length(),
                spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        minInvestmentButton.setText(spannableString);

        final List<Entry> smallcaseEntries = new ArrayList<>();
        final List<Entry> sipEntries = new ArrayList<>();
        for (int i = 0; i < investmentPoints.size(); i++) {
            smallcaseEntries.add(new Entry(i, (float) AppUtils.getInstance().round(investmentPoints.get(i).getIndex(), 2)));
            sipEntries.add(new Entry(i, (float) AppUtils.getInstance().round(sipPoints.get(i).getIndex(), 2)));
        }

        final LineDataSet smallcaseDataSet = new LineDataSet(smallcaseEntries, "");
        smallcaseDataSet.setDrawValues(false);
        smallcaseDataSet.setDrawCircleHole(false);
        smallcaseDataSet.setDrawCircles(false);
        smallcaseDataSet.setDrawFilled(false);
        smallcaseDataSet.setColor(ContextCompat.getColor(this, R.color.red_600));
        smallcaseDataSet.setLineWidth(2f);
        smallcaseDataSet.setHighlightLineWidth(1f);
        smallcaseDataSet.setDrawHorizontalHighlightIndicator(false);
        smallcaseDataSet.setHighLightColor(ContextCompat.getColor(this, R.color.grey_300));

        LineDataSet sipDataSet = new LineDataSet(sipEntries, "");
        sipDataSet.setDrawValues(false);
        sipDataSet.setDrawCircleHole(false);
        sipDataSet.setDrawCircles(false);
        sipDataSet.setDrawHorizontalHighlightIndicator(false);
        sipDataSet.setDrawFilled(false);
        sipDataSet.setColor(ContextCompat.getColor(this, R.color.blue_800));
        sipDataSet.setLineWidth(2f);
        sipDataSet.setHighlightLineWidth(1f);
        sipDataSet.setHighLightColor(ContextCompat.getColor(this, R.color.grey_300));

        List<ILineDataSet> lineDataSets = new ArrayList<>();
        lineDataSets.add(smallcaseDataSet);
        lineDataSets.add(sipDataSet);

        LineData lineData = new LineData(lineDataSets);

        if (dates == null) {
            dates = new ArrayList<>();
        } else {
            dates.clear();
        }

        for (Points smallcasePoint : investmentPoints) {
            dates.add(smallcasePoint.getDate());
        }

        XAxis xAxis = expectedPerformanceChart.getXAxis();
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                if (value >= dates.size()) return dates.get(dates.size() - 1);
                return dates.get((int) value);
            }
        });

        expectedPerformanceChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                int pos = (int) e.getX();
                Entry sipEntry = sipEntries.get(pos);
                Entry smallcaseEntry = smallcaseEntries.get(pos);

                double percentDifference = ((smallcaseEntry.getY() - sipEntry.getY()) / sipEntry.getY()) * 100;
                sipReturnsPercent.setText(String.format(Locale.getDefault(), "%.2f", percentDifference) + "%");
                sipReturnsPercent.setTextColor(ContextCompat.getColor(SetUpSipActivity.this, percentDifference < 0 ? R.color.red_600
                        : R.color.green_600));

                Log.d(TAG, "onValueSelected: " + dates.get(pos));
                totalInvested.setText(AppConstants.RUPEE_SYMBOL + AppConstants.getDecimalNumberFormatter().format(sipEntry.getY()));
                currentValue.setText(AppConstants.RUPEE_SYMBOL + AppConstants.getDecimalNumberFormatter().format(smallcaseEntry.getY()));
            }

            @Override
            public void onNothingSelected() {

            }
        });

        expectedPerformanceChart.setData(lineData);
        expectedPerformanceChart.highlightValue(smallcaseEntries.size() - 1, 0);
        expectedPerformanceChart.invalidate();
    }

    @Override
    public Calendar getCalendar() {
        return calendar;
    }

    @OnClick(R.id.edit_sip)
    public void editSip() {
        // Take user to edit sip
        Intent intent = new Intent(this, EditSipActivity.class);
        intent.putExtra(EditSipActivity.SMALLCASE_NAME, smallcaseName);
        intent.putExtra(EditSipActivity.ISCID, iScid);
        intent.putExtra(EditSipActivity.SCID, scid);
        intent.putExtra(EditSipActivity.FREQUENCY, getFrequency(false));
        intent.putExtra(EditSipActivity.NEXT_SIP, calendar.getTimeInMillis());
        intent.putExtra(EditSipActivity.SOURCE, source);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.continue_to_investment_detail)
    public void continueToInvestmentDetails() {
        finish();
    }
}
