package com.smallcase.android.investedsmallcase;

import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.repository.KiteRepository;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.data.repository.UserRepository;
import com.smallcase.android.data.repository.UserSmallcaseRepository;
import com.smallcase.android.investedsmallcase.sip.EditSipActivity;
import com.smallcase.android.investedsmallcase.sip.SetUpSipActivity;
import com.smallcase.android.news.NewsActivity;
import com.smallcase.android.onboarding.LandingActivity;
import com.smallcase.android.onboarding.LoginActivity;
import com.smallcase.android.rebalance.ReBalanceActivity;
import com.smallcase.android.smallcase.smallcasedetail.SmallcaseDetailActivity;
import com.smallcase.android.stock.StockHelper;
import com.smallcase.android.stock.StockInfo;
import com.smallcase.android.user.order.BatchDetailsActivity;
import com.smallcase.android.user.order.ReviewOrderActivity;
import com.smallcase.android.util.Animations;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.util.OrderLabel;
import com.smallcase.android.util.SmallcaseSource;
import com.smallcase.android.view.ItemClickListener;
import com.smallcase.android.view.adapter.NewsAdapter;
import com.smallcase.android.view.adapter.StockAdapterDelegate;
import com.smallcase.android.view.android.CustomTypefaceSpan;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.buy.InvestContract;
import com.smallcase.android.view.buy.InvestAmountPopUp;
import com.smallcase.android.view.model.PInvestedSmallcase;
import com.smallcase.android.view.model.PInvestedSmallcaseReturns;
import com.smallcase.android.view.model.PPlainNews;
import com.smallcase.android.view.model.PStock;
import com.smallcase.android.view.model.Stock;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.smallcase.android.onboarding.LoginActivity.REQUEST_TOKEN_KEY;

public class InvestedSmallcaseDetailActivity extends AppCompatActivity implements InvestedSmallcaseDetailContract.View,
        SwipeRefreshLayout.OnRefreshListener, InvestContract.Response, ItemClickListener {
    public static final String ISCID = "iscid"; // Required
    public static final String SCID = "scid"; // Required
    public static final String SOURCE = "source"; // Required
    public static final String OPEN_INVEST_MORE = "open_invest_more";

    private static final String TAG = "InvestSmallcaseDetAct";
    private static final Interpolator OVERSHOOT_INTERPOLATOR = new OvershootInterpolator();
    private static final Interpolator ANTICIPATE_OVERSHOOT_INTERPOLATOR = new AnticipateInterpolator();
    private static final int LOGIN_REQUEST_CODE = 1;

    private final AccelerateDecelerateInterpolator accelerateDecelerateInterpolator = new AccelerateDecelerateInterpolator();
    private final int sixHundredDp = (int) AppUtils.getInstance().dpToPx(200);

    @BindView(R.id.image_smallcase_up_or_down)
    ImageView smallcaseIndexUpOrDown;
    @BindView(R.id.text_smallcase_index)
    GraphikText smallcaseIndexValue;
    @BindView(R.id.image_smallcase)
    ImageView smallcaseImage;
    @BindView(R.id.text_smallcase_name)
    GraphikText smallcaseName;
    @BindView(R.id.text_smallcase_brought_on)
    GraphikText broughtOn;
    @BindView(R.id.smallcase_detail_list)
    RecyclerView smallcaseDetailsList;
    @BindView(R.id.loading_indicator_main)
    ProgressBar loadingIndicator;
    @BindView(R.id.tool_bar)
    Toolbar toolbar;
    @BindView(R.id.text_tool_bar_title)
    GraphikText toolbarTitle;
    @BindView(R.id.arrow)
    View arrow;

    @BindView(R.id.text_smallcase_p_and_l_percent)
    GraphikText smallcasePAndLPercent;
    @BindView(R.id.text_current_investment_amount)
    GraphikText currentInvestment;
    @BindView(R.id.text_net_worth_amount)
    GraphikText netWorth;
    @BindView(R.id.text_smallcase_p_and_l_amount)
    GraphikText pAndLAmount;
    @BindView(R.id.text_smallcase_realized_p_and_l_amount)
    GraphikText realisedPAndL;
    @BindView(R.id.text_total_amount)
    GraphikText totalAmount;
    @BindView(R.id.text_smallcase_total_p_and_l_amount)
    GraphikText totalPAndL;
    @BindView(R.id.news_list)
    RecyclerView newsList;
    @BindView(R.id.investments_container)
    View investmentsContainer;
    @BindView(R.id.smallcase_invested_card)
    CardView investedCard;
    @BindView(R.id.detail_container)
    View detailContainer;
    @BindView(R.id.news_smallcase)
    View newsSmallcase;
    @BindView(R.id.parent)
    View parentView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.negative_container)
    View negativeContainer;
    @BindView(R.id.negative_title)
    GraphikText negativeTitle;
    @BindView(R.id.negative_reason)
    GraphikText negativeReason;

    @BindView(R.id.see_all_stocks)
    GraphikText seeAllStocks;
    @BindView(R.id.see_or_manage_arrow)
    View seeAllOrManageArrow;
    @BindView(R.id.custom_tag)
    View customTag;
    @BindView(R.id.invest_blur)
    View investBlur;
    @BindView(R.id.smallcase_info_divider)
    View smallcaseInfoDivider;

    @BindView(R.id.re_balance_card)
    View reBalanceCard;
    @BindView(R.id.re_balance_description)
    GraphikText reBalanceDescription;
    @BindView(R.id.re_balance_date)
    GraphikText reBalanceDate;
    @BindView(R.id.rupee_symbol)
    View rupeeSymbol;
    @BindView(R.id.apply_re_balance)
    CardView applyReBalance;
    @BindView(R.id.expand_card)
    View expandArrow;
    @BindView(R.id.smallcase_info_card)
    CardView smallcaseInfoCard;
    @BindView(R.id.invest_more)
    CardView investMoreCard;
    @BindView(R.id.exit_info_blur)
    View exitInfoBlur;

    @BindView(R.id.exit_smallcase_card)
    CardView exitSmallcasePopUpCard;
    @BindView(R.id.exit_min_amount_container)
    View exitMinAmountContainer;
    @BindView(R.id.whole)
    GraphikText whole;
    @BindView(R.id.partial)
    GraphikText partial;
    @BindView(R.id.amount)
    GraphikText amount;
    @BindView(R.id.sell_amount)
    EditText sellAmount;
    @BindView(R.id.sell_amount_divider)
    View sellAmountDivider;
    @BindView(R.id.error_text)
    GraphikText metaText;
    @BindView(R.id.confirm_sell_amount)
    View confirmSellAmount;
    @BindView(R.id.loading_sell_amount)
    View loadingSellAmount;
    @BindView(R.id.exit_type_layout)
    View exitTypeLayout;
    @BindView(R.id.exit_confirmation_text)
    GraphikText exitConfirmationText;
    @BindView(R.id.whole_background)
    View wholeBackground;
    @BindView(R.id.partial_background)
    View partialBackground;
    @BindView(R.id.sip_primary_action)
    CardView sipPrimaryAction;
    @BindView(R.id.sip_secondary_action)
    CardView sipSecondaryAction;
    @BindView(R.id.sip_primary_text)
    GraphikText sipPrimaryText;
    @BindView(R.id.sip_available_card)
    View sipCard;
    @BindView(R.id.sip_title)
    GraphikText sipTitle;
    @BindView(R.id.sip_description)
    GraphikText sipDescription;
    @BindView(R.id.exit_smallcase_confirm_card)
    CardView exitSmallcaseCard;
    @BindView(R.id.exit_smallcase_confirm_text)
    GraphikText exitSmallcaseConfirmText;
    @BindView(R.id.start_sip_confirm_card)
    CardView startSipCard;
    @BindView(R.id.start_sip_confirm_text)
    GraphikText startSipText;
    @BindView(R.id.sell_amount_title)
    View sellAmountTitle;
    @BindView(R.id.exit_main_title)
    GraphikText exitMainTitle;
    @BindView(R.id.exit_info_container)
    View exitInfoContainer;
    @BindView(R.id.exit_info_text)
    GraphikText exitInfoText;
    @BindView(R.id.dialog_confirmation)
    View dialogConformation;
    @BindView(R.id.dialog_description)
    GraphikText dialogDescription;
    @BindView(R.id.dialog_primary_button)
    GraphikText dialogPrimaryButton;
    @BindView(R.id.dialog_secondary_button)
    GraphikText dialogSecondaryButton;
    @BindView(R.id.extra_card)
    View extraCard;
    @BindView(R.id.text_smallcase_dividend_amount)
    GraphikText dividendAmount;

    boolean isExpanded;
    InvestedSmallcaseDetailContract.Presenter presenter;
    List<PStock> stockList;
    PInvestedSmallcase investedSmallcase;

    private PInvestedSmallcaseReturns investedSmallcaseReturns;
    private Animations animations;
    private NewsAdapter newsAdapter;
    private String scid, iScid, boughtOn;
    private double index;
    private InvestContract.View investContract;
    private boolean isInvestMore, isWholeExit = true;
    private String source;
    private StockAdapterDelegate stockAdapterDelegate;
    private StockInfo stockInfo;
    private AnalyticsContract analyticsContract;
    private BottomSheetBehavior exitBottomSheet;
    private ProgressDialog progressDialog;
    private boolean shouldOpenInvestMore = false;
    private boolean isSkipSipDialog;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invested_smallcase_detail);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("");
            actionBar.setDisplayHomeAsUpEnabled(false);
            toolbarTitle.setText(getString(R.string.investment_details));
        }

        if (Build.VERSION.SDK_INT < 21) {
            smallcaseInfoCard.setCardElevation(0);
            smallcaseInfoCard.setMaxCardElevation(0);
            exitSmallcasePopUpCard.setCardElevation(0);
            exitSmallcasePopUpCard.setMaxCardElevation(0);
        }

        source = getIntent().getStringExtra(SOURCE);
        shouldOpenInvestMore = getIntent().getBooleanExtra(OPEN_INVEST_MORE, false);

        animations = new Animations();
        smallcaseDetailsList.setLayoutManager(new LinearLayoutManager(this));
        smallcaseDetailsList.setNestedScrollingEnabled(false);
        RecyclerView.ItemAnimator animator = smallcaseDetailsList.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        swipeRefreshLayout.setColorSchemeResources(R.color.blue_800);
        swipeRefreshLayout.setOnRefreshListener(this);

        SharedPrefService sharedPrefService = new SharedPrefService(this);
        MarketRepository marketRepository = new MarketRepository();
        NetworkHelper networkHelper = new NetworkHelper(this);
        StockHelper stockHelper = new StockHelper();

        iScid = getIntent().getStringExtra(ISCID);
        scid = getIntent().getStringExtra(SCID);
        analyticsContract = new AnalyticsManager(getApplicationContext());
        investContract = new InvestAmountPopUp(this, sharedPrefService, marketRepository, stockHelper, new KiteRepository(), this, true, analyticsContract);
        stockInfo = new StockInfo(this, stockHelper, marketRepository, sharedPrefService);

        if (getIntent().getData() != null && getIntent().getData().getPathSegments() != null) {
            List<String> segments = getIntent().getData().getPathSegments();
            if (segments.size() > 0) {
                iScid = segments.get(segments.size() - 1);
            }
        }

        if (iScid != null) {
            presenter = new InvestedSmallcaseDetailPresenter(this, new SmallcaseRepository(), this, sharedPrefService,
                    marketRepository, networkHelper, stockHelper, new UserRepository(), iScid, new UserSmallcaseRepository());
            presenter.fetchSmallcaseDetails(scid);
        }

        if (SmallcaseSource.CUSTOM.equals(source)) {
            customTag.setVisibility(View.VISIBLE);
        }

        exitBottomSheet = BottomSheetBehavior.from(exitSmallcasePopUpCard);
        exitBottomSheet.setHideable(true);
        exitBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
        sellAmount.setTypeface(AppUtils.getInstance().getFont(this, AppConstants.FontType.REGULAR));

        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.PLACED_ORDER_BROADCAST));
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.HARD_RELOAD_BROADCAST));
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.SHOW_SIP_CANCELLED));
    }

    @OnClick(R.id.back)
    void back() {
        onBackPressed();
    }

    @Override
    public void onStocksReceived(List<PStock> stockList) {
        this.stockList = new ArrayList<>(stockList);
        setUpStocksAdapter();
    }

    @Override
    public void showSnackBar(int resId) {
        snackbar = Snackbar.make(parentView, resId, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (scid != null && iScid != null) {
            presenter.fetchSmallcaseDetails(scid);
        }
    }

    @Override
    public void showReBalanceAvailable(String date, String label) {
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver,
                new IntentFilter(AppConstants.RE_BALANCE_IGNORED_BROADCAST));
        reBalanceCard.setVisibility(View.VISIBLE);
        reBalanceDate.setText(date);
        reBalanceDescription.setText(label);

        if (presenter.isSipDue()) {
            reBalanceCard.post(() -> {
                extraCard.setVisibility(View.VISIBLE);
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        reBalanceCard.getMeasuredHeight());
                layoutParams.leftMargin = (int) AppUtils.getInstance().dpToPx(18);
                layoutParams.rightMargin = (int) AppUtils.getInstance().dpToPx(6);
                layoutParams.topMargin = (int) AppUtils.getInstance().dpToPx(30);
                layoutParams.bottomMargin = (int) AppUtils.getInstance().dpToPx(2);
                extraCard.setLayoutParams(layoutParams);
            });
            sipCard.setVisibility(View.GONE);
        } else {
            extraCard.setVisibility(View.GONE);
        }

        if (!presenter.isSmallcaseValid()) {
            applyReBalance.setCardBackgroundColor(ContextCompat.getColor(this, R.color.blue_grey_100));
        } else {
            applyReBalance.setCardBackgroundColor(ContextCompat.getColor(this, R.color.purple_600));
        }
    }

    @Override
    public void redirectToLogin() {
        startActivity(new Intent(this, LandingActivity.class));
        finish();
    }

    @Override
    public void hideExitCard() {
        exitBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
        sellWhole();
        loadingSellAmount.setVisibility(View.VISIBLE);
        exitMinAmountContainer.setVisibility(View.INVISIBLE);
    }

    @Override
    public void noPartialExitAllowed(double totalAmount) {
        exitTypeLayout.setVisibility(View.GONE);
        metaText.setVisibility(View.VISIBLE);

        String exitTitle = "Exiting a smallcase\n\nThere are 2 ways of exiting a smallcase\n\n";
        String completeExitDescription = "Complete Exit: You can exit your smallcase by placing sell orders for all the stocks in the smallcase\n\n";
        String partialExitDescription = "Partial Exit: You can exit a part of your smallcase while ensuring minimum deviation in the weighting scheme\n\n";
        String generalInfo = "However, you can't exit partially from this smallcase right now to maintain the current weighing scheme";
        SpannableString exitInfo = new SpannableString(exitTitle + completeExitDescription + partialExitDescription + generalInfo);
        exitInfo.setSpan(new RelativeSizeSpan(1.2f), 0, 19, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        exitInfo.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM)), 0,
                19, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        exitInfo.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.primary_text)), 0, 19, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        exitInfo.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM)),
                exitTitle.length(), exitTitle.length() + 14, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        exitInfo.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.primary_text)), exitTitle.length(),
                exitTitle.length() + 14, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        exitInfo.setSpan(new CustomTypefaceSpan("", AppUtils.getInstance().getFont(this, AppConstants.FontType.MEDIUM)),
                exitTitle.length() + completeExitDescription.length(), exitTitle.length() + completeExitDescription
                        .length() + 13, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        exitInfo.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.primary_text)), exitTitle.length() +
                completeExitDescription.length(), exitTitle.length() + completeExitDescription.length() + 13, Spanned
                .SPAN_INCLUSIVE_EXCLUSIVE);
        exitInfoText.setText(exitInfo);

        exitMainTitle.setText(getString(R.string.sell_amount));
        sellAmountTitle.setVisibility(View.GONE);
        exitConfirmationText.setText("CONTINUE");
        SpannableString spannableString = new SpannableString("You can only exit this smallcase completely\nLearn More");
        spannableString.setSpan(new RelativeSizeSpan(1.0f), 0, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.blue_800)), spannableString
                .length() - 10, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                hideExitCard();
                animations.scaleIntoPlaceAnimation(exitInfoContainer);

                exitInfoBlur.setVisibility(View.VISIBLE);
                exitInfoBlur.setClickable(true);
                exitInfoBlur.setOnClickListener(v -> animations.scaleOutAnimation(exitInfoContainer, exitInfoBlur));
            }
        }, spannableString.length() - 10, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        metaText.setMovementMethod(LinkMovementMethod.getInstance());
        metaText.setText(spannableString);

        loadingSellAmount.setVisibility(View.GONE);
        exitMinAmountContainer.setVisibility(View.VISIBLE);
        String exitAmount = AppConstants.RUPEE_SYMBOL + String.format(Locale.getDefault(), "%.2f", totalAmount);
        amount.setText(exitAmount);
        exitBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    public void exitWhole(List<Stock> wholeExitStockList, double amountYouReceive) {
        for (Stock stock : wholeExitStockList) {
            stock.setBuy(false);
        }
        Intent intent = new Intent(this, ReviewOrderActivity.class);
        intent.putParcelableArrayListExtra(ReviewOrderActivity.STOCK_LIST, (ArrayList<? extends Parcelable>) wholeExitStockList);
        intent.putExtra(ReviewOrderActivity.SCID, scid);
        intent.putExtra(ReviewOrderActivity.ISCID, iScid);
        intent.putExtra(ReviewOrderActivity.SMALLCASE_NAME, investedSmallcase.getSmallcaseName());
        intent.putExtra(ReviewOrderActivity.INVESTMENT_AMOUNT, amountYouReceive);
        intent.putExtra(ReviewOrderActivity.ORDER_LABEL, OrderLabel.SELL_ALL);
        intent.putExtra(ReviewOrderActivity.SOURCE, source);
        startActivity(intent);
        overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    }

    @Override
    public void showInvalidAmountError(SpannableString spannableString) {
        exitMinAmountContainer.setVisibility(View.VISIBLE);
        loadingSellAmount.setVisibility(View.GONE);

        metaText.setText(spannableString);
    }

    @Override
    public void continuePartialExit(double finalAmount, List<Stock> stocksToSell) {
        Intent intent = new Intent(this, ReviewOrderActivity.class);
        intent.putParcelableArrayListExtra(ReviewOrderActivity.STOCK_LIST, (ArrayList<? extends Parcelable>) stocksToSell);
        intent.putExtra(ReviewOrderActivity.SCID, scid);
        intent.putExtra(ReviewOrderActivity.ISCID, iScid);
        intent.putExtra(ReviewOrderActivity.SMALLCASE_NAME, investedSmallcase.getSmallcaseName());
        intent.putExtra(ReviewOrderActivity.INVESTMENT_AMOUNT, finalAmount);
        intent.putExtra(ReviewOrderActivity.ORDER_LABEL, OrderLabel.PARTIAL_EXIT);
        intent.putExtra(ReviewOrderActivity.SOURCE, source);
        startActivity(intent);
        overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    }

    @Override
    public RecyclerView getRecyclerView() {
        return smallcaseDetailsList;
    }


    @Override
    public void hideStartSipButton() {
        startSipCard.setVisibility(View.GONE);
    }

    @Override
    public void showStartSipButton() {
        startSipCard.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSipDue() {
        if (presenter.isReBalancePending()) {
            extraCard.setVisibility(View.VISIBLE);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    reBalanceCard.getMeasuredHeight());
            layoutParams.leftMargin = (int) AppUtils.getInstance().dpToPx(18);
            layoutParams.rightMargin = (int) AppUtils.getInstance().dpToPx(6);
            layoutParams.topMargin = (int) AppUtils.getInstance().dpToPx(30);
            layoutParams.bottomMargin = (int) AppUtils.getInstance().dpToPx(2);
            extraCard.setLayoutParams(layoutParams);
            return;
        }

        extraCard.setVisibility(View.GONE);
        sipCard.setVisibility(View.VISIBLE);
        sipPrimaryText.setText("INVEST MORE");
        sipPrimaryText.setTextColor(ContextCompat.getColor(this, R.color.white));
        sipPrimaryAction.setCardBackgroundColor(ContextCompat.getColor(this, R.color.blue_800));

        sipTitle.setText("SIP Instalment Due");
        sipDescription.setText("SIP instalment for the minimum investment amount is due");
    }

    @Override
    public void showSipDetails(SpannableString sipString) {
        if (presenter.isReBalancePending()) {
            extraCard.setVisibility(View.VISIBLE);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    reBalanceCard.getMeasuredHeight());
            layoutParams.leftMargin = (int) AppUtils.getInstance().dpToPx(18);
            layoutParams.rightMargin = (int) AppUtils.getInstance().dpToPx(6);
            layoutParams.topMargin = (int) AppUtils.getInstance().dpToPx(30);
            layoutParams.bottomMargin = (int) AppUtils.getInstance().dpToPx(2);
            extraCard.setLayoutParams(layoutParams);
            return;
        }

        extraCard.setVisibility(View.GONE);
        sipCard.setVisibility(View.VISIBLE);
        sipSecondaryAction.setVisibility(View.GONE);
        sipPrimaryText.setText("EDIT SIP");
        sipPrimaryText.setTextColor(ContextCompat.getColor(this, R.color.blue_800));
        sipPrimaryAction.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));

        sipTitle.setText("SIP Active");
        sipDescription.setText(sipString);
    }

    @Override
    public void hideSipCard() {
        sipCard.setVisibility(View.GONE);
    }

    @Override
    public void hideLoader() {
        if (progressDialog != null) progressDialog.dismiss();
    }

    @Override
    public void enableReBalance() {
        applyReBalance.setCardBackgroundColor(ContextCompat.getColor(this, R.color.purple_600));
    }

    @OnClick(R.id.sip_primary_action)
    public void sipPrimaryAction() {
        if (presenter.isSipDue()) {
            onInvestMoreClicked();
            return;
        }

        // Open Edit SIP
        Intent intent = new Intent(this, EditSipActivity.class);
        intent.putExtra(EditSipActivity.SCID, scid);
        intent.putExtra(EditSipActivity.ISCID, iScid);
        intent.putExtra(EditSipActivity.FREQUENCY, presenter.getSipFrequency());
        intent.putExtra(EditSipActivity.NEXT_SIP, presenter.getNextSip());
        intent.putExtra(EditSipActivity.SMALLCASE_NAME, smallcaseName.getText().toString());
        intent.putExtra(EditSipActivity.SOURCE, source);
        startActivity(intent);
    }

    @OnClick(R.id.sip_secondary_action)
    public void sipSecondaryAction() {
        // Skip sip
        dialogDescription.setText(getString(R.string.skip_sip_confirmation));
        dialogPrimaryButton.setText(getString(R.string.skip_sip));
        dialogSecondaryButton.setText(getString(R.string.dont_skip_sip));
        isSkipSipDialog = true;

        animations.scaleIntoPlaceAnimation(dialogConformation);
        exitInfoBlur.setVisibility(View.VISIBLE);
        exitInfoBlur.setOnClickListener(v -> hideConfirmationDialog());
    }

    private void hideConfirmationDialog() {
        animations.scaleOutAnimation(dialogConformation, exitInfoBlur);
    }

    @OnClick(R.id.dialog_primary_button)
    public void dialogPrimaryButtonClicked() {
        hideConfirmationDialog();
        if (isSkipSipDialog) {
            // Skip sip confirmed
            HashMap<String, String> body = new HashMap<>();
            body.put("iscid", iScid);

            progressDialog = ProgressDialog.show(InvestedSmallcaseDetailActivity.this, "", "Loading...");
            presenter.skipSip(body);

            Map<String, Object> eventProperties = new HashMap<>();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date(presenter.getNextSip()));
            eventProperties.put("smallcaseName", smallcaseName.getText().toString());
            eventProperties.put("smallcaseType", source);
            eventProperties.put("frequency", presenter.getSipFrequency());
            eventProperties.put("sipDate", AppUtils.getInstance().getDateInApiSendableFormat(calendar));
            analyticsContract.sendEvent(eventProperties, "Skipped SIP", Analytics.MIXPANEL, Analytics.CLEVERTAP);
            return;
        }

        // View re-balance update
        onApplyReBalanceClicked();
    }

    @OnClick(R.id.dialog_secondary_button)
    public void dialogSecondaryButtonClicked() {
        hideConfirmationDialog(); // Decided to not skip sip
        if (isSkipSipDialog) return;

        // Invest more
        isInvestMore = true;
        presenter.checkMarketStatus();
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (AppConstants.PLACED_ORDER_BROADCAST.equals(action)) {
                if (!isFinishing()) {
                    finish();
                }
                return;
            }

            if (AppConstants.HARD_RELOAD_BROADCAST.equals(action)) {
                if (!isFinishing()) {
                    swipeRefreshLayout.setVisibility(View.GONE);
                    loadingIndicator.setVisibility(View.VISIBLE);
                    presenter.fetchSmallcaseDetails(scid);
                }
                return;
            }

            if (AppConstants.SHOW_SIP_CANCELLED.equals(action)) {
                // Let the activity load, jeez.
                new Handler().post(() -> {
                    if (!isFinishing()) {
                        showSnackBar(R.string.sip_cancel_successful);
                    }
                });

                return;
            }

            if (!isFinishing()) {
                reBalanceCard.setVisibility(View.GONE);
            }
        }
    };

    @OnClick(R.id.apply_re_balance)
    public void onApplyReBalanceClicked() {
        if (!presenter.isSmallcaseValid()) return;

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("accessedFrom", "Investment Details");
        eventProperties.put("updateDate", reBalanceDate.getText().toString());
        eventProperties.put("updateName", reBalanceDescription.getText().toString());
        eventProperties.put("smallcaseName", smallcaseName.getText().toString());
        analyticsContract.sendEvent(eventProperties, "Viewed Rebalance Update", Analytics.MIXPANEL, Analytics.CLEVERTAP);

        Intent intent = new Intent(this, ReBalanceActivity.class);
        intent.putExtra(ReBalanceActivity.SCID, scid);
        intent.putExtra(ReBalanceActivity.ISCID, iScid);
        intent.putExtra(ReBalanceActivity.TITLE, smallcaseName.getText().toString());
        intent.putExtra(ReBalanceActivity.DATE, presenter.getReBalanceDate());
        intent.putExtra(ReBalanceActivity.LABEL, reBalanceDescription.getText().toString());
        intent.putExtra(ReBalanceActivity.VERSION, presenter.getReBalanceVersion());
        intent.putExtra(ReBalanceActivity.SOURCE, source);
        startActivity(intent);
    }

    @OnClick(R.id.got_exit_info)
    public void dismissExitInfo() {
        animations.scaleOutAnimation(exitInfoContainer, exitInfoBlur);
    }

    @Override
    public void onSmallcaseReturnsReceived(PInvestedSmallcaseReturns pInvestedSmallcaseReturns) {
        loadingIndicator.setVisibility(View.GONE);
        swipeRefreshLayout.setVisibility(View.VISIBLE);
        detailContainer.setVisibility(View.VISIBLE);
        investedSmallcaseReturns = pInvestedSmallcaseReturns;
        setUserReturns();
    }

    @Override
    public void failedFetchingNews() {
        stopRefreshing();
        hideNews();
    }

    @Override
    public void onSmallcaseDataReceived(PInvestedSmallcase investedSmallcase) {
        if (isFinishing()) return;
        this.investedSmallcase = investedSmallcase;

        source = investedSmallcase.getSource();
        scid = investedSmallcase.getScid();
        try {
            index = Double.valueOf(smallcaseIndexValue.getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        boughtOn = investedSmallcase.getDate();
        Glide.with(this)
                .load(investedSmallcase.isIndexUp() ? R.drawable.up_arrow : R.drawable.down_arrow_red)
                .asBitmap()
                .into(smallcaseIndexUpOrDown);

        Glide.with(this)
                .load(investedSmallcase.getSmallcaseImage())
                .asBitmap()
                .into(new BitmapImageViewTarget(smallcaseImage) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable roundImage = RoundedBitmapDrawableFactory.create(getResources(), resource);
                        roundImage.setCornerRadius(AppUtils.getInstance().dpToPx(2));
                        smallcaseImage.setImageDrawable(roundImage);
                    }
                });

        if (presenter.isSmallcaseValid()) {
            investMoreCard.setCardBackgroundColor(ContextCompat.getColor(this, R.color.green_800));
            exitSmallcaseCard.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
            exitSmallcaseConfirmText.setTextColor(ContextCompat.getColor(this, R.color.red_600));
            startSipCard.setCardBackgroundColor(ContextCompat.getColor(this, R.color.white));
            startSipText.setTextColor(ContextCompat.getColor(this, R.color.blue_800));
            smallcaseInfoDivider.setVisibility(View.GONE);
        } else {
            disableActions();
            negativeContainer.setVisibility(View.VISIBLE);
            smallcaseInfoDivider.setVisibility(View.VISIBLE);
        }

        smallcaseIndexValue.setText(investedSmallcase.getSmallcaseIndex());
        smallcaseName.setText(investedSmallcase.getSmallcaseName());
        broughtOn.setText(showBroughtOn(investedSmallcase.getDate()));

        if (shouldOpenInvestMore) {
            onInvestMoreClicked();
            shouldOpenInvestMore = false;
        }
    }

    @Override
    public void marketOpen() {
        if (isInvestMore) {
            // Invest more on smallcase
            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("smallcaseName", smallcaseName.getText().toString());
            eventProperties.put("smallcaseType", source);
            eventProperties.put("accessedFrom", "Investment Details");
            eventProperties.put("isInvested", true);
            eventProperties.put("isWatchlisted", false);
            analyticsContract.sendEvent(eventProperties, "Viewed Invest Popup", Analytics.MIXPANEL, Analytics.CLEVERTAP);
            investContract.getMinAmount(presenter.getStockList());
            return;
        }

        // Exit smallcase
        investBlur.setVisibility(View.VISIBLE);
        exitBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
        presenter.getStockPricesToExitSmallcase();

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", smallcaseName.getText().toString());
        eventProperties.put("smallcaseType", source);
        eventProperties.put("index", index);
        eventProperties.put("boughtOn", boughtOn);
        analyticsContract.sendEvent(eventProperties, "Viewed Exit Popup", Analytics.MIXPANEL, Analytics.CLEVERTAP);
    }

    @Override
    public void showExitSmallcaseAmount(double moneyYouReceive) {
        exitMainTitle.setText("Confirm Exit Type");
        sellAmountTitle.setVisibility(View.VISIBLE);
        loadingSellAmount.setVisibility(View.GONE);
        exitMinAmountContainer.setVisibility(View.VISIBLE);
        amount.setText(AppConstants.RUPEE_SYMBOL + String.format(Locale.getDefault(), "%.2f", moneyYouReceive));
        exitBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            //Received request_token from Kite login. Request authToken from server.
            HashMap<String, String> body = new HashMap<>();
            body.put("broker", "kite");
            body.put("reqToken", data.getStringExtra(REQUEST_TOKEN_KEY));
            body.put("app", "platform");

            InvestContract.Authenticate authenticate = (InvestContract.Authenticate) investContract;
            authenticate.getAuthToken(body);
        }
    }

    @OnClick(R.id.confirm_sell_amount)
    public void confirmExitAmount() {
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", smallcaseName.getText().toString());
        eventProperties.put("smallcaseType", source);
        eventProperties.put("maxExitAmount", presenter.getMaxExitAmount());
        eventProperties.put("amountConfirmed", sellAmount.getText().toString());
        eventProperties.put("exitType", isWholeExit? "complete" : "partial");
        analyticsContract.sendEvent(eventProperties, "Confirmed Amount", Analytics.MIXPANEL, Analytics.CLEVERTAP);

        if (isWholeExit) {
            presenter.onWholeExitClicked();
            return;
        }

        if (sellAmount.getText() == null || sellAmount.getText().toString().trim().length() < 2) {
            metaText.setText("No stocks found to sell for the amount entered. Try increasing the amount");
            return;
        }

        double exitAmount = Double.valueOf(sellAmount.getText().toString().trim());
        if (AppUtils.getInstance().isZero(exitAmount)) {
            metaText.setText("No stocks found to sell for the amount entered. Try increasing the amount");
            return;
        }

        presenter.onConfirmExitClicked(exitAmount);
    }

    @OnClick(R.id.see_all_or_manage_container)
    public void seeAllOrManageStocks() {
        if (stockList.isEmpty()) return;

        if (isExpanded) {
            if (!presenter.isSmallcaseValid()) {
                showSnackBarWithRepair(R.string.pending_orders);
                return;
            }

            Intent intent = new Intent(this, ManageActivity.class);
            intent.putExtra(ManageActivity.SMALLCASE_NAME, investedSmallcase.getSmallcaseName());
            intent.putExtra(ManageActivity.BROUGHT_ON, broughtOn.getText().toString());
            intent.putExtra(ManageActivity.INDEX, investedSmallcase.getSmallcaseIndex());
            intent.putExtra(ManageActivity.IS_UP, investedSmallcase.isIndexUp());
            intent.putExtra(ManageActivity.SCID, scid);
            intent.putExtra(ManageActivity.ISCID, iScid);
            intent.putExtra(ManageActivity.SOURCE, investedSmallcase.getSource());
            intent.putParcelableArrayListExtra(ManageActivity.STOCKS_LIST, (ArrayList<? extends Parcelable>) stockAdapterDelegate.getStocks());
            startActivity(intent);
        } else {
            isExpanded = true;
            setUpStocksAdapter();
        }
    }

    @Override
    public void disableActions() {
        investMoreCard.setCardBackgroundColor(ContextCompat.getColor(this, R.color.blue_grey_100));
        exitSmallcaseCard.setCardBackgroundColor(ContextCompat.getColor(this, R.color.blue_grey_100));
        exitSmallcaseConfirmText.setTextColor(ContextCompat.getColor(this, R.color.white));
        startSipCard.setCardBackgroundColor(ContextCompat.getColor(this, R.color.blue_grey_100));
        startSipText.setTextColor(ContextCompat.getColor(this, R.color.white));
        applyReBalance.setCardBackgroundColor(ContextCompat.getColor(this, R.color.blue_grey_100));
    }

    @Override
    public void showNegativeContainer(SpannableString title, String description, boolean showArrow) {
        smallcaseInfoDivider.setVisibility(View.VISIBLE);
        negativeContainer.setVisibility(View.VISIBLE);
        negativeTitle.setText(title);
        if (description != null) {
            negativeReason.setText(description);
        } else {
            negativeReason.setVisibility(View.GONE);
        }
        negativeContainer.setBackgroundColor(ContextCompat.getColor(this, R.color.error_yellow));

        if (showArrow) {
            arrow.setVisibility(View.VISIBLE);
        } else {
            arrow.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.negative_container)
    public void negativeContainerClicked() {
        Intent intent = new Intent(this, BatchDetailsActivity.class);
        intent.putExtra(BatchDetailsActivity.ISCID, iScid);
        intent.putExtra(BatchDetailsActivity.SCID, scid);
        intent.putExtra(BatchDetailsActivity.SMALLCASE_NAME, investedSmallcase.getSmallcaseName());
        intent.putExtra(BatchDetailsActivity.SOURCE, source);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (stockInfo.isBottomSheetOpen()) {
            stockInfo.setBottomSheetState(BottomSheetBehavior.STATE_HIDDEN);
            return;
        }

        if (exitBottomSheet.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            hideExitCard();
            return;
        }

        if (investContract.isInvestCardVisible()) {
            investContract.hideInvestCard();
            return;
        }

        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        investContract.unBind();
        stockInfo.unBind();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
        if (presenter != null) presenter.destroySubscriptions();
        super.onDestroy();
    }

    @OnClick(R.id.invest_more)
    public void onInvestMoreClicked() {
        if (!presenter.isSmallcaseValid()) {
            showSnackBarWithRepair(R.string.not_filled);
            return;
        }

        if (presenter.isReBalancePending()) {
            isSkipSipDialog = false;
            dialogDescription.setText("You have a pending rebalance update, which may have impacted the smallcase stocks & weights." +
                    " Investing more without applying this update may mean your smallcase will not match the theme/model of the " +
                    "original smallcase.\n\n" + "Do you want to apply the rebalance update now or continue investing more?");
            dialogPrimaryButton.setText(getString(R.string.view_update));
            dialogSecondaryButton.setText(getString(R.string.invest_more));

            animations.scaleIntoPlaceAnimation(dialogConformation);
            exitInfoBlur.setVisibility(View.VISIBLE);
            exitInfoBlur.setOnClickListener(v -> hideConfirmationDialog());
            return;
        }

        isInvestMore = true;
        presenter.checkMarketStatus();
    }

    @OnClick(R.id.start_sip_confirm_card)
    public void startSip() {
        if (!presenter.isSmallcaseValid()) {
            showSnackBarWithRepair(R.string.not_filled);
            return;
        }

        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("accessedFrom", "Investment Details");
        eventProperties.put("smallcaseType", source);
        eventProperties.put("smallcaseName", smallcaseName.getText().toString());
        analyticsContract.sendEvent(eventProperties, "Viewed Start SIP Popup", Analytics.MIXPANEL, Analytics.CLEVERTAP);

        ArrayList<Stock> sipStocks = new ArrayList<>();
        for (Stock stock : presenter.getStockList()) {
            Stock sipStock = new Stock(stock);
            sipStocks.add(sipStock);
        }
        Intent intent = new Intent(this, SetUpSipActivity.class);
        intent.putExtra(SetUpSipActivity.SCID, scid);
        intent.putExtra(SetUpSipActivity.ISCID, iScid);
        intent.putExtra(SetUpSipActivity.SOURCE, source);
        intent.putExtra(SetUpSipActivity.SMALLCASE_NAME, smallcaseName.getText().toString());
        intent.putParcelableArrayListExtra(SetUpSipActivity.STOCKS, sipStocks);
        startActivity(intent);
    }

    @OnClick(R.id.exit_smallcase_confirm_card)
    public void onExitSmallcaseClicked() {
        if (!presenter.isSmallcaseValid()) {
            showSnackBarWithRepair(R.string.not_filled);
            return;
        }

        isInvestMore = false;

        investBlur.setOnClickListener(v -> hideExitCard());

        exitBottomSheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    metaText.setVisibility(View.GONE);
                    investBlur.setVisibility(View.GONE);
                    AppUtils.getInstance().hideKeyboard(InvestedSmallcaseDetailActivity.this);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        presenter.checkMarketStatus();
    }

    @OnClick(R.id.whole)
    public void sellWhole() {
        isWholeExit = true;

        AppUtils.getInstance().hideKeyboard(InvestedSmallcaseDetailActivity.this);

        collapse(partialBackground, -sixHundredDp);
        expand(wholeBackground, sixHundredDp);

        partial.setTextColor(ContextCompat.getColor(this, R.color.blue_800));
        whole.setTextColor(ContextCompat.getColor(this, R.color.white));

        amount.setVisibility(View.VISIBLE);
        sellAmount.setVisibility(View.GONE);
        sellAmountDivider.setVisibility(View.GONE);
        rupeeSymbol.setVisibility(View.GONE);
        metaText.setVisibility(View.GONE);
    }

    @OnClick(R.id.partial)
    public void sellPartial() {
        isWholeExit = false;

        collapse(wholeBackground, sixHundredDp);
        expand(partialBackground, -sixHundredDp);

        whole.setTextColor(ContextCompat.getColor(this, R.color.blue_800));
        partial.setTextColor(ContextCompat.getColor(this, R.color.white));

        amount.setVisibility(View.GONE);
        sellAmount.setVisibility(View.VISIBLE);
        sellAmountDivider.setVisibility(View.VISIBLE);
        rupeeSymbol.setVisibility(View.VISIBLE);
        metaText.setVisibility(View.VISIBLE);

        metaText.setText(presenter.getMaxExitSpannableString());
        sellAmount.setText(String.format(Locale.getDefault(), "%.2f", presenter.getMaxExitAmount()));

        sellAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (metaText.getVisibility() == View.VISIBLE) {
                    metaText.setText(presenter.getMaxExitSpannableString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @OnClick(R.id.smallcase_information_card)
    public void seeSmallcaseInfo() {
        if (!SmallcaseSource.PROFESSIONAL.equals(source)) return;

        Intent intent = new Intent(this, SmallcaseDetailActivity.class);
        intent.putExtra(SmallcaseDetailActivity.SCID, investedSmallcase.getScid());
        intent.putExtra(SmallcaseDetailActivity.TITLE, investedSmallcase.getSmallcaseName());
        intent.putExtra(SmallcaseDetailActivity.ACCESSED_FROM, "Investment Details");
        startActivity(intent);
    }

    @OnClick(R.id.see_all_news)
    public void seeAllNews() {
        if (newsAdapter == null) return;

        Intent intent = new Intent(this, NewsActivity.class);
        intent.putExtra(NewsActivity.SCID, investedSmallcase.getScid());
        intent.putExtra(NewsActivity.SMALLCASE_NAME, smallcaseName.getText().toString());
        intent.putParcelableArrayListExtra(NewsActivity.NEWS_LIST, (ArrayList<? extends Parcelable>) newsAdapter.getNewsList());
        startActivity(intent);
    }

    @Override
    public void onNewsReceived(List<PPlainNews> news) {
        stopRefreshing();

        newsList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        newsList.setHasFixedSize(true);
        newsAdapter = new NewsAdapter(this, news, analyticsContract, true);
        newsList.setAdapter(newsAdapter);
    }

    private void stopRefreshing() {
        if (null != swipeRefreshLayout && swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void showSnackBarWithRepair(@StringRes int resId) {
        Snackbar.make(parentView, resId, Snackbar.LENGTH_LONG)
                .setAction("REPAIR", v -> {
                    Intent intent = new Intent(InvestedSmallcaseDetailActivity.this, BatchDetailsActivity.class);
                    intent.putExtra(BatchDetailsActivity.ISCID, investedSmallcase.getiScid());
                    intent.putExtra(BatchDetailsActivity.SOURCE, investedSmallcase.getSource());
                    intent.putExtra(BatchDetailsActivity.SCID, investedSmallcase.getScid());
                    intent.putExtra(BatchDetailsActivity.SMALLCASE_NAME, investedSmallcase.getSmallcaseName());
                    startActivity(intent);
                }).show();
    }

    @OnClick(R.id.see_all_stocks_container)
    public void onSeeAllStocksCLicked() {
        stockAdapterDelegate.getStocks();
    }

    @OnClick({R.id.smallcase_invested_card, R.id.expand_card})
    public void expandCard() {
        if (investmentsContainer.getVisibility() == View.GONE) {
            animations.expand(investmentsContainer, OVERSHOOT_INTERPOLATOR, 300);
            expandArrow.setRotation(270);
        } else {
            animations.collapse(investmentsContainer, ANTICIPATE_OVERSHOOT_INTERPOLATOR, 250);
            expandArrow.setRotation(90);
        }
    }

    @Override
    public void noNewsForSmallcase() {
        stopRefreshing();
        hideNews();
    }

    private void setUserReturns() {
        smallcasePAndLPercent.setText(investedSmallcaseReturns.getUnRealizedPAndLPercent());
        currentInvestment.setText(investedSmallcaseReturns.getCurrentInvestment());
        pAndLAmount.setText(investedSmallcaseReturns.getUnRealizedAmount());

        netWorth.setText(investedSmallcaseReturns.getNetworth());
        realisedPAndL.setText(investedSmallcaseReturns.getRealizedPAndLAmount());

        totalAmount.setText(investedSmallcaseReturns.getTotalInvestment());
        totalPAndL.setText(investedSmallcaseReturns.getTotalPAndLAmount());
        dividendAmount.setText(investedSmallcaseReturns.getDividend());

        int red = ContextCompat.getColor(this, R.color.red_600);
        int green = ContextCompat.getColor(this, R.color.green_600);
        int black = ContextCompat.getColor(this, R.color.primary_text);

        smallcasePAndLPercent.setTextColor(investedSmallcaseReturns.isUnRealizedProfit() ? green : red);
        pAndLAmount.setTextColor(investedSmallcaseReturns.isUnRealizedProfit() ? green : red);
        if (investedSmallcaseReturns.getRealizedPAndLAmount().equals("0")) {
            realisedPAndL.setTextColor(black);
        } else {
            realisedPAndL.setTextColor(investedSmallcaseReturns.isRealizedProfit() ? green : red);
        }
        totalPAndL.setTextColor(investedSmallcaseReturns.isTotalProfit() ? green : red);
    }

    private void hideNews() {
        newsList.setVisibility(View.GONE);
        newsSmallcase.setVisibility(View.GONE);
    }

    @Override
    public void onItemClicked(int type, int position) {
        switch (type) {
            case AppConstants.OPTION_CHANGED:
                setUpStocksAdapter();
                break;

            case AppConstants.STOCK:
                String sid = stockAdapterDelegate.getStocks().get(position).getSid();
                stockInfo.showStockInfo(sid);
                break;
        }
    }

    private void setUpStocksAdapter() {
        if (stockList.isEmpty()) {
            seeAllStocks.setText("No stocks");
            return;
        }

        if (stockList.size() < 6 || isExpanded) {
            isExpanded = true;
            seeAllOrManageArrow.setRotation(0);
            seeAllStocks.setText("Add/Remove stocks");
        } else {
            isExpanded = false;
            seeAllOrManageArrow.setRotation(90);
            seeAllStocks.setText("See all stocks");
        }

        int initialVisibleStocks = 6;
        List<PStock> detailList = new ArrayList<>();

        PStock stockHeading = new PStock();
        stockHeading.setHeading(true);
        stockHeading.setSortPAndL(true);

        detailList.add(stockHeading);

        List<PStock> topStocks = new ArrayList<>(presenter.getSortedStocks(stockList,
                true, isExpanded ? stockList.size() : initialVisibleStocks));
        detailList.addAll(topStocks);

        if (stockAdapterDelegate != null) {
            stockAdapterDelegate.updateData(detailList);
            return;
        }

        stockAdapterDelegate = new StockAdapterDelegate(InvestedSmallcaseDetailActivity.this, this, detailList, analyticsContract, this);
        smallcaseDetailsList.setAdapter(stockAdapterDelegate);
    }

    private String showBroughtOn(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault());
        return "Bought on " + simpleDateFormat.format(AppUtils.getInstance().getPlainDate(date));
    }

    @Override
    public void onRefresh() {
        if (iScid != null && scid != null)
            presenter.fetchSmallcaseDetails(scid);
    }

    @Override
    public void canContinueTransaction(List<Stock> stockList) {
        for (Stock stock : stockList) {
            stock.setBuy(true);
        }
        Intent intent = new Intent(this, ReviewOrderActivity.class);
        intent.putParcelableArrayListExtra(ReviewOrderActivity.STOCK_LIST, (ArrayList<? extends Parcelable>) stockList);
        intent.putExtra(ReviewOrderActivity.SCID, scid);
        intent.putExtra(ReviewOrderActivity.INVESTMENT_AMOUNT, investContract.getInvestmentAmount());
        intent.putExtra(ReviewOrderActivity.SMALLCASE_NAME, investedSmallcase.getSmallcaseName());
        intent.putExtra(ReviewOrderActivity.ISCID, iScid);
        intent.putExtra(ReviewOrderActivity.ORDER_LABEL, presenter.isSipDue() ? OrderLabel.SIP : OrderLabel.INVEST_MORE);
        intent.putExtra(ReviewOrderActivity.SOURCE, source);
        startActivity(intent);
        overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
        investContract.hideInvestCard();
    }

    @Override
    public void onConfirmAmountClicked(double investmentAmount, double minInvestmentAmount) {
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", investedSmallcase.getSmallcaseName());
        eventProperties.put("smallcaseType", source);
        eventProperties.put("amountConfirmed", investmentAmount);
        eventProperties.put("minInvestAmount", minInvestmentAmount);
        analyticsContract.sendEvent(eventProperties, "Confirmed Amount", Analytics.MIXPANEL, Analytics.CLEVERTAP);
    }

    @Override
    public void onAddFundsClicked(double investmentAmount, double minInvestmentAmount) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(AppConstants.KITE_ADD_FUNDS));
        startActivity(intent);
    }

    @Override
    public void onConfirmShownAgain() {
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("smallcaseName", investedSmallcase.getSmallcaseName());
        eventProperties.put("smallcaseType", source);
        analyticsContract.sendEvent(eventProperties, "Changed Amount", Analytics.MIXPANEL, Analytics.CLEVERTAP);
    }

    @Override
    public void showBlur() {
        investBlur.setVisibility(View.VISIBLE);
        investBlur.setClickable(true);
        investBlur.setOnClickListener(v -> {
            investContract.hideInvestCard();
        });
    }

    @Override
    public void hideBlur() {
        investBlur.setClickable(false);
        investBlur.setVisibility(View.GONE);
    }

    @Override
    public void sendUserToKiteLogin() {
        startActivityForResult(new Intent(this, LoginActivity.class),
                LOGIN_REQUEST_CODE);
        overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    }

    public void expand(final View view, float start) {
        ObjectAnimator anim = ObjectAnimator.ofFloat(view, "translationX", start, 0f);
        anim.setInterpolator(accelerateDecelerateInterpolator);
        anim.setDuration(250).start();
    }

    public void collapse(final View view, float end) {
        ObjectAnimator anim = ObjectAnimator.ofFloat(view, "translationX", 0f, end);
        anim.setInterpolator(accelerateDecelerateInterpolator);
        anim.setDuration(250).start();
    }
}
