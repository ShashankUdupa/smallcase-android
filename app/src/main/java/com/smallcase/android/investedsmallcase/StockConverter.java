package com.smallcase.android.investedsmallcase;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by shashankm on 19/12/16.
 */

public class StockConverter<T> implements JsonDeserializer<T> {
    private static final String TAG = StockConverter.class.getSimpleName();
    private String sid;

    public StockConverter(String sid) {
        this.sid = sid;
    }

    @Override
    public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return new Gson().fromJson(json.getAsJsonObject().getAsJsonObject("data").get(sid), typeOfT);
    }
}