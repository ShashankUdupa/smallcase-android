package com.smallcase.android.investedsmallcase.sip;

import java.util.HashMap;

/**
 * Created by shashankm on 15/09/17.
 */

interface EditSipContract {
    interface View {

        void onSipSavedSuccessful();

        void onFailedSavingSip();

        void showSnackBar(int resId);

        void onSipEndedSuccessful();

        void onFailedEndingSip();
    }

    interface Service {

        void setUpSip(final HashMap<String, String> body);

        void endSip(final HashMap<String, String> body);

        void destroySubscriptions();
    }
}
