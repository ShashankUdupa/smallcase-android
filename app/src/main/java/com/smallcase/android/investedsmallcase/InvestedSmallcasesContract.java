package com.smallcase.android.investedsmallcase;

import com.smallcase.android.data.model.ExitedSmallcase;
import com.smallcase.android.data.model.InvestedSmallcases;
import com.smallcase.android.data.model.TotalInvestment;
import com.smallcase.android.view.model.PInvestedSmallcase;
import com.smallcase.android.view.model.PInvestments;

import java.util.List;

/**
 * Created by shashankm on 08/12/16.
 */

public interface InvestedSmallcasesContract {

    interface View {
        void onInvestedSmallcasesReceived(List<PInvestedSmallcase> investedSmallcaseList);

        void onInvestmentsReceived(PInvestments pInvestments);

        void onExitedSmallcasesReceived(List<ExitedSmallcase> exitedSmallcases);

        void showNoInternetMessage();

        void showTryAgain();

        void noInvestments();

        void showSnackBar(int something_wrong);
    }

    interface Presenter {
        void onExitedSmallcaseClicked(List<ExitedSmallcase> exitedSmallcases);

        void fetchUserInvestmentsData();

        void onInvestmentsReceived(List<InvestedSmallcases> investedSmallcases);

        void onTotalInvestmentReceived(TotalInvestment totalInvestment);

        void onExitedSmallcasesReceived(List<ExitedSmallcase> exitedSmallcases);

        void onFailedFetchingInvestments();

        void onFailedFetchingTotalInvestment();

        void onFailedFetchingExitedSmallcases();

        void destroySubscriptions();
    }

    interface Service {
        String getAuth();

        List<InvestedSmallcases> getCachedInvestedSmallcases();

        TotalInvestment getCachedTotalInvestments();

        void getInvestments();

        void getTotalInvestment();

        void getExitedSmallcases();

        void destroySubscriptions();
    }
}
