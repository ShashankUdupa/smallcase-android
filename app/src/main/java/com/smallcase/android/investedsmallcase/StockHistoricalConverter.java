package com.smallcase.android.investedsmallcase;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.smallcase.android.data.model.StockHistorical;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by shashankm on 19/12/16.
 */

public class StockHistoricalConverter implements JsonDeserializer<List<StockHistorical>> {
    private static final String TAG = StockHistorical.class.getSimpleName();
    private String sid;

    public StockHistoricalConverter(String sid) {
        this.sid = sid;
    }

    @Override
    public List<StockHistorical> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return new Gson().fromJson(json.getAsJsonObject().getAsJsonObject("data").getAsJsonObject(sid).get("point"), typeOfT);
    }
}
