package com.smallcase.android.investedsmallcase.sip;

import com.google.gson.Gson;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.Actions;
import com.smallcase.android.data.model.SipDetails;
import com.smallcase.android.data.repository.MarketRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.UserRepository;

import java.util.HashSet;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 16/09/17.
 */

class AllSipsService implements AllSipsContract.Service {
    private static final String TAG = "AllSipsService";

    private UserRepository userRepository;
    private SharedPrefService sharedPrefService;
    private AllSipsContract.View view;
    private MarketRepository marketRepository;
    private CompositeSubscription compositeSubscription;

    AllSipsService(UserRepository userRepository, SharedPrefService sharedPrefService, AllSipsContract
            .View view, MarketRepository marketRepository) {
        this.userRepository = userRepository;
        this.sharedPrefService = sharedPrefService;
        this.view = view;
        this.marketRepository = marketRepository;
        this.compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getBuyReminders() {
        if (sharedPrefService.getAuthorizationToken() == null) {
            view.redirectUserToLogin();
            return;
        }

        compositeSubscription.add(userRepository.getPendingActions(sharedPrefService.getAuthorizationToken(),
                sharedPrefService.getCsrfToken())
                .subscribe(new Action1<Actions>() {
                    @Override
                    public void call(Actions actions) {
                        cacheSipUpdates(actions.getSips());
                        view.onSipsFetched(actions.getSips());
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        view.onFailedFetchingSipUpdates();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getBuyReminders()");
                    }
                }));
    }

    @Override
    public void getMarketStatus() {
        compositeSubscription.add(marketRepository.getIfMarketIsOpen(sharedPrefService.getAuthorizationToken(), sharedPrefService.getCsrfToken())
                .subscribe(new Action1<ResponseBody>() {
                    @Override
                    public void call(ResponseBody responseBody) {
                        view.onMarketStatusReceived(responseBody);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        view.onFailedFetchingMarketStatus();
                        if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                        SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getMarketStatus()");
                    }
                }));
    }

    @Override
    public void destroySubscriptions() {
        if (compositeSubscription != null) compositeSubscription.unsubscribe();
    }

    private void cacheSipUpdates(List<SipDetails> sips) {
        HashSet<String> sipsSet = new HashSet<>();
        Gson gson = new Gson();
        for (SipDetails sip : sips) {
            sipsSet.add(gson.toJson(sip));
        }
        sharedPrefService.cacheSipUpdates(sipsSet);
    }
}
