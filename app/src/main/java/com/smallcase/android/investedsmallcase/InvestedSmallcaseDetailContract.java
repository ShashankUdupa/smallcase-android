package com.smallcase.android.investedsmallcase;

import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;

import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.ReBalanceUpdate;
import com.smallcase.android.data.model.SipDetails;
import com.smallcase.android.data.model.SmallcaseDetail;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.view.model.PInvestedSmallcase;
import com.smallcase.android.view.model.PInvestedSmallcaseReturns;
import com.smallcase.android.view.model.PPlainNews;
import com.smallcase.android.view.model.PStock;
import com.smallcase.android.view.model.Stock;

import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;

/**
 * Created by shashankm on 14/12/16.
 */

public interface InvestedSmallcaseDetailContract {

    interface View {

        void onStocksReceived(List<PStock> stockList);

        void showSnackBar(int resId);

        void onSmallcaseReturnsReceived(PInvestedSmallcaseReturns pInvestedSmallcaseReturns);

        void onNewsReceived(List<PPlainNews> newsList);

        void noNewsForSmallcase();

        void failedFetchingNews();

        void onSmallcaseDataReceived(PInvestedSmallcase investedSmallcase);

        void marketOpen();

        void showExitSmallcaseAmount(double moneyYouReceive);

        void disableActions();

        void showNegativeContainer(SpannableString title, String description, boolean showArrow);

        void showReBalanceAvailable(String date, String label);

        void redirectToLogin();

        void hideExitCard();

        void noPartialExitAllowed(double totalAmount);

        void exitWhole(List<Stock> wholeExitStockList, double amountYouReceive);

        void showInvalidAmountError(SpannableString spannableString);

        void continuePartialExit(double finalAmount, List<Stock> stocksToSell);

        RecyclerView getRecyclerView();

        void hideStartSipButton();

        void showStartSipButton();

        void showSipDue();

        void showSipDetails(SpannableString sipString);

        void hideSipCard();

        void hideLoader();

        void enableReBalance();
    }

    interface Presenter {

        void onSmallcaseDetailsReceived(SmallcaseDetail smallcaseDetail, ResponseBody responseBody);

        void onFailedFetchingSmallcaseDetails();

        void onSmallcaseNewsReceived(List<News> newses);

        void onFailedFetchingSmallcaseNews();

        void fetchSmallcaseDetails(String scid);

        void onMarketStatusReceived(ResponseBody responseBody);

        void checkMarketStatus();

        boolean isSmallcaseValid();

        List<PStock> getSortedStocks(List<PStock> stockList, boolean onPAndL, int numberOfStocks);

        List<Stock> getStockList();

        void onFailedFetchingMarketStatus();

        void getStockPricesToExitSmallcase();

        void addReminder(String scid);

        boolean isReBalancePending();

        int getReBalanceVersion();

        void destroySubscriptions();

        String getReBalanceDate();

        void onReBalancesReceived(List<ReBalanceUpdate> reBalanceUpdates);

        void onFailedFetchingReBalanceUpdates();

        SpannableString getMaxExitSpannableString();

        double getMaxExitAmount();

        void onConfirmExitClicked(double exitAmount);

        void onWholeExitClicked();

        void onFailedFetchingSipDetails();

        void onSipDetailsFetched(SipDetails sipDetails);

        boolean isSipDue();

        long getNextSip();

        String getSipFrequency();

        void onSipSkippedSuccessful();

        void onFailedSkippingSip();

        void skipSip(HashMap<String, String> body);
    }

    interface Service {

        String getAuthToken();

        void getSmallcaseDetail(String iScid);

        void getSmallcaseNews(List<String> scids, List<String> sids);

        void getIfMarketIsOpen();

        SharedPrefService getSharedPrefService();

        void addMarketOpenReminder(HashMap<String, String> body);

        void getReBalances();

        void destroySubscriptions();

        void clearCache();

        void getSipDetails(String iScid);

        boolean isSipDue(String iScid);

        void skipSip(final HashMap<String, String> body);
    }
}
