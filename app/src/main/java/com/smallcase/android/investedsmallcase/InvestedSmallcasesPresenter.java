package com.smallcase.android.investedsmallcase;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;

import com.smallcase.android.R;
import com.smallcase.android.data.model.ExitedSmallcase;
import com.smallcase.android.data.model.InvestedSmallcases;
import com.smallcase.android.data.model.TotalInvestment;
import com.smallcase.android.data.repository.InvestmentRepository;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.investedsmallcase.exited.ExitedSmallcasesActivity;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.InvestmentHelper;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.model.PInvestedSmallcase;
import com.smallcase.android.view.model.PInvestments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by shashankm on 02/12/16.
 */

class InvestedSmallcasesPresenter implements InvestedSmallcasesContract.Presenter {
    private static final String TAG = "InvestedSmallcasesPres";

    private InvestmentHelper investmentHelper;
    private InvestedSmallcasesContract.View investedSmallcasesContract;
    private InvestedSmallcasesContract.Service investedSmallcasesServiceContract;
    private NetworkHelper networkHelper;
    private Activity activity;
    private PInvestments pInvestments;
    private boolean failedFetchingInvestments = false, failedFetchingTotalInvestment = false, failedFetchingExitedSmallcase = false;

    InvestedSmallcasesPresenter(Activity activity, InvestedSmallcasesContract.View investedSmallcasesContract,
                                SharedPrefService sharedPrefService, InvestmentRepository investmentRepository, InvestmentHelper
                                        investmentHelper, NetworkHelper networkHelper) {
        this.activity = activity;
        this.investedSmallcasesContract = investedSmallcasesContract;
        this.investedSmallcasesServiceContract = new InvestedSmallcaseService(sharedPrefService, investmentRepository, this);
        this.investmentHelper = investmentHelper;
        this.networkHelper = networkHelper;
    }

    @Override
    public void fetchUserInvestmentsData() {
        String auth = investedSmallcasesServiceContract.getAuth();
        if (auth == null) return;

        pInvestments = null;

        if (!networkHelper.isNetworkAvailable()) {
            onInvestmentsReceived(investedSmallcasesServiceContract.getCachedInvestedSmallcases());
            onTotalInvestmentReceived(investedSmallcasesServiceContract.getCachedTotalInvestments());
            investedSmallcasesContract.showNoInternetMessage();
            return;
        }

        investedSmallcasesServiceContract.getInvestments();
        investedSmallcasesServiceContract.getTotalInvestment();
        investedSmallcasesServiceContract.getExitedSmallcases();
    }

    @Override
    public void onInvestmentsReceived(List<InvestedSmallcases> investedSmallcases) {
        failedFetchingInvestments = false;
        if (investedSmallcases.isEmpty()) {
            investedSmallcasesContract.noInvestments();
            return;
        }

        double totalNetWorth = 0, totalUnrealizedInvestment = 0, totalRealizedReturn = 0;
        double totalRealizedInvestment = 0, totalDivReturns = 0;

        List<PInvestedSmallcase> investedSmallcaseList = new ArrayList<>();
        for (InvestedSmallcases investedSmallcase : investedSmallcases) {
            boolean isIndexUp = investedSmallcase.getStats().getIndexValue() > investedSmallcase.getStats().getLastCloseIndex();
            String indexValue = String.format(Locale.getDefault(), "%.2f", investedSmallcase.getStats().getIndexValue());
            String netWorth = AppConstants.RUPEE_SYMBOL + (investedSmallcase.getReturns().getUnrealizedInvestment() == 0 ? AppConstants.HYPHEN_SYMBOL + AppConstants.HYPHEN_SYMBOL :
                     AppConstants.getNumberFormatter().format(investedSmallcase.getReturns().getNetworth()));
            double totalPAndLPercent = ((investedSmallcase.getReturns().getRealizedReturns() + (investedSmallcase.getReturns()
                    .getNetworth() - investedSmallcase.getReturns().getUnrealizedInvestment()) + investedSmallcase.getReturns().getDivReturns()) * 100) /
                    (investedSmallcase.getReturns().getRealizedInvestment() + investedSmallcase.getReturns().getUnrealizedInvestment());
            String percentage = (investedSmallcase.getReturns().getUnrealizedInvestment() == 0 ? AppConstants.HYPHEN_SYMBOL
                    + AppConstants.HYPHEN_SYMBOL : String.format(Locale.getDefault(), "%.2f", totalPAndLPercent) + "%");
            SpannableString pAndLPercentSpannable = new SpannableString("Total P&L " + percentage);
            pAndLPercentSpannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(activity, investedSmallcase.getStats().getIndexValue() < 100 ? R.color.red_600 : R.color.green_600)),
                    10, pAndLPercentSpannable.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

            totalNetWorth += investedSmallcase.getReturns().getNetworth();
            totalRealizedReturn += investedSmallcase.getReturns().getRealizedReturns();
            totalUnrealizedInvestment += investedSmallcase.getReturns().getUnrealizedInvestment();
            totalRealizedInvestment += investedSmallcase.getReturns().getRealizedInvestment();
            totalDivReturns += investedSmallcase.getReturns().getDivReturns();

            investedSmallcaseList.add(new PInvestedSmallcase(investedSmallcase.getName(), investedSmallcase.getSource(), "80", indexValue, isIndexUp, netWorth,
                    pAndLPercentSpannable, isIndexUp, investedSmallcase.getScid(), investedSmallcase.getDate(), investedSmallcase.get_id()));
        }

        double totalPAndLPercent = (totalRealizedReturn + (totalNetWorth - totalUnrealizedInvestment) + totalDivReturns) * 100 /
                (totalRealizedInvestment + totalUnrealizedInvestment);
        double totalPAndL = totalRealizedReturn + (totalNetWorth - totalUnrealizedInvestment) + totalDivReturns;

        boolean isToTalPAndLProfit = totalPAndL >= 0;
        String totalPAndLPercentFormatted = String.format(Locale.getDefault(), "%.2f", totalPAndLPercent) + "%";
        String totalPAndLFormatted = AppConstants.RUPEE_SYMBOL + AppConstants.getNumberFormatter().format(totalPAndL);

        if (pInvestments == null) {
            pInvestments = new PInvestments();
            setTotalInvestmentValues(isToTalPAndLProfit, totalPAndLPercentFormatted, totalPAndLFormatted);
        } else {
            setTotalInvestmentValues(isToTalPAndLProfit, totalPAndLPercentFormatted, totalPAndLFormatted);
            investedSmallcasesContract.onInvestmentsReceived(pInvestments);
        }

        Collections.reverse(investedSmallcaseList);
        investedSmallcasesContract.onInvestedSmallcasesReceived(investedSmallcaseList);
    }

    private void setTotalInvestmentValues(boolean isToTalPAndLProfit, String totalPAndLPercentFormatted, String totalPAndLFormatted) {
        pInvestments.setTotalPAndL(totalPAndLFormatted);
        pInvestments.setTotalPAndLPercent(totalPAndLPercentFormatted);
        pInvestments.setTotalPAndLProfit(isToTalPAndLProfit);
    }

    @Override
    public void onTotalInvestmentReceived(TotalInvestment totalInvestment) {
        failedFetchingTotalInvestment = false;

        if (null == totalInvestment || AppUtils.getInstance().isZero(totalInvestment.getUnrealizedInvestment())) return;

        String currentValue = AppConstants.RUPEE_SYMBOL + AppConstants.getNumberFormatter()
                .format(totalInvestment.getNetworth());
        String currentInvestment = AppConstants.RUPEE_SYMBOL + AppConstants.getNumberFormatter()
                .format(totalInvestment.getUnrealizedInvestment());
        String currentPAndL = AppConstants.RUPEE_SYMBOL + AppConstants.getNumberFormatter().format(totalInvestment.getNetworth() - totalInvestment.getUnrealizedInvestment());
        boolean isCurrentPAndLProfit = totalInvestment.getNetworth() > totalInvestment.getUnrealizedInvestment();
        String currentPAndLPercentage = String.format(Locale.getDefault(), "%.2f", investmentHelper
                .calculateProfitOrLossPercent(totalInvestment.getNetworth(), totalInvestment.getUnrealizedInvestment())) + "%";

        if (pInvestments == null) {
            pInvestments = new PInvestments();
            setCurrentInvestmentValues(currentValue, currentInvestment, currentPAndL, isCurrentPAndLProfit, currentPAndLPercentage);
        } else {
            setCurrentInvestmentValues(currentValue, currentInvestment, currentPAndL, isCurrentPAndLProfit, currentPAndLPercentage);
            investedSmallcasesContract.onInvestmentsReceived(pInvestments);
        }
    }

    private void setCurrentInvestmentValues(String currentValue, String currentInvestment, String currentPAndL, boolean isCurrentPAndLProfit, String currentPAndLPercentage) {
        pInvestments.setCurrentValue(currentValue);
        pInvestments.setCurrentInvestment(currentInvestment);
        pInvestments.setCurrentPAndL(currentPAndL);
        pInvestments.setCurrentPAndLProfit(isCurrentPAndLProfit);
        pInvestments.setCurrentPAndLPercent(currentPAndLPercentage);
    }

    @Override
    public void onExitedSmallcasesReceived(List<ExitedSmallcase> exitedSmallcases) {
        failedFetchingExitedSmallcase = false;
        if (exitedSmallcases.isEmpty()) return;
        investedSmallcasesContract.onExitedSmallcasesReceived(exitedSmallcases);
    }

    @Override
    public void onFailedFetchingInvestments() {
        failedFetchingInvestments = true;
        if (failedFetchingTotalInvestment && failedFetchingExitedSmallcase) {
            investedSmallcasesContract.showTryAgain();
            return;
        }

        investedSmallcasesContract.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onFailedFetchingTotalInvestment() {
        failedFetchingTotalInvestment = true;
        if (failedFetchingInvestments && failedFetchingExitedSmallcase) {
            investedSmallcasesContract.showTryAgain();
            return;
        }

        investedSmallcasesContract.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void onFailedFetchingExitedSmallcases() {
        failedFetchingExitedSmallcase = true;
        if (failedFetchingInvestments && failedFetchingTotalInvestment) {
            investedSmallcasesContract.showTryAgain();
            return;
        }

        investedSmallcasesContract.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void destroySubscriptions() {
        investedSmallcasesServiceContract.destroySubscriptions();
    }

    @Override
    public void onExitedSmallcaseClicked(final List<ExitedSmallcase> exitedSmallcases) {
        Intent intent = new Intent(activity, ExitedSmallcasesActivity.class);
        intent.putParcelableArrayListExtra(ExitedSmallcasesActivity.EXITED_SMALLCASES, (ArrayList<?
                extends Parcelable>) exitedSmallcases);
        activity.startActivity(intent);
    }
}
