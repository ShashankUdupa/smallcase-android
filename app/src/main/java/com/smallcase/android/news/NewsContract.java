package com.smallcase.android.news;

import com.smallcase.android.data.model.News;
import com.smallcase.android.view.model.PPlainNews;

import java.util.List;

/**
 * Created by shashankm on 08/02/17.
 */

interface NewsContract {
    interface View {

        void showSnackBar(int errId);

        void onNewsReceived(List<PPlainNews> newsList);

        void noMoreNews();
    }

    interface Presenter {

        void getSmallcaseNews();

        void onNewsReceived(List<News> newses);

        void onFailedFetchingNews();

        void destroySubscriptions();
    }

    interface Service {

        void getNewsForSmallcase(List<String> scids, int offset, int count);

        void destroySubscriptions();
    }
}
