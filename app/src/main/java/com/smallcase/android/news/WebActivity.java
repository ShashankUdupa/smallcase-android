package com.smallcase.android.news;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.smallcase.android.R;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.view.android.GraphikText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by shashankm on 05/12/16.
 */

public class WebActivity extends AppCompatActivity {
    public static final String URL = "url";
    public static final String TITLE = "title";
    public static final String IS_LITE_URL = "is_lite_url";

    private static final String LITE_URL = "http://googleweblight.com/?lite_url=";

    @BindView(R.id.text_tool_bar_title) GraphikText toolBarTitle;
    @BindView(R.id.web_view) WebView webView;
    @BindView(R.id.tool_bar) Toolbar toolbar;
    @BindView(R.id.loading_indicator) ProgressBar loadingIndicator;
    @BindView(R.id.back) ImageView backIcon;

    private boolean isCancel = false;
    private String url;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        toolBarTitle.setTextColor(ContextCompat.getColor(this, R.color.white));
        toolBarTitle.setText(getIntent().getStringExtra(TITLE));
        backIcon.setImageResource(R.drawable.ic_cancel);
        int padding = (int) AppUtils.getInstance().dpToPx(16);
        backIcon.setPadding(padding, padding, padding, padding);

        webView.setWebViewClient(new MyWebViewClient());
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAppCacheEnabled(false);

        if (Build.VERSION.SDK_INT >= 19) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        boolean shouldAppendLiteUrl = getIntent().getBooleanExtra(IS_LITE_URL, false);
        url = getIntent().getStringExtra(URL);
        webView.loadUrl(shouldAppendLiteUrl? (LITE_URL + url) : url);
    }

    @OnClick(R.id.back)
    public void back() {
        isCancel = true;
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        if (!isCancel && webView.canGoBack()) {
            webView.goBack();
            return;
        }

        super.onBackPressed();
        overridePendingTransition(R.anim.fade_in_with_scale, R.anim.top_to_down);
    }

    private class MyWebViewClient extends WebViewClient {
        private final String TAG = "MyWebViewClient";

        @Override
        public void onPageFinished(WebView view, String url) {
            loadingIndicator.setVisibility(View.GONE);
            view.scrollTo(0,0);
            webView.setVisibility(View.VISIBLE);
        }
    }
}
