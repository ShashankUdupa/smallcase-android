package com.smallcase.android.news;

import com.google.gson.Gson;
import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.model.InvestedSmallcases;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import io.intercom.retrofit2.HttpException;
import io.sentry.event.Breadcrumb;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 12/12/16.
 */

class AllNewsService implements AllNewsContract.Service {
    private static final String TAG = "AllNewsService";

    private SharedPrefService sharedPrefService;
    private AllNewsContract.Presenter allNewsPresenterContract;
    private SmallcaseRepository smallcaseRepository;
    private CompositeSubscription compositeSubscription;

    AllNewsService(SharedPrefService sharedPrefService, AllNewsContract.Presenter allNewsPresenterContract,
                   SmallcaseRepository smallcaseRepository) {
        this.sharedPrefService = sharedPrefService;
        this.allNewsPresenterContract = allNewsPresenterContract;
        this.smallcaseRepository = smallcaseRepository;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public HashMap<String, InvestedSmallcases> getInvestedSmallcaseMap() {
        Gson gson = new Gson();
        HashMap<String, InvestedSmallcases> investedSmallcasesHashMap = new HashMap<>();
        Set<String> stringSet = sharedPrefService.getInvestedSmallcases();
        for (String smallcaseString : stringSet) {
            InvestedSmallcases investedSmallcases = gson.fromJson(smallcaseString, InvestedSmallcases.class);
            investedSmallcasesHashMap.put(investedSmallcases.getScid(), investedSmallcases);
        }
        return investedSmallcasesHashMap;
    }

    @Override
    public void fetchInvestedSmallcaseNews(final List<String> scids, final int offset, final int count) {
        compositeSubscription.add(smallcaseRepository.getSmallcaseNews(sharedPrefService.getAuthorizationToken(), scids, offset, count,
                sharedPrefService.getCsrfToken(), null)
                .subscribe(newses -> allNewsPresenterContract.onNewsFetchedSuccessfully(newses), throwable -> {
                    allNewsPresenterContract.onFailedFetchingNews();

                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    HashMap<String, String> data = new HashMap<>();
                    data.put("offSet", String.valueOf(offset));
                    data.put("count", String.valueOf(count));
                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "News failure", data);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + ": fetchInvestedSmallcaseNews()");
                }));
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }
}
