package com.smallcase.android.news;

import android.app.Activity;
import android.os.Parcelable;

import com.smallcase.android.data.model.InvestedSmallcases;
import com.smallcase.android.data.model.News;
import com.smallcase.android.view.model.PInvestedSmallcase;

import java.util.HashMap;
import java.util.List;

import static android.support.v7.widget.RecyclerView.OnClickListener;

/**
 * Created by shashankm on 12/12/16.
 */

public interface AllNewsContract {

    interface View {

        void onNewsItemsFetched(List<Parcelable> investedSmallcaseWithNewsViewModel);

        void noMoreNews();

        void showSnackBar(int resId);

        void noNewsAvailable();
    }

    interface Service {
        // In the form HashMap<scid, InvestedSmallcases>
        HashMap<String, InvestedSmallcases> getInvestedSmallcaseMap();

        void fetchInvestedSmallcaseNews(List<String> scids, int offset, int count);

        void destroySubscriptions();
    }

    interface Presenter {

        void fetchMoreInvestedSmallcaseNews(int offset, int count, int type);

        OnClickListener getSmallcaseClickListener(final Activity activity, final PInvestedSmallcase smallcaseNews);

        void getNewsClickListener(final Activity activity, final String newsLink);

        void onNewsFetchedSuccessfully(List<News> newses);

        void onFailedFetchingNews();

        void destroySubscriptions();
    }
}
