package com.smallcase.android.news;


import com.smallcase.android.analytics.SentryAnalytics;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;

import java.util.HashMap;
import java.util.List;

import io.intercom.retrofit2.HttpException;
import io.sentry.event.Breadcrumb;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by shashankm on 08/02/17.
 */

class NewsService implements NewsContract.Service {
    private static final String TAG = "NewsService";

    private SmallcaseRepository smallcaseRepository;
    private SharedPrefService sharedPrefService;
    private NewsContract.Presenter presenterContract;
    private CompositeSubscription compositeSubscription;

    NewsService(SmallcaseRepository smallcaseRepository, SharedPrefService sharedPrefService, NewsContract.Presenter presenterContract) {
        this.smallcaseRepository = smallcaseRepository;
        this.sharedPrefService = sharedPrefService;
        this.presenterContract = presenterContract;
        compositeSubscription = new CompositeSubscription();
    }

    @Override
    public void getNewsForSmallcase(List<String> scids, final int offset, final int count) {
        compositeSubscription.add(smallcaseRepository.getSmallcaseNews(sharedPrefService.getAuthorizationToken(), scids, offset, count,
                sharedPrefService.getCsrfToken(), null)
                .subscribe(newses -> presenterContract.onNewsReceived(newses), throwable -> {
                    presenterContract.onFailedFetchingNews();
                    if (throwable instanceof HttpException && ((HttpException) throwable).code() == 401) return;

                    HashMap<String, String> data = new HashMap<>();
                    data.put("offSet", String.valueOf(offset));
                    data.put("count", String.valueOf(count));
                    SentryAnalytics.getInstance().recordEvent(Breadcrumb.Type.HTTP, "News failure", data);
                    SentryAnalytics.getInstance().captureEvent(sharedPrefService.getAuthorizationToken(), throwable, TAG + "; getNewsForSmallcase()");
                }));
    }

    @Override
    public void destroySubscriptions() {
        compositeSubscription.unsubscribe();
    }
}
