package com.smallcase.android.news;

import com.smallcase.android.R;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.model.PPlainNews;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by shashankm on 08/02/17.
 */

class NewsPresenter implements NewsContract.Presenter {
    private NewsContract.Service serviceContract;
    private NetworkHelper networkHelper;
    private NewsContract.View viewContract;
    private int offSet = 5;
    private List<String> scids;

    NewsPresenter(SmallcaseRepository smallcaseRepository, SharedPrefService sharedPrefService, NetworkHelper
            networkHelper, NewsContract.View viewContract, String scid) {
        this.networkHelper = networkHelper;
        this.serviceContract = new NewsService(smallcaseRepository, sharedPrefService, this);
        this.viewContract = viewContract;
        this.scids = new ArrayList<>();
        scids.add(scid);
    }

    @Override
    public void getSmallcaseNews() {
        if (!networkHelper.isNetworkAvailable()) {
            viewContract.showSnackBar(R.string.no_internet);
            return;
        }

        int count = 10;
        serviceContract.getNewsForSmallcase(scids, offSet, count);
    }

    @Override
    public void onNewsReceived(List<News> newses) {
        if (newses.isEmpty()) {
            viewContract.noMoreNews();
            return;
        }
        offSet += 10;
        List<PPlainNews> newsList = new ArrayList<>();
        for (News news : newses) {
            PPlainNews plainNews = new PPlainNews();
            plainNews.setLink(news.getLink());
            plainNews.setNewsHeading(news.getHeadline());
            plainNews.setNewsImage(news.getImageUrl());
            plainNews.setDate(AppUtils.getInstance().getTimeAgoDate(news.getDate()));
            newsList.add(plainNews);
        }
        viewContract.onNewsReceived(newsList);
    }

    @Override
    public void onFailedFetchingNews() {
        viewContract.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void destroySubscriptions() {
        serviceContract.destroySubscriptions();
    }
}
