package com.smallcase.android.news;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.android.ScrollListener;
import com.smallcase.android.view.model.PPlainNews;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewsActivity extends AppCompatActivity implements ScrollListener.ScrollInterface, NewsContract.View {
    public static final String NEWS_LIST = "news_list";
    public static final String SCID = "scid";
    public static final String SMALLCASE_NAME = "smallcase_name";

    @BindView(R.id.text_tool_bar_title) GraphikText toolBarTitle;
    @BindView(R.id.news_list) RecyclerView newsList;
    @BindView(R.id.activity_news) CoordinatorLayout parent;

    List<PPlainNews> newses = new ArrayList<>();
    boolean itemsRemaining = false;

    private boolean fetching = false;
    private NewsContract.Presenter presenterContract;
    private NewsAdapter newsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        ButterKnife.bind(this);

        toolBarTitle.setText("News for " + getIntent().getStringExtra(SMALLCASE_NAME));
        String scid = getIntent().getStringExtra(SCID);
        if (null == scid) return;

        newses = getIntent().getParcelableArrayListExtra(NEWS_LIST);
        presenterContract = new NewsPresenter(new SmallcaseRepository(), new SharedPrefService(this),
                new NetworkHelper(this), this, scid);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        newsList.setLayoutManager(layoutManager);
        newsList.setHasFixedSize(true);
        if (newses.size() == 5) {
            newsList.addOnScrollListener(new ScrollListener(layoutManager, this));
            itemsRemaining = true;
        }

        newsAdapter = new NewsAdapter();
        newsList.setAdapter(newsAdapter);
    }

    @Override
    public void onListEndReached() {
        if (!fetching) {
            fetching = true;
            presenterContract.getSmallcaseNews();
        }
    }

    @Override
    protected void onDestroy() {
        presenterContract.destroySubscriptions();
        super.onDestroy();
    }

    @Override
    public void showSnackBar(int errId) {
        AppUtils.getInstance().showSnackBar(parent, Snackbar.LENGTH_LONG, errId);
    }

    @Override
    public void onNewsReceived(List<PPlainNews> newsList) {
        fetching = false;
        int prevSize = newses.size();
        newses.addAll(newsList);
        newsAdapter.notifyItemRangeChanged(prevSize, newses.size() + 1);
    }

    @OnClick(R.id.back)
    void back() {
        onBackPressed();
    }

    @Override
    public void noMoreNews() {
        fetching = false;

        if (!itemsRemaining) return;

        itemsRemaining = false;
        newsAdapter.notifyItemRemoved(newses.size());
    }

    class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public int getItemViewType(int position) {
            if (position == newses.size() && itemsRemaining) return 0;
            return 1;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            if (viewType == 1) {
                viewHolder = new ViewHolder(inflater.inflate(R.layout.all_news_card, parent, false));
            } else {
                viewHolder = new LoadingHolder(inflater.inflate(R.layout.loading_indicator, parent, false));
            }

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            if (viewHolder.getItemViewType() == 1) {
                ViewHolder holder = (ViewHolder) viewHolder;
                final PPlainNews news = newses.get(position);

                holder.headingLoading.setVisibility(View.GONE);
                holder.dateLoading.setVisibility(View.GONE);

                holder.newsImage.setBackgroundColor(0);
                holder.newsHeadline.setVisibility(View.VISIBLE);
                holder.timeAgo.setVisibility(View.VISIBLE);

                if (null == news.getNewsImage() || news.getNewsImage().equals("")) {
                    holder.newsImage.setVisibility(View.GONE);
                } else {
                    holder.newsImage.setVisibility(View.VISIBLE);
                    Glide.with(NewsActivity.this)
                            .load(news.getNewsImage())
                            .asBitmap()
                            .placeholder(getResources().getDrawable(R.color.grey_300))
                            .into(holder.newsImage);
                }
                holder.newsHeadline.setText(news.getNewsHeading());
                holder.timeAgo.setText(news.getDate());

                holder.newsCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(NewsActivity.this, WebActivity.class);
                        intent.putExtra(WebActivity.TITLE, getString(R.string.smallcase_news));
                        intent.putExtra(WebActivity.URL, news.getLink());
                        intent.putExtra(WebActivity.IS_LITE_URL, true);
                        startActivity(intent);
                        overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            if (itemsRemaining) return newses.size() + 1;
            return newses.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.news_card) View newsCard;
            @BindView(R.id.image_p_l_today) ImageView newsImage;
            @BindView(R.id.text_news_headline) GraphikText newsHeadline;
            @BindView(R.id.text_time_ago) GraphikText timeAgo;
            @BindView(R.id.headline_loading) View headingLoading;
            @BindView(R.id.date_loading) View dateLoading;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

        private class LoadingHolder extends RecyclerView.ViewHolder {

            LoadingHolder(View itemView) {
                super(itemView);
            }
        }
    }
}
