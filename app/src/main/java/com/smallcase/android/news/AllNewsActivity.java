package com.smallcase.android.news;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.smallcase.android.R;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.analytics.AnalyticsManager;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.user.dashboard.DashboardPresenter;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.AppUtils;
import com.smallcase.android.util.InvestmentHelper;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.adapter.InvestedSmallcaseNewsAdapter;
import com.smallcase.android.view.android.GraphikText;
import com.smallcase.android.view.android.ScrollListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AllNewsActivity extends AppCompatActivity implements AllNewsContract.View, ScrollListener.ScrollInterface {
    public static final String SCREEN_TITLE = "screen_title";
    public static final String DATA_LIST = "data_list";
    public static final String SCID_LIST = "scid_list";
    public static final String TYPE = "type";

    private static final String TAG = AllNewsActivity.class.getSimpleName();

    @BindView(R.id.tool_bar) Toolbar toolbar;
    @BindView(R.id.text_tool_bar_title) GraphikText title;
    @BindView(R.id.small_case_news_list) RecyclerView newsList;
    @BindView(R.id.parent) View parentView;
    @BindView(R.id.empty_state_container) View noInvestmentsContainer;
    @BindView(R.id.text_empty_state) GraphikText emptyStateText;
    @BindView(R.id.empty_state_icon) ImageView noNewsIcon;
    @BindView(R.id.loading_indicator) View loadingIndicator;

    private InvestedSmallcaseNewsAdapter adapter;
    // Indicates if the data is already being fetched (but not yet fetched)
    private boolean loading = false;
    private AllNewsContract.Presenter allNewsPresenter;
    private int type;
    private final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
    private AnalyticsContract analyticsContract;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_news);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        title.setText(getIntent().getStringExtra(SCREEN_TITLE));

        newsList.setLayoutManager(layoutManager);
        newsList.setHasFixedSize(true);

        analyticsContract = new AnalyticsManager(getApplicationContext());
        ArrayList<Parcelable> newsItemsList = getIntent().getParcelableArrayListExtra(DATA_LIST);
        type = getIntent().getIntExtra(TYPE, AppConstants.NEWS_TYPE_UN_INVESTED);
        SharedPrefService sharedPrefService = new SharedPrefService(this);
        allNewsPresenter = new AllNewsPresenter(analyticsContract, new DashboardPresenter(new InvestmentHelper(), sharedPrefService), new NetworkHelper(this),
                new SmallcaseRepository(), sharedPrefService, this, getIntent().getStringArrayListExtra(SCID_LIST));

        if (newsItemsList == null || newsItemsList.isEmpty()) {
            loading = true;
            allNewsPresenter.fetchMoreInvestedSmallcaseNews(0, 10, type);
            return;
        }

        setAdapter(newsItemsList);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        allNewsPresenter.destroySubscriptions();
        super.onDestroy();
    }

    @Override
    public void onNewsItemsFetched(List<Parcelable> investedSmallcaseWithNewsViewModel) {
        // Not showing loading indicator
        loading = false;

        if (adapter == null) {
            setAdapter(investedSmallcaseWithNewsViewModel);
            return;
        }

        int previousAdapterSize = adapter.getNewsItems().size();

        // Append received news view model to existing news
        adapter.getNewsItems().addAll(investedSmallcaseWithNewsViewModel);
        adapter.notifyItemRangeChanged(previousAdapterSize, adapter.getNewsItems().size() + 1);
    }

    @Override
    public void noMoreNews() {
        if (adapter == null) return;

        adapter.noMoreItems();
        // Remove loading indicator
        adapter.notifyItemRemoved(adapter.getNewsItems().size());
        adapter.notifyItemChanged(adapter.getNewsItems().size());
    }

    @Override
    public void showSnackBar(int resId) {
        AppUtils.getInstance().showSnackBar(parentView, Snackbar.LENGTH_LONG, resId);
    }

    @Override
    public void noNewsAvailable() {
        loadingIndicator.setVisibility(View.GONE);
        if (adapter == null) {
            noInvestmentsContainer.setVisibility(View.VISIBLE);
            Glide.with(this)
                    .load(R.drawable.no_news)
                    .into(noNewsIcon);
            emptyStateText.setText("No recent news available for your investments");
        }
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onListEndReached() {
        // Already fetching news?
        if (!loading) {
            if (adapter == null || adapter.getNewsItems() == null) return;

            // Nope, need to fetch them
            loading = true;
            allNewsPresenter.fetchMoreInvestedSmallcaseNews(adapter.getNewsItems().size(), 10, type);
        }
    }

    private void setAdapter(List<Parcelable> newsItemsList) {
        loadingIndicator.setVisibility(View.GONE);
        adapter = new InvestedSmallcaseNewsAdapter(AllNewsActivity.this, allNewsPresenter, newsItemsList, analyticsContract);
        RecyclerView.ItemAnimator animator = newsList.getItemAnimator();

        // Disable default grey flicker "animation" when an item changes.
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }

        newsList.setAdapter(adapter);

        if (newsItemsList.size() < 5) {
            adapter.noMoreItems();
            return;
        }

        newsList.addOnScrollListener(new ScrollListener(layoutManager, this));
    }
}
