package com.smallcase.android.news;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.View;

import com.smallcase.android.R;
import com.smallcase.android.analytics.Analytics;
import com.smallcase.android.analytics.AnalyticsContract;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.Smallcases;
import com.smallcase.android.data.repository.SharedPrefService;
import com.smallcase.android.data.repository.SmallcaseRepository;
import com.smallcase.android.investedsmallcase.InvestedSmallcaseDetailActivity;
import com.smallcase.android.user.dashboard.DashboardPresenter;
import com.smallcase.android.util.AppConstants;
import com.smallcase.android.util.NetworkHelper;
import com.smallcase.android.view.model.PInvestedSmallcase;
import com.smallcase.android.view.model.PNewsSmallcase;
import com.smallcase.android.view.model.PSmallcaseWithNews;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by shashankm on 12/12/16.
 */

public class AllNewsPresenter implements AllNewsContract.Presenter {
    private static final String TAG = "AllNewsPresenter";

    private DashboardPresenter dashboardPresenter;
    private NetworkHelper networkHelper;
    private AllNewsContract.Service allNewsServiceContract;
    private AllNewsContract.View allNewsViewContract;
    private List<String> scids;
    private int type;
    private AnalyticsContract analyticsContract;

    public AllNewsPresenter(AnalyticsContract analyticsContract, DashboardPresenter dashboardPresenter, NetworkHelper networkHelper, SmallcaseRepository smallcaseRepository,
                            SharedPrefService sharedPrefService, AllNewsContract.View allNewsViewContract, @Nullable List<String> scids) {
        this.dashboardPresenter = dashboardPresenter;
        this.networkHelper = networkHelper;
        this.analyticsContract = analyticsContract;
        this.allNewsServiceContract = new AllNewsService(sharedPrefService, this, smallcaseRepository);
        this.allNewsViewContract = allNewsViewContract;
        this.scids = scids;
    }

    @Override
    public void fetchMoreInvestedSmallcaseNews(int offset, int count, int type) {
        this.type = type;
        if (!networkHelper.isNetworkAvailable()) {
            allNewsViewContract.showSnackBar(R.string.no_internet);
            return;
        }

        allNewsServiceContract.fetchInvestedSmallcaseNews(scids, offset, count);
    }

    @Override
    public View.OnClickListener getSmallcaseClickListener(final Activity activity, final PInvestedSmallcase smallcaseNews) {
        return v -> {
            Map<String, Object> eventProperties = new HashMap<>();
            eventProperties.put("accessedFrom", "All News");
            eventProperties.put("smallcaseName", smallcaseNews.getSmallcaseName());
            eventProperties.put("isInvested", true);
            eventProperties.put("isWatchlisted", dashboardPresenter.isSmallcaseWatchlisted(smallcaseNews.getScid()));
            analyticsContract.sendEvent(eventProperties, "Viewed Investment Details", Analytics.MIXPANEL, Analytics.CLEVERTAP);

            Intent intent = new Intent(activity, InvestedSmallcaseDetailActivity.class);
            intent.putExtra(InvestedSmallcaseDetailActivity.SCID, smallcaseNews.getScid());
            intent.putExtra(InvestedSmallcaseDetailActivity.ISCID, smallcaseNews.getiScid());
            intent.putExtra(InvestedSmallcaseDetailActivity.SOURCE, smallcaseNews.getSource());
            activity.startActivity(intent);
        };
    }

    @Override
    public void getNewsClickListener(final Activity activity, String newsLink) {
        Intent intent = new Intent(activity, WebActivity.class);
        intent.putExtra(WebActivity.TITLE, activity.getString(R.string.smallcase_news));
        intent.putExtra(WebActivity.URL, newsLink);
        intent.putExtra(WebActivity.IS_LITE_URL, true);
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim.down_to_top, R.anim.fade_out_with_scale);
    }

    @Override
    public void onNewsFetchedSuccessfully(List<News> newses) {
        if (newses.isEmpty()) {
            allNewsViewContract.noNewsAvailable();
            allNewsViewContract.noMoreNews();
            return;
        }

        switch (type) {
            case AppConstants.NEWS_TYPE_INVESTED:
                createInvestedNewsViewModel(newses);
                break;

            case AppConstants.NEWS_TYPE_UN_INVESTED:
                createUnInvestedNewsViewModel(newses);
                break;

            case AppConstants.NEWS_TYPE_WATCHLISTED:
                createWatchlistViewModel(newses);
                break;
        }
    }

    @Override
    public void onFailedFetchingNews() {
        allNewsViewContract.showSnackBar(R.string.something_wrong);
    }

    @Override
    public void destroySubscriptions() {
        allNewsServiceContract.destroySubscriptions();
    }

    private void createWatchlistViewModel(List<News> newses) {
        List<Parcelable> watchlistedNews = new ArrayList<Parcelable>(createWatchlistedSmallcaseWithNewsViewModel(newses));
        allNewsViewContract.onNewsItemsFetched(watchlistedNews);
    }

    private void createInvestedNewsViewModel(List<News> newses) {
        List<Parcelable> unInvestedSmallcaseWithNewses = new ArrayList<Parcelable>(dashboardPresenter
                .createInvestedSmallcaseWithNewsViewModel(newses, allNewsServiceContract.getInvestedSmallcaseMap()));

        allNewsViewContract.onNewsItemsFetched(unInvestedSmallcaseWithNewses);
    }

    private void createUnInvestedNewsViewModel(List<News> newses) {
        List<Parcelable> smallcaseWithNewses = new ArrayList<>();
        for (News news : newses) {
            List<PNewsSmallcase> newsSmallcases = new ArrayList<>();
            for (Smallcases smallcase : news.getSmallcases()) {
                double change = ((smallcase.getStats().getIndexValue() - smallcase.getInitialIndex()) / smallcase.getInitialIndex()) * 100;
                boolean isChangePositive = change > 0;
                String changePercent = String.format(Locale.getDefault(), "%.2f", change) + "%";
                PNewsSmallcase newsSmallcase = new PNewsSmallcase("80", smallcase.getInfo().getName(), changePercent,
                        smallcase.get_id(), smallcase.getScid(), isChangePositive, smallcase.getInfo().getTier());
                newsSmallcases.add(newsSmallcase);
            }
            PSmallcaseWithNews smallcaseWithNews = new PSmallcaseWithNews(news.getImageUrl(), news.getHeadline(),
                    news.getDate(), newsSmallcases, news.getLink());
            smallcaseWithNewses.add(smallcaseWithNews);
        }

        allNewsViewContract.onNewsItemsFetched(smallcaseWithNewses);
    }

    private List<PSmallcaseWithNews> createWatchlistedSmallcaseWithNewsViewModel(List<News> newses) {
        List<PSmallcaseWithNews> smallcaseWithNewses = new ArrayList<>();
        for (News news : newses) {
            List<PNewsSmallcase> newsSmallcaseList = new ArrayList<>();
            for (Smallcases smallcase : news.getSmallcases()) {
                double change = ((smallcase.getStats().getIndexValue() - smallcase.getInitialIndex()) / smallcase.getInitialIndex()) * 100;
                boolean isChangePositive = change > 0;
                String changePercent = String.format(Locale.getDefault(), "%.2f", change) + "%";
                PNewsSmallcase newsSmallcase = new PNewsSmallcase("80", smallcase.getInfo().getName(), changePercent,
                        smallcase.get_id(), smallcase.getScid(), isChangePositive, smallcase.getInfo().getTier());
                newsSmallcaseList.add(newsSmallcase);
            }
            PSmallcaseWithNews smallcaseWithNews = new PSmallcaseWithNews(news.getImageUrl(), news.getHeadline(),
                    news.getDate(), newsSmallcaseList, news.getLink());
            smallcaseWithNewses.add(smallcaseWithNews);
        }
        return smallcaseWithNewses;
    }
}
