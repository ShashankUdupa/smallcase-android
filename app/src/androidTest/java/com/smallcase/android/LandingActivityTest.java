package com.smallcase.android;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.smallcase.android.onboarding.LandingActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.not;

/**
 * Created by shashankm on 11/11/16.
 */

@RunWith(AndroidJUnit4.class)
public class LandingActivityTest {
    @Rule
    public ActivityTestRule<LandingActivity> activityTestRule =
            new ActivityTestRule<>(LandingActivity.class, true, true);

    @Test
    public void checkSignUpFormVisibility() {
        onView(ViewMatchers.withId(R.id.text_sign_up_layout)).perform(click());

        onView(withId(R.id.layout_sign_up)).check(matches(isCompletelyDisplayed()));
    }

    @Test
    public void checkOverlapBetweenLoginAndLogo() {
        onView(withId(R.id.text_sign_up_layout)).perform(click());
    }

    @Test
    public void checkIfViewsToggleWithKeyboardShows() {
        onView(withId(R.id.text_sign_up_layout)).perform(click());
        onView(withId(R.id.text_name)).perform(click());
        onView(withId(R.id.card_login)).check(matches(not(isDisplayed())));

        onView(withId(R.id.text_name)).perform(pressImeActionButton());
        onView(withId(R.id.card_login)).check(matches(not(isDisplayed())));

        onView(withId(R.id.text_email)).perform(pressImeActionButton());
        onView(withId(R.id.card_login)).check(matches(not(isDisplayed())));

        Espresso.closeSoftKeyboard();
        onView(withId(R.id.card_login)).check(matches(isCompletelyDisplayed()));
    }

    @Test
    public void checkIfLoadingDialogShows() {
        onView(withId(R.id.text_sign_up_layout)).perform(click());

        onView(withId(R.id.text_name)).perform(typeText("Testing M"));
        onView(withId(R.id.text_name)).perform(pressImeActionButton());
        onView(withId(R.id.text_email)).perform(typeText("sa@sma.com"));
        onView(withId(R.id.text_email)).perform(pressImeActionButton());
        onView(withId(R.id.text_phone)).perform(typeText("8888888888"));
        onView(withId(R.id.text_phone)).perform(closeSoftKeyboard());

        onView(withId(R.id.create_account)).perform(click());

        onView(withId(R.id.sign_up_message)).check(matches(isDisplayed()));
    }
}
