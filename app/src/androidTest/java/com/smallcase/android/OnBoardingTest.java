package com.smallcase.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.ImageView;

import com.smallcase.android.onboarding.LandingActivity;
import com.smallcase.android.onboarding.LandingContract;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayingAtLeast;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.not;

/**
 * Created by shashankm on 29/11/16.
 */
@RunWith(AndroidJUnit4.class)
public class OnBoardingTest {
    @Rule
    public ActivityTestRule<LandingActivity> activityTestRule =
            new ActivityTestRule<>(LandingActivity.class);
    private LandingContract.View landingContract;

    @Before
    public void setUp() throws Exception {
        landingContract = activityTestRule.getActivity();
    }

    @Test
    public void showAlreadyRegisteredError() throws Exception {

    }

    @Test
    public void showDuplicateLeadError() throws Exception {

    }

    @Test
    public void showAccountRequested() throws Exception {


    }

    private void basicSignUpChecks() {
        onView(withId(R.id.sign_up_loading)).check(matches(not(isDisplayed())));
        onView(withId(R.id.sign_up_message)).check(matches(isDisplayed()));
        onView(withId(R.id.card_login)).check(matches(isDisplayingAtLeast(100)));
    }

    private Matcher<View> withImageDrawable(final int resourceId) {
        return new BoundedMatcher<View, ImageView>(ImageView.class) {
            @Override
            public void describeTo(Description description) {
                description.appendText("has opening_bell drawable resource " + resourceId);
            }

            @Override
            public boolean matchesSafely(ImageView imageView) {
                return sameBitmap(imageView.getContext(), imageView.getDrawable(), resourceId);
            }
        };
    }

    private boolean sameBitmap(Context context, Drawable drawable, int resourceId) {
        Drawable otherDrawable = context.getResources().getDrawable(resourceId);
        if (drawable == null || otherDrawable == null) {
            return false;
        }
        if (drawable instanceof StateListDrawable && otherDrawable instanceof StateListDrawable) {
            drawable = drawable.getCurrent();
            otherDrawable = otherDrawable.getCurrent();
        }
        if (drawable instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Bitmap otherBitmap = ((BitmapDrawable) otherDrawable).getBitmap();
            return bitmap.sameAs(otherBitmap);
        }
        return false;
    }
}
