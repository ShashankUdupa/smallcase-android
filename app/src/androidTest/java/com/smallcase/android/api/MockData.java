package com.smallcase.android.api;

/**
 * Created by shashankm on 29/11/16.
 */
public class MockData {
    public static final String FAILURE_JSON = "{\n" +
            "  \"success\": false,\n" +
            "  \"errors\": null,\n" +
            "  \"data\": {}\n" +
            "}";

    public static final String ALREADY_REGISTERED_JSON = "{\n" +
            "  \"success\": false,\n" +
            "  \"errors\": \"email already registered\",\n" +
            "  \"data\": {\n" +
            "    \"success\": false,\n" +
            "    \"status\": \"ALREADYREGISTERED\"\n" +
            "  }\n" +
            "}";

    public static final String DUPLICATE_LEAD_JSON = "{\n" +
            "  \"success\": false,\n" +
            "  \"errors\": \"email already registered\",\n" +
            "  \"data\": {\n" +
            "    \"success\": false,\n" +
            "    \"status\": \"DUPLICATELEAD\"\n" +
            "  }\n" +
            "}";

    public static final String NIFTY_PROPER_RESPONSE_JSON = "{\n" +
            "  \"success\": true,\n" +
            "  \"errors\": null,\n" +
            "  \"data\": {\n" +
            "    \".NSEI\": {\n" +
            "      \"price\": 8126.9,\n" +
            "      \"change\": \"NaN\",\n" +
            "      \"date\": \"2016-11-28T11:34:08.440Z\",\n" +
            "      \"close\": 8126.9\n" +
            "    }\n" +
            "  }\n" +
            "}";

    public static final String TOTAL_INVESTMENTS_PROPER_RESPONSE_JSON = "{\n" +
            "  \"success\": true,\n" +
            "  \"errors\": null,\n" +
            "  \"data\": {\n" +
            "    \"otherReturns\": 0,\n" +
            "    \"divReturns\": 0,\n" +
            "    \"realizedReturns\": 0,\n" +
            "    \"realizedInvestment\": 0,\n" +
            "    \"unrealizedInvestment\": 122890.45999999999,\n" +
            "    \"networth\": 128797\n" +
            "  }\n" +
            "}";

    public static final String INVESTED_NEWS_PROPER_RESPONSE_JSON = "{\n" +
            "  \"success\": true,\n" +
            "  \"errors\": null,\n" +
            "  \"data\": [\n" +
            "    {\n" +
            "      \"_id\": \"5832ba552ccff54c334a62b3\",\n" +
            "      \"source\": \"pocket\",\n" +
            "      \"link\": \"http://www.ndtv.com/india-news/will-take-on-board-gst-bankruptcy-code-in-next-report-world-bank-1587069\",\n" +
            "      \"date\": \"2016-11-01T00:00:00.000Z\",\n" +
            "      \"headline\": \"Will Take On Board GST, Bankruptcy Code In Next Report: World Bank\",\n" +
            "      \"summary\": \"New Delhi: The World Bank will take on board implementation of the goods and services tax and enactment of the bankruptcy code while preparing the ease of doing business report next year, said its country Director Junaid Ahmad today.\",\n" +
            "      \"imageUrl\": \"https://img.readitlater.com/i/img.readitlater.com/i/i.ndtvimg.com/i/2016-08/gst-generic-afp_650x400_71471593758.jpg\",\n" +
            "      \"videoUrl\": null,\n" +
            "      \"status\": \"published\",\n" +
            "      \"sids\": null,\n" +
            "      \"smallcases\": [\n" +
            "        {\n" +
            "          \"_id\": \"5704fb528cedad2432b3ca65\",\n" +
            "          \"scid\": \"SCNM_0004\",\n" +
            "          \"initialIndex\": 275.77077726223,\n" +
            "          \"stats\": {\n" +
            "            \"indexValue\": 275.7707772622302\n" +
            "          },\n" +
            "          \"info\": {\n" +
            "            \"type\": \"Thematic\",\n" +
            "            \"name\": \"The GST Opportunity\",\n" +
            "            \"shortDescription\": \"Companies & sectors expected to benefit with the passage of GST bill\"\n" +
            "          }\n" +
            "        },\n" +
            "        {\n" +
            "          \"_id\": \"5704fb528cedad2432b3ca66\",\n" +
            "          \"scid\": \"SCNM_0005\",\n" +
            "          \"initialIndex\": 337.406443957272,\n" +
            "          \"stats\": {\n" +
            "            \"indexValue\": 337.406443957272\n" +
            "          },\n" +
            "          \"info\": {\n" +
            "            \"type\": \"Thematic\",\n" +
            "            \"name\": \"The GST Opportunity - Low-Cost Version\",\n" +
            "            \"shortDescription\": \"Companies & sectors expected to benefit with the passage of GST bill\"\n" +
            "          }\n" +
            "        },\n" +
            "        {\n" +
            "          \"_id\": \"5704fb528cedad2432b3ca62\",\n" +
            "          \"scid\": \"SCNM_0001\",\n" +
            "          \"initialIndex\": 241.779317669389,\n" +
            "          \"stats\": {\n" +
            "            \"indexValue\": 241.779317669389\n" +
            "          },\n" +
            "          \"info\": {\n" +
            "            \"type\": \"Thematic\",\n" +
            "            \"name\": \"Transporting India\",\n" +
            "            \"shortDescription\": \"Logistics companies poised for rapid growth, fuelled by GDP revival, GST and e-commerce\"\n" +
            "          }\n" +
            "        },\n" +
            "        {\n" +
            "          \"_id\": \"5704fb528cedad2432b3ca63\",\n" +
            "          \"scid\": \"SCNM_0002\",\n" +
            "          \"initialIndex\": 257.012562993187,\n" +
            "          \"stats\": {\n" +
            "            \"indexValue\": 257.012562993187\n" +
            "          },\n" +
            "          \"info\": {\n" +
            "            \"type\": \"Thematic\",\n" +
            "            \"name\": \"Transporting India - Low-Cost Version\",\n" +
            "            \"shortDescription\": \"Logistics companies poised for rapid growth, fuelled by GDP revival, GST and e-commerce\"\n" +
            "          }\n" +
            "        }\n" +
            "      ],\n" +
            "      \"publisher\": {\n" +
            "        \"publisherId\": \"www.ndtv.com\",\n" +
            "        \"name\": null,\n" +
            "        \"logo\": null\n" +
            "      },\n" +
            "      \"tags\": [\n" +
            "        \"gst opportunity nm_4\",\n" +
            "        \"gst opportunity nm_5\",\n" +
            "        \"transport india nm_1\",\n" +
            "        \"transport india nm_2\"\n" +
            "      ],\n" +
            "      \"__v\": 0\n" +
            "    }\n" +
            "  ]\n" +
            "}";
}
