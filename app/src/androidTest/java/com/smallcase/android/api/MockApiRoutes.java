package com.smallcase.android.api;

import android.support.annotation.Nullable;

import com.smallcase.android.data.model.Actions;
import com.smallcase.android.data.model.Collection;
import com.smallcase.android.data.model.DraftSmallcase;
import com.smallcase.android.data.model.ExitedSmallcase;
import com.smallcase.android.data.model.Historical;
import com.smallcase.android.data.model.ImageUploadOptions;
import com.smallcase.android.data.model.Investments;
import com.smallcase.android.data.model.News;
import com.smallcase.android.data.model.Nifty;
import com.smallcase.android.data.model.Notification;
import com.smallcase.android.data.model.Order;
import com.smallcase.android.data.model.SearchResult;
import com.smallcase.android.data.model.Similar;
import com.smallcase.android.data.model.SipDetails;
import com.smallcase.android.data.model.Smallcase;
import com.smallcase.android.data.model.SmallcaseDetail;
import com.smallcase.android.data.model.SmallcaseLedger;
import com.smallcase.android.data.model.SmallcaseOrder;
import com.smallcase.android.data.model.StockInfo;
import com.smallcase.android.data.model.StockPriceAndChange;
import com.smallcase.android.data.model.TotalInvestment;
import com.smallcase.android.data.model.UnInvestedSmallcase;
import com.smallcase.android.data.model.User;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by shashankm on 29/11/16.
 */

class MockApiRoutes implements ApiRoutes {

    @Override
    public Observable<ResponseBody> brokerLogin(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<ResponseBody> getRequestToken(String app) {
        return null;
    }

    @Override
    public Observable<ResponseBody> loginSmallcase(HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<ResponseBody> requestAccount(@Body HashMap<String, Object> body) {
        return null;
    }

    @Override
    public Observable<User> getUserDetails() {
        return null;
    }

    @Override
    public Observable<Nifty> getMarketPriceAndChange(@Query("stocks") String stocks) {
        return null;
    }

    @Override
    public Observable<TotalInvestment> getInvestmentsTotal() {
        return null;
    }

    @Override
    public Observable<Investments> getInvestedScallcases() {
        return null;
    }

    @Override
    public Observable<List<News>> getNewsForInvestedSmallcase(List<String> scids, int offset, int count, List<String> sids) {
        return null;
    }

    @Override
    public Observable<List<News>> getNewsForInvestedSmallcase(@Query("scids") List<String> scids, @Query("offset") int offset, @Query("count") int count) {
        return null;
    }

    @Override
    public Observable<List<ExitedSmallcase>> getExitedSmallcases() {
        return null;
    }

    @Override
    public Observable<SmallcaseDetail> getInvestedSmallcaseDetails(@Query("iscid") String iScid) {
        return null;
    }

    @Override
    public Observable<ResponseBody> getStocksPrice(@Query("stocks") List<String> sids) {
        return null;
    }

    @Override
    public Observable<StockInfo> getStockInfoAndHistorical(@Query("stocks") String sid) {
        return null;
    }

    @Override
    public Observable<ResponseBody> getStockInfo(@Query("stocks") List<String> stocks) {
        return null;
    }

    @Override
    public Observable<StockPriceAndChange> getStockPriceAndChange(@Query("stocks") String sid) {
        return null;
    }

    @Override
    public Observable<ResponseBody> registerDeviceForPushNotification(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<ResponseBody> deRegisterDeviceFromPushNotification(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<ResponseBody> getSettings(@Query("setting") String settingsType) {
        return null;
    }

    @Override
    public Observable<ResponseBody> setSettings(@Body HashMap<String, Object> body) {
        return null;
    }

    @Override
    public Observable<List<Smallcase>> getFeaturedSmallcases() {
        return null;
    }

    @Override
    public Observable<List<Smallcase>> getSmallcases(List<String> types, List<String> scids, String sortBy, int sortOrder, int offSet, int count, @Nullable Integer minMinInvestAmount, @Nullable Integer maxMinInvestAmount, String searchString, String sid, @Nullable String tier) {
        return null;
    }

    @Override
    public Observable<List<Collection>> getCollections() {
        return null;
    }

    @Override
    public Observable<ResponseBody> logoutUser() {
        return null;
    }

    @Override
    public Observable<UnInvestedSmallcase> getSmallcaseDetails(@Query("scid") String scid) {
        return null;
    }

    @Override
    public Observable<Historical> getSmallcaseHistorical(@Query("scid") String scid, @Query("benchmarkType") String benchmarkType, @Query("benchmarkId") String benchmarkId, @Query("duration") String duration) {
        return null;
    }

    @Override
    public Observable<ResponseBody> getAvailableFunds() {
        return null;
    }

    @Override
    public Observable<ResponseBody> placeOrder(@Body SmallcaseOrder smallcaseOrder) {
        return null;
    }

    @Override
    public Observable<ResponseBody> getIfMarketIsOpen() {
        return null;
    }


    @Override
    public Observable<ResponseBody> addToWatchlist(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<ResponseBody> removeFromWatchlist(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<List<Order>> getOrderDetails(@Query("iscid") String iScid, @Query("batchId") String batchId, @Query("onlyOne") boolean onlyOne, @Query("noDetails") boolean noDetails) {
        return null;
    }

    @Override
    public Observable<ResponseBody> cancelBatch(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<ResponseBody> fixBatch(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<List<SearchResult>> getStocksInfo(@Query("text") String searchText) {
        return null;
    }

    @Override
    public Observable<List<Similar>> getSimilarStocks(@Query("stocks") List<String> stocks) {
        return null;
    }

    @Override
    public Observable<List<SmallcaseLedger>> getFees() {
        return null;
    }

    @Override
    public Observable<List<Notification>> getNotifications() {
        return null;
    }

    @Override
    public Observable<ResponseBody> markNotificationAsRead(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<ResponseBody> markAllNotificationAsRead() {
        return null;
    }

    @Override
    public Observable<ResponseBody> addReminder(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<Actions> getUserUpdates() {
        return null;
    }

    @Override
    public Observable<ResponseBody> cancelUpdates(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<ResponseBody> noOrders(@Body HashMap<String, Object> body) {
        return null;
    }

    @Override
    public Observable<List<DraftSmallcase>> getDrafts() {
        return null;
    }

    @Override
    public Observable<DraftSmallcase> getDraft(@Query("did") String did) {
        return null;
    }

    @Override
    public Observable<ResponseBody> saveDrafts(DraftSmallcase draftSmallcase) {
        return null;
    }

    @Override
    public Observable<ResponseBody> deleteDraft(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<ResponseBody> ignoreAllBuyReminders() {
        return null;
    }

    @Override
    public Observable<ResponseBody> setFlag(@Body HashMap<String, Object> body) {
        return null;
    }

    @Override
    public Observable<ResponseBody> updateProfile(@Body HashMap<String, Object> body) {
        return null;
    }

    @Override
    public Observable<SipDetails> getSipDetails(String iscid) {
        return null;
    }

    @Override
    public Observable<ResponseBody> setUpSip(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<ResponseBody> manageSip(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<ResponseBody> endSip(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<ResponseBody> skipSip(@Body HashMap<String, String> body) {
        return null;
    }

    @Override
    public Observable<ResponseBody> getStockHistorical(@Query("stocks") List<String> stocks, @Query("duration") String duration) {
        return null;
    }

    @Override
    public Observable<ResponseBody> getUpdateStatus(@Query("app") String app, @Query("version") Integer version, @Query("os") String os) {
        return null;
    }

    @Override
    public Observable<ImageUploadOptions> getImageUploadConfig(String scid) {
        return null;
    }

    @Override
    public Observable<ResponseBody> uploadDraftImage(LinkedHashMap<String, RequestBody> partMap, MultipartBody.Part file) {
        return null;
    }
}
