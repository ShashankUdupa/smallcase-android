package com.smallcase.android;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.smallcase.android.user.ContainerActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by shashankm on 25/11/16.
 */
@RunWith(AndroidJUnit4.class)
public class ContainerActivityTest {
    @Rule
    public ActivityTestRule<ContainerActivity> activityTestRule =
            new ActivityTestRule<>(ContainerActivity.class);

    @Test
    public void checkIfViewPagerCannotBeSwiped() {
        onView(withId(R.id.view_pager)).perform(swipeRight());

        onView(withId(R.id.text_tool_bar_title)).check(matches(withText("Home")));
    }
}